﻿using UnityEngine;
using System.Collections;

public class EngdinCanvas : MonoBehaviour {

	[SerializeField] private float time;
	[SerializeField] private GameObject objCloud;

	void Start () {
        SoundManager.Instance.PlayBGM(SoundManager.Type.ED);
	}
	
	void Update () {
		//一定時間ごとに処理を行う
		time -= Time.deltaTime;
		if (time <= 0.0)
		{
			time = 6.0f;

			GameObject temp =  Instantiate(objCloud,new Vector3(0,220+Random.Range(10,50),0), Quaternion.identity) as GameObject;
			temp.transform.SetParent(this.transform, false);
			Destroy(temp, 20f);
		}
	}

	public void OnButtonPressed()
	{
		DataManager.GetInstance().ChangeToTrueEnding();
	}
}
