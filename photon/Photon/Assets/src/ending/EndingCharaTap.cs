﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;
using UnityEngine.EventSystems;

public class EndingCharaTap : MonoBehaviour {

	[SerializeField] private Sprite Chara1, Chara2, Chara3;
	[SerializeField] private Image CharaImage;
	private int i = 1;

	public void TapCharaChange()
	{
		if(i == 1) {
			CharaImage.sprite = Chara1;
			i += 1;
		}
		else if(i == 2) {
			CharaImage.sprite = Chara2;
			i += 1;
		}
		else {
			CharaImage.sprite = Chara3;
			i = 1;
		}

	}
}
