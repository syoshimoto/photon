﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;
using UnityEngine.EventSystems;
using System.Collections;

public class MonsterExampleCanvas : MonoBehaviour {

	[SerializeField] private Text text1, text2;
	[SerializeField] private GameObject image1, image2;

	// Use this for initialization
	void Start () {
		// 出現する敵の判定
		switch(Game.QuestionId)
		{
			case 0:
				text1.text = "　Ｘ　 ＝ 20の九九をみつけよう！";
				text2.text = "1たいめのまものがあらわれた！";
				image1.SetActive(true);
				break;
			case 1:
				text1.text = "　Ｘ　 ＝ 30の九九をみつけよう！";
				text2.text = "2たいめのまものがあらわれた！";
				image2.SetActive(true);
				break;
		}
    //    HiddenCommand.GetInstance().Reset();
	}

    void Update ()
    {
        if (HiddenCommand.GetInstance().Check())
        {
            OnButtonPressed();
        }
    }
    public void OnButtonPressed()
    {
		DataManager.GetInstance().ChangeToBattle();
    }
}
