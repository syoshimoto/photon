﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class TeacherStartCanvas : Photon.MonoBehaviour
{
	private static TeacherStartCanvas mInstance;
	public TeacherStartCanvas GetInstance()
	{
		return mInstance;
	}
    public void Awake()
    {
        if (!PhotonNetwork.connected)
        {
            Application.LoadLevel(Scene.Title);
            return;
        }
        PhotonNetwork.Instantiate("prefabs/PlayerNetwork", transform.position, Quaternion.identity, 0);
    }
	void Start () 
	{
		mInstance = this;
	}
	[SerializeField]
	private Text text;
	[SerializeField]
	private Button button;
	
	public void SetIsTeacher(bool isTeacher)
	{
		if (isTeacher)
		{
			text.text = "teacher";
		}
		else
		{
			text.text = "child";
			button.gameObject.SetActive(false);
		}
	}
	public void OnPressButton()
	{
		DataManager.GetInstance().ChangeToBattle();
	}
}
