﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class BattleWatchingPlayer : MonoBehaviour {
    [SerializeField]
    private Text nameText;
    private PlayerNetwork playerNetwork;
    [SerializeField]
    private Animator animator;
    [SerializeField]
    private Image image;
    private int score;
    public void Initialize(PlayerNetwork playerNetwork)
    {
        this.playerNetwork = playerNetwork;
        this.nameText.text = playerNetwork.Data.NowQuestionScore.ToString();
        score = 0;
        UpdateScore();
    }
    private void Update()
    {
        UpdateScore();
    }
    private void UpdateScore()
    {
        if (score != playerNetwork.Data.NowQuestionScore)
        {
            Attack();
            score = playerNetwork.Data.NowQuestionScore;
            this.nameText.text = score.ToString();
        }
    }
    public void SetSprite(Sprite sprite)
    {
        image.sprite = sprite;
    }
    public void Attack()
    {
        animator.SetTrigger("Attack");
        SoundManager.Instance.PlaySE(SEType.Hit);
    }
}
