﻿using UnityEngine;
using System.Collections;

public class EffectManager : MonoBehaviour {

	public GameObject[] effectObj;
	private int EffectNo = 0;

	// Use this for initialization
	void Start () {
		EffectNo = 0;
	}
	
	// Update is called once per frame
	void Update () {
	}

	public void Damage()
	{

		// プレハブからインスタンスを生成
		GameObject obj = (GameObject)Instantiate(effectObj[EffectNo],new Vector3(0,2,0),Quaternion.identity);

		EffectNo += 1;

		if (EffectNo >= 2)
		{
			EffectNo = 0;
		}

		// 作成したオブジェクトを子として登録
		obj.transform.parent = transform;
	}
}
