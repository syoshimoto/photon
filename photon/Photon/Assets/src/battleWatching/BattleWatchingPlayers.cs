﻿using UnityEngine;
using System.Collections;

public class BattleWatchingPlayers : MonoBehaviour
{
    public GameObject Character;

    public Sprite Char01_1;
    public Sprite Char01_2;
    public Sprite Char01_3;
    public Sprite Char02_1;
    public Sprite Char02_2;
    public Sprite Char02_3;
    public Sprite Char03_1;
    public Sprite Char03_2;
    public Sprite Char03_3;

    public void Start()
    {
        ShowPlayers();
    }
    public void ShowPlayers()
    {
        int count = 0;
        foreach(var player in PlayerManager.GetInstance().Players)
        {
            if (!player.photonView.isMine)
            {
                AddPlayer(count, player);
                count++;
            }
        }
    }
    private void AddPlayer(int count, PlayerNetwork playerNetwork)
    {
       GameObject obj = Instantiate(Character, Vector3.zero, Quaternion.identity) as GameObject;
        obj.transform.SetParent(transform);
        obj.transform.localPosition = GetPosition(count);
        obj.transform.localScale = Vector3.one;
        BattleWatchingPlayer player = obj.GetComponent<BattleWatchingPlayer>();
        player.Initialize(playerNetwork);
        int characterId = playerNetwork.Data.CharaNo;
        int level = 1;
        switch(characterId)
        {
            case 1:
                if (level == 1)
                {
                    player.SetSprite(Char01_1);
                }
                else if (level == 2)
                {
                    player.SetSprite(Char01_2);
                }
                break;
            case 2:
                if (level == 1)
                {
                    player.SetSprite(Char02_1);
                }
                else if (level == 2)
                {
                    player.SetSprite(Char02_2);
                }
                break;
            case 3:
                if (level == 1)
                {
                    player.SetSprite(Char03_1);
                }
                else if (level == 2)
                {
                    player.SetSprite(Char03_2);
                }
                break;
        }
    }
    private Vector3 GetPosition(int count)
    {
        return new Vector3(0, - count * 60, 0);
    }
}
