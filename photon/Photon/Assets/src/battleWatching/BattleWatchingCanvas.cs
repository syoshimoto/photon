﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class BattleWatchingCanvas : MonoBehaviour {
    [SerializeField]
    private Animator BossDragonAnimater;
	[SerializeField]
	private Animator BossKinokoAnimater;

	[SerializeField]
    private EffectManager effectManager;
//    [SerializeField]
//    private HPGauge hpGauge;

	[SerializeField]
	private Image hpGauge;


	[SerializeField]
	private GameObject BossDragon;
	[SerializeField]
	private GameObject BossKinoko;


	public void Awake()
	{
		if (!PhotonNetwork.connected)
		{
            Scene.ChangeApplicationScene(Scene.Title);
			return;
		}
        HiddenCommand.GetInstance().Reset();
	}

	private void Start()
    {
		//出現する敵の判定
		switch (Game.QuestionId)
		{
			case 0:
				BossKinoko.SetActive(true);
				break;
			case 1:
				BossDragon.SetActive(true);
				break;
		}

		DamageManager.GetInstance().canvas = this;
    }
    public void Update()
    {
        if (HiddenCommand.GetInstance().Check())
        {
            OnButtonPressed();
        }
    }
    public void OnButtonPressed()
    {
        ChangeScene();
    }
    public void OnBattleButtonPressed()
    {
        Damage();
    }
    public void Damage(PlayerNetwork attacker = null)
    {
        DataQuestion question = DataQuestion.Get(Game.QuestionId);
        int totalScore = PlayerManager.GetInstance().GetAllScore();
        if (totalScore > question.bossHp)
        {
            ChangeScene();
        }
        else
        {
            float hp = question.bossHp - totalScore;
			hpGauge.fillAmount = hp / (float)question.bossHp;
			//hpGauge.SetPercent(hp / (float)question.bossHp);
		}

		if(BossDragon.activeInHierarchy)
		{
			BossDragonAnimater.SetTrigger("Damage");
		}
		else if(BossKinoko.activeInHierarchy)
		{
			BossKinokoAnimater.SetTrigger("Damage");
		}
		effectManager.Damage();
    }
    private void ChangeScene()
    {
        DataManager.GetInstance().ChangeToMonsterDeath();
    }
}
