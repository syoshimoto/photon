﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class MonsterDeath : MonoBehaviour {

	[SerializeField] private float time;
	[SerializeField] private GameObject objCloud;
	[SerializeField] private GameObject preDragon;
	[SerializeField] private GameObject preFungus;
	[SerializeField] private Button button;
    [SerializeField] private GameObject clouds;

	// 死亡しているボスモンスターの管理番号(1：ドラゴン、2：キノコ)
	// シングルトンなどで管理したい。取り急ぎ無理やりstatic化。
	// [SerializeField] private static int monsterNo = 1;

	void Start()
	{
		if(Game.QuestionId == 1) {
			GameObject temp = Instantiate(preDragon);
			temp.transform.SetParent(this.transform, false);
			// monsterNo += 1;
        }
		else if(Game.QuestionId == 0) {
			GameObject temp = Instantiate(preFungus);
			temp.transform.SetParent(this.transform, false);
		}
        SoundManager.Instance.PlaySE(SEType.Kill);
        HiddenCommand.GetInstance().Reset();
	}

	void Update()
	{
		//一定時間ごとに処理を行う
		time -= Time.deltaTime;
		if (time <= 0.0) {
			time = 6.0f;

			GameObject temp = Instantiate(objCloud, new Vector3(0, 220 + Random.Range(10, 50), 0), Quaternion.identity) as GameObject;
			temp.transform.SetParent(clouds.transform, false);
			Destroy(temp, 20f);
		}
        if (HiddenCommand.GetInstance().Check())
        {
            OnButtonPressed();
        }
	}
    public void OnButtonPressed()
    {
        if (DataManager.GetInstance().IsGameMode(GameMode.Child))
        {
            return;
        }
        DataManager.GetInstance().ChangeToDescription();
    }
}
