﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class CharacterCanvas : Photon.MonoBehaviour
{
	[SerializeField]
	Button button;

    public void Awake()
    {
        HiddenCommand.GetInstance().Reset();
        PhotonNetwork.automaticallySyncScene = false;
    }
    public void Update()
    {
        if (HiddenCommand.GetInstance().Check())
        {
            OnButtonPressed();
        }
    }

	public void OnButtonPressed()
	{
        DataManager.GetInstance().ChangeToMonsterexample();
	}
}
