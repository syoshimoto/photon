﻿using UnityEngine;
using System.Collections;

public class ShowAnswerManager : MonoBehaviour
{
	[SerializeField]
	private KinokoCreater creater;

	private int index;
	private DataQuestion dataQuestion;
	private void Start()
	{
		index = 0;
		creater.Create(null);
		dataQuestion = DataQuestion.Get(Game.QuestionId);
		Show();
	}
	public void Update()
	{
		if (Input.GetKey(KeyCode.UpArrow))
		{
			ShowNext();
		}
	}
	public void ShowNext()
	{
		index++;
		Show();
	}
	public void ShowPre()
	{
		index--;
		Show();
	}
	private void Show()
	{
		if (Game.allAnswerList == null || Game.allAnswerList.Count == 0)
			return;
		creater.Reset();
		if (index >= Game.allAnswerList.Count)
			index = 0;
		if (index < 0)
			index = Game.allAnswerList.Count - 1;
		foreach(var oneSet in Game.allAnswerList[index])
		{
			int preI = KinokoCreater.initialIJ;
			int preJ = KinokoCreater.initialIJ;
			int j = 0;
			int i = 0;
			int count = 0;
			int countMax = oneSet.Count;
			foreach(var num in oneSet)
			{
				count++;
				if (count == countMax)
					break;
				j = num % 10;
				i = (num - j) / 10;
				creater.ShowLine(preI, preJ, i, j);
				preI = i;
				preJ = j;
			}
		}
	}
}
