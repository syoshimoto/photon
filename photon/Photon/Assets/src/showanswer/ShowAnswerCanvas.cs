﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class ShowAnswerCanvas : MonoBehaviour
{
	[SerializeField]
	private Button button;
    [SerializeField]
    private Image image;
    [SerializeField]
    private Text text;
	void Start () {
        SoundManager.Instance.PlayBGM(SoundManager.Type.ED);
        HiddenCommand.GetInstance().Reset();
        image.gameObject.SetActive(true);
        image.sprite = Resources.Load<Sprite>("Sprite/" + Game.GetIconPath());
        if (PlayerManager.GetInstance().MyPlayerNetwork != null)
            text.text = PlayerManager.GetInstance().MyPlayerNetwork.GetName();
	}
    public void Update()
    {
        if (HiddenCommand.GetInstance().Check())
        {
            OnButtonPressed();
        }
    }
	public void OnButtonPressed()
	{
        DataManager.GetInstance().ChangeToEvolution();
	}
}
