﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;   
using UnityEngine.EventSystems;
using System.Collections;

public class evolution : MonoBehaviour {
	public Sprite Char01_1;
	public Sprite Char01_2;
	public Sprite Char01_3;
	public Sprite Char02_1;
	public Sprite Char02_2;
	public Sprite Char02_3;
	public Sprite Char03_1;
	public Sprite Char03_2;
	public Sprite Char03_3;

	public Sprite Char01_1_icon;
	public Sprite Char01_2_icon;
	public Sprite Char01_3_icon;
	public Sprite Char02_1_icon;
	public Sprite Char02_2_icon;
	public Sprite Char02_3_icon;
	public Sprite Char03_1_icon;
	public Sprite Char03_2_icon;
	public Sprite Char03_3_icon;

	[SerializeField] private Text text;

	public Image CharIcon;

	public Image MaterImage;
	
	public Image MyChara;
	public Image NextChara;

	public float EvolutionMater;
	[SerializeField]
	private Button button;

	private float UpPercent;
	private int ButtonPushCount = 0;

	private bool shouldEvolutionAnimation = false;
	private bool isMaterAnimation = false;

	bool isEnd = false;

	void Start () {
		float prePercent = PlayerManager.GetInstance().MyData.PreEvolutionPercent;//アニメーション開始時のバーの進捗具合
        float nextPercent = PlayerManager.GetInstance().MyData.NextEvolutionPercent;//終了後のバーの進捗具合
		UpPercent = nextPercent - prePercent;
        int preLevel = (int)prePercent + 1;
		int nextLevel = (int)nextPercent + 1;
		CharIcon.sprite = GetIconSprite(preLevel);
		MyChara.sprite = GetSprite(preLevel);
		NextChara.sprite = GetSprite(nextLevel);
		shouldEvolutionAnimation = preLevel != nextLevel;
		MaterImage.fillAmount = prePercent - (int)prePercent;
        EvolutionMater = nextPercent;
        HiddenCommand.GetInstance().Reset();
	}

	public void MaterAnimation()
	{
		MyChara.GetComponent<Button>().enabled = false;
		ButtonPushCount += 1;

		switch (ButtonPushCount)
		{
			case 1:
				isMaterAnimation = true;
				break;
			case 2:
				OnButtonPress();
				break;
        }
	}


	private Sprite GetSprite(int level)
	{
		switch(Game.CharacterId)
		{
		case 1:
			switch(level)
			{
			default:
				Debug.LogError("unexpected level " + level);
				return Char01_1;
			case 1:
				return Char01_1;
			case 2:
				return Char01_2;
			case 3:
				return Char01_3;
			}
		case 2:
			switch(level)
			{
			default:
				Debug.LogError("unexpected level " + level);
				return Char02_1;
			case 1:
				return Char02_1;
			case 2:
				return Char02_2;
			case 3:
				return Char02_3;
			}
		case 3:
			switch(level)
			{
			default:
				Debug.LogError("unexpected level " + level);
				return Char03_1;
			case 1:
				return Char03_1;
			case 2:
				return Char03_2;
			case 3:
				return Char03_3;
			}
		}
		Debug.LogError("Game.CharacterId " + Game.CharacterId);
		return Char02_1;
	}


	private Sprite GetIconSprite(int level)
	{
		switch (Game.CharacterId)
		{
			case 1:
				switch (level)
				{
					default:
						Debug.LogError("unexpected level " + level);
						return Char01_1_icon;
					case 1:
						return Char01_1_icon;
					case 2:
						return Char01_2_icon;
					case 3:
						return Char01_3_icon;
				}
			case 2:
				switch (level)
				{
					default:
						Debug.LogError("unexpected level " + level);
						return Char02_1_icon;
					case 1:
						return Char02_1_icon;
					case 2:
						return Char02_2_icon;
					case 3:
						return Char02_3_icon;
				}
			case 3:
				switch (level)
				{
					default:
						Debug.LogError("unexpected level " + level);
						return Char03_1_icon;
					case 1:
						return Char03_1_icon;
					case 2:
						return Char03_2_icon;
					case 3:
						return Char03_3_icon;
				}
		}
		Debug.LogError("Game.CharacterId " + Game.CharacterId);
		return Char02_1_icon;
	}



	public void Update()
    {
		if (isMaterAnimation == true)
		{
			MaterImage.fillAmount += Time.deltaTime * UpPercent / 2;
			float percent = PlayerManager.GetInstance().MyData.NextEvolutionPercent - (int)PlayerManager.GetInstance().MyData.NextEvolutionPercent;
			//shouldEvolutionAnimationをつけないと進化後に今のfillAmountよりも低いpercentになる場合にfillAmountが1を超えてくれない
			if ((!shouldEvolutionAnimation && MaterImage.fillAmount >= percent) || MaterImage.fillAmount >= 1.0f)
			{
				if(MaterImage.fillAmount >= 1.0f)
				{
					shouldEvolutionAnimation = false;
					Invoke("MonsterAnimation",2);
				}
				isMaterAnimation = false;
            }
		}

       if (HiddenCommand.GetInstance().Check())
       {
           OnButtonPress();
       }
    }

	private void MonsterAnimation()
	{
		text.text = "‥‥おや！？　モンスターのようすが‥！";
		GetComponent<Animator>().SetTrigger("evolution");
	}

	public void OnButtonPress()
	{
		if (DataManager.GetInstance().IsGameMode(GameMode.Child))
	    {
			return;
		}
		if (Game.QuestionId == 1)
		{
			DataManager.GetInstance().ChangeToEnding();
		}
		else
		{
            Game.ClearQuestion();
			DataManager.GetInstance().ChangeToMonsterexample();
		}
	}
	public void EvolutionCheck()
	{
		float nextPercent = PlayerManager.GetInstance().MyData.NextEvolutionPercent;//終了後のバーの進捗具合
		int nextLevel = (int)nextPercent + 1;
		CharIcon.sprite = GetIconSprite(nextLevel);
		text.text = "おめでとう！　モンスターがしんかした！";
		SoundManager.Instance.PlaySE(SEType.Evolution);

		if (isEnd)
		{
			return;
		}
		//MaterImage.fillAmount = EvolutionMater;
		Game.AddLevel();
		MyChara.sprite = NextChara.sprite;
		isEnd = true;
		MyChara.GetComponent<Button>().enabled = true;

		if (PlayerManager.GetInstance().MyData.NextEvolutionPercent >= 1)
		{
			MaterImage.fillAmount = (PlayerManager.GetInstance().MyData.NextEvolutionPercent -1);
        }
    }
}
