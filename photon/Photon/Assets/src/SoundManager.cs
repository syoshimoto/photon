﻿using UnityEngine;
using System.Collections;

public enum SEType
{
	Correct,
	Bad,
	Dragon,
	Choiced,
	Hit,
	Kill,
	Duplicate,
	Evolution,
	Kinoko
}
public class SoundManager : MonoBehaviour
{
	public enum Type
	{
		OP,
		Battle,
		ED,
		Evolution
	}
	private static SoundManager mInstance;
	private AudioSource audioSource;
	private AudioSource audioSourceSE;
	private AudioSource audioSourceSE2;
	private AudioSource audioSourceSE3;
	int i = 0;
    private SoundManager () { // Private Constructor

    	audioSource = gameObject.AddComponent<AudioSource>();
    	audioSourceSE = gameObject.AddComponent<AudioSource>();
    	audioSourceSE2 = gameObject.AddComponent<AudioSource>();
    	audioSourceSE3 = gameObject.AddComponent<AudioSource>();
    	DontDestroyOnLoad(this);
    }

    public static SoundManager Instance {

        get {

            if( mInstance == null ) {

                GameObject go = new GameObject("SoundManager");
                mInstance = go.AddComponent<SoundManager>();
            }

            return mInstance;
        }
    }
    public void PlayBGM(Type type)
    {
    	audioSource.clip = Resources.Load(GetName(type)) as AudioClip;
    	audioSource.loop = true;
    	audioSource.Play();
    }

    public void PlaySE(SEType type)
    {
    	AudioSource source;
    	if (i % 3 == 0)
    	{
    		source = audioSourceSE;
	    }
	    else if (i % 3 == 1)
	    {
    		source = audioSourceSE2;
	    }
	    else
	    {
    		source = audioSourceSE3;
	    }
	    i++;
    	source.clip = Resources.Load("SE/" + GetSEName(type)) as AudioClip;
    	source.Play();
    }
    private string GetName(Type type)
    {
    	switch(type)
    	{
    		case Type.OP:
    			return "BGM/op";
    		case Type.Battle:
    			return "BGM/battle";
    		case Type.ED:
    			return "BGM/ed";
    		case Type.Evolution:
    			return "BGM/evo";
    	}
    	return "";
    }
    private string GetSEName(SEType type)
    {
    	switch(type)
    	{
    		case SEType.Correct:
    			return "correct";
    		case SEType.Bad:
    			return "bad";
    		case SEType.Dragon:
    			return "dragon";
    		case SEType.Choiced:
    			return "choiced";
    		case SEType.Hit:
    			return "hit";
    		case SEType.Kill:
    			return "kill";
    		case SEType.Duplicate:
    			return "duplicate";
    		case SEType.Evolution:
    			return "evolution";
    		case SEType.Kinoko:
	    		return "kinoko";
    	}
    	return "";
    }
}
