﻿using UnityEngine;
using System.Collections;

public enum GameState
{
    Start,
    CharacterSelect,
    Monsterexample,
    Battle,
    MonsterDeath,
    Description,
    Evolution,
	Result,
    Ending,
	TrueEnding,
}

public enum GameMode
{
    Teacher,
    Child,
    OnePlay
}

public class DataManager
{
    public static DataManager GetInstance()
    {
        if (mInstance == null)
        {
            mInstance = new DataManager();
        }
        return mInstance;
    }
    private static DataManager mInstance;
    private DataManager()
    {
        mMode = GameMode.OnePlay;
    }

    private GameState mState;
    public GameState State { get { return mState;}}
    public bool ShouldShowNextButton{ get { return mMode != GameMode.Child;}}
    private GameMode mMode;
    public void SetGameMode(GameMode mode) { mMode = mode;}
    public bool IsGameMode(GameMode mode) { return mMode == mode;}

    public void Initialize()
    {
        mState = GameState.Start;
    }

    public bool UpdateConnect()
    {
        if (mMode == GameMode.OnePlay)
            return true;
        if (!PhotonNetwork.connected)
        {
            Scene.ChangeApplicationScene(Scene.Title);
            return false;
        }
        return true;
    }
    public void ChangeToWorldview()
    {
        Scene.ChangeScene(Scene.WorldviewScene);
    }
    public void ChangeToCharaSelect()
    {
        mState = GameState.CharacterSelect;
        Scene.ChangeScene(Scene.CharacterSelect);
    }
    public void ChangeToMonsterexample()
    {
        SoundManager.Instance.PlayBGM(SoundManager.Type.Battle);
        mState = GameState.Monsterexample;
        Scene.ChangeScene(Scene.Monsterexample);
    }
    public void ChangeToBattle()
    {
        mState = GameState.Battle;
        SoundManager.Instance.PlayBGM(SoundManager.Type.Battle);
        PlayerManager.GetInstance().MyData.Reset();
        switch(mMode)
        {
            case GameMode.Teacher:
                Scene.ChangeScene(Scene.BattleWatching);
                break;
            case GameMode.Child:
                Scene.ChangeScene(Scene.Battle);
                break;
            case GameMode.OnePlay:
                Scene.ChangeScene(Scene.Battle);
                break;
        }
    }
    public void ChangeToMonsterDeath()
    {
        mState = GameState.MonsterDeath;
        Scene.ChangeScene(Scene.MonsterDeath);
        // PhotonNetwork.LoadLevel(Scene.MonsterDeath);
    }
    public void ChangeToEnding()
    {
        mState = GameState.Ending;
        Scene.ChangeScene(Scene.Ending);
    }
    public void ChangeToHome()
    {
        SoundManager.Instance.PlayBGM(SoundManager.Type.OP);
        mState = GameState.Start;
        Game.Reset();

        switch(mMode)
        {
            case GameMode.Teacher:
            case GameMode.Child:
                Scene.ChangeScene(Scene.Title);
                break;
            case GameMode.OnePlay:
                Scene.ChangeApplicationScene(Scene.Title);
                break;
        }
    }
    public void ChangeToDescription()
    {
        mState = GameState.Description;
        switch(mMode)
        {
            case GameMode.Teacher:
                Scene.ChangeScene(Scene.Description);
                break;
            case GameMode.Child:
                Scene.ChangeScene(Scene.ShowAnswer);
                break;
            case GameMode.OnePlay:
                Scene.ChangeApplicationScene(Scene.ShowAnswer);
                break;
        }
    }
    public void ChangeToEvolution()
    {
        SoundManager.Instance.PlayBGM(SoundManager.Type.Evolution);
        mState = GameState.Evolution;
        PhotonNetwork.LoadLevel(Scene.Evolution);
    }

	public void ChangeToTrueEnding()
	{
		mState = GameState.TrueEnding;
		Scene.ChangeScene(Scene.TrueEnding);
	}
}
