﻿using UnityEngine;
using System.Collections;

public class MatchingCanvas : MonoBehaviour
{
	[SerializeField]
	GameObject waitingConnectView;
	[SerializeField]
	ConnectedView connectedView;

	MatchingSearch matchingSearch;
	bool isConnected = false;
	private void Start()
	{
		matchingSearch = GetComponent<MatchingSearch>();
	}
	//from photon
	public void UpdateConnect(bool isConnect)
	{
		if (isConnected)
			return;
		if (isConnect)
		{
			isConnected = true;
			// ShowWaing(isWaiting:false);
			StartCoroutine("StartImidiately");
		}
	}
	public void UpdateRoomLength(int count)
	{
		// connectedView.SetActiveJoinButton(count != 0);
	}

	//from UI
	public void CreateRoom()
	{
		ShowWaing(isWaiting:true);
		matchingSearch.CreateRoom();
	}
	public void JoinRoom()
	{
		matchingSearch.JoinRoom();
	}

	//private
	private void ShowWaing(bool isWaiting)
	{
		waitingConnectView.SetActive(isWaiting);
		connectedView.gameObject.SetActive(!isWaiting);
		connectedView.SetActiveJoinButton(!isWaiting);
	}
	//debug
	private IEnumerator StartImidiately()
	{
		yield return new WaitForSeconds(1.2f);
		if (DataManager.GetInstance().IsGameMode(GameMode.Teacher))
		{
			CreateRoom();
		}
		else
		{
			JoinRoom();
		}
	}
}
