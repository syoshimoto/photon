﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class PlayerNetwork : Photon.MonoBehaviour
{
    private PlayerData data;
    public PlayerData Data { get { return data;}}
    public string GetName()
    {
        int id = (int)(photonView.viewID / 1000);
        return id.ToString();
    }

    public void Awake()
    {
         if (photonView.isMine)
        {
            PlayerManager.GetInstance().AddMyPlayer(this);
            DataManager.GetInstance().Initialize();
        }
        else
        {           
            PlayerManager.GetInstance().AddPlayer(this);
        }
        gameObject.name = gameObject.name + photonView.viewID;
        DontDestroyOnLoad(this);
        data = new PlayerData();
    }

    public void OnPhotonSerializeView(PhotonStream stream, PhotonMessageInfo info)
    {
        switch(DataManager.GetInstance().State)
        {
        case GameState.Start:
            UpdateStart(stream, info);
            break;
        case GameState.CharacterSelect:
            UpdateCharaSelect(stream, info);
            break;
        case GameState.Monsterexample:
            UpdateMonsterexample(stream, info);
            break;
        case GameState.Battle:
            UpdateBattle(stream, info);
            break;
        case GameState.MonsterDeath:
            UpdateMonsterDeath(stream, info);
            break;
        case GameState.Description:
            UpdateDescription(stream, info);
            break;
        case GameState.Evolution:
            UpdateEvolution(stream, info);
            break;
        case GameState.Ending:
            UpdateEnding(stream, info);
            break;
        }
    }
    private void UpdateStart(PhotonStream stream, PhotonMessageInfo info)
    {
        if (stream.isWriting)
        {
            stream.SendNext(DataManager.GetInstance().State);
        }
        else
        {
            GameState state = (GameState)stream.ReceiveNext();
            if (state == GameState.CharacterSelect)
            {
                DataManager.GetInstance().ChangeToCharaSelect();
            }
        }
    }
    private void UpdateCharaSelect(PhotonStream stream, PhotonMessageInfo info)
    {
        if (stream.isWriting)
        {
            SendCommon(stream, info);
        }
        else
        {
            GameState state = (GameState)stream.ReceiveNext();
            if (stream.Count > 1)
            {
                data.SetCharaNo((int)stream.ReceiveNext());
            }
            if (stream.Count > 2)
            {
                data.SetCharaLevel((int)stream.ReceiveNext());
            }
            if (state == GameState.Monsterexample)
            {
                DataManager.GetInstance().ChangeToMonsterexample();
            }
        }
    }
    private void UpdateMonsterexample(PhotonStream stream, PhotonMessageInfo info)
    {
        if (stream.isWriting)
        {
            SendCommon(stream, info);
        }
        else
        {
            GameState state = (GameState)stream.ReceiveNext();
            if (stream.Count > 1)
            {
                data.SetCharaNo((int)stream.ReceiveNext());
            }
            if (stream.Count > 2)
            {
                data.SetCharaLevel((int)stream.ReceiveNext());
            }
            if (state == GameState.Battle)
            {
                DataManager.GetInstance().ChangeToBattle();
            }
        }
    }
    private void UpdateBattle(PhotonStream stream, PhotonMessageInfo info)
    {
        if (stream.isWriting)
        {
            SendCommon(stream, info);
            foreach(var answer in data.Answers)
            {
                stream.SendNext(answer);
            }
        }
        else
        {
            GameState state = (GameState)stream.ReceiveNext();
            if (stream.Count > 1)
            {
                data.SetCharaNo((int)stream.ReceiveNext());
            }
            if (stream.Count > 2)
            {
                data.SetCharaLevel((int)stream.ReceiveNext());
            }
            if (state == GameState.MonsterDeath)
            {
                DataManager.GetInstance().ChangeToMonsterDeath();
                return;
            }
            int i = 0;
            bool isChanged = false;
            if (stream.Count > 3)
            {
                foreach(var answer in data.Answers)
                {
                    int new_answer = (int)stream.ReceiveNext();
                    if (data.Answers[i] != new_answer)
                    {
                        data.Answers[i] = new_answer;
                        isChanged = true;
                    }
                    i++;
                }
            }
            if (isChanged)
            {
                data.UpdateScore();
                DamageManager.GetInstance().Damage(this);
            }
        }
    }
    private void SendCommon(PhotonStream stream, PhotonMessageInfo info)
    {
        stream.SendNext(DataManager.GetInstance().State);
        stream.SendNext(Game.CharacterId);
        stream.SendNext(Game.CharacterLevel);
    }
    private void UpdateDescription(PhotonStream stream, PhotonMessageInfo info)
    {
        if (stream.isWriting)
        {
            stream.SendNext(DataManager.GetInstance().State); 
        }
        else
        {
            GameState state = (GameState)stream.ReceiveNext();
            if (state == GameState.Evolution)
            {
                DataManager.GetInstance().ChangeToEvolution();
            }
        }
    }
    private void UpdateMonsterDeath(PhotonStream stream, PhotonMessageInfo info)
    {
        if (stream.isWriting)
        {
            stream.SendNext(DataManager.GetInstance().State); 
        }
        else
        {
            GameState state = (GameState)stream.ReceiveNext();
            if (state == GameState.Description)
            {
                DataManager.GetInstance().ChangeToDescription();
            }
        }
    }
    private void UpdateEvolution(PhotonStream stream, PhotonMessageInfo info)
    {
        if (stream.isWriting)
        {
            stream.SendNext(DataManager.GetInstance().State); 
        }
        else
        {
            GameState state = (GameState)stream.ReceiveNext();
            if (state == GameState.Monsterexample)
            {
                Game.ClearQuestion();
                DataManager.GetInstance().ChangeToMonsterexample();
            }
            else if (state == GameState.Ending)
            {
                DataManager.GetInstance().ChangeToEnding();
            }
        }
    }
    private void UpdateEnding(PhotonStream stream, PhotonMessageInfo info)
    {
        if (stream.isWriting)
        {
            stream.SendNext(DataManager.GetInstance().State); 
        }
        else
        {
        }
    }
}