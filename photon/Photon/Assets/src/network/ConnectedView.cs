﻿using UnityEngine;
using System.Collections;

public class ConnectedView : MonoBehaviour
{
	[SerializeField]
	GameObject joinButton;

	public void SetActiveJoinButton(bool isActive)
	{
		joinButton.SetActive(isActive);
	}
}
