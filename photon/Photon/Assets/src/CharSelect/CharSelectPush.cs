﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;   
using UnityEngine.EventSystems;

public class CharSelectPush : MonoBehaviour {
	public GameObject char01;
	public GameObject char02;
	public GameObject char03;
	public GameObject char01text;
	public GameObject char02text;
	public GameObject char03text;
	
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
	
	public void CharcterSelect01()
	{
		char01.GetComponent<Animator> ().SetBool ("char1", true);
		char02.GetComponent<Animator> ().SetBool ("char2", false);
		char03.GetComponent<Animator> ().SetBool ("char3", false);
		char01text.SetActive (true);
		char02text.SetActive (false);
		char03text.SetActive (false);
	}

	public void CharcterSelect02()
	{
		char01.GetComponent<Animator> ().SetBool ("char1", false);
		char02.GetComponent<Animator> ().SetBool ("char2", true);
		char03.GetComponent<Animator> ().SetBool ("char3", false);
		char01text.SetActive (false);
		char02text.SetActive (true);
		char03text.SetActive (false);

	}

	public void CharcterSelect03()
	{
		char01.GetComponent<Animator> ().SetBool ("char1", false);
		char02.GetComponent<Animator> ().SetBool ("char2", false);
		char03.GetComponent<Animator> ().SetBool ("char3", true);
		char01text.SetActive (false);
		char02text.SetActive (false);
		char03text.SetActive (true);

	}

}
