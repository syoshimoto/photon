﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;   
using UnityEngine.EventSystems;

public class CharSelect : MonoBehaviour {

    /// <summary>
    /// 1: きつね
    /// 2: クリオネ
    /// 3: ウニ
    /// </summary>
    public int CharNo  = 1;

    public GameObject char01;
    public GameObject char02;
    public GameObject char03;
    public GameObject char01text;
    public GameObject char02text;
    public GameObject char03text;
    public Text text;

    // Use this for initialization
    void Start () {
        char01.GetComponent<Animator> ().SetBool ("char1", true);
        char02.GetComponent<Animator> ().SetBool ("char2", false);
        char03.GetComponent<Animator> ().SetBool ("char3", false);
        char01text.SetActive (true);
        char02text.SetActive (false);
        char03text.SetActive (false);

        Select(1);
    }
    void Update()
    {
        if (DataManager.GetInstance().ShouldShowNextButton)
        {
            text.text = (PlayerManager.GetInstance().Players.Count - 1).ToString();
        }
    }

    public void CharcterSelect01()
    {
        Select(1);
        SoundManager.Instance.PlaySE(SEType.Kinoko);
    }

    public void CharcterSelect02()
    {
        Select(2);
        SoundManager.Instance.PlaySE(SEType.Kinoko);
    }

    public void CharcterSelect03()
    {
        Select(3);
        SoundManager.Instance.PlaySE(SEType.Kinoko);
    }

    private void Select(int number)
    {
        CharNo = number;
        Game.SetCharacterId(CharNo);
        PlayerManager.GetInstance().MyData.SetCharaNo(CharNo);
    }
}
