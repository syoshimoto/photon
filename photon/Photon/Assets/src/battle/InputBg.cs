﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class InputBg : MonoBehaviour {
	public enum Type {
		Right,
		Up,
		UpRight,
		DownRight,
		Count
	}
    public const int SIZE_LONG = 42;
    public const int SIZE_SHORT = 12;//画像自体のサイズと円の部分との隙間


    private RectTransform rectTransform;
    private Image image;
    private int i;
    private int j;
    private float scale;

    public void Initialize(int i, int j, KinokoCreater creater, float scale, Type type)
    {
        image = GetComponent<Image>();
        Reset();
        this.i = i;
        this.j = j;
        name = "button_" + i.ToString() + "_" + j.ToString();
        this.scale = scale;
        if (creater != null)
        {
            creater.onResetEvent += Reset;
        }
        rectTransform = GetComponent (typeof (RectTransform)) as RectTransform;
	    rectTransform.sizeDelta = new Vector2(SIZE_LONG * 2, SIZE_SHORT * 2) * scale;
	    switch(type)
	    {
	    	case Type.Up:
	    		transform.Rotate(new Vector3(0, 0, 1), 90);
		    	break;
	    	case Type.UpRight:
	    		transform.Rotate(new Vector3(0, 0, 1), 45);
		    	break;
	    	case Type.DownRight:
	    		transform.Rotate(new Vector3(0, 0, 1), -45);
		    	break;
	    }
    }
    public void Reset()
    {
        // image.color = Color.white;
        UpdateColor();
        gameObject.SetActive(false);
    }
    private void UpdateColor()
    {
        // image.color = new Color(image.color.r, image.color.g, image.color.b, 1.0f);
    }
}
