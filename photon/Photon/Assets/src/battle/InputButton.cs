﻿using UnityEngine;
using System.Collections;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class InputButton : Button, ICanvasRaycastFilter, IPointerEnterHandler, IDragHandler{
    public const int SIZE_RADIUS = 42;
    public const int SIZE_GAP = 12;//画像自体のサイズと円の部分との隙間


    private RectTransform rectTransform;
    private Image image;
    private bool isTapped;
    private int i;
    private int j;
    private InputManager inputManager;
    private float scale;

    public void Start()
    {
        image = GetComponent<Image>();
        Reset();
    }
    public void Initialize(int i, int j, InputManager manager, float scale, KinokoCreater creater)
    {
        this.i = i;
        this.j = j;
        this.inputManager = manager;
        if (creater != null)
        {
            creater.onResetEvent += Reset;
        }
        name = "button_" + i.ToString() + "_" + j.ToString();
        this.scale = scale;
        rectTransform = GetComponent (typeof (RectTransform)) as RectTransform;

		if (Game.QuestionId == Game.START_QUESTION) {
			rectTransform.sizeDelta = new Vector2(SIZE_RADIUS * 1.45f, SIZE_RADIUS * 1.45f) * scale;
		}
		else {
			rectTransform.sizeDelta = new Vector2(SIZE_RADIUS * 1.25f, SIZE_RADIUS * 1.25f) * scale;
		}



    }
    GameObject objUp;
    GameObject objRight;
    GameObject objUpRight;
    GameObject objDownRight;
    public void SetBgs(GameObject objUp, GameObject objRight, GameObject objUpRight, GameObject objDownRight)
    {
        this.objUp = objUp;
        this.objRight = objRight;
        this.objUpRight = objUpRight;
        this.objDownRight = objDownRight;
    }
    public void ShowLine(InputBg.Type bgType)
    {
        switch(bgType)
        {
            case InputBg.Type.Up:
                if (objUp == null)
                {
                    Debug.LogError("line is null");
                    return;
                }
                objUp.SetActive(true);
                break;
            case InputBg.Type.Right:
                if (objRight == null)
                {
                    Debug.LogError("line is null");
                    return;
                }
                objRight.SetActive(true);
                break;
            case InputBg.Type.UpRight:
                if (objUpRight == null)
                {
                    Debug.LogError("line is null");
                    return;
                }
                objUpRight.SetActive(true);
                break;
            case InputBg.Type.DownRight:
                if (objDownRight == null)
                {
                    Debug.LogError("line is null");
                    return;
                }
                objDownRight.SetActive(true);
                break;
        }
    }
    public void Reset()
    {
        if (image != null)
            image.color = Color.white;
        isTapped = false;
        UpdateColor();
    }
    public bool IsRaycastLocationValid (Vector2 sp, Camera eventCamera)
    {
        return Vector2.Distance(sp, transform.position) < SIZE_RADIUS * scale;
    }
    public void OnDrag(PointerEventData eventData)
    {
        if (inputManager == null)
            return;
        OnMove();
    }
    public void OnPointerEnter(PointerEventData ped)
    {
        if (inputManager == null)
            return;
        OnMove();
    }
    private void OnMove()
    {
        if (inputManager.IsStartInput() && !isTapped)
        {
            image.color = Color.gray;
            isTapped = true;
            inputManager.OnChoiced(i, j);
            UpdateColor();
        }
    }
    private void UpdateColor()
    {
        if (image != null)
        {
            image.color = new Color(image.color.r, image.color.g, image.color.b, 1.0f);
        }
    }
}