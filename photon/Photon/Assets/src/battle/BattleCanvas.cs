﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class BattleCanvas : MonoBehaviour
{
	[SerializeField]
	private BattleAnswers battleAnswers;
	[SerializeField]
	private AnswerAnimation correct;
    [SerializeField]
    private AnswerAnimation bad;
    [SerializeField]
    private UIBattleScore score;
    [SerializeField]
    private Button debugButton;
    [SerializeField]
    private Button nextButton;

    [SerializeField]
    private Text text;

	[SerializeField]
	private GameObject BossDragon;
	[SerializeField]
	private GameObject BossKinoko;

    private bool isFailed = false;

	public void Awake()
    {
        DataManager.GetInstance().UpdateConnect();
    }
    private void Start()
    {
		//出現する敵の判定
		switch (Game.QuestionId)
		{
			case 0:
				BossKinoko.SetActive(true);
				break;
			case 1:
				BossDragon.SetActive(true);
				break;
		}



        if (DataManager.GetInstance().IsGameMode(GameMode.OnePlay))
        {
            nextButton.gameObject.SetActive(true);
        }
    }
    public void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space))
        {
            OnNextButtonTapped();
        }
    }
    public void OnNextButtonTapped()
    {
        DataManager.GetInstance().ChangeToMonsterDeath();
    }
    public void Cancel()
    {	
    }
    public void OnChoiceBegin()
    {
        if (isFailed)
        {
            text.text = "つぎはどうかな？？";
            isFailed = false;
			EnemyJump();
        }
    }
    public void ShowSuccess(int multi1, int multi2, DataQuestion dataQuestion)
    {
        SoundManager.Instance.PlaySE(SEType.Correct);
        correct.StartAnimation();
    	int index = battleAnswers.Add(multi1);
        switch(Random.Range(0, 3))
        {
        default:
        case 0:
            text.text = "やるなぁ！！";
            break;
        case 1:
            text.text = "それをみつけたか！";
            break;
        }
        score.Set(PlayerManager.GetInstance().MyData.AddAnswer(index, 1));
		EnemyJump();
    }
    public void ShowFailed()
    {
        SoundManager.Instance.PlaySE(SEType.Bad);
        text.text = "99になってないぞ！！";
    	bad.StartAnimation();
        isFailed = true;
		EnemyJump();
    }
    public void ShowDuplicate()
    {
        SoundManager.Instance.PlaySE(SEType.Bad);
        text.text = "それはもう答えたぞ！！";
        isFailed = true;
		EnemyJump();
    }
    public void ShowOver()
    {
        SoundManager.Instance.PlaySE(SEType.Bad);
        text.text = "となりをえらぶんだ！！";
        isFailed = true;
		EnemyJump();
    }
    public void OnPressedDebugButton()
    {
        OnNextButtonTapped();
    }

	private void EnemyJump()
	{
		//出現する敵の判定
		switch (Game.QuestionId)
		{
		case 0:
			BossKinoko.GetComponent<Animator> ().SetTrigger ("Jump");
			break;
		case 1:
			BossDragon.GetComponent<Animator> ().SetTrigger ("Jump");
			break;
		}
	}

}
