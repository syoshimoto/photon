﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class BattleAnswers : MonoBehaviour {
	private List<BattleAnswer> answerList;
	public void Start()
	{
		var prefab = Resources.Load("prefabs/battle/answer");
		int i = 0;
		var dataQuestion = DataQuestion.Get(Game.QuestionId);
		//var dataQuestion = DataQuestion.Get(0);
		answerList = new List<BattleAnswer>();
		foreach(var multi1 in dataQuestion.answer1)
		{

            GameObject obj = Instantiate(prefab, Vector3.zero, Quaternion.identity) as GameObject;
            obj.transform.SetParent(transform);
            obj.transform.localPosition = new Vector3(0, - 63 * i, 0);
            obj.transform.localScale = Vector3.one;
            BattleAnswer answer = obj.GetComponent<BattleAnswer>();
            answer.Initialize(multi1, dataQuestion.num / multi1);
            answerList.Add(answer);
            i++;
		}
	}
	public int Add(int multi1)
	{
		int count = 0;
		foreach(var answer in answerList)
		{
			if (answer.num1 == multi1)
			{
				answer.Add();
				return count;
			}
			count++;
		}
		Debug.LogError("multi is no " + multi1);
		return 0;
	}
}
