﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class UIBattleScore : MonoBehaviour {
	[SerializeField]
	private Text score;
	void Start ()
	{
		score.text = "";
	}

	public void Set(int num)
	{
		score.text = num.ToString();
	}	
}
