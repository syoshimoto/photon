﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class BattleAnswer : MonoBehaviour {
	[SerializeField]
	private Text multi1;
	[SerializeField]
	private Text multi2;
	[SerializeField]
	private Text num;
	[SerializeField]
	private Image multiImage1;
	[SerializeField]
	private Image multiImage2;
	public int num1;
	public int num2;
	private int count;
	public void Initialize(int num1, int num2)
	{
		multi1.text = num1.ToString();
		multi2.text = num2.ToString();
		num.text = "0";
		this.num1 = num1;
		this.num2 = num2;
		count = 0;
	}
	public void Add()
	{
		count++;
		num.text = count.ToString();
		multiImage1.gameObject.SetActive(false);
		multiImage2.gameObject.SetActive(false);
	}
}
