﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine.UI;

public class InputManager : MonoBehaviour {
    [SerializeField]
    private BattleCanvas canvas;
    [SerializeField]
    private KinokoCreater kinoko;

    GameObject prefab;
    private bool isStartInput;
    public bool IsStartInput() { return isStartInput;}
    private int setCount;
    private bool isFailed;
    private List<int> numberList;
    private List<List<int>> answerList;
    private int touchCount;
    private int touchCountSum;
    private DataQuestion dataQuestion;
	void Start () {
        if (Game.QuestionId == Game.START_QUESTION)
        {
            kinoko.gameObject.transform.localPosition = new Vector3(38, 63, 0);
        }
		else {
			kinoko.gameObject.transform.localPosition = new Vector3(32, 48, 0);
		}
        kinoko.Create(this);
       dataQuestion = DataQuestion.Get(Game.QuestionId);
       isStartInput = false;
       numberList = new List<int>();
       answerList = new List<List<int>>();
       Game.allAnswerList = new List<List<List<int>>>();
       ResetCommon();
	}
    public void ResetAndDelete()
    {
        isStartInput = false;
        kinoko.Reset();
        numberList.Clear();
        ResetCommon();
        answerList.Clear();
    }
    private void ResetCommon()
    {
       touchCountSum = 0;
       setCount = -1;
       touchCount = 0;
       isFailed = false;
       ResetIJ();
   }
    private int preI;
    private int preJ;
    public void OnChoiced(int i, int j)
    {
        if (IsOver(i, preI) || IsOver(j, preJ))
        {
            canvas.ShowOver();
            ResetAndDelete();
            return;
        }
        touchCount++;
        numberList.Add(i * 10 + j);
        kinoko.ShowLine(preI, preJ, i, j);
        if (preI == KinokoCreater.initialIJ && preJ == KinokoCreater.initialIJ)
        {
            canvas.OnChoiceBegin();
        }
        preI = i;
        preJ = j;
    }
    private bool IsOver(int i1, int i2)
    {
        if (i2 == KinokoCreater.initialIJ)
            return false;
        return i1 - i2 > 1 || i1 - i2 < -1;
    }
	void Update ()
    {
        UpdateMouse();
        UpdateTouch();
	}
    private void UpdateMouse()
    {
        if (Input.GetMouseButtonDown(0))
        {
            isStartInput = true;
        }
        if (isStartInput && Input.GetMouseButton(0))
        {
            // trail.transform.position = new Vector3(Input.mousePosition.x, Input.mousePosition.y, 0);
        }
        if (Input.GetMouseButtonUp(0))
        {
            OnEnd();
        }
    }
    private void UpdateTouch()
    {
        foreach (Touch touch in Input.touches) {
            switch(touch.phase)
            {
                case TouchPhase.Began:
                    isStartInput = true;
                    break;
                case TouchPhase.Ended:
                    OnEnd();
                    break;
                case TouchPhase.Moved:
                    // trail.transform.position = new Vector3(touch.position.x, touch.position.y, 0);
                    break;
            }
        }
    }
    private void OnEnd()
    {
        if (touchCount == 0)
            return;
        touchCountSum += touchCount;
        if (setCount == -1 && touchCount > 0)
        {
            setCount = touchCount;
        }
        else if (setCount != touchCount)// && (touchCount != 1 && setCount != 1))
        {//何かのバグで右を付け足した気がする。でも右を足すと2、1、1、2のときにバグになる
            isFailed = true;
        }
        bool shouldAddAnswerList = true;
        if (touchCountSum == dataQuestion.num)
        {
            if (isFailed)
            {
                canvas.ShowFailed();
            }
            else
            {
                AddAnswerList();
                shouldAddAnswerList = false;
                if (IsDuplicate())
                {
                    canvas.ShowDuplicate();
                }
                else
                {
                    AddNewAllAnswerList();
                    canvas.ShowSuccess(touchCount, dataQuestion.num / touchCount, dataQuestion);
                }
            }
            ResetAndDelete();
        }
        if (!isFailed && shouldAddAnswerList)
        {
            AddAnswerList();
        }
        touchCount = 0;
        isStartInput = false;
        ResetIJ();
    }
    private void AddAnswerList()
    {
        int count = 0;
        List<int> newList = new List<int>();
        foreach(var num in numberList)
        {
            newList.Add(num);
            count += num;
        }
        newList.Add(count);
        answerList.Add(newList);
        numberList.Clear();
    }
    private void AddNewAllAnswerList()
    {
        List<List<int>> list1 = new List<List<int>>();
        foreach(var setList in answerList)
        {
            List<int> list2 = new List<int>();
            foreach(var num in setList)
            {
                list2.Add(num);
            }
            list1.Add(list2);
        }
        Game.allAnswerList.Add(list1);
    }
    private void ResetIJ()
    {
        preI = KinokoCreater.initialIJ;
        preJ = KinokoCreater.initialIJ;
    }
    private bool IsDuplicate()
    {
        foreach(List<List<int>> oneAnswerList in Game.allAnswerList)
        {
            if (IsDuplicate(oneAnswerList))
            {
                return true;
            }
        }
        return false;
    }
    private bool IsDuplicate(List<List<int>> oneAnswerList)
    {
        if (oneAnswerList.Count != answerList.Count)
            return false;
        foreach(List<int> oneSet in oneAnswerList)
        {
            if (oneSet.Count == 1)
                return true;
            int sum = oneSet[oneSet.Count - 1];
            bool isSame = false;
            foreach(List<int> answerSet in answerList)
            {
                if (sum == answerSet[answerSet.Count - 1])
                {
                    isSame = true;
                    break;
                }
            }
            if (!isSame)
            {
                return false;
            }
        }
        return true;
    }
}
