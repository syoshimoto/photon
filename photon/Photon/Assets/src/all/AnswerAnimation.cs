﻿using UnityEngine;
using System.Collections;

public class AnswerAnimation : MonoBehaviour {
	private float time;
	public void StartAnimation()
	{
		gameObject.SetActive(true);
		time = 0.6f;
	}
	private void Update()
	{
		if (time <= 0)
			return;
		time -= Time.deltaTime;
		if (time <= 0)
		{
			time = 0;
			gameObject.SetActive(false);
		}
	}
}
