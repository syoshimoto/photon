﻿using UnityEngine;
using System.Collections;

public class DamageManager
{
	public BattleWatchingCanvas canvas;
	public static DamageManager GetInstance()
	{
		if (mInstance == null)
		{
			mInstance = new DamageManager();
		}
		return mInstance;
	}
	private static DamageManager mInstance;
	private DamageManager() {}

	public void Damage(PlayerNetwork playerNetwork)
	{
		if (canvas != null)
		{
			canvas.Damage(playerNetwork);
		}
	}
}
