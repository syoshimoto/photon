using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class PlayerManager
{
	public PlayerNetwork MyPlayerNetwork { get; private set;}
	private PlayerData data;
	public PlayerData MyData
	{
		get
		{
			if (MyPlayerNetwork != null)
			{
				return MyPlayerNetwork.Data;
			}
			return data;
		}
	}
	public List<PlayerNetwork> Players { get; private set;}
	public void AddPlayer(PlayerNetwork player)
	{
		Players.Add(player);
	}
	public void AddMyPlayer(PlayerNetwork player)
	{
		MyPlayerNetwork = player;
		Players.Add(player);
	}
	public int GetAllScore()
	{
		int score = 0;
		foreach(var player in Players)
		{
			score += player.Data.NowQuestionScore;
		}
		return score;
	}

	public static PlayerManager GetInstance()
	{
		if (mInstance == null)
		{
			mInstance = new PlayerManager();
		}
		return mInstance;
	}
	private PlayerManager()
	{
		Players = new List<PlayerNetwork>();
        data = new PlayerData();
	}
	private static PlayerManager mInstance;
}
