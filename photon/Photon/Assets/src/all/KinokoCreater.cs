﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;
using UnityEngine.EventSystems;
using System.Collections;

public delegate void InputEventHandler();

public class KinokoCreater : MonoBehaviour
{
    public event InputEventHandler onResetEvent;
    public const int initialIJ = -2;
	[SerializeField] private Sprite imgFire;
    [SerializeField] private Sprite imgKinoko;
    [SerializeField] private GameObject bgObject;
    [SerializeField] private Canvas canvas;
    GameObject prefab;
    private DataQuestion dataQuestion;
    public InputButton[,] inputButtons;
    private InputManager inputManager;
	public void Create(InputManager manager) {
    prefab = (GameObject)Resources.Load("Prefabs/Input/InputButton");

	// ドラゴンの時は火の玉に変更
	if(Game.QuestionId == 1)
	{
		prefab.GetComponent<Image>().sprite = imgFire;
	}
    else
    {
        prefab.GetComponent<Image>().sprite = imgKinoko;
    }

		var prefabBg = (GameObject)Resources.Load("Prefabs/Input/InputBG");
       dataQuestion = DataQuestion.Get(Game.QuestionId);
       inputButtons = new InputButton[dataQuestion.multiple2, dataQuestion.multiple1];
       this.inputManager = manager;
       for (int i = 0; i < dataQuestion.multiple2; i++)
       {
            for (int j = 0; j < dataQuestion.multiple1; j++)
            {
                GameObject objUp = null;
                GameObject objRight = null;
                GameObject objUpRight = null;
                GameObject objDownRight = null;
                if (j != dataQuestion.multiple1 -1)
                {
                    objUp = CreateBg(i, j, InputBg.Type.Up, prefabBg);
                }
                if (i != dataQuestion.multiple2 - 1)
                {
                    objRight = CreateBg(i, j, InputBg.Type.Right, prefabBg);
                }
                if (j != dataQuestion.multiple1 -1 && i != dataQuestion.multiple2 - 1)
                {
                    objUpRight = CreateBg(i, j, InputBg.Type.UpRight, prefabBg);
                    objDownRight = CreateBg(i, j, InputBg.Type.DownRight, prefabBg);
                }

                GameObject obj = Instantiate(prefab, Vector3.zero, Quaternion.identity) as GameObject;
                obj.transform.SetParent(transform);
                obj.transform.localPosition = GetButtonPosition(i, j);
                obj.GetComponent<InputButton>().Initialize(i, j, manager, canvas.scaleFactor, this);
                obj.GetComponent<InputButton>().SetBgs(objUp, objRight, objUpRight, objDownRight);
                inputButtons[i, j] = obj.GetComponent<InputButton>();
            }
       }
	}
    private GameObject CreateBg(int i, int j, InputBg.Type type, GameObject prefabBg)
    {
        GameObject objBg = Instantiate(prefabBg, Vector3.zero, Quaternion.identity) as GameObject;
        objBg.transform.SetParent(bgObject.transform);
        objBg.transform.localPosition = GetButtonBgPosition(i, j, type);
        objBg.GetComponent<InputBg>().Initialize(i, j, this, canvas.GetComponent<Canvas>().scaleFactor, type);
        objBg.SetActive(false);
        return objBg;
    }
    public void Reset()
    {
        onResetEvent();
    }
    public void ShowLine(int preI, int preJ, int i, int j)
    {
        if (preJ == initialIJ || preI == initialIJ)
            return;
        bool isUp = j > preJ;
        bool isDown = j < preJ;
        bool isRight = i > preI;
        bool isLeft = i < preI;
        InputBg.Type type;
        int baseI = preI;
        int baseJ = preJ;
        if (isUp)
        {
            if (isRight)
            {
                ShowLine(preI, preJ, InputBg.Type.UpRight);
            }
            else if (isLeft)
            {
                ShowLine(i, preJ, InputBg.Type.DownRight);
            }
            else
            {
                ShowLine(preI, preJ, InputBg.Type.Up);
            }
        }
        else if (isDown)
        {
            if (isRight)
            {
                ShowLine(preI, j, InputBg.Type.DownRight);
            }
            else if (isLeft)
            {
                ShowLine(i, j, InputBg.Type.UpRight);
            }
            else
            {
                ShowLine(i, j, InputBg.Type.Up);
            }
        }
        else
        {
            if (isRight)
            {
                ShowLine(preI, preJ, InputBg.Type.Right);
            }
            else
            {
                ShowLine(i, j, InputBg.Type.Right);
            }
        }
    }
    public void ShowLine(int i, int j, InputBg.Type type)
    {
        if (inputButtons[i, j] != null)
        {
            inputButtons[i, j].ShowLine(type);
        }
    }
    private Vector3 GetButtonBgPosition(int i, int j, InputBg.Type type)
    {
        int size = 100;
        switch(type)
        {
            case InputBg.Type.Right:
                return GetButtonPosition(i, j) + new Vector3(size / 2f, 0.0f, 0.0f);
            case InputBg.Type.Up:
                return GetButtonPosition(i, j) + new Vector3(0.0f, size / 2f, 0.0f);
            case InputBg.Type.UpRight:
            case InputBg.Type.DownRight:
                return GetButtonPosition(i, j) + new Vector3(size / 2f, size / 2f, 0.0f);

        }
        return Vector3.zero;
    }
    private Vector3 GetButtonPosition(int i, int j)
    {
        int size;

		if (Game.QuestionId == 0) {
			size = 110;
		}
		else {
			size = 90;
		}

		return new Vector3(i * size, j * size, 0);
        // float scale = canvas.scaleFactor;
        // return (basePosition + new Vector3(i * (InputButton.SIZE_RADIUS * 2- InputButton.SIZE_GAP),
            // j * (InputButton.SIZE_RADIUS * 2 - InputButton.SIZE_GAP), 0)) * scale;
    }
}
