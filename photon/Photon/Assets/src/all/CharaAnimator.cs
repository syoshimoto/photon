﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class CharaAnimator : MonoBehaviour
{
    enum AnimationState
    {
        None,
        Up,
        Down,
    }
    [SerializeField] Image image;
    [SerializeField] Button button;
    private AnimationState state = AnimationState.None;
    private int moveSpeed = 1;
    [SerializeField] private Vector2 startPosition;
    [SerializeField] private Vector2 animationPosition;
    private void Start()
    {
        image = GetComponent<Image>();
        button = gameObject.AddComponent<Button>();
        button.onClick.AddListener (() => {
            StartAnimation();
        });
        animationPosition = Vector2.zero;
    }
    private void StartAnimation()
    {
        SoundManager.Instance.PlaySE(SEType.Kinoko);
        state = AnimationState.Up;
    }
    private void Update()
    {
        switch(state)
        {
        case AnimationState.None:
            startPosition = GetComponent<RectTransform>().anchoredPosition;
            break;
        case AnimationState.Up:
            if (animationPosition.y < 30)
            {
                animationPosition += new Vector2(0, moveSpeed);
            }
            else
            {
                state = AnimationState.Down;
            }
            GetComponent<RectTransform>().anchoredPosition = startPosition + animationPosition;
            break;
        case AnimationState.Down:
            if (animationPosition.y > 0)
            {
                animationPosition -= new Vector2(0, moveSpeed);
            }
            else
            {
                animationPosition = Vector2.zero;
                state = AnimationState.None;
            }
            GetComponent<RectTransform>().anchoredPosition = startPosition + animationPosition;
            break;
        }
        Debug.LogError("GetComponent<RectTransform>().anchoredPosition is " + GetComponent<RectTransform>().anchoredPosition.y);
    }
}
