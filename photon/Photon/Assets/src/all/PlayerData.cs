using UnityEngine;
using System.Collections;
using System.Collections.Generic;

//ネットワークを使う場合も使わない場合もこのクラスをデータとして扱う。
public class PlayerData
{
    private const float EVOLUTION_VALUE_1 = 200.0f;
    private const float EVOLUTION_VALUE_2 = 1000.0f;
    private List<int> answers;
    public List<int> Answers { get { return answers;}}
    private int score1;//1体目のモンスターのスコア
    private int score2;//2体目のモンスターのスコア
    public int NowQuestionScore
    {
        get
        {
            if (Game.QuestionId == 0)
                return score1;
            else
                return score2;
        }
        private set
        {
            if (Game.QuestionId == 0)
                score1 = value;
            else
                score2 = value;
        }
    }
    public float PreEvolutionPercent
    {
        get
        {
            if (Game.QuestionId == 0)
                return 0.0f;
            else
            {
                return GetScorePercent(score1);
            }
        }
    }
    public float NextEvolutionPercent
    {
        get
        {
            if (Game.QuestionId == 0)
            {
                return GetScorePercent(score1);
            }
            else
            {
                return GetScorePercent(score1 + score2);
            }
        }
    }
    public float GetScorePercent(int score)
    {
        if (score < EVOLUTION_VALUE_1)
            return score / EVOLUTION_VALUE_1;
        else
            return 1 + (score - EVOLUTION_VALUE_1) / EVOLUTION_VALUE_2;
    }
    private int charaNo;
    public void SetCharaNo(int number) { charaNo = number;}
    public int CharaNo { get { return charaNo;}}

    private int charaLevel = 1;
    public void SetCharaLevel(int level) { charaLevel = level;}
    public int CharaLevel{ get { return charaLevel;}}

    public PlayerData()
    {
        answers = new List<int>();
        for (int i = 0; i < 10; i++)
        {
            answers.Add(0);
        }
    }
    public void Reset()
    {
        for (int i = 0; i < 10; i++)
        {
            answers[i] = 0;
        }
    }
    public int AddAnswer(int index, int num)
    {
        answers[index] += num;
        return UpdateScore();
    }
    public int UpdateScore()
    {
        int tmpScore = 0;
        foreach(var answer in answers)
        {
            tmpScore += answer * 50;
            if (answer != 0)
            {
                tmpScore += 25;
            }
        }
        NowQuestionScore = tmpScore;
        return NowQuestionScore;
    }
}
