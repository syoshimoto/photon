﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Game
{
    public static string RoomName = "myRoom2";
    public static int QuestionId { get { return questionId;}}
    public static int CharacterId { get { return characterId;}}
    public static int CharacterLevel { get { return characterLevel;}}
    public static List<List<List<int>>> allAnswerList = null;
    public static void ClearQuestion() { questionId = START_QUESTION + 1;}
    public static void SetCharacterId(int no) { characterId = no;}
    public static void AddLevel() { characterLevel++;}

    public const int START_QUESTION = 0;
    private static int questionId = START_QUESTION;
    private static int characterId = 1;
    private static int characterLevel = 1;
    public static void Reset()
    {
        questionId = START_QUESTION;
        characterId = 1;
        characterLevel = 1;
        allAnswerList = null;
    }

    public static string GetIconPath()
    {
        return GetIconPath(Game.CharacterId, Game.CharacterLevel);
    }
    public static string GetIconPath(int characterId, int characterLevel)
    {
        string charaName = GetCharaName(characterId);
        return charaName + "/CharIcon_" + charaName + "0" + characterLevel;
    }
    public static string GetCharaName(int num)
    {
        switch(num)
        {
        case 1:
            return "FOX";
        case 2:
            return "Clione";
        case 3:
            return "SeaUrchin";
        }
        return "";
    }
}
