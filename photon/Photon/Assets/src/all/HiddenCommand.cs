﻿using UnityEngine;
using System.Collections;

public class HiddenCommand {
    public static HiddenCommand GetInstance()
    {
        if (mInstance == null)
        {
            mInstance = new HiddenCommand();
        }
        return mInstance;
    }
    private static HiddenCommand mInstance;

    private int debugFrame = 0;
    public void Reset()
    {
        debugFrame = 0;
    }
    public bool Check()
    {
        if (Input.GetKeyDown(KeyCode.Space))
        {
            return true;
        }
        if (DataManager.GetInstance().IsGameMode(GameMode.Child))
        {
            return false;
        }
        if (Input.touchCount > 1)
        {
            if (debugFrame == 0)
            {
                bool isStart = false;
                for (int i = 0; i < Input.touchCount; i++)
                {
                    if (Input.touches[i].phase == TouchPhase.Began)
                    {
                        isStart = true;
                    }
                }
                if (!isStart)
                {
                    return false;
                }
            }
           debugFrame++;
           if (debugFrame > 20)
           {
               return true;
           }
       }
       else
       {
           debugFrame = 0;
       }
       return false;
    }
}
