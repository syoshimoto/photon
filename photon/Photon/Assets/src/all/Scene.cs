﻿using UnityEngine;
using System.Collections;

public class Scene
{
	public static string Title = "TitleScene";
	public static string Maching = "MachingScene";
    public static string WorldviewScene = "WorldviewScene";
	public static string CharacterSelect = "CharacterSelect";
	public static string Monsterexample = "Monsterexample";
	public static string Battle = "BattleScene";
	public static string BattleWatching = "BattleWatchingScene";
	public static string MonsterDeath = "MonsterDeathScene";
	public static string Description = "DescriptionScene";
	public static string Evolution = "Evolution";
    public static string Ending = "EndingScene";
	public static string TrueEnding = "TrueEndingScene";
	public static string ShowAnswer = "ShowAnswerScene";

	public static void ChangeScene(string str)
	{
        SoundManager.Instance.PlaySE(SEType.Choiced);
		PhotonNetwork.LoadLevel(str);
	}
	public static void ChangeApplicationScene(string str)
	{
        SoundManager.Instance.PlaySE(SEType.Choiced);
		Application.LoadLevel(str);
	}
}
