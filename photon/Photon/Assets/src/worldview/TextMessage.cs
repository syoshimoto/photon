﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;
using UnityEngine.EventSystems;

public class TextMessage : MonoBehaviour {

	[SerializeField] private int pageNo = 1;
	[SerializeField] private Text temp = null;
	[SerializeField] private GameObject obj = null;
	[SerializeField] private GameObject NextButton = null;
	[SerializeField] private WorldViewCanvas worldViewCanvas;

	void Start()
	{
		temp = this.transform.FindChild("Text").GetComponent<Text>();
    }

	void TextChange()
	{
		switch(pageNo)
		{
			// 世界観説明で使用
			case 1:
				temp.text = "のどかでへいわなさんすう村をあらすものが\nちかごろあらわれました。";
				pageNo += 1;
				break;
			case 2:
				temp.text = "手に入れたじょうほうによると\nまものたちは九九が大のにがてなようです。";
				pageNo += 1;
				break;
			case 3:
				temp.text = "かわいいモンスターたちと九九の力をつかって";
				pageNo += 1;
				break;
			case 4:
				temp.text = "さんすう村のへいわをまもりましょう！";
				pageNo += 1;
				NextButton.SetActive (false);
				break;

			// エンディングで使用
			case 11:
				temp.text = "まものたちはすがたを見せなくなった。";
				pageNo += 1;
				break;
			case 12:
				temp.text = "ゆうしゃとモンスターたちのかつやくは";
				pageNo += 1;
				break;
			case 13:
				temp.text = "これからもずっとさんすう村でかたりつがれるだろう・・・";
				pageNo += 1;
				break;
		}
	}

	public void TextChangeAnimation()
	{
		// ページ送りアニメーション
		if(pageNo != 5 && pageNo != 14) {
			this.GetComponent<Animator>().SetTrigger("NextAction");
		}
		// 世界観のテキスト読み上げ終了後にタップした場合
		else if(pageNo == 5)
		{
			worldViewCanvas.OnStoryEnd();
		}
		// エンディングのテキスト読み上げ終了後にタップした場合
		else if (pageNo == 14)
		{
			this.GetComponent<Animator>().SetTrigger("Ending");
			obj = this.transform.parent.gameObject;
        }
	}

	public void EndingLastAnimation()
	{
		obj.GetComponent<Animator>().Play("EndingLast");
	}
}
