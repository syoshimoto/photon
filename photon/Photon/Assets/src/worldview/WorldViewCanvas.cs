﻿using UnityEngine;
using System.Collections;

public class WorldViewCanvas : MonoBehaviour {

	[SerializeField] private float time;
	[SerializeField] private GameObject objCloud;

	void Start () {
        HiddenCommand.GetInstance().Reset();
        if (DataManager.GetInstance().IsGameMode(GameMode.OnePlay))
            return;
        if (!DataManager.GetInstance().UpdateConnect())
        {
            return;
        }
        PhotonNetwork.Instantiate("prefabs/PlayerNetwork", transform.position, Quaternion.identity, 0);
	}
	
	void Update () {
		//一定時間ごとに処理を行う
		time -= Time.deltaTime;
		if (time <= 0.0)
		{
			time = 6.0f;

			GameObject temp =  Instantiate(objCloud,new Vector3(0,220+Random.Range(10,50),0), Quaternion.identity) as GameObject;
			temp.transform.SetParent(this.transform, false);
			Destroy(temp, 20f);
		}
        if (HiddenCommand.GetInstance().Check())
        {
            OnStoryEnd();
        }
	}
	public void OnStoryEnd()
	{
        if (DataManager.GetInstance().IsGameMode(GameMode.Child))
            return;
		DataManager.GetInstance().ChangeToCharaSelect();
	}

}
