﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class TitleCanvas : MonoBehaviour
{
	[SerializeField] private Animator StartButtonAniime = null;

    private int hiddenTeacherFrame = 0;
    private int hiddenSingleFrame = 0;
    private const int HIDDEN_COMMAND_FRAME = 70;

    [SerializeField]
    private Text debugText1;
    [SerializeField]
    private Text debugText2;
    [SerializeField]
    private Text debugText3;

	public void Start()
	{
		this.GetComponent<Animator> ().SetTrigger ("TitleAnime1");
//		SoundManager.Instance.PlayBGM(SoundManager.Type.OP);
	}
	public void Update()
	{
        if (Input.GetKeyDown(KeyCode.Alpha2))
        {
            StartTeacher();
        }
        else if (Input.GetKeyDown(KeyCode.Alpha3))
        {
            StartSingle();
        }
		else if (Input.GetKeyDown(KeyCode.Space))
		{
			OnStartButtonPressed();
		}
        if (Input.touchCount == 2 && CheckHiddenInput())
        {
            hiddenTeacherFrame++;
            if (hiddenTeacherFrame > HIDDEN_COMMAND_FRAME)
            {
                StartTeacher();
            }
        }
        else
        {
            hiddenTeacherFrame = 0;
        }
        if (Input.touchCount == 3 && CheckHiddenInput())
        {
            hiddenSingleFrame++;
            if (hiddenSingleFrame > HIDDEN_COMMAND_FRAME)
            {
                StartSingle();
            }
        }
        else
        {
            hiddenSingleFrame = 0;
        }
	}
    private bool CheckHiddenInput()
    {
        for (int i = 0; i < Input.touchCount; i++)
        {
            string pos = ((int)Input.touches[i].position.x).ToString();
            switch(i)
            {
            case 0:
                debugText1.text = pos;
                break;
            case 1:
                debugText2.text = pos;
                break;
            case 2:
                debugText3.text = pos;
                break;
            }
            if (Input.touches[i].position.x > 300)
                return false;
        }
        return true;
    }
	public void OnStartButtonPressed()
	{
        SoundManager.Instance.PlaySE(SEType.Choiced);
		//StartChild();
		StartSingle();
	}
    private void StartSingle()
    {
        Scene.ChangeApplicationScene(Scene.WorldviewScene);
    }
	public void StartTeacher()
	{
		DataManager.GetInstance().SetGameMode(GameMode.Teacher);
		StartMatching();
	}
	public void StartChild()
	{
		DataManager.GetInstance().SetGameMode(GameMode.Child);
		StartMatching();
	}
	private void StartMatching()
	{
		SoundManager.Instance.PlaySE(SEType.Choiced);
        PhotonNetwork.automaticallySyncScene = false;
        PhotonNetwork.LoadLevel("MatchingScene");
	}

	public void StartButtonAnimationStart()
	{
		SoundManager.Instance.PlayBGM(SoundManager.Type.OP);
		StartButtonAniime.Play("StartButton");
	}


}
