﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

public class UIPlayerResult : MonoBehaviour
{
	[SerializeField]
	private UIAnswers answers;
	[SerializeField]
	private PlayerNetwork playerNetwork;
	[SerializeField]
	private GameObject crown;
	[SerializeField]
	private Text groupname;

	public void Set(int rank, string name, DataQuestion question, int characterId, int characterLevel, List<int> answerList)
	{
		groupname.text = name;
		if(rank == 1)
		{
			crown.SetActive(true);
		}
        answers.Show(rank, name, question, characterId, characterLevel, answerList);
	}
}
