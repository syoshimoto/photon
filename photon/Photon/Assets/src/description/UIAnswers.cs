﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class UIAnswers : MonoBehaviour {
	[SerializeField]
	private GameObject answer;
	public void Show(int rank, string name, DataQuestion question, int characterId, int characterLevel, List<int> answers)
	{
        string iconName = Game.GetIconPath(characterId, characterLevel);
        //image.sprite = Resources.Load<Sprite>("Sprite/" + iconName);
		for (int i = 0; i < question.answer1.Count; i++)
		{
			CreateAnswer(i, answers[i]);
		}
	}
	
	private void CreateAnswer(int i, int count)
	{
		var prefab = (GameObject)Resources.Load("prefabs/description/Answer");
        GameObject obj = Instantiate(prefab, Vector3.zero, Quaternion.identity) as GameObject;
        if (obj == null)
        {
        	Debug.LogError("null");
        	return;
        }
        obj.transform.SetParent(answer.transform);
        obj.transform.localPosition = new Vector3(i * DescriptionCanvas.Width * 0.92f, 0, 0);
        obj.transform.localScale = Vector3.one;
        obj.GetComponent<UIAnswer>().Set(count);
	}
}
