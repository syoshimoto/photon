﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;

public class DescriptionCanvas : MonoBehaviour
{
	[SerializeField]
	private GameObject results;
	[SerializeField]
	private GameObject examples;
	[SerializeField]
	private Button button;

	public static int Width = 95;
	void Start()
	{
		DataQuestion question = DataQuestion.Get(Game.QuestionId);
		//DataQuestion question = DataQuestion.Get(0);
		if (true)
		{
			int i = 0;
            PlayerManager.GetInstance().Players.Sort(
                delegate (PlayerNetwork a, PlayerNetwork b)
                {
                    return (a.Data.NowQuestionScore > b.Data.NowQuestionScore) ? -1 : 1;
                });
			foreach (var playerNetwork in PlayerManager.GetInstance().Players)
			{
				if (playerNetwork.photonView.owner.name == "owner")
					continue;
				CreatePlayer(i, playerNetwork.GetName(), question, playerNetwork.Data.CharaNo, playerNetwork.Data.CharaLevel, playerNetwork.Data.Answers);
				i++;
			}
		}
		else
		{
			List<int> answers = new List<int>();
			for (int i = 0; i < question.answer1.Count; i++)
			{
				answers.Add(i);
			}
			for (int i = 0; i < 7; i++)
			{
				CreatePlayer(i, (i+1).ToString(), question, Game.CharacterId, Game.CharacterLevel, answers);
			}
		}
		HiddenCommand.GetInstance().Reset();
		CreateAnswerExamples(question);
	}

	public void Update()
	{
		if (HiddenCommand.GetInstance().Check())
		{
			OnPressButton();
		}
	}



	public void OnPressButton()
	{
		DataManager.GetInstance().ChangeToEvolution();
	}
	private void CreatePlayer(int i, string name, DataQuestion question, int characterId, int characterLevel, List<int> answers)
	{
        int rank = i + 1;
		string iconName = Game.GetIconPath(characterId, characterLevel);
		Debug.Log(iconName);
		Sprite sprite = Resources.Load<Sprite>("Sprite/" + iconName);


		var prefab = (GameObject)Resources.Load("prefabs/description/UIPlayerResult");
		GameObject obj = Instantiate(prefab, Vector3.zero, Quaternion.identity) as GameObject;
		if (obj == null)
		{
			Debug.LogError("null");
			return;
		}
		obj.transform.FindChild("Name").GetComponent<Image>().sprite = sprite;
		obj.transform.SetParent(results.transform);
		obj.transform.localPosition = new Vector3(0, -i * 88, 0);
		obj.transform.localScale = Vector3.one * 1.1f;
		obj.GetComponent<UIPlayerResult>().Set(rank, "グループ" + name, question, characterId, characterLevel, answers);
	}
	private void CreateAnswerExamples(DataQuestion question)
	{
		int count = 10;
		int i = 0;
		foreach (var multi1 in question.answer1)
		{
			CreateAnswerExample(i, multi1, question.num / multi1);
			i++;
		}
	}
	private void CreateAnswerExample(int i, int multi1, int multi2)
	{
		var prefab = (GameObject)Resources.Load("prefabs/description/AnswerExample");
		GameObject obj = Instantiate(prefab, Vector3.zero, Quaternion.identity) as GameObject;
		if (obj == null)
		{
			Debug.LogError("null");
			return;
		}
		obj.transform.SetParent(examples.transform);
		obj.transform.localPosition = new Vector3(i * DescriptionCanvas.Width, 32, 0);
		obj.transform.localScale = Vector3.one * 1.4f;
		obj.GetComponent<AnswerExample>().Set(multi1, multi2);
	}
}
