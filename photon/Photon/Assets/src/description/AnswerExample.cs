﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class AnswerExample : MonoBehaviour
{
	[SerializeField]
	private Text multi1;
	[SerializeField]
	private Text multi2;
	public void Set(int i1, int i2)
	{
		multi1.text = i1.ToString();
		multi2.text = i2.ToString();
	}
}
