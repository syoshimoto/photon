﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class UIAnswer : MonoBehaviour {
	[SerializeField]
	private Text text;
	public void Set(int count)
	{
		text.text = count.ToString();
	}
}
