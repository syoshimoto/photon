﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class DataQuestion
{
	public int multiple1;//問題を表示するときの縦の丸の数
	public int multiple2;//横の丸の数
	public int num;//multiple1 * multiple2
	public int bossHp;//これ以上ダメージ見つけると死亡
	public List<int> answer1;
	public static DataQuestion Get(int i)
	{
		switch(i)
		{
			case 0:
			{
				// DataQuestion question = CreateAnswer(2, 3);
				// question.answer1.Add(1);
				// question.answer1.Add(2);
				// question.answer1.Add(3);
				// question.answer1.Add(6);
				// question.bossHp = 400;
				// return question;

				// DataQuestion question = CreateAnswer(3, 4);
				// question.answer1.Add(1);
				// question.answer1.Add(2);
				// question.answer1.Add(3);
				// question.answer1.Add(4);
				// question.answer1.Add(6);
				// question.answer1.Add(12);
				// question.bossHp = 5;
				// return question;

				DataQuestion question = CreateAnswer(4, 5);
				question.answer1.Add(1);
				question.answer1.Add(2);
				question.answer1.Add(4);
				question.answer1.Add(5);
				question.answer1.Add(10);
				question.answer1.Add(20);
				question.bossHp = 2000;
				return question;
			}
			case 1:
			{
				DataQuestion question = CreateAnswer(5, 6);
				question.answer1.Add(1);
				question.answer1.Add(2);
				question.answer1.Add(3);
				question.answer1.Add(5);
				question.answer1.Add(6);
				question.answer1.Add(10);
				question.answer1.Add(15);
				question.answer1.Add(30);
				question.bossHp = 5000;
				return question;
			}
		}
		return null;
	}
	private static DataQuestion CreateAnswer(int num1, int num2)
	{
		DataQuestion question = new DataQuestion();
		question.multiple1 = num1;
		question.multiple2 = num2;
		question.num = num1 * num2;
		question.answer1 = new List<int>();
		return question;
	}
}
