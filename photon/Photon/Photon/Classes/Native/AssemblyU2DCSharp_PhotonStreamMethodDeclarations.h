﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PhotonStream
struct PhotonStream_t8_106;
// System.Object[]
struct ObjectU5BU5D_t1_157;
// System.Object
struct Object_t;
// System.String
struct String_t;
// PhotonPlayer
struct PhotonPlayer_t8_102;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Vector3.h"
#include "UnityEngine_UnityEngine_Vector2.h"
#include "UnityEngine_UnityEngine_Quaternion.h"

// System.Void PhotonStream::.ctor(System.Boolean,System.Object[])
extern "C" void PhotonStream__ctor_m8_537 (PhotonStream_t8_106 * __this, bool ___write, ObjectU5BU5D_t1_157* ___incomingData, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PhotonStream::get_isWriting()
extern "C" bool PhotonStream_get_isWriting_m8_538 (PhotonStream_t8_106 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PhotonStream::get_isReading()
extern "C" bool PhotonStream_get_isReading_m8_539 (PhotonStream_t8_106 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 PhotonStream::get_Count()
extern "C" int32_t PhotonStream_get_Count_m8_540 (PhotonStream_t8_106 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object PhotonStream::ReceiveNext()
extern "C" Object_t * PhotonStream_ReceiveNext_m8_541 (PhotonStream_t8_106 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object PhotonStream::PeekNext()
extern "C" Object_t * PhotonStream_PeekNext_m8_542 (PhotonStream_t8_106 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PhotonStream::SendNext(System.Object)
extern "C" void PhotonStream_SendNext_m8_543 (PhotonStream_t8_106 * __this, Object_t * ___obj, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object[] PhotonStream::ToArray()
extern "C" ObjectU5BU5D_t1_157* PhotonStream_ToArray_m8_544 (PhotonStream_t8_106 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PhotonStream::Serialize(System.Boolean&)
extern "C" void PhotonStream_Serialize_m8_545 (PhotonStream_t8_106 * __this, bool* ___myBool, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PhotonStream::Serialize(System.Int32&)
extern "C" void PhotonStream_Serialize_m8_546 (PhotonStream_t8_106 * __this, int32_t* ___myInt, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PhotonStream::Serialize(System.String&)
extern "C" void PhotonStream_Serialize_m8_547 (PhotonStream_t8_106 * __this, String_t** ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PhotonStream::Serialize(System.Char&)
extern "C" void PhotonStream_Serialize_m8_548 (PhotonStream_t8_106 * __this, uint16_t* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PhotonStream::Serialize(System.Int16&)
extern "C" void PhotonStream_Serialize_m8_549 (PhotonStream_t8_106 * __this, int16_t* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PhotonStream::Serialize(System.Single&)
extern "C" void PhotonStream_Serialize_m8_550 (PhotonStream_t8_106 * __this, float* ___obj, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PhotonStream::Serialize(PhotonPlayer&)
extern "C" void PhotonStream_Serialize_m8_551 (PhotonStream_t8_106 * __this, PhotonPlayer_t8_102 ** ___obj, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PhotonStream::Serialize(UnityEngine.Vector3&)
extern "C" void PhotonStream_Serialize_m8_552 (PhotonStream_t8_106 * __this, Vector3_t6_49 * ___obj, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PhotonStream::Serialize(UnityEngine.Vector2&)
extern "C" void PhotonStream_Serialize_m8_553 (PhotonStream_t8_106 * __this, Vector2_t6_48 * ___obj, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PhotonStream::Serialize(UnityEngine.Quaternion&)
extern "C" void PhotonStream_Serialize_m8_554 (PhotonStream_t8_106 * __this, Quaternion_t6_51 * ___obj, const MethodInfo* method) IL2CPP_METHOD_ATTR;
