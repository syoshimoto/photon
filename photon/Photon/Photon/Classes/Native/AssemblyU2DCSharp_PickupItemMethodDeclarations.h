﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PickupItem
struct PickupItem_t8_163;
// UnityEngine.Collider
struct Collider_t6_100;
// PhotonStream
struct PhotonStream_t8_106;
// PhotonMessageInfo
struct PhotonMessageInfo_t8_104;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Vector3.h"

// System.Void PickupItem::.ctor()
extern "C" void PickupItem__ctor_m8_981 (PickupItem_t8_163 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PickupItem::.cctor()
extern "C" void PickupItem__cctor_m8_982 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 PickupItem::get_ViewID()
extern "C" int32_t PickupItem_get_ViewID_m8_983 (PickupItem_t8_163 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PickupItem::OnTriggerEnter(UnityEngine.Collider)
extern "C" void PickupItem_OnTriggerEnter_m8_984 (PickupItem_t8_163 * __this, Collider_t6_100 * ___other, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PickupItem::OnPhotonSerializeView(PhotonStream,PhotonMessageInfo)
extern "C" void PickupItem_OnPhotonSerializeView_m8_985 (PickupItem_t8_163 * __this, PhotonStream_t8_106 * ___stream, PhotonMessageInfo_t8_104 * ___info, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PickupItem::Pickup()
extern "C" void PickupItem_Pickup_m8_986 (PickupItem_t8_163 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PickupItem::Drop()
extern "C" void PickupItem_Drop_m8_987 (PickupItem_t8_163 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PickupItem::Drop(UnityEngine.Vector3)
extern "C" void PickupItem_Drop_m8_988 (PickupItem_t8_163 * __this, Vector3_t6_49  ___newPosition, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PickupItem::PunPickup(PhotonMessageInfo)
extern "C" void PickupItem_PunPickup_m8_989 (PickupItem_t8_163 * __this, PhotonMessageInfo_t8_104 * ___msgInfo, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PickupItem::PickedUp(System.Single)
extern "C" void PickupItem_PickedUp_m8_990 (PickupItem_t8_163 * __this, float ___timeUntilRespawn, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PickupItem::PunRespawn(UnityEngine.Vector3)
extern "C" void PickupItem_PunRespawn_m8_991 (PickupItem_t8_163 * __this, Vector3_t6_49  ___pos, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PickupItem::PunRespawn()
extern "C" void PickupItem_PunRespawn_m8_992 (PickupItem_t8_163 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
