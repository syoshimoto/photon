﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// WorkerMenu
struct WorkerMenu_t8_51;
// System.String
struct String_t;
// System.Object[]
struct ObjectU5BU5D_t1_157;
// System.Object
struct Object_t;

#include "codegen/il2cpp-codegen.h"

// System.Void WorkerMenu::.ctor()
extern "C" void WorkerMenu__ctor_m8_224 (WorkerMenu_t8_51 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WorkerMenu::.cctor()
extern "C" void WorkerMenu__cctor_m8_225 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String WorkerMenu::get_ErrorDialog()
extern "C" String_t* WorkerMenu_get_ErrorDialog_m8_226 (WorkerMenu_t8_51 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WorkerMenu::set_ErrorDialog(System.String)
extern "C" void WorkerMenu_set_ErrorDialog_m8_227 (WorkerMenu_t8_51 * __this, String_t* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WorkerMenu::Awake()
extern "C" void WorkerMenu_Awake_m8_228 (WorkerMenu_t8_51 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WorkerMenu::OnGUI()
extern "C" void WorkerMenu_OnGUI_m8_229 (WorkerMenu_t8_51 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WorkerMenu::OnJoinedRoom()
extern "C" void WorkerMenu_OnJoinedRoom_m8_230 (WorkerMenu_t8_51 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WorkerMenu::OnPhotonCreateRoomFailed()
extern "C" void WorkerMenu_OnPhotonCreateRoomFailed_m8_231 (WorkerMenu_t8_51 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WorkerMenu::OnPhotonJoinRoomFailed(System.Object[])
extern "C" void WorkerMenu_OnPhotonJoinRoomFailed_m8_232 (WorkerMenu_t8_51 * __this, ObjectU5BU5D_t1_157* ___cause, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WorkerMenu::OnPhotonRandomJoinFailed()
extern "C" void WorkerMenu_OnPhotonRandomJoinFailed_m8_233 (WorkerMenu_t8_51 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WorkerMenu::OnCreatedRoom()
extern "C" void WorkerMenu_OnCreatedRoom_m8_234 (WorkerMenu_t8_51 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WorkerMenu::OnDisconnectedFromPhoton()
extern "C" void WorkerMenu_OnDisconnectedFromPhoton_m8_235 (WorkerMenu_t8_51 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WorkerMenu::OnFailedToConnectToPhoton(System.Object)
extern "C" void WorkerMenu_OnFailedToConnectToPhoton_m8_236 (WorkerMenu_t8_51 * __this, Object_t * ___parameters, const MethodInfo* method) IL2CPP_METHOD_ATTR;
