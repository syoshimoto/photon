﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PhotonTransformViewRotationModel
struct PhotonTransformViewRotationModel_t8_139;

#include "codegen/il2cpp-codegen.h"

// System.Void PhotonTransformViewRotationModel::.ctor()
extern "C" void PhotonTransformViewRotationModel__ctor_m8_915 (PhotonTransformViewRotationModel_t8_139 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
