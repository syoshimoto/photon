﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PhotonStreamQueue
struct PhotonStreamQueue_t8_117;
// System.Object
struct Object_t;
// PhotonStream
struct PhotonStream_t8_106;

#include "codegen/il2cpp-codegen.h"

// System.Void PhotonStreamQueue::.ctor(System.Int32)
extern "C" void PhotonStreamQueue__ctor_m8_754 (PhotonStreamQueue_t8_117 * __this, int32_t ___sampleRate, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PhotonStreamQueue::BeginWritePackage()
extern "C" void PhotonStreamQueue_BeginWritePackage_m8_755 (PhotonStreamQueue_t8_117 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PhotonStreamQueue::Reset()
extern "C" void PhotonStreamQueue_Reset_m8_756 (PhotonStreamQueue_t8_117 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PhotonStreamQueue::SendNext(System.Object)
extern "C" void PhotonStreamQueue_SendNext_m8_757 (PhotonStreamQueue_t8_117 * __this, Object_t * ___obj, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PhotonStreamQueue::HasQueuedObjects()
extern "C" bool PhotonStreamQueue_HasQueuedObjects_m8_758 (PhotonStreamQueue_t8_117 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object PhotonStreamQueue::ReceiveNext()
extern "C" Object_t * PhotonStreamQueue_ReceiveNext_m8_759 (PhotonStreamQueue_t8_117 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PhotonStreamQueue::Serialize(PhotonStream)
extern "C" void PhotonStreamQueue_Serialize_m8_760 (PhotonStreamQueue_t8_117 * __this, PhotonStream_t8_106 * ___stream, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PhotonStreamQueue::Deserialize(PhotonStream)
extern "C" void PhotonStreamQueue_Deserialize_m8_761 (PhotonStreamQueue_t8_117 * __this, PhotonStream_t8_106 * ___stream, const MethodInfo* method) IL2CPP_METHOD_ATTR;
