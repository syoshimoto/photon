﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// ExitGames.Client.Photon.TPeer
struct TPeer_t5_59;
// System.Byte[]
struct ByteU5BU5D_t1_71;

#include "mscorlib_System_Object.h"

// ExitGames.Client.Photon.TPeer/<>c__DisplayClass3
struct  U3CU3Ec__DisplayClass3_t5_58  : public Object_t
{
	// ExitGames.Client.Photon.TPeer ExitGames.Client.Photon.TPeer/<>c__DisplayClass3::<>4__this
	TPeer_t5_59 * ___U3CU3E4__this_0;
	// System.Byte[] ExitGames.Client.Photon.TPeer/<>c__DisplayClass3::data
	ByteU5BU5D_t1_71* ___data_1;
};
