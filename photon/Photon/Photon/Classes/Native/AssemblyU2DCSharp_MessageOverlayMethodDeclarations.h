﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// MessageOverlay
struct MessageOverlay_t8_28;

#include "codegen/il2cpp-codegen.h"

// System.Void MessageOverlay::.ctor()
extern "C" void MessageOverlay__ctor_m8_98 (MessageOverlay_t8_28 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MessageOverlay::Start()
extern "C" void MessageOverlay_Start_m8_99 (MessageOverlay_t8_28 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MessageOverlay::OnJoinedRoom()
extern "C" void MessageOverlay_OnJoinedRoom_m8_100 (MessageOverlay_t8_28 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MessageOverlay::OnLeftRoom()
extern "C" void MessageOverlay_OnLeftRoom_m8_101 (MessageOverlay_t8_28 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MessageOverlay::SetActive(System.Boolean)
extern "C" void MessageOverlay_SetActive_m8_102 (MessageOverlay_t8_28 * __this, bool ___enable, const MethodInfo* method) IL2CPP_METHOD_ATTR;
