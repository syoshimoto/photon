﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "mscorlib_System_Object.h"
#include "AssemblyU2DCSharp_PhotonAnimatorView_SynchronizeType.h"

// PhotonAnimatorView/SynchronizedLayer
struct  SynchronizedLayer_t8_129  : public Object_t
{
	// PhotonAnimatorView/SynchronizeType PhotonAnimatorView/SynchronizedLayer::SynchronizeType
	int32_t ___SynchronizeType_0;
	// System.Int32 PhotonAnimatorView/SynchronizedLayer::LayerIndex
	int32_t ___LayerIndex_1;
};
