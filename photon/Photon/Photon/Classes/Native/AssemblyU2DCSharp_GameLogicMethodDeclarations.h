﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GameLogic
struct GameLogic_t8_54;
// PhotonPlayer
struct PhotonPlayer_t8_102;

#include "codegen/il2cpp-codegen.h"

// System.Void GameLogic::.ctor()
extern "C" void GameLogic__ctor_m8_245 (GameLogic_t8_54 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameLogic::.cctor()
extern "C" void GameLogic__cctor_m8_246 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameLogic::Start()
extern "C" void GameLogic_Start_m8_247 (GameLogic_t8_54 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameLogic::OnJoinedRoom()
extern "C" void GameLogic_OnJoinedRoom_m8_248 (GameLogic_t8_54 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameLogic::OnPhotonPlayerConnected(PhotonPlayer)
extern "C" void GameLogic_OnPhotonPlayerConnected_m8_249 (GameLogic_t8_54 * __this, PhotonPlayer_t8_102 * ___player, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameLogic::TagPlayer(System.Int32)
extern "C" void GameLogic_TagPlayer_m8_250 (Object_t * __this /* static, unused */, int32_t ___playerID, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameLogic::TaggedPlayer(System.Int32)
extern "C" void GameLogic_TaggedPlayer_m8_251 (GameLogic_t8_54 * __this, int32_t ___playerID, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameLogic::OnPhotonPlayerDisconnected(PhotonPlayer)
extern "C" void GameLogic_OnPhotonPlayerDisconnected_m8_252 (GameLogic_t8_54 * __this, PhotonPlayer_t8_102 * ___player, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameLogic::OnMasterClientSwitched()
extern "C" void GameLogic_OnMasterClientSwitched_m8_253 (GameLogic_t8_54 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
