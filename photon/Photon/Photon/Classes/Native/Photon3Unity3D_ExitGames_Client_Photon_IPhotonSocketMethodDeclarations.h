﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ExitGames.Client.Photon.IPhotonSocket
struct IPhotonSocket_t5_12;
// ExitGames.Client.Photon.IPhotonPeerListener
struct IPhotonPeerListener_t5_17;
// System.String
struct String_t;
// ExitGames.Client.Photon.PeerBase
struct PeerBase_t5_10;
// System.Byte[]
struct ByteU5BU5D_t1_71;
// System.Net.IPAddress
struct IPAddress_t3_54;

#include "codegen/il2cpp-codegen.h"
#include "Photon3Unity3D_ExitGames_Client_Photon_ConnectionProtocol.h"
#include "Photon3Unity3D_ExitGames_Client_Photon_PhotonSocketState.h"
#include "Photon3Unity3D_ExitGames_Client_Photon_DebugLevel.h"
#include "Photon3Unity3D_ExitGames_Client_Photon_StatusCode.h"

// ExitGames.Client.Photon.IPhotonPeerListener ExitGames.Client.Photon.IPhotonSocket::get_Listener()
extern "C" Object_t * IPhotonSocket_get_Listener_m5_137 (IPhotonSocket_t5_12 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ExitGames.Client.Photon.ConnectionProtocol ExitGames.Client.Photon.IPhotonSocket::get_Protocol()
extern "C" uint8_t IPhotonSocket_get_Protocol_m5_138 (IPhotonSocket_t5_12 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ExitGames.Client.Photon.IPhotonSocket::set_Protocol(ExitGames.Client.Photon.ConnectionProtocol)
extern "C" void IPhotonSocket_set_Protocol_m5_139 (IPhotonSocket_t5_12 * __this, uint8_t ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ExitGames.Client.Photon.PhotonSocketState ExitGames.Client.Photon.IPhotonSocket::get_State()
extern "C" int32_t IPhotonSocket_get_State_m5_140 (IPhotonSocket_t5_12 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ExitGames.Client.Photon.IPhotonSocket::set_State(ExitGames.Client.Photon.PhotonSocketState)
extern "C" void IPhotonSocket_set_State_m5_141 (IPhotonSocket_t5_12 * __this, int32_t ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String ExitGames.Client.Photon.IPhotonSocket::get_ServerAddress()
extern "C" String_t* IPhotonSocket_get_ServerAddress_m5_142 (IPhotonSocket_t5_12 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ExitGames.Client.Photon.IPhotonSocket::set_ServerAddress(System.String)
extern "C" void IPhotonSocket_set_ServerAddress_m5_143 (IPhotonSocket_t5_12 * __this, String_t* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ExitGames.Client.Photon.IPhotonSocket::get_ServerPort()
extern "C" int32_t IPhotonSocket_get_ServerPort_m5_144 (IPhotonSocket_t5_12 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ExitGames.Client.Photon.IPhotonSocket::set_ServerPort(System.Int32)
extern "C" void IPhotonSocket_set_ServerPort_m5_145 (IPhotonSocket_t5_12 * __this, int32_t ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ExitGames.Client.Photon.IPhotonSocket::get_Connected()
extern "C" bool IPhotonSocket_get_Connected_m5_146 (IPhotonSocket_t5_12 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ExitGames.Client.Photon.IPhotonSocket::get_MTU()
extern "C" int32_t IPhotonSocket_get_MTU_m5_147 (IPhotonSocket_t5_12 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ExitGames.Client.Photon.IPhotonSocket::.ctor(ExitGames.Client.Photon.PeerBase)
extern "C" void IPhotonSocket__ctor_m5_148 (IPhotonSocket_t5_12 * __this, PeerBase_t5_10 * ___peerBase, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ExitGames.Client.Photon.IPhotonSocket::Connect()
extern "C" bool IPhotonSocket_Connect_m5_149 (IPhotonSocket_t5_12 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ExitGames.Client.Photon.IPhotonSocket::HandleReceivedDatagram(System.Byte[],System.Int32,System.Boolean)
extern "C" void IPhotonSocket_HandleReceivedDatagram_m5_150 (IPhotonSocket_t5_12 * __this, ByteU5BU5D_t1_71* ___inBuffer, int32_t ___length, bool ___willBeReused, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ExitGames.Client.Photon.IPhotonSocket::ReportDebugOfLevel(ExitGames.Client.Photon.DebugLevel)
extern "C" bool IPhotonSocket_ReportDebugOfLevel_m5_151 (IPhotonSocket_t5_12 * __this, uint8_t ___levelOfMessage, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ExitGames.Client.Photon.IPhotonSocket::EnqueueDebugReturn(ExitGames.Client.Photon.DebugLevel,System.String)
extern "C" void IPhotonSocket_EnqueueDebugReturn_m5_152 (IPhotonSocket_t5_12 * __this, uint8_t ___debugLevel, String_t* ___message, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ExitGames.Client.Photon.IPhotonSocket::HandleException(ExitGames.Client.Photon.StatusCode)
extern "C" void IPhotonSocket_HandleException_m5_153 (IPhotonSocket_t5_12 * __this, int32_t ___statusCode, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ExitGames.Client.Photon.IPhotonSocket::TryParseAddress(System.String,System.String&,System.UInt16&)
extern "C" bool IPhotonSocket_TryParseAddress_m5_154 (IPhotonSocket_t5_12 * __this, String_t* ___addressAndPort, String_t** ___address, uint16_t* ___port, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Net.IPAddress ExitGames.Client.Photon.IPhotonSocket::GetIpAddress(System.String)
extern "C" IPAddress_t3_54 * IPhotonSocket_GetIpAddress_m5_155 (Object_t * __this /* static, unused */, String_t* ___serverIp, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ExitGames.Client.Photon.IPhotonSocket::<HandleException>b__7()
extern "C" void IPhotonSocket_U3CHandleExceptionU3Eb__7_m5_156 (IPhotonSocket_t5_12 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
