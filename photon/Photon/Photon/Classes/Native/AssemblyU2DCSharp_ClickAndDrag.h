﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "AssemblyU2DCSharp_Photon_MonoBehaviour.h"
#include "UnityEngine_UnityEngine_Vector3.h"

// ClickAndDrag
struct  ClickAndDrag_t8_7  : public MonoBehaviour_t8_6
{
	// UnityEngine.Vector3 ClickAndDrag::camOnPress
	Vector3_t6_49  ___camOnPress_2;
	// System.Boolean ClickAndDrag::following
	bool ___following_3;
	// System.Single ClickAndDrag::factor
	float ___factor_4;
};
