﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.ControllerColliderHit
struct ControllerColliderHit_t6_98;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Vector3.h"

// UnityEngine.Vector3 UnityEngine.ControllerColliderHit::get_moveDirection()
extern "C" Vector3_t6_49  ControllerColliderHit_get_moveDirection_m6_711 (ControllerColliderHit_t6_98 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
