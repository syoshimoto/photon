﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.GUILayout/LayoutedWindow
struct LayoutedWindow_t6_164;
// UnityEngine.GUI/WindowFunction
struct WindowFunction_t6_159;
// UnityEngine.GUIContent
struct GUIContent_t6_163;
// UnityEngine.GUILayoutOption[]
struct GUILayoutOptionU5BU5D_t6_165;
// UnityEngine.GUIStyle
struct GUIStyle_t6_166;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Rect.h"

// System.Void UnityEngine.GUILayout/LayoutedWindow::.ctor(UnityEngine.GUI/WindowFunction,UnityEngine.Rect,UnityEngine.GUIContent,UnityEngine.GUILayoutOption[],UnityEngine.GUIStyle)
extern "C" void LayoutedWindow__ctor_m6_1110 (LayoutedWindow_t6_164 * __this, WindowFunction_t6_159 * ___f, Rect_t6_52  ___screenRect, GUIContent_t6_163 * ___content, GUILayoutOptionU5BU5D_t6_165* ___options, GUIStyle_t6_166 * ___style, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.GUILayout/LayoutedWindow::DoWindow(System.Int32)
extern "C" void LayoutedWindow_DoWindow_m6_1111 (LayoutedWindow_t6_164 * __this, int32_t ___windowID, const MethodInfo* method) IL2CPP_METHOD_ATTR;
