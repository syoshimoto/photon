﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_KeyCollecti_14MethodDeclarations.h"

// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<System.Int32,PhotonPlayer>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
#define KeyCollection__ctor_m1_9979(__this, ___dictionary, method) (( void (*) (KeyCollection_t1_962 *, Dictionary_2_t1_936 *, const MethodInfo*))KeyCollection__ctor_m1_7465_gshared)(__this, ___dictionary, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<System.Int32,PhotonPlayer>::System.Collections.Generic.ICollection<TKey>.Add(TKey)
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m1_9980(__this, ___item, method) (( void (*) (KeyCollection_t1_962 *, int32_t, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m1_7466_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<System.Int32,PhotonPlayer>::System.Collections.Generic.ICollection<TKey>.Clear()
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m1_9981(__this, method) (( void (*) (KeyCollection_t1_962 *, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m1_7467_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<System.Int32,PhotonPlayer>::System.Collections.Generic.ICollection<TKey>.Contains(TKey)
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m1_9982(__this, ___item, method) (( bool (*) (KeyCollection_t1_962 *, int32_t, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m1_7468_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<System.Int32,PhotonPlayer>::System.Collections.Generic.ICollection<TKey>.Remove(TKey)
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m1_9983(__this, ___item, method) (( bool (*) (KeyCollection_t1_962 *, int32_t, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m1_7469_gshared)(__this, ___item, method)
// System.Collections.Generic.IEnumerator`1<TKey> System.Collections.Generic.Dictionary`2/KeyCollection<System.Int32,PhotonPlayer>::System.Collections.Generic.IEnumerable<TKey>.GetEnumerator()
#define KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m1_9984(__this, method) (( Object_t* (*) (KeyCollection_t1_962 *, const MethodInfo*))KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m1_7470_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<System.Int32,PhotonPlayer>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define KeyCollection_System_Collections_ICollection_CopyTo_m1_9985(__this, ___array, ___index, method) (( void (*) (KeyCollection_t1_962 *, Array_t *, int32_t, const MethodInfo*))KeyCollection_System_Collections_ICollection_CopyTo_m1_7471_gshared)(__this, ___array, ___index, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2/KeyCollection<System.Int32,PhotonPlayer>::System.Collections.IEnumerable.GetEnumerator()
#define KeyCollection_System_Collections_IEnumerable_GetEnumerator_m1_9986(__this, method) (( Object_t * (*) (KeyCollection_t1_962 *, const MethodInfo*))KeyCollection_System_Collections_IEnumerable_GetEnumerator_m1_7472_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<System.Int32,PhotonPlayer>::System.Collections.Generic.ICollection<TKey>.get_IsReadOnly()
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m1_9987(__this, method) (( bool (*) (KeyCollection_t1_962 *, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m1_7473_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/KeyCollection<System.Int32,PhotonPlayer>::System.Collections.ICollection.get_SyncRoot()
#define KeyCollection_System_Collections_ICollection_get_SyncRoot_m1_9988(__this, method) (( Object_t * (*) (KeyCollection_t1_962 *, const MethodInfo*))KeyCollection_System_Collections_ICollection_get_SyncRoot_m1_7474_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<System.Int32,PhotonPlayer>::CopyTo(TKey[],System.Int32)
#define KeyCollection_CopyTo_m1_9989(__this, ___array, ___index, method) (( void (*) (KeyCollection_t1_962 *, Int32U5BU5D_t1_160*, int32_t, const MethodInfo*))KeyCollection_CopyTo_m1_7475_gshared)(__this, ___array, ___index, method)
// System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2/KeyCollection<System.Int32,PhotonPlayer>::GetEnumerator()
#define KeyCollection_GetEnumerator_m1_5652(__this, method) (( Enumerator_t1_961  (*) (KeyCollection_t1_962 *, const MethodInfo*))KeyCollection_GetEnumerator_m1_7476_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2/KeyCollection<System.Int32,PhotonPlayer>::get_Count()
#define KeyCollection_get_Count_m1_9990(__this, method) (( int32_t (*) (KeyCollection_t1_962 *, const MethodInfo*))KeyCollection_get_Count_m1_7477_gshared)(__this, method)
