﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Specialized.ListDictionary/DictionaryNodeCollection/DictionaryNodeCollectionEnumerator
struct DictionaryNodeCollectionEnumerator_t3_7;
// System.Collections.IDictionaryEnumerator
struct IDictionaryEnumerator_t1_458;
// System.Object
struct Object_t;

#include "codegen/il2cpp-codegen.h"

// System.Void System.Collections.Specialized.ListDictionary/DictionaryNodeCollection/DictionaryNodeCollectionEnumerator::.ctor(System.Collections.IDictionaryEnumerator,System.Boolean)
extern "C" void DictionaryNodeCollectionEnumerator__ctor_m3_29 (DictionaryNodeCollectionEnumerator_t3_7 * __this, Object_t * ___inner, bool ___isKeyList, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Collections.Specialized.ListDictionary/DictionaryNodeCollection/DictionaryNodeCollectionEnumerator::get_Current()
extern "C" Object_t * DictionaryNodeCollectionEnumerator_get_Current_m3_30 (DictionaryNodeCollectionEnumerator_t3_7 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Collections.Specialized.ListDictionary/DictionaryNodeCollection/DictionaryNodeCollectionEnumerator::MoveNext()
extern "C" bool DictionaryNodeCollectionEnumerator_MoveNext_m3_31 (DictionaryNodeCollectionEnumerator_t3_7 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.Specialized.ListDictionary/DictionaryNodeCollection/DictionaryNodeCollectionEnumerator::Reset()
extern "C" void DictionaryNodeCollectionEnumerator_Reset_m3_32 (DictionaryNodeCollectionEnumerator_t3_7 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
