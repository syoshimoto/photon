﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "AssemblyU2DCSharp_Photon_MonoBehaviour.h"

// PickupItemSimple
struct  PickupItemSimple_t8_164  : public MonoBehaviour_t8_6
{
	// System.Single PickupItemSimple::SecondsBeforeRespawn
	float ___SecondsBeforeRespawn_2;
	// System.Boolean PickupItemSimple::PickupOnCollide
	bool ___PickupOnCollide_3;
	// System.Boolean PickupItemSimple::SentPickup
	bool ___SentPickup_4;
};
