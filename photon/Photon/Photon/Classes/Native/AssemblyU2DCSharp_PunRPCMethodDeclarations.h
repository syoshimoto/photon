﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PunRPC
struct PunRPC_t8_123;

#include "codegen/il2cpp-codegen.h"

// System.Void PunRPC::.ctor()
extern "C" void PunRPC__ctor_m8_806 (PunRPC_t8_123 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
