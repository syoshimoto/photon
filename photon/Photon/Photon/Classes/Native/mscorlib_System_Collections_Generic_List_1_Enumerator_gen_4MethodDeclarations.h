﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_6MethodDeclarations.h"

// System.Void System.Collections.Generic.List`1/Enumerator<FriendInfo>::.ctor(System.Collections.Generic.List`1<T>)
#define Enumerator__ctor_m1_9508(__this, ___l, method) (( void (*) (Enumerator_t1_953 *, List_1_t1_946 *, const MethodInfo*))Enumerator__ctor_m1_5779_gshared)(__this, ___l, method)
// System.Void System.Collections.Generic.List`1/Enumerator<FriendInfo>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m1_9509(__this, method) (( void (*) (Enumerator_t1_953 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m1_5780_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1/Enumerator<FriendInfo>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m1_9510(__this, method) (( Object_t * (*) (Enumerator_t1_953 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m1_5781_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<FriendInfo>::Dispose()
#define Enumerator_Dispose_m1_9511(__this, method) (( void (*) (Enumerator_t1_953 *, const MethodInfo*))Enumerator_Dispose_m1_5782_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<FriendInfo>::VerifyState()
#define Enumerator_VerifyState_m1_9512(__this, method) (( void (*) (Enumerator_t1_953 *, const MethodInfo*))Enumerator_VerifyState_m1_5783_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<FriendInfo>::MoveNext()
#define Enumerator_MoveNext_m1_5626(__this, method) (( bool (*) (Enumerator_t1_953 *, const MethodInfo*))Enumerator_MoveNext_m1_5784_gshared)(__this, method)
// T System.Collections.Generic.List`1/Enumerator<FriendInfo>::get_Current()
#define Enumerator_get_Current_m1_5625(__this, method) (( FriendInfo_t8_74 * (*) (Enumerator_t1_953 *, const MethodInfo*))Enumerator_get_Current_m1_5785_gshared)(__this, method)
