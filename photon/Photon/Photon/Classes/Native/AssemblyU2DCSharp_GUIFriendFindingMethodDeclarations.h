﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GUIFriendFinding
struct GUIFriendFinding_t8_18;
// System.String[]
struct StringU5BU5D_t1_202;

#include "codegen/il2cpp-codegen.h"

// System.Void GUIFriendFinding::.ctor()
extern "C" void GUIFriendFinding__ctor_m8_70 (GUIFriendFinding_t8_18 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GUIFriendFinding::Start()
extern "C" void GUIFriendFinding_Start_m8_71 (GUIFriendFinding_t8_18 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String[] GUIFriendFinding::FetchFriendsFromCommunity()
extern "C" StringU5BU5D_t1_202* GUIFriendFinding_FetchFriendsFromCommunity_m8_72 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GUIFriendFinding::OnUpdatedFriendList()
extern "C" void GUIFriendFinding_OnUpdatedFriendList_m8_73 (GUIFriendFinding_t8_18 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GUIFriendFinding::OnGUI()
extern "C" void GUIFriendFinding_OnGUI_m8_74 (GUIFriendFinding_t8_18 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
