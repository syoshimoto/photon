﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Array
struct Array_t;
// System.Object
struct Object_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen_47.h"
#include "mscorlib_System_Reflection_CustomAttributeTypedArgument.h"

// System.Void System.Array/InternalEnumerator`1<System.Reflection.CustomAttributeTypedArgument>::.ctor(System.Array)
extern "C" void InternalEnumerator_1__ctor_m1_6529_gshared (InternalEnumerator_1_t1_1070 * __this, Array_t * ___array, const MethodInfo* method);
#define InternalEnumerator_1__ctor_m1_6529(__this, ___array, method) (( void (*) (InternalEnumerator_1_t1_1070 *, Array_t *, const MethodInfo*))InternalEnumerator_1__ctor_m1_6529_gshared)(__this, ___array, method)
// System.Void System.Array/InternalEnumerator`1<System.Reflection.CustomAttributeTypedArgument>::System.Collections.IEnumerator.Reset()
extern "C" void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1_6530_gshared (InternalEnumerator_1_t1_1070 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1_6530(__this, method) (( void (*) (InternalEnumerator_1_t1_1070 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1_6530_gshared)(__this, method)
// System.Object System.Array/InternalEnumerator`1<System.Reflection.CustomAttributeTypedArgument>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1_6531_gshared (InternalEnumerator_1_t1_1070 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1_6531(__this, method) (( Object_t * (*) (InternalEnumerator_1_t1_1070 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1_6531_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<System.Reflection.CustomAttributeTypedArgument>::Dispose()
extern "C" void InternalEnumerator_1_Dispose_m1_6532_gshared (InternalEnumerator_1_t1_1070 * __this, const MethodInfo* method);
#define InternalEnumerator_1_Dispose_m1_6532(__this, method) (( void (*) (InternalEnumerator_1_t1_1070 *, const MethodInfo*))InternalEnumerator_1_Dispose_m1_6532_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<System.Reflection.CustomAttributeTypedArgument>::MoveNext()
extern "C" bool InternalEnumerator_1_MoveNext_m1_6533_gshared (InternalEnumerator_1_t1_1070 * __this, const MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m1_6533(__this, method) (( bool (*) (InternalEnumerator_1_t1_1070 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m1_6533_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<System.Reflection.CustomAttributeTypedArgument>::get_Current()
extern "C" CustomAttributeTypedArgument_t1_332  InternalEnumerator_1_get_Current_m1_6534_gshared (InternalEnumerator_1_t1_1070 * __this, const MethodInfo* method);
#define InternalEnumerator_1_get_Current_m1_6534(__this, method) (( CustomAttributeTypedArgument_t1_332  (*) (InternalEnumerator_1_t1_1070 *, const MethodInfo*))InternalEnumerator_1_get_Current_m1_6534_gshared)(__this, method)
