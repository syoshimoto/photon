﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.GenericEqualityComparer`1<System.DateTimeOffset>
struct GenericEqualityComparer_1_t1_874;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_DateTimeOffset.h"

// System.Void System.Collections.Generic.GenericEqualityComparer`1<System.DateTimeOffset>::.ctor()
extern "C" void GenericEqualityComparer_1__ctor_m1_5534_gshared (GenericEqualityComparer_1_t1_874 * __this, const MethodInfo* method);
#define GenericEqualityComparer_1__ctor_m1_5534(__this, method) (( void (*) (GenericEqualityComparer_1_t1_874 *, const MethodInfo*))GenericEqualityComparer_1__ctor_m1_5534_gshared)(__this, method)
// System.Int32 System.Collections.Generic.GenericEqualityComparer`1<System.DateTimeOffset>::GetHashCode(T)
extern "C" int32_t GenericEqualityComparer_1_GetHashCode_m1_6970_gshared (GenericEqualityComparer_1_t1_874 * __this, DateTimeOffset_t1_707  ___obj, const MethodInfo* method);
#define GenericEqualityComparer_1_GetHashCode_m1_6970(__this, ___obj, method) (( int32_t (*) (GenericEqualityComparer_1_t1_874 *, DateTimeOffset_t1_707 , const MethodInfo*))GenericEqualityComparer_1_GetHashCode_m1_6970_gshared)(__this, ___obj, method)
// System.Boolean System.Collections.Generic.GenericEqualityComparer`1<System.DateTimeOffset>::Equals(T,T)
extern "C" bool GenericEqualityComparer_1_Equals_m1_6971_gshared (GenericEqualityComparer_1_t1_874 * __this, DateTimeOffset_t1_707  ___x, DateTimeOffset_t1_707  ___y, const MethodInfo* method);
#define GenericEqualityComparer_1_Equals_m1_6971(__this, ___x, ___y, method) (( bool (*) (GenericEqualityComparer_1_t1_874 *, DateTimeOffset_t1_707 , DateTimeOffset_t1_707 , const MethodInfo*))GenericEqualityComparer_1_Equals_m1_6971_gshared)(__this, ___x, ___y, method)
