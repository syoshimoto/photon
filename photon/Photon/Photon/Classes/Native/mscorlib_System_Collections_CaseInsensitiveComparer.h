﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Collections.CaseInsensitiveComparer
struct CaseInsensitiveComparer_t1_161;
// System.Globalization.CultureInfo
struct CultureInfo_t1_162;

#include "mscorlib_System_Object.h"

// System.Collections.CaseInsensitiveComparer
struct  CaseInsensitiveComparer_t1_161  : public Object_t
{
	// System.Globalization.CultureInfo System.Collections.CaseInsensitiveComparer::culture
	CultureInfo_t1_162 * ___culture_2;
};
struct CaseInsensitiveComparer_t1_161_StaticFields{
	// System.Collections.CaseInsensitiveComparer System.Collections.CaseInsensitiveComparer::defaultComparer
	CaseInsensitiveComparer_t1_161 * ___defaultComparer_0;
	// System.Collections.CaseInsensitiveComparer System.Collections.CaseInsensitiveComparer::defaultInvariantComparer
	CaseInsensitiveComparer_t1_161 * ___defaultInvariantComparer_1;
};
