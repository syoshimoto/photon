﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Collections.Generic.Comparer`1<System.DateTime>
struct Comparer_1_t1_1104;

#include "mscorlib_System_Object.h"

// System.Collections.Generic.Comparer`1<System.DateTime>
struct  Comparer_1_t1_1104  : public Object_t
{
};
struct Comparer_1_t1_1104_StaticFields{
	// System.Collections.Generic.Comparer`1<T> System.Collections.Generic.Comparer`1::_default
	Comparer_1_t1_1104 * ____default_0;
};
