﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_KeyCollecti_15MethodDeclarations.h"

// System.Void System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Int32,ExitGames.Client.Photon.NCommand>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
#define Enumerator__ctor_m1_7867(__this, ___host, method) (( void (*) (Enumerator_t1_898 *, Dictionary_2_t1_885 *, const MethodInfo*))Enumerator__ctor_m1_7478_gshared)(__this, ___host, method)
// System.Object System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Int32,ExitGames.Client.Photon.NCommand>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m1_7868(__this, method) (( Object_t * (*) (Enumerator_t1_898 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m1_7479_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Int32,ExitGames.Client.Photon.NCommand>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m1_7869(__this, method) (( void (*) (Enumerator_t1_898 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m1_7480_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Int32,ExitGames.Client.Photon.NCommand>::Dispose()
#define Enumerator_Dispose_m1_5569(__this, method) (( void (*) (Enumerator_t1_898 *, const MethodInfo*))Enumerator_Dispose_m1_7481_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Int32,ExitGames.Client.Photon.NCommand>::MoveNext()
#define Enumerator_MoveNext_m1_5568(__this, method) (( bool (*) (Enumerator_t1_898 *, const MethodInfo*))Enumerator_MoveNext_m1_7482_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Int32,ExitGames.Client.Photon.NCommand>::get_Current()
#define Enumerator_get_Current_m1_5567(__this, method) (( int32_t (*) (Enumerator_t1_898 *, const MethodInfo*))Enumerator_get_Current_m1_7483_gshared)(__this, method)
