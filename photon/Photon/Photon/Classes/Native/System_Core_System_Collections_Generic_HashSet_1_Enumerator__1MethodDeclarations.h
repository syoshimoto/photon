﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.HashSet`1<System.Object>
struct HashSet_1_t2_20;
// System.Object
struct Object_t;

#include "codegen/il2cpp-codegen.h"
#include "System_Core_System_Collections_Generic_HashSet_1_Enumerator__1.h"

// System.Void System.Collections.Generic.HashSet`1/Enumerator<System.Object>::.ctor(System.Collections.Generic.HashSet`1<T>)
extern "C" void Enumerator__ctor_m2_86_gshared (Enumerator_t2_23 * __this, HashSet_1_t2_20 * ___hashset, const MethodInfo* method);
#define Enumerator__ctor_m2_86(__this, ___hashset, method) (( void (*) (Enumerator_t2_23 *, HashSet_1_t2_20 *, const MethodInfo*))Enumerator__ctor_m2_86_gshared)(__this, ___hashset, method)
// System.Object System.Collections.Generic.HashSet`1/Enumerator<System.Object>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * Enumerator_System_Collections_IEnumerator_get_Current_m2_87_gshared (Enumerator_t2_23 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m2_87(__this, method) (( Object_t * (*) (Enumerator_t2_23 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m2_87_gshared)(__this, method)
// System.Void System.Collections.Generic.HashSet`1/Enumerator<System.Object>::System.Collections.IEnumerator.Reset()
extern "C" void Enumerator_System_Collections_IEnumerator_Reset_m2_88_gshared (Enumerator_t2_23 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_Reset_m2_88(__this, method) (( void (*) (Enumerator_t2_23 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m2_88_gshared)(__this, method)
// System.Boolean System.Collections.Generic.HashSet`1/Enumerator<System.Object>::MoveNext()
extern "C" bool Enumerator_MoveNext_m2_89_gshared (Enumerator_t2_23 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m2_89(__this, method) (( bool (*) (Enumerator_t2_23 *, const MethodInfo*))Enumerator_MoveNext_m2_89_gshared)(__this, method)
// T System.Collections.Generic.HashSet`1/Enumerator<System.Object>::get_Current()
extern "C" Object_t * Enumerator_get_Current_m2_90_gshared (Enumerator_t2_23 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m2_90(__this, method) (( Object_t * (*) (Enumerator_t2_23 *, const MethodInfo*))Enumerator_get_Current_m2_90_gshared)(__this, method)
// System.Void System.Collections.Generic.HashSet`1/Enumerator<System.Object>::Dispose()
extern "C" void Enumerator_Dispose_m2_91_gshared (Enumerator_t2_23 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m2_91(__this, method) (( void (*) (Enumerator_t2_23 *, const MethodInfo*))Enumerator_Dispose_m2_91_gshared)(__this, method)
// System.Void System.Collections.Generic.HashSet`1/Enumerator<System.Object>::CheckState()
extern "C" void Enumerator_CheckState_m2_92_gshared (Enumerator_t2_23 * __this, const MethodInfo* method);
#define Enumerator_CheckState_m2_92(__this, method) (( void (*) (Enumerator_t2_23 *, const MethodInfo*))Enumerator_CheckState_m2_92_gshared)(__this, method)
