﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ExitGames.Client.GUI.GizmoTypeDrawer
struct GizmoTypeDrawer_t8_76;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Vector3.h"
#include "AssemblyU2DCSharp_ExitGames_Client_GUI_GizmoType.h"
#include "UnityEngine_UnityEngine_Color.h"

// System.Void ExitGames.Client.GUI.GizmoTypeDrawer::.ctor()
extern "C" void GizmoTypeDrawer__ctor_m8_313 (GizmoTypeDrawer_t8_76 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ExitGames.Client.GUI.GizmoTypeDrawer::Draw(UnityEngine.Vector3,ExitGames.Client.GUI.GizmoType,UnityEngine.Color,System.Single)
extern "C" void GizmoTypeDrawer_Draw_m8_314 (Object_t * __this /* static, unused */, Vector3_t6_49  ___center, int32_t ___type, Color_t6_40  ___color, float ___size, const MethodInfo* method) IL2CPP_METHOD_ATTR;
