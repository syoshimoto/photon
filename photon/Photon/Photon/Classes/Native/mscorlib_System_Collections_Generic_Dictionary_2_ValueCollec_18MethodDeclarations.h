﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2/ValueCollection<System.Byte,System.Object>
struct ValueCollection_t1_1179;
// System.Collections.Generic.Dictionary`2<System.Byte,System.Object>
struct Dictionary_2_t1_888;
// System.Object
struct Object_t;
// System.Collections.Generic.IEnumerator`1<System.Object>
struct IEnumerator_1_t1_1402;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t1_130;
// System.Object[]
struct ObjectU5BU5D_t1_157;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_ValueCollec_19.h"

// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<System.Byte,System.Object>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C" void ValueCollection__ctor_m1_7676_gshared (ValueCollection_t1_1179 * __this, Dictionary_2_t1_888 * ___dictionary, const MethodInfo* method);
#define ValueCollection__ctor_m1_7676(__this, ___dictionary, method) (( void (*) (ValueCollection_t1_1179 *, Dictionary_2_t1_888 *, const MethodInfo*))ValueCollection__ctor_m1_7676_gshared)(__this, ___dictionary, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<System.Byte,System.Object>::System.Collections.Generic.ICollection<TValue>.Add(TValue)
extern "C" void ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m1_7677_gshared (ValueCollection_t1_1179 * __this, Object_t * ___item, const MethodInfo* method);
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m1_7677(__this, ___item, method) (( void (*) (ValueCollection_t1_1179 *, Object_t *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m1_7677_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<System.Byte,System.Object>::System.Collections.Generic.ICollection<TValue>.Clear()
extern "C" void ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m1_7678_gshared (ValueCollection_t1_1179 * __this, const MethodInfo* method);
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m1_7678(__this, method) (( void (*) (ValueCollection_t1_1179 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m1_7678_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<System.Byte,System.Object>::System.Collections.Generic.ICollection<TValue>.Contains(TValue)
extern "C" bool ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m1_7679_gshared (ValueCollection_t1_1179 * __this, Object_t * ___item, const MethodInfo* method);
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m1_7679(__this, ___item, method) (( bool (*) (ValueCollection_t1_1179 *, Object_t *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m1_7679_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<System.Byte,System.Object>::System.Collections.Generic.ICollection<TValue>.Remove(TValue)
extern "C" bool ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m1_7680_gshared (ValueCollection_t1_1179 * __this, Object_t * ___item, const MethodInfo* method);
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m1_7680(__this, ___item, method) (( bool (*) (ValueCollection_t1_1179 *, Object_t *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m1_7680_gshared)(__this, ___item, method)
// System.Collections.Generic.IEnumerator`1<TValue> System.Collections.Generic.Dictionary`2/ValueCollection<System.Byte,System.Object>::System.Collections.Generic.IEnumerable<TValue>.GetEnumerator()
extern "C" Object_t* ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m1_7681_gshared (ValueCollection_t1_1179 * __this, const MethodInfo* method);
#define ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m1_7681(__this, method) (( Object_t* (*) (ValueCollection_t1_1179 *, const MethodInfo*))ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m1_7681_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<System.Byte,System.Object>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C" void ValueCollection_System_Collections_ICollection_CopyTo_m1_7682_gshared (ValueCollection_t1_1179 * __this, Array_t * ___array, int32_t ___index, const MethodInfo* method);
#define ValueCollection_System_Collections_ICollection_CopyTo_m1_7682(__this, ___array, ___index, method) (( void (*) (ValueCollection_t1_1179 *, Array_t *, int32_t, const MethodInfo*))ValueCollection_System_Collections_ICollection_CopyTo_m1_7682_gshared)(__this, ___array, ___index, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2/ValueCollection<System.Byte,System.Object>::System.Collections.IEnumerable.GetEnumerator()
extern "C" Object_t * ValueCollection_System_Collections_IEnumerable_GetEnumerator_m1_7683_gshared (ValueCollection_t1_1179 * __this, const MethodInfo* method);
#define ValueCollection_System_Collections_IEnumerable_GetEnumerator_m1_7683(__this, method) (( Object_t * (*) (ValueCollection_t1_1179 *, const MethodInfo*))ValueCollection_System_Collections_IEnumerable_GetEnumerator_m1_7683_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<System.Byte,System.Object>::System.Collections.Generic.ICollection<TValue>.get_IsReadOnly()
extern "C" bool ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m1_7684_gshared (ValueCollection_t1_1179 * __this, const MethodInfo* method);
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m1_7684(__this, method) (( bool (*) (ValueCollection_t1_1179 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m1_7684_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ValueCollection<System.Byte,System.Object>::System.Collections.ICollection.get_SyncRoot()
extern "C" Object_t * ValueCollection_System_Collections_ICollection_get_SyncRoot_m1_7685_gshared (ValueCollection_t1_1179 * __this, const MethodInfo* method);
#define ValueCollection_System_Collections_ICollection_get_SyncRoot_m1_7685(__this, method) (( Object_t * (*) (ValueCollection_t1_1179 *, const MethodInfo*))ValueCollection_System_Collections_ICollection_get_SyncRoot_m1_7685_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<System.Byte,System.Object>::CopyTo(TValue[],System.Int32)
extern "C" void ValueCollection_CopyTo_m1_7686_gshared (ValueCollection_t1_1179 * __this, ObjectU5BU5D_t1_157* ___array, int32_t ___index, const MethodInfo* method);
#define ValueCollection_CopyTo_m1_7686(__this, ___array, ___index, method) (( void (*) (ValueCollection_t1_1179 *, ObjectU5BU5D_t1_157*, int32_t, const MethodInfo*))ValueCollection_CopyTo_m1_7686_gshared)(__this, ___array, ___index, method)
// System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2/ValueCollection<System.Byte,System.Object>::GetEnumerator()
extern "C" Enumerator_t1_1180  ValueCollection_GetEnumerator_m1_7687_gshared (ValueCollection_t1_1179 * __this, const MethodInfo* method);
#define ValueCollection_GetEnumerator_m1_7687(__this, method) (( Enumerator_t1_1180  (*) (ValueCollection_t1_1179 *, const MethodInfo*))ValueCollection_GetEnumerator_m1_7687_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2/ValueCollection<System.Byte,System.Object>::get_Count()
extern "C" int32_t ValueCollection_get_Count_m1_7688_gshared (ValueCollection_t1_1179 * __this, const MethodInfo* method);
#define ValueCollection_get_Count_m1_7688(__this, method) (( int32_t (*) (ValueCollection_t1_1179 *, const MethodInfo*))ValueCollection_get_Count_m1_7688_gshared)(__this, method)
