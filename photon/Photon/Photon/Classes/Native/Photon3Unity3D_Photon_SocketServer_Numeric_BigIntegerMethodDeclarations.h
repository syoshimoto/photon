﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Photon.SocketServer.Numeric.BigInteger
struct BigInteger_t5_3;
// System.Byte[]
struct ByteU5BU5D_t1_71;
// System.UInt32[]
struct UInt32U5BU5D_t1_97;
// System.Object
struct Object_t;
// System.String
struct String_t;
// System.Random
struct Random_t1_757;

#include "codegen/il2cpp-codegen.h"

// System.Void Photon.SocketServer.Numeric.BigInteger::.ctor()
extern "C" void BigInteger__ctor_m5_12 (BigInteger_t5_3 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Photon.SocketServer.Numeric.BigInteger::.ctor(System.Int64)
extern "C" void BigInteger__ctor_m5_13 (BigInteger_t5_3 * __this, int64_t ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Photon.SocketServer.Numeric.BigInteger::.ctor(Photon.SocketServer.Numeric.BigInteger)
extern "C" void BigInteger__ctor_m5_14 (BigInteger_t5_3 * __this, BigInteger_t5_3 * ___bi, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Photon.SocketServer.Numeric.BigInteger::.ctor(System.Byte[])
extern "C" void BigInteger__ctor_m5_15 (BigInteger_t5_3 * __this, ByteU5BU5D_t1_71* ___inData, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Photon.SocketServer.Numeric.BigInteger::.ctor(System.UInt32[])
extern "C" void BigInteger__ctor_m5_16 (BigInteger_t5_3 * __this, UInt32U5BU5D_t1_97* ___inData, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Photon.SocketServer.Numeric.BigInteger Photon.SocketServer.Numeric.BigInteger::op_Implicit(System.Int64)
extern "C" BigInteger_t5_3 * BigInteger_op_Implicit_m5_17 (Object_t * __this /* static, unused */, int64_t ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Photon.SocketServer.Numeric.BigInteger Photon.SocketServer.Numeric.BigInteger::op_Implicit(System.Int32)
extern "C" BigInteger_t5_3 * BigInteger_op_Implicit_m5_18 (Object_t * __this /* static, unused */, int32_t ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Photon.SocketServer.Numeric.BigInteger Photon.SocketServer.Numeric.BigInteger::op_Addition(Photon.SocketServer.Numeric.BigInteger,Photon.SocketServer.Numeric.BigInteger)
extern "C" BigInteger_t5_3 * BigInteger_op_Addition_m5_19 (Object_t * __this /* static, unused */, BigInteger_t5_3 * ___bi1, BigInteger_t5_3 * ___bi2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Photon.SocketServer.Numeric.BigInteger Photon.SocketServer.Numeric.BigInteger::op_Subtraction(Photon.SocketServer.Numeric.BigInteger,Photon.SocketServer.Numeric.BigInteger)
extern "C" BigInteger_t5_3 * BigInteger_op_Subtraction_m5_20 (Object_t * __this /* static, unused */, BigInteger_t5_3 * ___bi1, BigInteger_t5_3 * ___bi2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Photon.SocketServer.Numeric.BigInteger Photon.SocketServer.Numeric.BigInteger::op_Multiply(Photon.SocketServer.Numeric.BigInteger,Photon.SocketServer.Numeric.BigInteger)
extern "C" BigInteger_t5_3 * BigInteger_op_Multiply_m5_21 (Object_t * __this /* static, unused */, BigInteger_t5_3 * ___bi1, BigInteger_t5_3 * ___bi2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Photon.SocketServer.Numeric.BigInteger Photon.SocketServer.Numeric.BigInteger::op_LeftShift(Photon.SocketServer.Numeric.BigInteger,System.Int32)
extern "C" BigInteger_t5_3 * BigInteger_op_LeftShift_m5_22 (Object_t * __this /* static, unused */, BigInteger_t5_3 * ___bi1, int32_t ___shiftVal, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Photon.SocketServer.Numeric.BigInteger::shiftLeft(System.UInt32[],System.Int32)
extern "C" int32_t BigInteger_shiftLeft_m5_23 (Object_t * __this /* static, unused */, UInt32U5BU5D_t1_97* ___buffer, int32_t ___shiftVal, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Photon.SocketServer.Numeric.BigInteger::shiftRight(System.UInt32[],System.Int32)
extern "C" int32_t BigInteger_shiftRight_m5_24 (Object_t * __this /* static, unused */, UInt32U5BU5D_t1_97* ___buffer, int32_t ___shiftVal, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Photon.SocketServer.Numeric.BigInteger Photon.SocketServer.Numeric.BigInteger::op_UnaryNegation(Photon.SocketServer.Numeric.BigInteger)
extern "C" BigInteger_t5_3 * BigInteger_op_UnaryNegation_m5_25 (Object_t * __this /* static, unused */, BigInteger_t5_3 * ___bi1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Photon.SocketServer.Numeric.BigInteger::op_Equality(Photon.SocketServer.Numeric.BigInteger,Photon.SocketServer.Numeric.BigInteger)
extern "C" bool BigInteger_op_Equality_m5_26 (Object_t * __this /* static, unused */, BigInteger_t5_3 * ___bi1, BigInteger_t5_3 * ___bi2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Photon.SocketServer.Numeric.BigInteger::Equals(System.Object)
extern "C" bool BigInteger_Equals_m5_27 (BigInteger_t5_3 * __this, Object_t * ___o, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Photon.SocketServer.Numeric.BigInteger::GetHashCode()
extern "C" int32_t BigInteger_GetHashCode_m5_28 (BigInteger_t5_3 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Photon.SocketServer.Numeric.BigInteger::op_GreaterThan(Photon.SocketServer.Numeric.BigInteger,Photon.SocketServer.Numeric.BigInteger)
extern "C" bool BigInteger_op_GreaterThan_m5_29 (Object_t * __this /* static, unused */, BigInteger_t5_3 * ___bi1, BigInteger_t5_3 * ___bi2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Photon.SocketServer.Numeric.BigInteger::op_LessThan(Photon.SocketServer.Numeric.BigInteger,Photon.SocketServer.Numeric.BigInteger)
extern "C" bool BigInteger_op_LessThan_m5_30 (Object_t * __this /* static, unused */, BigInteger_t5_3 * ___bi1, BigInteger_t5_3 * ___bi2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Photon.SocketServer.Numeric.BigInteger::op_GreaterThanOrEqual(Photon.SocketServer.Numeric.BigInteger,Photon.SocketServer.Numeric.BigInteger)
extern "C" bool BigInteger_op_GreaterThanOrEqual_m5_31 (Object_t * __this /* static, unused */, BigInteger_t5_3 * ___bi1, BigInteger_t5_3 * ___bi2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Photon.SocketServer.Numeric.BigInteger::multiByteDivide(Photon.SocketServer.Numeric.BigInteger,Photon.SocketServer.Numeric.BigInteger,Photon.SocketServer.Numeric.BigInteger,Photon.SocketServer.Numeric.BigInteger)
extern "C" void BigInteger_multiByteDivide_m5_32 (Object_t * __this /* static, unused */, BigInteger_t5_3 * ___bi1, BigInteger_t5_3 * ___bi2, BigInteger_t5_3 * ___outQuotient, BigInteger_t5_3 * ___outRemainder, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Photon.SocketServer.Numeric.BigInteger::singleByteDivide(Photon.SocketServer.Numeric.BigInteger,Photon.SocketServer.Numeric.BigInteger,Photon.SocketServer.Numeric.BigInteger,Photon.SocketServer.Numeric.BigInteger)
extern "C" void BigInteger_singleByteDivide_m5_33 (Object_t * __this /* static, unused */, BigInteger_t5_3 * ___bi1, BigInteger_t5_3 * ___bi2, BigInteger_t5_3 * ___outQuotient, BigInteger_t5_3 * ___outRemainder, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Photon.SocketServer.Numeric.BigInteger Photon.SocketServer.Numeric.BigInteger::op_Division(Photon.SocketServer.Numeric.BigInteger,Photon.SocketServer.Numeric.BigInteger)
extern "C" BigInteger_t5_3 * BigInteger_op_Division_m5_34 (Object_t * __this /* static, unused */, BigInteger_t5_3 * ___bi1, BigInteger_t5_3 * ___bi2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Photon.SocketServer.Numeric.BigInteger Photon.SocketServer.Numeric.BigInteger::op_Modulus(Photon.SocketServer.Numeric.BigInteger,Photon.SocketServer.Numeric.BigInteger)
extern "C" BigInteger_t5_3 * BigInteger_op_Modulus_m5_35 (Object_t * __this /* static, unused */, BigInteger_t5_3 * ___bi1, BigInteger_t5_3 * ___bi2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Photon.SocketServer.Numeric.BigInteger::ToString()
extern "C" String_t* BigInteger_ToString_m5_36 (BigInteger_t5_3 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Photon.SocketServer.Numeric.BigInteger::ToString(System.Int32)
extern "C" String_t* BigInteger_ToString_m5_37 (BigInteger_t5_3 * __this, int32_t ___radix, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Photon.SocketServer.Numeric.BigInteger Photon.SocketServer.Numeric.BigInteger::ModPow(Photon.SocketServer.Numeric.BigInteger,Photon.SocketServer.Numeric.BigInteger)
extern "C" BigInteger_t5_3 * BigInteger_ModPow_m5_38 (BigInteger_t5_3 * __this, BigInteger_t5_3 * ___exp, BigInteger_t5_3 * ___n, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Photon.SocketServer.Numeric.BigInteger Photon.SocketServer.Numeric.BigInteger::BarrettReduction(Photon.SocketServer.Numeric.BigInteger,Photon.SocketServer.Numeric.BigInteger,Photon.SocketServer.Numeric.BigInteger)
extern "C" BigInteger_t5_3 * BigInteger_BarrettReduction_m5_39 (BigInteger_t5_3 * __this, BigInteger_t5_3 * ___x, BigInteger_t5_3 * ___n, BigInteger_t5_3 * ___constant, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Photon.SocketServer.Numeric.BigInteger Photon.SocketServer.Numeric.BigInteger::GenerateRandom(System.Int32)
extern "C" BigInteger_t5_3 * BigInteger_GenerateRandom_m5_40 (Object_t * __this /* static, unused */, int32_t ___bits, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Photon.SocketServer.Numeric.BigInteger::genRandomBits(System.Int32,System.Random)
extern "C" void BigInteger_genRandomBits_m5_41 (BigInteger_t5_3 * __this, int32_t ___bits, Random_t1_757 * ___rand, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Photon.SocketServer.Numeric.BigInteger::bitCount()
extern "C" int32_t BigInteger_bitCount_m5_42 (BigInteger_t5_3 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] Photon.SocketServer.Numeric.BigInteger::GetBytes()
extern "C" ByteU5BU5D_t1_71* BigInteger_GetBytes_m5_43 (BigInteger_t5_3 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Photon.SocketServer.Numeric.BigInteger::.cctor()
extern "C" void BigInteger__cctor_m5_44 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
