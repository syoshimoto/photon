﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// RPGCamera
struct RPGCamera_t8_37;

#include "UnityEngine_UnityEngine_MonoBehaviour.h"

// DemoRPGMovement
struct  DemoRPGMovement_t8_36  : public MonoBehaviour_t6_80
{
	// RPGCamera DemoRPGMovement::Camera
	RPGCamera_t8_37 * ___Camera_2;
};
