﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ExitGames.Client.Photon.PeerBase
struct PeerBase_t5_10;
// System.String
struct String_t;
// ExitGames.Client.Photon.IPhotonPeerListener
struct IPhotonPeerListener_t5_17;
// ExitGames.Client.Photon.NetworkSimulationSet
struct NetworkSimulationSet_t5_16;
// System.Collections.Generic.Dictionary`2<System.Byte,System.Object>
struct Dictionary_2_t1_888;
// ExitGames.Client.Photon.OperationResponse
struct OperationResponse_t5_43;
// ExitGames.Client.Photon.PeerBase/MyAction
struct MyAction_t5_6;
// System.Byte[]
struct ByteU5BU5D_t1_71;

#include "codegen/il2cpp-codegen.h"
#include "Photon3Unity3D_ExitGames_Client_Photon_DebugLevel.h"
#include "Photon3Unity3D_ExitGames_Client_Photon_StatusCode.h"

// System.Int64 ExitGames.Client.Photon.PeerBase::get_TrafficStatsEnabledTime()
extern "C" int64_t PeerBase_get_TrafficStatsEnabledTime_m5_71 (PeerBase_t5_10 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ExitGames.Client.Photon.PeerBase::get_TrafficStatsEnabled()
extern "C" bool PeerBase_get_TrafficStatsEnabled_m5_72 (PeerBase_t5_10 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ExitGames.Client.Photon.PeerBase::set_TrafficStatsEnabled(System.Boolean)
extern "C" void PeerBase_set_TrafficStatsEnabled_m5_73 (PeerBase_t5_10 * __this, bool ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String ExitGames.Client.Photon.PeerBase::get_ServerAddress()
extern "C" String_t* PeerBase_get_ServerAddress_m5_74 (PeerBase_t5_10 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ExitGames.Client.Photon.PeerBase::set_ServerAddress(System.String)
extern "C" void PeerBase_set_ServerAddress_m5_75 (PeerBase_t5_10 * __this, String_t* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ExitGames.Client.Photon.IPhotonPeerListener ExitGames.Client.Photon.PeerBase::get_Listener()
extern "C" Object_t * PeerBase_get_Listener_m5_76 (PeerBase_t5_10 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ExitGames.Client.Photon.PeerBase::set_Listener(ExitGames.Client.Photon.IPhotonPeerListener)
extern "C" void PeerBase_set_Listener_m5_77 (PeerBase_t5_10 * __this, Object_t * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte ExitGames.Client.Photon.PeerBase::get_QuickResendAttempts()
extern "C" uint8_t PeerBase_get_QuickResendAttempts_m5_78 (PeerBase_t5_10 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ExitGames.Client.Photon.PeerBase::set_QuickResendAttempts(System.Byte)
extern "C" void PeerBase_set_QuickResendAttempts_m5_79 (PeerBase_t5_10 * __this, uint8_t ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ExitGames.Client.Photon.NetworkSimulationSet ExitGames.Client.Photon.PeerBase::get_NetworkSimulationSettings()
extern "C" NetworkSimulationSet_t5_16 * PeerBase_get_NetworkSimulationSettings_m5_80 (PeerBase_t5_10 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ExitGames.Client.Photon.PeerBase::CommandLogResize()
extern "C" void PeerBase_CommandLogResize_m5_81 (PeerBase_t5_10 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ExitGames.Client.Photon.PeerBase::CommandLogInit()
extern "C" void PeerBase_CommandLogInit_m5_82 (PeerBase_t5_10 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ExitGames.Client.Photon.PeerBase::InitOnce()
extern "C" void PeerBase_InitOnce_m5_83 (PeerBase_t5_10 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ExitGames.Client.Photon.PeerBase::EnqueueOperation(System.Collections.Generic.Dictionary`2<System.Byte,System.Object>,System.Byte,System.Boolean,System.Byte,System.Boolean)
extern "C" bool PeerBase_EnqueueOperation_m5_84 (PeerBase_t5_10 * __this, Dictionary_2_t1_888 * ___parameters, uint8_t ___opCode, bool ___sendReliable, uint8_t ___channelId, bool ___encrypted, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ExitGames.Client.Photon.PeerBase::SendAcksOnly()
extern "C" bool PeerBase_SendAcksOnly_m5_85 (PeerBase_t5_10 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ExitGames.Client.Photon.PeerBase::InitCallback()
extern "C" void PeerBase_InitCallback_m5_86 (PeerBase_t5_10 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ExitGames.Client.Photon.PeerBase::get_IsSendingOnlyAcks()
extern "C" bool PeerBase_get_IsSendingOnlyAcks_m5_87 (PeerBase_t5_10 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ExitGames.Client.Photon.PeerBase::set_IsSendingOnlyAcks(System.Boolean)
extern "C" void PeerBase_set_IsSendingOnlyAcks_m5_88 (PeerBase_t5_10 * __this, bool ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ExitGames.Client.Photon.PeerBase::ExchangeKeysForEncryption()
extern "C" bool PeerBase_ExchangeKeysForEncryption_m5_89 (PeerBase_t5_10 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ExitGames.Client.Photon.PeerBase::DeriveSharedKey(ExitGames.Client.Photon.OperationResponse)
extern "C" void PeerBase_DeriveSharedKey_m5_90 (PeerBase_t5_10 * __this, OperationResponse_t5_43 * ___operationResponse, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ExitGames.Client.Photon.PeerBase::EnqueueActionForDispatch(ExitGames.Client.Photon.PeerBase/MyAction)
extern "C" void PeerBase_EnqueueActionForDispatch_m5_91 (PeerBase_t5_10 * __this, MyAction_t5_6 * ___action, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ExitGames.Client.Photon.PeerBase::EnqueueDebugReturn(ExitGames.Client.Photon.DebugLevel,System.String)
extern "C" void PeerBase_EnqueueDebugReturn_m5_92 (PeerBase_t5_10 * __this, uint8_t ___level, String_t* ___debugReturn, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ExitGames.Client.Photon.PeerBase::EnqueueStatusCallback(ExitGames.Client.Photon.StatusCode)
extern "C" void PeerBase_EnqueueStatusCallback_m5_93 (PeerBase_t5_10 * __this, int32_t ___statusValue, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ExitGames.Client.Photon.PeerBase::InitPeerBase()
extern "C" void PeerBase_InitPeerBase_m5_94 (PeerBase_t5_10 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ExitGames.Client.Photon.PeerBase::DeserializeMessageAndCallback(System.Byte[])
extern "C" bool PeerBase_DeserializeMessageAndCallback_m5_95 (PeerBase_t5_10 * __this, ByteU5BU5D_t1_71* ___inBuff, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ExitGames.Client.Photon.PeerBase::SendNetworkSimulated(ExitGames.Client.Photon.PeerBase/MyAction)
extern "C" void PeerBase_SendNetworkSimulated_m5_96 (PeerBase_t5_10 * __this, MyAction_t5_6 * ___sendAction, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ExitGames.Client.Photon.PeerBase::ReceiveNetworkSimulated(ExitGames.Client.Photon.PeerBase/MyAction)
extern "C" void PeerBase_ReceiveNetworkSimulated_m5_97 (PeerBase_t5_10 * __this, MyAction_t5_6 * ___receiveAction, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ExitGames.Client.Photon.PeerBase::NetworkSimRun()
extern "C" void PeerBase_NetworkSimRun_m5_98 (PeerBase_t5_10 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ExitGames.Client.Photon.PeerBase::UpdateRoundTripTimeAndVariance(System.Int32)
extern "C" void PeerBase_UpdateRoundTripTimeAndVariance_m5_99 (PeerBase_t5_10 * __this, int32_t ___lastRoundtripTime, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ExitGames.Client.Photon.PeerBase::InitializeTrafficStats()
extern "C" void PeerBase_InitializeTrafficStats_m5_100 (PeerBase_t5_10 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ExitGames.Client.Photon.PeerBase::.ctor()
extern "C" void PeerBase__ctor_m5_101 (PeerBase_t5_10 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ExitGames.Client.Photon.PeerBase::.cctor()
extern "C" void PeerBase__cctor_m5_102 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
