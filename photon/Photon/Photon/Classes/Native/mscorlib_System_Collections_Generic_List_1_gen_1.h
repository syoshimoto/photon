﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// ExitGames.Client.Photon.NCommand[]
struct NCommandU5BU5D_t5_67;

#include "mscorlib_System_Object.h"

// System.Collections.Generic.List`1<ExitGames.Client.Photon.NCommand>
struct  List_1_t1_887  : public Object_t
{
	// T[] System.Collections.Generic.List`1::_items
	NCommandU5BU5D_t5_67* ____items_1;
	// System.Int32 System.Collections.Generic.List`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1::_version
	int32_t ____version_3;
};
struct List_1_t1_887_StaticFields{
	// T[] System.Collections.Generic.List`1::EmptyArray
	NCommandU5BU5D_t5_67* ___EmptyArray_4;
};
