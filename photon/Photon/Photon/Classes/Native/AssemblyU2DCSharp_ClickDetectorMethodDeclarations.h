﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ClickDetector
struct ClickDetector_t8_53;
// UnityEngine.GameObject
struct GameObject_t6_85;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Vector2.h"

// System.Void ClickDetector::.ctor()
extern "C" void ClickDetector__ctor_m8_242 (ClickDetector_t8_53 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ClickDetector::Update()
extern "C" void ClickDetector_Update_m8_243 (ClickDetector_t8_53 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.GameObject ClickDetector::RaycastObject(UnityEngine.Vector2)
extern "C" GameObject_t6_85 * ClickDetector_RaycastObject_m8_244 (ClickDetector_t8_53 * __this, Vector2_t6_48  ___screenPos, const MethodInfo* method) IL2CPP_METHOD_ATTR;
