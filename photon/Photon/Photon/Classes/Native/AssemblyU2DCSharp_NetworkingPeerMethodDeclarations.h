﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// NetworkingPeer
struct NetworkingPeer_t8_98;
// System.String
struct String_t;
// AuthenticationValues
struct AuthenticationValues_t8_97;
// System.Collections.Generic.List`1<Region>
struct List_1_t1_941;
// TypedLobby
struct TypedLobby_t8_79;
// Room
struct Room_t8_100;
// PhotonPlayer
struct PhotonPlayer_t8_102;
// ExitGames.Client.Photon.Hashtable
struct Hashtable_t5_1;
// ExitGames.Client.Photon.EventData
struct EventData_t5_44;
// PhotonPlayer[]
struct PhotonPlayerU5BU5D_t8_101;
// ExitGames.Client.Photon.OperationResponse
struct OperationResponse_t5_43;
// ExitGames.Client.Photon.LoadbalancingPeer/EnterRoomParams
struct EnterRoomParams_t8_77;
// ExitGames.Client.Photon.LoadbalancingPeer/OpJoinRandomRoomParams
struct OpJoinRandomRoomParams_t8_80;
// System.Object
struct Object_t;
// RaiseEventOptions
struct RaiseEventOptions_t8_93;
// System.String[]
struct StringU5BU5D_t1_202;
// System.Int32[]
struct Int32U5BU5D_t1_160;
// System.Object[]
struct ObjectU5BU5D_t1_157;
// System.Reflection.ParameterInfo[]
struct ParameterInfoU5BU5D_t1_812;
// System.Type[]
struct TypeU5BU5D_t1_31;
// UnityEngine.GameObject
struct GameObject_t6_85;
// PhotonView
struct PhotonView_t8_3;
// UnityEngine.MonoBehaviour
struct MonoBehaviour_t6_80;
// System.Reflection.MethodInfo
struct MethodInfo_t;

#include "codegen/il2cpp-codegen.h"
#include "Photon3Unity3D_ExitGames_Client_Photon_ConnectionProtocol.h"
#include "AssemblyU2DCSharp_ServerConnection.h"
#include "AssemblyU2DCSharp_PeerState.h"
#include "AssemblyU2DCSharp_CloudRegionCode.h"
#include "Photon3Unity3D_ExitGames_Client_Photon_DebugLevel.h"
#include "Photon3Unity3D_ExitGames_Client_Photon_StatusCode.h"
#include "AssemblyU2DCSharp_PhotonNetworkingMessage.h"
#include "UnityEngine_UnityEngine_Vector3.h"
#include "UnityEngine_UnityEngine_Quaternion.h"
#include "AssemblyU2DCSharp_PhotonTargets.h"

// System.Void NetworkingPeer::.ctor(System.String,ExitGames.Client.Photon.ConnectionProtocol)
extern "C" void NetworkingPeer__ctor_m8_367 (NetworkingPeer_t8_98 * __this, String_t* ___playername, uint8_t ___connectionProtocol, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NetworkingPeer::.cctor()
extern "C" void NetworkingPeer__cctor_m8_368 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String NetworkingPeer::get_mAppVersionPun()
extern "C" String_t* NetworkingPeer_get_mAppVersionPun_m8_369 (NetworkingPeer_t8_98 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// AuthenticationValues NetworkingPeer::get_CustomAuthenticationValues()
extern "C" AuthenticationValues_t8_97 * NetworkingPeer_get_CustomAuthenticationValues_m8_370 (NetworkingPeer_t8_98 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NetworkingPeer::set_CustomAuthenticationValues(AuthenticationValues)
extern "C" void NetworkingPeer_set_CustomAuthenticationValues_m8_371 (NetworkingPeer_t8_98 * __this, AuthenticationValues_t8_97 * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String NetworkingPeer::get_NameServerAddress()
extern "C" String_t* NetworkingPeer_get_NameServerAddress_m8_372 (NetworkingPeer_t8_98 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String NetworkingPeer::get_MasterServerAddress()
extern "C" String_t* NetworkingPeer_get_MasterServerAddress_m8_373 (NetworkingPeer_t8_98 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NetworkingPeer::set_MasterServerAddress(System.String)
extern "C" void NetworkingPeer_set_MasterServerAddress_m8_374 (NetworkingPeer_t8_98 * __this, String_t* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String NetworkingPeer::get_mGameserver()
extern "C" String_t* NetworkingPeer_get_mGameserver_m8_375 (NetworkingPeer_t8_98 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NetworkingPeer::set_mGameserver(System.String)
extern "C" void NetworkingPeer_set_mGameserver_m8_376 (NetworkingPeer_t8_98 * __this, String_t* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ServerConnection NetworkingPeer::get_server()
extern "C" int32_t NetworkingPeer_get_server_m8_377 (NetworkingPeer_t8_98 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NetworkingPeer::set_server(ServerConnection)
extern "C" void NetworkingPeer_set_server_m8_378 (NetworkingPeer_t8_98 * __this, int32_t ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// PeerState NetworkingPeer::get_State()
extern "C" int32_t NetworkingPeer_get_State_m8_379 (NetworkingPeer_t8_98 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NetworkingPeer::set_State(PeerState)
extern "C" void NetworkingPeer_set_State_m8_380 (NetworkingPeer_t8_98 * __this, int32_t ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean NetworkingPeer::get_IsUsingNameServer()
extern "C" bool NetworkingPeer_get_IsUsingNameServer_m8_381 (NetworkingPeer_t8_98 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NetworkingPeer::set_IsUsingNameServer(System.Boolean)
extern "C" void NetworkingPeer_set_IsUsingNameServer_m8_382 (NetworkingPeer_t8_98 * __this, bool ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean NetworkingPeer::get_IsAuthorizeSecretAvailable()
extern "C" bool NetworkingPeer_get_IsAuthorizeSecretAvailable_m8_383 (NetworkingPeer_t8_98 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<Region> NetworkingPeer::get_AvailableRegions()
extern "C" List_1_t1_941 * NetworkingPeer_get_AvailableRegions_m8_384 (NetworkingPeer_t8_98 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NetworkingPeer::set_AvailableRegions(System.Collections.Generic.List`1<Region>)
extern "C" void NetworkingPeer_set_AvailableRegions_m8_385 (NetworkingPeer_t8_98 * __this, List_1_t1_941 * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// CloudRegionCode NetworkingPeer::get_CloudRegion()
extern "C" int32_t NetworkingPeer_get_CloudRegion_m8_386 (NetworkingPeer_t8_98 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NetworkingPeer::set_CloudRegion(CloudRegionCode)
extern "C" void NetworkingPeer_set_CloudRegion_m8_387 (NetworkingPeer_t8_98 * __this, int32_t ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean NetworkingPeer::get_requestLobbyStatistics()
extern "C" bool NetworkingPeer_get_requestLobbyStatistics_m8_388 (NetworkingPeer_t8_98 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// TypedLobby NetworkingPeer::get_lobby()
extern "C" TypedLobby_t8_79 * NetworkingPeer_get_lobby_m8_389 (NetworkingPeer_t8_98 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NetworkingPeer::set_lobby(TypedLobby)
extern "C" void NetworkingPeer_set_lobby_m8_390 (NetworkingPeer_t8_98 * __this, TypedLobby_t8_79 * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 NetworkingPeer::get_mPlayersOnMasterCount()
extern "C" int32_t NetworkingPeer_get_mPlayersOnMasterCount_m8_391 (NetworkingPeer_t8_98 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NetworkingPeer::set_mPlayersOnMasterCount(System.Int32)
extern "C" void NetworkingPeer_set_mPlayersOnMasterCount_m8_392 (NetworkingPeer_t8_98 * __this, int32_t ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 NetworkingPeer::get_mGameCount()
extern "C" int32_t NetworkingPeer_get_mGameCount_m8_393 (NetworkingPeer_t8_98 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NetworkingPeer::set_mGameCount(System.Int32)
extern "C" void NetworkingPeer_set_mGameCount_m8_394 (NetworkingPeer_t8_98 * __this, int32_t ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 NetworkingPeer::get_mPlayersInRoomsCount()
extern "C" int32_t NetworkingPeer_get_mPlayersInRoomsCount_m8_395 (NetworkingPeer_t8_98 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NetworkingPeer::set_mPlayersInRoomsCount(System.Int32)
extern "C" void NetworkingPeer_set_mPlayersInRoomsCount_m8_396 (NetworkingPeer_t8_98 * __this, int32_t ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 NetworkingPeer::get_FriendsListAge()
extern "C" int32_t NetworkingPeer_get_FriendsListAge_m8_397 (NetworkingPeer_t8_98 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String NetworkingPeer::get_PlayerName()
extern "C" String_t* NetworkingPeer_get_PlayerName_m8_398 (NetworkingPeer_t8_98 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NetworkingPeer::set_PlayerName(System.String)
extern "C" void NetworkingPeer_set_PlayerName_m8_399 (NetworkingPeer_t8_98 * __this, String_t* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Room NetworkingPeer::get_CurrentGame()
extern "C" Room_t8_100 * NetworkingPeer_get_CurrentGame_m8_400 (NetworkingPeer_t8_98 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NetworkingPeer::set_CurrentGame(Room)
extern "C" void NetworkingPeer_set_CurrentGame_m8_401 (NetworkingPeer_t8_98 * __this, Room_t8_100 * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// PhotonPlayer NetworkingPeer::get_mLocalActor()
extern "C" PhotonPlayer_t8_102 * NetworkingPeer_get_mLocalActor_m8_402 (NetworkingPeer_t8_98 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NetworkingPeer::set_mLocalActor(PhotonPlayer)
extern "C" void NetworkingPeer_set_mLocalActor_m8_403 (NetworkingPeer_t8_98 * __this, PhotonPlayer_t8_102 * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 NetworkingPeer::get_mMasterClientId()
extern "C" int32_t NetworkingPeer_get_mMasterClientId_m8_404 (NetworkingPeer_t8_98 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NetworkingPeer::set_mMasterClientId(System.Int32)
extern "C" void NetworkingPeer_set_mMasterClientId_m8_405 (NetworkingPeer_t8_98 * __this, int32_t ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String NetworkingPeer::GetNameServerAddress()
extern "C" String_t* NetworkingPeer_GetNameServerAddress_m8_406 (NetworkingPeer_t8_98 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean NetworkingPeer::Connect(System.String,System.String)
extern "C" bool NetworkingPeer_Connect_m8_407 (NetworkingPeer_t8_98 * __this, String_t* ___serverAddress, String_t* ___applicationName, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean NetworkingPeer::Connect(System.String,ServerConnection)
extern "C" bool NetworkingPeer_Connect_m8_408 (NetworkingPeer_t8_98 * __this, String_t* ___serverAddress, int32_t ___type, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean NetworkingPeer::ConnectToNameServer()
extern "C" bool NetworkingPeer_ConnectToNameServer_m8_409 (NetworkingPeer_t8_98 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean NetworkingPeer::ConnectToRegionMaster(CloudRegionCode)
extern "C" bool NetworkingPeer_ConnectToRegionMaster_m8_410 (NetworkingPeer_t8_98 * __this, int32_t ___region, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean NetworkingPeer::GetRegions()
extern "C" bool NetworkingPeer_GetRegions_m8_411 (NetworkingPeer_t8_98 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NetworkingPeer::Disconnect()
extern "C" void NetworkingPeer_Disconnect_m8_412 (NetworkingPeer_t8_98 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NetworkingPeer::DisconnectToReconnect()
extern "C" void NetworkingPeer_DisconnectToReconnect_m8_413 (NetworkingPeer_t8_98 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NetworkingPeer::LeftLobbyCleanup()
extern "C" void NetworkingPeer_LeftLobbyCleanup_m8_414 (NetworkingPeer_t8_98 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NetworkingPeer::LeftRoomCleanup()
extern "C" void NetworkingPeer_LeftRoomCleanup_m8_415 (NetworkingPeer_t8_98 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NetworkingPeer::LocalCleanupAnythingInstantiated(System.Boolean)
extern "C" void NetworkingPeer_LocalCleanupAnythingInstantiated_m8_416 (NetworkingPeer_t8_98 * __this, bool ___destroyInstantiatedGameObjects, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NetworkingPeer::ReadoutProperties(ExitGames.Client.Photon.Hashtable,ExitGames.Client.Photon.Hashtable,System.Int32)
extern "C" void NetworkingPeer_ReadoutProperties_m8_417 (NetworkingPeer_t8_98 * __this, Hashtable_t5_1 * ___gameProperties, Hashtable_t5_1 * ___pActorProperties, int32_t ___targetActorNr, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NetworkingPeer::AddNewPlayer(System.Int32,PhotonPlayer)
extern "C" void NetworkingPeer_AddNewPlayer_m8_418 (NetworkingPeer_t8_98 * __this, int32_t ___ID, PhotonPlayer_t8_102 * ___player, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NetworkingPeer::RemovePlayer(System.Int32,PhotonPlayer)
extern "C" void NetworkingPeer_RemovePlayer_m8_419 (NetworkingPeer_t8_98 * __this, int32_t ___ID, PhotonPlayer_t8_102 * ___player, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NetworkingPeer::RebuildPlayerListCopies()
extern "C" void NetworkingPeer_RebuildPlayerListCopies_m8_420 (NetworkingPeer_t8_98 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NetworkingPeer::ResetPhotonViewsOnSerialize()
extern "C" void NetworkingPeer_ResetPhotonViewsOnSerialize_m8_421 (NetworkingPeer_t8_98 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NetworkingPeer::HandleEventLeave(System.Int32,ExitGames.Client.Photon.EventData)
extern "C" void NetworkingPeer_HandleEventLeave_m8_422 (NetworkingPeer_t8_98 * __this, int32_t ___actorID, EventData_t5_44 * ___evLeave, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NetworkingPeer::CheckMasterClient(System.Int32)
extern "C" void NetworkingPeer_CheckMasterClient_m8_423 (NetworkingPeer_t8_98 * __this, int32_t ___leavingPlayerId, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NetworkingPeer::UpdateMasterClient()
extern "C" void NetworkingPeer_UpdateMasterClient_m8_424 (NetworkingPeer_t8_98 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 NetworkingPeer::ReturnLowestPlayerId(PhotonPlayer[],System.Int32)
extern "C" int32_t NetworkingPeer_ReturnLowestPlayerId_m8_425 (Object_t * __this /* static, unused */, PhotonPlayerU5BU5D_t8_101* ___players, int32_t ___playerIdToIgnore, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean NetworkingPeer::SetMasterClient(System.Int32,System.Boolean)
extern "C" bool NetworkingPeer_SetMasterClient_m8_426 (NetworkingPeer_t8_98 * __this, int32_t ___playerId, bool ___sync, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean NetworkingPeer::SetMasterClient(System.Int32)
extern "C" bool NetworkingPeer_SetMasterClient_m8_427 (NetworkingPeer_t8_98 * __this, int32_t ___nextMasterId, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ExitGames.Client.Photon.Hashtable NetworkingPeer::GetActorPropertiesForActorNr(ExitGames.Client.Photon.Hashtable,System.Int32)
extern "C" Hashtable_t5_1 * NetworkingPeer_GetActorPropertiesForActorNr_m8_428 (NetworkingPeer_t8_98 * __this, Hashtable_t5_1 * ___actorProperties, int32_t ___actorNr, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// PhotonPlayer NetworkingPeer::GetPlayerWithId(System.Int32)
extern "C" PhotonPlayer_t8_102 * NetworkingPeer_GetPlayerWithId_m8_429 (NetworkingPeer_t8_98 * __this, int32_t ___number, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NetworkingPeer::SendPlayerName()
extern "C" void NetworkingPeer_SendPlayerName_m8_430 (NetworkingPeer_t8_98 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NetworkingPeer::GameEnteredOnGameServer(ExitGames.Client.Photon.OperationResponse)
extern "C" void NetworkingPeer_GameEnteredOnGameServer_m8_431 (NetworkingPeer_t8_98 * __this, OperationResponse_t5_43 * ___operationResponse, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ExitGames.Client.Photon.Hashtable NetworkingPeer::GetLocalActorProperties()
extern "C" Hashtable_t5_1 * NetworkingPeer_GetLocalActorProperties_m8_432 (NetworkingPeer_t8_98 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NetworkingPeer::ChangeLocalID(System.Int32)
extern "C" void NetworkingPeer_ChangeLocalID_m8_433 (NetworkingPeer_t8_98 * __this, int32_t ___newID, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean NetworkingPeer::OpCreateGame(ExitGames.Client.Photon.LoadbalancingPeer/EnterRoomParams)
extern "C" bool NetworkingPeer_OpCreateGame_m8_434 (NetworkingPeer_t8_98 * __this, EnterRoomParams_t8_77 * ___enterRoomParams, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean NetworkingPeer::OpJoinRoom(ExitGames.Client.Photon.LoadbalancingPeer/EnterRoomParams)
extern "C" bool NetworkingPeer_OpJoinRoom_m8_435 (NetworkingPeer_t8_98 * __this, EnterRoomParams_t8_77 * ___opParams, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean NetworkingPeer::OpJoinRandomRoom(ExitGames.Client.Photon.LoadbalancingPeer/OpJoinRandomRoomParams)
extern "C" bool NetworkingPeer_OpJoinRandomRoom_m8_436 (NetworkingPeer_t8_98 * __this, OpJoinRandomRoomParams_t8_80 * ___opJoinRandomRoomParams, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean NetworkingPeer::OpLeave()
extern "C" bool NetworkingPeer_OpLeave_m8_437 (NetworkingPeer_t8_98 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean NetworkingPeer::OpRaiseEvent(System.Byte,System.Object,System.Boolean,RaiseEventOptions)
extern "C" bool NetworkingPeer_OpRaiseEvent_m8_438 (NetworkingPeer_t8_98 * __this, uint8_t ___eventCode, Object_t * ___customEventContent, bool ___sendReliable, RaiseEventOptions_t8_93 * ___raiseEventOptions, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NetworkingPeer::DebugReturn(ExitGames.Client.Photon.DebugLevel,System.String)
extern "C" void NetworkingPeer_DebugReturn_m8_439 (NetworkingPeer_t8_98 * __this, uint8_t ___level, String_t* ___message, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NetworkingPeer::OnOperationResponse(ExitGames.Client.Photon.OperationResponse)
extern "C" void NetworkingPeer_OnOperationResponse_m8_440 (NetworkingPeer_t8_98 * __this, OperationResponse_t5_43 * ___operationResponse, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean NetworkingPeer::OpFindFriends(System.String[])
extern "C" bool NetworkingPeer_OpFindFriends_m8_441 (NetworkingPeer_t8_98 * __this, StringU5BU5D_t1_202* ___friendsToFind, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NetworkingPeer::OnStatusChanged(ExitGames.Client.Photon.StatusCode)
extern "C" void NetworkingPeer_OnStatusChanged_m8_442 (NetworkingPeer_t8_98 * __this, int32_t ___statusCode, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NetworkingPeer::OnEvent(ExitGames.Client.Photon.EventData)
extern "C" void NetworkingPeer_OnEvent_m8_443 (NetworkingPeer_t8_98 * __this, EventData_t5_44 * ___photonEvent, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NetworkingPeer::UpdatedActorList(System.Int32[])
extern "C" void NetworkingPeer_UpdatedActorList_m8_444 (NetworkingPeer_t8_98 * __this, Int32U5BU5D_t1_160* ___actorsInRoom, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NetworkingPeer::SendVacantViewIds()
extern "C" void NetworkingPeer_SendVacantViewIds_m8_445 (NetworkingPeer_t8_98 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NetworkingPeer::SendMonoMessage(PhotonNetworkingMessage,System.Object[])
extern "C" void NetworkingPeer_SendMonoMessage_m8_446 (Object_t * __this /* static, unused */, int32_t ___methodString, ObjectU5BU5D_t1_157* ___parameters, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NetworkingPeer::ExecuteRpc(ExitGames.Client.Photon.Hashtable,PhotonPlayer)
extern "C" void NetworkingPeer_ExecuteRpc_m8_447 (NetworkingPeer_t8_98 * __this, Hashtable_t5_1 * ___rpcData, PhotonPlayer_t8_102 * ___sender, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean NetworkingPeer::CheckTypeMatch(System.Reflection.ParameterInfo[],System.Type[])
extern "C" bool NetworkingPeer_CheckTypeMatch_m8_448 (NetworkingPeer_t8_98 * __this, ParameterInfoU5BU5D_t1_812* ___methodParameters, TypeU5BU5D_t1_31* ___callParameterTypes, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ExitGames.Client.Photon.Hashtable NetworkingPeer::SendInstantiate(System.String,UnityEngine.Vector3,UnityEngine.Quaternion,System.Int32,System.Int32[],System.Object[],System.Boolean)
extern "C" Hashtable_t5_1 * NetworkingPeer_SendInstantiate_m8_449 (NetworkingPeer_t8_98 * __this, String_t* ___prefabName, Vector3_t6_49  ___position, Quaternion_t6_51  ___rotation, int32_t ___group, Int32U5BU5D_t1_160* ___viewIDs, ObjectU5BU5D_t1_157* ___data, bool ___isGlobalObject, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.GameObject NetworkingPeer::DoInstantiate(ExitGames.Client.Photon.Hashtable,PhotonPlayer,UnityEngine.GameObject)
extern "C" GameObject_t6_85 * NetworkingPeer_DoInstantiate_m8_450 (NetworkingPeer_t8_98 * __this, Hashtable_t5_1 * ___evData, PhotonPlayer_t8_102 * ___photonPlayer, GameObject_t6_85 * ___resourceGameObject, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NetworkingPeer::StoreInstantiationData(System.Int32,System.Object[])
extern "C" void NetworkingPeer_StoreInstantiationData_m8_451 (NetworkingPeer_t8_98 * __this, int32_t ___instantiationId, ObjectU5BU5D_t1_157* ___instantiationData, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object[] NetworkingPeer::FetchInstantiationData(System.Int32)
extern "C" ObjectU5BU5D_t1_157* NetworkingPeer_FetchInstantiationData_m8_452 (NetworkingPeer_t8_98 * __this, int32_t ___instantiationId, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NetworkingPeer::RemoveInstantiationData(System.Int32)
extern "C" void NetworkingPeer_RemoveInstantiationData_m8_453 (NetworkingPeer_t8_98 * __this, int32_t ___instantiationId, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NetworkingPeer::DestroyPlayerObjects(System.Int32,System.Boolean)
extern "C" void NetworkingPeer_DestroyPlayerObjects_m8_454 (NetworkingPeer_t8_98 * __this, int32_t ___playerId, bool ___localOnly, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NetworkingPeer::DestroyAll(System.Boolean)
extern "C" void NetworkingPeer_DestroyAll_m8_455 (NetworkingPeer_t8_98 * __this, bool ___localOnly, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NetworkingPeer::RemoveInstantiatedGO(UnityEngine.GameObject,System.Boolean)
extern "C" void NetworkingPeer_RemoveInstantiatedGO_m8_456 (NetworkingPeer_t8_98 * __this, GameObject_t6_85 * ___go, bool ___localOnly, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 NetworkingPeer::GetInstantiatedObjectsId(UnityEngine.GameObject)
extern "C" int32_t NetworkingPeer_GetInstantiatedObjectsId_m8_457 (NetworkingPeer_t8_98 * __this, GameObject_t6_85 * ___go, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NetworkingPeer::ServerCleanInstantiateAndDestroy(System.Int32,System.Int32,System.Boolean)
extern "C" void NetworkingPeer_ServerCleanInstantiateAndDestroy_m8_458 (NetworkingPeer_t8_98 * __this, int32_t ___instantiateId, int32_t ___creatorId, bool ___isRuntimeInstantiated, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NetworkingPeer::SendDestroyOfPlayer(System.Int32)
extern "C" void NetworkingPeer_SendDestroyOfPlayer_m8_459 (NetworkingPeer_t8_98 * __this, int32_t ___actorNr, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NetworkingPeer::SendDestroyOfAll()
extern "C" void NetworkingPeer_SendDestroyOfAll_m8_460 (NetworkingPeer_t8_98 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NetworkingPeer::OpRemoveFromServerInstantiationsOfPlayer(System.Int32)
extern "C" void NetworkingPeer_OpRemoveFromServerInstantiationsOfPlayer_m8_461 (NetworkingPeer_t8_98 * __this, int32_t ___actorNr, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NetworkingPeer::RequestOwnership(System.Int32,System.Int32)
extern "C" void NetworkingPeer_RequestOwnership_m8_462 (NetworkingPeer_t8_98 * __this, int32_t ___viewID, int32_t ___fromOwner, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NetworkingPeer::TransferOwnership(System.Int32,System.Int32)
extern "C" void NetworkingPeer_TransferOwnership_m8_463 (NetworkingPeer_t8_98 * __this, int32_t ___viewID, int32_t ___playerID, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean NetworkingPeer::LocalCleanPhotonView(PhotonView)
extern "C" bool NetworkingPeer_LocalCleanPhotonView_m8_464 (NetworkingPeer_t8_98 * __this, PhotonView_t8_3 * ___view, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// PhotonView NetworkingPeer::GetPhotonView(System.Int32)
extern "C" PhotonView_t8_3 * NetworkingPeer_GetPhotonView_m8_465 (NetworkingPeer_t8_98 * __this, int32_t ___viewID, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NetworkingPeer::RegisterPhotonView(PhotonView)
extern "C" void NetworkingPeer_RegisterPhotonView_m8_466 (NetworkingPeer_t8_98 * __this, PhotonView_t8_3 * ___netView, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NetworkingPeer::OpCleanRpcBuffer(System.Int32)
extern "C" void NetworkingPeer_OpCleanRpcBuffer_m8_467 (NetworkingPeer_t8_98 * __this, int32_t ___actorNumber, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NetworkingPeer::OpRemoveCompleteCacheOfPlayer(System.Int32)
extern "C" void NetworkingPeer_OpRemoveCompleteCacheOfPlayer_m8_468 (NetworkingPeer_t8_98 * __this, int32_t ___actorNumber, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NetworkingPeer::OpRemoveCompleteCache()
extern "C" void NetworkingPeer_OpRemoveCompleteCache_m8_469 (NetworkingPeer_t8_98 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NetworkingPeer::RemoveCacheOfLeftPlayers()
extern "C" void NetworkingPeer_RemoveCacheOfLeftPlayers_m8_470 (NetworkingPeer_t8_98 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NetworkingPeer::CleanRpcBufferIfMine(PhotonView)
extern "C" void NetworkingPeer_CleanRpcBufferIfMine_m8_471 (NetworkingPeer_t8_98 * __this, PhotonView_t8_3 * ___view, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NetworkingPeer::OpCleanRpcBuffer(PhotonView)
extern "C" void NetworkingPeer_OpCleanRpcBuffer_m8_472 (NetworkingPeer_t8_98 * __this, PhotonView_t8_3 * ___view, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NetworkingPeer::RemoveRPCsInGroup(System.Int32)
extern "C" void NetworkingPeer_RemoveRPCsInGroup_m8_473 (NetworkingPeer_t8_98 * __this, int32_t ___group, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NetworkingPeer::SetLevelPrefix(System.Int16)
extern "C" void NetworkingPeer_SetLevelPrefix_m8_474 (NetworkingPeer_t8_98 * __this, int16_t ___prefix, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NetworkingPeer::RPC(PhotonView,System.String,PhotonPlayer,System.Boolean,System.Object[])
extern "C" void NetworkingPeer_RPC_m8_475 (NetworkingPeer_t8_98 * __this, PhotonView_t8_3 * ___view, String_t* ___methodName, PhotonPlayer_t8_102 * ___player, bool ___encrypt, ObjectU5BU5D_t1_157* ___parameters, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NetworkingPeer::RPC(PhotonView,System.String,PhotonTargets,System.Boolean,System.Object[])
extern "C" void NetworkingPeer_RPC_m8_476 (NetworkingPeer_t8_98 * __this, PhotonView_t8_3 * ___view, String_t* ___methodName, int32_t ___target, bool ___encrypt, ObjectU5BU5D_t1_157* ___parameters, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NetworkingPeer::SetReceivingEnabled(System.Int32,System.Boolean)
extern "C" void NetworkingPeer_SetReceivingEnabled_m8_477 (NetworkingPeer_t8_98 * __this, int32_t ___group, bool ___enabled, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NetworkingPeer::SetReceivingEnabled(System.Int32[],System.Int32[])
extern "C" void NetworkingPeer_SetReceivingEnabled_m8_478 (NetworkingPeer_t8_98 * __this, Int32U5BU5D_t1_160* ___enableGroups, Int32U5BU5D_t1_160* ___disableGroups, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NetworkingPeer::SetSendingEnabled(System.Int32,System.Boolean)
extern "C" void NetworkingPeer_SetSendingEnabled_m8_479 (NetworkingPeer_t8_98 * __this, int32_t ___group, bool ___enabled, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NetworkingPeer::SetSendingEnabled(System.Int32[],System.Int32[])
extern "C" void NetworkingPeer_SetSendingEnabled_m8_480 (NetworkingPeer_t8_98 * __this, Int32U5BU5D_t1_160* ___enableGroups, Int32U5BU5D_t1_160* ___disableGroups, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NetworkingPeer::NewSceneLoaded()
extern "C" void NetworkingPeer_NewSceneLoaded_m8_481 (NetworkingPeer_t8_98 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NetworkingPeer::RunViewUpdate()
extern "C" void NetworkingPeer_RunViewUpdate_m8_482 (NetworkingPeer_t8_98 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ExitGames.Client.Photon.Hashtable NetworkingPeer::OnSerializeWrite(PhotonView)
extern "C" Hashtable_t5_1 * NetworkingPeer_OnSerializeWrite_m8_483 (NetworkingPeer_t8_98 * __this, PhotonView_t8_3 * ___view, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NetworkingPeer::OnSerializeRead(ExitGames.Client.Photon.Hashtable,PhotonPlayer,System.Int32,System.Int16)
extern "C" void NetworkingPeer_OnSerializeRead_m8_484 (NetworkingPeer_t8_98 * __this, Hashtable_t5_1 * ___data, PhotonPlayer_t8_102 * ___sender, int32_t ___networkTime, int16_t ___correctPrefix, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean NetworkingPeer::AlmostEquals(System.Object[],System.Object[])
extern "C" bool NetworkingPeer_AlmostEquals_m8_485 (NetworkingPeer_t8_98 * __this, ObjectU5BU5D_t1_157* ___lastData, ObjectU5BU5D_t1_157* ___currentContent, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean NetworkingPeer::DeltaCompressionWrite(PhotonView,ExitGames.Client.Photon.Hashtable)
extern "C" bool NetworkingPeer_DeltaCompressionWrite_m8_486 (NetworkingPeer_t8_98 * __this, PhotonView_t8_3 * ___view, Hashtable_t5_1 * ___data, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean NetworkingPeer::DeltaCompressionRead(PhotonView,ExitGames.Client.Photon.Hashtable)
extern "C" bool NetworkingPeer_DeltaCompressionRead_m8_487 (NetworkingPeer_t8_98 * __this, PhotonView_t8_3 * ___view, Hashtable_t5_1 * ___data, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean NetworkingPeer::ObjectIsSameWithInprecision(System.Object,System.Object)
extern "C" bool NetworkingPeer_ObjectIsSameWithInprecision_m8_488 (NetworkingPeer_t8_98 * __this, Object_t * ___one, Object_t * ___two, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean NetworkingPeer::GetMethod(UnityEngine.MonoBehaviour,System.String,System.Reflection.MethodInfo&)
extern "C" bool NetworkingPeer_GetMethod_m8_489 (Object_t * __this /* static, unused */, MonoBehaviour_t6_80 * ___monob, String_t* ___methodType, MethodInfo_t ** ___mi, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NetworkingPeer::LoadLevelIfSynced()
extern "C" void NetworkingPeer_LoadLevelIfSynced_m8_490 (NetworkingPeer_t8_98 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NetworkingPeer::SetLevelInPropsIfSynced(System.Object)
extern "C" void NetworkingPeer_SetLevelInPropsIfSynced_m8_491 (NetworkingPeer_t8_98 * __this, Object_t * ___levelId, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NetworkingPeer::SetApp(System.String,System.String)
extern "C" void NetworkingPeer_SetApp_m8_492 (NetworkingPeer_t8_98 * __this, String_t* ___appId, String_t* ___gameVersion, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean NetworkingPeer::WebRpc(System.String,System.Object)
extern "C" bool NetworkingPeer_WebRpc_m8_493 (NetworkingPeer_t8_98 * __this, String_t* ___uriPath, Object_t * ___parameters, const MethodInfo* method) IL2CPP_METHOD_ATTR;
