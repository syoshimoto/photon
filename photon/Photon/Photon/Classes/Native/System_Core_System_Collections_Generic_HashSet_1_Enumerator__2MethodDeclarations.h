﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.HashSet`1<System.Int32>
struct HashSet_1_t2_15;
// System.Object
struct Object_t;

#include "codegen/il2cpp-codegen.h"
#include "System_Core_System_Collections_Generic_HashSet_1_Enumerator__2.h"

// System.Void System.Collections.Generic.HashSet`1/Enumerator<System.Int32>::.ctor(System.Collections.Generic.HashSet`1<T>)
extern "C" void Enumerator__ctor_m2_150_gshared (Enumerator_t2_30 * __this, HashSet_1_t2_15 * ___hashset, const MethodInfo* method);
#define Enumerator__ctor_m2_150(__this, ___hashset, method) (( void (*) (Enumerator_t2_30 *, HashSet_1_t2_15 *, const MethodInfo*))Enumerator__ctor_m2_150_gshared)(__this, ___hashset, method)
// System.Object System.Collections.Generic.HashSet`1/Enumerator<System.Int32>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * Enumerator_System_Collections_IEnumerator_get_Current_m2_151_gshared (Enumerator_t2_30 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m2_151(__this, method) (( Object_t * (*) (Enumerator_t2_30 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m2_151_gshared)(__this, method)
// System.Void System.Collections.Generic.HashSet`1/Enumerator<System.Int32>::System.Collections.IEnumerator.Reset()
extern "C" void Enumerator_System_Collections_IEnumerator_Reset_m2_152_gshared (Enumerator_t2_30 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_Reset_m2_152(__this, method) (( void (*) (Enumerator_t2_30 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m2_152_gshared)(__this, method)
// System.Boolean System.Collections.Generic.HashSet`1/Enumerator<System.Int32>::MoveNext()
extern "C" bool Enumerator_MoveNext_m2_153_gshared (Enumerator_t2_30 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m2_153(__this, method) (( bool (*) (Enumerator_t2_30 *, const MethodInfo*))Enumerator_MoveNext_m2_153_gshared)(__this, method)
// T System.Collections.Generic.HashSet`1/Enumerator<System.Int32>::get_Current()
extern "C" int32_t Enumerator_get_Current_m2_154_gshared (Enumerator_t2_30 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m2_154(__this, method) (( int32_t (*) (Enumerator_t2_30 *, const MethodInfo*))Enumerator_get_Current_m2_154_gshared)(__this, method)
// System.Void System.Collections.Generic.HashSet`1/Enumerator<System.Int32>::Dispose()
extern "C" void Enumerator_Dispose_m2_155_gshared (Enumerator_t2_30 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m2_155(__this, method) (( void (*) (Enumerator_t2_30 *, const MethodInfo*))Enumerator_Dispose_m2_155_gshared)(__this, method)
// System.Void System.Collections.Generic.HashSet`1/Enumerator<System.Int32>::CheckState()
extern "C" void Enumerator_CheckState_m2_156_gshared (Enumerator_t2_30 * __this, const MethodInfo* method);
#define Enumerator_CheckState_m2_156(__this, method) (( void (*) (Enumerator_t2_30 *, const MethodInfo*))Enumerator_CheckState_m2_156_gshared)(__this, method)
