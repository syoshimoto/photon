﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Array
struct Array_t;
// System.Object
struct Object_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen_4.h"

// System.Void System.Array/InternalEnumerator`1<System.Single>::.ctor(System.Array)
extern "C" void InternalEnumerator_1__ctor_m1_5821_gshared (InternalEnumerator_1_t1_983 * __this, Array_t * ___array, const MethodInfo* method);
#define InternalEnumerator_1__ctor_m1_5821(__this, ___array, method) (( void (*) (InternalEnumerator_1_t1_983 *, Array_t *, const MethodInfo*))InternalEnumerator_1__ctor_m1_5821_gshared)(__this, ___array, method)
// System.Void System.Array/InternalEnumerator`1<System.Single>::System.Collections.IEnumerator.Reset()
extern "C" void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1_5822_gshared (InternalEnumerator_1_t1_983 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1_5822(__this, method) (( void (*) (InternalEnumerator_1_t1_983 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1_5822_gshared)(__this, method)
// System.Object System.Array/InternalEnumerator`1<System.Single>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1_5823_gshared (InternalEnumerator_1_t1_983 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1_5823(__this, method) (( Object_t * (*) (InternalEnumerator_1_t1_983 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1_5823_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<System.Single>::Dispose()
extern "C" void InternalEnumerator_1_Dispose_m1_5824_gshared (InternalEnumerator_1_t1_983 * __this, const MethodInfo* method);
#define InternalEnumerator_1_Dispose_m1_5824(__this, method) (( void (*) (InternalEnumerator_1_t1_983 *, const MethodInfo*))InternalEnumerator_1_Dispose_m1_5824_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<System.Single>::MoveNext()
extern "C" bool InternalEnumerator_1_MoveNext_m1_5825_gshared (InternalEnumerator_1_t1_983 * __this, const MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m1_5825(__this, method) (( bool (*) (InternalEnumerator_1_t1_983 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m1_5825_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<System.Single>::get_Current()
extern "C" float InternalEnumerator_1_get_Current_m1_5826_gshared (InternalEnumerator_1_t1_983 * __this, const MethodInfo* method);
#define InternalEnumerator_1_get_Current_m1_5826(__this, method) (( float (*) (InternalEnumerator_1_t1_983 *, const MethodInfo*))InternalEnumerator_1_get_Current_m1_5826_gshared)(__this, method)
