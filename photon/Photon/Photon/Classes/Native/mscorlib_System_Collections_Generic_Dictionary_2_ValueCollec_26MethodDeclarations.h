﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_ValueCollec_10MethodDeclarations.h"

// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<System.String,ExitGames.Client.Photon.Chat.ChatChannel>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
#define ValueCollection__ctor_m1_9329(__this, ___dictionary, method) (( void (*) (ValueCollection_t1_1290 *, Dictionary_2_t1_932 *, const MethodInfo*))ValueCollection__ctor_m1_6390_gshared)(__this, ___dictionary, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<System.String,ExitGames.Client.Photon.Chat.ChatChannel>::System.Collections.Generic.ICollection<TValue>.Add(TValue)
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m1_9330(__this, ___item, method) (( void (*) (ValueCollection_t1_1290 *, ChatChannel_t7_1 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m1_6391_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<System.String,ExitGames.Client.Photon.Chat.ChatChannel>::System.Collections.Generic.ICollection<TValue>.Clear()
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m1_9331(__this, method) (( void (*) (ValueCollection_t1_1290 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m1_6392_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<System.String,ExitGames.Client.Photon.Chat.ChatChannel>::System.Collections.Generic.ICollection<TValue>.Contains(TValue)
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m1_9332(__this, ___item, method) (( bool (*) (ValueCollection_t1_1290 *, ChatChannel_t7_1 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m1_6393_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<System.String,ExitGames.Client.Photon.Chat.ChatChannel>::System.Collections.Generic.ICollection<TValue>.Remove(TValue)
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m1_9333(__this, ___item, method) (( bool (*) (ValueCollection_t1_1290 *, ChatChannel_t7_1 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m1_6394_gshared)(__this, ___item, method)
// System.Collections.Generic.IEnumerator`1<TValue> System.Collections.Generic.Dictionary`2/ValueCollection<System.String,ExitGames.Client.Photon.Chat.ChatChannel>::System.Collections.Generic.IEnumerable<TValue>.GetEnumerator()
#define ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m1_9334(__this, method) (( Object_t* (*) (ValueCollection_t1_1290 *, const MethodInfo*))ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m1_6395_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<System.String,ExitGames.Client.Photon.Chat.ChatChannel>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define ValueCollection_System_Collections_ICollection_CopyTo_m1_9335(__this, ___array, ___index, method) (( void (*) (ValueCollection_t1_1290 *, Array_t *, int32_t, const MethodInfo*))ValueCollection_System_Collections_ICollection_CopyTo_m1_6396_gshared)(__this, ___array, ___index, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2/ValueCollection<System.String,ExitGames.Client.Photon.Chat.ChatChannel>::System.Collections.IEnumerable.GetEnumerator()
#define ValueCollection_System_Collections_IEnumerable_GetEnumerator_m1_9336(__this, method) (( Object_t * (*) (ValueCollection_t1_1290 *, const MethodInfo*))ValueCollection_System_Collections_IEnumerable_GetEnumerator_m1_6397_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<System.String,ExitGames.Client.Photon.Chat.ChatChannel>::System.Collections.Generic.ICollection<TValue>.get_IsReadOnly()
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m1_9337(__this, method) (( bool (*) (ValueCollection_t1_1290 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m1_6398_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ValueCollection<System.String,ExitGames.Client.Photon.Chat.ChatChannel>::System.Collections.ICollection.get_SyncRoot()
#define ValueCollection_System_Collections_ICollection_get_SyncRoot_m1_9338(__this, method) (( Object_t * (*) (ValueCollection_t1_1290 *, const MethodInfo*))ValueCollection_System_Collections_ICollection_get_SyncRoot_m1_6399_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<System.String,ExitGames.Client.Photon.Chat.ChatChannel>::CopyTo(TValue[],System.Int32)
#define ValueCollection_CopyTo_m1_9339(__this, ___array, ___index, method) (( void (*) (ValueCollection_t1_1290 *, ChatChannelU5BU5D_t7_15*, int32_t, const MethodInfo*))ValueCollection_CopyTo_m1_6400_gshared)(__this, ___array, ___index, method)
// System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2/ValueCollection<System.String,ExitGames.Client.Photon.Chat.ChatChannel>::GetEnumerator()
#define ValueCollection_GetEnumerator_m1_9340(__this, method) (( Enumerator_t1_1459  (*) (ValueCollection_t1_1290 *, const MethodInfo*))ValueCollection_GetEnumerator_m1_6401_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2/ValueCollection<System.String,ExitGames.Client.Photon.Chat.ChatChannel>::get_Count()
#define ValueCollection_get_Count_m1_9341(__this, method) (( int32_t (*) (ValueCollection_t1_1290 *, const MethodInfo*))ValueCollection_get_Count_m1_6402_gshared)(__this, method)
