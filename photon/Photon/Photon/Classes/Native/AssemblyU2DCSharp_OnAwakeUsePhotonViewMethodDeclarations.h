﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// OnAwakeUsePhotonView
struct OnAwakeUsePhotonView_t8_155;

#include "codegen/il2cpp-codegen.h"

// System.Void OnAwakeUsePhotonView::.ctor()
extern "C" void OnAwakeUsePhotonView__ctor_m8_958 (OnAwakeUsePhotonView_t8_155 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OnAwakeUsePhotonView::Awake()
extern "C" void OnAwakeUsePhotonView_Awake_m8_959 (OnAwakeUsePhotonView_t8_155 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OnAwakeUsePhotonView::Start()
extern "C" void OnAwakeUsePhotonView_Start_m8_960 (OnAwakeUsePhotonView_t8_155 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OnAwakeUsePhotonView::OnAwakeRPC()
extern "C" void OnAwakeUsePhotonView_OnAwakeRPC_m8_961 (OnAwakeUsePhotonView_t8_155 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OnAwakeUsePhotonView::OnAwakeRPC(System.Byte)
extern "C" void OnAwakeUsePhotonView_OnAwakeRPC_m8_962 (OnAwakeUsePhotonView_t8_155 * __this, uint8_t ___myParameter, const MethodInfo* method) IL2CPP_METHOD_ATTR;
