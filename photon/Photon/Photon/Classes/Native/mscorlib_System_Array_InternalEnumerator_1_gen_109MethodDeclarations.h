﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Array
struct Array_t;
// System.Object
struct Object_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen_109.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_18.h"

// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<PunTeams/Team,System.Object>>::.ctor(System.Array)
extern "C" void InternalEnumerator_1__ctor_m1_9690_gshared (InternalEnumerator_1_t1_1320 * __this, Array_t * ___array, const MethodInfo* method);
#define InternalEnumerator_1__ctor_m1_9690(__this, ___array, method) (( void (*) (InternalEnumerator_1_t1_1320 *, Array_t *, const MethodInfo*))InternalEnumerator_1__ctor_m1_9690_gshared)(__this, ___array, method)
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<PunTeams/Team,System.Object>>::System.Collections.IEnumerator.Reset()
extern "C" void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1_9691_gshared (InternalEnumerator_1_t1_1320 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1_9691(__this, method) (( void (*) (InternalEnumerator_1_t1_1320 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1_9691_gshared)(__this, method)
// System.Object System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<PunTeams/Team,System.Object>>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1_9692_gshared (InternalEnumerator_1_t1_1320 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1_9692(__this, method) (( Object_t * (*) (InternalEnumerator_1_t1_1320 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1_9692_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<PunTeams/Team,System.Object>>::Dispose()
extern "C" void InternalEnumerator_1_Dispose_m1_9693_gshared (InternalEnumerator_1_t1_1320 * __this, const MethodInfo* method);
#define InternalEnumerator_1_Dispose_m1_9693(__this, method) (( void (*) (InternalEnumerator_1_t1_1320 *, const MethodInfo*))InternalEnumerator_1_Dispose_m1_9693_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<PunTeams/Team,System.Object>>::MoveNext()
extern "C" bool InternalEnumerator_1_MoveNext_m1_9694_gshared (InternalEnumerator_1_t1_1320 * __this, const MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m1_9694(__this, method) (( bool (*) (InternalEnumerator_1_t1_1320 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m1_9694_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<PunTeams/Team,System.Object>>::get_Current()
extern "C" KeyValuePair_2_t1_1317  InternalEnumerator_1_get_Current_m1_9695_gshared (InternalEnumerator_1_t1_1320 * __this, const MethodInfo* method);
#define InternalEnumerator_1_get_Current_m1_9695(__this, method) (( KeyValuePair_2_t1_1317  (*) (InternalEnumerator_1_t1_1320 *, const MethodInfo*))InternalEnumerator_1_get_Current_m1_9695_gshared)(__this, method)
