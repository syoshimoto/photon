﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "mscorlib_System_Enum.h"
#include "AssemblyU2DCSharp_ExitGames_Client_Photon_MatchmakingMode.h"

// ExitGames.Client.Photon.MatchmakingMode
struct  MatchmakingMode_t8_89 
{
	// System.Byte ExitGames.Client.Photon.MatchmakingMode::value__
	uint8_t ___value___1;
};
