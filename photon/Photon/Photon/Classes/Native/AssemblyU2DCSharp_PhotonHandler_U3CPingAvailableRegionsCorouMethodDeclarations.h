﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PhotonHandler/<PingAvailableRegionsCoroutine>c__Iterator0
struct U3CPingAvailableRegionsCoroutineU3Ec__Iterator0_t8_108;
// System.Object
struct Object_t;

#include "codegen/il2cpp-codegen.h"

// System.Void PhotonHandler/<PingAvailableRegionsCoroutine>c__Iterator0::.ctor()
extern "C" void U3CPingAvailableRegionsCoroutineU3Ec__Iterator0__ctor_m8_565 (U3CPingAvailableRegionsCoroutineU3Ec__Iterator0_t8_108 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object PhotonHandler/<PingAvailableRegionsCoroutine>c__Iterator0::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C" Object_t * U3CPingAvailableRegionsCoroutineU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m8_566 (U3CPingAvailableRegionsCoroutineU3Ec__Iterator0_t8_108 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object PhotonHandler/<PingAvailableRegionsCoroutine>c__Iterator0::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * U3CPingAvailableRegionsCoroutineU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m8_567 (U3CPingAvailableRegionsCoroutineU3Ec__Iterator0_t8_108 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PhotonHandler/<PingAvailableRegionsCoroutine>c__Iterator0::MoveNext()
extern "C" bool U3CPingAvailableRegionsCoroutineU3Ec__Iterator0_MoveNext_m8_568 (U3CPingAvailableRegionsCoroutineU3Ec__Iterator0_t8_108 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PhotonHandler/<PingAvailableRegionsCoroutine>c__Iterator0::Dispose()
extern "C" void U3CPingAvailableRegionsCoroutineU3Ec__Iterator0_Dispose_m8_569 (U3CPingAvailableRegionsCoroutineU3Ec__Iterator0_t8_108 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PhotonHandler/<PingAvailableRegionsCoroutine>c__Iterator0::Reset()
extern "C" void U3CPingAvailableRegionsCoroutineU3Ec__Iterator0_Reset_m8_570 (U3CPingAvailableRegionsCoroutineU3Ec__Iterator0_t8_108 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
