﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GUICustomAuth
struct GUICustomAuth_t8_17;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"

// System.Void GUICustomAuth::.ctor()
extern "C" void GUICustomAuth__ctor_m8_59 (GUICustomAuth_t8_17 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GUICustomAuth::Start()
extern "C" void GUICustomAuth_Start_m8_60 (GUICustomAuth_t8_17 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GUICustomAuth::OnJoinedLobby()
extern "C" void GUICustomAuth_OnJoinedLobby_m8_61 (GUICustomAuth_t8_17 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GUICustomAuth::OnConnectedToMaster()
extern "C" void GUICustomAuth_OnConnectedToMaster_m8_62 (GUICustomAuth_t8_17 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GUICustomAuth::OnCustomAuthenticationFailed(System.String)
extern "C" void GUICustomAuth_OnCustomAuthenticationFailed_m8_63 (GUICustomAuth_t8_17 * __this, String_t* ___debugMessage, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GUICustomAuth::SetStateAuthInput()
extern "C" void GUICustomAuth_SetStateAuthInput_m8_64 (GUICustomAuth_t8_17 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GUICustomAuth::SetStateAuthHelp()
extern "C" void GUICustomAuth_SetStateAuthHelp_m8_65 (GUICustomAuth_t8_17 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GUICustomAuth::SetStateAuthOrNot()
extern "C" void GUICustomAuth_SetStateAuthOrNot_m8_66 (GUICustomAuth_t8_17 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GUICustomAuth::SetStateAuthFailed()
extern "C" void GUICustomAuth_SetStateAuthFailed_m8_67 (GUICustomAuth_t8_17 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GUICustomAuth::ConnectWithNickname()
extern "C" void GUICustomAuth_ConnectWithNickname_m8_68 (GUICustomAuth_t8_17 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GUICustomAuth::OnGUI()
extern "C" void GUICustomAuth_OnGUI_m8_69 (GUICustomAuth_t8_17 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
