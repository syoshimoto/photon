﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Queue`1<System.Int32>
struct Queue_1_t3_185;
// System.Object
struct Object_t;

#include "codegen/il2cpp-codegen.h"
#include "System_System_Collections_Generic_Queue_1_Enumerator_gen_3.h"

// System.Void System.Collections.Generic.Queue`1/Enumerator<System.Int32>::.ctor(System.Collections.Generic.Queue`1<T>)
extern "C" void Enumerator__ctor_m3_1212_gshared (Enumerator_t3_201 * __this, Queue_1_t3_185 * ___q, const MethodInfo* method);
#define Enumerator__ctor_m3_1212(__this, ___q, method) (( void (*) (Enumerator_t3_201 *, Queue_1_t3_185 *, const MethodInfo*))Enumerator__ctor_m3_1212_gshared)(__this, ___q, method)
// System.Void System.Collections.Generic.Queue`1/Enumerator<System.Int32>::System.Collections.IEnumerator.Reset()
extern "C" void Enumerator_System_Collections_IEnumerator_Reset_m3_1213_gshared (Enumerator_t3_201 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_Reset_m3_1213(__this, method) (( void (*) (Enumerator_t3_201 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m3_1213_gshared)(__this, method)
// System.Object System.Collections.Generic.Queue`1/Enumerator<System.Int32>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * Enumerator_System_Collections_IEnumerator_get_Current_m3_1214_gshared (Enumerator_t3_201 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m3_1214(__this, method) (( Object_t * (*) (Enumerator_t3_201 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m3_1214_gshared)(__this, method)
// System.Void System.Collections.Generic.Queue`1/Enumerator<System.Int32>::Dispose()
extern "C" void Enumerator_Dispose_m3_1215_gshared (Enumerator_t3_201 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m3_1215(__this, method) (( void (*) (Enumerator_t3_201 *, const MethodInfo*))Enumerator_Dispose_m3_1215_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Queue`1/Enumerator<System.Int32>::MoveNext()
extern "C" bool Enumerator_MoveNext_m3_1216_gshared (Enumerator_t3_201 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m3_1216(__this, method) (( bool (*) (Enumerator_t3_201 *, const MethodInfo*))Enumerator_MoveNext_m3_1216_gshared)(__this, method)
// T System.Collections.Generic.Queue`1/Enumerator<System.Int32>::get_Current()
extern "C" int32_t Enumerator_get_Current_m3_1217_gshared (Enumerator_t3_201 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m3_1217(__this, method) (( int32_t (*) (Enumerator_t3_201 *, const MethodInfo*))Enumerator_get_Current_m3_1217_gshared)(__this, method)
