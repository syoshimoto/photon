﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "UnityEngine_UnityEngine_MonoBehaviour.h"

// PickupDemoGui
struct  PickupDemoGui_t8_34  : public MonoBehaviour_t6_80
{
	// System.Boolean PickupDemoGui::ShowScores
	bool ___ShowScores_2;
	// System.Boolean PickupDemoGui::ShowDropButton
	bool ___ShowDropButton_3;
	// System.Boolean PickupDemoGui::ShowTeams
	bool ___ShowTeams_4;
	// System.Single PickupDemoGui::DropOffset
	float ___DropOffset_5;
};
