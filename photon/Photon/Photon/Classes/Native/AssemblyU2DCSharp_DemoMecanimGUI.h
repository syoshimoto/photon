﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.GUISkin
struct GUISkin_t6_161;
// PhotonAnimatorView
struct PhotonAnimatorView_t8_27;
// UnityEngine.Animator
struct Animator_t6_138;

#include "AssemblyU2DCSharp_Photon_PunBehaviour.h"

// DemoMecanimGUI
struct  DemoMecanimGUI_t8_25  : public PunBehaviour_t8_26
{
	// UnityEngine.GUISkin DemoMecanimGUI::Skin
	GUISkin_t6_161 * ___Skin_2;
	// PhotonAnimatorView DemoMecanimGUI::m_AnimatorView
	PhotonAnimatorView_t8_27 * ___m_AnimatorView_3;
	// UnityEngine.Animator DemoMecanimGUI::m_RemoteAnimator
	Animator_t6_138 * ___m_RemoteAnimator_4;
	// System.Single DemoMecanimGUI::m_SlideIn
	float ___m_SlideIn_5;
	// System.Single DemoMecanimGUI::m_FoundPlayerSlideIn
	float ___m_FoundPlayerSlideIn_6;
	// System.Boolean DemoMecanimGUI::m_IsOpen
	bool ___m_IsOpen_7;
};
