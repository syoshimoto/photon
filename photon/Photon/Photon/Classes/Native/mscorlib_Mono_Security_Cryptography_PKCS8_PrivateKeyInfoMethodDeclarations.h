﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Mono.Security.Cryptography.PKCS8/PrivateKeyInfo
struct PrivateKeyInfo_t1_112;
// System.Byte[]
struct ByteU5BU5D_t1_71;
// System.Security.Cryptography.RSA
struct RSA_t1_118;
// System.Security.Cryptography.DSA
struct DSA_t1_106;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Security_Cryptography_DSAParameters.h"

// System.Void Mono.Security.Cryptography.PKCS8/PrivateKeyInfo::.ctor()
extern "C" void PrivateKeyInfo__ctor_m1_1272 (PrivateKeyInfo_t1_112 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Security.Cryptography.PKCS8/PrivateKeyInfo::.ctor(System.Byte[])
extern "C" void PrivateKeyInfo__ctor_m1_1273 (PrivateKeyInfo_t1_112 * __this, ByteU5BU5D_t1_71* ___data, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] Mono.Security.Cryptography.PKCS8/PrivateKeyInfo::get_PrivateKey()
extern "C" ByteU5BU5D_t1_71* PrivateKeyInfo_get_PrivateKey_m1_1274 (PrivateKeyInfo_t1_112 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Security.Cryptography.PKCS8/PrivateKeyInfo::Decode(System.Byte[])
extern "C" void PrivateKeyInfo_Decode_m1_1275 (PrivateKeyInfo_t1_112 * __this, ByteU5BU5D_t1_71* ___data, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] Mono.Security.Cryptography.PKCS8/PrivateKeyInfo::RemoveLeadingZero(System.Byte[])
extern "C" ByteU5BU5D_t1_71* PrivateKeyInfo_RemoveLeadingZero_m1_1276 (Object_t * __this /* static, unused */, ByteU5BU5D_t1_71* ___bigInt, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] Mono.Security.Cryptography.PKCS8/PrivateKeyInfo::Normalize(System.Byte[],System.Int32)
extern "C" ByteU5BU5D_t1_71* PrivateKeyInfo_Normalize_m1_1277 (Object_t * __this /* static, unused */, ByteU5BU5D_t1_71* ___bigInt, int32_t ___length, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.Cryptography.RSA Mono.Security.Cryptography.PKCS8/PrivateKeyInfo::DecodeRSA(System.Byte[])
extern "C" RSA_t1_118 * PrivateKeyInfo_DecodeRSA_m1_1278 (Object_t * __this /* static, unused */, ByteU5BU5D_t1_71* ___keypair, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.Cryptography.DSA Mono.Security.Cryptography.PKCS8/PrivateKeyInfo::DecodeDSA(System.Byte[],System.Security.Cryptography.DSAParameters)
extern "C" DSA_t1_106 * PrivateKeyInfo_DecodeDSA_m1_1279 (Object_t * __this /* static, unused */, ByteU5BU5D_t1_71* ___privateKey, DSAParameters_t1_563  ___dsaParameters, const MethodInfo* method) IL2CPP_METHOD_ATTR;
