﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.Transform
struct Transform_t6_61;
// UnityEngine.GameObject[]
struct GameObjectU5BU5D_t6_258;

#include "UnityEngine_UnityEngine_MonoBehaviour.h"

// OnJoinedInstantiate
struct  OnJoinedInstantiate_t8_161  : public MonoBehaviour_t6_80
{
	// UnityEngine.Transform OnJoinedInstantiate::SpawnPosition
	Transform_t6_61 * ___SpawnPosition_2;
	// System.Single OnJoinedInstantiate::PositionOffset
	float ___PositionOffset_3;
	// UnityEngine.GameObject[] OnJoinedInstantiate::PrefabsToInstantiate
	GameObjectU5BU5D_t6_258* ___PrefabsToInstantiate_4;
};
