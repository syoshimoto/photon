﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2/ValueCollection<PunTeams/Team,System.Object>
struct ValueCollection_t1_1324;
// System.Collections.Generic.Dictionary`2<PunTeams/Team,System.Object>
struct Dictionary_2_t1_1316;
// System.Object
struct Object_t;
// System.Collections.Generic.IEnumerator`1<System.Object>
struct IEnumerator_1_t1_1402;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t1_130;
// System.Object[]
struct ObjectU5BU5D_t1_157;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_ValueCollec_30.h"

// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<PunTeams/Team,System.Object>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C" void ValueCollection__ctor_m1_9719_gshared (ValueCollection_t1_1324 * __this, Dictionary_2_t1_1316 * ___dictionary, const MethodInfo* method);
#define ValueCollection__ctor_m1_9719(__this, ___dictionary, method) (( void (*) (ValueCollection_t1_1324 *, Dictionary_2_t1_1316 *, const MethodInfo*))ValueCollection__ctor_m1_9719_gshared)(__this, ___dictionary, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<PunTeams/Team,System.Object>::System.Collections.Generic.ICollection<TValue>.Add(TValue)
extern "C" void ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m1_9720_gshared (ValueCollection_t1_1324 * __this, Object_t * ___item, const MethodInfo* method);
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m1_9720(__this, ___item, method) (( void (*) (ValueCollection_t1_1324 *, Object_t *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m1_9720_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<PunTeams/Team,System.Object>::System.Collections.Generic.ICollection<TValue>.Clear()
extern "C" void ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m1_9721_gshared (ValueCollection_t1_1324 * __this, const MethodInfo* method);
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m1_9721(__this, method) (( void (*) (ValueCollection_t1_1324 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m1_9721_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<PunTeams/Team,System.Object>::System.Collections.Generic.ICollection<TValue>.Contains(TValue)
extern "C" bool ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m1_9722_gshared (ValueCollection_t1_1324 * __this, Object_t * ___item, const MethodInfo* method);
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m1_9722(__this, ___item, method) (( bool (*) (ValueCollection_t1_1324 *, Object_t *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m1_9722_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<PunTeams/Team,System.Object>::System.Collections.Generic.ICollection<TValue>.Remove(TValue)
extern "C" bool ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m1_9723_gshared (ValueCollection_t1_1324 * __this, Object_t * ___item, const MethodInfo* method);
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m1_9723(__this, ___item, method) (( bool (*) (ValueCollection_t1_1324 *, Object_t *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m1_9723_gshared)(__this, ___item, method)
// System.Collections.Generic.IEnumerator`1<TValue> System.Collections.Generic.Dictionary`2/ValueCollection<PunTeams/Team,System.Object>::System.Collections.Generic.IEnumerable<TValue>.GetEnumerator()
extern "C" Object_t* ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m1_9724_gshared (ValueCollection_t1_1324 * __this, const MethodInfo* method);
#define ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m1_9724(__this, method) (( Object_t* (*) (ValueCollection_t1_1324 *, const MethodInfo*))ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m1_9724_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<PunTeams/Team,System.Object>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C" void ValueCollection_System_Collections_ICollection_CopyTo_m1_9725_gshared (ValueCollection_t1_1324 * __this, Array_t * ___array, int32_t ___index, const MethodInfo* method);
#define ValueCollection_System_Collections_ICollection_CopyTo_m1_9725(__this, ___array, ___index, method) (( void (*) (ValueCollection_t1_1324 *, Array_t *, int32_t, const MethodInfo*))ValueCollection_System_Collections_ICollection_CopyTo_m1_9725_gshared)(__this, ___array, ___index, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2/ValueCollection<PunTeams/Team,System.Object>::System.Collections.IEnumerable.GetEnumerator()
extern "C" Object_t * ValueCollection_System_Collections_IEnumerable_GetEnumerator_m1_9726_gshared (ValueCollection_t1_1324 * __this, const MethodInfo* method);
#define ValueCollection_System_Collections_IEnumerable_GetEnumerator_m1_9726(__this, method) (( Object_t * (*) (ValueCollection_t1_1324 *, const MethodInfo*))ValueCollection_System_Collections_IEnumerable_GetEnumerator_m1_9726_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<PunTeams/Team,System.Object>::System.Collections.Generic.ICollection<TValue>.get_IsReadOnly()
extern "C" bool ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m1_9727_gshared (ValueCollection_t1_1324 * __this, const MethodInfo* method);
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m1_9727(__this, method) (( bool (*) (ValueCollection_t1_1324 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m1_9727_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ValueCollection<PunTeams/Team,System.Object>::System.Collections.ICollection.get_SyncRoot()
extern "C" Object_t * ValueCollection_System_Collections_ICollection_get_SyncRoot_m1_9728_gshared (ValueCollection_t1_1324 * __this, const MethodInfo* method);
#define ValueCollection_System_Collections_ICollection_get_SyncRoot_m1_9728(__this, method) (( Object_t * (*) (ValueCollection_t1_1324 *, const MethodInfo*))ValueCollection_System_Collections_ICollection_get_SyncRoot_m1_9728_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<PunTeams/Team,System.Object>::CopyTo(TValue[],System.Int32)
extern "C" void ValueCollection_CopyTo_m1_9729_gshared (ValueCollection_t1_1324 * __this, ObjectU5BU5D_t1_157* ___array, int32_t ___index, const MethodInfo* method);
#define ValueCollection_CopyTo_m1_9729(__this, ___array, ___index, method) (( void (*) (ValueCollection_t1_1324 *, ObjectU5BU5D_t1_157*, int32_t, const MethodInfo*))ValueCollection_CopyTo_m1_9729_gshared)(__this, ___array, ___index, method)
// System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2/ValueCollection<PunTeams/Team,System.Object>::GetEnumerator()
extern "C" Enumerator_t1_1325  ValueCollection_GetEnumerator_m1_9730_gshared (ValueCollection_t1_1324 * __this, const MethodInfo* method);
#define ValueCollection_GetEnumerator_m1_9730(__this, method) (( Enumerator_t1_1325  (*) (ValueCollection_t1_1324 *, const MethodInfo*))ValueCollection_GetEnumerator_m1_9730_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2/ValueCollection<PunTeams/Team,System.Object>::get_Count()
extern "C" int32_t ValueCollection_get_Count_m1_9731_gshared (ValueCollection_t1_1324 * __this, const MethodInfo* method);
#define ValueCollection_get_Count_m1_9731(__this, method) (( int32_t (*) (ValueCollection_t1_1324 *, const MethodInfo*))ValueCollection_get_Count_m1_9731_gshared)(__this, method)
