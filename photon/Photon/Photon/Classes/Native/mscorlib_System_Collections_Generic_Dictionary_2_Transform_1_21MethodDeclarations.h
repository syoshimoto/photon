﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Transform_1_19MethodDeclarations.h"

// System.Void System.Collections.Generic.Dictionary`2/Transform`1<System.Byte,ExitGames.Client.Photon.EnetChannel,System.Collections.DictionaryEntry>::.ctor(System.Object,System.IntPtr)
#define Transform_1__ctor_m1_7769(__this, ___object, ___method, method) (( void (*) (Transform_1_t1_1187 *, Object_t *, IntPtr_t, const MethodInfo*))Transform_1__ctor_m1_7699_gshared)(__this, ___object, ___method, method)
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Byte,ExitGames.Client.Photon.EnetChannel,System.Collections.DictionaryEntry>::Invoke(TKey,TValue)
#define Transform_1_Invoke_m1_7770(__this, ___key, ___value, method) (( DictionaryEntry_t1_167  (*) (Transform_1_t1_1187 *, uint8_t, EnetChannel_t5_5 *, const MethodInfo*))Transform_1_Invoke_m1_7700_gshared)(__this, ___key, ___value, method)
// System.IAsyncResult System.Collections.Generic.Dictionary`2/Transform`1<System.Byte,ExitGames.Client.Photon.EnetChannel,System.Collections.DictionaryEntry>::BeginInvoke(TKey,TValue,System.AsyncCallback,System.Object)
#define Transform_1_BeginInvoke_m1_7771(__this, ___key, ___value, ___callback, ___object, method) (( Object_t * (*) (Transform_1_t1_1187 *, uint8_t, EnetChannel_t5_5 *, AsyncCallback_t1_28 *, Object_t *, const MethodInfo*))Transform_1_BeginInvoke_m1_7701_gshared)(__this, ___key, ___value, ___callback, ___object, method)
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Byte,ExitGames.Client.Photon.EnetChannel,System.Collections.DictionaryEntry>::EndInvoke(System.IAsyncResult)
#define Transform_1_EndInvoke_m1_7772(__this, ___result, method) (( DictionaryEntry_t1_167  (*) (Transform_1_t1_1187 *, Object_t *, const MethodInfo*))Transform_1_EndInvoke_m1_7702_gshared)(__this, ___result, method)
