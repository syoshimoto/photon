﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.GameObject
struct GameObject_t6_85;
// System.String
struct String_t;

#include "AssemblyU2DCSharp_Photon_MonoBehaviour.h"

// OnClickCallMethod
struct  OnClickCallMethod_t8_20  : public MonoBehaviour_t8_6
{
	// UnityEngine.GameObject OnClickCallMethod::TargetGameObject
	GameObject_t6_85 * ___TargetGameObject_2;
	// System.String OnClickCallMethod::TargetMethod
	String_t* ___TargetMethod_3;
};
