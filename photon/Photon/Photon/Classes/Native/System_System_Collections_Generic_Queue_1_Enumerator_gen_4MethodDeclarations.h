﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "System_System_Collections_Generic_Queue_1_Enumerator_genMethodDeclarations.h"

// System.Void System.Collections.Generic.Queue`1/Enumerator<System.Byte[]>::.ctor(System.Collections.Generic.Queue`1<T>)
#define Enumerator__ctor_m3_1228(__this, ___q, method) (( void (*) (Enumerator_t3_202 *, Queue_1_t3_186 *, const MethodInfo*))Enumerator__ctor_m3_1108_gshared)(__this, ___q, method)
// System.Void System.Collections.Generic.Queue`1/Enumerator<System.Byte[]>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m3_1229(__this, method) (( void (*) (Enumerator_t3_202 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m3_1109_gshared)(__this, method)
// System.Object System.Collections.Generic.Queue`1/Enumerator<System.Byte[]>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m3_1230(__this, method) (( Object_t * (*) (Enumerator_t3_202 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m3_1110_gshared)(__this, method)
// System.Void System.Collections.Generic.Queue`1/Enumerator<System.Byte[]>::Dispose()
#define Enumerator_Dispose_m3_1231(__this, method) (( void (*) (Enumerator_t3_202 *, const MethodInfo*))Enumerator_Dispose_m3_1111_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Queue`1/Enumerator<System.Byte[]>::MoveNext()
#define Enumerator_MoveNext_m3_1232(__this, method) (( bool (*) (Enumerator_t3_202 *, const MethodInfo*))Enumerator_MoveNext_m3_1112_gshared)(__this, method)
// T System.Collections.Generic.Queue`1/Enumerator<System.Byte[]>::get_Current()
#define Enumerator_get_Current_m3_1233(__this, method) (( ByteU5BU5D_t1_71* (*) (Enumerator_t3_202 *, const MethodInfo*))Enumerator_get_Current_m3_1113_gshared)(__this, method)
