﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2<ExitGames.Client.Photon.ConnectionProtocol,System.Int32>
struct Dictionary_2_t1_933;
// System.Collections.Generic.IEqualityComparer`1<ExitGames.Client.Photon.ConnectionProtocol>
struct IEqualityComparer_1_t1_1293;
// System.Runtime.Serialization.SerializationInfo
struct SerializationInfo_t1_177;
// System.Collections.ICollection
struct ICollection_t1_811;
// System.Object
struct Object_t;
// System.Collections.Generic.KeyValuePair`2<ExitGames.Client.Photon.ConnectionProtocol,System.Int32>[]
struct KeyValuePair_2U5BU5D_t1_1460;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t1_130;
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<ExitGames.Client.Photon.ConnectionProtocol,System.Int32>>
struct IEnumerator_1_t1_1461;
// System.Collections.IDictionaryEnumerator
struct IDictionaryEnumerator_t1_458;
// System.Collections.Generic.Dictionary`2/KeyCollection<ExitGames.Client.Photon.ConnectionProtocol,System.Int32>
struct KeyCollection_t1_1297;
// System.Collections.Generic.Dictionary`2/ValueCollection<ExitGames.Client.Photon.ConnectionProtocol,System.Int32>
struct ValueCollection_t1_1301;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Runtime_Serialization_StreamingContext.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_17.h"
#include "Photon3Unity3D_ExitGames_Client_Photon_ConnectionProtocol.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Enumerator__17.h"
#include "mscorlib_System_Collections_DictionaryEntry.h"

// System.Void System.Collections.Generic.Dictionary`2<ExitGames.Client.Photon.ConnectionProtocol,System.Int32>::.ctor()
extern "C" void Dictionary_2__ctor_m1_5621_gshared (Dictionary_2_t1_933 * __this, const MethodInfo* method);
#define Dictionary_2__ctor_m1_5621(__this, method) (( void (*) (Dictionary_2_t1_933 *, const MethodInfo*))Dictionary_2__ctor_m1_5621_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<ExitGames.Client.Photon.ConnectionProtocol,System.Int32>::.ctor(System.Collections.Generic.IEqualityComparer`1<TKey>)
extern "C" void Dictionary_2__ctor_m1_9356_gshared (Dictionary_2_t1_933 * __this, Object_t* ___comparer, const MethodInfo* method);
#define Dictionary_2__ctor_m1_9356(__this, ___comparer, method) (( void (*) (Dictionary_2_t1_933 *, Object_t*, const MethodInfo*))Dictionary_2__ctor_m1_9356_gshared)(__this, ___comparer, method)
// System.Void System.Collections.Generic.Dictionary`2<ExitGames.Client.Photon.ConnectionProtocol,System.Int32>::.ctor(System.Int32)
extern "C" void Dictionary_2__ctor_m1_9357_gshared (Dictionary_2_t1_933 * __this, int32_t ___capacity, const MethodInfo* method);
#define Dictionary_2__ctor_m1_9357(__this, ___capacity, method) (( void (*) (Dictionary_2_t1_933 *, int32_t, const MethodInfo*))Dictionary_2__ctor_m1_9357_gshared)(__this, ___capacity, method)
// System.Void System.Collections.Generic.Dictionary`2<ExitGames.Client.Photon.ConnectionProtocol,System.Int32>::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C" void Dictionary_2__ctor_m1_9358_gshared (Dictionary_2_t1_933 * __this, SerializationInfo_t1_177 * ___info, StreamingContext_t1_514  ___context, const MethodInfo* method);
#define Dictionary_2__ctor_m1_9358(__this, ___info, ___context, method) (( void (*) (Dictionary_2_t1_933 *, SerializationInfo_t1_177 *, StreamingContext_t1_514 , const MethodInfo*))Dictionary_2__ctor_m1_9358_gshared)(__this, ___info, ___context, method)
// System.Collections.ICollection System.Collections.Generic.Dictionary`2<ExitGames.Client.Photon.ConnectionProtocol,System.Int32>::System.Collections.IDictionary.get_Keys()
extern "C" Object_t * Dictionary_2_System_Collections_IDictionary_get_Keys_m1_9359_gshared (Dictionary_2_t1_933 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_get_Keys_m1_9359(__this, method) (( Object_t * (*) (Dictionary_2_t1_933 *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_get_Keys_m1_9359_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2<ExitGames.Client.Photon.ConnectionProtocol,System.Int32>::System.Collections.IDictionary.get_Item(System.Object)
extern "C" Object_t * Dictionary_2_System_Collections_IDictionary_get_Item_m1_9360_gshared (Dictionary_2_t1_933 * __this, Object_t * ___key, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_get_Item_m1_9360(__this, ___key, method) (( Object_t * (*) (Dictionary_2_t1_933 *, Object_t *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_get_Item_m1_9360_gshared)(__this, ___key, method)
// System.Void System.Collections.Generic.Dictionary`2<ExitGames.Client.Photon.ConnectionProtocol,System.Int32>::System.Collections.IDictionary.set_Item(System.Object,System.Object)
extern "C" void Dictionary_2_System_Collections_IDictionary_set_Item_m1_9361_gshared (Dictionary_2_t1_933 * __this, Object_t * ___key, Object_t * ___value, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_set_Item_m1_9361(__this, ___key, ___value, method) (( void (*) (Dictionary_2_t1_933 *, Object_t *, Object_t *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_set_Item_m1_9361_gshared)(__this, ___key, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<ExitGames.Client.Photon.ConnectionProtocol,System.Int32>::System.Collections.IDictionary.Add(System.Object,System.Object)
extern "C" void Dictionary_2_System_Collections_IDictionary_Add_m1_9362_gshared (Dictionary_2_t1_933 * __this, Object_t * ___key, Object_t * ___value, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_Add_m1_9362(__this, ___key, ___value, method) (( void (*) (Dictionary_2_t1_933 *, Object_t *, Object_t *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Add_m1_9362_gshared)(__this, ___key, ___value, method)
// System.Boolean System.Collections.Generic.Dictionary`2<ExitGames.Client.Photon.ConnectionProtocol,System.Int32>::System.Collections.IDictionary.Contains(System.Object)
extern "C" bool Dictionary_2_System_Collections_IDictionary_Contains_m1_9363_gshared (Dictionary_2_t1_933 * __this, Object_t * ___key, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_Contains_m1_9363(__this, ___key, method) (( bool (*) (Dictionary_2_t1_933 *, Object_t *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Contains_m1_9363_gshared)(__this, ___key, method)
// System.Void System.Collections.Generic.Dictionary`2<ExitGames.Client.Photon.ConnectionProtocol,System.Int32>::System.Collections.IDictionary.Remove(System.Object)
extern "C" void Dictionary_2_System_Collections_IDictionary_Remove_m1_9364_gshared (Dictionary_2_t1_933 * __this, Object_t * ___key, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_Remove_m1_9364(__this, ___key, method) (( void (*) (Dictionary_2_t1_933 *, Object_t *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Remove_m1_9364_gshared)(__this, ___key, method)
// System.Object System.Collections.Generic.Dictionary`2<ExitGames.Client.Photon.ConnectionProtocol,System.Int32>::System.Collections.ICollection.get_SyncRoot()
extern "C" Object_t * Dictionary_2_System_Collections_ICollection_get_SyncRoot_m1_9365_gshared (Dictionary_2_t1_933 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_ICollection_get_SyncRoot_m1_9365(__this, method) (( Object_t * (*) (Dictionary_2_t1_933 *, const MethodInfo*))Dictionary_2_System_Collections_ICollection_get_SyncRoot_m1_9365_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<ExitGames.Client.Photon.ConnectionProtocol,System.Int32>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.get_IsReadOnly()
extern "C" bool Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m1_9366_gshared (Dictionary_2_t1_933 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m1_9366(__this, method) (( bool (*) (Dictionary_2_t1_933 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m1_9366_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<ExitGames.Client.Photon.ConnectionProtocol,System.Int32>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Add(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C" void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m1_9367_gshared (Dictionary_2_t1_933 * __this, KeyValuePair_2_t1_1294  ___keyValuePair, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m1_9367(__this, ___keyValuePair, method) (( void (*) (Dictionary_2_t1_933 *, KeyValuePair_2_t1_1294 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m1_9367_gshared)(__this, ___keyValuePair, method)
// System.Boolean System.Collections.Generic.Dictionary`2<ExitGames.Client.Photon.ConnectionProtocol,System.Int32>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Contains(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C" bool Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m1_9368_gshared (Dictionary_2_t1_933 * __this, KeyValuePair_2_t1_1294  ___keyValuePair, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m1_9368(__this, ___keyValuePair, method) (( bool (*) (Dictionary_2_t1_933 *, KeyValuePair_2_t1_1294 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m1_9368_gshared)(__this, ___keyValuePair, method)
// System.Void System.Collections.Generic.Dictionary`2<ExitGames.Client.Photon.ConnectionProtocol,System.Int32>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.CopyTo(System.Collections.Generic.KeyValuePair`2<TKey,TValue>[],System.Int32)
extern "C" void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m1_9369_gshared (Dictionary_2_t1_933 * __this, KeyValuePair_2U5BU5D_t1_1460* ___array, int32_t ___index, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m1_9369(__this, ___array, ___index, method) (( void (*) (Dictionary_2_t1_933 *, KeyValuePair_2U5BU5D_t1_1460*, int32_t, const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m1_9369_gshared)(__this, ___array, ___index, method)
// System.Boolean System.Collections.Generic.Dictionary`2<ExitGames.Client.Photon.ConnectionProtocol,System.Int32>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Remove(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C" bool Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m1_9370_gshared (Dictionary_2_t1_933 * __this, KeyValuePair_2_t1_1294  ___keyValuePair, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m1_9370(__this, ___keyValuePair, method) (( bool (*) (Dictionary_2_t1_933 *, KeyValuePair_2_t1_1294 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m1_9370_gshared)(__this, ___keyValuePair, method)
// System.Void System.Collections.Generic.Dictionary`2<ExitGames.Client.Photon.ConnectionProtocol,System.Int32>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C" void Dictionary_2_System_Collections_ICollection_CopyTo_m1_9371_gshared (Dictionary_2_t1_933 * __this, Array_t * ___array, int32_t ___index, const MethodInfo* method);
#define Dictionary_2_System_Collections_ICollection_CopyTo_m1_9371(__this, ___array, ___index, method) (( void (*) (Dictionary_2_t1_933 *, Array_t *, int32_t, const MethodInfo*))Dictionary_2_System_Collections_ICollection_CopyTo_m1_9371_gshared)(__this, ___array, ___index, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2<ExitGames.Client.Photon.ConnectionProtocol,System.Int32>::System.Collections.IEnumerable.GetEnumerator()
extern "C" Object_t * Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m1_9372_gshared (Dictionary_2_t1_933 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m1_9372(__this, method) (( Object_t * (*) (Dictionary_2_t1_933 *, const MethodInfo*))Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m1_9372_gshared)(__this, method)
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<TKey,TValue>> System.Collections.Generic.Dictionary`2<ExitGames.Client.Photon.ConnectionProtocol,System.Int32>::System.Collections.Generic.IEnumerable<System.Collections.Generic.KeyValuePair<TKey,TValue>>.GetEnumerator()
extern "C" Object_t* Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m1_9373_gshared (Dictionary_2_t1_933 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m1_9373(__this, method) (( Object_t* (*) (Dictionary_2_t1_933 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m1_9373_gshared)(__this, method)
// System.Collections.IDictionaryEnumerator System.Collections.Generic.Dictionary`2<ExitGames.Client.Photon.ConnectionProtocol,System.Int32>::System.Collections.IDictionary.GetEnumerator()
extern "C" Object_t * Dictionary_2_System_Collections_IDictionary_GetEnumerator_m1_9374_gshared (Dictionary_2_t1_933 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_GetEnumerator_m1_9374(__this, method) (( Object_t * (*) (Dictionary_2_t1_933 *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_GetEnumerator_m1_9374_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2<ExitGames.Client.Photon.ConnectionProtocol,System.Int32>::get_Count()
extern "C" int32_t Dictionary_2_get_Count_m1_9375_gshared (Dictionary_2_t1_933 * __this, const MethodInfo* method);
#define Dictionary_2_get_Count_m1_9375(__this, method) (( int32_t (*) (Dictionary_2_t1_933 *, const MethodInfo*))Dictionary_2_get_Count_m1_9375_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2<ExitGames.Client.Photon.ConnectionProtocol,System.Int32>::get_Item(TKey)
extern "C" int32_t Dictionary_2_get_Item_m1_9376_gshared (Dictionary_2_t1_933 * __this, uint8_t ___key, const MethodInfo* method);
#define Dictionary_2_get_Item_m1_9376(__this, ___key, method) (( int32_t (*) (Dictionary_2_t1_933 *, uint8_t, const MethodInfo*))Dictionary_2_get_Item_m1_9376_gshared)(__this, ___key, method)
// System.Void System.Collections.Generic.Dictionary`2<ExitGames.Client.Photon.ConnectionProtocol,System.Int32>::set_Item(TKey,TValue)
extern "C" void Dictionary_2_set_Item_m1_9377_gshared (Dictionary_2_t1_933 * __this, uint8_t ___key, int32_t ___value, const MethodInfo* method);
#define Dictionary_2_set_Item_m1_9377(__this, ___key, ___value, method) (( void (*) (Dictionary_2_t1_933 *, uint8_t, int32_t, const MethodInfo*))Dictionary_2_set_Item_m1_9377_gshared)(__this, ___key, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<ExitGames.Client.Photon.ConnectionProtocol,System.Int32>::Init(System.Int32,System.Collections.Generic.IEqualityComparer`1<TKey>)
extern "C" void Dictionary_2_Init_m1_9378_gshared (Dictionary_2_t1_933 * __this, int32_t ___capacity, Object_t* ___hcp, const MethodInfo* method);
#define Dictionary_2_Init_m1_9378(__this, ___capacity, ___hcp, method) (( void (*) (Dictionary_2_t1_933 *, int32_t, Object_t*, const MethodInfo*))Dictionary_2_Init_m1_9378_gshared)(__this, ___capacity, ___hcp, method)
// System.Void System.Collections.Generic.Dictionary`2<ExitGames.Client.Photon.ConnectionProtocol,System.Int32>::InitArrays(System.Int32)
extern "C" void Dictionary_2_InitArrays_m1_9379_gshared (Dictionary_2_t1_933 * __this, int32_t ___size, const MethodInfo* method);
#define Dictionary_2_InitArrays_m1_9379(__this, ___size, method) (( void (*) (Dictionary_2_t1_933 *, int32_t, const MethodInfo*))Dictionary_2_InitArrays_m1_9379_gshared)(__this, ___size, method)
// System.Void System.Collections.Generic.Dictionary`2<ExitGames.Client.Photon.ConnectionProtocol,System.Int32>::CopyToCheck(System.Array,System.Int32)
extern "C" void Dictionary_2_CopyToCheck_m1_9380_gshared (Dictionary_2_t1_933 * __this, Array_t * ___array, int32_t ___index, const MethodInfo* method);
#define Dictionary_2_CopyToCheck_m1_9380(__this, ___array, ___index, method) (( void (*) (Dictionary_2_t1_933 *, Array_t *, int32_t, const MethodInfo*))Dictionary_2_CopyToCheck_m1_9380_gshared)(__this, ___array, ___index, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2<ExitGames.Client.Photon.ConnectionProtocol,System.Int32>::make_pair(TKey,TValue)
extern "C" KeyValuePair_2_t1_1294  Dictionary_2_make_pair_m1_9381_gshared (Object_t * __this /* static, unused */, uint8_t ___key, int32_t ___value, const MethodInfo* method);
#define Dictionary_2_make_pair_m1_9381(__this /* static, unused */, ___key, ___value, method) (( KeyValuePair_2_t1_1294  (*) (Object_t * /* static, unused */, uint8_t, int32_t, const MethodInfo*))Dictionary_2_make_pair_m1_9381_gshared)(__this /* static, unused */, ___key, ___value, method)
// TKey System.Collections.Generic.Dictionary`2<ExitGames.Client.Photon.ConnectionProtocol,System.Int32>::pick_key(TKey,TValue)
extern "C" uint8_t Dictionary_2_pick_key_m1_9382_gshared (Object_t * __this /* static, unused */, uint8_t ___key, int32_t ___value, const MethodInfo* method);
#define Dictionary_2_pick_key_m1_9382(__this /* static, unused */, ___key, ___value, method) (( uint8_t (*) (Object_t * /* static, unused */, uint8_t, int32_t, const MethodInfo*))Dictionary_2_pick_key_m1_9382_gshared)(__this /* static, unused */, ___key, ___value, method)
// TValue System.Collections.Generic.Dictionary`2<ExitGames.Client.Photon.ConnectionProtocol,System.Int32>::pick_value(TKey,TValue)
extern "C" int32_t Dictionary_2_pick_value_m1_9383_gshared (Object_t * __this /* static, unused */, uint8_t ___key, int32_t ___value, const MethodInfo* method);
#define Dictionary_2_pick_value_m1_9383(__this /* static, unused */, ___key, ___value, method) (( int32_t (*) (Object_t * /* static, unused */, uint8_t, int32_t, const MethodInfo*))Dictionary_2_pick_value_m1_9383_gshared)(__this /* static, unused */, ___key, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<ExitGames.Client.Photon.ConnectionProtocol,System.Int32>::CopyTo(System.Collections.Generic.KeyValuePair`2<TKey,TValue>[],System.Int32)
extern "C" void Dictionary_2_CopyTo_m1_9384_gshared (Dictionary_2_t1_933 * __this, KeyValuePair_2U5BU5D_t1_1460* ___array, int32_t ___index, const MethodInfo* method);
#define Dictionary_2_CopyTo_m1_9384(__this, ___array, ___index, method) (( void (*) (Dictionary_2_t1_933 *, KeyValuePair_2U5BU5D_t1_1460*, int32_t, const MethodInfo*))Dictionary_2_CopyTo_m1_9384_gshared)(__this, ___array, ___index, method)
// System.Void System.Collections.Generic.Dictionary`2<ExitGames.Client.Photon.ConnectionProtocol,System.Int32>::Resize()
extern "C" void Dictionary_2_Resize_m1_9385_gshared (Dictionary_2_t1_933 * __this, const MethodInfo* method);
#define Dictionary_2_Resize_m1_9385(__this, method) (( void (*) (Dictionary_2_t1_933 *, const MethodInfo*))Dictionary_2_Resize_m1_9385_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<ExitGames.Client.Photon.ConnectionProtocol,System.Int32>::Add(TKey,TValue)
extern "C" void Dictionary_2_Add_m1_9386_gshared (Dictionary_2_t1_933 * __this, uint8_t ___key, int32_t ___value, const MethodInfo* method);
#define Dictionary_2_Add_m1_9386(__this, ___key, ___value, method) (( void (*) (Dictionary_2_t1_933 *, uint8_t, int32_t, const MethodInfo*))Dictionary_2_Add_m1_9386_gshared)(__this, ___key, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<ExitGames.Client.Photon.ConnectionProtocol,System.Int32>::Clear()
extern "C" void Dictionary_2_Clear_m1_9387_gshared (Dictionary_2_t1_933 * __this, const MethodInfo* method);
#define Dictionary_2_Clear_m1_9387(__this, method) (( void (*) (Dictionary_2_t1_933 *, const MethodInfo*))Dictionary_2_Clear_m1_9387_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<ExitGames.Client.Photon.ConnectionProtocol,System.Int32>::ContainsKey(TKey)
extern "C" bool Dictionary_2_ContainsKey_m1_9388_gshared (Dictionary_2_t1_933 * __this, uint8_t ___key, const MethodInfo* method);
#define Dictionary_2_ContainsKey_m1_9388(__this, ___key, method) (( bool (*) (Dictionary_2_t1_933 *, uint8_t, const MethodInfo*))Dictionary_2_ContainsKey_m1_9388_gshared)(__this, ___key, method)
// System.Boolean System.Collections.Generic.Dictionary`2<ExitGames.Client.Photon.ConnectionProtocol,System.Int32>::ContainsValue(TValue)
extern "C" bool Dictionary_2_ContainsValue_m1_9389_gshared (Dictionary_2_t1_933 * __this, int32_t ___value, const MethodInfo* method);
#define Dictionary_2_ContainsValue_m1_9389(__this, ___value, method) (( bool (*) (Dictionary_2_t1_933 *, int32_t, const MethodInfo*))Dictionary_2_ContainsValue_m1_9389_gshared)(__this, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<ExitGames.Client.Photon.ConnectionProtocol,System.Int32>::GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C" void Dictionary_2_GetObjectData_m1_9390_gshared (Dictionary_2_t1_933 * __this, SerializationInfo_t1_177 * ___info, StreamingContext_t1_514  ___context, const MethodInfo* method);
#define Dictionary_2_GetObjectData_m1_9390(__this, ___info, ___context, method) (( void (*) (Dictionary_2_t1_933 *, SerializationInfo_t1_177 *, StreamingContext_t1_514 , const MethodInfo*))Dictionary_2_GetObjectData_m1_9390_gshared)(__this, ___info, ___context, method)
// System.Void System.Collections.Generic.Dictionary`2<ExitGames.Client.Photon.ConnectionProtocol,System.Int32>::OnDeserialization(System.Object)
extern "C" void Dictionary_2_OnDeserialization_m1_9391_gshared (Dictionary_2_t1_933 * __this, Object_t * ___sender, const MethodInfo* method);
#define Dictionary_2_OnDeserialization_m1_9391(__this, ___sender, method) (( void (*) (Dictionary_2_t1_933 *, Object_t *, const MethodInfo*))Dictionary_2_OnDeserialization_m1_9391_gshared)(__this, ___sender, method)
// System.Boolean System.Collections.Generic.Dictionary`2<ExitGames.Client.Photon.ConnectionProtocol,System.Int32>::Remove(TKey)
extern "C" bool Dictionary_2_Remove_m1_9392_gshared (Dictionary_2_t1_933 * __this, uint8_t ___key, const MethodInfo* method);
#define Dictionary_2_Remove_m1_9392(__this, ___key, method) (( bool (*) (Dictionary_2_t1_933 *, uint8_t, const MethodInfo*))Dictionary_2_Remove_m1_9392_gshared)(__this, ___key, method)
// System.Boolean System.Collections.Generic.Dictionary`2<ExitGames.Client.Photon.ConnectionProtocol,System.Int32>::TryGetValue(TKey,TValue&)
extern "C" bool Dictionary_2_TryGetValue_m1_9393_gshared (Dictionary_2_t1_933 * __this, uint8_t ___key, int32_t* ___value, const MethodInfo* method);
#define Dictionary_2_TryGetValue_m1_9393(__this, ___key, ___value, method) (( bool (*) (Dictionary_2_t1_933 *, uint8_t, int32_t*, const MethodInfo*))Dictionary_2_TryGetValue_m1_9393_gshared)(__this, ___key, ___value, method)
// System.Collections.Generic.Dictionary`2/KeyCollection<TKey,TValue> System.Collections.Generic.Dictionary`2<ExitGames.Client.Photon.ConnectionProtocol,System.Int32>::get_Keys()
extern "C" KeyCollection_t1_1297 * Dictionary_2_get_Keys_m1_9394_gshared (Dictionary_2_t1_933 * __this, const MethodInfo* method);
#define Dictionary_2_get_Keys_m1_9394(__this, method) (( KeyCollection_t1_1297 * (*) (Dictionary_2_t1_933 *, const MethodInfo*))Dictionary_2_get_Keys_m1_9394_gshared)(__this, method)
// System.Collections.Generic.Dictionary`2/ValueCollection<TKey,TValue> System.Collections.Generic.Dictionary`2<ExitGames.Client.Photon.ConnectionProtocol,System.Int32>::get_Values()
extern "C" ValueCollection_t1_1301 * Dictionary_2_get_Values_m1_9395_gshared (Dictionary_2_t1_933 * __this, const MethodInfo* method);
#define Dictionary_2_get_Values_m1_9395(__this, method) (( ValueCollection_t1_1301 * (*) (Dictionary_2_t1_933 *, const MethodInfo*))Dictionary_2_get_Values_m1_9395_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2<ExitGames.Client.Photon.ConnectionProtocol,System.Int32>::ToTKey(System.Object)
extern "C" uint8_t Dictionary_2_ToTKey_m1_9396_gshared (Dictionary_2_t1_933 * __this, Object_t * ___key, const MethodInfo* method);
#define Dictionary_2_ToTKey_m1_9396(__this, ___key, method) (( uint8_t (*) (Dictionary_2_t1_933 *, Object_t *, const MethodInfo*))Dictionary_2_ToTKey_m1_9396_gshared)(__this, ___key, method)
// TValue System.Collections.Generic.Dictionary`2<ExitGames.Client.Photon.ConnectionProtocol,System.Int32>::ToTValue(System.Object)
extern "C" int32_t Dictionary_2_ToTValue_m1_9397_gshared (Dictionary_2_t1_933 * __this, Object_t * ___value, const MethodInfo* method);
#define Dictionary_2_ToTValue_m1_9397(__this, ___value, method) (( int32_t (*) (Dictionary_2_t1_933 *, Object_t *, const MethodInfo*))Dictionary_2_ToTValue_m1_9397_gshared)(__this, ___value, method)
// System.Boolean System.Collections.Generic.Dictionary`2<ExitGames.Client.Photon.ConnectionProtocol,System.Int32>::ContainsKeyValuePair(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C" bool Dictionary_2_ContainsKeyValuePair_m1_9398_gshared (Dictionary_2_t1_933 * __this, KeyValuePair_2_t1_1294  ___pair, const MethodInfo* method);
#define Dictionary_2_ContainsKeyValuePair_m1_9398(__this, ___pair, method) (( bool (*) (Dictionary_2_t1_933 *, KeyValuePair_2_t1_1294 , const MethodInfo*))Dictionary_2_ContainsKeyValuePair_m1_9398_gshared)(__this, ___pair, method)
// System.Collections.Generic.Dictionary`2/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2<ExitGames.Client.Photon.ConnectionProtocol,System.Int32>::GetEnumerator()
extern "C" Enumerator_t1_1299  Dictionary_2_GetEnumerator_m1_9399_gshared (Dictionary_2_t1_933 * __this, const MethodInfo* method);
#define Dictionary_2_GetEnumerator_m1_9399(__this, method) (( Enumerator_t1_1299  (*) (Dictionary_2_t1_933 *, const MethodInfo*))Dictionary_2_GetEnumerator_m1_9399_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2<ExitGames.Client.Photon.ConnectionProtocol,System.Int32>::<CopyTo>m__0(TKey,TValue)
extern "C" DictionaryEntry_t1_167  Dictionary_2_U3CCopyToU3Em__0_m1_9400_gshared (Object_t * __this /* static, unused */, uint8_t ___key, int32_t ___value, const MethodInfo* method);
#define Dictionary_2_U3CCopyToU3Em__0_m1_9400(__this /* static, unused */, ___key, ___value, method) (( DictionaryEntry_t1_167  (*) (Object_t * /* static, unused */, uint8_t, int32_t, const MethodInfo*))Dictionary_2_U3CCopyToU3Em__0_m1_9400_gshared)(__this /* static, unused */, ___key, ___value, method)
