﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.EqualityComparer`1/DefaultComparer<PunTeams/Team>
struct DefaultComparer_t1_1330;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_PunTeams_Team.h"

// System.Void System.Collections.Generic.EqualityComparer`1/DefaultComparer<PunTeams/Team>::.ctor()
extern "C" void DefaultComparer__ctor_m1_9762_gshared (DefaultComparer_t1_1330 * __this, const MethodInfo* method);
#define DefaultComparer__ctor_m1_9762(__this, method) (( void (*) (DefaultComparer_t1_1330 *, const MethodInfo*))DefaultComparer__ctor_m1_9762_gshared)(__this, method)
// System.Int32 System.Collections.Generic.EqualityComparer`1/DefaultComparer<PunTeams/Team>::GetHashCode(T)
extern "C" int32_t DefaultComparer_GetHashCode_m1_9763_gshared (DefaultComparer_t1_1330 * __this, uint8_t ___obj, const MethodInfo* method);
#define DefaultComparer_GetHashCode_m1_9763(__this, ___obj, method) (( int32_t (*) (DefaultComparer_t1_1330 *, uint8_t, const MethodInfo*))DefaultComparer_GetHashCode_m1_9763_gshared)(__this, ___obj, method)
// System.Boolean System.Collections.Generic.EqualityComparer`1/DefaultComparer<PunTeams/Team>::Equals(T,T)
extern "C" bool DefaultComparer_Equals_m1_9764_gshared (DefaultComparer_t1_1330 * __this, uint8_t ___x, uint8_t ___y, const MethodInfo* method);
#define DefaultComparer_Equals_m1_9764(__this, ___x, ___y, method) (( bool (*) (DefaultComparer_t1_1330 *, uint8_t, uint8_t, const MethodInfo*))DefaultComparer_Equals_m1_9764_gshared)(__this, ___x, ___y, method)
