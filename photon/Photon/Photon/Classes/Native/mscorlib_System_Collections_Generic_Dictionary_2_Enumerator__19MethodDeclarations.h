﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2<PunTeams/Team,System.Object>
struct Dictionary_2_t1_1316;
// System.Object
struct Object_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Enumerator__19.h"
#include "mscorlib_System_Collections_DictionaryEntry.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_18.h"
#include "AssemblyU2DCSharp_PunTeams_Team.h"

// System.Void System.Collections.Generic.Dictionary`2/Enumerator<PunTeams/Team,System.Object>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C" void Enumerator__ctor_m1_9624_gshared (Enumerator_t1_1315 * __this, Dictionary_2_t1_1316 * ___dictionary, const MethodInfo* method);
#define Enumerator__ctor_m1_9624(__this, ___dictionary, method) (( void (*) (Enumerator_t1_1315 *, Dictionary_2_t1_1316 *, const MethodInfo*))Enumerator__ctor_m1_9624_gshared)(__this, ___dictionary, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<PunTeams/Team,System.Object>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * Enumerator_System_Collections_IEnumerator_get_Current_m1_9625_gshared (Enumerator_t1_1315 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m1_9625(__this, method) (( Object_t * (*) (Enumerator_t1_1315 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m1_9625_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<PunTeams/Team,System.Object>::System.Collections.IEnumerator.Reset()
extern "C" void Enumerator_System_Collections_IEnumerator_Reset_m1_9626_gshared (Enumerator_t1_1315 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_Reset_m1_9626(__this, method) (( void (*) (Enumerator_t1_1315 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m1_9626_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<PunTeams/Team,System.Object>::System.Collections.IDictionaryEnumerator.get_Entry()
extern "C" DictionaryEntry_t1_167  Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m1_9627_gshared (Enumerator_t1_1315 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m1_9627(__this, method) (( DictionaryEntry_t1_167  (*) (Enumerator_t1_1315 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m1_9627_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<PunTeams/Team,System.Object>::System.Collections.IDictionaryEnumerator.get_Key()
extern "C" Object_t * Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m1_9628_gshared (Enumerator_t1_1315 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m1_9628(__this, method) (( Object_t * (*) (Enumerator_t1_1315 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m1_9628_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<PunTeams/Team,System.Object>::System.Collections.IDictionaryEnumerator.get_Value()
extern "C" Object_t * Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m1_9629_gshared (Enumerator_t1_1315 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m1_9629(__this, method) (( Object_t * (*) (Enumerator_t1_1315 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m1_9629_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<PunTeams/Team,System.Object>::MoveNext()
extern "C" bool Enumerator_MoveNext_m1_9630_gshared (Enumerator_t1_1315 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m1_9630(__this, method) (( bool (*) (Enumerator_t1_1315 *, const MethodInfo*))Enumerator_MoveNext_m1_9630_gshared)(__this, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<PunTeams/Team,System.Object>::get_Current()
extern "C" KeyValuePair_2_t1_1317  Enumerator_get_Current_m1_9631_gshared (Enumerator_t1_1315 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m1_9631(__this, method) (( KeyValuePair_2_t1_1317  (*) (Enumerator_t1_1315 *, const MethodInfo*))Enumerator_get_Current_m1_9631_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/Enumerator<PunTeams/Team,System.Object>::get_CurrentKey()
extern "C" uint8_t Enumerator_get_CurrentKey_m1_9632_gshared (Enumerator_t1_1315 * __this, const MethodInfo* method);
#define Enumerator_get_CurrentKey_m1_9632(__this, method) (( uint8_t (*) (Enumerator_t1_1315 *, const MethodInfo*))Enumerator_get_CurrentKey_m1_9632_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/Enumerator<PunTeams/Team,System.Object>::get_CurrentValue()
extern "C" Object_t * Enumerator_get_CurrentValue_m1_9633_gshared (Enumerator_t1_1315 * __this, const MethodInfo* method);
#define Enumerator_get_CurrentValue_m1_9633(__this, method) (( Object_t * (*) (Enumerator_t1_1315 *, const MethodInfo*))Enumerator_get_CurrentValue_m1_9633_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<PunTeams/Team,System.Object>::Reset()
extern "C" void Enumerator_Reset_m1_9634_gshared (Enumerator_t1_1315 * __this, const MethodInfo* method);
#define Enumerator_Reset_m1_9634(__this, method) (( void (*) (Enumerator_t1_1315 *, const MethodInfo*))Enumerator_Reset_m1_9634_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<PunTeams/Team,System.Object>::VerifyState()
extern "C" void Enumerator_VerifyState_m1_9635_gshared (Enumerator_t1_1315 * __this, const MethodInfo* method);
#define Enumerator_VerifyState_m1_9635(__this, method) (( void (*) (Enumerator_t1_1315 *, const MethodInfo*))Enumerator_VerifyState_m1_9635_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<PunTeams/Team,System.Object>::VerifyCurrent()
extern "C" void Enumerator_VerifyCurrent_m1_9636_gshared (Enumerator_t1_1315 * __this, const MethodInfo* method);
#define Enumerator_VerifyCurrent_m1_9636(__this, method) (( void (*) (Enumerator_t1_1315 *, const MethodInfo*))Enumerator_VerifyCurrent_m1_9636_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<PunTeams/Team,System.Object>::Dispose()
extern "C" void Enumerator_Dispose_m1_9637_gshared (Enumerator_t1_1315 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m1_9637(__this, method) (( void (*) (Enumerator_t1_1315 *, const MethodInfo*))Enumerator_Dispose_m1_9637_gshared)(__this, method)
