﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// OnClickRequestOwnership
struct OnClickRequestOwnership_t8_12;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Vector3.h"

// System.Void OnClickRequestOwnership::.ctor()
extern "C" void OnClickRequestOwnership__ctor_m8_28 (OnClickRequestOwnership_t8_12 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OnClickRequestOwnership::OnClick()
extern "C" void OnClickRequestOwnership_OnClick_m8_29 (OnClickRequestOwnership_t8_12 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OnClickRequestOwnership::ColorRpc(UnityEngine.Vector3)
extern "C" void OnClickRequestOwnership_ColorRpc_m8_30 (OnClickRequestOwnership_t8_12 * __this, Vector3_t6_49  ___col, const MethodInfo* method) IL2CPP_METHOD_ATTR;
