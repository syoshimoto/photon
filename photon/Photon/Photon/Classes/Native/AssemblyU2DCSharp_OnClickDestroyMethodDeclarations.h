﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// OnClickDestroy
struct OnClickDestroy_t8_157;
// System.Collections.IEnumerator
struct IEnumerator_t1_130;

#include "codegen/il2cpp-codegen.h"

// System.Void OnClickDestroy::.ctor()
extern "C" void OnClickDestroy__ctor_m8_969 (OnClickDestroy_t8_157 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OnClickDestroy::OnClick()
extern "C" void OnClickDestroy_OnClick_m8_970 (OnClickDestroy_t8_157 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator OnClickDestroy::DestroyRpc()
extern "C" Object_t * OnClickDestroy_DestroyRpc_m8_971 (OnClickDestroy_t8_157 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
