﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Photon.SocketServer.Security.DiffieHellmanCryptoProvider
struct DiffieHellmanCryptoProvider_t5_4;
// System.Byte[]
struct ByteU5BU5D_t1_71;
// Photon.SocketServer.Numeric.BigInteger
struct BigInteger_t5_3;

#include "codegen/il2cpp-codegen.h"

// System.Void Photon.SocketServer.Security.DiffieHellmanCryptoProvider::.ctor()
extern "C" void DiffieHellmanCryptoProvider__ctor_m5_45 (DiffieHellmanCryptoProvider_t5_4 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] Photon.SocketServer.Security.DiffieHellmanCryptoProvider::get_PublicKey()
extern "C" ByteU5BU5D_t1_71* DiffieHellmanCryptoProvider_get_PublicKey_m5_46 (DiffieHellmanCryptoProvider_t5_4 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] Photon.SocketServer.Security.DiffieHellmanCryptoProvider::get_SharedKey()
extern "C" ByteU5BU5D_t1_71* DiffieHellmanCryptoProvider_get_SharedKey_m5_47 (DiffieHellmanCryptoProvider_t5_4 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Photon.SocketServer.Security.DiffieHellmanCryptoProvider::DeriveSharedKey(System.Byte[])
extern "C" void DiffieHellmanCryptoProvider_DeriveSharedKey_m5_48 (DiffieHellmanCryptoProvider_t5_4 * __this, ByteU5BU5D_t1_71* ___otherPartyPublicKey, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] Photon.SocketServer.Security.DiffieHellmanCryptoProvider::Encrypt(System.Byte[])
extern "C" ByteU5BU5D_t1_71* DiffieHellmanCryptoProvider_Encrypt_m5_49 (DiffieHellmanCryptoProvider_t5_4 * __this, ByteU5BU5D_t1_71* ___data, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] Photon.SocketServer.Security.DiffieHellmanCryptoProvider::Encrypt(System.Byte[],System.Int32,System.Int32)
extern "C" ByteU5BU5D_t1_71* DiffieHellmanCryptoProvider_Encrypt_m5_50 (DiffieHellmanCryptoProvider_t5_4 * __this, ByteU5BU5D_t1_71* ___data, int32_t ___offset, int32_t ___count, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] Photon.SocketServer.Security.DiffieHellmanCryptoProvider::Decrypt(System.Byte[],System.Int32,System.Int32)
extern "C" ByteU5BU5D_t1_71* DiffieHellmanCryptoProvider_Decrypt_m5_51 (DiffieHellmanCryptoProvider_t5_4 * __this, ByteU5BU5D_t1_71* ___data, int32_t ___offset, int32_t ___count, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Photon.SocketServer.Security.DiffieHellmanCryptoProvider::Dispose()
extern "C" void DiffieHellmanCryptoProvider_Dispose_m5_52 (DiffieHellmanCryptoProvider_t5_4 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Photon.SocketServer.Security.DiffieHellmanCryptoProvider::Dispose(System.Boolean)
extern "C" void DiffieHellmanCryptoProvider_Dispose_m5_53 (DiffieHellmanCryptoProvider_t5_4 * __this, bool ___disposing, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Photon.SocketServer.Numeric.BigInteger Photon.SocketServer.Security.DiffieHellmanCryptoProvider::CalculatePublicKey()
extern "C" BigInteger_t5_3 * DiffieHellmanCryptoProvider_CalculatePublicKey_m5_54 (DiffieHellmanCryptoProvider_t5_4 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Photon.SocketServer.Numeric.BigInteger Photon.SocketServer.Security.DiffieHellmanCryptoProvider::CalculateSharedKey(Photon.SocketServer.Numeric.BigInteger)
extern "C" BigInteger_t5_3 * DiffieHellmanCryptoProvider_CalculateSharedKey_m5_55 (DiffieHellmanCryptoProvider_t5_4 * __this, BigInteger_t5_3 * ___otherPartyPublicKey, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Photon.SocketServer.Numeric.BigInteger Photon.SocketServer.Security.DiffieHellmanCryptoProvider::GenerateRandomSecret(System.Int32)
extern "C" BigInteger_t5_3 * DiffieHellmanCryptoProvider_GenerateRandomSecret_m5_56 (DiffieHellmanCryptoProvider_t5_4 * __this, int32_t ___secretLength, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Photon.SocketServer.Security.DiffieHellmanCryptoProvider::.cctor()
extern "C" void DiffieHellmanCryptoProvider__cctor_m5_57 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
