﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>

// UnityEngine.CharacterController
struct CharacterController_t6_99;
// UnityEngine.Collider2D
struct Collider2D_t6_111;
// UnityEngine.Rigidbody2D
struct Rigidbody2D_t6_113;
// UnityEngine.AudioSettings/AudioConfigurationChangeHandler
struct AudioConfigurationChangeHandler_t6_117;
// System.Object
struct Object_t;
// System.IAsyncResult
struct IAsyncResult_t1_27;
// System.AsyncCallback
struct AsyncCallback_t1_28;
// UnityEngine.AudioClip/PCMReaderCallback
struct PCMReaderCallback_t6_119;
// System.Single[]
struct SingleU5BU5D_t1_863;
// UnityEngine.AudioClip/PCMSetPositionCallback
struct PCMSetPositionCallback_t6_120;
// UnityEngine.AudioClip
struct AudioClip_t6_121;
// UnityEngine.AudioSource
struct AudioSource_t6_122;
// System.String
struct String_t;
// UnityEngine.AnimationEvent
struct AnimationEvent_t6_126;
// UnityEngine.Object
struct Object_t6_5;
struct Object_t6_5_marshaled;
// UnityEngine.AnimationState
struct AnimationState_t6_127;
// UnityEngine.AnimationCurve
struct AnimationCurve_t6_132;
struct AnimationCurve_t6_132_marshaled;
// UnityEngine.Keyframe[]
struct KeyframeU5BU5D_t6_259;
// UnityEngine.Animation/Enumerator
struct Enumerator_t6_134;
// UnityEngine.Animation
struct Animation_t6_135;
// System.Collections.IEnumerator
struct IEnumerator_t1_130;
// UnityEngine.Animator
struct Animator_t6_138;
// UnityEngine.TextMesh
struct TextMesh_t6_145;
// UnityEngine.Font
struct Font_t6_148;
// UnityEngine.Font/FontTextureRebuildCallback
struct FontTextureRebuildCallback_t6_147;
// System.String[]
struct StringU5BU5D_t1_202;
// System.Action`1<UnityEngine.Font>
struct Action_1_t1_912;
// UnityEngine.Material
struct Material_t6_67;
// UnityEngine.CharacterInfo[]
struct CharacterInfoU5BU5D_t6_260;
// UnityEngine.TextGenerator
struct TextGenerator_t6_151;
// UnityEngine.UIVertex[]
struct UIVertexU5BU5D_t6_261;
// UnityEngine.UICharInfo[]
struct UICharInfoU5BU5D_t6_262;
// UnityEngine.UILineInfo[]
struct UILineInfoU5BU5D_t6_263;
// System.Collections.Generic.List`1<UnityEngine.UICharInfo>
struct List_1_t1_914;
// System.Collections.Generic.List`1<UnityEngine.UILineInfo>
struct List_1_t1_915;
// System.Collections.Generic.List`1<UnityEngine.UIVertex>
struct List_1_t1_913;
// System.Collections.Generic.IList`1<UnityEngine.UIVertex>
struct IList_1_t1_923;
// System.Collections.Generic.IList`1<UnityEngine.UICharInfo>
struct IList_1_t1_924;
// System.Collections.Generic.IList`1<UnityEngine.UILineInfo>
struct IList_1_t1_925;
// UnityEngine.Event
struct Event_t6_154;
struct Event_t6_154_marshaled;
// UnityEngine.GUI/ScrollViewState
struct ScrollViewState_t6_158;
// UnityEngine.GUI/WindowFunction
struct WindowFunction_t6_159;
// UnityEngine.GUISkin
struct GUISkin_t6_161;
// UnityEngine.GUIStyle
struct GUIStyle_t6_166;
// UnityEngine.GUIContent
struct GUIContent_t6_163;
// UnityEngine.Texture
struct Texture_t6_32;
// UnityEngine.TextEditor
struct TextEditor_t6_238;
// UnityEngine.GUIContent[]
struct GUIContentU5BU5D_t6_264;
// UnityEngine.Rect[]
struct RectU5BU5D_t6_265;
// UnityEngine.GUILayout/LayoutedWindow
struct LayoutedWindow_t6_164;
// UnityEngine.GUILayoutOption[]
struct GUILayoutOptionU5BU5D_t6_165;
// UnityEngine.GUILayoutOption
struct GUILayoutOption_t6_176;
// UnityEngine.GUILayoutUtility/LayoutCache
struct LayoutCache_t6_168;
// UnityEngine.GUILayoutGroup
struct GUILayoutGroup_t6_169;
// System.Type
struct Type_t;
// UnityEngine.GUILayoutEntry
struct GUILayoutEntry_t6_171;
// UnityEngine.RectOffset
struct RectOffset_t6_172;
// UnityEngine.GUIScrollGroup
struct GUIScrollGroup_t6_173;
// UnityEngine.GUIWordWrapSizer
struct GUIWordWrapSizer_t6_174;
// UnityEngine.GUISettings
struct GUISettings_t6_177;
// UnityEngine.GUISkin/SkinChangedDelegate
struct SkinChangedDelegate_t6_178;
// UnityEngine.GUIStyle[]
struct GUIStyleU5BU5D_t6_179;
// UnityEngine.GUIStyleState
struct GUIStyleState_t6_180;
// UnityEngine.Texture2D
struct Texture2D_t6_33;
// System.Exception
struct Exception_t1_33;
// UnityEngine.WrapperlessIcall
struct WrapperlessIcall_t6_189;
// UnityEngine.IL2CPPStructAlignmentAttribute
struct IL2CPPStructAlignmentAttribute_t6_190;
// System.Type[]
struct TypeU5BU5D_t1_31;
// UnityEngine.RequireComponent
struct RequireComponent_t6_196;
// UnityEngine.AddComponentMenu
struct AddComponentMenu_t6_197;
// UnityEngine.ExecuteInEditMode
struct ExecuteInEditMode_t6_198;
// UnityEngine.HideInInspector
struct HideInInspector_t6_199;
// UnityEngine.SetupCoroutine
struct SetupCoroutine_t6_200;
// UnityEngine.WritableAttribute
struct WritableAttribute_t6_201;
// UnityEngine.AssemblyIsEditorAssembly
struct AssemblyIsEditorAssembly_t6_202;
// UnityEngine.SocialPlatforms.Impl.UserProfile
struct UserProfile_t6_213;
// UnityEngine.SocialPlatforms.Impl.UserProfile[]
struct UserProfileU5BU5D_t6_20;
// UnityEngine.SocialPlatforms.Impl.AchievementDescription
struct AchievementDescription_t6_216;
// UnityEngine.SocialPlatforms.Impl.Achievement
struct Achievement_t6_215;
// UnityEngine.SocialPlatforms.Impl.Score
struct Score_t6_217;
// UnityEngine.SocialPlatforms.Impl.LocalUser
struct LocalUser_t6_21;
// UnityEngine.SocialPlatforms.IUserProfile[]
struct IUserProfileU5BU5D_t6_214;

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array.h"
#include "UnityEngine_UnityEngine_CharacterController.h"
#include "UnityEngine_UnityEngine_CharacterControllerMethodDeclarations.h"
#include "UnityEngine_UnityEngine_CollisionFlags.h"
#include "UnityEngine_UnityEngine_Vector3.h"
#include "UnityEngine_UnityEngine_Physics2D.h"
#include "UnityEngine_UnityEngine_Physics2DMethodDeclarations.h"
#include "mscorlib_System_Void.h"
#include "mscorlib_System_Collections_Generic_List_1_gen_5MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen_5.h"
#include "UnityEngine_UnityEngine_Vector2.h"
#include "UnityEngine_UnityEngine_RaycastHit2D.h"
#include "mscorlib_System_Single.h"
#include "mscorlib_System_Int32.h"
#include "UnityEngine_UnityEngine_RaycastHit2DMethodDeclarations.h"
#include "UnityEngine_UnityEngine_Collider2D.h"
#include "UnityEngine_UnityEngine_ForceMode2D.h"
#include "UnityEngine_UnityEngine_ForceMode2DMethodDeclarations.h"
#include "UnityEngine_UnityEngine_Rigidbody2D.h"
#include "UnityEngine_UnityEngine_Rigidbody2DMethodDeclarations.h"
#include "mscorlib_System_Boolean.h"
#include "UnityEngine_UnityEngine_Collider2DMethodDeclarations.h"
#include "UnityEngine_UnityEngine_ContactPoint2D.h"
#include "UnityEngine_UnityEngine_ContactPoint2DMethodDeclarations.h"
#include "UnityEngine_UnityEngine_Collision2D.h"
#include "UnityEngine_UnityEngine_Collision2DMethodDeclarations.h"
#include "UnityEngine_UnityEngine_AudioSettings_AudioConfigurationChan.h"
#include "UnityEngine_UnityEngine_AudioSettings_AudioConfigurationChanMethodDeclarations.h"
#include "mscorlib_System_IntPtr.h"
#include "mscorlib_System_Object.h"
#include "mscorlib_System_AsyncCallback.h"
#include "UnityEngine_UnityEngine_AudioSettings.h"
#include "UnityEngine_UnityEngine_AudioSettingsMethodDeclarations.h"
#include "UnityEngine_UnityEngine_AudioClip_PCMReaderCallback.h"
#include "UnityEngine_UnityEngine_AudioClip_PCMReaderCallbackMethodDeclarations.h"
#include "mscorlib_ArrayTypes.h"
#include "UnityEngine_UnityEngine_AudioClip_PCMSetPositionCallback.h"
#include "UnityEngine_UnityEngine_AudioClip_PCMSetPositionCallbackMethodDeclarations.h"
#include "UnityEngine_UnityEngine_AudioClip.h"
#include "UnityEngine_UnityEngine_AudioClipMethodDeclarations.h"
#include "UnityEngine_UnityEngine_AudioSource.h"
#include "UnityEngine_UnityEngine_AudioSourceMethodDeclarations.h"
#include "mscorlib_System_UInt64.h"
#include "UnityEngine_UnityEngine_WebCamDevice.h"
#include "UnityEngine_UnityEngine_WebCamDeviceMethodDeclarations.h"
#include "mscorlib_System_String.h"
#include "UnityEngine_UnityEngine_WrapMode.h"
#include "UnityEngine_UnityEngine_WrapModeMethodDeclarations.h"
#include "UnityEngine_UnityEngine_AnimationEventSource.h"
#include "UnityEngine_UnityEngine_AnimationEventSourceMethodDeclarations.h"
#include "UnityEngine_UnityEngine_AnimationEvent.h"
#include "UnityEngine_UnityEngine_AnimationEventMethodDeclarations.h"
#include "mscorlib_System_ObjectMethodDeclarations.h"
#include "mscorlib_System_StringMethodDeclarations.h"
#include "UnityEngine_UnityEngine_Object.h"
#include "UnityEngine_UnityEngine_AnimationState.h"
#include "UnityEngine_UnityEngine_SendMessageOptions.h"
#include "UnityEngine_UnityEngine_DebugMethodDeclarations.h"
#include "UnityEngine_UnityEngine_AnimatorStateInfo.h"
#include "UnityEngine_UnityEngine_AnimatorClipInfo.h"
#include "mscorlib_System_SingleMethodDeclarations.h"
#include "UnityEngine_UnityEngine_AnimationClip.h"
#include "UnityEngine_UnityEngine_AnimationClipMethodDeclarations.h"
#include "UnityEngine_UnityEngine_Keyframe.h"
#include "UnityEngine_UnityEngine_KeyframeMethodDeclarations.h"
#include "UnityEngine_UnityEngine_AnimationCurve.h"
#include "UnityEngine_UnityEngine_AnimationCurveMethodDeclarations.h"
#include "UnityEngine_ArrayTypes.h"
#include "UnityEngine_UnityEngine_PlayMode.h"
#include "UnityEngine_UnityEngine_PlayModeMethodDeclarations.h"
#include "UnityEngine_UnityEngine_Animation_Enumerator.h"
#include "UnityEngine_UnityEngine_Animation_EnumeratorMethodDeclarations.h"
#include "UnityEngine_UnityEngine_Animation.h"
#include "UnityEngine_UnityEngine_AnimationMethodDeclarations.h"
#include "UnityEngine_UnityEngine_AnimationStateMethodDeclarations.h"
#include "UnityEngine_UnityEngine_AnimatorClipInfoMethodDeclarations.h"
#include "UnityEngine_UnityEngine_AnimatorStateInfoMethodDeclarations.h"
#include "UnityEngine_UnityEngine_AnimatorMethodDeclarations.h"
#include "UnityEngine_UnityEngine_AnimatorTransitionInfo.h"
#include "UnityEngine_UnityEngine_AnimatorTransitionInfoMethodDeclarations.h"
#include "UnityEngine_UnityEngine_Animator.h"
#include "UnityEngine_UnityEngine_SkeletonBone.h"
#include "UnityEngine_UnityEngine_SkeletonBoneMethodDeclarations.h"
#include "UnityEngine_UnityEngine_Quaternion.h"
#include "UnityEngine_UnityEngine_HumanLimit.h"
#include "UnityEngine_UnityEngine_HumanLimitMethodDeclarations.h"
#include "UnityEngine_UnityEngine_HumanBone.h"
#include "UnityEngine_UnityEngine_HumanBoneMethodDeclarations.h"
#include "UnityEngine_UnityEngine_TextAnchor.h"
#include "UnityEngine_UnityEngine_TextAnchorMethodDeclarations.h"
#include "UnityEngine_UnityEngine_HorizontalWrapMode.h"
#include "UnityEngine_UnityEngine_HorizontalWrapModeMethodDeclarations.h"
#include "UnityEngine_UnityEngine_VerticalWrapMode.h"
#include "UnityEngine_UnityEngine_VerticalWrapModeMethodDeclarations.h"
#include "UnityEngine_UnityEngine_TextMesh.h"
#include "UnityEngine_UnityEngine_TextMeshMethodDeclarations.h"
#include "UnityEngine_UnityEngine_Font.h"
#include "UnityEngine_UnityEngine_CharacterInfo.h"
#include "UnityEngine_UnityEngine_CharacterInfoMethodDeclarations.h"
#include "UnityEngine_UnityEngine_RectMethodDeclarations.h"
#include "UnityEngine_UnityEngine_Rect.h"
#include "UnityEngine_UnityEngine_Vector2MethodDeclarations.h"
#include "UnityEngine_UnityEngine_FontStyle.h"
#include "UnityEngine_UnityEngine_Font_FontTextureRebuildCallback.h"
#include "UnityEngine_UnityEngine_Font_FontTextureRebuildCallbackMethodDeclarations.h"
#include "UnityEngine_UnityEngine_FontMethodDeclarations.h"
#include "UnityEngine_UnityEngine_ObjectMethodDeclarations.h"
#include "mscorlib_System_Action_1_gen_4.h"
#include "mscorlib_System_DelegateMethodDeclarations.h"
#include "mscorlib_System_Delegate.h"
#include "UnityEngine_UnityEngine_Material.h"
#include "mscorlib_System_Char.h"
#include "mscorlib_System_Action_1_gen_4MethodDeclarations.h"
#include "UnityEngine_UnityEngine_UICharInfo.h"
#include "UnityEngine_UnityEngine_UICharInfoMethodDeclarations.h"
#include "UnityEngine_UnityEngine_UILineInfo.h"
#include "UnityEngine_UnityEngine_UILineInfoMethodDeclarations.h"
#include "UnityEngine_UnityEngine_TextGenerator.h"
#include "UnityEngine_UnityEngine_TextGeneratorMethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen_6MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen_7MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen_8MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen_6.h"
#include "mscorlib_System_Collections_Generic_List_1_gen_7.h"
#include "mscorlib_System_Collections_Generic_List_1_gen_8.h"
#include "UnityEngine_UnityEngine_Color.h"
#include "UnityEngine_UnityEngine_UIVertex.h"
#include "UnityEngine_UnityEngine_MathfMethodDeclarations.h"
#include "UnityEngine_UnityEngine_TextGenerationSettings.h"
#include "UnityEngine_UnityEngine_TextGenerationSettingsMethodDeclarations.h"
#include "UnityEngine_UnityEngine_UIVertexMethodDeclarations.h"
#include "UnityEngine_UnityEngine_Color32MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Vector4MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Vector3MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Color32.h"
#include "mscorlib_System_Byte.h"
#include "UnityEngine_UnityEngine_Vector4.h"
#include "UnityEngine_UnityEngine_Event.h"
#include "UnityEngine_UnityEngine_EventMethodDeclarations.h"
#include "mscorlib_System_ArgumentExceptionMethodDeclarations.h"
#include "mscorlib_System_ArgumentException.h"
#include "UnityEngine_UnityEngine_Ray.h"
#include "UnityEngine_UnityEngine_RayMethodDeclarations.h"
#include "UnityEngine_UnityEngine_EventModifiers.h"
#include "UnityEngine_UnityEngine_EventType.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_genMethodDeclarations.h"
#include "mscorlib_System_TypeMethodDeclarations.h"
#include "mscorlib_System_EnumMethodDeclarations.h"
#include "UnityEngine_UnityEngine_UnityStringMethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_gen.h"
#include "UnityEngine_UnityEngine_KeyCode.h"
#include "mscorlib_System_Type.h"
#include "mscorlib_System_RuntimeTypeHandle.h"
#include "UnityEngine_UnityEngine_KeyCodeMethodDeclarations.h"
#include "UnityEngine_UnityEngine_EventTypeMethodDeclarations.h"
#include "UnityEngine_UnityEngine_EventModifiersMethodDeclarations.h"
#include "UnityEngine_UnityEngine_GUI_ScrollViewState.h"
#include "UnityEngine_UnityEngine_GUI_ScrollViewStateMethodDeclarations.h"
#include "UnityEngine_UnityEngine_GUI_WindowFunction.h"
#include "UnityEngine_UnityEngine_GUI_WindowFunctionMethodDeclarations.h"
#include "UnityEngine_UnityEngine_GUI.h"
#include "UnityEngine_UnityEngine_GUIMethodDeclarations.h"
#include "UnityEngine_UnityEngineInternal_GenericStackMethodDeclarations.h"
#include "mscorlib_System_DateTimeMethodDeclarations.h"
#include "UnityEngine_UnityEngineInternal_GenericStack.h"
#include "mscorlib_System_DateTime.h"
#include "UnityEngine_UnityEngine_GUISkin.h"
#include "UnityEngine_UnityEngine_GUIUtilityMethodDeclarations.h"
#include "UnityEngine_UnityEngine_GUISkinMethodDeclarations.h"
#include "UnityEngine_UnityEngine_GUIContentMethodDeclarations.h"
#include "UnityEngine_UnityEngine_GUIContent.h"
#include "UnityEngine_UnityEngine_GUIStyle.h"
#include "UnityEngine_UnityEngine_GUIStyleMethodDeclarations.h"
#include "UnityEngine_UnityEngine_FocusType.h"
#include "UnityEngine_UnityEngine_Texture.h"
#include "UnityEngine_UnityEngine_TextEditorMethodDeclarations.h"
#include "UnityEngine_UnityEngine_TouchScreenKeyboardMethodDeclarations.h"
#include "UnityEngine_UnityEngine_TextEditor.h"
#include "UnityEngine_UnityEngine_TouchScreenKeyboard.h"
#include "UnityEngine_UnityEngine_TouchScreenKeyboardType.h"
#include "UnityEngine_UnityEngine_GUISettingsMethodDeclarations.h"
#include "UnityEngine_UnityEngine_InputMethodDeclarations.h"
#include "UnityEngine_UnityEngine_GUISettings.h"
#include "UnityEngine_UnityEngine_TextEditor_DblClickSnapping.h"
#include "UnityEngine_UnityEngine_RectOffsetMethodDeclarations.h"
#include "UnityEngine_UnityEngine_RectOffset.h"
#include "UnityEngine_UnityEngine_GUIClipMethodDeclarations.h"
#include "UnityEngine_UnityEngine_SliderHandlerMethodDeclarations.h"
#include "UnityEngine_UnityEngine_SliderHandler.h"
#include "mscorlib_System_Double.h"
#include "mscorlib_System_Collections_Stack.h"
#include "mscorlib_System_Collections_StackMethodDeclarations.h"
#include "UnityEngine_UnityEngine_GUILayoutUtilityMethodDeclarations.h"
#include "UnityEngine_UnityEngine_GUILayoutMethodDeclarations.h"
#include "UnityEngine_UnityEngine_GUILayoutOption.h"
#include "UnityEngine_UnityEngine_GUILayoutUtility_LayoutCache.h"
#include "UnityEngine_UnityEngine_GUILayout_LayoutedWindow.h"
#include "UnityEngine_UnityEngine_GUILayout_LayoutedWindowMethodDeclarations.h"
#include "UnityEngine_UnityEngine_GUILayoutEntryMethodDeclarations.h"
#include "UnityEngine_UnityEngine_GUILayoutGroupMethodDeclarations.h"
#include "UnityEngine_UnityEngine_GUILayoutGroup.h"
#include "UnityEngine_UnityEngine_GUILayoutUtility.h"
#include "UnityEngine_UnityEngine_GUILayoutEntry.h"
#include "UnityEngine_UnityEngine_GUILayout.h"
#include "UnityEngine_UnityEngine_GUIScrollGroup.h"
#include "UnityEngine_UnityEngine_GUILayoutOptionMethodDeclarations.h"
#include "UnityEngine_UnityEngine_GUILayoutOption_Type.h"
#include "UnityEngine_UnityEngine_GUILayoutUtility_LayoutCacheMethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_gen_7MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_gen_7.h"
#include "UnityEngine_UnityEngine_ScreenMethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen_9MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_2MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_2.h"
#include "mscorlib_System_Collections_Generic_List_1_gen_9.h"
#include "mscorlib_System_ActivatorMethodDeclarations.h"
#include "UnityEngine_UnityEngine_GUIWordWrapSizerMethodDeclarations.h"
#include "UnityEngine_UnityEngine_GUIWordWrapSizer.h"
#include "UnityEngine_UnityEngine_GUIScrollGroupMethodDeclarations.h"
#include "UnityEngine_UnityEngine_GUILayoutOption_TypeMethodDeclarations.h"
#include "UnityEngine_UnityEngine_ColorMethodDeclarations.h"
#include "UnityEngine_UnityEngine_GUISkin_SkinChangedDelegate.h"
#include "UnityEngine_UnityEngine_GUISkin_SkinChangedDelegateMethodDeclarations.h"
#include "UnityEngine_UnityEngine_ScriptableObjectMethodDeclarations.h"
#include "UnityEngine_UnityEngine_ScriptableObject.h"
#include "mscorlib_System_StringComparerMethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_gen_8MethodDeclarations.h"
#include "UnityEngine_UnityEngine_GUIStyleStateMethodDeclarations.h"
#include "mscorlib_System_StringComparer.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_gen_8.h"
#include "UnityEngine_UnityEngine_GUIStyleState.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_ValueCollec_1MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_ValueCollec_1.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_ValueCollec_2.h"
#include "UnityEngine_UnityEngine_Texture2D.h"
#include "UnityEngine_UnityEngine_FontStyleMethodDeclarations.h"
#include "UnityEngine_UnityEngine_ImagePosition.h"
#include "UnityEngine_UnityEngine_ImagePositionMethodDeclarations.h"
#include "UnityEngine_UnityEngine_Internal_DrawArguments.h"
#include "UnityEngine_UnityEngine_TimeMethodDeclarations.h"
#include "UnityEngine_UnityEngine_Internal_DrawWithTextSelectionArgume.h"
#include "UnityEngine_UnityEngine_ExitGUIException.h"
#include "UnityEngine_UnityEngine_ExitGUIExceptionMethodDeclarations.h"
#include "UnityEngine_UnityEngine_FocusTypeMethodDeclarations.h"
#include "UnityEngine_UnityEngine_GUIUtility.h"
#include "UnityEngine_UnityEngine_GUIStateObjectsMethodDeclarations.h"
#include "mscorlib_System_Exception.h"
#include "mscorlib_System_ExceptionMethodDeclarations.h"
#include "UnityEngine_UnityEngine_GUIClip.h"
#include "UnityEngine_UnityEngine_Internal_DrawArgumentsMethodDeclarations.h"
#include "UnityEngine_UnityEngine_Internal_DrawWithTextSelectionArgumeMethodDeclarations.h"
#include "UnityEngine_UnityEngine_WrapperlessIcall.h"
#include "UnityEngine_UnityEngine_WrapperlessIcallMethodDeclarations.h"
#include "mscorlib_System_AttributeMethodDeclarations.h"
#include "mscorlib_System_Attribute.h"
#include "UnityEngine_UnityEngine_IL2CPPStructAlignmentAttribute.h"
#include "UnityEngine_UnityEngine_IL2CPPStructAlignmentAttributeMethodDeclarations.h"
#include "UnityEngine_UnityEngine_AttributeHelperEngine.h"
#include "UnityEngine_UnityEngine_AttributeHelperEngineMethodDeclarations.h"
#include "UnityEngine_UnityEngine_DisallowMultipleComponent.h"
#include "UnityEngine_UnityEngine_ExecuteInEditMode.h"
#include "UnityEngine_UnityEngine_RequireComponent.h"
#include "System_System_Collections_Generic_Stack_1_genMethodDeclarations.h"
#include "System_System_Collections_Generic_Stack_1_gen.h"
#include "mscorlib_System_Reflection_MemberInfo.h"
#include "mscorlib_System_Reflection_MemberInfoMethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen_10MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen_10.h"
#include "UnityEngine_UnityEngine_DisallowMultipleComponentMethodDeclarations.h"
#include "UnityEngine_UnityEngine_RequireComponentMethodDeclarations.h"
#include "UnityEngine_UnityEngine_AddComponentMenu.h"
#include "UnityEngine_UnityEngine_AddComponentMenuMethodDeclarations.h"
#include "UnityEngine_UnityEngine_ExecuteInEditModeMethodDeclarations.h"
#include "UnityEngine_UnityEngine_HideInInspector.h"
#include "UnityEngine_UnityEngine_HideInInspectorMethodDeclarations.h"
#include "UnityEngine_UnityEngine_SetupCoroutine.h"
#include "UnityEngine_UnityEngine_SetupCoroutineMethodDeclarations.h"
#include "mscorlib_System_Reflection_BindingFlags.h"
#include "mscorlib_System_Reflection_Binder.h"
#include "mscorlib_System_Reflection_ParameterModifier.h"
#include "mscorlib_System_Globalization_CultureInfo.h"
#include "UnityEngine_UnityEngine_WritableAttribute.h"
#include "UnityEngine_UnityEngine_WritableAttributeMethodDeclarations.h"
#include "UnityEngine_UnityEngine_AssemblyIsEditorAssembly.h"
#include "UnityEngine_UnityEngine_AssemblyIsEditorAssemblyMethodDeclarations.h"
#include "UnityEngine_UnityEngine_SocialPlatforms_GameCenter_GcUserPro.h"
#include "UnityEngine_UnityEngine_SocialPlatforms_GameCenter_GcUserProMethodDeclarations.h"
#include "UnityEngine_UnityEngine_SocialPlatforms_Impl_UserProfile.h"
#include "UnityEngine_UnityEngine_SocialPlatforms_Impl_UserProfileMethodDeclarations.h"
#include "UnityEngine_UnityEngine_SocialPlatforms_UserState.h"
#include "UnityEngine_UnityEngine_SocialPlatforms_GameCenter_GcAchieve.h"
#include "UnityEngine_UnityEngine_SocialPlatforms_GameCenter_GcAchieveMethodDeclarations.h"
#include "UnityEngine_UnityEngine_SocialPlatforms_Impl_AchievementDesc.h"
#include "UnityEngine_UnityEngine_SocialPlatforms_Impl_AchievementDescMethodDeclarations.h"
#include "UnityEngine_UnityEngine_SocialPlatforms_GameCenter_GcAchieve_0.h"
#include "UnityEngine_UnityEngine_SocialPlatforms_GameCenter_GcAchieve_0MethodDeclarations.h"
#include "UnityEngine_UnityEngine_SocialPlatforms_Impl_Achievement.h"
#include "UnityEngine_UnityEngine_SocialPlatforms_Impl_AchievementMethodDeclarations.h"
#include "UnityEngine_UnityEngine_SocialPlatforms_GameCenter_GcScoreDa.h"
#include "UnityEngine_UnityEngine_SocialPlatforms_GameCenter_GcScoreDaMethodDeclarations.h"
#include "UnityEngine_UnityEngine_SocialPlatforms_Impl_Score.h"
#include "UnityEngine_UnityEngine_SocialPlatforms_Impl_ScoreMethodDeclarations.h"
#include "mscorlib_System_Int64.h"
#include "UnityEngine_UnityEngine_Resolution.h"
#include "UnityEngine_UnityEngine_ResolutionMethodDeclarations.h"
#include "UnityEngine_UnityEngine_RenderBuffer.h"
#include "UnityEngine_UnityEngine_RenderBufferMethodDeclarations.h"
#include "UnityEngine_UnityEngine_CameraClearFlags.h"
#include "UnityEngine_UnityEngine_CameraClearFlagsMethodDeclarations.h"
#include "UnityEngine_UnityEngine_TextureFormat.h"
#include "UnityEngine_UnityEngine_TextureFormatMethodDeclarations.h"
#include "UnityEngine_UnityEngine_Rendering_ReflectionProbeBlendInfo.h"
#include "UnityEngine_UnityEngine_Rendering_ReflectionProbeBlendInfoMethodDeclarations.h"
#include "UnityEngine_UnityEngine_GUIStateObjects.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_gen_9MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_gen_9.h"
#include "UnityEngine_UnityEngine_SocialPlatforms_Impl_LocalUser.h"
#include "UnityEngine_UnityEngine_SocialPlatforms_Impl_LocalUserMethodDeclarations.h"
#include "UnityEngine_UnityEngine_Texture2DMethodDeclarations.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// UnityEngine.CollisionFlags UnityEngine.CharacterController::Move(UnityEngine.Vector3)
extern "C" int32_t CharacterController_Move_m6_732 (CharacterController_t6_99 * __this, Vector3_t6_49  ___motion, const MethodInfo* method)
{
	{
		int32_t L_0 = CharacterController_INTERNAL_CALL_Move_m6_733(NULL /*static, unused*/, __this, (&___motion), /*hidden argument*/NULL);
		return L_0;
	}
}
// UnityEngine.CollisionFlags UnityEngine.CharacterController::INTERNAL_CALL_Move(UnityEngine.CharacterController,UnityEngine.Vector3&)
extern "C" int32_t CharacterController_INTERNAL_CALL_Move_m6_733 (Object_t * __this /* static, unused */, CharacterController_t6_99 * ___self, Vector3_t6_49 * ___motion, const MethodInfo* method)
{
	typedef int32_t (*CharacterController_INTERNAL_CALL_Move_m6_733_ftn) (CharacterController_t6_99 *, Vector3_t6_49 *);
	static CharacterController_INTERNAL_CALL_Move_m6_733_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (CharacterController_INTERNAL_CALL_Move_m6_733_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.CharacterController::INTERNAL_CALL_Move(UnityEngine.CharacterController,UnityEngine.Vector3&)");
	return _il2cpp_icall_func(___self, ___motion);
}
// System.Void UnityEngine.Physics2D::.cctor()
extern TypeInfo* List_1_t1_911_il2cpp_TypeInfo_var;
extern TypeInfo* Physics2D_t6_109_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1__ctor_m1_5598_MethodInfo_var;
extern "C" void Physics2D__cctor_m6_734 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		List_1_t1_911_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(965);
		Physics2D_t6_109_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(966);
		List_1__ctor_m1_5598_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483805);
		s_Il2CppMethodIntialized = true;
	}
	{
		List_1_t1_911 * L_0 = (List_1_t1_911 *)il2cpp_codegen_object_new (List_1_t1_911_il2cpp_TypeInfo_var);
		List_1__ctor_m1_5598(L_0, /*hidden argument*/List_1__ctor_m1_5598_MethodInfo_var);
		((Physics2D_t6_109_StaticFields*)Physics2D_t6_109_il2cpp_TypeInfo_var->static_fields)->___m_LastDisabledRigidbody2D_0 = L_0;
		return;
	}
}
// System.Void UnityEngine.Physics2D::Internal_Raycast(UnityEngine.Vector2,UnityEngine.Vector2,System.Single,System.Int32,System.Single,System.Single,UnityEngine.RaycastHit2D&)
extern TypeInfo* Physics2D_t6_109_il2cpp_TypeInfo_var;
extern "C" void Physics2D_Internal_Raycast_m6_735 (Object_t * __this /* static, unused */, Vector2_t6_48  ___origin, Vector2_t6_48  ___direction, float ___distance, int32_t ___layerMask, float ___minDepth, float ___maxDepth, RaycastHit2D_t6_110 * ___raycastHit, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Physics2D_t6_109_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(966);
		s_Il2CppMethodIntialized = true;
	}
	{
		float L_0 = ___distance;
		int32_t L_1 = ___layerMask;
		float L_2 = ___minDepth;
		float L_3 = ___maxDepth;
		RaycastHit2D_t6_110 * L_4 = ___raycastHit;
		IL2CPP_RUNTIME_CLASS_INIT(Physics2D_t6_109_il2cpp_TypeInfo_var);
		Physics2D_INTERNAL_CALL_Internal_Raycast_m6_736(NULL /*static, unused*/, (&___origin), (&___direction), L_0, L_1, L_2, L_3, L_4, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Physics2D::INTERNAL_CALL_Internal_Raycast(UnityEngine.Vector2&,UnityEngine.Vector2&,System.Single,System.Int32,System.Single,System.Single,UnityEngine.RaycastHit2D&)
extern "C" void Physics2D_INTERNAL_CALL_Internal_Raycast_m6_736 (Object_t * __this /* static, unused */, Vector2_t6_48 * ___origin, Vector2_t6_48 * ___direction, float ___distance, int32_t ___layerMask, float ___minDepth, float ___maxDepth, RaycastHit2D_t6_110 * ___raycastHit, const MethodInfo* method)
{
	typedef void (*Physics2D_INTERNAL_CALL_Internal_Raycast_m6_736_ftn) (Vector2_t6_48 *, Vector2_t6_48 *, float, int32_t, float, float, RaycastHit2D_t6_110 *);
	static Physics2D_INTERNAL_CALL_Internal_Raycast_m6_736_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Physics2D_INTERNAL_CALL_Internal_Raycast_m6_736_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Physics2D::INTERNAL_CALL_Internal_Raycast(UnityEngine.Vector2&,UnityEngine.Vector2&,System.Single,System.Int32,System.Single,System.Single,UnityEngine.RaycastHit2D&)");
	_il2cpp_icall_func(___origin, ___direction, ___distance, ___layerMask, ___minDepth, ___maxDepth, ___raycastHit);
}
// UnityEngine.RaycastHit2D UnityEngine.Physics2D::Raycast(UnityEngine.Vector2,UnityEngine.Vector2,System.Single)
extern TypeInfo* Physics2D_t6_109_il2cpp_TypeInfo_var;
extern "C" RaycastHit2D_t6_110  Physics2D_Raycast_m6_737 (Object_t * __this /* static, unused */, Vector2_t6_48  ___origin, Vector2_t6_48  ___direction, float ___distance, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Physics2D_t6_109_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(966);
		s_Il2CppMethodIntialized = true;
	}
	float V_0 = 0.0f;
	float V_1 = 0.0f;
	int32_t V_2 = 0;
	{
		V_0 = (std::numeric_limits<float>::infinity());
		V_1 = (-std::numeric_limits<float>::infinity());
		V_2 = ((int32_t)-5);
		Vector2_t6_48  L_0 = ___origin;
		Vector2_t6_48  L_1 = ___direction;
		float L_2 = ___distance;
		int32_t L_3 = V_2;
		float L_4 = V_1;
		float L_5 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Physics2D_t6_109_il2cpp_TypeInfo_var);
		RaycastHit2D_t6_110  L_6 = Physics2D_Raycast_m6_738(NULL /*static, unused*/, L_0, L_1, L_2, L_3, L_4, L_5, /*hidden argument*/NULL);
		return L_6;
	}
}
// UnityEngine.RaycastHit2D UnityEngine.Physics2D::Raycast(UnityEngine.Vector2,UnityEngine.Vector2,System.Single,System.Int32,System.Single,System.Single)
extern TypeInfo* Physics2D_t6_109_il2cpp_TypeInfo_var;
extern "C" RaycastHit2D_t6_110  Physics2D_Raycast_m6_738 (Object_t * __this /* static, unused */, Vector2_t6_48  ___origin, Vector2_t6_48  ___direction, float ___distance, int32_t ___layerMask, float ___minDepth, float ___maxDepth, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Physics2D_t6_109_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(966);
		s_Il2CppMethodIntialized = true;
	}
	RaycastHit2D_t6_110  V_0 = {0};
	{
		Vector2_t6_48  L_0 = ___origin;
		Vector2_t6_48  L_1 = ___direction;
		float L_2 = ___distance;
		int32_t L_3 = ___layerMask;
		float L_4 = ___minDepth;
		float L_5 = ___maxDepth;
		IL2CPP_RUNTIME_CLASS_INIT(Physics2D_t6_109_il2cpp_TypeInfo_var);
		Physics2D_Internal_Raycast_m6_735(NULL /*static, unused*/, L_0, L_1, L_2, L_3, L_4, L_5, (&V_0), /*hidden argument*/NULL);
		RaycastHit2D_t6_110  L_6 = V_0;
		return L_6;
	}
}
// UnityEngine.Collider2D UnityEngine.RaycastHit2D::get_collider()
extern "C" Collider2D_t6_111 * RaycastHit2D_get_collider_m6_739 (RaycastHit2D_t6_110 * __this, const MethodInfo* method)
{
	{
		Collider2D_t6_111 * L_0 = (__this->___m_Collider_5);
		return L_0;
	}
}
// UnityEngine.Vector2 UnityEngine.Rigidbody2D::get_velocity()
extern "C" Vector2_t6_48  Rigidbody2D_get_velocity_m6_740 (Rigidbody2D_t6_113 * __this, const MethodInfo* method)
{
	Vector2_t6_48  V_0 = {0};
	{
		Rigidbody2D_INTERNAL_get_velocity_m6_742(__this, (&V_0), /*hidden argument*/NULL);
		Vector2_t6_48  L_0 = V_0;
		return L_0;
	}
}
// System.Void UnityEngine.Rigidbody2D::set_velocity(UnityEngine.Vector2)
extern "C" void Rigidbody2D_set_velocity_m6_741 (Rigidbody2D_t6_113 * __this, Vector2_t6_48  ___value, const MethodInfo* method)
{
	{
		Rigidbody2D_INTERNAL_set_velocity_m6_743(__this, (&___value), /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Rigidbody2D::INTERNAL_get_velocity(UnityEngine.Vector2&)
extern "C" void Rigidbody2D_INTERNAL_get_velocity_m6_742 (Rigidbody2D_t6_113 * __this, Vector2_t6_48 * ___value, const MethodInfo* method)
{
	typedef void (*Rigidbody2D_INTERNAL_get_velocity_m6_742_ftn) (Rigidbody2D_t6_113 *, Vector2_t6_48 *);
	static Rigidbody2D_INTERNAL_get_velocity_m6_742_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Rigidbody2D_INTERNAL_get_velocity_m6_742_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Rigidbody2D::INTERNAL_get_velocity(UnityEngine.Vector2&)");
	_il2cpp_icall_func(__this, ___value);
}
// System.Void UnityEngine.Rigidbody2D::INTERNAL_set_velocity(UnityEngine.Vector2&)
extern "C" void Rigidbody2D_INTERNAL_set_velocity_m6_743 (Rigidbody2D_t6_113 * __this, Vector2_t6_48 * ___value, const MethodInfo* method)
{
	typedef void (*Rigidbody2D_INTERNAL_set_velocity_m6_743_ftn) (Rigidbody2D_t6_113 *, Vector2_t6_48 *);
	static Rigidbody2D_INTERNAL_set_velocity_m6_743_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Rigidbody2D_INTERNAL_set_velocity_m6_743_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Rigidbody2D::INTERNAL_set_velocity(UnityEngine.Vector2&)");
	_il2cpp_icall_func(__this, ___value);
}
// System.Single UnityEngine.Rigidbody2D::get_angularVelocity()
extern "C" float Rigidbody2D_get_angularVelocity_m6_744 (Rigidbody2D_t6_113 * __this, const MethodInfo* method)
{
	typedef float (*Rigidbody2D_get_angularVelocity_m6_744_ftn) (Rigidbody2D_t6_113 *);
	static Rigidbody2D_get_angularVelocity_m6_744_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Rigidbody2D_get_angularVelocity_m6_744_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Rigidbody2D::get_angularVelocity()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.Rigidbody2D::set_angularVelocity(System.Single)
extern "C" void Rigidbody2D_set_angularVelocity_m6_745 (Rigidbody2D_t6_113 * __this, float ___value, const MethodInfo* method)
{
	typedef void (*Rigidbody2D_set_angularVelocity_m6_745_ftn) (Rigidbody2D_t6_113 *, float);
	static Rigidbody2D_set_angularVelocity_m6_745_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Rigidbody2D_set_angularVelocity_m6_745_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Rigidbody2D::set_angularVelocity(System.Single)");
	_il2cpp_icall_func(__this, ___value);
}
// System.Void UnityEngine.Rigidbody2D::set_isKinematic(System.Boolean)
extern "C" void Rigidbody2D_set_isKinematic_m6_746 (Rigidbody2D_t6_113 * __this, bool ___value, const MethodInfo* method)
{
	typedef void (*Rigidbody2D_set_isKinematic_m6_746_ftn) (Rigidbody2D_t6_113 *, bool);
	static Rigidbody2D_set_isKinematic_m6_746_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Rigidbody2D_set_isKinematic_m6_746_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Rigidbody2D::set_isKinematic(System.Boolean)");
	_il2cpp_icall_func(__this, ___value);
}
// System.Void UnityEngine.Rigidbody2D::AddForce(UnityEngine.Vector2)
extern "C" void Rigidbody2D_AddForce_m6_747 (Rigidbody2D_t6_113 * __this, Vector2_t6_48  ___force, const MethodInfo* method)
{
	int32_t V_0 = {0};
	{
		V_0 = 0;
		int32_t L_0 = V_0;
		Rigidbody2D_INTERNAL_CALL_AddForce_m6_748(NULL /*static, unused*/, __this, (&___force), L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Rigidbody2D::INTERNAL_CALL_AddForce(UnityEngine.Rigidbody2D,UnityEngine.Vector2&,UnityEngine.ForceMode2D)
extern "C" void Rigidbody2D_INTERNAL_CALL_AddForce_m6_748 (Object_t * __this /* static, unused */, Rigidbody2D_t6_113 * ___self, Vector2_t6_48 * ___force, int32_t ___mode, const MethodInfo* method)
{
	typedef void (*Rigidbody2D_INTERNAL_CALL_AddForce_m6_748_ftn) (Rigidbody2D_t6_113 *, Vector2_t6_48 *, int32_t);
	static Rigidbody2D_INTERNAL_CALL_AddForce_m6_748_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Rigidbody2D_INTERNAL_CALL_AddForce_m6_748_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Rigidbody2D::INTERNAL_CALL_AddForce(UnityEngine.Rigidbody2D,UnityEngine.Vector2&,UnityEngine.ForceMode2D)");
	_il2cpp_icall_func(___self, ___force, ___mode);
}
// System.Void UnityEngine.AudioSettings/AudioConfigurationChangeHandler::.ctor(System.Object,System.IntPtr)
extern "C" void AudioConfigurationChangeHandler__ctor_m6_749 (AudioConfigurationChangeHandler_t6_117 * __this, Object_t * ___object, IntPtr_t ___method, const MethodInfo* method)
{
	__this->___method_ptr_0 = (methodPointerType)((MethodInfo*)___method.___m_value_0)->method;
	__this->___method_3 = ___method;
	__this->___m_target_2 = ___object;
}
// System.Void UnityEngine.AudioSettings/AudioConfigurationChangeHandler::Invoke(System.Boolean)
extern "C" void AudioConfigurationChangeHandler_Invoke_m6_750 (AudioConfigurationChangeHandler_t6_117 * __this, bool ___deviceWasChanged, const MethodInfo* method)
{
	if(__this->___prev_9 != NULL)
	{
		AudioConfigurationChangeHandler_Invoke_m6_750((AudioConfigurationChangeHandler_t6_117 *)__this->___prev_9,___deviceWasChanged, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->___method_3.___m_value_0));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->___method_3.___m_value_0));
	if (__this->___m_target_2 != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Object_t *, Object_t * __this, bool ___deviceWasChanged, const MethodInfo* method);
		((FunctionPointerType)__this->___method_ptr_0)(NULL,__this->___m_target_2,___deviceWasChanged,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
	else
	{
		typedef void (*FunctionPointerType) (Object_t * __this, bool ___deviceWasChanged, const MethodInfo* method);
		((FunctionPointerType)__this->___method_ptr_0)(__this->___m_target_2,___deviceWasChanged,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
}
extern "C" void pinvoke_delegate_wrapper_AudioConfigurationChangeHandler_t6_117(Il2CppObject* delegate, bool ___deviceWasChanged)
{
	typedef void (STDCALL *native_function_ptr_type)(int32_t);
	native_function_ptr_type _il2cpp_pinvoke_func = ((native_function_ptr_type)((Il2CppDelegate*)delegate)->method->method);
	// Marshaling of parameter '___deviceWasChanged' to native representation

	// Native function invocation
	_il2cpp_pinvoke_func(___deviceWasChanged);

	// Marshaling cleanup of parameter '___deviceWasChanged' native representation

}
// System.IAsyncResult UnityEngine.AudioSettings/AudioConfigurationChangeHandler::BeginInvoke(System.Boolean,System.AsyncCallback,System.Object)
extern TypeInfo* Boolean_t1_20_il2cpp_TypeInfo_var;
extern "C" Object_t * AudioConfigurationChangeHandler_BeginInvoke_m6_751 (AudioConfigurationChangeHandler_t6_117 * __this, bool ___deviceWasChanged, AsyncCallback_t1_28 * ___callback, Object_t * ___object, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Boolean_t1_20_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(49);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(Boolean_t1_20_il2cpp_TypeInfo_var, &___deviceWasChanged);
	return (Object_t *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback, (Il2CppObject*)___object);
}
// System.Void UnityEngine.AudioSettings/AudioConfigurationChangeHandler::EndInvoke(System.IAsyncResult)
extern "C" void AudioConfigurationChangeHandler_EndInvoke_m6_752 (AudioConfigurationChangeHandler_t6_117 * __this, Object_t * ___result, const MethodInfo* method)
{
	il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result, 0);
}
// System.Void UnityEngine.AudioSettings::InvokeOnAudioConfigurationChanged(System.Boolean)
extern TypeInfo* AudioSettings_t6_118_il2cpp_TypeInfo_var;
extern "C" void AudioSettings_InvokeOnAudioConfigurationChanged_m6_753 (Object_t * __this /* static, unused */, bool ___deviceWasChanged, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		AudioSettings_t6_118_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(967);
		s_Il2CppMethodIntialized = true;
	}
	{
		AudioConfigurationChangeHandler_t6_117 * L_0 = ((AudioSettings_t6_118_StaticFields*)AudioSettings_t6_118_il2cpp_TypeInfo_var->static_fields)->___OnAudioConfigurationChanged_0;
		if (!L_0)
		{
			goto IL_0015;
		}
	}
	{
		AudioConfigurationChangeHandler_t6_117 * L_1 = ((AudioSettings_t6_118_StaticFields*)AudioSettings_t6_118_il2cpp_TypeInfo_var->static_fields)->___OnAudioConfigurationChanged_0;
		bool L_2 = ___deviceWasChanged;
		NullCheck(L_1);
		AudioConfigurationChangeHandler_Invoke_m6_750(L_1, L_2, /*hidden argument*/NULL);
	}

IL_0015:
	{
		return;
	}
}
// System.Void UnityEngine.AudioClip/PCMReaderCallback::.ctor(System.Object,System.IntPtr)
extern "C" void PCMReaderCallback__ctor_m6_754 (PCMReaderCallback_t6_119 * __this, Object_t * ___object, IntPtr_t ___method, const MethodInfo* method)
{
	__this->___method_ptr_0 = (methodPointerType)((MethodInfo*)___method.___m_value_0)->method;
	__this->___method_3 = ___method;
	__this->___m_target_2 = ___object;
}
// System.Void UnityEngine.AudioClip/PCMReaderCallback::Invoke(System.Single[])
extern "C" void PCMReaderCallback_Invoke_m6_755 (PCMReaderCallback_t6_119 * __this, SingleU5BU5D_t1_863* ___data, const MethodInfo* method)
{
	if(__this->___prev_9 != NULL)
	{
		PCMReaderCallback_Invoke_m6_755((PCMReaderCallback_t6_119 *)__this->___prev_9,___data, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->___method_3.___m_value_0));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->___method_3.___m_value_0));
	if (__this->___m_target_2 != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Object_t *, Object_t * __this, SingleU5BU5D_t1_863* ___data, const MethodInfo* method);
		((FunctionPointerType)__this->___method_ptr_0)(NULL,__this->___m_target_2,___data,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
	else if (__this->___m_target_2 != NULL || ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Object_t * __this, SingleU5BU5D_t1_863* ___data, const MethodInfo* method);
		((FunctionPointerType)__this->___method_ptr_0)(__this->___m_target_2,___data,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
	else
	{
		typedef void (*FunctionPointerType) (Object_t * __this, const MethodInfo* method);
		((FunctionPointerType)__this->___method_ptr_0)(___data,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
}
extern "C" void pinvoke_delegate_wrapper_PCMReaderCallback_t6_119(Il2CppObject* delegate, SingleU5BU5D_t1_863* ___data)
{
	typedef void (STDCALL *native_function_ptr_type)(float*);
	native_function_ptr_type _il2cpp_pinvoke_func = ((native_function_ptr_type)((Il2CppDelegate*)delegate)->method->method);
	// Marshaling of parameter '___data' to native representation
	float* ____data_marshaled = { 0 };
	____data_marshaled = il2cpp_codegen_marshal_array<float>((Il2CppCodeGenArray*)___data);

	// Native function invocation
	_il2cpp_pinvoke_func(____data_marshaled);

	// Marshaling cleanup of parameter '___data' native representation

}
// System.IAsyncResult UnityEngine.AudioClip/PCMReaderCallback::BeginInvoke(System.Single[],System.AsyncCallback,System.Object)
extern "C" Object_t * PCMReaderCallback_BeginInvoke_m6_756 (PCMReaderCallback_t6_119 * __this, SingleU5BU5D_t1_863* ___data, AsyncCallback_t1_28 * ___callback, Object_t * ___object, const MethodInfo* method)
{
	void *__d_args[2] = {0};
	__d_args[0] = ___data;
	return (Object_t *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback, (Il2CppObject*)___object);
}
// System.Void UnityEngine.AudioClip/PCMReaderCallback::EndInvoke(System.IAsyncResult)
extern "C" void PCMReaderCallback_EndInvoke_m6_757 (PCMReaderCallback_t6_119 * __this, Object_t * ___result, const MethodInfo* method)
{
	il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result, 0);
}
// System.Void UnityEngine.AudioClip/PCMSetPositionCallback::.ctor(System.Object,System.IntPtr)
extern "C" void PCMSetPositionCallback__ctor_m6_758 (PCMSetPositionCallback_t6_120 * __this, Object_t * ___object, IntPtr_t ___method, const MethodInfo* method)
{
	__this->___method_ptr_0 = (methodPointerType)((MethodInfo*)___method.___m_value_0)->method;
	__this->___method_3 = ___method;
	__this->___m_target_2 = ___object;
}
// System.Void UnityEngine.AudioClip/PCMSetPositionCallback::Invoke(System.Int32)
extern "C" void PCMSetPositionCallback_Invoke_m6_759 (PCMSetPositionCallback_t6_120 * __this, int32_t ___position, const MethodInfo* method)
{
	if(__this->___prev_9 != NULL)
	{
		PCMSetPositionCallback_Invoke_m6_759((PCMSetPositionCallback_t6_120 *)__this->___prev_9,___position, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->___method_3.___m_value_0));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->___method_3.___m_value_0));
	if (__this->___m_target_2 != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Object_t *, Object_t * __this, int32_t ___position, const MethodInfo* method);
		((FunctionPointerType)__this->___method_ptr_0)(NULL,__this->___m_target_2,___position,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
	else
	{
		typedef void (*FunctionPointerType) (Object_t * __this, int32_t ___position, const MethodInfo* method);
		((FunctionPointerType)__this->___method_ptr_0)(__this->___m_target_2,___position,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
}
extern "C" void pinvoke_delegate_wrapper_PCMSetPositionCallback_t6_120(Il2CppObject* delegate, int32_t ___position)
{
	typedef void (STDCALL *native_function_ptr_type)(int32_t);
	native_function_ptr_type _il2cpp_pinvoke_func = ((native_function_ptr_type)((Il2CppDelegate*)delegate)->method->method);
	// Marshaling of parameter '___position' to native representation

	// Native function invocation
	_il2cpp_pinvoke_func(___position);

	// Marshaling cleanup of parameter '___position' native representation

}
// System.IAsyncResult UnityEngine.AudioClip/PCMSetPositionCallback::BeginInvoke(System.Int32,System.AsyncCallback,System.Object)
extern TypeInfo* Int32_t1_3_il2cpp_TypeInfo_var;
extern "C" Object_t * PCMSetPositionCallback_BeginInvoke_m6_760 (PCMSetPositionCallback_t6_120 * __this, int32_t ___position, AsyncCallback_t1_28 * ___callback, Object_t * ___object, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Int32_t1_3_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(8);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(Int32_t1_3_il2cpp_TypeInfo_var, &___position);
	return (Object_t *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback, (Il2CppObject*)___object);
}
// System.Void UnityEngine.AudioClip/PCMSetPositionCallback::EndInvoke(System.IAsyncResult)
extern "C" void PCMSetPositionCallback_EndInvoke_m6_761 (PCMSetPositionCallback_t6_120 * __this, Object_t * ___result, const MethodInfo* method)
{
	il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result, 0);
}
// System.Void UnityEngine.AudioClip::InvokePCMReaderCallback_Internal(System.Single[])
extern "C" void AudioClip_InvokePCMReaderCallback_Internal_m6_762 (AudioClip_t6_121 * __this, SingleU5BU5D_t1_863* ___data, const MethodInfo* method)
{
	{
		PCMReaderCallback_t6_119 * L_0 = (__this->___m_PCMReaderCallback_2);
		if (!L_0)
		{
			goto IL_0017;
		}
	}
	{
		PCMReaderCallback_t6_119 * L_1 = (__this->___m_PCMReaderCallback_2);
		SingleU5BU5D_t1_863* L_2 = ___data;
		NullCheck(L_1);
		PCMReaderCallback_Invoke_m6_755(L_1, L_2, /*hidden argument*/NULL);
	}

IL_0017:
	{
		return;
	}
}
// System.Void UnityEngine.AudioClip::InvokePCMSetPositionCallback_Internal(System.Int32)
extern "C" void AudioClip_InvokePCMSetPositionCallback_Internal_m6_763 (AudioClip_t6_121 * __this, int32_t ___position, const MethodInfo* method)
{
	{
		PCMSetPositionCallback_t6_120 * L_0 = (__this->___m_PCMSetPositionCallback_3);
		if (!L_0)
		{
			goto IL_0017;
		}
	}
	{
		PCMSetPositionCallback_t6_120 * L_1 = (__this->___m_PCMSetPositionCallback_3);
		int32_t L_2 = ___position;
		NullCheck(L_1);
		PCMSetPositionCallback_Invoke_m6_759(L_1, L_2, /*hidden argument*/NULL);
	}

IL_0017:
	{
		return;
	}
}
// System.Void UnityEngine.AudioSource::set_clip(UnityEngine.AudioClip)
extern "C" void AudioSource_set_clip_m6_764 (AudioSource_t6_122 * __this, AudioClip_t6_121 * ___value, const MethodInfo* method)
{
	typedef void (*AudioSource_set_clip_m6_764_ftn) (AudioSource_t6_122 *, AudioClip_t6_121 *);
	static AudioSource_set_clip_m6_764_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (AudioSource_set_clip_m6_764_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.AudioSource::set_clip(UnityEngine.AudioClip)");
	_il2cpp_icall_func(__this, ___value);
}
// System.Void UnityEngine.AudioSource::Play(System.UInt64)
extern "C" void AudioSource_Play_m6_765 (AudioSource_t6_122 * __this, uint64_t ___delay, const MethodInfo* method)
{
	typedef void (*AudioSource_Play_m6_765_ftn) (AudioSource_t6_122 *, uint64_t);
	static AudioSource_Play_m6_765_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (AudioSource_Play_m6_765_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.AudioSource::Play(System.UInt64)");
	_il2cpp_icall_func(__this, ___delay);
}
// System.Void UnityEngine.AudioSource::Play()
extern "C" void AudioSource_Play_m6_766 (AudioSource_t6_122 * __this, const MethodInfo* method)
{
	uint64_t V_0 = 0;
	{
		V_0 = (((int64_t)((int64_t)0)));
		uint64_t L_0 = V_0;
		AudioSource_Play_m6_765(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.String UnityEngine.WebCamDevice::get_name()
extern "C" String_t* WebCamDevice_get_name_m6_767 (WebCamDevice_t6_123 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = (__this->___m_Name_0);
		return L_0;
	}
}
// System.Boolean UnityEngine.WebCamDevice::get_isFrontFacing()
extern "C" bool WebCamDevice_get_isFrontFacing_m6_768 (WebCamDevice_t6_123 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (__this->___m_Flags_1);
		return ((((int32_t)((int32_t)((int32_t)L_0&(int32_t)1))) == ((int32_t)1))? 1 : 0);
	}
}
// Conversion methods for marshalling of: UnityEngine.WebCamDevice
extern "C" void WebCamDevice_t6_123_marshal(const WebCamDevice_t6_123& unmarshaled, WebCamDevice_t6_123_marshaled& marshaled)
{
	marshaled.___m_Name_0 = il2cpp_codegen_marshal_string(unmarshaled.___m_Name_0);
	marshaled.___m_Flags_1 = unmarshaled.___m_Flags_1;
}
extern "C" void WebCamDevice_t6_123_marshal_back(const WebCamDevice_t6_123_marshaled& marshaled, WebCamDevice_t6_123& unmarshaled)
{
	unmarshaled.___m_Name_0 = il2cpp_codegen_marshal_string_result(marshaled.___m_Name_0);
	unmarshaled.___m_Flags_1 = marshaled.___m_Flags_1;
}
// Conversion method for clean up from marshalling of: UnityEngine.WebCamDevice
extern "C" void WebCamDevice_t6_123_marshal_cleanup(WebCamDevice_t6_123_marshaled& marshaled)
{
	il2cpp_codegen_marshal_free(marshaled.___m_Name_0);
	marshaled.___m_Name_0 = NULL;
}
// System.Void UnityEngine.AnimationEvent::.ctor()
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern "C" void AnimationEvent__ctor_m6_769 (AnimationEvent_t6_126 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(11);
		s_Il2CppMethodIntialized = true;
	}
	{
		Object__ctor_m1_0(__this, /*hidden argument*/NULL);
		__this->___m_Time_0 = (0.0f);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_0 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->___Empty_2;
		__this->___m_FunctionName_1 = L_0;
		String_t* L_1 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->___Empty_2;
		__this->___m_StringParameter_2 = L_1;
		__this->___m_ObjectReferenceParameter_3 = (Object_t6_5 *)NULL;
		__this->___m_FloatParameter_4 = (0.0f);
		__this->___m_IntParameter_5 = 0;
		__this->___m_MessageOptions_6 = 0;
		__this->___m_Source_7 = 0;
		__this->___m_StateSender_8 = (AnimationState_t6_127 *)NULL;
		return;
	}
}
// System.String UnityEngine.AnimationEvent::get_data()
extern "C" String_t* AnimationEvent_get_data_m6_770 (AnimationEvent_t6_126 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = (__this->___m_StringParameter_2);
		return L_0;
	}
}
// System.Void UnityEngine.AnimationEvent::set_data(System.String)
extern "C" void AnimationEvent_set_data_m6_771 (AnimationEvent_t6_126 * __this, String_t* ___value, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value;
		__this->___m_StringParameter_2 = L_0;
		return;
	}
}
// System.String UnityEngine.AnimationEvent::get_stringParameter()
extern "C" String_t* AnimationEvent_get_stringParameter_m6_772 (AnimationEvent_t6_126 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = (__this->___m_StringParameter_2);
		return L_0;
	}
}
// System.Void UnityEngine.AnimationEvent::set_stringParameter(System.String)
extern "C" void AnimationEvent_set_stringParameter_m6_773 (AnimationEvent_t6_126 * __this, String_t* ___value, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value;
		__this->___m_StringParameter_2 = L_0;
		return;
	}
}
// System.Single UnityEngine.AnimationEvent::get_floatParameter()
extern "C" float AnimationEvent_get_floatParameter_m6_774 (AnimationEvent_t6_126 * __this, const MethodInfo* method)
{
	{
		float L_0 = (__this->___m_FloatParameter_4);
		return L_0;
	}
}
// System.Void UnityEngine.AnimationEvent::set_floatParameter(System.Single)
extern "C" void AnimationEvent_set_floatParameter_m6_775 (AnimationEvent_t6_126 * __this, float ___value, const MethodInfo* method)
{
	{
		float L_0 = ___value;
		__this->___m_FloatParameter_4 = L_0;
		return;
	}
}
// System.Int32 UnityEngine.AnimationEvent::get_intParameter()
extern "C" int32_t AnimationEvent_get_intParameter_m6_776 (AnimationEvent_t6_126 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (__this->___m_IntParameter_5);
		return L_0;
	}
}
// System.Void UnityEngine.AnimationEvent::set_intParameter(System.Int32)
extern "C" void AnimationEvent_set_intParameter_m6_777 (AnimationEvent_t6_126 * __this, int32_t ___value, const MethodInfo* method)
{
	{
		int32_t L_0 = ___value;
		__this->___m_IntParameter_5 = L_0;
		return;
	}
}
// UnityEngine.Object UnityEngine.AnimationEvent::get_objectReferenceParameter()
extern "C" Object_t6_5 * AnimationEvent_get_objectReferenceParameter_m6_778 (AnimationEvent_t6_126 * __this, const MethodInfo* method)
{
	{
		Object_t6_5 * L_0 = (__this->___m_ObjectReferenceParameter_3);
		return L_0;
	}
}
// System.Void UnityEngine.AnimationEvent::set_objectReferenceParameter(UnityEngine.Object)
extern "C" void AnimationEvent_set_objectReferenceParameter_m6_779 (AnimationEvent_t6_126 * __this, Object_t6_5 * ___value, const MethodInfo* method)
{
	{
		Object_t6_5 * L_0 = ___value;
		__this->___m_ObjectReferenceParameter_3 = L_0;
		return;
	}
}
// System.String UnityEngine.AnimationEvent::get_functionName()
extern "C" String_t* AnimationEvent_get_functionName_m6_780 (AnimationEvent_t6_126 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = (__this->___m_FunctionName_1);
		return L_0;
	}
}
// System.Void UnityEngine.AnimationEvent::set_functionName(System.String)
extern "C" void AnimationEvent_set_functionName_m6_781 (AnimationEvent_t6_126 * __this, String_t* ___value, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value;
		__this->___m_FunctionName_1 = L_0;
		return;
	}
}
// System.Single UnityEngine.AnimationEvent::get_time()
extern "C" float AnimationEvent_get_time_m6_782 (AnimationEvent_t6_126 * __this, const MethodInfo* method)
{
	{
		float L_0 = (__this->___m_Time_0);
		return L_0;
	}
}
// System.Void UnityEngine.AnimationEvent::set_time(System.Single)
extern "C" void AnimationEvent_set_time_m6_783 (AnimationEvent_t6_126 * __this, float ___value, const MethodInfo* method)
{
	{
		float L_0 = ___value;
		__this->___m_Time_0 = L_0;
		return;
	}
}
// UnityEngine.SendMessageOptions UnityEngine.AnimationEvent::get_messageOptions()
extern "C" int32_t AnimationEvent_get_messageOptions_m6_784 (AnimationEvent_t6_126 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (__this->___m_MessageOptions_6);
		return (int32_t)(L_0);
	}
}
// System.Void UnityEngine.AnimationEvent::set_messageOptions(UnityEngine.SendMessageOptions)
extern "C" void AnimationEvent_set_messageOptions_m6_785 (AnimationEvent_t6_126 * __this, int32_t ___value, const MethodInfo* method)
{
	{
		int32_t L_0 = ___value;
		__this->___m_MessageOptions_6 = L_0;
		return;
	}
}
// System.Boolean UnityEngine.AnimationEvent::get_isFiredByLegacy()
extern "C" bool AnimationEvent_get_isFiredByLegacy_m6_786 (AnimationEvent_t6_126 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (__this->___m_Source_7);
		return ((((int32_t)L_0) == ((int32_t)1))? 1 : 0);
	}
}
// System.Boolean UnityEngine.AnimationEvent::get_isFiredByAnimator()
extern "C" bool AnimationEvent_get_isFiredByAnimator_m6_787 (AnimationEvent_t6_126 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (__this->___m_Source_7);
		return ((((int32_t)L_0) == ((int32_t)2))? 1 : 0);
	}
}
// UnityEngine.AnimationState UnityEngine.AnimationEvent::get_animationState()
extern Il2CppCodeGenString* _stringLiteral2710;
extern "C" AnimationState_t6_127 * AnimationEvent_get_animationState_m6_788 (AnimationEvent_t6_126 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		_stringLiteral2710 = il2cpp_codegen_string_literal_from_index(2710);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool L_0 = AnimationEvent_get_isFiredByLegacy_m6_786(__this, /*hidden argument*/NULL);
		if (L_0)
		{
			goto IL_0015;
		}
	}
	{
		Debug_LogError_m6_490(NULL /*static, unused*/, _stringLiteral2710, /*hidden argument*/NULL);
	}

IL_0015:
	{
		AnimationState_t6_127 * L_1 = (__this->___m_StateSender_8);
		return L_1;
	}
}
// UnityEngine.AnimatorStateInfo UnityEngine.AnimationEvent::get_animatorStateInfo()
extern Il2CppCodeGenString* _stringLiteral2711;
extern "C" AnimatorStateInfo_t6_128  AnimationEvent_get_animatorStateInfo_m6_789 (AnimationEvent_t6_126 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		_stringLiteral2711 = il2cpp_codegen_string_literal_from_index(2711);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool L_0 = AnimationEvent_get_isFiredByAnimator_m6_787(__this, /*hidden argument*/NULL);
		if (L_0)
		{
			goto IL_0015;
		}
	}
	{
		Debug_LogError_m6_490(NULL /*static, unused*/, _stringLiteral2711, /*hidden argument*/NULL);
	}

IL_0015:
	{
		AnimatorStateInfo_t6_128  L_1 = (__this->___m_AnimatorStateInfo_9);
		return L_1;
	}
}
// UnityEngine.AnimatorClipInfo UnityEngine.AnimationEvent::get_animatorClipInfo()
extern Il2CppCodeGenString* _stringLiteral2712;
extern "C" AnimatorClipInfo_t6_129  AnimationEvent_get_animatorClipInfo_m6_790 (AnimationEvent_t6_126 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		_stringLiteral2712 = il2cpp_codegen_string_literal_from_index(2712);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool L_0 = AnimationEvent_get_isFiredByAnimator_m6_787(__this, /*hidden argument*/NULL);
		if (L_0)
		{
			goto IL_0015;
		}
	}
	{
		Debug_LogError_m6_490(NULL /*static, unused*/, _stringLiteral2712, /*hidden argument*/NULL);
	}

IL_0015:
	{
		AnimatorClipInfo_t6_129  L_1 = (__this->___m_AnimatorClipInfo_10);
		return L_1;
	}
}
// System.Int32 UnityEngine.AnimationEvent::GetHash()
extern "C" int32_t AnimationEvent_GetHash_m6_791 (AnimationEvent_t6_126 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	float V_1 = 0.0f;
	{
		V_0 = 0;
		String_t* L_0 = AnimationEvent_get_functionName_m6_780(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		int32_t L_1 = String_GetHashCode_m1_433(L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		int32_t L_2 = V_0;
		float L_3 = AnimationEvent_get_time_m6_782(__this, /*hidden argument*/NULL);
		V_1 = L_3;
		int32_t L_4 = Single_GetHashCode_m1_475((&V_1), /*hidden argument*/NULL);
		V_0 = ((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)33)*(int32_t)L_2))+(int32_t)L_4));
		int32_t L_5 = V_0;
		return L_5;
	}
}
// System.Void UnityEngine.Keyframe::.ctor(System.Single,System.Single,System.Single,System.Single)
extern "C" void Keyframe__ctor_m6_792 (Keyframe_t6_131 * __this, float ___time, float ___value, float ___inTangent, float ___outTangent, const MethodInfo* method)
{
	{
		float L_0 = ___time;
		__this->___m_Time_0 = L_0;
		float L_1 = ___value;
		__this->___m_Value_1 = L_1;
		float L_2 = ___inTangent;
		__this->___m_InTangent_2 = L_2;
		float L_3 = ___outTangent;
		__this->___m_OutTangent_3 = L_3;
		return;
	}
}
// System.Void UnityEngine.AnimationCurve::.ctor(UnityEngine.Keyframe[])
extern "C" void AnimationCurve__ctor_m6_793 (AnimationCurve_t6_132 * __this, KeyframeU5BU5D_t6_259* ___keys, const MethodInfo* method)
{
	{
		Object__ctor_m1_0(__this, /*hidden argument*/NULL);
		KeyframeU5BU5D_t6_259* L_0 = ___keys;
		AnimationCurve_Init_m6_797(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.AnimationCurve::.ctor()
extern "C" void AnimationCurve__ctor_m6_794 (AnimationCurve_t6_132 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1_0(__this, /*hidden argument*/NULL);
		AnimationCurve_Init_m6_797(__this, (KeyframeU5BU5D_t6_259*)(KeyframeU5BU5D_t6_259*)NULL, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.AnimationCurve::Cleanup()
extern "C" void AnimationCurve_Cleanup_m6_795 (AnimationCurve_t6_132 * __this, const MethodInfo* method)
{
	typedef void (*AnimationCurve_Cleanup_m6_795_ftn) (AnimationCurve_t6_132 *);
	static AnimationCurve_Cleanup_m6_795_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (AnimationCurve_Cleanup_m6_795_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.AnimationCurve::Cleanup()");
	_il2cpp_icall_func(__this);
}
// System.Void UnityEngine.AnimationCurve::Finalize()
extern "C" void AnimationCurve_Finalize_m6_796 (AnimationCurve_t6_132 * __this, const MethodInfo* method)
{
	Exception_t1_33 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1_33 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		AnimationCurve_Cleanup_m6_795(__this, /*hidden argument*/NULL);
		IL2CPP_LEAVE(0x12, FINALLY_000b);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1_33 *)e.ex;
		goto FINALLY_000b;
	}

FINALLY_000b:
	{ // begin finally (depth: 1)
		Object_Finalize_m1_3(__this, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(11)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(11)
	{
		IL2CPP_JUMP_TBL(0x12, IL_0012)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1_33 *)
	}

IL_0012:
	{
		return;
	}
}
// System.Void UnityEngine.AnimationCurve::Init(UnityEngine.Keyframe[])
extern "C" void AnimationCurve_Init_m6_797 (AnimationCurve_t6_132 * __this, KeyframeU5BU5D_t6_259* ___keys, const MethodInfo* method)
{
	typedef void (*AnimationCurve_Init_m6_797_ftn) (AnimationCurve_t6_132 *, KeyframeU5BU5D_t6_259*);
	static AnimationCurve_Init_m6_797_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (AnimationCurve_Init_m6_797_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.AnimationCurve::Init(UnityEngine.Keyframe[])");
	_il2cpp_icall_func(__this, ___keys);
}
// Conversion methods for marshalling of: UnityEngine.AnimationCurve
extern "C" void AnimationCurve_t6_132_marshal(const AnimationCurve_t6_132& unmarshaled, AnimationCurve_t6_132_marshaled& marshaled)
{
	marshaled.___m_Ptr_0 = reinterpret_cast<intptr_t>((unmarshaled.___m_Ptr_0).___m_value_0);
}
extern "C" void AnimationCurve_t6_132_marshal_back(const AnimationCurve_t6_132_marshaled& marshaled, AnimationCurve_t6_132& unmarshaled)
{
	(unmarshaled.___m_Ptr_0).___m_value_0 = reinterpret_cast<void*>(marshaled.___m_Ptr_0);
}
// Conversion method for clean up from marshalling of: UnityEngine.AnimationCurve
extern "C" void AnimationCurve_t6_132_marshal_cleanup(AnimationCurve_t6_132_marshaled& marshaled)
{
}
// System.Void UnityEngine.Animation/Enumerator::.ctor(UnityEngine.Animation)
extern "C" void Enumerator__ctor_m6_798 (Enumerator_t6_134 * __this, Animation_t6_135 * ___outer, const MethodInfo* method)
{
	{
		__this->___m_CurrentIndex_1 = (-1);
		Object__ctor_m1_0(__this, /*hidden argument*/NULL);
		Animation_t6_135 * L_0 = ___outer;
		__this->___m_Outer_0 = L_0;
		return;
	}
}
// System.Object UnityEngine.Animation/Enumerator::get_Current()
extern "C" Object_t * Enumerator_get_Current_m6_799 (Enumerator_t6_134 * __this, const MethodInfo* method)
{
	{
		Animation_t6_135 * L_0 = (__this->___m_Outer_0);
		int32_t L_1 = (__this->___m_CurrentIndex_1);
		NullCheck(L_0);
		AnimationState_t6_127 * L_2 = Animation_GetStateAtIndex_m6_807(L_0, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// System.Boolean UnityEngine.Animation/Enumerator::MoveNext()
extern "C" bool Enumerator_MoveNext_m6_800 (Enumerator_t6_134 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		Animation_t6_135 * L_0 = (__this->___m_Outer_0);
		NullCheck(L_0);
		int32_t L_1 = Animation_GetStateCount_m6_808(L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		int32_t L_2 = (__this->___m_CurrentIndex_1);
		__this->___m_CurrentIndex_1 = ((int32_t)((int32_t)L_2+(int32_t)1));
		int32_t L_3 = (__this->___m_CurrentIndex_1);
		int32_t L_4 = V_0;
		return ((((int32_t)L_3) < ((int32_t)L_4))? 1 : 0);
	}
}
// System.Void UnityEngine.Animation/Enumerator::Reset()
extern "C" void Enumerator_Reset_m6_801 (Enumerator_t6_134 * __this, const MethodInfo* method)
{
	{
		__this->___m_CurrentIndex_1 = (-1);
		return;
	}
}
// UnityEngine.AnimationState UnityEngine.Animation::get_Item(System.String)
extern "C" AnimationState_t6_127 * Animation_get_Item_m6_802 (Animation_t6_135 * __this, String_t* ___name, const MethodInfo* method)
{
	{
		String_t* L_0 = ___name;
		AnimationState_t6_127 * L_1 = Animation_GetState_m6_806(__this, L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Void UnityEngine.Animation::CrossFade(System.String,System.Single,UnityEngine.PlayMode)
extern "C" void Animation_CrossFade_m6_803 (Animation_t6_135 * __this, String_t* ___animation, float ___fadeLength, int32_t ___mode, const MethodInfo* method)
{
	typedef void (*Animation_CrossFade_m6_803_ftn) (Animation_t6_135 *, String_t*, float, int32_t);
	static Animation_CrossFade_m6_803_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Animation_CrossFade_m6_803_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Animation::CrossFade(System.String,System.Single,UnityEngine.PlayMode)");
	_il2cpp_icall_func(__this, ___animation, ___fadeLength, ___mode);
}
// System.Void UnityEngine.Animation::CrossFade(System.String)
extern "C" void Animation_CrossFade_m6_804 (Animation_t6_135 * __this, String_t* ___animation, const MethodInfo* method)
{
	int32_t V_0 = {0};
	float V_1 = 0.0f;
	{
		V_0 = 0;
		V_1 = (0.3f);
		String_t* L_0 = ___animation;
		float L_1 = V_1;
		int32_t L_2 = V_0;
		Animation_CrossFade_m6_803(__this, L_0, L_1, L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Collections.IEnumerator UnityEngine.Animation::GetEnumerator()
extern TypeInfo* Enumerator_t6_134_il2cpp_TypeInfo_var;
extern "C" Object_t * Animation_GetEnumerator_m6_805 (Animation_t6_135 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Enumerator_t6_134_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(968);
		s_Il2CppMethodIntialized = true;
	}
	{
		Enumerator_t6_134 * L_0 = (Enumerator_t6_134 *)il2cpp_codegen_object_new (Enumerator_t6_134_il2cpp_TypeInfo_var);
		Enumerator__ctor_m6_798(L_0, __this, /*hidden argument*/NULL);
		return L_0;
	}
}
// UnityEngine.AnimationState UnityEngine.Animation::GetState(System.String)
extern "C" AnimationState_t6_127 * Animation_GetState_m6_806 (Animation_t6_135 * __this, String_t* ___name, const MethodInfo* method)
{
	typedef AnimationState_t6_127 * (*Animation_GetState_m6_806_ftn) (Animation_t6_135 *, String_t*);
	static Animation_GetState_m6_806_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Animation_GetState_m6_806_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Animation::GetState(System.String)");
	return _il2cpp_icall_func(__this, ___name);
}
// UnityEngine.AnimationState UnityEngine.Animation::GetStateAtIndex(System.Int32)
extern "C" AnimationState_t6_127 * Animation_GetStateAtIndex_m6_807 (Animation_t6_135 * __this, int32_t ___index, const MethodInfo* method)
{
	typedef AnimationState_t6_127 * (*Animation_GetStateAtIndex_m6_807_ftn) (Animation_t6_135 *, int32_t);
	static Animation_GetStateAtIndex_m6_807_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Animation_GetStateAtIndex_m6_807_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Animation::GetStateAtIndex(System.Int32)");
	return _il2cpp_icall_func(__this, ___index);
}
// System.Int32 UnityEngine.Animation::GetStateCount()
extern "C" int32_t Animation_GetStateCount_m6_808 (Animation_t6_135 * __this, const MethodInfo* method)
{
	typedef int32_t (*Animation_GetStateCount_m6_808_ftn) (Animation_t6_135 *);
	static Animation_GetStateCount_m6_808_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Animation_GetStateCount_m6_808_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Animation::GetStateCount()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.AnimationState::set_wrapMode(UnityEngine.WrapMode)
extern "C" void AnimationState_set_wrapMode_m6_809 (AnimationState_t6_127 * __this, int32_t ___value, const MethodInfo* method)
{
	typedef void (*AnimationState_set_wrapMode_m6_809_ftn) (AnimationState_t6_127 *, int32_t);
	static AnimationState_set_wrapMode_m6_809_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (AnimationState_set_wrapMode_m6_809_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.AnimationState::set_wrapMode(UnityEngine.WrapMode)");
	_il2cpp_icall_func(__this, ___value);
}
// System.Void UnityEngine.AnimationState::set_time(System.Single)
extern "C" void AnimationState_set_time_m6_810 (AnimationState_t6_127 * __this, float ___value, const MethodInfo* method)
{
	typedef void (*AnimationState_set_time_m6_810_ftn) (AnimationState_t6_127 *, float);
	static AnimationState_set_time_m6_810_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (AnimationState_set_time_m6_810_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.AnimationState::set_time(System.Single)");
	_il2cpp_icall_func(__this, ___value);
}
// System.Void UnityEngine.AnimationState::set_speed(System.Single)
extern "C" void AnimationState_set_speed_m6_811 (AnimationState_t6_127 * __this, float ___value, const MethodInfo* method)
{
	typedef void (*AnimationState_set_speed_m6_811_ftn) (AnimationState_t6_127 *, float);
	static AnimationState_set_speed_m6_811_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (AnimationState_set_speed_m6_811_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.AnimationState::set_speed(System.Single)");
	_il2cpp_icall_func(__this, ___value);
}
// System.Boolean UnityEngine.AnimatorStateInfo::IsName(System.String)
extern "C" bool AnimatorStateInfo_IsName_m6_812 (AnimatorStateInfo_t6_128 * __this, String_t* ___name, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B4_0 = 0;
	{
		String_t* L_0 = ___name;
		int32_t L_1 = Animator_StringToHash_m6_845(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		int32_t L_2 = V_0;
		int32_t L_3 = (__this->___m_FullPath_2);
		if ((((int32_t)L_2) == ((int32_t)L_3)))
		{
			goto IL_002a;
		}
	}
	{
		int32_t L_4 = V_0;
		int32_t L_5 = (__this->___m_Name_0);
		if ((((int32_t)L_4) == ((int32_t)L_5)))
		{
			goto IL_002a;
		}
	}
	{
		int32_t L_6 = V_0;
		int32_t L_7 = (__this->___m_Path_1);
		G_B4_0 = ((((int32_t)L_6) == ((int32_t)L_7))? 1 : 0);
		goto IL_002b;
	}

IL_002a:
	{
		G_B4_0 = 1;
	}

IL_002b:
	{
		return G_B4_0;
	}
}
// System.Int32 UnityEngine.AnimatorStateInfo::get_fullPathHash()
extern "C" int32_t AnimatorStateInfo_get_fullPathHash_m6_813 (AnimatorStateInfo_t6_128 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (__this->___m_FullPath_2);
		return L_0;
	}
}
// System.Int32 UnityEngine.AnimatorStateInfo::get_nameHash()
extern "C" int32_t AnimatorStateInfo_get_nameHash_m6_814 (AnimatorStateInfo_t6_128 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (__this->___m_Path_1);
		return L_0;
	}
}
// System.Int32 UnityEngine.AnimatorStateInfo::get_shortNameHash()
extern "C" int32_t AnimatorStateInfo_get_shortNameHash_m6_815 (AnimatorStateInfo_t6_128 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (__this->___m_Name_0);
		return L_0;
	}
}
// System.Single UnityEngine.AnimatorStateInfo::get_normalizedTime()
extern "C" float AnimatorStateInfo_get_normalizedTime_m6_816 (AnimatorStateInfo_t6_128 * __this, const MethodInfo* method)
{
	{
		float L_0 = (__this->___m_NormalizedTime_3);
		return L_0;
	}
}
// System.Single UnityEngine.AnimatorStateInfo::get_length()
extern "C" float AnimatorStateInfo_get_length_m6_817 (AnimatorStateInfo_t6_128 * __this, const MethodInfo* method)
{
	{
		float L_0 = (__this->___m_Length_4);
		return L_0;
	}
}
// System.Single UnityEngine.AnimatorStateInfo::get_speed()
extern "C" float AnimatorStateInfo_get_speed_m6_818 (AnimatorStateInfo_t6_128 * __this, const MethodInfo* method)
{
	{
		float L_0 = (__this->___m_Speed_5);
		return L_0;
	}
}
// System.Single UnityEngine.AnimatorStateInfo::get_speedMultiplier()
extern "C" float AnimatorStateInfo_get_speedMultiplier_m6_819 (AnimatorStateInfo_t6_128 * __this, const MethodInfo* method)
{
	{
		float L_0 = (__this->___m_SpeedMultiplier_6);
		return L_0;
	}
}
// System.Int32 UnityEngine.AnimatorStateInfo::get_tagHash()
extern "C" int32_t AnimatorStateInfo_get_tagHash_m6_820 (AnimatorStateInfo_t6_128 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (__this->___m_Tag_7);
		return L_0;
	}
}
// System.Boolean UnityEngine.AnimatorStateInfo::IsTag(System.String)
extern "C" bool AnimatorStateInfo_IsTag_m6_821 (AnimatorStateInfo_t6_128 * __this, String_t* ___tag, const MethodInfo* method)
{
	{
		String_t* L_0 = ___tag;
		int32_t L_1 = Animator_StringToHash_m6_845(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		int32_t L_2 = (__this->___m_Tag_7);
		return ((((int32_t)L_1) == ((int32_t)L_2))? 1 : 0);
	}
}
// System.Boolean UnityEngine.AnimatorStateInfo::get_loop()
extern "C" bool AnimatorStateInfo_get_loop_m6_822 (AnimatorStateInfo_t6_128 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (__this->___m_Loop_8);
		return ((((int32_t)((((int32_t)L_0) == ((int32_t)0))? 1 : 0)) == ((int32_t)0))? 1 : 0);
	}
}
// System.Boolean UnityEngine.AnimatorTransitionInfo::IsName(System.String)
extern "C" bool AnimatorTransitionInfo_IsName_m6_823 (AnimatorTransitionInfo_t6_137 * __this, String_t* ___name, const MethodInfo* method)
{
	int32_t G_B3_0 = 0;
	{
		String_t* L_0 = ___name;
		int32_t L_1 = Animator_StringToHash_m6_845(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		int32_t L_2 = (__this->___m_Name_2);
		if ((((int32_t)L_1) == ((int32_t)L_2)))
		{
			goto IL_0021;
		}
	}
	{
		String_t* L_3 = ___name;
		int32_t L_4 = Animator_StringToHash_m6_845(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		int32_t L_5 = (__this->___m_FullPath_0);
		G_B3_0 = ((((int32_t)L_4) == ((int32_t)L_5))? 1 : 0);
		goto IL_0022;
	}

IL_0021:
	{
		G_B3_0 = 1;
	}

IL_0022:
	{
		return G_B3_0;
	}
}
// System.Boolean UnityEngine.AnimatorTransitionInfo::IsUserName(System.String)
extern "C" bool AnimatorTransitionInfo_IsUserName_m6_824 (AnimatorTransitionInfo_t6_137 * __this, String_t* ___name, const MethodInfo* method)
{
	{
		String_t* L_0 = ___name;
		int32_t L_1 = Animator_StringToHash_m6_845(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		int32_t L_2 = (__this->___m_UserName_1);
		return ((((int32_t)L_1) == ((int32_t)L_2))? 1 : 0);
	}
}
// System.Int32 UnityEngine.AnimatorTransitionInfo::get_fullPathHash()
extern "C" int32_t AnimatorTransitionInfo_get_fullPathHash_m6_825 (AnimatorTransitionInfo_t6_137 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (__this->___m_FullPath_0);
		return L_0;
	}
}
// System.Int32 UnityEngine.AnimatorTransitionInfo::get_nameHash()
extern "C" int32_t AnimatorTransitionInfo_get_nameHash_m6_826 (AnimatorTransitionInfo_t6_137 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (__this->___m_Name_2);
		return L_0;
	}
}
// System.Int32 UnityEngine.AnimatorTransitionInfo::get_userNameHash()
extern "C" int32_t AnimatorTransitionInfo_get_userNameHash_m6_827 (AnimatorTransitionInfo_t6_137 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (__this->___m_UserName_1);
		return L_0;
	}
}
// System.Single UnityEngine.AnimatorTransitionInfo::get_normalizedTime()
extern "C" float AnimatorTransitionInfo_get_normalizedTime_m6_828 (AnimatorTransitionInfo_t6_137 * __this, const MethodInfo* method)
{
	{
		float L_0 = (__this->___m_NormalizedTime_3);
		return L_0;
	}
}
// System.Boolean UnityEngine.AnimatorTransitionInfo::get_anyState()
extern "C" bool AnimatorTransitionInfo_get_anyState_m6_829 (AnimatorTransitionInfo_t6_137 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (__this->___m_AnyState_4);
		return L_0;
	}
}
// System.Boolean UnityEngine.AnimatorTransitionInfo::get_entry()
extern "C" bool AnimatorTransitionInfo_get_entry_m6_830 (AnimatorTransitionInfo_t6_137 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (__this->___m_TransitionType_5);
		return ((((int32_t)((((int32_t)((int32_t)((int32_t)L_0&(int32_t)2))) == ((int32_t)0))? 1 : 0)) == ((int32_t)0))? 1 : 0);
	}
}
// System.Boolean UnityEngine.AnimatorTransitionInfo::get_exit()
extern "C" bool AnimatorTransitionInfo_get_exit_m6_831 (AnimatorTransitionInfo_t6_137 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (__this->___m_TransitionType_5);
		return ((((int32_t)((((int32_t)((int32_t)((int32_t)L_0&(int32_t)4))) == ((int32_t)0))? 1 : 0)) == ((int32_t)0))? 1 : 0);
	}
}
// Conversion methods for marshalling of: UnityEngine.AnimatorTransitionInfo
extern "C" void AnimatorTransitionInfo_t6_137_marshal(const AnimatorTransitionInfo_t6_137& unmarshaled, AnimatorTransitionInfo_t6_137_marshaled& marshaled)
{
	marshaled.___m_FullPath_0 = unmarshaled.___m_FullPath_0;
	marshaled.___m_UserName_1 = unmarshaled.___m_UserName_1;
	marshaled.___m_Name_2 = unmarshaled.___m_Name_2;
	marshaled.___m_NormalizedTime_3 = unmarshaled.___m_NormalizedTime_3;
	marshaled.___m_AnyState_4 = unmarshaled.___m_AnyState_4;
	marshaled.___m_TransitionType_5 = unmarshaled.___m_TransitionType_5;
}
extern "C" void AnimatorTransitionInfo_t6_137_marshal_back(const AnimatorTransitionInfo_t6_137_marshaled& marshaled, AnimatorTransitionInfo_t6_137& unmarshaled)
{
	unmarshaled.___m_FullPath_0 = marshaled.___m_FullPath_0;
	unmarshaled.___m_UserName_1 = marshaled.___m_UserName_1;
	unmarshaled.___m_Name_2 = marshaled.___m_Name_2;
	unmarshaled.___m_NormalizedTime_3 = marshaled.___m_NormalizedTime_3;
	unmarshaled.___m_AnyState_4 = marshaled.___m_AnyState_4;
	unmarshaled.___m_TransitionType_5 = marshaled.___m_TransitionType_5;
}
// Conversion method for clean up from marshalling of: UnityEngine.AnimatorTransitionInfo
extern "C" void AnimatorTransitionInfo_t6_137_marshal_cleanup(AnimatorTransitionInfo_t6_137_marshaled& marshaled)
{
}
// System.Single UnityEngine.Animator::GetFloat(System.String)
extern "C" float Animator_GetFloat_m6_832 (Animator_t6_138 * __this, String_t* ___name, const MethodInfo* method)
{
	{
		String_t* L_0 = ___name;
		float L_1 = Animator_GetFloatString_m6_847(__this, L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Void UnityEngine.Animator::SetFloat(System.String,System.Single)
extern "C" void Animator_SetFloat_m6_833 (Animator_t6_138 * __this, String_t* ___name, float ___value, const MethodInfo* method)
{
	{
		String_t* L_0 = ___name;
		float L_1 = ___value;
		Animator_SetFloatString_m6_846(__this, L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Animator::SetFloat(System.String,System.Single,System.Single,System.Single)
extern "C" void Animator_SetFloat_m6_834 (Animator_t6_138 * __this, String_t* ___name, float ___value, float ___dampTime, float ___deltaTime, const MethodInfo* method)
{
	{
		String_t* L_0 = ___name;
		float L_1 = ___value;
		float L_2 = ___dampTime;
		float L_3 = ___deltaTime;
		Animator_SetFloatStringDamp_m6_853(__this, L_0, L_1, L_2, L_3, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean UnityEngine.Animator::GetBool(System.String)
extern "C" bool Animator_GetBool_m6_835 (Animator_t6_138 * __this, String_t* ___name, const MethodInfo* method)
{
	{
		String_t* L_0 = ___name;
		bool L_1 = Animator_GetBoolString_m6_849(__this, L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Void UnityEngine.Animator::SetBool(System.String,System.Boolean)
extern "C" void Animator_SetBool_m6_836 (Animator_t6_138 * __this, String_t* ___name, bool ___value, const MethodInfo* method)
{
	{
		String_t* L_0 = ___name;
		bool L_1 = ___value;
		Animator_SetBoolString_m6_848(__this, L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Int32 UnityEngine.Animator::GetInteger(System.String)
extern "C" int32_t Animator_GetInteger_m6_837 (Animator_t6_138 * __this, String_t* ___name, const MethodInfo* method)
{
	{
		String_t* L_0 = ___name;
		int32_t L_1 = Animator_GetIntegerString_m6_851(__this, L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Void UnityEngine.Animator::SetInteger(System.String,System.Int32)
extern "C" void Animator_SetInteger_m6_838 (Animator_t6_138 * __this, String_t* ___name, int32_t ___value, const MethodInfo* method)
{
	{
		String_t* L_0 = ___name;
		int32_t L_1 = ___value;
		Animator_SetIntegerString_m6_850(__this, L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Animator::SetTrigger(System.String)
extern "C" void Animator_SetTrigger_m6_839 (Animator_t6_138 * __this, String_t* ___name, const MethodInfo* method)
{
	{
		String_t* L_0 = ___name;
		Animator_SetTriggerString_m6_852(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Animator::set_applyRootMotion(System.Boolean)
extern "C" void Animator_set_applyRootMotion_m6_840 (Animator_t6_138 * __this, bool ___value, const MethodInfo* method)
{
	typedef void (*Animator_set_applyRootMotion_m6_840_ftn) (Animator_t6_138 *, bool);
	static Animator_set_applyRootMotion_m6_840_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Animator_set_applyRootMotion_m6_840_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Animator::set_applyRootMotion(System.Boolean)");
	_il2cpp_icall_func(__this, ___value);
}
// System.Int32 UnityEngine.Animator::get_layerCount()
extern "C" int32_t Animator_get_layerCount_m6_841 (Animator_t6_138 * __this, const MethodInfo* method)
{
	typedef int32_t (*Animator_get_layerCount_m6_841_ftn) (Animator_t6_138 *);
	static Animator_get_layerCount_m6_841_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Animator_get_layerCount_m6_841_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Animator::get_layerCount()");
	return _il2cpp_icall_func(__this);
}
// System.Single UnityEngine.Animator::GetLayerWeight(System.Int32)
extern "C" float Animator_GetLayerWeight_m6_842 (Animator_t6_138 * __this, int32_t ___layerIndex, const MethodInfo* method)
{
	typedef float (*Animator_GetLayerWeight_m6_842_ftn) (Animator_t6_138 *, int32_t);
	static Animator_GetLayerWeight_m6_842_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Animator_GetLayerWeight_m6_842_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Animator::GetLayerWeight(System.Int32)");
	return _il2cpp_icall_func(__this, ___layerIndex);
}
// System.Void UnityEngine.Animator::SetLayerWeight(System.Int32,System.Single)
extern "C" void Animator_SetLayerWeight_m6_843 (Animator_t6_138 * __this, int32_t ___layerIndex, float ___weight, const MethodInfo* method)
{
	typedef void (*Animator_SetLayerWeight_m6_843_ftn) (Animator_t6_138 *, int32_t, float);
	static Animator_SetLayerWeight_m6_843_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Animator_SetLayerWeight_m6_843_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Animator::SetLayerWeight(System.Int32,System.Single)");
	_il2cpp_icall_func(__this, ___layerIndex, ___weight);
}
// UnityEngine.AnimatorStateInfo UnityEngine.Animator::GetCurrentAnimatorStateInfo(System.Int32)
extern "C" AnimatorStateInfo_t6_128  Animator_GetCurrentAnimatorStateInfo_m6_844 (Animator_t6_138 * __this, int32_t ___layerIndex, const MethodInfo* method)
{
	typedef AnimatorStateInfo_t6_128  (*Animator_GetCurrentAnimatorStateInfo_m6_844_ftn) (Animator_t6_138 *, int32_t);
	static Animator_GetCurrentAnimatorStateInfo_m6_844_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Animator_GetCurrentAnimatorStateInfo_m6_844_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Animator::GetCurrentAnimatorStateInfo(System.Int32)");
	return _il2cpp_icall_func(__this, ___layerIndex);
}
// System.Int32 UnityEngine.Animator::StringToHash(System.String)
extern "C" int32_t Animator_StringToHash_m6_845 (Object_t * __this /* static, unused */, String_t* ___name, const MethodInfo* method)
{
	typedef int32_t (*Animator_StringToHash_m6_845_ftn) (String_t*);
	static Animator_StringToHash_m6_845_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Animator_StringToHash_m6_845_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Animator::StringToHash(System.String)");
	return _il2cpp_icall_func(___name);
}
// System.Void UnityEngine.Animator::SetFloatString(System.String,System.Single)
extern "C" void Animator_SetFloatString_m6_846 (Animator_t6_138 * __this, String_t* ___name, float ___value, const MethodInfo* method)
{
	typedef void (*Animator_SetFloatString_m6_846_ftn) (Animator_t6_138 *, String_t*, float);
	static Animator_SetFloatString_m6_846_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Animator_SetFloatString_m6_846_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Animator::SetFloatString(System.String,System.Single)");
	_il2cpp_icall_func(__this, ___name, ___value);
}
// System.Single UnityEngine.Animator::GetFloatString(System.String)
extern "C" float Animator_GetFloatString_m6_847 (Animator_t6_138 * __this, String_t* ___name, const MethodInfo* method)
{
	typedef float (*Animator_GetFloatString_m6_847_ftn) (Animator_t6_138 *, String_t*);
	static Animator_GetFloatString_m6_847_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Animator_GetFloatString_m6_847_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Animator::GetFloatString(System.String)");
	return _il2cpp_icall_func(__this, ___name);
}
// System.Void UnityEngine.Animator::SetBoolString(System.String,System.Boolean)
extern "C" void Animator_SetBoolString_m6_848 (Animator_t6_138 * __this, String_t* ___name, bool ___value, const MethodInfo* method)
{
	typedef void (*Animator_SetBoolString_m6_848_ftn) (Animator_t6_138 *, String_t*, bool);
	static Animator_SetBoolString_m6_848_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Animator_SetBoolString_m6_848_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Animator::SetBoolString(System.String,System.Boolean)");
	_il2cpp_icall_func(__this, ___name, ___value);
}
// System.Boolean UnityEngine.Animator::GetBoolString(System.String)
extern "C" bool Animator_GetBoolString_m6_849 (Animator_t6_138 * __this, String_t* ___name, const MethodInfo* method)
{
	typedef bool (*Animator_GetBoolString_m6_849_ftn) (Animator_t6_138 *, String_t*);
	static Animator_GetBoolString_m6_849_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Animator_GetBoolString_m6_849_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Animator::GetBoolString(System.String)");
	return _il2cpp_icall_func(__this, ___name);
}
// System.Void UnityEngine.Animator::SetIntegerString(System.String,System.Int32)
extern "C" void Animator_SetIntegerString_m6_850 (Animator_t6_138 * __this, String_t* ___name, int32_t ___value, const MethodInfo* method)
{
	typedef void (*Animator_SetIntegerString_m6_850_ftn) (Animator_t6_138 *, String_t*, int32_t);
	static Animator_SetIntegerString_m6_850_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Animator_SetIntegerString_m6_850_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Animator::SetIntegerString(System.String,System.Int32)");
	_il2cpp_icall_func(__this, ___name, ___value);
}
// System.Int32 UnityEngine.Animator::GetIntegerString(System.String)
extern "C" int32_t Animator_GetIntegerString_m6_851 (Animator_t6_138 * __this, String_t* ___name, const MethodInfo* method)
{
	typedef int32_t (*Animator_GetIntegerString_m6_851_ftn) (Animator_t6_138 *, String_t*);
	static Animator_GetIntegerString_m6_851_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Animator_GetIntegerString_m6_851_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Animator::GetIntegerString(System.String)");
	return _il2cpp_icall_func(__this, ___name);
}
// System.Void UnityEngine.Animator::SetTriggerString(System.String)
extern "C" void Animator_SetTriggerString_m6_852 (Animator_t6_138 * __this, String_t* ___name, const MethodInfo* method)
{
	typedef void (*Animator_SetTriggerString_m6_852_ftn) (Animator_t6_138 *, String_t*);
	static Animator_SetTriggerString_m6_852_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Animator_SetTriggerString_m6_852_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Animator::SetTriggerString(System.String)");
	_il2cpp_icall_func(__this, ___name);
}
// System.Void UnityEngine.Animator::SetFloatStringDamp(System.String,System.Single,System.Single,System.Single)
extern "C" void Animator_SetFloatStringDamp_m6_853 (Animator_t6_138 * __this, String_t* ___name, float ___value, float ___dampTime, float ___deltaTime, const MethodInfo* method)
{
	typedef void (*Animator_SetFloatStringDamp_m6_853_ftn) (Animator_t6_138 *, String_t*, float, float, float);
	static Animator_SetFloatStringDamp_m6_853_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Animator_SetFloatStringDamp_m6_853_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Animator::SetFloatStringDamp(System.String,System.Single,System.Single,System.Single)");
	_il2cpp_icall_func(__this, ___name, ___value, ___dampTime, ___deltaTime);
}
// Conversion methods for marshalling of: UnityEngine.SkeletonBone
extern "C" void SkeletonBone_t6_139_marshal(const SkeletonBone_t6_139& unmarshaled, SkeletonBone_t6_139_marshaled& marshaled)
{
	marshaled.___name_0 = il2cpp_codegen_marshal_string(unmarshaled.___name_0);
	marshaled.___position_1 = unmarshaled.___position_1;
	marshaled.___rotation_2 = unmarshaled.___rotation_2;
	marshaled.___scale_3 = unmarshaled.___scale_3;
	marshaled.___transformModified_4 = unmarshaled.___transformModified_4;
}
extern "C" void SkeletonBone_t6_139_marshal_back(const SkeletonBone_t6_139_marshaled& marshaled, SkeletonBone_t6_139& unmarshaled)
{
	unmarshaled.___name_0 = il2cpp_codegen_marshal_string_result(marshaled.___name_0);
	unmarshaled.___position_1 = marshaled.___position_1;
	unmarshaled.___rotation_2 = marshaled.___rotation_2;
	unmarshaled.___scale_3 = marshaled.___scale_3;
	unmarshaled.___transformModified_4 = marshaled.___transformModified_4;
}
// Conversion method for clean up from marshalling of: UnityEngine.SkeletonBone
extern "C" void SkeletonBone_t6_139_marshal_cleanup(SkeletonBone_t6_139_marshaled& marshaled)
{
	il2cpp_codegen_marshal_free(marshaled.___name_0);
	marshaled.___name_0 = NULL;
}
// System.String UnityEngine.HumanBone::get_boneName()
extern "C" String_t* HumanBone_get_boneName_m6_854 (HumanBone_t6_141 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = (__this->___m_BoneName_0);
		return L_0;
	}
}
// System.Void UnityEngine.HumanBone::set_boneName(System.String)
extern "C" void HumanBone_set_boneName_m6_855 (HumanBone_t6_141 * __this, String_t* ___value, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value;
		__this->___m_BoneName_0 = L_0;
		return;
	}
}
// System.String UnityEngine.HumanBone::get_humanName()
extern "C" String_t* HumanBone_get_humanName_m6_856 (HumanBone_t6_141 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = (__this->___m_HumanName_1);
		return L_0;
	}
}
// System.Void UnityEngine.HumanBone::set_humanName(System.String)
extern "C" void HumanBone_set_humanName_m6_857 (HumanBone_t6_141 * __this, String_t* ___value, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value;
		__this->___m_HumanName_1 = L_0;
		return;
	}
}
// Conversion methods for marshalling of: UnityEngine.HumanBone
extern "C" void HumanBone_t6_141_marshal(const HumanBone_t6_141& unmarshaled, HumanBone_t6_141_marshaled& marshaled)
{
	marshaled.___m_BoneName_0 = il2cpp_codegen_marshal_string(unmarshaled.___m_BoneName_0);
	marshaled.___m_HumanName_1 = il2cpp_codegen_marshal_string(unmarshaled.___m_HumanName_1);
	marshaled.___limit_2 = unmarshaled.___limit_2;
}
extern "C" void HumanBone_t6_141_marshal_back(const HumanBone_t6_141_marshaled& marshaled, HumanBone_t6_141& unmarshaled)
{
	unmarshaled.___m_BoneName_0 = il2cpp_codegen_marshal_string_result(marshaled.___m_BoneName_0);
	unmarshaled.___m_HumanName_1 = il2cpp_codegen_marshal_string_result(marshaled.___m_HumanName_1);
	unmarshaled.___limit_2 = marshaled.___limit_2;
}
// Conversion method for clean up from marshalling of: UnityEngine.HumanBone
extern "C" void HumanBone_t6_141_marshal_cleanup(HumanBone_t6_141_marshaled& marshaled)
{
	il2cpp_codegen_marshal_free(marshaled.___m_BoneName_0);
	marshaled.___m_BoneName_0 = NULL;
	il2cpp_codegen_marshal_free(marshaled.___m_HumanName_1);
	marshaled.___m_HumanName_1 = NULL;
}
// System.Void UnityEngine.TextMesh::set_text(System.String)
extern "C" void TextMesh_set_text_m6_858 (TextMesh_t6_145 * __this, String_t* ___value, const MethodInfo* method)
{
	typedef void (*TextMesh_set_text_m6_858_ftn) (TextMesh_t6_145 *, String_t*);
	static TextMesh_set_text_m6_858_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (TextMesh_set_text_m6_858_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.TextMesh::set_text(System.String)");
	_il2cpp_icall_func(__this, ___value);
}
// System.Void UnityEngine.TextMesh::set_font(UnityEngine.Font)
extern "C" void TextMesh_set_font_m6_859 (TextMesh_t6_145 * __this, Font_t6_148 * ___value, const MethodInfo* method)
{
	typedef void (*TextMesh_set_font_m6_859_ftn) (TextMesh_t6_145 *, Font_t6_148 *);
	static TextMesh_set_font_m6_859_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (TextMesh_set_font_m6_859_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.TextMesh::set_font(UnityEngine.Font)");
	_il2cpp_icall_func(__this, ___value);
}
// System.Void UnityEngine.TextMesh::set_anchor(UnityEngine.TextAnchor)
extern "C" void TextMesh_set_anchor_m6_860 (TextMesh_t6_145 * __this, int32_t ___value, const MethodInfo* method)
{
	typedef void (*TextMesh_set_anchor_m6_860_ftn) (TextMesh_t6_145 *, int32_t);
	static TextMesh_set_anchor_m6_860_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (TextMesh_set_anchor_m6_860_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.TextMesh::set_anchor(UnityEngine.TextAnchor)");
	_il2cpp_icall_func(__this, ___value);
}
// System.Void UnityEngine.TextMesh::set_characterSize(System.Single)
extern "C" void TextMesh_set_characterSize_m6_861 (TextMesh_t6_145 * __this, float ___value, const MethodInfo* method)
{
	typedef void (*TextMesh_set_characterSize_m6_861_ftn) (TextMesh_t6_145 *, float);
	static TextMesh_set_characterSize_m6_861_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (TextMesh_set_characterSize_m6_861_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.TextMesh::set_characterSize(System.Single)");
	_il2cpp_icall_func(__this, ___value);
}
// System.Int32 UnityEngine.CharacterInfo::get_advance()
extern "C" int32_t CharacterInfo_get_advance_m6_862 (CharacterInfo_t6_146 * __this, const MethodInfo* method)
{
	{
		float L_0 = (__this->___width_3);
		return (((int32_t)((int32_t)L_0)));
	}
}
// System.Void UnityEngine.CharacterInfo::set_advance(System.Int32)
extern "C" void CharacterInfo_set_advance_m6_863 (CharacterInfo_t6_146 * __this, int32_t ___value, const MethodInfo* method)
{
	{
		int32_t L_0 = ___value;
		__this->___width_3 = (((float)((float)L_0)));
		return;
	}
}
// System.Int32 UnityEngine.CharacterInfo::get_glyphWidth()
extern "C" int32_t CharacterInfo_get_glyphWidth_m6_864 (CharacterInfo_t6_146 * __this, const MethodInfo* method)
{
	{
		Rect_t6_52 * L_0 = &(__this->___vert_2);
		float L_1 = Rect_get_width_m6_277(L_0, /*hidden argument*/NULL);
		return (((int32_t)((int32_t)L_1)));
	}
}
// System.Void UnityEngine.CharacterInfo::set_glyphWidth(System.Int32)
extern "C" void CharacterInfo_set_glyphWidth_m6_865 (CharacterInfo_t6_146 * __this, int32_t ___value, const MethodInfo* method)
{
	{
		Rect_t6_52 * L_0 = &(__this->___vert_2);
		int32_t L_1 = ___value;
		Rect_set_width_m6_278(L_0, (((float)((float)L_1))), /*hidden argument*/NULL);
		return;
	}
}
// System.Int32 UnityEngine.CharacterInfo::get_glyphHeight()
extern "C" int32_t CharacterInfo_get_glyphHeight_m6_866 (CharacterInfo_t6_146 * __this, const MethodInfo* method)
{
	{
		Rect_t6_52 * L_0 = &(__this->___vert_2);
		float L_1 = Rect_get_height_m6_279(L_0, /*hidden argument*/NULL);
		return (((int32_t)((int32_t)((-L_1)))));
	}
}
// System.Void UnityEngine.CharacterInfo::set_glyphHeight(System.Int32)
extern "C" void CharacterInfo_set_glyphHeight_m6_867 (CharacterInfo_t6_146 * __this, int32_t ___value, const MethodInfo* method)
{
	float V_0 = 0.0f;
	{
		Rect_t6_52 * L_0 = &(__this->___vert_2);
		float L_1 = Rect_get_height_m6_279(L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		Rect_t6_52 * L_2 = &(__this->___vert_2);
		int32_t L_3 = ___value;
		Rect_set_height_m6_280(L_2, (((float)((float)((-L_3))))), /*hidden argument*/NULL);
		Rect_t6_52 * L_4 = &(__this->___vert_2);
		Rect_t6_52 * L_5 = L_4;
		float L_6 = Rect_get_y_m6_275(L_5, /*hidden argument*/NULL);
		float L_7 = V_0;
		Rect_t6_52 * L_8 = &(__this->___vert_2);
		float L_9 = Rect_get_height_m6_279(L_8, /*hidden argument*/NULL);
		Rect_set_y_m6_276(L_5, ((float)((float)L_6+(float)((float)((float)L_7-(float)L_9)))), /*hidden argument*/NULL);
		return;
	}
}
// System.Int32 UnityEngine.CharacterInfo::get_bearing()
extern "C" int32_t CharacterInfo_get_bearing_m6_868 (CharacterInfo_t6_146 * __this, const MethodInfo* method)
{
	{
		Rect_t6_52 * L_0 = &(__this->___vert_2);
		float L_1 = Rect_get_x_m6_273(L_0, /*hidden argument*/NULL);
		return (((int32_t)((int32_t)L_1)));
	}
}
// System.Void UnityEngine.CharacterInfo::set_bearing(System.Int32)
extern "C" void CharacterInfo_set_bearing_m6_869 (CharacterInfo_t6_146 * __this, int32_t ___value, const MethodInfo* method)
{
	{
		Rect_t6_52 * L_0 = &(__this->___vert_2);
		int32_t L_1 = ___value;
		Rect_set_x_m6_274(L_0, (((float)((float)L_1))), /*hidden argument*/NULL);
		return;
	}
}
// System.Int32 UnityEngine.CharacterInfo::get_minY()
extern "C" int32_t CharacterInfo_get_minY_m6_870 (CharacterInfo_t6_146 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (__this->___ascent_7);
		Rect_t6_52 * L_1 = &(__this->___vert_2);
		float L_2 = Rect_get_y_m6_275(L_1, /*hidden argument*/NULL);
		Rect_t6_52 * L_3 = &(__this->___vert_2);
		float L_4 = Rect_get_height_m6_279(L_3, /*hidden argument*/NULL);
		return ((int32_t)((int32_t)L_0+(int32_t)(((int32_t)((int32_t)((float)((float)L_2+(float)L_4)))))));
	}
}
// System.Void UnityEngine.CharacterInfo::set_minY(System.Int32)
extern "C" void CharacterInfo_set_minY_m6_871 (CharacterInfo_t6_146 * __this, int32_t ___value, const MethodInfo* method)
{
	{
		Rect_t6_52 * L_0 = &(__this->___vert_2);
		int32_t L_1 = ___value;
		int32_t L_2 = (__this->___ascent_7);
		Rect_t6_52 * L_3 = &(__this->___vert_2);
		float L_4 = Rect_get_y_m6_275(L_3, /*hidden argument*/NULL);
		Rect_set_height_m6_280(L_0, ((float)((float)(((float)((float)((int32_t)((int32_t)L_1-(int32_t)L_2)))))-(float)L_4)), /*hidden argument*/NULL);
		return;
	}
}
// System.Int32 UnityEngine.CharacterInfo::get_maxY()
extern "C" int32_t CharacterInfo_get_maxY_m6_872 (CharacterInfo_t6_146 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (__this->___ascent_7);
		Rect_t6_52 * L_1 = &(__this->___vert_2);
		float L_2 = Rect_get_y_m6_275(L_1, /*hidden argument*/NULL);
		return ((int32_t)((int32_t)L_0+(int32_t)(((int32_t)((int32_t)L_2)))));
	}
}
// System.Void UnityEngine.CharacterInfo::set_maxY(System.Int32)
extern "C" void CharacterInfo_set_maxY_m6_873 (CharacterInfo_t6_146 * __this, int32_t ___value, const MethodInfo* method)
{
	float V_0 = 0.0f;
	{
		Rect_t6_52 * L_0 = &(__this->___vert_2);
		float L_1 = Rect_get_y_m6_275(L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		Rect_t6_52 * L_2 = &(__this->___vert_2);
		int32_t L_3 = ___value;
		int32_t L_4 = (__this->___ascent_7);
		Rect_set_y_m6_276(L_2, (((float)((float)((int32_t)((int32_t)L_3-(int32_t)L_4))))), /*hidden argument*/NULL);
		Rect_t6_52 * L_5 = &(__this->___vert_2);
		Rect_t6_52 * L_6 = L_5;
		float L_7 = Rect_get_height_m6_279(L_6, /*hidden argument*/NULL);
		float L_8 = V_0;
		Rect_t6_52 * L_9 = &(__this->___vert_2);
		float L_10 = Rect_get_y_m6_275(L_9, /*hidden argument*/NULL);
		Rect_set_height_m6_280(L_6, ((float)((float)L_7+(float)((float)((float)L_8-(float)L_10)))), /*hidden argument*/NULL);
		return;
	}
}
// System.Int32 UnityEngine.CharacterInfo::get_minX()
extern "C" int32_t CharacterInfo_get_minX_m6_874 (CharacterInfo_t6_146 * __this, const MethodInfo* method)
{
	{
		Rect_t6_52 * L_0 = &(__this->___vert_2);
		float L_1 = Rect_get_x_m6_273(L_0, /*hidden argument*/NULL);
		return (((int32_t)((int32_t)L_1)));
	}
}
// System.Void UnityEngine.CharacterInfo::set_minX(System.Int32)
extern "C" void CharacterInfo_set_minX_m6_875 (CharacterInfo_t6_146 * __this, int32_t ___value, const MethodInfo* method)
{
	float V_0 = 0.0f;
	{
		Rect_t6_52 * L_0 = &(__this->___vert_2);
		float L_1 = Rect_get_x_m6_273(L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		Rect_t6_52 * L_2 = &(__this->___vert_2);
		int32_t L_3 = ___value;
		Rect_set_x_m6_274(L_2, (((float)((float)L_3))), /*hidden argument*/NULL);
		Rect_t6_52 * L_4 = &(__this->___vert_2);
		Rect_t6_52 * L_5 = L_4;
		float L_6 = Rect_get_width_m6_277(L_5, /*hidden argument*/NULL);
		float L_7 = V_0;
		Rect_t6_52 * L_8 = &(__this->___vert_2);
		float L_9 = Rect_get_x_m6_273(L_8, /*hidden argument*/NULL);
		Rect_set_width_m6_278(L_5, ((float)((float)L_6+(float)((float)((float)L_7-(float)L_9)))), /*hidden argument*/NULL);
		return;
	}
}
// System.Int32 UnityEngine.CharacterInfo::get_maxX()
extern "C" int32_t CharacterInfo_get_maxX_m6_876 (CharacterInfo_t6_146 * __this, const MethodInfo* method)
{
	{
		Rect_t6_52 * L_0 = &(__this->___vert_2);
		float L_1 = Rect_get_x_m6_273(L_0, /*hidden argument*/NULL);
		Rect_t6_52 * L_2 = &(__this->___vert_2);
		float L_3 = Rect_get_width_m6_277(L_2, /*hidden argument*/NULL);
		return (((int32_t)((int32_t)((float)((float)L_1+(float)L_3)))));
	}
}
// System.Void UnityEngine.CharacterInfo::set_maxX(System.Int32)
extern "C" void CharacterInfo_set_maxX_m6_877 (CharacterInfo_t6_146 * __this, int32_t ___value, const MethodInfo* method)
{
	{
		Rect_t6_52 * L_0 = &(__this->___vert_2);
		int32_t L_1 = ___value;
		Rect_t6_52 * L_2 = &(__this->___vert_2);
		float L_3 = Rect_get_x_m6_273(L_2, /*hidden argument*/NULL);
		Rect_set_width_m6_278(L_0, ((float)((float)(((float)((float)L_1)))-(float)L_3)), /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Vector2 UnityEngine.CharacterInfo::get_uvBottomLeftUnFlipped()
extern "C" Vector2_t6_48  CharacterInfo_get_uvBottomLeftUnFlipped_m6_878 (CharacterInfo_t6_146 * __this, const MethodInfo* method)
{
	{
		Rect_t6_52 * L_0 = &(__this->___uv_1);
		float L_1 = Rect_get_x_m6_273(L_0, /*hidden argument*/NULL);
		Rect_t6_52 * L_2 = &(__this->___uv_1);
		float L_3 = Rect_get_y_m6_275(L_2, /*hidden argument*/NULL);
		Vector2_t6_48  L_4 = {0};
		Vector2__ctor_m6_176(&L_4, L_1, L_3, /*hidden argument*/NULL);
		return L_4;
	}
}
// System.Void UnityEngine.CharacterInfo::set_uvBottomLeftUnFlipped(UnityEngine.Vector2)
extern "C" void CharacterInfo_set_uvBottomLeftUnFlipped_m6_879 (CharacterInfo_t6_146 * __this, Vector2_t6_48  ___value, const MethodInfo* method)
{
	Vector2_t6_48  V_0 = {0};
	{
		Vector2_t6_48  L_0 = CharacterInfo_get_uvTopRightUnFlipped_m6_882(__this, /*hidden argument*/NULL);
		V_0 = L_0;
		Rect_t6_52 * L_1 = &(__this->___uv_1);
		float L_2 = ((&___value)->___x_1);
		Rect_set_x_m6_274(L_1, L_2, /*hidden argument*/NULL);
		Rect_t6_52 * L_3 = &(__this->___uv_1);
		float L_4 = ((&___value)->___y_2);
		Rect_set_y_m6_276(L_3, L_4, /*hidden argument*/NULL);
		Rect_t6_52 * L_5 = &(__this->___uv_1);
		float L_6 = ((&V_0)->___x_1);
		Rect_t6_52 * L_7 = &(__this->___uv_1);
		float L_8 = Rect_get_x_m6_273(L_7, /*hidden argument*/NULL);
		Rect_set_width_m6_278(L_5, ((float)((float)L_6-(float)L_8)), /*hidden argument*/NULL);
		Rect_t6_52 * L_9 = &(__this->___uv_1);
		float L_10 = ((&V_0)->___y_2);
		Rect_t6_52 * L_11 = &(__this->___uv_1);
		float L_12 = Rect_get_y_m6_275(L_11, /*hidden argument*/NULL);
		Rect_set_height_m6_280(L_9, ((float)((float)L_10-(float)L_12)), /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Vector2 UnityEngine.CharacterInfo::get_uvBottomRightUnFlipped()
extern "C" Vector2_t6_48  CharacterInfo_get_uvBottomRightUnFlipped_m6_880 (CharacterInfo_t6_146 * __this, const MethodInfo* method)
{
	{
		Rect_t6_52 * L_0 = &(__this->___uv_1);
		float L_1 = Rect_get_x_m6_273(L_0, /*hidden argument*/NULL);
		Rect_t6_52 * L_2 = &(__this->___uv_1);
		float L_3 = Rect_get_width_m6_277(L_2, /*hidden argument*/NULL);
		Rect_t6_52 * L_4 = &(__this->___uv_1);
		float L_5 = Rect_get_y_m6_275(L_4, /*hidden argument*/NULL);
		Vector2_t6_48  L_6 = {0};
		Vector2__ctor_m6_176(&L_6, ((float)((float)L_1+(float)L_3)), L_5, /*hidden argument*/NULL);
		return L_6;
	}
}
// System.Void UnityEngine.CharacterInfo::set_uvBottomRightUnFlipped(UnityEngine.Vector2)
extern "C" void CharacterInfo_set_uvBottomRightUnFlipped_m6_881 (CharacterInfo_t6_146 * __this, Vector2_t6_48  ___value, const MethodInfo* method)
{
	Vector2_t6_48  V_0 = {0};
	{
		Vector2_t6_48  L_0 = CharacterInfo_get_uvTopRightUnFlipped_m6_882(__this, /*hidden argument*/NULL);
		V_0 = L_0;
		Rect_t6_52 * L_1 = &(__this->___uv_1);
		float L_2 = ((&___value)->___x_1);
		Rect_t6_52 * L_3 = &(__this->___uv_1);
		float L_4 = Rect_get_x_m6_273(L_3, /*hidden argument*/NULL);
		Rect_set_width_m6_278(L_1, ((float)((float)L_2-(float)L_4)), /*hidden argument*/NULL);
		Rect_t6_52 * L_5 = &(__this->___uv_1);
		float L_6 = ((&___value)->___y_2);
		Rect_set_y_m6_276(L_5, L_6, /*hidden argument*/NULL);
		Rect_t6_52 * L_7 = &(__this->___uv_1);
		float L_8 = ((&V_0)->___y_2);
		Rect_t6_52 * L_9 = &(__this->___uv_1);
		float L_10 = Rect_get_y_m6_275(L_9, /*hidden argument*/NULL);
		Rect_set_height_m6_280(L_7, ((float)((float)L_8-(float)L_10)), /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Vector2 UnityEngine.CharacterInfo::get_uvTopRightUnFlipped()
extern "C" Vector2_t6_48  CharacterInfo_get_uvTopRightUnFlipped_m6_882 (CharacterInfo_t6_146 * __this, const MethodInfo* method)
{
	{
		Rect_t6_52 * L_0 = &(__this->___uv_1);
		float L_1 = Rect_get_x_m6_273(L_0, /*hidden argument*/NULL);
		Rect_t6_52 * L_2 = &(__this->___uv_1);
		float L_3 = Rect_get_width_m6_277(L_2, /*hidden argument*/NULL);
		Rect_t6_52 * L_4 = &(__this->___uv_1);
		float L_5 = Rect_get_y_m6_275(L_4, /*hidden argument*/NULL);
		Rect_t6_52 * L_6 = &(__this->___uv_1);
		float L_7 = Rect_get_height_m6_279(L_6, /*hidden argument*/NULL);
		Vector2_t6_48  L_8 = {0};
		Vector2__ctor_m6_176(&L_8, ((float)((float)L_1+(float)L_3)), ((float)((float)L_5+(float)L_7)), /*hidden argument*/NULL);
		return L_8;
	}
}
// System.Void UnityEngine.CharacterInfo::set_uvTopRightUnFlipped(UnityEngine.Vector2)
extern "C" void CharacterInfo_set_uvTopRightUnFlipped_m6_883 (CharacterInfo_t6_146 * __this, Vector2_t6_48  ___value, const MethodInfo* method)
{
	{
		Rect_t6_52 * L_0 = &(__this->___uv_1);
		float L_1 = ((&___value)->___x_1);
		Rect_t6_52 * L_2 = &(__this->___uv_1);
		float L_3 = Rect_get_x_m6_273(L_2, /*hidden argument*/NULL);
		Rect_set_width_m6_278(L_0, ((float)((float)L_1-(float)L_3)), /*hidden argument*/NULL);
		Rect_t6_52 * L_4 = &(__this->___uv_1);
		float L_5 = ((&___value)->___y_2);
		Rect_t6_52 * L_6 = &(__this->___uv_1);
		float L_7 = Rect_get_y_m6_275(L_6, /*hidden argument*/NULL);
		Rect_set_height_m6_280(L_4, ((float)((float)L_5-(float)L_7)), /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Vector2 UnityEngine.CharacterInfo::get_uvTopLeftUnFlipped()
extern "C" Vector2_t6_48  CharacterInfo_get_uvTopLeftUnFlipped_m6_884 (CharacterInfo_t6_146 * __this, const MethodInfo* method)
{
	{
		Rect_t6_52 * L_0 = &(__this->___uv_1);
		float L_1 = Rect_get_x_m6_273(L_0, /*hidden argument*/NULL);
		Rect_t6_52 * L_2 = &(__this->___uv_1);
		float L_3 = Rect_get_y_m6_275(L_2, /*hidden argument*/NULL);
		Rect_t6_52 * L_4 = &(__this->___uv_1);
		float L_5 = Rect_get_height_m6_279(L_4, /*hidden argument*/NULL);
		Vector2_t6_48  L_6 = {0};
		Vector2__ctor_m6_176(&L_6, L_1, ((float)((float)L_3+(float)L_5)), /*hidden argument*/NULL);
		return L_6;
	}
}
// System.Void UnityEngine.CharacterInfo::set_uvTopLeftUnFlipped(UnityEngine.Vector2)
extern "C" void CharacterInfo_set_uvTopLeftUnFlipped_m6_885 (CharacterInfo_t6_146 * __this, Vector2_t6_48  ___value, const MethodInfo* method)
{
	Vector2_t6_48  V_0 = {0};
	{
		Vector2_t6_48  L_0 = CharacterInfo_get_uvTopRightUnFlipped_m6_882(__this, /*hidden argument*/NULL);
		V_0 = L_0;
		Rect_t6_52 * L_1 = &(__this->___uv_1);
		float L_2 = ((&___value)->___x_1);
		Rect_set_x_m6_274(L_1, L_2, /*hidden argument*/NULL);
		Rect_t6_52 * L_3 = &(__this->___uv_1);
		float L_4 = ((&___value)->___y_2);
		Rect_t6_52 * L_5 = &(__this->___uv_1);
		float L_6 = Rect_get_y_m6_275(L_5, /*hidden argument*/NULL);
		Rect_set_height_m6_280(L_3, ((float)((float)L_4-(float)L_6)), /*hidden argument*/NULL);
		Rect_t6_52 * L_7 = &(__this->___uv_1);
		float L_8 = ((&V_0)->___x_1);
		Rect_t6_52 * L_9 = &(__this->___uv_1);
		float L_10 = Rect_get_x_m6_273(L_9, /*hidden argument*/NULL);
		Rect_set_width_m6_278(L_7, ((float)((float)L_8-(float)L_10)), /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Vector2 UnityEngine.CharacterInfo::get_uvBottomLeft()
extern "C" Vector2_t6_48  CharacterInfo_get_uvBottomLeft_m6_886 (CharacterInfo_t6_146 * __this, const MethodInfo* method)
{
	{
		Vector2_t6_48  L_0 = CharacterInfo_get_uvBottomLeftUnFlipped_m6_878(__this, /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Void UnityEngine.CharacterInfo::set_uvBottomLeft(UnityEngine.Vector2)
extern "C" void CharacterInfo_set_uvBottomLeft_m6_887 (CharacterInfo_t6_146 * __this, Vector2_t6_48  ___value, const MethodInfo* method)
{
	{
		Vector2_t6_48  L_0 = ___value;
		CharacterInfo_set_uvBottomLeftUnFlipped_m6_879(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Vector2 UnityEngine.CharacterInfo::get_uvBottomRight()
extern "C" Vector2_t6_48  CharacterInfo_get_uvBottomRight_m6_888 (CharacterInfo_t6_146 * __this, const MethodInfo* method)
{
	Vector2_t6_48  G_B3_0 = {0};
	{
		bool L_0 = (__this->___flipped_6);
		if (!L_0)
		{
			goto IL_0016;
		}
	}
	{
		Vector2_t6_48  L_1 = CharacterInfo_get_uvTopLeftUnFlipped_m6_884(__this, /*hidden argument*/NULL);
		G_B3_0 = L_1;
		goto IL_001c;
	}

IL_0016:
	{
		Vector2_t6_48  L_2 = CharacterInfo_get_uvBottomRightUnFlipped_m6_880(__this, /*hidden argument*/NULL);
		G_B3_0 = L_2;
	}

IL_001c:
	{
		return G_B3_0;
	}
}
// System.Void UnityEngine.CharacterInfo::set_uvBottomRight(UnityEngine.Vector2)
extern "C" void CharacterInfo_set_uvBottomRight_m6_889 (CharacterInfo_t6_146 * __this, Vector2_t6_48  ___value, const MethodInfo* method)
{
	{
		bool L_0 = (__this->___flipped_6);
		if (!L_0)
		{
			goto IL_0017;
		}
	}
	{
		Vector2_t6_48  L_1 = ___value;
		CharacterInfo_set_uvTopLeftUnFlipped_m6_885(__this, L_1, /*hidden argument*/NULL);
		goto IL_001e;
	}

IL_0017:
	{
		Vector2_t6_48  L_2 = ___value;
		CharacterInfo_set_uvBottomRightUnFlipped_m6_881(__this, L_2, /*hidden argument*/NULL);
	}

IL_001e:
	{
		return;
	}
}
// UnityEngine.Vector2 UnityEngine.CharacterInfo::get_uvTopRight()
extern "C" Vector2_t6_48  CharacterInfo_get_uvTopRight_m6_890 (CharacterInfo_t6_146 * __this, const MethodInfo* method)
{
	{
		Vector2_t6_48  L_0 = CharacterInfo_get_uvTopRightUnFlipped_m6_882(__this, /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Void UnityEngine.CharacterInfo::set_uvTopRight(UnityEngine.Vector2)
extern "C" void CharacterInfo_set_uvTopRight_m6_891 (CharacterInfo_t6_146 * __this, Vector2_t6_48  ___value, const MethodInfo* method)
{
	{
		Vector2_t6_48  L_0 = ___value;
		CharacterInfo_set_uvTopRightUnFlipped_m6_883(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Vector2 UnityEngine.CharacterInfo::get_uvTopLeft()
extern "C" Vector2_t6_48  CharacterInfo_get_uvTopLeft_m6_892 (CharacterInfo_t6_146 * __this, const MethodInfo* method)
{
	Vector2_t6_48  G_B3_0 = {0};
	{
		bool L_0 = (__this->___flipped_6);
		if (!L_0)
		{
			goto IL_0016;
		}
	}
	{
		Vector2_t6_48  L_1 = CharacterInfo_get_uvBottomRightUnFlipped_m6_880(__this, /*hidden argument*/NULL);
		G_B3_0 = L_1;
		goto IL_001c;
	}

IL_0016:
	{
		Vector2_t6_48  L_2 = CharacterInfo_get_uvTopLeftUnFlipped_m6_884(__this, /*hidden argument*/NULL);
		G_B3_0 = L_2;
	}

IL_001c:
	{
		return G_B3_0;
	}
}
// System.Void UnityEngine.CharacterInfo::set_uvTopLeft(UnityEngine.Vector2)
extern "C" void CharacterInfo_set_uvTopLeft_m6_893 (CharacterInfo_t6_146 * __this, Vector2_t6_48  ___value, const MethodInfo* method)
{
	{
		bool L_0 = (__this->___flipped_6);
		if (!L_0)
		{
			goto IL_0017;
		}
	}
	{
		Vector2_t6_48  L_1 = ___value;
		CharacterInfo_set_uvBottomRightUnFlipped_m6_881(__this, L_1, /*hidden argument*/NULL);
		goto IL_001e;
	}

IL_0017:
	{
		Vector2_t6_48  L_2 = ___value;
		CharacterInfo_set_uvTopLeftUnFlipped_m6_885(__this, L_2, /*hidden argument*/NULL);
	}

IL_001e:
	{
		return;
	}
}
// Conversion methods for marshalling of: UnityEngine.CharacterInfo
extern "C" void CharacterInfo_t6_146_marshal(const CharacterInfo_t6_146& unmarshaled, CharacterInfo_t6_146_marshaled& marshaled)
{
	marshaled.___index_0 = unmarshaled.___index_0;
	marshaled.___uv_1 = unmarshaled.___uv_1;
	marshaled.___vert_2 = unmarshaled.___vert_2;
	marshaled.___width_3 = unmarshaled.___width_3;
	marshaled.___size_4 = unmarshaled.___size_4;
	marshaled.___style_5 = unmarshaled.___style_5;
	marshaled.___flipped_6 = unmarshaled.___flipped_6;
	marshaled.___ascent_7 = unmarshaled.___ascent_7;
}
extern "C" void CharacterInfo_t6_146_marshal_back(const CharacterInfo_t6_146_marshaled& marshaled, CharacterInfo_t6_146& unmarshaled)
{
	unmarshaled.___index_0 = marshaled.___index_0;
	unmarshaled.___uv_1 = marshaled.___uv_1;
	unmarshaled.___vert_2 = marshaled.___vert_2;
	unmarshaled.___width_3 = marshaled.___width_3;
	unmarshaled.___size_4 = marshaled.___size_4;
	unmarshaled.___style_5 = marshaled.___style_5;
	unmarshaled.___flipped_6 = marshaled.___flipped_6;
	unmarshaled.___ascent_7 = marshaled.___ascent_7;
}
// Conversion method for clean up from marshalling of: UnityEngine.CharacterInfo
extern "C" void CharacterInfo_t6_146_marshal_cleanup(CharacterInfo_t6_146_marshaled& marshaled)
{
}
// System.Void UnityEngine.Font/FontTextureRebuildCallback::.ctor(System.Object,System.IntPtr)
extern "C" void FontTextureRebuildCallback__ctor_m6_894 (FontTextureRebuildCallback_t6_147 * __this, Object_t * ___object, IntPtr_t ___method, const MethodInfo* method)
{
	__this->___method_ptr_0 = (methodPointerType)((MethodInfo*)___method.___m_value_0)->method;
	__this->___method_3 = ___method;
	__this->___m_target_2 = ___object;
}
// System.Void UnityEngine.Font/FontTextureRebuildCallback::Invoke()
extern "C" void FontTextureRebuildCallback_Invoke_m6_895 (FontTextureRebuildCallback_t6_147 * __this, const MethodInfo* method)
{
	if(__this->___prev_9 != NULL)
	{
		FontTextureRebuildCallback_Invoke_m6_895((FontTextureRebuildCallback_t6_147 *)__this->___prev_9, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->___method_3.___m_value_0));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->___method_3.___m_value_0));
	if ((__this->___m_target_2 != NULL || MethodHasParameters((MethodInfo*)(__this->___method_3.___m_value_0))) && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Object_t *, Object_t * __this, const MethodInfo* method);
		((FunctionPointerType)__this->___method_ptr_0)(NULL,__this->___m_target_2,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
	else
	{
		typedef void (*FunctionPointerType) (Object_t * __this, const MethodInfo* method);
		((FunctionPointerType)__this->___method_ptr_0)(__this->___m_target_2,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
}
extern "C" void pinvoke_delegate_wrapper_FontTextureRebuildCallback_t6_147(Il2CppObject* delegate)
{
	typedef void (STDCALL *native_function_ptr_type)();
	native_function_ptr_type _il2cpp_pinvoke_func = ((native_function_ptr_type)((Il2CppDelegate*)delegate)->method->method);
	// Native function invocation
	_il2cpp_pinvoke_func();

}
// System.IAsyncResult UnityEngine.Font/FontTextureRebuildCallback::BeginInvoke(System.AsyncCallback,System.Object)
extern "C" Object_t * FontTextureRebuildCallback_BeginInvoke_m6_896 (FontTextureRebuildCallback_t6_147 * __this, AsyncCallback_t1_28 * ___callback, Object_t * ___object, const MethodInfo* method)
{
	void *__d_args[1] = {0};
	return (Object_t *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback, (Il2CppObject*)___object);
}
// System.Void UnityEngine.Font/FontTextureRebuildCallback::EndInvoke(System.IAsyncResult)
extern "C" void FontTextureRebuildCallback_EndInvoke_m6_897 (FontTextureRebuildCallback_t6_147 * __this, Object_t * ___result, const MethodInfo* method)
{
	il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result, 0);
}
// System.Void UnityEngine.Font::.ctor()
extern "C" void Font__ctor_m6_898 (Font_t6_148 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m6_553(__this, /*hidden argument*/NULL);
		Font_Internal_CreateFont_m6_906(NULL /*static, unused*/, __this, (String_t*)NULL, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Font::.ctor(System.String)
extern "C" void Font__ctor_m6_899 (Font_t6_148 * __this, String_t* ___name, const MethodInfo* method)
{
	{
		Object__ctor_m6_553(__this, /*hidden argument*/NULL);
		String_t* L_0 = ___name;
		Font_Internal_CreateFont_m6_906(NULL /*static, unused*/, __this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Font::.ctor(System.String[],System.Int32)
extern "C" void Font__ctor_m6_900 (Font_t6_148 * __this, StringU5BU5D_t1_202* ___names, int32_t ___size, const MethodInfo* method)
{
	{
		Object__ctor_m6_553(__this, /*hidden argument*/NULL);
		StringU5BU5D_t1_202* L_0 = ___names;
		int32_t L_1 = ___size;
		Font_Internal_CreateDynamicFont_m6_907(NULL /*static, unused*/, __this, L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Font::add_textureRebuilt(System.Action`1<UnityEngine.Font>)
extern TypeInfo* Font_t6_148_il2cpp_TypeInfo_var;
extern TypeInfo* Action_1_t1_912_il2cpp_TypeInfo_var;
extern "C" void Font_add_textureRebuilt_m6_901 (Object_t * __this /* static, unused */, Action_1_t1_912 * ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Font_t6_148_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(969);
		Action_1_t1_912_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(970);
		s_Il2CppMethodIntialized = true;
	}
	{
		Action_1_t1_912 * L_0 = ((Font_t6_148_StaticFields*)Font_t6_148_il2cpp_TypeInfo_var->static_fields)->___textureRebuilt_2;
		Action_1_t1_912 * L_1 = ___value;
		Delegate_t1_22 * L_2 = Delegate_Combine_m1_695(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		((Font_t6_148_StaticFields*)Font_t6_148_il2cpp_TypeInfo_var->static_fields)->___textureRebuilt_2 = ((Action_1_t1_912 *)CastclassSealed(L_2, Action_1_t1_912_il2cpp_TypeInfo_var));
		return;
	}
}
// System.Void UnityEngine.Font::remove_textureRebuilt(System.Action`1<UnityEngine.Font>)
extern TypeInfo* Font_t6_148_il2cpp_TypeInfo_var;
extern TypeInfo* Action_1_t1_912_il2cpp_TypeInfo_var;
extern "C" void Font_remove_textureRebuilt_m6_902 (Object_t * __this /* static, unused */, Action_1_t1_912 * ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Font_t6_148_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(969);
		Action_1_t1_912_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(970);
		s_Il2CppMethodIntialized = true;
	}
	{
		Action_1_t1_912 * L_0 = ((Font_t6_148_StaticFields*)Font_t6_148_il2cpp_TypeInfo_var->static_fields)->___textureRebuilt_2;
		Action_1_t1_912 * L_1 = ___value;
		Delegate_t1_22 * L_2 = Delegate_Remove_m1_698(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		((Font_t6_148_StaticFields*)Font_t6_148_il2cpp_TypeInfo_var->static_fields)->___textureRebuilt_2 = ((Action_1_t1_912 *)CastclassSealed(L_2, Action_1_t1_912_il2cpp_TypeInfo_var));
		return;
	}
}
// System.Void UnityEngine.Font::add_m_FontTextureRebuildCallback(UnityEngine.Font/FontTextureRebuildCallback)
extern TypeInfo* FontTextureRebuildCallback_t6_147_il2cpp_TypeInfo_var;
extern "C" void Font_add_m_FontTextureRebuildCallback_m6_903 (Font_t6_148 * __this, FontTextureRebuildCallback_t6_147 * ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		FontTextureRebuildCallback_t6_147_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(971);
		s_Il2CppMethodIntialized = true;
	}
	{
		FontTextureRebuildCallback_t6_147 * L_0 = (__this->___m_FontTextureRebuildCallback_3);
		FontTextureRebuildCallback_t6_147 * L_1 = ___value;
		Delegate_t1_22 * L_2 = Delegate_Combine_m1_695(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		__this->___m_FontTextureRebuildCallback_3 = ((FontTextureRebuildCallback_t6_147 *)CastclassSealed(L_2, FontTextureRebuildCallback_t6_147_il2cpp_TypeInfo_var));
		return;
	}
}
// System.Void UnityEngine.Font::remove_m_FontTextureRebuildCallback(UnityEngine.Font/FontTextureRebuildCallback)
extern TypeInfo* FontTextureRebuildCallback_t6_147_il2cpp_TypeInfo_var;
extern "C" void Font_remove_m_FontTextureRebuildCallback_m6_904 (Font_t6_148 * __this, FontTextureRebuildCallback_t6_147 * ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		FontTextureRebuildCallback_t6_147_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(971);
		s_Il2CppMethodIntialized = true;
	}
	{
		FontTextureRebuildCallback_t6_147 * L_0 = (__this->___m_FontTextureRebuildCallback_3);
		FontTextureRebuildCallback_t6_147 * L_1 = ___value;
		Delegate_t1_22 * L_2 = Delegate_Remove_m1_698(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		__this->___m_FontTextureRebuildCallback_3 = ((FontTextureRebuildCallback_t6_147 *)CastclassSealed(L_2, FontTextureRebuildCallback_t6_147_il2cpp_TypeInfo_var));
		return;
	}
}
// System.String[] UnityEngine.Font::GetOSInstalledFontNames()
extern "C" StringU5BU5D_t1_202* Font_GetOSInstalledFontNames_m6_905 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	typedef StringU5BU5D_t1_202* (*Font_GetOSInstalledFontNames_m6_905_ftn) ();
	static Font_GetOSInstalledFontNames_m6_905_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Font_GetOSInstalledFontNames_m6_905_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Font::GetOSInstalledFontNames()");
	return _il2cpp_icall_func();
}
// System.Void UnityEngine.Font::Internal_CreateFont(UnityEngine.Font,System.String)
extern "C" void Font_Internal_CreateFont_m6_906 (Object_t * __this /* static, unused */, Font_t6_148 * ____font, String_t* ___name, const MethodInfo* method)
{
	typedef void (*Font_Internal_CreateFont_m6_906_ftn) (Font_t6_148 *, String_t*);
	static Font_Internal_CreateFont_m6_906_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Font_Internal_CreateFont_m6_906_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Font::Internal_CreateFont(UnityEngine.Font,System.String)");
	_il2cpp_icall_func(____font, ___name);
}
// System.Void UnityEngine.Font::Internal_CreateDynamicFont(UnityEngine.Font,System.String[],System.Int32)
extern "C" void Font_Internal_CreateDynamicFont_m6_907 (Object_t * __this /* static, unused */, Font_t6_148 * ____font, StringU5BU5D_t1_202* ____names, int32_t ___size, const MethodInfo* method)
{
	typedef void (*Font_Internal_CreateDynamicFont_m6_907_ftn) (Font_t6_148 *, StringU5BU5D_t1_202*, int32_t);
	static Font_Internal_CreateDynamicFont_m6_907_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Font_Internal_CreateDynamicFont_m6_907_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Font::Internal_CreateDynamicFont(UnityEngine.Font,System.String[],System.Int32)");
	_il2cpp_icall_func(____font, ____names, ___size);
}
// UnityEngine.Font UnityEngine.Font::CreateDynamicFontFromOSFont(System.String,System.Int32)
extern TypeInfo* StringU5BU5D_t1_202_il2cpp_TypeInfo_var;
extern TypeInfo* Font_t6_148_il2cpp_TypeInfo_var;
extern "C" Font_t6_148 * Font_CreateDynamicFontFromOSFont_m6_908 (Object_t * __this /* static, unused */, String_t* ___fontname, int32_t ___size, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		StringU5BU5D_t1_202_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(32);
		Font_t6_148_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(969);
		s_Il2CppMethodIntialized = true;
	}
	Font_t6_148 * V_0 = {0};
	{
		StringU5BU5D_t1_202* L_0 = ((StringU5BU5D_t1_202*)SZArrayNew(StringU5BU5D_t1_202_il2cpp_TypeInfo_var, 1));
		String_t* L_1 = ___fontname;
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, 0);
		ArrayElementTypeCheck (L_0, L_1);
		*((String_t**)(String_t**)SZArrayLdElema(L_0, 0, sizeof(String_t*))) = (String_t*)L_1;
		int32_t L_2 = ___size;
		Font_t6_148 * L_3 = (Font_t6_148 *)il2cpp_codegen_object_new (Font_t6_148_il2cpp_TypeInfo_var);
		Font__ctor_m6_900(L_3, L_0, L_2, /*hidden argument*/NULL);
		V_0 = L_3;
		Font_t6_148 * L_4 = V_0;
		return L_4;
	}
}
// UnityEngine.Font UnityEngine.Font::CreateDynamicFontFromOSFont(System.String[],System.Int32)
extern TypeInfo* Font_t6_148_il2cpp_TypeInfo_var;
extern "C" Font_t6_148 * Font_CreateDynamicFontFromOSFont_m6_909 (Object_t * __this /* static, unused */, StringU5BU5D_t1_202* ___fontnames, int32_t ___size, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Font_t6_148_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(969);
		s_Il2CppMethodIntialized = true;
	}
	Font_t6_148 * V_0 = {0};
	{
		StringU5BU5D_t1_202* L_0 = ___fontnames;
		int32_t L_1 = ___size;
		Font_t6_148 * L_2 = (Font_t6_148 *)il2cpp_codegen_object_new (Font_t6_148_il2cpp_TypeInfo_var);
		Font__ctor_m6_900(L_2, L_0, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		Font_t6_148 * L_3 = V_0;
		return L_3;
	}
}
// UnityEngine.Material UnityEngine.Font::get_material()
extern "C" Material_t6_67 * Font_get_material_m6_910 (Font_t6_148 * __this, const MethodInfo* method)
{
	typedef Material_t6_67 * (*Font_get_material_m6_910_ftn) (Font_t6_148 *);
	static Font_get_material_m6_910_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Font_get_material_m6_910_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Font::get_material()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.Font::set_material(UnityEngine.Material)
extern "C" void Font_set_material_m6_911 (Font_t6_148 * __this, Material_t6_67 * ___value, const MethodInfo* method)
{
	typedef void (*Font_set_material_m6_911_ftn) (Font_t6_148 *, Material_t6_67 *);
	static Font_set_material_m6_911_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Font_set_material_m6_911_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Font::set_material(UnityEngine.Material)");
	_il2cpp_icall_func(__this, ___value);
}
// System.Boolean UnityEngine.Font::HasCharacter(System.Char)
extern "C" bool Font_HasCharacter_m6_912 (Font_t6_148 * __this, uint16_t ___c, const MethodInfo* method)
{
	typedef bool (*Font_HasCharacter_m6_912_ftn) (Font_t6_148 *, uint16_t);
	static Font_HasCharacter_m6_912_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Font_HasCharacter_m6_912_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Font::HasCharacter(System.Char)");
	return _il2cpp_icall_func(__this, ___c);
}
// System.String[] UnityEngine.Font::get_fontNames()
extern "C" StringU5BU5D_t1_202* Font_get_fontNames_m6_913 (Font_t6_148 * __this, const MethodInfo* method)
{
	typedef StringU5BU5D_t1_202* (*Font_get_fontNames_m6_913_ftn) (Font_t6_148 *);
	static Font_get_fontNames_m6_913_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Font_get_fontNames_m6_913_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Font::get_fontNames()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.Font::set_fontNames(System.String[])
extern "C" void Font_set_fontNames_m6_914 (Font_t6_148 * __this, StringU5BU5D_t1_202* ___value, const MethodInfo* method)
{
	typedef void (*Font_set_fontNames_m6_914_ftn) (Font_t6_148 *, StringU5BU5D_t1_202*);
	static Font_set_fontNames_m6_914_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Font_set_fontNames_m6_914_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Font::set_fontNames(System.String[])");
	_il2cpp_icall_func(__this, ___value);
}
// UnityEngine.CharacterInfo[] UnityEngine.Font::get_characterInfo()
extern "C" CharacterInfoU5BU5D_t6_260* Font_get_characterInfo_m6_915 (Font_t6_148 * __this, const MethodInfo* method)
{
	typedef CharacterInfoU5BU5D_t6_260* (*Font_get_characterInfo_m6_915_ftn) (Font_t6_148 *);
	static Font_get_characterInfo_m6_915_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Font_get_characterInfo_m6_915_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Font::get_characterInfo()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.Font::set_characterInfo(UnityEngine.CharacterInfo[])
extern "C" void Font_set_characterInfo_m6_916 (Font_t6_148 * __this, CharacterInfoU5BU5D_t6_260* ___value, const MethodInfo* method)
{
	typedef void (*Font_set_characterInfo_m6_916_ftn) (Font_t6_148 *, CharacterInfoU5BU5D_t6_260*);
	static Font_set_characterInfo_m6_916_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Font_set_characterInfo_m6_916_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Font::set_characterInfo(UnityEngine.CharacterInfo[])");
	_il2cpp_icall_func(__this, ___value);
}
// System.Void UnityEngine.Font::RequestCharactersInTexture(System.String,System.Int32,UnityEngine.FontStyle)
extern "C" void Font_RequestCharactersInTexture_m6_917 (Font_t6_148 * __this, String_t* ___characters, int32_t ___size, int32_t ___style, const MethodInfo* method)
{
	typedef void (*Font_RequestCharactersInTexture_m6_917_ftn) (Font_t6_148 *, String_t*, int32_t, int32_t);
	static Font_RequestCharactersInTexture_m6_917_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Font_RequestCharactersInTexture_m6_917_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Font::RequestCharactersInTexture(System.String,System.Int32,UnityEngine.FontStyle)");
	_il2cpp_icall_func(__this, ___characters, ___size, ___style);
}
// System.Void UnityEngine.Font::RequestCharactersInTexture(System.String,System.Int32)
extern "C" void Font_RequestCharactersInTexture_m6_918 (Font_t6_148 * __this, String_t* ___characters, int32_t ___size, const MethodInfo* method)
{
	int32_t V_0 = {0};
	{
		V_0 = 0;
		String_t* L_0 = ___characters;
		int32_t L_1 = ___size;
		int32_t L_2 = V_0;
		Font_RequestCharactersInTexture_m6_917(__this, L_0, L_1, L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Font::RequestCharactersInTexture(System.String)
extern "C" void Font_RequestCharactersInTexture_m6_919 (Font_t6_148 * __this, String_t* ___characters, const MethodInfo* method)
{
	int32_t V_0 = {0};
	int32_t V_1 = 0;
	{
		V_0 = 0;
		V_1 = 0;
		String_t* L_0 = ___characters;
		int32_t L_1 = V_1;
		int32_t L_2 = V_0;
		Font_RequestCharactersInTexture_m6_917(__this, L_0, L_1, L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Font::InvokeTextureRebuilt_Internal(UnityEngine.Font)
extern TypeInfo* Font_t6_148_il2cpp_TypeInfo_var;
extern const MethodInfo* Action_1_Invoke_m1_5599_MethodInfo_var;
extern "C" void Font_InvokeTextureRebuilt_Internal_m6_920 (Object_t * __this /* static, unused */, Font_t6_148 * ___font, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Font_t6_148_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(969);
		Action_1_Invoke_m1_5599_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483806);
		s_Il2CppMethodIntialized = true;
	}
	Action_1_t1_912 * V_0 = {0};
	{
		Action_1_t1_912 * L_0 = ((Font_t6_148_StaticFields*)Font_t6_148_il2cpp_TypeInfo_var->static_fields)->___textureRebuilt_2;
		V_0 = L_0;
		Action_1_t1_912 * L_1 = V_0;
		if (!L_1)
		{
			goto IL_0013;
		}
	}
	{
		Action_1_t1_912 * L_2 = V_0;
		Font_t6_148 * L_3 = ___font;
		NullCheck(L_2);
		Action_1_Invoke_m1_5599(L_2, L_3, /*hidden argument*/Action_1_Invoke_m1_5599_MethodInfo_var);
	}

IL_0013:
	{
		Font_t6_148 * L_4 = ___font;
		NullCheck(L_4);
		FontTextureRebuildCallback_t6_147 * L_5 = (L_4->___m_FontTextureRebuildCallback_3);
		if (!L_5)
		{
			goto IL_0029;
		}
	}
	{
		Font_t6_148 * L_6 = ___font;
		NullCheck(L_6);
		FontTextureRebuildCallback_t6_147 * L_7 = (L_6->___m_FontTextureRebuildCallback_3);
		NullCheck(L_7);
		FontTextureRebuildCallback_Invoke_m6_895(L_7, /*hidden argument*/NULL);
	}

IL_0029:
	{
		return;
	}
}
// UnityEngine.Font/FontTextureRebuildCallback UnityEngine.Font::get_textureRebuildCallback()
extern "C" FontTextureRebuildCallback_t6_147 * Font_get_textureRebuildCallback_m6_921 (Font_t6_148 * __this, const MethodInfo* method)
{
	{
		FontTextureRebuildCallback_t6_147 * L_0 = (__this->___m_FontTextureRebuildCallback_3);
		return L_0;
	}
}
// System.Void UnityEngine.Font::set_textureRebuildCallback(UnityEngine.Font/FontTextureRebuildCallback)
extern "C" void Font_set_textureRebuildCallback_m6_922 (Font_t6_148 * __this, FontTextureRebuildCallback_t6_147 * ___value, const MethodInfo* method)
{
	{
		FontTextureRebuildCallback_t6_147 * L_0 = ___value;
		__this->___m_FontTextureRebuildCallback_3 = L_0;
		return;
	}
}
// System.Int32 UnityEngine.Font::GetMaxVertsForString(System.String)
extern "C" int32_t Font_GetMaxVertsForString_m6_923 (Object_t * __this /* static, unused */, String_t* ___str, const MethodInfo* method)
{
	{
		String_t* L_0 = ___str;
		NullCheck(L_0);
		int32_t L_1 = String_get_Length_m1_428(L_0, /*hidden argument*/NULL);
		return ((int32_t)((int32_t)((int32_t)((int32_t)L_1*(int32_t)4))+(int32_t)4));
	}
}
// System.Boolean UnityEngine.Font::GetCharacterInfo(System.Char,UnityEngine.CharacterInfo&,System.Int32,UnityEngine.FontStyle)
extern "C" bool Font_GetCharacterInfo_m6_924 (Font_t6_148 * __this, uint16_t ___ch, CharacterInfo_t6_146 * ___info, int32_t ___size, int32_t ___style, const MethodInfo* method)
{
	typedef bool (*Font_GetCharacterInfo_m6_924_ftn) (Font_t6_148 *, uint16_t, CharacterInfo_t6_146 *, int32_t, int32_t);
	static Font_GetCharacterInfo_m6_924_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Font_GetCharacterInfo_m6_924_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Font::GetCharacterInfo(System.Char,UnityEngine.CharacterInfo&,System.Int32,UnityEngine.FontStyle)");
	return _il2cpp_icall_func(__this, ___ch, ___info, ___size, ___style);
}
// System.Boolean UnityEngine.Font::GetCharacterInfo(System.Char,UnityEngine.CharacterInfo&,System.Int32)
extern "C" bool Font_GetCharacterInfo_m6_925 (Font_t6_148 * __this, uint16_t ___ch, CharacterInfo_t6_146 * ___info, int32_t ___size, const MethodInfo* method)
{
	int32_t V_0 = {0};
	{
		V_0 = 0;
		uint16_t L_0 = ___ch;
		CharacterInfo_t6_146 * L_1 = ___info;
		int32_t L_2 = ___size;
		int32_t L_3 = V_0;
		bool L_4 = Font_GetCharacterInfo_m6_924(__this, L_0, L_1, L_2, L_3, /*hidden argument*/NULL);
		return L_4;
	}
}
// System.Boolean UnityEngine.Font::GetCharacterInfo(System.Char,UnityEngine.CharacterInfo&)
extern "C" bool Font_GetCharacterInfo_m6_926 (Font_t6_148 * __this, uint16_t ___ch, CharacterInfo_t6_146 * ___info, const MethodInfo* method)
{
	int32_t V_0 = {0};
	int32_t V_1 = 0;
	{
		V_0 = 0;
		V_1 = 0;
		uint16_t L_0 = ___ch;
		CharacterInfo_t6_146 * L_1 = ___info;
		int32_t L_2 = V_1;
		int32_t L_3 = V_0;
		bool L_4 = Font_GetCharacterInfo_m6_924(__this, L_0, L_1, L_2, L_3, /*hidden argument*/NULL);
		return L_4;
	}
}
// System.Boolean UnityEngine.Font::get_dynamic()
extern "C" bool Font_get_dynamic_m6_927 (Font_t6_148 * __this, const MethodInfo* method)
{
	typedef bool (*Font_get_dynamic_m6_927_ftn) (Font_t6_148 *);
	static Font_get_dynamic_m6_927_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Font_get_dynamic_m6_927_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Font::get_dynamic()");
	return _il2cpp_icall_func(__this);
}
// System.Int32 UnityEngine.Font::get_ascent()
extern "C" int32_t Font_get_ascent_m6_928 (Font_t6_148 * __this, const MethodInfo* method)
{
	typedef int32_t (*Font_get_ascent_m6_928_ftn) (Font_t6_148 *);
	static Font_get_ascent_m6_928_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Font_get_ascent_m6_928_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Font::get_ascent()");
	return _il2cpp_icall_func(__this);
}
// System.Int32 UnityEngine.Font::get_lineHeight()
extern "C" int32_t Font_get_lineHeight_m6_929 (Font_t6_148 * __this, const MethodInfo* method)
{
	typedef int32_t (*Font_get_lineHeight_m6_929_ftn) (Font_t6_148 *);
	static Font_get_lineHeight_m6_929_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Font_get_lineHeight_m6_929_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Font::get_lineHeight()");
	return _il2cpp_icall_func(__this);
}
// System.Int32 UnityEngine.Font::get_fontSize()
extern "C" int32_t Font_get_fontSize_m6_930 (Font_t6_148 * __this, const MethodInfo* method)
{
	typedef int32_t (*Font_get_fontSize_m6_930_ftn) (Font_t6_148 *);
	static Font_get_fontSize_m6_930_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Font_get_fontSize_m6_930_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Font::get_fontSize()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.TextGenerator::.ctor()
extern "C" void TextGenerator__ctor_m6_931 (TextGenerator_t6_151 * __this, const MethodInfo* method)
{
	{
		TextGenerator__ctor_m6_932(__this, ((int32_t)50), /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.TextGenerator::.ctor(System.Int32)
extern TypeInfo* List_1_t1_913_il2cpp_TypeInfo_var;
extern TypeInfo* List_1_t1_914_il2cpp_TypeInfo_var;
extern TypeInfo* List_1_t1_915_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1__ctor_m1_5600_MethodInfo_var;
extern const MethodInfo* List_1__ctor_m1_5601_MethodInfo_var;
extern const MethodInfo* List_1__ctor_m1_5602_MethodInfo_var;
extern "C" void TextGenerator__ctor_m6_932 (TextGenerator_t6_151 * __this, int32_t ___initialCapacity, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		List_1_t1_913_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(975);
		List_1_t1_914_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(976);
		List_1_t1_915_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(977);
		List_1__ctor_m1_5600_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483807);
		List_1__ctor_m1_5601_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483808);
		List_1__ctor_m1_5602_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483809);
		s_Il2CppMethodIntialized = true;
	}
	{
		Object__ctor_m1_0(__this, /*hidden argument*/NULL);
		int32_t L_0 = ___initialCapacity;
		List_1_t1_913 * L_1 = (List_1_t1_913 *)il2cpp_codegen_object_new (List_1_t1_913_il2cpp_TypeInfo_var);
		List_1__ctor_m1_5600(L_1, ((int32_t)((int32_t)((int32_t)((int32_t)L_0+(int32_t)1))*(int32_t)4)), /*hidden argument*/List_1__ctor_m1_5600_MethodInfo_var);
		__this->___m_Verts_5 = L_1;
		int32_t L_2 = ___initialCapacity;
		List_1_t1_914 * L_3 = (List_1_t1_914 *)il2cpp_codegen_object_new (List_1_t1_914_il2cpp_TypeInfo_var);
		List_1__ctor_m1_5601(L_3, ((int32_t)((int32_t)L_2+(int32_t)1)), /*hidden argument*/List_1__ctor_m1_5601_MethodInfo_var);
		__this->___m_Characters_6 = L_3;
		List_1_t1_915 * L_4 = (List_1_t1_915 *)il2cpp_codegen_object_new (List_1_t1_915_il2cpp_TypeInfo_var);
		List_1__ctor_m1_5602(L_4, ((int32_t)20), /*hidden argument*/List_1__ctor_m1_5602_MethodInfo_var);
		__this->___m_Lines_7 = L_4;
		TextGenerator_Init_m6_934(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.TextGenerator::System.IDisposable.Dispose()
extern "C" void TextGenerator_System_IDisposable_Dispose_m6_933 (TextGenerator_t6_151 * __this, const MethodInfo* method)
{
	{
		TextGenerator_Dispose_cpp_m6_935(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.TextGenerator::Init()
extern "C" void TextGenerator_Init_m6_934 (TextGenerator_t6_151 * __this, const MethodInfo* method)
{
	typedef void (*TextGenerator_Init_m6_934_ftn) (TextGenerator_t6_151 *);
	static TextGenerator_Init_m6_934_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (TextGenerator_Init_m6_934_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.TextGenerator::Init()");
	_il2cpp_icall_func(__this);
}
// System.Void UnityEngine.TextGenerator::Dispose_cpp()
extern "C" void TextGenerator_Dispose_cpp_m6_935 (TextGenerator_t6_151 * __this, const MethodInfo* method)
{
	typedef void (*TextGenerator_Dispose_cpp_m6_935_ftn) (TextGenerator_t6_151 *);
	static TextGenerator_Dispose_cpp_m6_935_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (TextGenerator_Dispose_cpp_m6_935_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.TextGenerator::Dispose_cpp()");
	_il2cpp_icall_func(__this);
}
// System.Boolean UnityEngine.TextGenerator::Populate_Internal(System.String,UnityEngine.Font,UnityEngine.Color,System.Int32,System.Single,System.Single,UnityEngine.FontStyle,System.Boolean,System.Boolean,System.Int32,System.Int32,UnityEngine.VerticalWrapMode,UnityEngine.HorizontalWrapMode,System.Boolean,UnityEngine.TextAnchor,UnityEngine.Vector2,UnityEngine.Vector2,System.Boolean)
extern "C" bool TextGenerator_Populate_Internal_m6_936 (TextGenerator_t6_151 * __this, String_t* ___str, Font_t6_148 * ___font, Color_t6_40  ___color, int32_t ___fontSize, float ___scaleFactor, float ___lineSpacing, int32_t ___style, bool ___richText, bool ___resizeTextForBestFit, int32_t ___resizeTextMinSize, int32_t ___resizeTextMaxSize, int32_t ___verticalOverFlow, int32_t ___horizontalOverflow, bool ___updateBounds, int32_t ___anchor, Vector2_t6_48  ___extents, Vector2_t6_48  ___pivot, bool ___generateOutOfBounds, const MethodInfo* method)
{
	{
		String_t* L_0 = ___str;
		Font_t6_148 * L_1 = ___font;
		Color_t6_40  L_2 = ___color;
		int32_t L_3 = ___fontSize;
		float L_4 = ___scaleFactor;
		float L_5 = ___lineSpacing;
		int32_t L_6 = ___style;
		bool L_7 = ___richText;
		bool L_8 = ___resizeTextForBestFit;
		int32_t L_9 = ___resizeTextMinSize;
		int32_t L_10 = ___resizeTextMaxSize;
		int32_t L_11 = ___verticalOverFlow;
		int32_t L_12 = ___horizontalOverflow;
		bool L_13 = ___updateBounds;
		int32_t L_14 = ___anchor;
		float L_15 = ((&___extents)->___x_1);
		float L_16 = ((&___extents)->___y_2);
		float L_17 = ((&___pivot)->___x_1);
		float L_18 = ((&___pivot)->___y_2);
		bool L_19 = ___generateOutOfBounds;
		bool L_20 = TextGenerator_Populate_Internal_cpp_m6_937(__this, L_0, L_1, L_2, L_3, L_4, L_5, L_6, L_7, L_8, L_9, L_10, L_11, L_12, L_13, L_14, L_15, L_16, L_17, L_18, L_19, /*hidden argument*/NULL);
		return L_20;
	}
}
// System.Boolean UnityEngine.TextGenerator::Populate_Internal_cpp(System.String,UnityEngine.Font,UnityEngine.Color,System.Int32,System.Single,System.Single,UnityEngine.FontStyle,System.Boolean,System.Boolean,System.Int32,System.Int32,System.Int32,System.Int32,System.Boolean,UnityEngine.TextAnchor,System.Single,System.Single,System.Single,System.Single,System.Boolean)
extern "C" bool TextGenerator_Populate_Internal_cpp_m6_937 (TextGenerator_t6_151 * __this, String_t* ___str, Font_t6_148 * ___font, Color_t6_40  ___color, int32_t ___fontSize, float ___scaleFactor, float ___lineSpacing, int32_t ___style, bool ___richText, bool ___resizeTextForBestFit, int32_t ___resizeTextMinSize, int32_t ___resizeTextMaxSize, int32_t ___verticalOverFlow, int32_t ___horizontalOverflow, bool ___updateBounds, int32_t ___anchor, float ___extentsX, float ___extentsY, float ___pivotX, float ___pivotY, bool ___generateOutOfBounds, const MethodInfo* method)
{
	{
		String_t* L_0 = ___str;
		Font_t6_148 * L_1 = ___font;
		int32_t L_2 = ___fontSize;
		float L_3 = ___scaleFactor;
		float L_4 = ___lineSpacing;
		int32_t L_5 = ___style;
		bool L_6 = ___richText;
		bool L_7 = ___resizeTextForBestFit;
		int32_t L_8 = ___resizeTextMinSize;
		int32_t L_9 = ___resizeTextMaxSize;
		int32_t L_10 = ___verticalOverFlow;
		int32_t L_11 = ___horizontalOverflow;
		bool L_12 = ___updateBounds;
		int32_t L_13 = ___anchor;
		float L_14 = ___extentsX;
		float L_15 = ___extentsY;
		float L_16 = ___pivotX;
		float L_17 = ___pivotY;
		bool L_18 = ___generateOutOfBounds;
		bool L_19 = TextGenerator_INTERNAL_CALL_Populate_Internal_cpp_m6_938(NULL /*static, unused*/, __this, L_0, L_1, (&___color), L_2, L_3, L_4, L_5, L_6, L_7, L_8, L_9, L_10, L_11, L_12, L_13, L_14, L_15, L_16, L_17, L_18, /*hidden argument*/NULL);
		return L_19;
	}
}
// System.Boolean UnityEngine.TextGenerator::INTERNAL_CALL_Populate_Internal_cpp(UnityEngine.TextGenerator,System.String,UnityEngine.Font,UnityEngine.Color&,System.Int32,System.Single,System.Single,UnityEngine.FontStyle,System.Boolean,System.Boolean,System.Int32,System.Int32,System.Int32,System.Int32,System.Boolean,UnityEngine.TextAnchor,System.Single,System.Single,System.Single,System.Single,System.Boolean)
extern "C" bool TextGenerator_INTERNAL_CALL_Populate_Internal_cpp_m6_938 (Object_t * __this /* static, unused */, TextGenerator_t6_151 * ___self, String_t* ___str, Font_t6_148 * ___font, Color_t6_40 * ___color, int32_t ___fontSize, float ___scaleFactor, float ___lineSpacing, int32_t ___style, bool ___richText, bool ___resizeTextForBestFit, int32_t ___resizeTextMinSize, int32_t ___resizeTextMaxSize, int32_t ___verticalOverFlow, int32_t ___horizontalOverflow, bool ___updateBounds, int32_t ___anchor, float ___extentsX, float ___extentsY, float ___pivotX, float ___pivotY, bool ___generateOutOfBounds, const MethodInfo* method)
{
	typedef bool (*TextGenerator_INTERNAL_CALL_Populate_Internal_cpp_m6_938_ftn) (TextGenerator_t6_151 *, String_t*, Font_t6_148 *, Color_t6_40 *, int32_t, float, float, int32_t, bool, bool, int32_t, int32_t, int32_t, int32_t, bool, int32_t, float, float, float, float, bool);
	static TextGenerator_INTERNAL_CALL_Populate_Internal_cpp_m6_938_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (TextGenerator_INTERNAL_CALL_Populate_Internal_cpp_m6_938_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.TextGenerator::INTERNAL_CALL_Populate_Internal_cpp(UnityEngine.TextGenerator,System.String,UnityEngine.Font,UnityEngine.Color&,System.Int32,System.Single,System.Single,UnityEngine.FontStyle,System.Boolean,System.Boolean,System.Int32,System.Int32,System.Int32,System.Int32,System.Boolean,UnityEngine.TextAnchor,System.Single,System.Single,System.Single,System.Single,System.Boolean)");
	return _il2cpp_icall_func(___self, ___str, ___font, ___color, ___fontSize, ___scaleFactor, ___lineSpacing, ___style, ___richText, ___resizeTextForBestFit, ___resizeTextMinSize, ___resizeTextMaxSize, ___verticalOverFlow, ___horizontalOverflow, ___updateBounds, ___anchor, ___extentsX, ___extentsY, ___pivotX, ___pivotY, ___generateOutOfBounds);
}
// UnityEngine.Rect UnityEngine.TextGenerator::get_rectExtents()
extern "C" Rect_t6_52  TextGenerator_get_rectExtents_m6_939 (TextGenerator_t6_151 * __this, const MethodInfo* method)
{
	Rect_t6_52  V_0 = {0};
	{
		TextGenerator_INTERNAL_get_rectExtents_m6_940(__this, (&V_0), /*hidden argument*/NULL);
		Rect_t6_52  L_0 = V_0;
		return L_0;
	}
}
// System.Void UnityEngine.TextGenerator::INTERNAL_get_rectExtents(UnityEngine.Rect&)
extern "C" void TextGenerator_INTERNAL_get_rectExtents_m6_940 (TextGenerator_t6_151 * __this, Rect_t6_52 * ___value, const MethodInfo* method)
{
	typedef void (*TextGenerator_INTERNAL_get_rectExtents_m6_940_ftn) (TextGenerator_t6_151 *, Rect_t6_52 *);
	static TextGenerator_INTERNAL_get_rectExtents_m6_940_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (TextGenerator_INTERNAL_get_rectExtents_m6_940_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.TextGenerator::INTERNAL_get_rectExtents(UnityEngine.Rect&)");
	_il2cpp_icall_func(__this, ___value);
}
// System.Int32 UnityEngine.TextGenerator::get_vertexCount()
extern "C" int32_t TextGenerator_get_vertexCount_m6_941 (TextGenerator_t6_151 * __this, const MethodInfo* method)
{
	typedef int32_t (*TextGenerator_get_vertexCount_m6_941_ftn) (TextGenerator_t6_151 *);
	static TextGenerator_get_vertexCount_m6_941_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (TextGenerator_get_vertexCount_m6_941_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.TextGenerator::get_vertexCount()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.TextGenerator::GetVerticesInternal(System.Object)
extern "C" void TextGenerator_GetVerticesInternal_m6_942 (TextGenerator_t6_151 * __this, Object_t * ___vertices, const MethodInfo* method)
{
	typedef void (*TextGenerator_GetVerticesInternal_m6_942_ftn) (TextGenerator_t6_151 *, Object_t *);
	static TextGenerator_GetVerticesInternal_m6_942_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (TextGenerator_GetVerticesInternal_m6_942_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.TextGenerator::GetVerticesInternal(System.Object)");
	_il2cpp_icall_func(__this, ___vertices);
}
// UnityEngine.UIVertex[] UnityEngine.TextGenerator::GetVerticesArray()
extern "C" UIVertexU5BU5D_t6_261* TextGenerator_GetVerticesArray_m6_943 (TextGenerator_t6_151 * __this, const MethodInfo* method)
{
	typedef UIVertexU5BU5D_t6_261* (*TextGenerator_GetVerticesArray_m6_943_ftn) (TextGenerator_t6_151 *);
	static TextGenerator_GetVerticesArray_m6_943_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (TextGenerator_GetVerticesArray_m6_943_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.TextGenerator::GetVerticesArray()");
	return _il2cpp_icall_func(__this);
}
// System.Int32 UnityEngine.TextGenerator::get_characterCount()
extern "C" int32_t TextGenerator_get_characterCount_m6_944 (TextGenerator_t6_151 * __this, const MethodInfo* method)
{
	typedef int32_t (*TextGenerator_get_characterCount_m6_944_ftn) (TextGenerator_t6_151 *);
	static TextGenerator_get_characterCount_m6_944_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (TextGenerator_get_characterCount_m6_944_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.TextGenerator::get_characterCount()");
	return _il2cpp_icall_func(__this);
}
// System.Int32 UnityEngine.TextGenerator::get_characterCountVisible()
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* Mathf_t6_58_il2cpp_TypeInfo_var;
extern "C" int32_t TextGenerator_get_characterCountVisible_m6_945 (TextGenerator_t6_151 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(11);
		Mathf_t6_58_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(940);
		s_Il2CppMethodIntialized = true;
	}
	int32_t G_B3_0 = 0;
	{
		String_t* L_0 = (__this->___m_LastString_1);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_1 = String_IsNullOrEmpty_m1_397(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0016;
		}
	}
	{
		G_B3_0 = 0;
		goto IL_0036;
	}

IL_0016:
	{
		String_t* L_2 = (__this->___m_LastString_1);
		NullCheck(L_2);
		int32_t L_3 = String_get_Length_m1_428(L_2, /*hidden argument*/NULL);
		int32_t L_4 = TextGenerator_get_vertexCount_m6_941(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t6_58_il2cpp_TypeInfo_var);
		int32_t L_5 = Mathf_Max_m6_384(NULL /*static, unused*/, 0, ((int32_t)((int32_t)((int32_t)((int32_t)L_4-(int32_t)4))/(int32_t)4)), /*hidden argument*/NULL);
		int32_t L_6 = Mathf_Min_m6_382(NULL /*static, unused*/, L_3, L_5, /*hidden argument*/NULL);
		G_B3_0 = L_6;
	}

IL_0036:
	{
		return G_B3_0;
	}
}
// System.Void UnityEngine.TextGenerator::GetCharactersInternal(System.Object)
extern "C" void TextGenerator_GetCharactersInternal_m6_946 (TextGenerator_t6_151 * __this, Object_t * ___characters, const MethodInfo* method)
{
	typedef void (*TextGenerator_GetCharactersInternal_m6_946_ftn) (TextGenerator_t6_151 *, Object_t *);
	static TextGenerator_GetCharactersInternal_m6_946_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (TextGenerator_GetCharactersInternal_m6_946_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.TextGenerator::GetCharactersInternal(System.Object)");
	_il2cpp_icall_func(__this, ___characters);
}
// UnityEngine.UICharInfo[] UnityEngine.TextGenerator::GetCharactersArray()
extern "C" UICharInfoU5BU5D_t6_262* TextGenerator_GetCharactersArray_m6_947 (TextGenerator_t6_151 * __this, const MethodInfo* method)
{
	typedef UICharInfoU5BU5D_t6_262* (*TextGenerator_GetCharactersArray_m6_947_ftn) (TextGenerator_t6_151 *);
	static TextGenerator_GetCharactersArray_m6_947_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (TextGenerator_GetCharactersArray_m6_947_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.TextGenerator::GetCharactersArray()");
	return _il2cpp_icall_func(__this);
}
// System.Int32 UnityEngine.TextGenerator::get_lineCount()
extern "C" int32_t TextGenerator_get_lineCount_m6_948 (TextGenerator_t6_151 * __this, const MethodInfo* method)
{
	typedef int32_t (*TextGenerator_get_lineCount_m6_948_ftn) (TextGenerator_t6_151 *);
	static TextGenerator_get_lineCount_m6_948_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (TextGenerator_get_lineCount_m6_948_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.TextGenerator::get_lineCount()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.TextGenerator::GetLinesInternal(System.Object)
extern "C" void TextGenerator_GetLinesInternal_m6_949 (TextGenerator_t6_151 * __this, Object_t * ___lines, const MethodInfo* method)
{
	typedef void (*TextGenerator_GetLinesInternal_m6_949_ftn) (TextGenerator_t6_151 *, Object_t *);
	static TextGenerator_GetLinesInternal_m6_949_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (TextGenerator_GetLinesInternal_m6_949_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.TextGenerator::GetLinesInternal(System.Object)");
	_il2cpp_icall_func(__this, ___lines);
}
// UnityEngine.UILineInfo[] UnityEngine.TextGenerator::GetLinesArray()
extern "C" UILineInfoU5BU5D_t6_263* TextGenerator_GetLinesArray_m6_950 (TextGenerator_t6_151 * __this, const MethodInfo* method)
{
	typedef UILineInfoU5BU5D_t6_263* (*TextGenerator_GetLinesArray_m6_950_ftn) (TextGenerator_t6_151 *);
	static TextGenerator_GetLinesArray_m6_950_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (TextGenerator_GetLinesArray_m6_950_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.TextGenerator::GetLinesArray()");
	return _il2cpp_icall_func(__this);
}
// System.Int32 UnityEngine.TextGenerator::get_fontSizeUsedForBestFit()
extern "C" int32_t TextGenerator_get_fontSizeUsedForBestFit_m6_951 (TextGenerator_t6_151 * __this, const MethodInfo* method)
{
	typedef int32_t (*TextGenerator_get_fontSizeUsedForBestFit_m6_951_ftn) (TextGenerator_t6_151 *);
	static TextGenerator_get_fontSizeUsedForBestFit_m6_951_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (TextGenerator_get_fontSizeUsedForBestFit_m6_951_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.TextGenerator::get_fontSizeUsedForBestFit()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.TextGenerator::Finalize()
extern TypeInfo* IDisposable_t1_835_il2cpp_TypeInfo_var;
extern "C" void TextGenerator_Finalize_m6_952 (TextGenerator_t6_151 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		IDisposable_t1_835_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(142);
		s_Il2CppMethodIntialized = true;
	}
	Exception_t1_33 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1_33 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1_835_il2cpp_TypeInfo_var, __this);
		IL2CPP_LEAVE(0x12, FINALLY_000b);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1_33 *)e.ex;
		goto FINALLY_000b;
	}

FINALLY_000b:
	{ // begin finally (depth: 1)
		Object_Finalize_m1_3(__this, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(11)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(11)
	{
		IL2CPP_JUMP_TBL(0x12, IL_0012)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1_33 *)
	}

IL_0012:
	{
		return;
	}
}
// UnityEngine.TextGenerationSettings UnityEngine.TextGenerator::ValidatedSettings(UnityEngine.TextGenerationSettings)
extern Il2CppCodeGenString* _stringLiteral2713;
extern Il2CppCodeGenString* _stringLiteral2714;
extern "C" TextGenerationSettings_t6_152  TextGenerator_ValidatedSettings_m6_953 (TextGenerator_t6_151 * __this, TextGenerationSettings_t6_152  ___settings, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		_stringLiteral2713 = il2cpp_codegen_string_literal_from_index(2713);
		_stringLiteral2714 = il2cpp_codegen_string_literal_from_index(2714);
		s_Il2CppMethodIntialized = true;
	}
	{
		Font_t6_148 * L_0 = ((&___settings)->___font_0);
		bool L_1 = Object_op_Inequality_m6_580(NULL /*static, unused*/, L_0, (Object_t6_5 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0025;
		}
	}
	{
		Font_t6_148 * L_2 = ((&___settings)->___font_0);
		NullCheck(L_2);
		bool L_3 = Font_get_dynamic_m6_927(L_2, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_0025;
		}
	}
	{
		TextGenerationSettings_t6_152  L_4 = ___settings;
		return L_4;
	}

IL_0025:
	{
		int32_t L_5 = ((&___settings)->___fontSize_2);
		if (L_5)
		{
			goto IL_003d;
		}
	}
	{
		int32_t L_6 = ((&___settings)->___fontStyle_6);
		if (!L_6)
		{
			goto IL_0057;
		}
	}

IL_003d:
	{
		Debug_LogWarning_m6_491(NULL /*static, unused*/, _stringLiteral2713, /*hidden argument*/NULL);
		(&___settings)->___fontSize_2 = 0;
		(&___settings)->___fontStyle_6 = 0;
	}

IL_0057:
	{
		bool L_7 = ((&___settings)->___resizeTextForBestFit_8);
		if (!L_7)
		{
			goto IL_0075;
		}
	}
	{
		Debug_LogWarning_m6_491(NULL /*static, unused*/, _stringLiteral2714, /*hidden argument*/NULL);
		(&___settings)->___resizeTextForBestFit_8 = 0;
	}

IL_0075:
	{
		TextGenerationSettings_t6_152  L_8 = ___settings;
		return L_8;
	}
}
// System.Void UnityEngine.TextGenerator::Invalidate()
extern "C" void TextGenerator_Invalidate_m6_954 (TextGenerator_t6_151 * __this, const MethodInfo* method)
{
	{
		__this->___m_HasGenerated_3 = 0;
		return;
	}
}
// System.Void UnityEngine.TextGenerator::GetCharacters(System.Collections.Generic.List`1<UnityEngine.UICharInfo>)
extern "C" void TextGenerator_GetCharacters_m6_955 (TextGenerator_t6_151 * __this, List_1_t1_914 * ___characters, const MethodInfo* method)
{
	{
		List_1_t1_914 * L_0 = ___characters;
		TextGenerator_GetCharactersInternal_m6_946(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.TextGenerator::GetLines(System.Collections.Generic.List`1<UnityEngine.UILineInfo>)
extern "C" void TextGenerator_GetLines_m6_956 (TextGenerator_t6_151 * __this, List_1_t1_915 * ___lines, const MethodInfo* method)
{
	{
		List_1_t1_915 * L_0 = ___lines;
		TextGenerator_GetLinesInternal_m6_949(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.TextGenerator::GetVertices(System.Collections.Generic.List`1<UnityEngine.UIVertex>)
extern "C" void TextGenerator_GetVertices_m6_957 (TextGenerator_t6_151 * __this, List_1_t1_913 * ___vertices, const MethodInfo* method)
{
	{
		List_1_t1_913 * L_0 = ___vertices;
		TextGenerator_GetVerticesInternal_m6_942(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Single UnityEngine.TextGenerator::GetPreferredWidth(System.String,UnityEngine.TextGenerationSettings)
extern "C" float TextGenerator_GetPreferredWidth_m6_958 (TextGenerator_t6_151 * __this, String_t* ___str, TextGenerationSettings_t6_152  ___settings, const MethodInfo* method)
{
	Rect_t6_52  V_0 = {0};
	{
		(&___settings)->___horizontalOverflow_13 = 1;
		(&___settings)->___verticalOverflow_12 = 1;
		(&___settings)->___updateBounds_11 = 1;
		String_t* L_0 = ___str;
		TextGenerationSettings_t6_152  L_1 = ___settings;
		TextGenerator_Populate_m6_960(__this, L_0, L_1, /*hidden argument*/NULL);
		Rect_t6_52  L_2 = TextGenerator_get_rectExtents_m6_939(__this, /*hidden argument*/NULL);
		V_0 = L_2;
		float L_3 = Rect_get_width_m6_277((&V_0), /*hidden argument*/NULL);
		return L_3;
	}
}
// System.Single UnityEngine.TextGenerator::GetPreferredHeight(System.String,UnityEngine.TextGenerationSettings)
extern "C" float TextGenerator_GetPreferredHeight_m6_959 (TextGenerator_t6_151 * __this, String_t* ___str, TextGenerationSettings_t6_152  ___settings, const MethodInfo* method)
{
	Rect_t6_52  V_0 = {0};
	{
		(&___settings)->___verticalOverflow_12 = 1;
		(&___settings)->___updateBounds_11 = 1;
		String_t* L_0 = ___str;
		TextGenerationSettings_t6_152  L_1 = ___settings;
		TextGenerator_Populate_m6_960(__this, L_0, L_1, /*hidden argument*/NULL);
		Rect_t6_52  L_2 = TextGenerator_get_rectExtents_m6_939(__this, /*hidden argument*/NULL);
		V_0 = L_2;
		float L_3 = Rect_get_height_m6_279((&V_0), /*hidden argument*/NULL);
		return L_3;
	}
}
// System.Boolean UnityEngine.TextGenerator::Populate(System.String,UnityEngine.TextGenerationSettings)
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern "C" bool TextGenerator_Populate_m6_960 (TextGenerator_t6_151 * __this, String_t* ___str, TextGenerationSettings_t6_152  ___settings, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(11);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool L_0 = (__this->___m_HasGenerated_3);
		if (!L_0)
		{
			goto IL_0035;
		}
	}
	{
		String_t* L_1 = ___str;
		String_t* L_2 = (__this->___m_LastString_1);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_3 = String_op_Equality_m1_454(NULL /*static, unused*/, L_1, L_2, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_0035;
		}
	}
	{
		TextGenerationSettings_t6_152  L_4 = (__this->___m_LastSettings_2);
		bool L_5 = TextGenerationSettings_Equals_m6_1621((&___settings), L_4, /*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_0035;
		}
	}
	{
		bool L_6 = (__this->___m_LastValid_4);
		return L_6;
	}

IL_0035:
	{
		String_t* L_7 = ___str;
		TextGenerationSettings_t6_152  L_8 = ___settings;
		bool L_9 = TextGenerator_PopulateAlways_m6_961(__this, L_7, L_8, /*hidden argument*/NULL);
		return L_9;
	}
}
// System.Boolean UnityEngine.TextGenerator::PopulateAlways(System.String,UnityEngine.TextGenerationSettings)
extern "C" bool TextGenerator_PopulateAlways_m6_961 (TextGenerator_t6_151 * __this, String_t* ___str, TextGenerationSettings_t6_152  ___settings, const MethodInfo* method)
{
	TextGenerationSettings_t6_152  V_0 = {0};
	{
		String_t* L_0 = ___str;
		__this->___m_LastString_1 = L_0;
		__this->___m_HasGenerated_3 = 1;
		__this->___m_CachedVerts_8 = 0;
		__this->___m_CachedCharacters_9 = 0;
		__this->___m_CachedLines_10 = 0;
		TextGenerationSettings_t6_152  L_1 = ___settings;
		__this->___m_LastSettings_2 = L_1;
		TextGenerationSettings_t6_152  L_2 = ___settings;
		TextGenerationSettings_t6_152  L_3 = TextGenerator_ValidatedSettings_m6_953(__this, L_2, /*hidden argument*/NULL);
		V_0 = L_3;
		String_t* L_4 = ___str;
		Font_t6_148 * L_5 = ((&V_0)->___font_0);
		Color_t6_40  L_6 = ((&V_0)->___color_1);
		int32_t L_7 = ((&V_0)->___fontSize_2);
		float L_8 = ((&V_0)->___scaleFactor_5);
		float L_9 = ((&V_0)->___lineSpacing_3);
		int32_t L_10 = ((&V_0)->___fontStyle_6);
		bool L_11 = ((&V_0)->___richText_4);
		bool L_12 = ((&V_0)->___resizeTextForBestFit_8);
		int32_t L_13 = ((&V_0)->___resizeTextMinSize_9);
		int32_t L_14 = ((&V_0)->___resizeTextMaxSize_10);
		int32_t L_15 = ((&V_0)->___verticalOverflow_12);
		int32_t L_16 = ((&V_0)->___horizontalOverflow_13);
		bool L_17 = ((&V_0)->___updateBounds_11);
		int32_t L_18 = ((&V_0)->___textAnchor_7);
		Vector2_t6_48  L_19 = ((&V_0)->___generationExtents_14);
		Vector2_t6_48  L_20 = ((&V_0)->___pivot_15);
		bool L_21 = ((&V_0)->___generateOutOfBounds_16);
		bool L_22 = TextGenerator_Populate_Internal_m6_936(__this, L_4, L_5, L_6, L_7, L_8, L_9, L_10, L_11, L_12, L_13, L_14, L_15, L_16, L_17, L_18, L_19, L_20, L_21, /*hidden argument*/NULL);
		__this->___m_LastValid_4 = L_22;
		bool L_23 = (__this->___m_LastValid_4);
		return L_23;
	}
}
// System.Collections.Generic.IList`1<UnityEngine.UIVertex> UnityEngine.TextGenerator::get_verts()
extern "C" Object_t* TextGenerator_get_verts_m6_962 (TextGenerator_t6_151 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (__this->___m_CachedVerts_8);
		if (L_0)
		{
			goto IL_001e;
		}
	}
	{
		List_1_t1_913 * L_1 = (__this->___m_Verts_5);
		TextGenerator_GetVertices_m6_957(__this, L_1, /*hidden argument*/NULL);
		__this->___m_CachedVerts_8 = 1;
	}

IL_001e:
	{
		List_1_t1_913 * L_2 = (__this->___m_Verts_5);
		return L_2;
	}
}
// System.Collections.Generic.IList`1<UnityEngine.UICharInfo> UnityEngine.TextGenerator::get_characters()
extern "C" Object_t* TextGenerator_get_characters_m6_963 (TextGenerator_t6_151 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (__this->___m_CachedCharacters_9);
		if (L_0)
		{
			goto IL_001e;
		}
	}
	{
		List_1_t1_914 * L_1 = (__this->___m_Characters_6);
		TextGenerator_GetCharacters_m6_955(__this, L_1, /*hidden argument*/NULL);
		__this->___m_CachedCharacters_9 = 1;
	}

IL_001e:
	{
		List_1_t1_914 * L_2 = (__this->___m_Characters_6);
		return L_2;
	}
}
// System.Collections.Generic.IList`1<UnityEngine.UILineInfo> UnityEngine.TextGenerator::get_lines()
extern "C" Object_t* TextGenerator_get_lines_m6_964 (TextGenerator_t6_151 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (__this->___m_CachedLines_10);
		if (L_0)
		{
			goto IL_001e;
		}
	}
	{
		List_1_t1_915 * L_1 = (__this->___m_Lines_7);
		TextGenerator_GetLines_m6_956(__this, L_1, /*hidden argument*/NULL);
		__this->___m_CachedLines_10 = 1;
	}

IL_001e:
	{
		List_1_t1_915 * L_2 = (__this->___m_Lines_7);
		return L_2;
	}
}
// System.Void UnityEngine.UIVertex::.cctor()
extern TypeInfo* UIVertex_t6_153_il2cpp_TypeInfo_var;
extern "C" void UIVertex__cctor_m6_965 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		UIVertex_t6_153_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(972);
		s_Il2CppMethodIntialized = true;
	}
	UIVertex_t6_153  V_0 = {0};
	{
		Color32_t6_50  L_0 = {0};
		Color32__ctor_m6_241(&L_0, ((int32_t)255), ((int32_t)255), ((int32_t)255), ((int32_t)255), /*hidden argument*/NULL);
		((UIVertex_t6_153_StaticFields*)UIVertex_t6_153_il2cpp_TypeInfo_var->static_fields)->___s_DefaultColor_6 = L_0;
		Vector4_t6_55  L_1 = {0};
		Vector4__ctor_m6_363(&L_1, (1.0f), (0.0f), (0.0f), (-1.0f), /*hidden argument*/NULL);
		((UIVertex_t6_153_StaticFields*)UIVertex_t6_153_il2cpp_TypeInfo_var->static_fields)->___s_DefaultTangent_7 = L_1;
		Initobj (UIVertex_t6_153_il2cpp_TypeInfo_var, (&V_0));
		Vector3_t6_49  L_2 = Vector3_get_zero_m6_213(NULL /*static, unused*/, /*hidden argument*/NULL);
		(&V_0)->___position_0 = L_2;
		Vector3_t6_49  L_3 = Vector3_get_back_m6_216(NULL /*static, unused*/, /*hidden argument*/NULL);
		(&V_0)->___normal_1 = L_3;
		Vector4_t6_55  L_4 = ((UIVertex_t6_153_StaticFields*)UIVertex_t6_153_il2cpp_TypeInfo_var->static_fields)->___s_DefaultTangent_7;
		(&V_0)->___tangent_5 = L_4;
		Color32_t6_50  L_5 = ((UIVertex_t6_153_StaticFields*)UIVertex_t6_153_il2cpp_TypeInfo_var->static_fields)->___s_DefaultColor_6;
		(&V_0)->___color_2 = L_5;
		Vector2_t6_48  L_6 = Vector2_get_zero_m6_182(NULL /*static, unused*/, /*hidden argument*/NULL);
		(&V_0)->___uv0_3 = L_6;
		Vector2_t6_48  L_7 = Vector2_get_zero_m6_182(NULL /*static, unused*/, /*hidden argument*/NULL);
		(&V_0)->___uv1_4 = L_7;
		UIVertex_t6_153  L_8 = V_0;
		((UIVertex_t6_153_StaticFields*)UIVertex_t6_153_il2cpp_TypeInfo_var->static_fields)->___simpleVert_8 = L_8;
		return;
	}
}
// System.Void UnityEngine.Event::.ctor()
extern "C" void Event__ctor_m6_966 (Event_t6_154 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1_0(__this, /*hidden argument*/NULL);
		Event_Init_m6_998(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Event::.ctor(UnityEngine.Event)
extern TypeInfo* ArgumentException_t1_646_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2715;
extern "C" void Event__ctor_m6_967 (Event_t6_154 * __this, Event_t6_154 * ___other, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ArgumentException_t1_646_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4);
		_stringLiteral2715 = il2cpp_codegen_string_literal_from_index(2715);
		s_Il2CppMethodIntialized = true;
	}
	{
		Object__ctor_m1_0(__this, /*hidden argument*/NULL);
		Event_t6_154 * L_0 = ___other;
		if (L_0)
		{
			goto IL_0017;
		}
	}
	{
		ArgumentException_t1_646 * L_1 = (ArgumentException_t1_646 *)il2cpp_codegen_object_new (ArgumentException_t1_646_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m1_4605(L_1, _stringLiteral2715, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_1);
	}

IL_0017:
	{
		Event_t6_154 * L_2 = ___other;
		Event_InitCopy_m6_1000(__this, L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Event::.ctor(System.IntPtr)
extern "C" void Event__ctor_m6_968 (Event_t6_154 * __this, IntPtr_t ___ptr, const MethodInfo* method)
{
	{
		Object__ctor_m1_0(__this, /*hidden argument*/NULL);
		IntPtr_t L_0 = ___ptr;
		Event_InitPtr_m6_1001(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Event::Finalize()
extern "C" void Event_Finalize_m6_969 (Event_t6_154 * __this, const MethodInfo* method)
{
	Exception_t1_33 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1_33 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		Event_Cleanup_m6_999(__this, /*hidden argument*/NULL);
		IL2CPP_LEAVE(0x12, FINALLY_000b);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1_33 *)e.ex;
		goto FINALLY_000b;
	}

FINALLY_000b:
	{ // begin finally (depth: 1)
		Object_Finalize_m1_3(__this, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(11)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(11)
	{
		IL2CPP_JUMP_TBL(0x12, IL_0012)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1_33 *)
	}

IL_0012:
	{
		return;
	}
}
// UnityEngine.Vector2 UnityEngine.Event::get_mousePosition()
extern "C" Vector2_t6_48  Event_get_mousePosition_m6_970 (Event_t6_154 * __this, const MethodInfo* method)
{
	Vector2_t6_48  V_0 = {0};
	{
		Event_Internal_GetMousePosition_m6_1008(__this, (&V_0), /*hidden argument*/NULL);
		Vector2_t6_48  L_0 = V_0;
		return L_0;
	}
}
// System.Void UnityEngine.Event::set_mousePosition(UnityEngine.Vector2)
extern "C" void Event_set_mousePosition_m6_971 (Event_t6_154 * __this, Vector2_t6_48  ___value, const MethodInfo* method)
{
	{
		Vector2_t6_48  L_0 = ___value;
		Event_Internal_SetMousePosition_m6_1006(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Vector2 UnityEngine.Event::get_delta()
extern "C" Vector2_t6_48  Event_get_delta_m6_972 (Event_t6_154 * __this, const MethodInfo* method)
{
	Vector2_t6_48  V_0 = {0};
	{
		Event_Internal_GetMouseDelta_m6_1011(__this, (&V_0), /*hidden argument*/NULL);
		Vector2_t6_48  L_0 = V_0;
		return L_0;
	}
}
// System.Void UnityEngine.Event::set_delta(UnityEngine.Vector2)
extern "C" void Event_set_delta_m6_973 (Event_t6_154 * __this, Vector2_t6_48  ___value, const MethodInfo* method)
{
	{
		Vector2_t6_48  L_0 = ___value;
		Event_Internal_SetMouseDelta_m6_1009(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Ray UnityEngine.Event::get_mouseRay()
extern "C" Ray_t6_56  Event_get_mouseRay_m6_974 (Event_t6_154 * __this, const MethodInfo* method)
{
	{
		Vector3_t6_49  L_0 = Vector3_get_up_m6_217(NULL /*static, unused*/, /*hidden argument*/NULL);
		Vector3_t6_49  L_1 = Vector3_get_up_m6_217(NULL /*static, unused*/, /*hidden argument*/NULL);
		Ray_t6_56  L_2 = {0};
		Ray__ctor_m6_371(&L_2, L_0, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// System.Void UnityEngine.Event::set_mouseRay(UnityEngine.Ray)
extern "C" void Event_set_mouseRay_m6_975 (Event_t6_154 * __this, Ray_t6_56  ___value, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Boolean UnityEngine.Event::get_shift()
extern "C" bool Event_get_shift_m6_976 (Event_t6_154 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = Event_get_modifiers_m6_1014(__this, /*hidden argument*/NULL);
		return ((((int32_t)((((int32_t)((int32_t)((int32_t)L_0&(int32_t)1))) == ((int32_t)0))? 1 : 0)) == ((int32_t)0))? 1 : 0);
	}
}
// System.Void UnityEngine.Event::set_shift(System.Boolean)
extern "C" void Event_set_shift_m6_977 (Event_t6_154 * __this, bool ___value, const MethodInfo* method)
{
	{
		bool L_0 = ___value;
		if (L_0)
		{
			goto IL_001a;
		}
	}
	{
		int32_t L_1 = Event_get_modifiers_m6_1014(__this, /*hidden argument*/NULL);
		Event_set_modifiers_m6_1015(__this, ((int32_t)((int32_t)L_1&(int32_t)((int32_t)-2))), /*hidden argument*/NULL);
		goto IL_0028;
	}

IL_001a:
	{
		int32_t L_2 = Event_get_modifiers_m6_1014(__this, /*hidden argument*/NULL);
		Event_set_modifiers_m6_1015(__this, ((int32_t)((int32_t)L_2|(int32_t)1)), /*hidden argument*/NULL);
	}

IL_0028:
	{
		return;
	}
}
// System.Boolean UnityEngine.Event::get_control()
extern "C" bool Event_get_control_m6_978 (Event_t6_154 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = Event_get_modifiers_m6_1014(__this, /*hidden argument*/NULL);
		return ((((int32_t)((((int32_t)((int32_t)((int32_t)L_0&(int32_t)2))) == ((int32_t)0))? 1 : 0)) == ((int32_t)0))? 1 : 0);
	}
}
// System.Void UnityEngine.Event::set_control(System.Boolean)
extern "C" void Event_set_control_m6_979 (Event_t6_154 * __this, bool ___value, const MethodInfo* method)
{
	{
		bool L_0 = ___value;
		if (L_0)
		{
			goto IL_001a;
		}
	}
	{
		int32_t L_1 = Event_get_modifiers_m6_1014(__this, /*hidden argument*/NULL);
		Event_set_modifiers_m6_1015(__this, ((int32_t)((int32_t)L_1&(int32_t)((int32_t)-3))), /*hidden argument*/NULL);
		goto IL_0028;
	}

IL_001a:
	{
		int32_t L_2 = Event_get_modifiers_m6_1014(__this, /*hidden argument*/NULL);
		Event_set_modifiers_m6_1015(__this, ((int32_t)((int32_t)L_2|(int32_t)2)), /*hidden argument*/NULL);
	}

IL_0028:
	{
		return;
	}
}
// System.Boolean UnityEngine.Event::get_alt()
extern "C" bool Event_get_alt_m6_980 (Event_t6_154 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = Event_get_modifiers_m6_1014(__this, /*hidden argument*/NULL);
		return ((((int32_t)((((int32_t)((int32_t)((int32_t)L_0&(int32_t)4))) == ((int32_t)0))? 1 : 0)) == ((int32_t)0))? 1 : 0);
	}
}
// System.Void UnityEngine.Event::set_alt(System.Boolean)
extern "C" void Event_set_alt_m6_981 (Event_t6_154 * __this, bool ___value, const MethodInfo* method)
{
	{
		bool L_0 = ___value;
		if (L_0)
		{
			goto IL_001a;
		}
	}
	{
		int32_t L_1 = Event_get_modifiers_m6_1014(__this, /*hidden argument*/NULL);
		Event_set_modifiers_m6_1015(__this, ((int32_t)((int32_t)L_1&(int32_t)((int32_t)-5))), /*hidden argument*/NULL);
		goto IL_0028;
	}

IL_001a:
	{
		int32_t L_2 = Event_get_modifiers_m6_1014(__this, /*hidden argument*/NULL);
		Event_set_modifiers_m6_1015(__this, ((int32_t)((int32_t)L_2|(int32_t)4)), /*hidden argument*/NULL);
	}

IL_0028:
	{
		return;
	}
}
// System.Boolean UnityEngine.Event::get_command()
extern "C" bool Event_get_command_m6_982 (Event_t6_154 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = Event_get_modifiers_m6_1014(__this, /*hidden argument*/NULL);
		return ((((int32_t)((((int32_t)((int32_t)((int32_t)L_0&(int32_t)8))) == ((int32_t)0))? 1 : 0)) == ((int32_t)0))? 1 : 0);
	}
}
// System.Void UnityEngine.Event::set_command(System.Boolean)
extern "C" void Event_set_command_m6_983 (Event_t6_154 * __this, bool ___value, const MethodInfo* method)
{
	{
		bool L_0 = ___value;
		if (L_0)
		{
			goto IL_001a;
		}
	}
	{
		int32_t L_1 = Event_get_modifiers_m6_1014(__this, /*hidden argument*/NULL);
		Event_set_modifiers_m6_1015(__this, ((int32_t)((int32_t)L_1&(int32_t)((int32_t)-9))), /*hidden argument*/NULL);
		goto IL_0028;
	}

IL_001a:
	{
		int32_t L_2 = Event_get_modifiers_m6_1014(__this, /*hidden argument*/NULL);
		Event_set_modifiers_m6_1015(__this, ((int32_t)((int32_t)L_2|(int32_t)8)), /*hidden argument*/NULL);
	}

IL_0028:
	{
		return;
	}
}
// System.Boolean UnityEngine.Event::get_capsLock()
extern "C" bool Event_get_capsLock_m6_984 (Event_t6_154 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = Event_get_modifiers_m6_1014(__this, /*hidden argument*/NULL);
		return ((((int32_t)((((int32_t)((int32_t)((int32_t)L_0&(int32_t)((int32_t)32)))) == ((int32_t)0))? 1 : 0)) == ((int32_t)0))? 1 : 0);
	}
}
// System.Void UnityEngine.Event::set_capsLock(System.Boolean)
extern "C" void Event_set_capsLock_m6_985 (Event_t6_154 * __this, bool ___value, const MethodInfo* method)
{
	{
		bool L_0 = ___value;
		if (L_0)
		{
			goto IL_001a;
		}
	}
	{
		int32_t L_1 = Event_get_modifiers_m6_1014(__this, /*hidden argument*/NULL);
		Event_set_modifiers_m6_1015(__this, ((int32_t)((int32_t)L_1&(int32_t)((int32_t)-33))), /*hidden argument*/NULL);
		goto IL_0029;
	}

IL_001a:
	{
		int32_t L_2 = Event_get_modifiers_m6_1014(__this, /*hidden argument*/NULL);
		Event_set_modifiers_m6_1015(__this, ((int32_t)((int32_t)L_2|(int32_t)((int32_t)32))), /*hidden argument*/NULL);
	}

IL_0029:
	{
		return;
	}
}
// System.Boolean UnityEngine.Event::get_numeric()
extern "C" bool Event_get_numeric_m6_986 (Event_t6_154 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = Event_get_modifiers_m6_1014(__this, /*hidden argument*/NULL);
		return ((((int32_t)((((int32_t)((int32_t)((int32_t)L_0&(int32_t)((int32_t)16)))) == ((int32_t)0))? 1 : 0)) == ((int32_t)0))? 1 : 0);
	}
}
// System.Void UnityEngine.Event::set_numeric(System.Boolean)
extern "C" void Event_set_numeric_m6_987 (Event_t6_154 * __this, bool ___value, const MethodInfo* method)
{
	{
		bool L_0 = ___value;
		if (L_0)
		{
			goto IL_001a;
		}
	}
	{
		int32_t L_1 = Event_get_modifiers_m6_1014(__this, /*hidden argument*/NULL);
		Event_set_modifiers_m6_1015(__this, ((int32_t)((int32_t)L_1&(int32_t)((int32_t)-2))), /*hidden argument*/NULL);
		goto IL_0028;
	}

IL_001a:
	{
		int32_t L_2 = Event_get_modifiers_m6_1014(__this, /*hidden argument*/NULL);
		Event_set_modifiers_m6_1015(__this, ((int32_t)((int32_t)L_2|(int32_t)1)), /*hidden argument*/NULL);
	}

IL_0028:
	{
		return;
	}
}
// System.Boolean UnityEngine.Event::get_functionKey()
extern "C" bool Event_get_functionKey_m6_988 (Event_t6_154 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = Event_get_modifiers_m6_1014(__this, /*hidden argument*/NULL);
		return ((((int32_t)((((int32_t)((int32_t)((int32_t)L_0&(int32_t)((int32_t)64)))) == ((int32_t)0))? 1 : 0)) == ((int32_t)0))? 1 : 0);
	}
}
// UnityEngine.Event UnityEngine.Event::get_current()
extern TypeInfo* Event_t6_154_il2cpp_TypeInfo_var;
extern "C" Event_t6_154 * Event_get_current_m6_989 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Event_t6_154_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(978);
		s_Il2CppMethodIntialized = true;
	}
	{
		Event_t6_154 * L_0 = ((Event_t6_154_StaticFields*)Event_t6_154_il2cpp_TypeInfo_var->static_fields)->___s_Current_1;
		return L_0;
	}
}
// System.Void UnityEngine.Event::set_current(UnityEngine.Event)
extern TypeInfo* Event_t6_154_il2cpp_TypeInfo_var;
extern "C" void Event_set_current_m6_990 (Object_t * __this /* static, unused */, Event_t6_154 * ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Event_t6_154_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(978);
		s_Il2CppMethodIntialized = true;
	}
	{
		Event_t6_154 * L_0 = ___value;
		if (!L_0)
		{
			goto IL_0011;
		}
	}
	{
		Event_t6_154 * L_1 = ___value;
		((Event_t6_154_StaticFields*)Event_t6_154_il2cpp_TypeInfo_var->static_fields)->___s_Current_1 = L_1;
		goto IL_001b;
	}

IL_0011:
	{
		Event_t6_154 * L_2 = ((Event_t6_154_StaticFields*)Event_t6_154_il2cpp_TypeInfo_var->static_fields)->___s_MasterEvent_2;
		((Event_t6_154_StaticFields*)Event_t6_154_il2cpp_TypeInfo_var->static_fields)->___s_Current_1 = L_2;
	}

IL_001b:
	{
		Event_t6_154 * L_3 = ((Event_t6_154_StaticFields*)Event_t6_154_il2cpp_TypeInfo_var->static_fields)->___s_Current_1;
		NullCheck(L_3);
		IntPtr_t L_4 = (L_3->___m_Ptr_0);
		Event_Internal_SetNativeEvent_m6_1026(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Event::Internal_MakeMasterEventCurrent()
extern TypeInfo* Event_t6_154_il2cpp_TypeInfo_var;
extern "C" void Event_Internal_MakeMasterEventCurrent_m6_991 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Event_t6_154_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(978);
		s_Il2CppMethodIntialized = true;
	}
	{
		Event_t6_154 * L_0 = ((Event_t6_154_StaticFields*)Event_t6_154_il2cpp_TypeInfo_var->static_fields)->___s_MasterEvent_2;
		if (L_0)
		{
			goto IL_0014;
		}
	}
	{
		Event_t6_154 * L_1 = (Event_t6_154 *)il2cpp_codegen_object_new (Event_t6_154_il2cpp_TypeInfo_var);
		Event__ctor_m6_966(L_1, /*hidden argument*/NULL);
		((Event_t6_154_StaticFields*)Event_t6_154_il2cpp_TypeInfo_var->static_fields)->___s_MasterEvent_2 = L_1;
	}

IL_0014:
	{
		Event_t6_154 * L_2 = ((Event_t6_154_StaticFields*)Event_t6_154_il2cpp_TypeInfo_var->static_fields)->___s_MasterEvent_2;
		((Event_t6_154_StaticFields*)Event_t6_154_il2cpp_TypeInfo_var->static_fields)->___s_Current_1 = L_2;
		Event_t6_154 * L_3 = ((Event_t6_154_StaticFields*)Event_t6_154_il2cpp_TypeInfo_var->static_fields)->___s_MasterEvent_2;
		NullCheck(L_3);
		IntPtr_t L_4 = (L_3->___m_Ptr_0);
		Event_Internal_SetNativeEvent_m6_1026(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean UnityEngine.Event::get_isKey()
extern "C" bool Event_get_isKey_m6_992 (Event_t6_154 * __this, const MethodInfo* method)
{
	int32_t V_0 = {0};
	int32_t G_B3_0 = 0;
	{
		int32_t L_0 = Event_get_type_m6_1003(__this, /*hidden argument*/NULL);
		V_0 = L_0;
		int32_t L_1 = V_0;
		if ((((int32_t)L_1) == ((int32_t)4)))
		{
			goto IL_0014;
		}
	}
	{
		int32_t L_2 = V_0;
		G_B3_0 = ((((int32_t)L_2) == ((int32_t)5))? 1 : 0);
		goto IL_0015;
	}

IL_0014:
	{
		G_B3_0 = 1;
	}

IL_0015:
	{
		return G_B3_0;
	}
}
// System.Boolean UnityEngine.Event::get_isMouse()
extern "C" bool Event_get_isMouse_m6_993 (Event_t6_154 * __this, const MethodInfo* method)
{
	int32_t V_0 = {0};
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = Event_get_type_m6_1003(__this, /*hidden argument*/NULL);
		V_0 = L_0;
		int32_t L_1 = V_0;
		if ((((int32_t)L_1) == ((int32_t)2)))
		{
			goto IL_0021;
		}
	}
	{
		int32_t L_2 = V_0;
		if (!L_2)
		{
			goto IL_0021;
		}
	}
	{
		int32_t L_3 = V_0;
		if ((((int32_t)L_3) == ((int32_t)1)))
		{
			goto IL_0021;
		}
	}
	{
		int32_t L_4 = V_0;
		G_B5_0 = ((((int32_t)L_4) == ((int32_t)3))? 1 : 0);
		goto IL_0022;
	}

IL_0021:
	{
		G_B5_0 = 1;
	}

IL_0022:
	{
		return G_B5_0;
	}
}
// UnityEngine.Event UnityEngine.Event::KeyboardEvent(System.String)
extern const Il2CppType* KeyCode_t6_155_0_0_0_var;
extern TypeInfo* Event_t6_154_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* Dictionary_2_t1_77_il2cpp_TypeInfo_var;
extern TypeInfo* Type_t_il2cpp_TypeInfo_var;
extern TypeInfo* Enum_t1_24_il2cpp_TypeInfo_var;
extern TypeInfo* Int32_t1_3_il2cpp_TypeInfo_var;
extern TypeInfo* ArgumentException_t1_646_il2cpp_TypeInfo_var;
extern TypeInfo* ObjectU5BU5D_t1_157_il2cpp_TypeInfo_var;
extern const MethodInfo* Dictionary_2__ctor_m1_5521_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral2716;
extern Il2CppCodeGenString* _stringLiteral2717;
extern Il2CppCodeGenString* _stringLiteral2718;
extern Il2CppCodeGenString* _stringLiteral2719;
extern Il2CppCodeGenString* _stringLiteral2720;
extern Il2CppCodeGenString* _stringLiteral2721;
extern Il2CppCodeGenString* _stringLiteral2722;
extern Il2CppCodeGenString* _stringLiteral2723;
extern Il2CppCodeGenString* _stringLiteral2724;
extern Il2CppCodeGenString* _stringLiteral2725;
extern Il2CppCodeGenString* _stringLiteral2726;
extern Il2CppCodeGenString* _stringLiteral2727;
extern Il2CppCodeGenString* _stringLiteral2728;
extern Il2CppCodeGenString* _stringLiteral2729;
extern Il2CppCodeGenString* _stringLiteral2730;
extern Il2CppCodeGenString* _stringLiteral2731;
extern Il2CppCodeGenString* _stringLiteral2732;
extern Il2CppCodeGenString* _stringLiteral2733;
extern Il2CppCodeGenString* _stringLiteral2734;
extern Il2CppCodeGenString* _stringLiteral2735;
extern Il2CppCodeGenString* _stringLiteral2736;
extern Il2CppCodeGenString* _stringLiteral2737;
extern Il2CppCodeGenString* _stringLiteral2738;
extern Il2CppCodeGenString* _stringLiteral2739;
extern Il2CppCodeGenString* _stringLiteral2740;
extern Il2CppCodeGenString* _stringLiteral2741;
extern Il2CppCodeGenString* _stringLiteral2742;
extern Il2CppCodeGenString* _stringLiteral2743;
extern Il2CppCodeGenString* _stringLiteral2744;
extern Il2CppCodeGenString* _stringLiteral2745;
extern Il2CppCodeGenString* _stringLiteral2746;
extern Il2CppCodeGenString* _stringLiteral2747;
extern Il2CppCodeGenString* _stringLiteral2748;
extern Il2CppCodeGenString* _stringLiteral2749;
extern Il2CppCodeGenString* _stringLiteral2750;
extern Il2CppCodeGenString* _stringLiteral2751;
extern Il2CppCodeGenString* _stringLiteral2752;
extern Il2CppCodeGenString* _stringLiteral2753;
extern Il2CppCodeGenString* _stringLiteral2754;
extern Il2CppCodeGenString* _stringLiteral2755;
extern Il2CppCodeGenString* _stringLiteral2756;
extern Il2CppCodeGenString* _stringLiteral2757;
extern Il2CppCodeGenString* _stringLiteral2758;
extern Il2CppCodeGenString* _stringLiteral2759;
extern Il2CppCodeGenString* _stringLiteral2760;
extern Il2CppCodeGenString* _stringLiteral2761;
extern Il2CppCodeGenString* _stringLiteral2762;
extern Il2CppCodeGenString* _stringLiteral1282;
extern Il2CppCodeGenString* _stringLiteral2763;
extern Il2CppCodeGenString* _stringLiteral2764;
extern "C" Event_t6_154 * Event_KeyboardEvent_m6_994 (Object_t * __this /* static, unused */, String_t* ___key, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		KeyCode_t6_155_0_0_0_var = il2cpp_codegen_type_from_index(979);
		Event_t6_154_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(978);
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(11);
		Dictionary_2_t1_77_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(116);
		Type_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3);
		Enum_t1_24_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(61);
		Int32_t1_3_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(8);
		ArgumentException_t1_646_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4);
		ObjectU5BU5D_t1_157_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(19);
		Dictionary_2__ctor_m1_5521_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483658);
		_stringLiteral2716 = il2cpp_codegen_string_literal_from_index(2716);
		_stringLiteral2717 = il2cpp_codegen_string_literal_from_index(2717);
		_stringLiteral2718 = il2cpp_codegen_string_literal_from_index(2718);
		_stringLiteral2719 = il2cpp_codegen_string_literal_from_index(2719);
		_stringLiteral2720 = il2cpp_codegen_string_literal_from_index(2720);
		_stringLiteral2721 = il2cpp_codegen_string_literal_from_index(2721);
		_stringLiteral2722 = il2cpp_codegen_string_literal_from_index(2722);
		_stringLiteral2723 = il2cpp_codegen_string_literal_from_index(2723);
		_stringLiteral2724 = il2cpp_codegen_string_literal_from_index(2724);
		_stringLiteral2725 = il2cpp_codegen_string_literal_from_index(2725);
		_stringLiteral2726 = il2cpp_codegen_string_literal_from_index(2726);
		_stringLiteral2727 = il2cpp_codegen_string_literal_from_index(2727);
		_stringLiteral2728 = il2cpp_codegen_string_literal_from_index(2728);
		_stringLiteral2729 = il2cpp_codegen_string_literal_from_index(2729);
		_stringLiteral2730 = il2cpp_codegen_string_literal_from_index(2730);
		_stringLiteral2731 = il2cpp_codegen_string_literal_from_index(2731);
		_stringLiteral2732 = il2cpp_codegen_string_literal_from_index(2732);
		_stringLiteral2733 = il2cpp_codegen_string_literal_from_index(2733);
		_stringLiteral2734 = il2cpp_codegen_string_literal_from_index(2734);
		_stringLiteral2735 = il2cpp_codegen_string_literal_from_index(2735);
		_stringLiteral2736 = il2cpp_codegen_string_literal_from_index(2736);
		_stringLiteral2737 = il2cpp_codegen_string_literal_from_index(2737);
		_stringLiteral2738 = il2cpp_codegen_string_literal_from_index(2738);
		_stringLiteral2739 = il2cpp_codegen_string_literal_from_index(2739);
		_stringLiteral2740 = il2cpp_codegen_string_literal_from_index(2740);
		_stringLiteral2741 = il2cpp_codegen_string_literal_from_index(2741);
		_stringLiteral2742 = il2cpp_codegen_string_literal_from_index(2742);
		_stringLiteral2743 = il2cpp_codegen_string_literal_from_index(2743);
		_stringLiteral2744 = il2cpp_codegen_string_literal_from_index(2744);
		_stringLiteral2745 = il2cpp_codegen_string_literal_from_index(2745);
		_stringLiteral2746 = il2cpp_codegen_string_literal_from_index(2746);
		_stringLiteral2747 = il2cpp_codegen_string_literal_from_index(2747);
		_stringLiteral2748 = il2cpp_codegen_string_literal_from_index(2748);
		_stringLiteral2749 = il2cpp_codegen_string_literal_from_index(2749);
		_stringLiteral2750 = il2cpp_codegen_string_literal_from_index(2750);
		_stringLiteral2751 = il2cpp_codegen_string_literal_from_index(2751);
		_stringLiteral2752 = il2cpp_codegen_string_literal_from_index(2752);
		_stringLiteral2753 = il2cpp_codegen_string_literal_from_index(2753);
		_stringLiteral2754 = il2cpp_codegen_string_literal_from_index(2754);
		_stringLiteral2755 = il2cpp_codegen_string_literal_from_index(2755);
		_stringLiteral2756 = il2cpp_codegen_string_literal_from_index(2756);
		_stringLiteral2757 = il2cpp_codegen_string_literal_from_index(2757);
		_stringLiteral2758 = il2cpp_codegen_string_literal_from_index(2758);
		_stringLiteral2759 = il2cpp_codegen_string_literal_from_index(2759);
		_stringLiteral2760 = il2cpp_codegen_string_literal_from_index(2760);
		_stringLiteral2761 = il2cpp_codegen_string_literal_from_index(2761);
		_stringLiteral2762 = il2cpp_codegen_string_literal_from_index(2762);
		_stringLiteral1282 = il2cpp_codegen_string_literal_from_index(1282);
		_stringLiteral2763 = il2cpp_codegen_string_literal_from_index(2763);
		_stringLiteral2764 = il2cpp_codegen_string_literal_from_index(2764);
		s_Il2CppMethodIntialized = true;
	}
	Event_t6_154 * V_0 = {0};
	int32_t V_1 = 0;
	bool V_2 = false;
	String_t* V_3 = {0};
	uint16_t V_4 = 0x0;
	String_t* V_5 = {0};
	Dictionary_2_t1_77 * V_6 = {0};
	int32_t V_7 = 0;
	Exception_t1_33 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1_33 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		Event_t6_154 * L_0 = (Event_t6_154 *)il2cpp_codegen_object_new (Event_t6_154_il2cpp_TypeInfo_var);
		Event__ctor_m6_966(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		Event_t6_154 * L_1 = V_0;
		NullCheck(L_1);
		Event_set_type_m6_1004(L_1, 4, /*hidden argument*/NULL);
		String_t* L_2 = ___key;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_3 = String_IsNullOrEmpty_m1_397(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_001a;
		}
	}
	{
		Event_t6_154 * L_4 = V_0;
		return L_4;
	}

IL_001a:
	{
		V_1 = 0;
		V_2 = 0;
	}

IL_001e:
	{
		V_2 = 1;
		int32_t L_5 = V_1;
		String_t* L_6 = ___key;
		NullCheck(L_6);
		int32_t L_7 = String_get_Length_m1_428(L_6, /*hidden argument*/NULL);
		if ((((int32_t)L_5) < ((int32_t)L_7)))
		{
			goto IL_0033;
		}
	}
	{
		V_2 = 0;
		goto IL_00cd;
	}

IL_0033:
	{
		String_t* L_8 = ___key;
		int32_t L_9 = V_1;
		NullCheck(L_8);
		uint16_t L_10 = String_get_Chars_m1_341(L_8, L_9, /*hidden argument*/NULL);
		V_4 = L_10;
		uint16_t L_11 = V_4;
		if (((int32_t)((int32_t)L_11-(int32_t)((int32_t)35))) == 0)
		{
			goto IL_00a9;
		}
		if (((int32_t)((int32_t)L_11-(int32_t)((int32_t)35))) == 1)
		{
			goto IL_0056;
		}
		if (((int32_t)((int32_t)L_11-(int32_t)((int32_t)35))) == 2)
		{
			goto IL_0092;
		}
		if (((int32_t)((int32_t)L_11-(int32_t)((int32_t)35))) == 3)
		{
			goto IL_0064;
		}
	}

IL_0056:
	{
		uint16_t L_12 = V_4;
		if ((((int32_t)L_12) == ((int32_t)((int32_t)94))))
		{
			goto IL_007b;
		}
	}
	{
		goto IL_00c0;
	}

IL_0064:
	{
		Event_t6_154 * L_13 = V_0;
		Event_t6_154 * L_14 = L_13;
		NullCheck(L_14);
		int32_t L_15 = Event_get_modifiers_m6_1014(L_14, /*hidden argument*/NULL);
		NullCheck(L_14);
		Event_set_modifiers_m6_1015(L_14, ((int32_t)((int32_t)L_15|(int32_t)4)), /*hidden argument*/NULL);
		int32_t L_16 = V_1;
		V_1 = ((int32_t)((int32_t)L_16+(int32_t)1));
		goto IL_00c7;
	}

IL_007b:
	{
		Event_t6_154 * L_17 = V_0;
		Event_t6_154 * L_18 = L_17;
		NullCheck(L_18);
		int32_t L_19 = Event_get_modifiers_m6_1014(L_18, /*hidden argument*/NULL);
		NullCheck(L_18);
		Event_set_modifiers_m6_1015(L_18, ((int32_t)((int32_t)L_19|(int32_t)2)), /*hidden argument*/NULL);
		int32_t L_20 = V_1;
		V_1 = ((int32_t)((int32_t)L_20+(int32_t)1));
		goto IL_00c7;
	}

IL_0092:
	{
		Event_t6_154 * L_21 = V_0;
		Event_t6_154 * L_22 = L_21;
		NullCheck(L_22);
		int32_t L_23 = Event_get_modifiers_m6_1014(L_22, /*hidden argument*/NULL);
		NullCheck(L_22);
		Event_set_modifiers_m6_1015(L_22, ((int32_t)((int32_t)L_23|(int32_t)8)), /*hidden argument*/NULL);
		int32_t L_24 = V_1;
		V_1 = ((int32_t)((int32_t)L_24+(int32_t)1));
		goto IL_00c7;
	}

IL_00a9:
	{
		Event_t6_154 * L_25 = V_0;
		Event_t6_154 * L_26 = L_25;
		NullCheck(L_26);
		int32_t L_27 = Event_get_modifiers_m6_1014(L_26, /*hidden argument*/NULL);
		NullCheck(L_26);
		Event_set_modifiers_m6_1015(L_26, ((int32_t)((int32_t)L_27|(int32_t)1)), /*hidden argument*/NULL);
		int32_t L_28 = V_1;
		V_1 = ((int32_t)((int32_t)L_28+(int32_t)1));
		goto IL_00c7;
	}

IL_00c0:
	{
		V_2 = 0;
		goto IL_00c7;
	}

IL_00c7:
	{
		bool L_29 = V_2;
		if (L_29)
		{
			goto IL_001e;
		}
	}

IL_00cd:
	{
		String_t* L_30 = ___key;
		int32_t L_31 = V_1;
		String_t* L_32 = ___key;
		NullCheck(L_32);
		int32_t L_33 = String_get_Length_m1_428(L_32, /*hidden argument*/NULL);
		int32_t L_34 = V_1;
		NullCheck(L_30);
		String_t* L_35 = String_Substring_m1_352(L_30, L_31, ((int32_t)((int32_t)L_33-(int32_t)L_34)), /*hidden argument*/NULL);
		NullCheck(L_35);
		String_t* L_36 = String_ToLower_m1_405(L_35, /*hidden argument*/NULL);
		V_3 = L_36;
		String_t* L_37 = V_3;
		V_5 = L_37;
		String_t* L_38 = V_5;
		if (!L_38)
		{
			goto IL_09e5;
		}
	}
	{
		Dictionary_2_t1_77 * L_39 = ((Event_t6_154_StaticFields*)Event_t6_154_il2cpp_TypeInfo_var->static_fields)->___U3CU3Ef__switchU24map0_3;
		if (L_39)
		{
			goto IL_03ab;
		}
	}
	{
		Dictionary_2_t1_77 * L_40 = (Dictionary_2_t1_77 *)il2cpp_codegen_object_new (Dictionary_2_t1_77_il2cpp_TypeInfo_var);
		Dictionary_2__ctor_m1_5521(L_40, ((int32_t)49), /*hidden argument*/Dictionary_2__ctor_m1_5521_MethodInfo_var);
		V_6 = L_40;
		Dictionary_2_t1_77 * L_41 = V_6;
		NullCheck(L_41);
		VirtActionInvoker2< String_t*, int32_t >::Invoke(27 /* System.Void System.Collections.Generic.Dictionary`2<System.String,System.Int32>::Add(!0,!1) */, L_41, _stringLiteral2716, 0);
		Dictionary_2_t1_77 * L_42 = V_6;
		NullCheck(L_42);
		VirtActionInvoker2< String_t*, int32_t >::Invoke(27 /* System.Void System.Collections.Generic.Dictionary`2<System.String,System.Int32>::Add(!0,!1) */, L_42, _stringLiteral2717, 1);
		Dictionary_2_t1_77 * L_43 = V_6;
		NullCheck(L_43);
		VirtActionInvoker2< String_t*, int32_t >::Invoke(27 /* System.Void System.Collections.Generic.Dictionary`2<System.String,System.Int32>::Add(!0,!1) */, L_43, _stringLiteral2718, 2);
		Dictionary_2_t1_77 * L_44 = V_6;
		NullCheck(L_44);
		VirtActionInvoker2< String_t*, int32_t >::Invoke(27 /* System.Void System.Collections.Generic.Dictionary`2<System.String,System.Int32>::Add(!0,!1) */, L_44, _stringLiteral2719, 3);
		Dictionary_2_t1_77 * L_45 = V_6;
		NullCheck(L_45);
		VirtActionInvoker2< String_t*, int32_t >::Invoke(27 /* System.Void System.Collections.Generic.Dictionary`2<System.String,System.Int32>::Add(!0,!1) */, L_45, _stringLiteral2720, 4);
		Dictionary_2_t1_77 * L_46 = V_6;
		NullCheck(L_46);
		VirtActionInvoker2< String_t*, int32_t >::Invoke(27 /* System.Void System.Collections.Generic.Dictionary`2<System.String,System.Int32>::Add(!0,!1) */, L_46, _stringLiteral2721, 5);
		Dictionary_2_t1_77 * L_47 = V_6;
		NullCheck(L_47);
		VirtActionInvoker2< String_t*, int32_t >::Invoke(27 /* System.Void System.Collections.Generic.Dictionary`2<System.String,System.Int32>::Add(!0,!1) */, L_47, _stringLiteral2722, 6);
		Dictionary_2_t1_77 * L_48 = V_6;
		NullCheck(L_48);
		VirtActionInvoker2< String_t*, int32_t >::Invoke(27 /* System.Void System.Collections.Generic.Dictionary`2<System.String,System.Int32>::Add(!0,!1) */, L_48, _stringLiteral2723, 7);
		Dictionary_2_t1_77 * L_49 = V_6;
		NullCheck(L_49);
		VirtActionInvoker2< String_t*, int32_t >::Invoke(27 /* System.Void System.Collections.Generic.Dictionary`2<System.String,System.Int32>::Add(!0,!1) */, L_49, _stringLiteral2724, 8);
		Dictionary_2_t1_77 * L_50 = V_6;
		NullCheck(L_50);
		VirtActionInvoker2< String_t*, int32_t >::Invoke(27 /* System.Void System.Collections.Generic.Dictionary`2<System.String,System.Int32>::Add(!0,!1) */, L_50, _stringLiteral2725, ((int32_t)9));
		Dictionary_2_t1_77 * L_51 = V_6;
		NullCheck(L_51);
		VirtActionInvoker2< String_t*, int32_t >::Invoke(27 /* System.Void System.Collections.Generic.Dictionary`2<System.String,System.Int32>::Add(!0,!1) */, L_51, _stringLiteral2726, ((int32_t)10));
		Dictionary_2_t1_77 * L_52 = V_6;
		NullCheck(L_52);
		VirtActionInvoker2< String_t*, int32_t >::Invoke(27 /* System.Void System.Collections.Generic.Dictionary`2<System.String,System.Int32>::Add(!0,!1) */, L_52, _stringLiteral2727, ((int32_t)11));
		Dictionary_2_t1_77 * L_53 = V_6;
		NullCheck(L_53);
		VirtActionInvoker2< String_t*, int32_t >::Invoke(27 /* System.Void System.Collections.Generic.Dictionary`2<System.String,System.Int32>::Add(!0,!1) */, L_53, _stringLiteral2728, ((int32_t)12));
		Dictionary_2_t1_77 * L_54 = V_6;
		NullCheck(L_54);
		VirtActionInvoker2< String_t*, int32_t >::Invoke(27 /* System.Void System.Collections.Generic.Dictionary`2<System.String,System.Int32>::Add(!0,!1) */, L_54, _stringLiteral2729, ((int32_t)13));
		Dictionary_2_t1_77 * L_55 = V_6;
		NullCheck(L_55);
		VirtActionInvoker2< String_t*, int32_t >::Invoke(27 /* System.Void System.Collections.Generic.Dictionary`2<System.String,System.Int32>::Add(!0,!1) */, L_55, _stringLiteral2730, ((int32_t)14));
		Dictionary_2_t1_77 * L_56 = V_6;
		NullCheck(L_56);
		VirtActionInvoker2< String_t*, int32_t >::Invoke(27 /* System.Void System.Collections.Generic.Dictionary`2<System.String,System.Int32>::Add(!0,!1) */, L_56, _stringLiteral2731, ((int32_t)15));
		Dictionary_2_t1_77 * L_57 = V_6;
		NullCheck(L_57);
		VirtActionInvoker2< String_t*, int32_t >::Invoke(27 /* System.Void System.Collections.Generic.Dictionary`2<System.String,System.Int32>::Add(!0,!1) */, L_57, _stringLiteral2732, ((int32_t)16));
		Dictionary_2_t1_77 * L_58 = V_6;
		NullCheck(L_58);
		VirtActionInvoker2< String_t*, int32_t >::Invoke(27 /* System.Void System.Collections.Generic.Dictionary`2<System.String,System.Int32>::Add(!0,!1) */, L_58, _stringLiteral2733, ((int32_t)17));
		Dictionary_2_t1_77 * L_59 = V_6;
		NullCheck(L_59);
		VirtActionInvoker2< String_t*, int32_t >::Invoke(27 /* System.Void System.Collections.Generic.Dictionary`2<System.String,System.Int32>::Add(!0,!1) */, L_59, _stringLiteral2734, ((int32_t)18));
		Dictionary_2_t1_77 * L_60 = V_6;
		NullCheck(L_60);
		VirtActionInvoker2< String_t*, int32_t >::Invoke(27 /* System.Void System.Collections.Generic.Dictionary`2<System.String,System.Int32>::Add(!0,!1) */, L_60, _stringLiteral2735, ((int32_t)19));
		Dictionary_2_t1_77 * L_61 = V_6;
		NullCheck(L_61);
		VirtActionInvoker2< String_t*, int32_t >::Invoke(27 /* System.Void System.Collections.Generic.Dictionary`2<System.String,System.Int32>::Add(!0,!1) */, L_61, _stringLiteral2736, ((int32_t)20));
		Dictionary_2_t1_77 * L_62 = V_6;
		NullCheck(L_62);
		VirtActionInvoker2< String_t*, int32_t >::Invoke(27 /* System.Void System.Collections.Generic.Dictionary`2<System.String,System.Int32>::Add(!0,!1) */, L_62, _stringLiteral2737, ((int32_t)21));
		Dictionary_2_t1_77 * L_63 = V_6;
		NullCheck(L_63);
		VirtActionInvoker2< String_t*, int32_t >::Invoke(27 /* System.Void System.Collections.Generic.Dictionary`2<System.String,System.Int32>::Add(!0,!1) */, L_63, _stringLiteral2738, ((int32_t)22));
		Dictionary_2_t1_77 * L_64 = V_6;
		NullCheck(L_64);
		VirtActionInvoker2< String_t*, int32_t >::Invoke(27 /* System.Void System.Collections.Generic.Dictionary`2<System.String,System.Int32>::Add(!0,!1) */, L_64, _stringLiteral2739, ((int32_t)23));
		Dictionary_2_t1_77 * L_65 = V_6;
		NullCheck(L_65);
		VirtActionInvoker2< String_t*, int32_t >::Invoke(27 /* System.Void System.Collections.Generic.Dictionary`2<System.String,System.Int32>::Add(!0,!1) */, L_65, _stringLiteral2740, ((int32_t)24));
		Dictionary_2_t1_77 * L_66 = V_6;
		NullCheck(L_66);
		VirtActionInvoker2< String_t*, int32_t >::Invoke(27 /* System.Void System.Collections.Generic.Dictionary`2<System.String,System.Int32>::Add(!0,!1) */, L_66, _stringLiteral2741, ((int32_t)25));
		Dictionary_2_t1_77 * L_67 = V_6;
		NullCheck(L_67);
		VirtActionInvoker2< String_t*, int32_t >::Invoke(27 /* System.Void System.Collections.Generic.Dictionary`2<System.String,System.Int32>::Add(!0,!1) */, L_67, _stringLiteral2742, ((int32_t)26));
		Dictionary_2_t1_77 * L_68 = V_6;
		NullCheck(L_68);
		VirtActionInvoker2< String_t*, int32_t >::Invoke(27 /* System.Void System.Collections.Generic.Dictionary`2<System.String,System.Int32>::Add(!0,!1) */, L_68, _stringLiteral2743, ((int32_t)27));
		Dictionary_2_t1_77 * L_69 = V_6;
		NullCheck(L_69);
		VirtActionInvoker2< String_t*, int32_t >::Invoke(27 /* System.Void System.Collections.Generic.Dictionary`2<System.String,System.Int32>::Add(!0,!1) */, L_69, _stringLiteral2744, ((int32_t)28));
		Dictionary_2_t1_77 * L_70 = V_6;
		NullCheck(L_70);
		VirtActionInvoker2< String_t*, int32_t >::Invoke(27 /* System.Void System.Collections.Generic.Dictionary`2<System.String,System.Int32>::Add(!0,!1) */, L_70, _stringLiteral2745, ((int32_t)29));
		Dictionary_2_t1_77 * L_71 = V_6;
		NullCheck(L_71);
		VirtActionInvoker2< String_t*, int32_t >::Invoke(27 /* System.Void System.Collections.Generic.Dictionary`2<System.String,System.Int32>::Add(!0,!1) */, L_71, _stringLiteral2746, ((int32_t)30));
		Dictionary_2_t1_77 * L_72 = V_6;
		NullCheck(L_72);
		VirtActionInvoker2< String_t*, int32_t >::Invoke(27 /* System.Void System.Collections.Generic.Dictionary`2<System.String,System.Int32>::Add(!0,!1) */, L_72, _stringLiteral2747, ((int32_t)31));
		Dictionary_2_t1_77 * L_73 = V_6;
		NullCheck(L_73);
		VirtActionInvoker2< String_t*, int32_t >::Invoke(27 /* System.Void System.Collections.Generic.Dictionary`2<System.String,System.Int32>::Add(!0,!1) */, L_73, _stringLiteral2748, ((int32_t)32));
		Dictionary_2_t1_77 * L_74 = V_6;
		NullCheck(L_74);
		VirtActionInvoker2< String_t*, int32_t >::Invoke(27 /* System.Void System.Collections.Generic.Dictionary`2<System.String,System.Int32>::Add(!0,!1) */, L_74, _stringLiteral2749, ((int32_t)33));
		Dictionary_2_t1_77 * L_75 = V_6;
		NullCheck(L_75);
		VirtActionInvoker2< String_t*, int32_t >::Invoke(27 /* System.Void System.Collections.Generic.Dictionary`2<System.String,System.Int32>::Add(!0,!1) */, L_75, _stringLiteral2750, ((int32_t)34));
		Dictionary_2_t1_77 * L_76 = V_6;
		NullCheck(L_76);
		VirtActionInvoker2< String_t*, int32_t >::Invoke(27 /* System.Void System.Collections.Generic.Dictionary`2<System.String,System.Int32>::Add(!0,!1) */, L_76, _stringLiteral2751, ((int32_t)35));
		Dictionary_2_t1_77 * L_77 = V_6;
		NullCheck(L_77);
		VirtActionInvoker2< String_t*, int32_t >::Invoke(27 /* System.Void System.Collections.Generic.Dictionary`2<System.String,System.Int32>::Add(!0,!1) */, L_77, _stringLiteral2752, ((int32_t)36));
		Dictionary_2_t1_77 * L_78 = V_6;
		NullCheck(L_78);
		VirtActionInvoker2< String_t*, int32_t >::Invoke(27 /* System.Void System.Collections.Generic.Dictionary`2<System.String,System.Int32>::Add(!0,!1) */, L_78, _stringLiteral2753, ((int32_t)37));
		Dictionary_2_t1_77 * L_79 = V_6;
		NullCheck(L_79);
		VirtActionInvoker2< String_t*, int32_t >::Invoke(27 /* System.Void System.Collections.Generic.Dictionary`2<System.String,System.Int32>::Add(!0,!1) */, L_79, _stringLiteral2754, ((int32_t)38));
		Dictionary_2_t1_77 * L_80 = V_6;
		NullCheck(L_80);
		VirtActionInvoker2< String_t*, int32_t >::Invoke(27 /* System.Void System.Collections.Generic.Dictionary`2<System.String,System.Int32>::Add(!0,!1) */, L_80, _stringLiteral2755, ((int32_t)39));
		Dictionary_2_t1_77 * L_81 = V_6;
		NullCheck(L_81);
		VirtActionInvoker2< String_t*, int32_t >::Invoke(27 /* System.Void System.Collections.Generic.Dictionary`2<System.String,System.Int32>::Add(!0,!1) */, L_81, _stringLiteral2756, ((int32_t)40));
		Dictionary_2_t1_77 * L_82 = V_6;
		NullCheck(L_82);
		VirtActionInvoker2< String_t*, int32_t >::Invoke(27 /* System.Void System.Collections.Generic.Dictionary`2<System.String,System.Int32>::Add(!0,!1) */, L_82, _stringLiteral2757, ((int32_t)41));
		Dictionary_2_t1_77 * L_83 = V_6;
		NullCheck(L_83);
		VirtActionInvoker2< String_t*, int32_t >::Invoke(27 /* System.Void System.Collections.Generic.Dictionary`2<System.String,System.Int32>::Add(!0,!1) */, L_83, _stringLiteral2758, ((int32_t)42));
		Dictionary_2_t1_77 * L_84 = V_6;
		NullCheck(L_84);
		VirtActionInvoker2< String_t*, int32_t >::Invoke(27 /* System.Void System.Collections.Generic.Dictionary`2<System.String,System.Int32>::Add(!0,!1) */, L_84, _stringLiteral2759, ((int32_t)43));
		Dictionary_2_t1_77 * L_85 = V_6;
		NullCheck(L_85);
		VirtActionInvoker2< String_t*, int32_t >::Invoke(27 /* System.Void System.Collections.Generic.Dictionary`2<System.String,System.Int32>::Add(!0,!1) */, L_85, _stringLiteral2760, ((int32_t)44));
		Dictionary_2_t1_77 * L_86 = V_6;
		NullCheck(L_86);
		VirtActionInvoker2< String_t*, int32_t >::Invoke(27 /* System.Void System.Collections.Generic.Dictionary`2<System.String,System.Int32>::Add(!0,!1) */, L_86, _stringLiteral2761, ((int32_t)45));
		Dictionary_2_t1_77 * L_87 = V_6;
		NullCheck(L_87);
		VirtActionInvoker2< String_t*, int32_t >::Invoke(27 /* System.Void System.Collections.Generic.Dictionary`2<System.String,System.Int32>::Add(!0,!1) */, L_87, _stringLiteral2762, ((int32_t)46));
		Dictionary_2_t1_77 * L_88 = V_6;
		NullCheck(L_88);
		VirtActionInvoker2< String_t*, int32_t >::Invoke(27 /* System.Void System.Collections.Generic.Dictionary`2<System.String,System.Int32>::Add(!0,!1) */, L_88, _stringLiteral1282, ((int32_t)47));
		Dictionary_2_t1_77 * L_89 = V_6;
		NullCheck(L_89);
		VirtActionInvoker2< String_t*, int32_t >::Invoke(27 /* System.Void System.Collections.Generic.Dictionary`2<System.String,System.Int32>::Add(!0,!1) */, L_89, _stringLiteral2763, ((int32_t)48));
		Dictionary_2_t1_77 * L_90 = V_6;
		((Event_t6_154_StaticFields*)Event_t6_154_il2cpp_TypeInfo_var->static_fields)->___U3CU3Ef__switchU24map0_3 = L_90;
	}

IL_03ab:
	{
		Dictionary_2_t1_77 * L_91 = ((Event_t6_154_StaticFields*)Event_t6_154_il2cpp_TypeInfo_var->static_fields)->___U3CU3Ef__switchU24map0_3;
		String_t* L_92 = V_5;
		NullCheck(L_91);
		bool L_93 = (bool)VirtFuncInvoker2< bool, String_t*, int32_t* >::Invoke(32 /* System.Boolean System.Collections.Generic.Dictionary`2<System.String,System.Int32>::TryGetValue(!0,!1&) */, L_91, L_92, (&V_7));
		if (!L_93)
		{
			goto IL_09e5;
		}
	}
	{
		int32_t L_94 = V_7;
		if (L_94 == 0)
		{
			goto IL_048e;
		}
		if (L_94 == 1)
		{
			goto IL_04a6;
		}
		if (L_94 == 2)
		{
			goto IL_04be;
		}
		if (L_94 == 3)
		{
			goto IL_04d6;
		}
		if (L_94 == 4)
		{
			goto IL_04ee;
		}
		if (L_94 == 5)
		{
			goto IL_0506;
		}
		if (L_94 == 6)
		{
			goto IL_051e;
		}
		if (L_94 == 7)
		{
			goto IL_0536;
		}
		if (L_94 == 8)
		{
			goto IL_054e;
		}
		if (L_94 == 9)
		{
			goto IL_0566;
		}
		if (L_94 == 10)
		{
			goto IL_057e;
		}
		if (L_94 == 11)
		{
			goto IL_0596;
		}
		if (L_94 == 12)
		{
			goto IL_05ae;
		}
		if (L_94 == 13)
		{
			goto IL_05c6;
		}
		if (L_94 == 14)
		{
			goto IL_05de;
		}
		if (L_94 == 15)
		{
			goto IL_05f6;
		}
		if (L_94 == 16)
		{
			goto IL_060e;
		}
		if (L_94 == 17)
		{
			goto IL_0626;
		}
		if (L_94 == 18)
		{
			goto IL_0645;
		}
		if (L_94 == 19)
		{
			goto IL_0664;
		}
		if (L_94 == 20)
		{
			goto IL_0683;
		}
		if (L_94 == 21)
		{
			goto IL_06a2;
		}
		if (L_94 == 22)
		{
			goto IL_06c1;
		}
		if (L_94 == 23)
		{
			goto IL_06e0;
		}
		if (L_94 == 24)
		{
			goto IL_06ff;
		}
		if (L_94 == 25)
		{
			goto IL_071e;
		}
		if (L_94 == 26)
		{
			goto IL_073d;
		}
		if (L_94 == 27)
		{
			goto IL_075c;
		}
		if (L_94 == 28)
		{
			goto IL_077b;
		}
		if (L_94 == 29)
		{
			goto IL_0796;
		}
		if (L_94 == 30)
		{
			goto IL_07b2;
		}
		if (L_94 == 31)
		{
			goto IL_07bf;
		}
		if (L_94 == 32)
		{
			goto IL_07de;
		}
		if (L_94 == 33)
		{
			goto IL_07fd;
		}
		if (L_94 == 34)
		{
			goto IL_081c;
		}
		if (L_94 == 35)
		{
			goto IL_083b;
		}
		if (L_94 == 36)
		{
			goto IL_085a;
		}
		if (L_94 == 37)
		{
			goto IL_0879;
		}
		if (L_94 == 38)
		{
			goto IL_0898;
		}
		if (L_94 == 39)
		{
			goto IL_08b7;
		}
		if (L_94 == 40)
		{
			goto IL_08d6;
		}
		if (L_94 == 41)
		{
			goto IL_08f5;
		}
		if (L_94 == 42)
		{
			goto IL_0914;
		}
		if (L_94 == 43)
		{
			goto IL_0933;
		}
		if (L_94 == 44)
		{
			goto IL_0952;
		}
		if (L_94 == 45)
		{
			goto IL_0971;
		}
		if (L_94 == 46)
		{
			goto IL_0990;
		}
		if (L_94 == 47)
		{
			goto IL_099d;
		}
		if (L_94 == 48)
		{
			goto IL_09c1;
		}
	}
	{
		goto IL_09e5;
	}

IL_048e:
	{
		Event_t6_154 * L_95 = V_0;
		NullCheck(L_95);
		Event_set_character_m6_1021(L_95, ((int32_t)48), /*hidden argument*/NULL);
		Event_t6_154 * L_96 = V_0;
		NullCheck(L_96);
		Event_set_keyCode_m6_1025(L_96, ((int32_t)256), /*hidden argument*/NULL);
		goto IL_0a6b;
	}

IL_04a6:
	{
		Event_t6_154 * L_97 = V_0;
		NullCheck(L_97);
		Event_set_character_m6_1021(L_97, ((int32_t)49), /*hidden argument*/NULL);
		Event_t6_154 * L_98 = V_0;
		NullCheck(L_98);
		Event_set_keyCode_m6_1025(L_98, ((int32_t)257), /*hidden argument*/NULL);
		goto IL_0a6b;
	}

IL_04be:
	{
		Event_t6_154 * L_99 = V_0;
		NullCheck(L_99);
		Event_set_character_m6_1021(L_99, ((int32_t)50), /*hidden argument*/NULL);
		Event_t6_154 * L_100 = V_0;
		NullCheck(L_100);
		Event_set_keyCode_m6_1025(L_100, ((int32_t)258), /*hidden argument*/NULL);
		goto IL_0a6b;
	}

IL_04d6:
	{
		Event_t6_154 * L_101 = V_0;
		NullCheck(L_101);
		Event_set_character_m6_1021(L_101, ((int32_t)51), /*hidden argument*/NULL);
		Event_t6_154 * L_102 = V_0;
		NullCheck(L_102);
		Event_set_keyCode_m6_1025(L_102, ((int32_t)259), /*hidden argument*/NULL);
		goto IL_0a6b;
	}

IL_04ee:
	{
		Event_t6_154 * L_103 = V_0;
		NullCheck(L_103);
		Event_set_character_m6_1021(L_103, ((int32_t)52), /*hidden argument*/NULL);
		Event_t6_154 * L_104 = V_0;
		NullCheck(L_104);
		Event_set_keyCode_m6_1025(L_104, ((int32_t)260), /*hidden argument*/NULL);
		goto IL_0a6b;
	}

IL_0506:
	{
		Event_t6_154 * L_105 = V_0;
		NullCheck(L_105);
		Event_set_character_m6_1021(L_105, ((int32_t)53), /*hidden argument*/NULL);
		Event_t6_154 * L_106 = V_0;
		NullCheck(L_106);
		Event_set_keyCode_m6_1025(L_106, ((int32_t)261), /*hidden argument*/NULL);
		goto IL_0a6b;
	}

IL_051e:
	{
		Event_t6_154 * L_107 = V_0;
		NullCheck(L_107);
		Event_set_character_m6_1021(L_107, ((int32_t)54), /*hidden argument*/NULL);
		Event_t6_154 * L_108 = V_0;
		NullCheck(L_108);
		Event_set_keyCode_m6_1025(L_108, ((int32_t)262), /*hidden argument*/NULL);
		goto IL_0a6b;
	}

IL_0536:
	{
		Event_t6_154 * L_109 = V_0;
		NullCheck(L_109);
		Event_set_character_m6_1021(L_109, ((int32_t)55), /*hidden argument*/NULL);
		Event_t6_154 * L_110 = V_0;
		NullCheck(L_110);
		Event_set_keyCode_m6_1025(L_110, ((int32_t)263), /*hidden argument*/NULL);
		goto IL_0a6b;
	}

IL_054e:
	{
		Event_t6_154 * L_111 = V_0;
		NullCheck(L_111);
		Event_set_character_m6_1021(L_111, ((int32_t)56), /*hidden argument*/NULL);
		Event_t6_154 * L_112 = V_0;
		NullCheck(L_112);
		Event_set_keyCode_m6_1025(L_112, ((int32_t)264), /*hidden argument*/NULL);
		goto IL_0a6b;
	}

IL_0566:
	{
		Event_t6_154 * L_113 = V_0;
		NullCheck(L_113);
		Event_set_character_m6_1021(L_113, ((int32_t)57), /*hidden argument*/NULL);
		Event_t6_154 * L_114 = V_0;
		NullCheck(L_114);
		Event_set_keyCode_m6_1025(L_114, ((int32_t)265), /*hidden argument*/NULL);
		goto IL_0a6b;
	}

IL_057e:
	{
		Event_t6_154 * L_115 = V_0;
		NullCheck(L_115);
		Event_set_character_m6_1021(L_115, ((int32_t)46), /*hidden argument*/NULL);
		Event_t6_154 * L_116 = V_0;
		NullCheck(L_116);
		Event_set_keyCode_m6_1025(L_116, ((int32_t)266), /*hidden argument*/NULL);
		goto IL_0a6b;
	}

IL_0596:
	{
		Event_t6_154 * L_117 = V_0;
		NullCheck(L_117);
		Event_set_character_m6_1021(L_117, ((int32_t)47), /*hidden argument*/NULL);
		Event_t6_154 * L_118 = V_0;
		NullCheck(L_118);
		Event_set_keyCode_m6_1025(L_118, ((int32_t)267), /*hidden argument*/NULL);
		goto IL_0a6b;
	}

IL_05ae:
	{
		Event_t6_154 * L_119 = V_0;
		NullCheck(L_119);
		Event_set_character_m6_1021(L_119, ((int32_t)45), /*hidden argument*/NULL);
		Event_t6_154 * L_120 = V_0;
		NullCheck(L_120);
		Event_set_keyCode_m6_1025(L_120, ((int32_t)269), /*hidden argument*/NULL);
		goto IL_0a6b;
	}

IL_05c6:
	{
		Event_t6_154 * L_121 = V_0;
		NullCheck(L_121);
		Event_set_character_m6_1021(L_121, ((int32_t)43), /*hidden argument*/NULL);
		Event_t6_154 * L_122 = V_0;
		NullCheck(L_122);
		Event_set_keyCode_m6_1025(L_122, ((int32_t)270), /*hidden argument*/NULL);
		goto IL_0a6b;
	}

IL_05de:
	{
		Event_t6_154 * L_123 = V_0;
		NullCheck(L_123);
		Event_set_character_m6_1021(L_123, ((int32_t)61), /*hidden argument*/NULL);
		Event_t6_154 * L_124 = V_0;
		NullCheck(L_124);
		Event_set_keyCode_m6_1025(L_124, ((int32_t)272), /*hidden argument*/NULL);
		goto IL_0a6b;
	}

IL_05f6:
	{
		Event_t6_154 * L_125 = V_0;
		NullCheck(L_125);
		Event_set_character_m6_1021(L_125, ((int32_t)61), /*hidden argument*/NULL);
		Event_t6_154 * L_126 = V_0;
		NullCheck(L_126);
		Event_set_keyCode_m6_1025(L_126, ((int32_t)272), /*hidden argument*/NULL);
		goto IL_0a6b;
	}

IL_060e:
	{
		Event_t6_154 * L_127 = V_0;
		NullCheck(L_127);
		Event_set_character_m6_1021(L_127, ((int32_t)10), /*hidden argument*/NULL);
		Event_t6_154 * L_128 = V_0;
		NullCheck(L_128);
		Event_set_keyCode_m6_1025(L_128, ((int32_t)271), /*hidden argument*/NULL);
		goto IL_0a6b;
	}

IL_0626:
	{
		Event_t6_154 * L_129 = V_0;
		NullCheck(L_129);
		Event_set_keyCode_m6_1025(L_129, ((int32_t)273), /*hidden argument*/NULL);
		Event_t6_154 * L_130 = V_0;
		Event_t6_154 * L_131 = L_130;
		NullCheck(L_131);
		int32_t L_132 = Event_get_modifiers_m6_1014(L_131, /*hidden argument*/NULL);
		NullCheck(L_131);
		Event_set_modifiers_m6_1015(L_131, ((int32_t)((int32_t)L_132|(int32_t)((int32_t)64))), /*hidden argument*/NULL);
		goto IL_0a6b;
	}

IL_0645:
	{
		Event_t6_154 * L_133 = V_0;
		NullCheck(L_133);
		Event_set_keyCode_m6_1025(L_133, ((int32_t)274), /*hidden argument*/NULL);
		Event_t6_154 * L_134 = V_0;
		Event_t6_154 * L_135 = L_134;
		NullCheck(L_135);
		int32_t L_136 = Event_get_modifiers_m6_1014(L_135, /*hidden argument*/NULL);
		NullCheck(L_135);
		Event_set_modifiers_m6_1015(L_135, ((int32_t)((int32_t)L_136|(int32_t)((int32_t)64))), /*hidden argument*/NULL);
		goto IL_0a6b;
	}

IL_0664:
	{
		Event_t6_154 * L_137 = V_0;
		NullCheck(L_137);
		Event_set_keyCode_m6_1025(L_137, ((int32_t)276), /*hidden argument*/NULL);
		Event_t6_154 * L_138 = V_0;
		Event_t6_154 * L_139 = L_138;
		NullCheck(L_139);
		int32_t L_140 = Event_get_modifiers_m6_1014(L_139, /*hidden argument*/NULL);
		NullCheck(L_139);
		Event_set_modifiers_m6_1015(L_139, ((int32_t)((int32_t)L_140|(int32_t)((int32_t)64))), /*hidden argument*/NULL);
		goto IL_0a6b;
	}

IL_0683:
	{
		Event_t6_154 * L_141 = V_0;
		NullCheck(L_141);
		Event_set_keyCode_m6_1025(L_141, ((int32_t)275), /*hidden argument*/NULL);
		Event_t6_154 * L_142 = V_0;
		Event_t6_154 * L_143 = L_142;
		NullCheck(L_143);
		int32_t L_144 = Event_get_modifiers_m6_1014(L_143, /*hidden argument*/NULL);
		NullCheck(L_143);
		Event_set_modifiers_m6_1015(L_143, ((int32_t)((int32_t)L_144|(int32_t)((int32_t)64))), /*hidden argument*/NULL);
		goto IL_0a6b;
	}

IL_06a2:
	{
		Event_t6_154 * L_145 = V_0;
		NullCheck(L_145);
		Event_set_keyCode_m6_1025(L_145, ((int32_t)277), /*hidden argument*/NULL);
		Event_t6_154 * L_146 = V_0;
		Event_t6_154 * L_147 = L_146;
		NullCheck(L_147);
		int32_t L_148 = Event_get_modifiers_m6_1014(L_147, /*hidden argument*/NULL);
		NullCheck(L_147);
		Event_set_modifiers_m6_1015(L_147, ((int32_t)((int32_t)L_148|(int32_t)((int32_t)64))), /*hidden argument*/NULL);
		goto IL_0a6b;
	}

IL_06c1:
	{
		Event_t6_154 * L_149 = V_0;
		NullCheck(L_149);
		Event_set_keyCode_m6_1025(L_149, ((int32_t)278), /*hidden argument*/NULL);
		Event_t6_154 * L_150 = V_0;
		Event_t6_154 * L_151 = L_150;
		NullCheck(L_151);
		int32_t L_152 = Event_get_modifiers_m6_1014(L_151, /*hidden argument*/NULL);
		NullCheck(L_151);
		Event_set_modifiers_m6_1015(L_151, ((int32_t)((int32_t)L_152|(int32_t)((int32_t)64))), /*hidden argument*/NULL);
		goto IL_0a6b;
	}

IL_06e0:
	{
		Event_t6_154 * L_153 = V_0;
		NullCheck(L_153);
		Event_set_keyCode_m6_1025(L_153, ((int32_t)279), /*hidden argument*/NULL);
		Event_t6_154 * L_154 = V_0;
		Event_t6_154 * L_155 = L_154;
		NullCheck(L_155);
		int32_t L_156 = Event_get_modifiers_m6_1014(L_155, /*hidden argument*/NULL);
		NullCheck(L_155);
		Event_set_modifiers_m6_1015(L_155, ((int32_t)((int32_t)L_156|(int32_t)((int32_t)64))), /*hidden argument*/NULL);
		goto IL_0a6b;
	}

IL_06ff:
	{
		Event_t6_154 * L_157 = V_0;
		NullCheck(L_157);
		Event_set_keyCode_m6_1025(L_157, ((int32_t)281), /*hidden argument*/NULL);
		Event_t6_154 * L_158 = V_0;
		Event_t6_154 * L_159 = L_158;
		NullCheck(L_159);
		int32_t L_160 = Event_get_modifiers_m6_1014(L_159, /*hidden argument*/NULL);
		NullCheck(L_159);
		Event_set_modifiers_m6_1015(L_159, ((int32_t)((int32_t)L_160|(int32_t)((int32_t)64))), /*hidden argument*/NULL);
		goto IL_0a6b;
	}

IL_071e:
	{
		Event_t6_154 * L_161 = V_0;
		NullCheck(L_161);
		Event_set_keyCode_m6_1025(L_161, ((int32_t)280), /*hidden argument*/NULL);
		Event_t6_154 * L_162 = V_0;
		Event_t6_154 * L_163 = L_162;
		NullCheck(L_163);
		int32_t L_164 = Event_get_modifiers_m6_1014(L_163, /*hidden argument*/NULL);
		NullCheck(L_163);
		Event_set_modifiers_m6_1015(L_163, ((int32_t)((int32_t)L_164|(int32_t)((int32_t)64))), /*hidden argument*/NULL);
		goto IL_0a6b;
	}

IL_073d:
	{
		Event_t6_154 * L_165 = V_0;
		NullCheck(L_165);
		Event_set_keyCode_m6_1025(L_165, ((int32_t)280), /*hidden argument*/NULL);
		Event_t6_154 * L_166 = V_0;
		Event_t6_154 * L_167 = L_166;
		NullCheck(L_167);
		int32_t L_168 = Event_get_modifiers_m6_1014(L_167, /*hidden argument*/NULL);
		NullCheck(L_167);
		Event_set_modifiers_m6_1015(L_167, ((int32_t)((int32_t)L_168|(int32_t)((int32_t)64))), /*hidden argument*/NULL);
		goto IL_0a6b;
	}

IL_075c:
	{
		Event_t6_154 * L_169 = V_0;
		NullCheck(L_169);
		Event_set_keyCode_m6_1025(L_169, ((int32_t)281), /*hidden argument*/NULL);
		Event_t6_154 * L_170 = V_0;
		Event_t6_154 * L_171 = L_170;
		NullCheck(L_171);
		int32_t L_172 = Event_get_modifiers_m6_1014(L_171, /*hidden argument*/NULL);
		NullCheck(L_171);
		Event_set_modifiers_m6_1015(L_171, ((int32_t)((int32_t)L_172|(int32_t)((int32_t)64))), /*hidden argument*/NULL);
		goto IL_0a6b;
	}

IL_077b:
	{
		Event_t6_154 * L_173 = V_0;
		NullCheck(L_173);
		Event_set_keyCode_m6_1025(L_173, 8, /*hidden argument*/NULL);
		Event_t6_154 * L_174 = V_0;
		Event_t6_154 * L_175 = L_174;
		NullCheck(L_175);
		int32_t L_176 = Event_get_modifiers_m6_1014(L_175, /*hidden argument*/NULL);
		NullCheck(L_175);
		Event_set_modifiers_m6_1015(L_175, ((int32_t)((int32_t)L_176|(int32_t)((int32_t)64))), /*hidden argument*/NULL);
		goto IL_0a6b;
	}

IL_0796:
	{
		Event_t6_154 * L_177 = V_0;
		NullCheck(L_177);
		Event_set_keyCode_m6_1025(L_177, ((int32_t)127), /*hidden argument*/NULL);
		Event_t6_154 * L_178 = V_0;
		Event_t6_154 * L_179 = L_178;
		NullCheck(L_179);
		int32_t L_180 = Event_get_modifiers_m6_1014(L_179, /*hidden argument*/NULL);
		NullCheck(L_179);
		Event_set_modifiers_m6_1015(L_179, ((int32_t)((int32_t)L_180|(int32_t)((int32_t)64))), /*hidden argument*/NULL);
		goto IL_0a6b;
	}

IL_07b2:
	{
		Event_t6_154 * L_181 = V_0;
		NullCheck(L_181);
		Event_set_keyCode_m6_1025(L_181, ((int32_t)9), /*hidden argument*/NULL);
		goto IL_0a6b;
	}

IL_07bf:
	{
		Event_t6_154 * L_182 = V_0;
		NullCheck(L_182);
		Event_set_keyCode_m6_1025(L_182, ((int32_t)282), /*hidden argument*/NULL);
		Event_t6_154 * L_183 = V_0;
		Event_t6_154 * L_184 = L_183;
		NullCheck(L_184);
		int32_t L_185 = Event_get_modifiers_m6_1014(L_184, /*hidden argument*/NULL);
		NullCheck(L_184);
		Event_set_modifiers_m6_1015(L_184, ((int32_t)((int32_t)L_185|(int32_t)((int32_t)64))), /*hidden argument*/NULL);
		goto IL_0a6b;
	}

IL_07de:
	{
		Event_t6_154 * L_186 = V_0;
		NullCheck(L_186);
		Event_set_keyCode_m6_1025(L_186, ((int32_t)283), /*hidden argument*/NULL);
		Event_t6_154 * L_187 = V_0;
		Event_t6_154 * L_188 = L_187;
		NullCheck(L_188);
		int32_t L_189 = Event_get_modifiers_m6_1014(L_188, /*hidden argument*/NULL);
		NullCheck(L_188);
		Event_set_modifiers_m6_1015(L_188, ((int32_t)((int32_t)L_189|(int32_t)((int32_t)64))), /*hidden argument*/NULL);
		goto IL_0a6b;
	}

IL_07fd:
	{
		Event_t6_154 * L_190 = V_0;
		NullCheck(L_190);
		Event_set_keyCode_m6_1025(L_190, ((int32_t)284), /*hidden argument*/NULL);
		Event_t6_154 * L_191 = V_0;
		Event_t6_154 * L_192 = L_191;
		NullCheck(L_192);
		int32_t L_193 = Event_get_modifiers_m6_1014(L_192, /*hidden argument*/NULL);
		NullCheck(L_192);
		Event_set_modifiers_m6_1015(L_192, ((int32_t)((int32_t)L_193|(int32_t)((int32_t)64))), /*hidden argument*/NULL);
		goto IL_0a6b;
	}

IL_081c:
	{
		Event_t6_154 * L_194 = V_0;
		NullCheck(L_194);
		Event_set_keyCode_m6_1025(L_194, ((int32_t)285), /*hidden argument*/NULL);
		Event_t6_154 * L_195 = V_0;
		Event_t6_154 * L_196 = L_195;
		NullCheck(L_196);
		int32_t L_197 = Event_get_modifiers_m6_1014(L_196, /*hidden argument*/NULL);
		NullCheck(L_196);
		Event_set_modifiers_m6_1015(L_196, ((int32_t)((int32_t)L_197|(int32_t)((int32_t)64))), /*hidden argument*/NULL);
		goto IL_0a6b;
	}

IL_083b:
	{
		Event_t6_154 * L_198 = V_0;
		NullCheck(L_198);
		Event_set_keyCode_m6_1025(L_198, ((int32_t)286), /*hidden argument*/NULL);
		Event_t6_154 * L_199 = V_0;
		Event_t6_154 * L_200 = L_199;
		NullCheck(L_200);
		int32_t L_201 = Event_get_modifiers_m6_1014(L_200, /*hidden argument*/NULL);
		NullCheck(L_200);
		Event_set_modifiers_m6_1015(L_200, ((int32_t)((int32_t)L_201|(int32_t)((int32_t)64))), /*hidden argument*/NULL);
		goto IL_0a6b;
	}

IL_085a:
	{
		Event_t6_154 * L_202 = V_0;
		NullCheck(L_202);
		Event_set_keyCode_m6_1025(L_202, ((int32_t)287), /*hidden argument*/NULL);
		Event_t6_154 * L_203 = V_0;
		Event_t6_154 * L_204 = L_203;
		NullCheck(L_204);
		int32_t L_205 = Event_get_modifiers_m6_1014(L_204, /*hidden argument*/NULL);
		NullCheck(L_204);
		Event_set_modifiers_m6_1015(L_204, ((int32_t)((int32_t)L_205|(int32_t)((int32_t)64))), /*hidden argument*/NULL);
		goto IL_0a6b;
	}

IL_0879:
	{
		Event_t6_154 * L_206 = V_0;
		NullCheck(L_206);
		Event_set_keyCode_m6_1025(L_206, ((int32_t)288), /*hidden argument*/NULL);
		Event_t6_154 * L_207 = V_0;
		Event_t6_154 * L_208 = L_207;
		NullCheck(L_208);
		int32_t L_209 = Event_get_modifiers_m6_1014(L_208, /*hidden argument*/NULL);
		NullCheck(L_208);
		Event_set_modifiers_m6_1015(L_208, ((int32_t)((int32_t)L_209|(int32_t)((int32_t)64))), /*hidden argument*/NULL);
		goto IL_0a6b;
	}

IL_0898:
	{
		Event_t6_154 * L_210 = V_0;
		NullCheck(L_210);
		Event_set_keyCode_m6_1025(L_210, ((int32_t)289), /*hidden argument*/NULL);
		Event_t6_154 * L_211 = V_0;
		Event_t6_154 * L_212 = L_211;
		NullCheck(L_212);
		int32_t L_213 = Event_get_modifiers_m6_1014(L_212, /*hidden argument*/NULL);
		NullCheck(L_212);
		Event_set_modifiers_m6_1015(L_212, ((int32_t)((int32_t)L_213|(int32_t)((int32_t)64))), /*hidden argument*/NULL);
		goto IL_0a6b;
	}

IL_08b7:
	{
		Event_t6_154 * L_214 = V_0;
		NullCheck(L_214);
		Event_set_keyCode_m6_1025(L_214, ((int32_t)290), /*hidden argument*/NULL);
		Event_t6_154 * L_215 = V_0;
		Event_t6_154 * L_216 = L_215;
		NullCheck(L_216);
		int32_t L_217 = Event_get_modifiers_m6_1014(L_216, /*hidden argument*/NULL);
		NullCheck(L_216);
		Event_set_modifiers_m6_1015(L_216, ((int32_t)((int32_t)L_217|(int32_t)((int32_t)64))), /*hidden argument*/NULL);
		goto IL_0a6b;
	}

IL_08d6:
	{
		Event_t6_154 * L_218 = V_0;
		NullCheck(L_218);
		Event_set_keyCode_m6_1025(L_218, ((int32_t)291), /*hidden argument*/NULL);
		Event_t6_154 * L_219 = V_0;
		Event_t6_154 * L_220 = L_219;
		NullCheck(L_220);
		int32_t L_221 = Event_get_modifiers_m6_1014(L_220, /*hidden argument*/NULL);
		NullCheck(L_220);
		Event_set_modifiers_m6_1015(L_220, ((int32_t)((int32_t)L_221|(int32_t)((int32_t)64))), /*hidden argument*/NULL);
		goto IL_0a6b;
	}

IL_08f5:
	{
		Event_t6_154 * L_222 = V_0;
		NullCheck(L_222);
		Event_set_keyCode_m6_1025(L_222, ((int32_t)292), /*hidden argument*/NULL);
		Event_t6_154 * L_223 = V_0;
		Event_t6_154 * L_224 = L_223;
		NullCheck(L_224);
		int32_t L_225 = Event_get_modifiers_m6_1014(L_224, /*hidden argument*/NULL);
		NullCheck(L_224);
		Event_set_modifiers_m6_1015(L_224, ((int32_t)((int32_t)L_225|(int32_t)((int32_t)64))), /*hidden argument*/NULL);
		goto IL_0a6b;
	}

IL_0914:
	{
		Event_t6_154 * L_226 = V_0;
		NullCheck(L_226);
		Event_set_keyCode_m6_1025(L_226, ((int32_t)293), /*hidden argument*/NULL);
		Event_t6_154 * L_227 = V_0;
		Event_t6_154 * L_228 = L_227;
		NullCheck(L_228);
		int32_t L_229 = Event_get_modifiers_m6_1014(L_228, /*hidden argument*/NULL);
		NullCheck(L_228);
		Event_set_modifiers_m6_1015(L_228, ((int32_t)((int32_t)L_229|(int32_t)((int32_t)64))), /*hidden argument*/NULL);
		goto IL_0a6b;
	}

IL_0933:
	{
		Event_t6_154 * L_230 = V_0;
		NullCheck(L_230);
		Event_set_keyCode_m6_1025(L_230, ((int32_t)294), /*hidden argument*/NULL);
		Event_t6_154 * L_231 = V_0;
		Event_t6_154 * L_232 = L_231;
		NullCheck(L_232);
		int32_t L_233 = Event_get_modifiers_m6_1014(L_232, /*hidden argument*/NULL);
		NullCheck(L_232);
		Event_set_modifiers_m6_1015(L_232, ((int32_t)((int32_t)L_233|(int32_t)((int32_t)64))), /*hidden argument*/NULL);
		goto IL_0a6b;
	}

IL_0952:
	{
		Event_t6_154 * L_234 = V_0;
		NullCheck(L_234);
		Event_set_keyCode_m6_1025(L_234, ((int32_t)295), /*hidden argument*/NULL);
		Event_t6_154 * L_235 = V_0;
		Event_t6_154 * L_236 = L_235;
		NullCheck(L_236);
		int32_t L_237 = Event_get_modifiers_m6_1014(L_236, /*hidden argument*/NULL);
		NullCheck(L_236);
		Event_set_modifiers_m6_1015(L_236, ((int32_t)((int32_t)L_237|(int32_t)((int32_t)64))), /*hidden argument*/NULL);
		goto IL_0a6b;
	}

IL_0971:
	{
		Event_t6_154 * L_238 = V_0;
		NullCheck(L_238);
		Event_set_keyCode_m6_1025(L_238, ((int32_t)296), /*hidden argument*/NULL);
		Event_t6_154 * L_239 = V_0;
		Event_t6_154 * L_240 = L_239;
		NullCheck(L_240);
		int32_t L_241 = Event_get_modifiers_m6_1014(L_240, /*hidden argument*/NULL);
		NullCheck(L_240);
		Event_set_modifiers_m6_1015(L_240, ((int32_t)((int32_t)L_241|(int32_t)((int32_t)64))), /*hidden argument*/NULL);
		goto IL_0a6b;
	}

IL_0990:
	{
		Event_t6_154 * L_242 = V_0;
		NullCheck(L_242);
		Event_set_keyCode_m6_1025(L_242, ((int32_t)27), /*hidden argument*/NULL);
		goto IL_0a6b;
	}

IL_099d:
	{
		Event_t6_154 * L_243 = V_0;
		NullCheck(L_243);
		Event_set_character_m6_1021(L_243, ((int32_t)10), /*hidden argument*/NULL);
		Event_t6_154 * L_244 = V_0;
		NullCheck(L_244);
		Event_set_keyCode_m6_1025(L_244, ((int32_t)13), /*hidden argument*/NULL);
		Event_t6_154 * L_245 = V_0;
		Event_t6_154 * L_246 = L_245;
		NullCheck(L_246);
		int32_t L_247 = Event_get_modifiers_m6_1014(L_246, /*hidden argument*/NULL);
		NullCheck(L_246);
		Event_set_modifiers_m6_1015(L_246, ((int32_t)((int32_t)L_247&(int32_t)((int32_t)-65))), /*hidden argument*/NULL);
		goto IL_0a6b;
	}

IL_09c1:
	{
		Event_t6_154 * L_248 = V_0;
		NullCheck(L_248);
		Event_set_keyCode_m6_1025(L_248, ((int32_t)32), /*hidden argument*/NULL);
		Event_t6_154 * L_249 = V_0;
		NullCheck(L_249);
		Event_set_character_m6_1021(L_249, ((int32_t)32), /*hidden argument*/NULL);
		Event_t6_154 * L_250 = V_0;
		Event_t6_154 * L_251 = L_250;
		NullCheck(L_251);
		int32_t L_252 = Event_get_modifiers_m6_1014(L_251, /*hidden argument*/NULL);
		NullCheck(L_251);
		Event_set_modifiers_m6_1015(L_251, ((int32_t)((int32_t)L_252&(int32_t)((int32_t)-65))), /*hidden argument*/NULL);
		goto IL_0a6b;
	}

IL_09e5:
	{
		String_t* L_253 = V_3;
		NullCheck(L_253);
		int32_t L_254 = String_get_Length_m1_428(L_253, /*hidden argument*/NULL);
		if ((((int32_t)L_254) == ((int32_t)1)))
		{
			goto IL_0a36;
		}
	}

IL_09f1:
	try
	{ // begin try (depth: 1)
		Event_t6_154 * L_255 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_256 = Type_GetTypeFromHandle_m1_893(NULL /*static, unused*/, LoadTypeToken(KeyCode_t6_155_0_0_0_var), /*hidden argument*/NULL);
		String_t* L_257 = V_3;
		IL2CPP_RUNTIME_CLASS_INIT(Enum_t1_24_il2cpp_TypeInfo_var);
		Object_t * L_258 = Enum_Parse_m1_729(NULL /*static, unused*/, L_256, L_257, 1, /*hidden argument*/NULL);
		NullCheck(L_255);
		Event_set_keyCode_m6_1025(L_255, ((*(int32_t*)((int32_t*)UnBox (L_258, Int32_t1_3_il2cpp_TypeInfo_var)))), /*hidden argument*/NULL);
		goto IL_0a31;
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t1_33 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (ArgumentException_t1_646_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_0a12;
		throw e;
	}

CATCH_0a12:
	{ // begin catch(System.ArgumentException)
		ObjectU5BU5D_t1_157* L_259 = ((ObjectU5BU5D_t1_157*)SZArrayNew(ObjectU5BU5D_t1_157_il2cpp_TypeInfo_var, 1));
		String_t* L_260 = V_3;
		NullCheck(L_259);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_259, 0);
		ArrayElementTypeCheck (L_259, L_260);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_259, 0, sizeof(Object_t *))) = (Object_t *)L_260;
		String_t* L_261 = UnityString_Format_m6_437(NULL /*static, unused*/, _stringLiteral2764, L_259, /*hidden argument*/NULL);
		Debug_LogError_m6_490(NULL /*static, unused*/, L_261, /*hidden argument*/NULL);
		goto IL_0a31;
	} // end catch (depth: 1)

IL_0a31:
	{
		goto IL_0a66;
	}

IL_0a36:
	{
		Event_t6_154 * L_262 = V_0;
		String_t* L_263 = V_3;
		NullCheck(L_263);
		String_t* L_264 = String_ToLower_m1_405(L_263, /*hidden argument*/NULL);
		NullCheck(L_264);
		uint16_t L_265 = String_get_Chars_m1_341(L_264, 0, /*hidden argument*/NULL);
		NullCheck(L_262);
		Event_set_character_m6_1021(L_262, L_265, /*hidden argument*/NULL);
		Event_t6_154 * L_266 = V_0;
		Event_t6_154 * L_267 = V_0;
		NullCheck(L_267);
		uint16_t L_268 = Event_get_character_m6_1020(L_267, /*hidden argument*/NULL);
		NullCheck(L_266);
		Event_set_keyCode_m6_1025(L_266, L_268, /*hidden argument*/NULL);
		Event_t6_154 * L_269 = V_0;
		NullCheck(L_269);
		int32_t L_270 = Event_get_modifiers_m6_1014(L_269, /*hidden argument*/NULL);
		if (!L_270)
		{
			goto IL_0a66;
		}
	}
	{
		Event_t6_154 * L_271 = V_0;
		NullCheck(L_271);
		Event_set_character_m6_1021(L_271, 0, /*hidden argument*/NULL);
	}

IL_0a66:
	{
		goto IL_0a6b;
	}

IL_0a6b:
	{
		Event_t6_154 * L_272 = V_0;
		return L_272;
	}
}
// System.Int32 UnityEngine.Event::GetHashCode()
extern "C" int32_t Event_GetHashCode_m6_995 (Event_t6_154 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	Vector2_t6_48  V_1 = {0};
	{
		V_0 = 1;
		bool L_0 = Event_get_isKey_m6_992(__this, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_0015;
		}
	}
	{
		int32_t L_1 = Event_get_keyCode_m6_1024(__this, /*hidden argument*/NULL);
		V_0 = (((int32_t)((uint16_t)L_1)));
	}

IL_0015:
	{
		bool L_2 = Event_get_isMouse_m6_993(__this, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_002f;
		}
	}
	{
		Vector2_t6_48  L_3 = Event_get_mousePosition_m6_970(__this, /*hidden argument*/NULL);
		V_1 = L_3;
		int32_t L_4 = Vector2_GetHashCode_m6_178((&V_1), /*hidden argument*/NULL);
		V_0 = L_4;
	}

IL_002f:
	{
		int32_t L_5 = V_0;
		int32_t L_6 = Event_get_modifiers_m6_1014(__this, /*hidden argument*/NULL);
		V_0 = ((int32_t)((int32_t)((int32_t)((int32_t)L_5*(int32_t)((int32_t)37)))|(int32_t)L_6));
		int32_t L_7 = V_0;
		return L_7;
	}
}
// System.Boolean UnityEngine.Event::Equals(System.Object)
extern TypeInfo* Event_t6_154_il2cpp_TypeInfo_var;
extern "C" bool Event_Equals_m6_996 (Event_t6_154 * __this, Object_t * ___obj, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Event_t6_154_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(978);
		s_Il2CppMethodIntialized = true;
	}
	Event_t6_154 * V_0 = {0};
	{
		Object_t * L_0 = ___obj;
		if (L_0)
		{
			goto IL_0008;
		}
	}
	{
		return 0;
	}

IL_0008:
	{
		Object_t * L_1 = ___obj;
		bool L_2 = Object_ReferenceEquals_m1_8(NULL /*static, unused*/, __this, L_1, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_0016;
		}
	}
	{
		return 1;
	}

IL_0016:
	{
		Object_t * L_3 = ___obj;
		NullCheck(L_3);
		Type_t * L_4 = Object_GetType_m1_5(L_3, /*hidden argument*/NULL);
		Type_t * L_5 = Object_GetType_m1_5(__this, /*hidden argument*/NULL);
		if ((((Object_t*)(Type_t *)L_4) == ((Object_t*)(Type_t *)L_5)))
		{
			goto IL_0029;
		}
	}
	{
		return 0;
	}

IL_0029:
	{
		Object_t * L_6 = ___obj;
		V_0 = ((Event_t6_154 *)CastclassSealed(L_6, Event_t6_154_il2cpp_TypeInfo_var));
		int32_t L_7 = Event_get_type_m6_1003(__this, /*hidden argument*/NULL);
		Event_t6_154 * L_8 = V_0;
		NullCheck(L_8);
		int32_t L_9 = Event_get_type_m6_1003(L_8, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_7) == ((uint32_t)L_9))))
		{
			goto IL_0058;
		}
	}
	{
		int32_t L_10 = Event_get_modifiers_m6_1014(__this, /*hidden argument*/NULL);
		Event_t6_154 * L_11 = V_0;
		NullCheck(L_11);
		int32_t L_12 = Event_get_modifiers_m6_1014(L_11, /*hidden argument*/NULL);
		if ((((int32_t)((int32_t)((int32_t)L_10&(int32_t)((int32_t)-33)))) == ((int32_t)((int32_t)((int32_t)L_12&(int32_t)((int32_t)-33))))))
		{
			goto IL_005a;
		}
	}

IL_0058:
	{
		return 0;
	}

IL_005a:
	{
		bool L_13 = Event_get_isKey_m6_992(__this, /*hidden argument*/NULL);
		if (!L_13)
		{
			goto IL_0074;
		}
	}
	{
		int32_t L_14 = Event_get_keyCode_m6_1024(__this, /*hidden argument*/NULL);
		Event_t6_154 * L_15 = V_0;
		NullCheck(L_15);
		int32_t L_16 = Event_get_keyCode_m6_1024(L_15, /*hidden argument*/NULL);
		return ((((int32_t)L_14) == ((int32_t)L_16))? 1 : 0);
	}

IL_0074:
	{
		bool L_17 = Event_get_isMouse_m6_993(__this, /*hidden argument*/NULL);
		if (!L_17)
		{
			goto IL_0091;
		}
	}
	{
		Vector2_t6_48  L_18 = Event_get_mousePosition_m6_970(__this, /*hidden argument*/NULL);
		Event_t6_154 * L_19 = V_0;
		NullCheck(L_19);
		Vector2_t6_48  L_20 = Event_get_mousePosition_m6_970(L_19, /*hidden argument*/NULL);
		bool L_21 = Vector2_op_Equality_m6_188(NULL /*static, unused*/, L_18, L_20, /*hidden argument*/NULL);
		return L_21;
	}

IL_0091:
	{
		return 0;
	}
}
// System.String UnityEngine.Event::ToString()
extern TypeInfo* ObjectU5BU5D_t1_157_il2cpp_TypeInfo_var;
extern TypeInfo* EventType_t6_156_il2cpp_TypeInfo_var;
extern TypeInfo* EventModifiers_t6_157_il2cpp_TypeInfo_var;
extern TypeInfo* KeyCode_t6_155_il2cpp_TypeInfo_var;
extern TypeInfo* Int32_t1_3_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* Vector2_t6_48_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2765;
extern Il2CppCodeGenString* _stringLiteral2766;
extern Il2CppCodeGenString* _stringLiteral2767;
extern Il2CppCodeGenString* _stringLiteral2768;
extern Il2CppCodeGenString* _stringLiteral2769;
extern Il2CppCodeGenString* _stringLiteral2770;
extern Il2CppCodeGenString* _stringLiteral2771;
extern "C" String_t* Event_ToString_m6_997 (Event_t6_154 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ObjectU5BU5D_t1_157_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(19);
		EventType_t6_156_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(980);
		EventModifiers_t6_157_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(981);
		KeyCode_t6_155_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(979);
		Int32_t1_3_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(8);
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(11);
		Vector2_t6_48_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(939);
		_stringLiteral2765 = il2cpp_codegen_string_literal_from_index(2765);
		_stringLiteral2766 = il2cpp_codegen_string_literal_from_index(2766);
		_stringLiteral2767 = il2cpp_codegen_string_literal_from_index(2767);
		_stringLiteral2768 = il2cpp_codegen_string_literal_from_index(2768);
		_stringLiteral2769 = il2cpp_codegen_string_literal_from_index(2769);
		_stringLiteral2770 = il2cpp_codegen_string_literal_from_index(2770);
		_stringLiteral2771 = il2cpp_codegen_string_literal_from_index(2771);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool L_0 = Event_get_isKey_m6_992(__this, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_00b5;
		}
	}
	{
		uint16_t L_1 = Event_get_character_m6_1020(__this, /*hidden argument*/NULL);
		if (L_1)
		{
			goto IL_0051;
		}
	}
	{
		ObjectU5BU5D_t1_157* L_2 = ((ObjectU5BU5D_t1_157*)SZArrayNew(ObjectU5BU5D_t1_157_il2cpp_TypeInfo_var, 3));
		int32_t L_3 = Event_get_type_m6_1003(__this, /*hidden argument*/NULL);
		int32_t L_4 = L_3;
		Object_t * L_5 = Box(EventType_t6_156_il2cpp_TypeInfo_var, &L_4);
		NullCheck(L_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_2, 0);
		ArrayElementTypeCheck (L_2, L_5);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_2, 0, sizeof(Object_t *))) = (Object_t *)L_5;
		ObjectU5BU5D_t1_157* L_6 = L_2;
		int32_t L_7 = Event_get_modifiers_m6_1014(__this, /*hidden argument*/NULL);
		int32_t L_8 = L_7;
		Object_t * L_9 = Box(EventModifiers_t6_157_il2cpp_TypeInfo_var, &L_8);
		NullCheck(L_6);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_6, 1);
		ArrayElementTypeCheck (L_6, L_9);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_6, 1, sizeof(Object_t *))) = (Object_t *)L_9;
		ObjectU5BU5D_t1_157* L_10 = L_6;
		int32_t L_11 = Event_get_keyCode_m6_1024(__this, /*hidden argument*/NULL);
		int32_t L_12 = L_11;
		Object_t * L_13 = Box(KeyCode_t6_155_il2cpp_TypeInfo_var, &L_12);
		NullCheck(L_10);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_10, 2);
		ArrayElementTypeCheck (L_10, L_13);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_10, 2, sizeof(Object_t *))) = (Object_t *)L_13;
		String_t* L_14 = UnityString_Format_m6_437(NULL /*static, unused*/, _stringLiteral2765, L_10, /*hidden argument*/NULL);
		return L_14;
	}

IL_0051:
	{
		ObjectU5BU5D_t1_157* L_15 = ((ObjectU5BU5D_t1_157*)SZArrayNew(ObjectU5BU5D_t1_157_il2cpp_TypeInfo_var, 8));
		NullCheck(L_15);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_15, 0);
		ArrayElementTypeCheck (L_15, _stringLiteral2766);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_15, 0, sizeof(Object_t *))) = (Object_t *)_stringLiteral2766;
		ObjectU5BU5D_t1_157* L_16 = L_15;
		int32_t L_17 = Event_get_type_m6_1003(__this, /*hidden argument*/NULL);
		int32_t L_18 = L_17;
		Object_t * L_19 = Box(EventType_t6_156_il2cpp_TypeInfo_var, &L_18);
		NullCheck(L_16);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_16, 1);
		ArrayElementTypeCheck (L_16, L_19);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_16, 1, sizeof(Object_t *))) = (Object_t *)L_19;
		ObjectU5BU5D_t1_157* L_20 = L_16;
		NullCheck(L_20);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_20, 2);
		ArrayElementTypeCheck (L_20, _stringLiteral2767);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_20, 2, sizeof(Object_t *))) = (Object_t *)_stringLiteral2767;
		ObjectU5BU5D_t1_157* L_21 = L_20;
		uint16_t L_22 = Event_get_character_m6_1020(__this, /*hidden argument*/NULL);
		int32_t L_23 = L_22;
		Object_t * L_24 = Box(Int32_t1_3_il2cpp_TypeInfo_var, &L_23);
		NullCheck(L_21);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_21, 3);
		ArrayElementTypeCheck (L_21, L_24);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_21, 3, sizeof(Object_t *))) = (Object_t *)L_24;
		ObjectU5BU5D_t1_157* L_25 = L_21;
		NullCheck(L_25);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_25, 4);
		ArrayElementTypeCheck (L_25, _stringLiteral2768);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_25, 4, sizeof(Object_t *))) = (Object_t *)_stringLiteral2768;
		ObjectU5BU5D_t1_157* L_26 = L_25;
		int32_t L_27 = Event_get_modifiers_m6_1014(__this, /*hidden argument*/NULL);
		int32_t L_28 = L_27;
		Object_t * L_29 = Box(EventModifiers_t6_157_il2cpp_TypeInfo_var, &L_28);
		NullCheck(L_26);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_26, 5);
		ArrayElementTypeCheck (L_26, L_29);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_26, 5, sizeof(Object_t *))) = (Object_t *)L_29;
		ObjectU5BU5D_t1_157* L_30 = L_26;
		NullCheck(L_30);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_30, 6);
		ArrayElementTypeCheck (L_30, _stringLiteral2769);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_30, 6, sizeof(Object_t *))) = (Object_t *)_stringLiteral2769;
		ObjectU5BU5D_t1_157* L_31 = L_30;
		int32_t L_32 = Event_get_keyCode_m6_1024(__this, /*hidden argument*/NULL);
		int32_t L_33 = L_32;
		Object_t * L_34 = Box(KeyCode_t6_155_il2cpp_TypeInfo_var, &L_33);
		NullCheck(L_31);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_31, 7);
		ArrayElementTypeCheck (L_31, L_34);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_31, 7, sizeof(Object_t *))) = (Object_t *)L_34;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_35 = String_Concat_m1_421(NULL /*static, unused*/, L_31, /*hidden argument*/NULL);
		return L_35;
	}

IL_00b5:
	{
		bool L_36 = Event_get_isMouse_m6_993(__this, /*hidden argument*/NULL);
		if (!L_36)
		{
			goto IL_00fb;
		}
	}
	{
		ObjectU5BU5D_t1_157* L_37 = ((ObjectU5BU5D_t1_157*)SZArrayNew(ObjectU5BU5D_t1_157_il2cpp_TypeInfo_var, 3));
		int32_t L_38 = Event_get_type_m6_1003(__this, /*hidden argument*/NULL);
		int32_t L_39 = L_38;
		Object_t * L_40 = Box(EventType_t6_156_il2cpp_TypeInfo_var, &L_39);
		NullCheck(L_37);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_37, 0);
		ArrayElementTypeCheck (L_37, L_40);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_37, 0, sizeof(Object_t *))) = (Object_t *)L_40;
		ObjectU5BU5D_t1_157* L_41 = L_37;
		Vector2_t6_48  L_42 = Event_get_mousePosition_m6_970(__this, /*hidden argument*/NULL);
		Vector2_t6_48  L_43 = L_42;
		Object_t * L_44 = Box(Vector2_t6_48_il2cpp_TypeInfo_var, &L_43);
		NullCheck(L_41);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_41, 1);
		ArrayElementTypeCheck (L_41, L_44);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_41, 1, sizeof(Object_t *))) = (Object_t *)L_44;
		ObjectU5BU5D_t1_157* L_45 = L_41;
		int32_t L_46 = Event_get_modifiers_m6_1014(__this, /*hidden argument*/NULL);
		int32_t L_47 = L_46;
		Object_t * L_48 = Box(EventModifiers_t6_157_il2cpp_TypeInfo_var, &L_47);
		NullCheck(L_45);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_45, 2);
		ArrayElementTypeCheck (L_45, L_48);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_45, 2, sizeof(Object_t *))) = (Object_t *)L_48;
		String_t* L_49 = UnityString_Format_m6_437(NULL /*static, unused*/, _stringLiteral2770, L_45, /*hidden argument*/NULL);
		return L_49;
	}

IL_00fb:
	{
		int32_t L_50 = Event_get_type_m6_1003(__this, /*hidden argument*/NULL);
		if ((((int32_t)L_50) == ((int32_t)((int32_t)14))))
		{
			goto IL_0115;
		}
	}
	{
		int32_t L_51 = Event_get_type_m6_1003(__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_51) == ((uint32_t)((int32_t)13)))))
		{
			goto IL_013d;
		}
	}

IL_0115:
	{
		ObjectU5BU5D_t1_157* L_52 = ((ObjectU5BU5D_t1_157*)SZArrayNew(ObjectU5BU5D_t1_157_il2cpp_TypeInfo_var, 2));
		int32_t L_53 = Event_get_type_m6_1003(__this, /*hidden argument*/NULL);
		int32_t L_54 = L_53;
		Object_t * L_55 = Box(EventType_t6_156_il2cpp_TypeInfo_var, &L_54);
		NullCheck(L_52);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_52, 0);
		ArrayElementTypeCheck (L_52, L_55);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_52, 0, sizeof(Object_t *))) = (Object_t *)L_55;
		ObjectU5BU5D_t1_157* L_56 = L_52;
		String_t* L_57 = Event_get_commandName_m6_1022(__this, /*hidden argument*/NULL);
		NullCheck(L_56);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_56, 1);
		ArrayElementTypeCheck (L_56, L_57);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_56, 1, sizeof(Object_t *))) = (Object_t *)L_57;
		String_t* L_58 = UnityString_Format_m6_437(NULL /*static, unused*/, _stringLiteral2771, L_56, /*hidden argument*/NULL);
		return L_58;
	}

IL_013d:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_59 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->___Empty_2;
		int32_t L_60 = Event_get_type_m6_1003(__this, /*hidden argument*/NULL);
		int32_t L_61 = L_60;
		Object_t * L_62 = Box(EventType_t6_156_il2cpp_TypeInfo_var, &L_61);
		String_t* L_63 = String_Concat_m1_416(NULL /*static, unused*/, L_59, L_62, /*hidden argument*/NULL);
		return L_63;
	}
}
// System.Void UnityEngine.Event::Init()
extern "C" void Event_Init_m6_998 (Event_t6_154 * __this, const MethodInfo* method)
{
	typedef void (*Event_Init_m6_998_ftn) (Event_t6_154 *);
	static Event_Init_m6_998_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Event_Init_m6_998_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Event::Init()");
	_il2cpp_icall_func(__this);
}
// System.Void UnityEngine.Event::Cleanup()
extern "C" void Event_Cleanup_m6_999 (Event_t6_154 * __this, const MethodInfo* method)
{
	typedef void (*Event_Cleanup_m6_999_ftn) (Event_t6_154 *);
	static Event_Cleanup_m6_999_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Event_Cleanup_m6_999_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Event::Cleanup()");
	_il2cpp_icall_func(__this);
}
// System.Void UnityEngine.Event::InitCopy(UnityEngine.Event)
extern "C" void Event_InitCopy_m6_1000 (Event_t6_154 * __this, Event_t6_154 * ___other, const MethodInfo* method)
{
	typedef void (*Event_InitCopy_m6_1000_ftn) (Event_t6_154 *, Event_t6_154 *);
	static Event_InitCopy_m6_1000_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Event_InitCopy_m6_1000_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Event::InitCopy(UnityEngine.Event)");
	_il2cpp_icall_func(__this, ___other);
}
// System.Void UnityEngine.Event::InitPtr(System.IntPtr)
extern "C" void Event_InitPtr_m6_1001 (Event_t6_154 * __this, IntPtr_t ___ptr, const MethodInfo* method)
{
	typedef void (*Event_InitPtr_m6_1001_ftn) (Event_t6_154 *, IntPtr_t);
	static Event_InitPtr_m6_1001_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Event_InitPtr_m6_1001_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Event::InitPtr(System.IntPtr)");
	_il2cpp_icall_func(__this, ___ptr);
}
// UnityEngine.EventType UnityEngine.Event::get_rawType()
extern "C" int32_t Event_get_rawType_m6_1002 (Event_t6_154 * __this, const MethodInfo* method)
{
	typedef int32_t (*Event_get_rawType_m6_1002_ftn) (Event_t6_154 *);
	static Event_get_rawType_m6_1002_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Event_get_rawType_m6_1002_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Event::get_rawType()");
	return _il2cpp_icall_func(__this);
}
// UnityEngine.EventType UnityEngine.Event::get_type()
extern "C" int32_t Event_get_type_m6_1003 (Event_t6_154 * __this, const MethodInfo* method)
{
	typedef int32_t (*Event_get_type_m6_1003_ftn) (Event_t6_154 *);
	static Event_get_type_m6_1003_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Event_get_type_m6_1003_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Event::get_type()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.Event::set_type(UnityEngine.EventType)
extern "C" void Event_set_type_m6_1004 (Event_t6_154 * __this, int32_t ___value, const MethodInfo* method)
{
	typedef void (*Event_set_type_m6_1004_ftn) (Event_t6_154 *, int32_t);
	static Event_set_type_m6_1004_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Event_set_type_m6_1004_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Event::set_type(UnityEngine.EventType)");
	_il2cpp_icall_func(__this, ___value);
}
// UnityEngine.EventType UnityEngine.Event::GetTypeForControl(System.Int32)
extern "C" int32_t Event_GetTypeForControl_m6_1005 (Event_t6_154 * __this, int32_t ___controlID, const MethodInfo* method)
{
	typedef int32_t (*Event_GetTypeForControl_m6_1005_ftn) (Event_t6_154 *, int32_t);
	static Event_GetTypeForControl_m6_1005_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Event_GetTypeForControl_m6_1005_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Event::GetTypeForControl(System.Int32)");
	return _il2cpp_icall_func(__this, ___controlID);
}
// System.Void UnityEngine.Event::Internal_SetMousePosition(UnityEngine.Vector2)
extern "C" void Event_Internal_SetMousePosition_m6_1006 (Event_t6_154 * __this, Vector2_t6_48  ___value, const MethodInfo* method)
{
	{
		Event_INTERNAL_CALL_Internal_SetMousePosition_m6_1007(NULL /*static, unused*/, __this, (&___value), /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Event::INTERNAL_CALL_Internal_SetMousePosition(UnityEngine.Event,UnityEngine.Vector2&)
extern "C" void Event_INTERNAL_CALL_Internal_SetMousePosition_m6_1007 (Object_t * __this /* static, unused */, Event_t6_154 * ___self, Vector2_t6_48 * ___value, const MethodInfo* method)
{
	typedef void (*Event_INTERNAL_CALL_Internal_SetMousePosition_m6_1007_ftn) (Event_t6_154 *, Vector2_t6_48 *);
	static Event_INTERNAL_CALL_Internal_SetMousePosition_m6_1007_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Event_INTERNAL_CALL_Internal_SetMousePosition_m6_1007_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Event::INTERNAL_CALL_Internal_SetMousePosition(UnityEngine.Event,UnityEngine.Vector2&)");
	_il2cpp_icall_func(___self, ___value);
}
// System.Void UnityEngine.Event::Internal_GetMousePosition(UnityEngine.Vector2&)
extern "C" void Event_Internal_GetMousePosition_m6_1008 (Event_t6_154 * __this, Vector2_t6_48 * ___value, const MethodInfo* method)
{
	typedef void (*Event_Internal_GetMousePosition_m6_1008_ftn) (Event_t6_154 *, Vector2_t6_48 *);
	static Event_Internal_GetMousePosition_m6_1008_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Event_Internal_GetMousePosition_m6_1008_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Event::Internal_GetMousePosition(UnityEngine.Vector2&)");
	_il2cpp_icall_func(__this, ___value);
}
// System.Void UnityEngine.Event::Internal_SetMouseDelta(UnityEngine.Vector2)
extern "C" void Event_Internal_SetMouseDelta_m6_1009 (Event_t6_154 * __this, Vector2_t6_48  ___value, const MethodInfo* method)
{
	{
		Event_INTERNAL_CALL_Internal_SetMouseDelta_m6_1010(NULL /*static, unused*/, __this, (&___value), /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Event::INTERNAL_CALL_Internal_SetMouseDelta(UnityEngine.Event,UnityEngine.Vector2&)
extern "C" void Event_INTERNAL_CALL_Internal_SetMouseDelta_m6_1010 (Object_t * __this /* static, unused */, Event_t6_154 * ___self, Vector2_t6_48 * ___value, const MethodInfo* method)
{
	typedef void (*Event_INTERNAL_CALL_Internal_SetMouseDelta_m6_1010_ftn) (Event_t6_154 *, Vector2_t6_48 *);
	static Event_INTERNAL_CALL_Internal_SetMouseDelta_m6_1010_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Event_INTERNAL_CALL_Internal_SetMouseDelta_m6_1010_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Event::INTERNAL_CALL_Internal_SetMouseDelta(UnityEngine.Event,UnityEngine.Vector2&)");
	_il2cpp_icall_func(___self, ___value);
}
// System.Void UnityEngine.Event::Internal_GetMouseDelta(UnityEngine.Vector2&)
extern "C" void Event_Internal_GetMouseDelta_m6_1011 (Event_t6_154 * __this, Vector2_t6_48 * ___value, const MethodInfo* method)
{
	typedef void (*Event_Internal_GetMouseDelta_m6_1011_ftn) (Event_t6_154 *, Vector2_t6_48 *);
	static Event_Internal_GetMouseDelta_m6_1011_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Event_Internal_GetMouseDelta_m6_1011_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Event::Internal_GetMouseDelta(UnityEngine.Vector2&)");
	_il2cpp_icall_func(__this, ___value);
}
// System.Int32 UnityEngine.Event::get_button()
extern "C" int32_t Event_get_button_m6_1012 (Event_t6_154 * __this, const MethodInfo* method)
{
	typedef int32_t (*Event_get_button_m6_1012_ftn) (Event_t6_154 *);
	static Event_get_button_m6_1012_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Event_get_button_m6_1012_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Event::get_button()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.Event::set_button(System.Int32)
extern "C" void Event_set_button_m6_1013 (Event_t6_154 * __this, int32_t ___value, const MethodInfo* method)
{
	typedef void (*Event_set_button_m6_1013_ftn) (Event_t6_154 *, int32_t);
	static Event_set_button_m6_1013_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Event_set_button_m6_1013_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Event::set_button(System.Int32)");
	_il2cpp_icall_func(__this, ___value);
}
// UnityEngine.EventModifiers UnityEngine.Event::get_modifiers()
extern "C" int32_t Event_get_modifiers_m6_1014 (Event_t6_154 * __this, const MethodInfo* method)
{
	typedef int32_t (*Event_get_modifiers_m6_1014_ftn) (Event_t6_154 *);
	static Event_get_modifiers_m6_1014_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Event_get_modifiers_m6_1014_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Event::get_modifiers()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.Event::set_modifiers(UnityEngine.EventModifiers)
extern "C" void Event_set_modifiers_m6_1015 (Event_t6_154 * __this, int32_t ___value, const MethodInfo* method)
{
	typedef void (*Event_set_modifiers_m6_1015_ftn) (Event_t6_154 *, int32_t);
	static Event_set_modifiers_m6_1015_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Event_set_modifiers_m6_1015_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Event::set_modifiers(UnityEngine.EventModifiers)");
	_il2cpp_icall_func(__this, ___value);
}
// System.Single UnityEngine.Event::get_pressure()
extern "C" float Event_get_pressure_m6_1016 (Event_t6_154 * __this, const MethodInfo* method)
{
	typedef float (*Event_get_pressure_m6_1016_ftn) (Event_t6_154 *);
	static Event_get_pressure_m6_1016_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Event_get_pressure_m6_1016_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Event::get_pressure()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.Event::set_pressure(System.Single)
extern "C" void Event_set_pressure_m6_1017 (Event_t6_154 * __this, float ___value, const MethodInfo* method)
{
	typedef void (*Event_set_pressure_m6_1017_ftn) (Event_t6_154 *, float);
	static Event_set_pressure_m6_1017_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Event_set_pressure_m6_1017_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Event::set_pressure(System.Single)");
	_il2cpp_icall_func(__this, ___value);
}
// System.Int32 UnityEngine.Event::get_clickCount()
extern "C" int32_t Event_get_clickCount_m6_1018 (Event_t6_154 * __this, const MethodInfo* method)
{
	typedef int32_t (*Event_get_clickCount_m6_1018_ftn) (Event_t6_154 *);
	static Event_get_clickCount_m6_1018_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Event_get_clickCount_m6_1018_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Event::get_clickCount()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.Event::set_clickCount(System.Int32)
extern "C" void Event_set_clickCount_m6_1019 (Event_t6_154 * __this, int32_t ___value, const MethodInfo* method)
{
	typedef void (*Event_set_clickCount_m6_1019_ftn) (Event_t6_154 *, int32_t);
	static Event_set_clickCount_m6_1019_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Event_set_clickCount_m6_1019_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Event::set_clickCount(System.Int32)");
	_il2cpp_icall_func(__this, ___value);
}
// System.Char UnityEngine.Event::get_character()
extern "C" uint16_t Event_get_character_m6_1020 (Event_t6_154 * __this, const MethodInfo* method)
{
	typedef uint16_t (*Event_get_character_m6_1020_ftn) (Event_t6_154 *);
	static Event_get_character_m6_1020_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Event_get_character_m6_1020_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Event::get_character()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.Event::set_character(System.Char)
extern "C" void Event_set_character_m6_1021 (Event_t6_154 * __this, uint16_t ___value, const MethodInfo* method)
{
	typedef void (*Event_set_character_m6_1021_ftn) (Event_t6_154 *, uint16_t);
	static Event_set_character_m6_1021_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Event_set_character_m6_1021_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Event::set_character(System.Char)");
	_il2cpp_icall_func(__this, ___value);
}
// System.String UnityEngine.Event::get_commandName()
extern "C" String_t* Event_get_commandName_m6_1022 (Event_t6_154 * __this, const MethodInfo* method)
{
	typedef String_t* (*Event_get_commandName_m6_1022_ftn) (Event_t6_154 *);
	static Event_get_commandName_m6_1022_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Event_get_commandName_m6_1022_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Event::get_commandName()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.Event::set_commandName(System.String)
extern "C" void Event_set_commandName_m6_1023 (Event_t6_154 * __this, String_t* ___value, const MethodInfo* method)
{
	typedef void (*Event_set_commandName_m6_1023_ftn) (Event_t6_154 *, String_t*);
	static Event_set_commandName_m6_1023_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Event_set_commandName_m6_1023_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Event::set_commandName(System.String)");
	_il2cpp_icall_func(__this, ___value);
}
// UnityEngine.KeyCode UnityEngine.Event::get_keyCode()
extern "C" int32_t Event_get_keyCode_m6_1024 (Event_t6_154 * __this, const MethodInfo* method)
{
	typedef int32_t (*Event_get_keyCode_m6_1024_ftn) (Event_t6_154 *);
	static Event_get_keyCode_m6_1024_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Event_get_keyCode_m6_1024_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Event::get_keyCode()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.Event::set_keyCode(UnityEngine.KeyCode)
extern "C" void Event_set_keyCode_m6_1025 (Event_t6_154 * __this, int32_t ___value, const MethodInfo* method)
{
	typedef void (*Event_set_keyCode_m6_1025_ftn) (Event_t6_154 *, int32_t);
	static Event_set_keyCode_m6_1025_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Event_set_keyCode_m6_1025_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Event::set_keyCode(UnityEngine.KeyCode)");
	_il2cpp_icall_func(__this, ___value);
}
// System.Void UnityEngine.Event::Internal_SetNativeEvent(System.IntPtr)
extern "C" void Event_Internal_SetNativeEvent_m6_1026 (Object_t * __this /* static, unused */, IntPtr_t ___ptr, const MethodInfo* method)
{
	typedef void (*Event_Internal_SetNativeEvent_m6_1026_ftn) (IntPtr_t);
	static Event_Internal_SetNativeEvent_m6_1026_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Event_Internal_SetNativeEvent_m6_1026_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Event::Internal_SetNativeEvent(System.IntPtr)");
	_il2cpp_icall_func(___ptr);
}
// System.Void UnityEngine.Event::Use()
extern "C" void Event_Use_m6_1027 (Event_t6_154 * __this, const MethodInfo* method)
{
	typedef void (*Event_Use_m6_1027_ftn) (Event_t6_154 *);
	static Event_Use_m6_1027_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Event_Use_m6_1027_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Event::Use()");
	_il2cpp_icall_func(__this);
}
// System.Boolean UnityEngine.Event::PopEvent(UnityEngine.Event)
extern "C" bool Event_PopEvent_m6_1028 (Object_t * __this /* static, unused */, Event_t6_154 * ___outEvent, const MethodInfo* method)
{
	typedef bool (*Event_PopEvent_m6_1028_ftn) (Event_t6_154 *);
	static Event_PopEvent_m6_1028_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Event_PopEvent_m6_1028_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Event::PopEvent(UnityEngine.Event)");
	return _il2cpp_icall_func(___outEvent);
}
// System.Int32 UnityEngine.Event::GetEventCount()
extern "C" int32_t Event_GetEventCount_m6_1029 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	typedef int32_t (*Event_GetEventCount_m6_1029_ftn) ();
	static Event_GetEventCount_m6_1029_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Event_GetEventCount_m6_1029_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Event::GetEventCount()");
	return _il2cpp_icall_func();
}
// Conversion methods for marshalling of: UnityEngine.Event
extern "C" void Event_t6_154_marshal(const Event_t6_154& unmarshaled, Event_t6_154_marshaled& marshaled)
{
	Il2CppCodeGenException* ___s_Current_1Exception = il2cpp_codegen_get_not_supported_exception("Cannot marshal field 's_Current' of type 'Event': Reference type field marshaling is not supported.");
	il2cpp_codegen_raise_exception((Il2CppCodeGenException*)___s_Current_1Exception);
}
extern "C" void Event_t6_154_marshal_back(const Event_t6_154_marshaled& marshaled, Event_t6_154& unmarshaled)
{
	Il2CppCodeGenException* ___s_Current_1Exception = il2cpp_codegen_get_not_supported_exception("Cannot marshal field 's_Current' of type 'Event': Reference type field marshaling is not supported.");
	il2cpp_codegen_raise_exception((Il2CppCodeGenException*)___s_Current_1Exception);
}
// Conversion method for clean up from marshalling of: UnityEngine.Event
extern "C" void Event_t6_154_marshal_cleanup(Event_t6_154_marshaled& marshaled)
{
}
// System.Void UnityEngine.GUI/ScrollViewState::.ctor()
extern "C" void ScrollViewState__ctor_m6_1030 (ScrollViewState_t6_158 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1_0(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.GUI/WindowFunction::.ctor(System.Object,System.IntPtr)
extern "C" void WindowFunction__ctor_m6_1031 (WindowFunction_t6_159 * __this, Object_t * ___object, IntPtr_t ___method, const MethodInfo* method)
{
	__this->___method_ptr_0 = (methodPointerType)((MethodInfo*)___method.___m_value_0)->method;
	__this->___method_3 = ___method;
	__this->___m_target_2 = ___object;
}
// System.Void UnityEngine.GUI/WindowFunction::Invoke(System.Int32)
extern "C" void WindowFunction_Invoke_m6_1032 (WindowFunction_t6_159 * __this, int32_t ___id, const MethodInfo* method)
{
	if(__this->___prev_9 != NULL)
	{
		WindowFunction_Invoke_m6_1032((WindowFunction_t6_159 *)__this->___prev_9,___id, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->___method_3.___m_value_0));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->___method_3.___m_value_0));
	if (__this->___m_target_2 != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Object_t *, Object_t * __this, int32_t ___id, const MethodInfo* method);
		((FunctionPointerType)__this->___method_ptr_0)(NULL,__this->___m_target_2,___id,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
	else
	{
		typedef void (*FunctionPointerType) (Object_t * __this, int32_t ___id, const MethodInfo* method);
		((FunctionPointerType)__this->___method_ptr_0)(__this->___m_target_2,___id,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
}
extern "C" void pinvoke_delegate_wrapper_WindowFunction_t6_159(Il2CppObject* delegate, int32_t ___id)
{
	typedef void (STDCALL *native_function_ptr_type)(int32_t);
	native_function_ptr_type _il2cpp_pinvoke_func = ((native_function_ptr_type)((Il2CppDelegate*)delegate)->method->method);
	// Marshaling of parameter '___id' to native representation

	// Native function invocation
	_il2cpp_pinvoke_func(___id);

	// Marshaling cleanup of parameter '___id' native representation

}
// System.IAsyncResult UnityEngine.GUI/WindowFunction::BeginInvoke(System.Int32,System.AsyncCallback,System.Object)
extern TypeInfo* Int32_t1_3_il2cpp_TypeInfo_var;
extern "C" Object_t * WindowFunction_BeginInvoke_m6_1033 (WindowFunction_t6_159 * __this, int32_t ___id, AsyncCallback_t1_28 * ___callback, Object_t * ___object, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Int32_t1_3_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(8);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(Int32_t1_3_il2cpp_TypeInfo_var, &___id);
	return (Object_t *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback, (Il2CppObject*)___object);
}
// System.Void UnityEngine.GUI/WindowFunction::EndInvoke(System.IAsyncResult)
extern "C" void WindowFunction_EndInvoke_m6_1034 (WindowFunction_t6_159 * __this, Object_t * ___result, const MethodInfo* method)
{
	il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result, 0);
}
// System.Void UnityEngine.GUI::.cctor()
extern TypeInfo* GUI_t6_160_il2cpp_TypeInfo_var;
extern TypeInfo* GenericStack_t6_162_il2cpp_TypeInfo_var;
extern TypeInfo* DateTime_t1_127_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2772;
extern Il2CppCodeGenString* _stringLiteral2773;
extern Il2CppCodeGenString* _stringLiteral2774;
extern Il2CppCodeGenString* _stringLiteral2775;
extern Il2CppCodeGenString* _stringLiteral2776;
extern Il2CppCodeGenString* _stringLiteral2777;
extern Il2CppCodeGenString* _stringLiteral2778;
extern "C" void GUI__cctor_m6_1035 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GUI_t6_160_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(982);
		GenericStack_t6_162_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(983);
		DateTime_t1_127_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(172);
		_stringLiteral2772 = il2cpp_codegen_string_literal_from_index(2772);
		_stringLiteral2773 = il2cpp_codegen_string_literal_from_index(2773);
		_stringLiteral2774 = il2cpp_codegen_string_literal_from_index(2774);
		_stringLiteral2775 = il2cpp_codegen_string_literal_from_index(2775);
		_stringLiteral2776 = il2cpp_codegen_string_literal_from_index(2776);
		_stringLiteral2777 = il2cpp_codegen_string_literal_from_index(2777);
		_stringLiteral2778 = il2cpp_codegen_string_literal_from_index(2778);
		s_Il2CppMethodIntialized = true;
	}
	{
		((GUI_t6_160_StaticFields*)GUI_t6_160_il2cpp_TypeInfo_var->static_fields)->___s_ScrollStepSize_0 = (10.0f);
		((GUI_t6_160_StaticFields*)GUI_t6_160_il2cpp_TypeInfo_var->static_fields)->___s_HotTextField_2 = (-1);
		NullCheck(_stringLiteral2772);
		int32_t L_0 = String_GetHashCode_m1_433(_stringLiteral2772, /*hidden argument*/NULL);
		((GUI_t6_160_StaticFields*)GUI_t6_160_il2cpp_TypeInfo_var->static_fields)->___s_BoxHash_3 = L_0;
		NullCheck(_stringLiteral2773);
		int32_t L_1 = String_GetHashCode_m1_433(_stringLiteral2773, /*hidden argument*/NULL);
		((GUI_t6_160_StaticFields*)GUI_t6_160_il2cpp_TypeInfo_var->static_fields)->___s_RepeatButtonHash_4 = L_1;
		NullCheck(_stringLiteral2774);
		int32_t L_2 = String_GetHashCode_m1_433(_stringLiteral2774, /*hidden argument*/NULL);
		((GUI_t6_160_StaticFields*)GUI_t6_160_il2cpp_TypeInfo_var->static_fields)->___s_ToggleHash_5 = L_2;
		NullCheck(_stringLiteral2775);
		int32_t L_3 = String_GetHashCode_m1_433(_stringLiteral2775, /*hidden argument*/NULL);
		((GUI_t6_160_StaticFields*)GUI_t6_160_il2cpp_TypeInfo_var->static_fields)->___s_ButtonGridHash_6 = L_3;
		NullCheck(_stringLiteral2776);
		int32_t L_4 = String_GetHashCode_m1_433(_stringLiteral2776, /*hidden argument*/NULL);
		((GUI_t6_160_StaticFields*)GUI_t6_160_il2cpp_TypeInfo_var->static_fields)->___s_SliderHash_7 = L_4;
		NullCheck(_stringLiteral2777);
		int32_t L_5 = String_GetHashCode_m1_433(_stringLiteral2777, /*hidden argument*/NULL);
		((GUI_t6_160_StaticFields*)GUI_t6_160_il2cpp_TypeInfo_var->static_fields)->___s_BeginGroupHash_8 = L_5;
		NullCheck(_stringLiteral2778);
		int32_t L_6 = String_GetHashCode_m1_433(_stringLiteral2778, /*hidden argument*/NULL);
		((GUI_t6_160_StaticFields*)GUI_t6_160_il2cpp_TypeInfo_var->static_fields)->___s_ScrollviewHash_9 = L_6;
		GenericStack_t6_162 * L_7 = (GenericStack_t6_162 *)il2cpp_codegen_object_new (GenericStack_t6_162_il2cpp_TypeInfo_var);
		GenericStack__ctor_m6_1648(L_7, /*hidden argument*/NULL);
		((GUI_t6_160_StaticFields*)GUI_t6_160_il2cpp_TypeInfo_var->static_fields)->___s_ScrollViewStates_12 = L_7;
		IL2CPP_RUNTIME_CLASS_INIT(DateTime_t1_127_il2cpp_TypeInfo_var);
		DateTime_t1_127  L_8 = DateTime_get_Now_m1_4932(NULL /*static, unused*/, /*hidden argument*/NULL);
		GUI_set_nextScrollStepTime_m6_1037(NULL /*static, unused*/, L_8, /*hidden argument*/NULL);
		return;
	}
}
// System.DateTime UnityEngine.GUI::get_nextScrollStepTime()
extern TypeInfo* GUI_t6_160_il2cpp_TypeInfo_var;
extern "C" DateTime_t1_127  GUI_get_nextScrollStepTime_m6_1036 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GUI_t6_160_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(982);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t6_160_il2cpp_TypeInfo_var);
		DateTime_t1_127  L_0 = ((GUI_t6_160_StaticFields*)GUI_t6_160_il2cpp_TypeInfo_var->static_fields)->___U3CnextScrollStepTimeU3Ek__BackingField_13;
		return L_0;
	}
}
// System.Void UnityEngine.GUI::set_nextScrollStepTime(System.DateTime)
extern TypeInfo* GUI_t6_160_il2cpp_TypeInfo_var;
extern "C" void GUI_set_nextScrollStepTime_m6_1037 (Object_t * __this /* static, unused */, DateTime_t1_127  ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GUI_t6_160_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(982);
		s_Il2CppMethodIntialized = true;
	}
	{
		DateTime_t1_127  L_0 = ___value;
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t6_160_il2cpp_TypeInfo_var);
		((GUI_t6_160_StaticFields*)GUI_t6_160_il2cpp_TypeInfo_var->static_fields)->___U3CnextScrollStepTimeU3Ek__BackingField_13 = L_0;
		return;
	}
}
// System.Int32 UnityEngine.GUI::get_scrollTroughSide()
extern TypeInfo* GUI_t6_160_il2cpp_TypeInfo_var;
extern "C" int32_t GUI_get_scrollTroughSide_m6_1038 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GUI_t6_160_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(982);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t6_160_il2cpp_TypeInfo_var);
		int32_t L_0 = ((GUI_t6_160_StaticFields*)GUI_t6_160_il2cpp_TypeInfo_var->static_fields)->___U3CscrollTroughSideU3Ek__BackingField_14;
		return L_0;
	}
}
// System.Void UnityEngine.GUI::set_scrollTroughSide(System.Int32)
extern TypeInfo* GUI_t6_160_il2cpp_TypeInfo_var;
extern "C" void GUI_set_scrollTroughSide_m6_1039 (Object_t * __this /* static, unused */, int32_t ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GUI_t6_160_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(982);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = ___value;
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t6_160_il2cpp_TypeInfo_var);
		((GUI_t6_160_StaticFields*)GUI_t6_160_il2cpp_TypeInfo_var->static_fields)->___U3CscrollTroughSideU3Ek__BackingField_14 = L_0;
		return;
	}
}
// System.Void UnityEngine.GUI::set_skin(UnityEngine.GUISkin)
extern TypeInfo* GUIUtility_t6_185_il2cpp_TypeInfo_var;
extern TypeInfo* GUI_t6_160_il2cpp_TypeInfo_var;
extern "C" void GUI_set_skin_m6_1040 (Object_t * __this /* static, unused */, GUISkin_t6_161 * ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GUIUtility_t6_185_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(984);
		GUI_t6_160_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(982);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GUIUtility_t6_185_il2cpp_TypeInfo_var);
		GUIUtility_CheckOnGUI_m6_1376(NULL /*static, unused*/, /*hidden argument*/NULL);
		GUISkin_t6_161 * L_0 = ___value;
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t6_160_il2cpp_TypeInfo_var);
		GUI_DoSetSkin_m6_1042(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.GUISkin UnityEngine.GUI::get_skin()
extern TypeInfo* GUIUtility_t6_185_il2cpp_TypeInfo_var;
extern TypeInfo* GUI_t6_160_il2cpp_TypeInfo_var;
extern "C" GUISkin_t6_161 * GUI_get_skin_m6_1041 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GUIUtility_t6_185_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(984);
		GUI_t6_160_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(982);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GUIUtility_t6_185_il2cpp_TypeInfo_var);
		GUIUtility_CheckOnGUI_m6_1376(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t6_160_il2cpp_TypeInfo_var);
		GUISkin_t6_161 * L_0 = ((GUI_t6_160_StaticFields*)GUI_t6_160_il2cpp_TypeInfo_var->static_fields)->___s_Skin_10;
		return L_0;
	}
}
// System.Void UnityEngine.GUI::DoSetSkin(UnityEngine.GUISkin)
extern TypeInfo* GUIUtility_t6_185_il2cpp_TypeInfo_var;
extern TypeInfo* GUI_t6_160_il2cpp_TypeInfo_var;
extern "C" void GUI_DoSetSkin_m6_1042 (Object_t * __this /* static, unused */, GUISkin_t6_161 * ___newSkin, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GUIUtility_t6_185_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(984);
		GUI_t6_160_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(982);
		s_Il2CppMethodIntialized = true;
	}
	{
		GUISkin_t6_161 * L_0 = ___newSkin;
		bool L_1 = Object_op_Implicit_m6_578(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		if (L_1)
		{
			goto IL_0012;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GUIUtility_t6_185_il2cpp_TypeInfo_var);
		GUISkin_t6_161 * L_2 = GUIUtility_GetDefaultSkin_m6_1372(NULL /*static, unused*/, /*hidden argument*/NULL);
		___newSkin = L_2;
	}

IL_0012:
	{
		GUISkin_t6_161 * L_3 = ___newSkin;
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t6_160_il2cpp_TypeInfo_var);
		((GUI_t6_160_StaticFields*)GUI_t6_160_il2cpp_TypeInfo_var->static_fields)->___s_Skin_10 = L_3;
		GUISkin_t6_161 * L_4 = ___newSkin;
		NullCheck(L_4);
		GUISkin_MakeCurrent_m6_1267(L_4, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.GUI::set_tooltip(System.String)
extern TypeInfo* GUI_t6_160_il2cpp_TypeInfo_var;
extern "C" void GUI_set_tooltip_m6_1043 (Object_t * __this /* static, unused */, String_t* ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GUI_t6_160_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(982);
		s_Il2CppMethodIntialized = true;
	}
	{
		String_t* L_0 = ___value;
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t6_160_il2cpp_TypeInfo_var);
		GUI_Internal_SetTooltip_m6_1083(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.GUI::Label(UnityEngine.Rect,System.String)
extern TypeInfo* GUIContent_t6_163_il2cpp_TypeInfo_var;
extern TypeInfo* GUI_t6_160_il2cpp_TypeInfo_var;
extern "C" void GUI_Label_m6_1044 (Object_t * __this /* static, unused */, Rect_t6_52  ___position, String_t* ___text, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GUIContent_t6_163_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(985);
		GUI_t6_160_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(982);
		s_Il2CppMethodIntialized = true;
	}
	{
		Rect_t6_52  L_0 = ___position;
		String_t* L_1 = ___text;
		IL2CPP_RUNTIME_CLASS_INIT(GUIContent_t6_163_il2cpp_TypeInfo_var);
		GUIContent_t6_163 * L_2 = GUIContent_Temp_m6_1106(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t6_160_il2cpp_TypeInfo_var);
		GUISkin_t6_161 * L_3 = ((GUI_t6_160_StaticFields*)GUI_t6_160_il2cpp_TypeInfo_var->static_fields)->___s_Skin_10;
		NullCheck(L_3);
		GUIStyle_t6_166 * L_4 = GUISkin_get_label_m6_1221(L_3, /*hidden argument*/NULL);
		GUI_Label_m6_1046(NULL /*static, unused*/, L_0, L_2, L_4, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.GUI::Label(UnityEngine.Rect,System.String,UnityEngine.GUIStyle)
extern TypeInfo* GUIContent_t6_163_il2cpp_TypeInfo_var;
extern TypeInfo* GUI_t6_160_il2cpp_TypeInfo_var;
extern "C" void GUI_Label_m6_1045 (Object_t * __this /* static, unused */, Rect_t6_52  ___position, String_t* ___text, GUIStyle_t6_166 * ___style, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GUIContent_t6_163_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(985);
		GUI_t6_160_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(982);
		s_Il2CppMethodIntialized = true;
	}
	{
		Rect_t6_52  L_0 = ___position;
		String_t* L_1 = ___text;
		IL2CPP_RUNTIME_CLASS_INIT(GUIContent_t6_163_il2cpp_TypeInfo_var);
		GUIContent_t6_163 * L_2 = GUIContent_Temp_m6_1106(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		GUIStyle_t6_166 * L_3 = ___style;
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t6_160_il2cpp_TypeInfo_var);
		GUI_Label_m6_1046(NULL /*static, unused*/, L_0, L_2, L_3, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.GUI::Label(UnityEngine.Rect,UnityEngine.GUIContent,UnityEngine.GUIStyle)
extern TypeInfo* GUIUtility_t6_185_il2cpp_TypeInfo_var;
extern TypeInfo* GUI_t6_160_il2cpp_TypeInfo_var;
extern "C" void GUI_Label_m6_1046 (Object_t * __this /* static, unused */, Rect_t6_52  ___position, GUIContent_t6_163 * ___content, GUIStyle_t6_166 * ___style, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GUIUtility_t6_185_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(984);
		GUI_t6_160_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(982);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GUIUtility_t6_185_il2cpp_TypeInfo_var);
		GUIUtility_CheckOnGUI_m6_1376(NULL /*static, unused*/, /*hidden argument*/NULL);
		Rect_t6_52  L_0 = ___position;
		GUIContent_t6_163 * L_1 = ___content;
		GUIStyle_t6_166 * L_2 = ___style;
		NullCheck(L_2);
		IntPtr_t L_3 = (L_2->___m_Ptr_0);
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t6_160_il2cpp_TypeInfo_var);
		GUI_DoLabel_m6_1084(NULL /*static, unused*/, L_0, L_1, L_3, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.GUI::Box(UnityEngine.Rect,System.String)
extern TypeInfo* GUIContent_t6_163_il2cpp_TypeInfo_var;
extern TypeInfo* GUI_t6_160_il2cpp_TypeInfo_var;
extern "C" void GUI_Box_m6_1047 (Object_t * __this /* static, unused */, Rect_t6_52  ___position, String_t* ___text, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GUIContent_t6_163_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(985);
		GUI_t6_160_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(982);
		s_Il2CppMethodIntialized = true;
	}
	{
		Rect_t6_52  L_0 = ___position;
		String_t* L_1 = ___text;
		IL2CPP_RUNTIME_CLASS_INIT(GUIContent_t6_163_il2cpp_TypeInfo_var);
		GUIContent_t6_163 * L_2 = GUIContent_Temp_m6_1106(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t6_160_il2cpp_TypeInfo_var);
		GUISkin_t6_161 * L_3 = ((GUI_t6_160_StaticFields*)GUI_t6_160_il2cpp_TypeInfo_var->static_fields)->___s_Skin_10;
		NullCheck(L_3);
		GUIStyle_t6_166 * L_4 = GUISkin_get_box_m6_1219(L_3, /*hidden argument*/NULL);
		GUI_Box_m6_1048(NULL /*static, unused*/, L_0, L_2, L_4, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.GUI::Box(UnityEngine.Rect,UnityEngine.GUIContent,UnityEngine.GUIStyle)
extern TypeInfo* GUIUtility_t6_185_il2cpp_TypeInfo_var;
extern TypeInfo* GUI_t6_160_il2cpp_TypeInfo_var;
extern "C" void GUI_Box_m6_1048 (Object_t * __this /* static, unused */, Rect_t6_52  ___position, GUIContent_t6_163 * ___content, GUIStyle_t6_166 * ___style, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GUIUtility_t6_185_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(984);
		GUI_t6_160_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(982);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	{
		IL2CPP_RUNTIME_CLASS_INIT(GUIUtility_t6_185_il2cpp_TypeInfo_var);
		GUIUtility_CheckOnGUI_m6_1376(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t6_160_il2cpp_TypeInfo_var);
		int32_t L_0 = ((GUI_t6_160_StaticFields*)GUI_t6_160_il2cpp_TypeInfo_var->static_fields)->___s_BoxHash_3;
		int32_t L_1 = GUIUtility_GetControlID_m6_1378(NULL /*static, unused*/, L_0, 2, /*hidden argument*/NULL);
		V_0 = L_1;
		Event_t6_154 * L_2 = Event_get_current_m6_989(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_2);
		int32_t L_3 = Event_get_type_m6_1003(L_2, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_3) == ((uint32_t)7))))
		{
			goto IL_002a;
		}
	}
	{
		GUIStyle_t6_166 * L_4 = ___style;
		Rect_t6_52  L_5 = ___position;
		GUIContent_t6_163 * L_6 = ___content;
		int32_t L_7 = V_0;
		NullCheck(L_4);
		GUIStyle_Draw_m6_1311(L_4, L_5, L_6, L_7, /*hidden argument*/NULL);
	}

IL_002a:
	{
		return;
	}
}
// System.Boolean UnityEngine.GUI::Button(UnityEngine.Rect,UnityEngine.Texture,UnityEngine.GUIStyle)
extern TypeInfo* GUIUtility_t6_185_il2cpp_TypeInfo_var;
extern TypeInfo* GUIContent_t6_163_il2cpp_TypeInfo_var;
extern TypeInfo* GUI_t6_160_il2cpp_TypeInfo_var;
extern "C" bool GUI_Button_m6_1049 (Object_t * __this /* static, unused */, Rect_t6_52  ___position, Texture_t6_32 * ___image, GUIStyle_t6_166 * ___style, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GUIUtility_t6_185_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(984);
		GUIContent_t6_163_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(985);
		GUI_t6_160_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(982);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GUIUtility_t6_185_il2cpp_TypeInfo_var);
		GUIUtility_CheckOnGUI_m6_1376(NULL /*static, unused*/, /*hidden argument*/NULL);
		Rect_t6_52  L_0 = ___position;
		Texture_t6_32 * L_1 = ___image;
		IL2CPP_RUNTIME_CLASS_INIT(GUIContent_t6_163_il2cpp_TypeInfo_var);
		GUIContent_t6_163 * L_2 = GUIContent_Temp_m6_1107(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		GUIStyle_t6_166 * L_3 = ___style;
		NullCheck(L_3);
		IntPtr_t L_4 = (L_3->___m_Ptr_0);
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t6_160_il2cpp_TypeInfo_var);
		bool L_5 = GUI_DoButton_m6_1086(NULL /*static, unused*/, L_0, L_2, L_4, /*hidden argument*/NULL);
		return L_5;
	}
}
// System.Boolean UnityEngine.GUI::Button(UnityEngine.Rect,UnityEngine.GUIContent,UnityEngine.GUIStyle)
extern TypeInfo* GUIUtility_t6_185_il2cpp_TypeInfo_var;
extern TypeInfo* GUI_t6_160_il2cpp_TypeInfo_var;
extern "C" bool GUI_Button_m6_1050 (Object_t * __this /* static, unused */, Rect_t6_52  ___position, GUIContent_t6_163 * ___content, GUIStyle_t6_166 * ___style, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GUIUtility_t6_185_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(984);
		GUI_t6_160_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(982);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GUIUtility_t6_185_il2cpp_TypeInfo_var);
		GUIUtility_CheckOnGUI_m6_1376(NULL /*static, unused*/, /*hidden argument*/NULL);
		Rect_t6_52  L_0 = ___position;
		GUIContent_t6_163 * L_1 = ___content;
		GUIStyle_t6_166 * L_2 = ___style;
		NullCheck(L_2);
		IntPtr_t L_3 = (L_2->___m_Ptr_0);
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t6_160_il2cpp_TypeInfo_var);
		bool L_4 = GUI_DoButton_m6_1086(NULL /*static, unused*/, L_0, L_1, L_3, /*hidden argument*/NULL);
		return L_4;
	}
}
// System.Boolean UnityEngine.GUI::DoRepeatButton(UnityEngine.Rect,UnityEngine.GUIContent,UnityEngine.GUIStyle,UnityEngine.FocusType)
extern TypeInfo* GUIUtility_t6_185_il2cpp_TypeInfo_var;
extern TypeInfo* GUI_t6_160_il2cpp_TypeInfo_var;
extern "C" bool GUI_DoRepeatButton_m6_1051 (Object_t * __this /* static, unused */, Rect_t6_52  ___position, GUIContent_t6_163 * ___content, GUIStyle_t6_166 * ___style, int32_t ___focusType, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GUIUtility_t6_185_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(984);
		GUI_t6_160_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(982);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = {0};
	int32_t G_B13_0 = 0;
	{
		IL2CPP_RUNTIME_CLASS_INIT(GUIUtility_t6_185_il2cpp_TypeInfo_var);
		GUIUtility_CheckOnGUI_m6_1376(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t6_160_il2cpp_TypeInfo_var);
		int32_t L_0 = ((GUI_t6_160_StaticFields*)GUI_t6_160_il2cpp_TypeInfo_var->static_fields)->___s_RepeatButtonHash_4;
		int32_t L_1 = ___focusType;
		Rect_t6_52  L_2 = ___position;
		int32_t L_3 = GUIUtility_GetControlID_m6_1368(NULL /*static, unused*/, L_0, L_1, L_2, /*hidden argument*/NULL);
		V_0 = L_3;
		Event_t6_154 * L_4 = Event_get_current_m6_989(NULL /*static, unused*/, /*hidden argument*/NULL);
		int32_t L_5 = V_0;
		NullCheck(L_4);
		int32_t L_6 = Event_GetTypeForControl_m6_1005(L_4, L_5, /*hidden argument*/NULL);
		V_1 = L_6;
		int32_t L_7 = V_1;
		if (!L_7)
		{
			goto IL_0037;
		}
	}
	{
		int32_t L_8 = V_1;
		if ((((int32_t)L_8) == ((int32_t)1)))
		{
			goto IL_005f;
		}
	}
	{
		int32_t L_9 = V_1;
		if ((((int32_t)L_9) == ((int32_t)7)))
		{
			goto IL_008e;
		}
	}
	{
		goto IL_00b7;
	}

IL_0037:
	{
		Event_t6_154 * L_10 = Event_get_current_m6_989(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_10);
		Vector2_t6_48  L_11 = Event_get_mousePosition_m6_970(L_10, /*hidden argument*/NULL);
		bool L_12 = Rect_Contains_m6_286((&___position), L_11, /*hidden argument*/NULL);
		if (!L_12)
		{
			goto IL_005d;
		}
	}
	{
		int32_t L_13 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(GUIUtility_t6_185_il2cpp_TypeInfo_var);
		GUIUtility_set_hotControl_m6_1371(NULL /*static, unused*/, L_13, /*hidden argument*/NULL);
		Event_t6_154 * L_14 = Event_get_current_m6_989(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_14);
		Event_Use_m6_1027(L_14, /*hidden argument*/NULL);
	}

IL_005d:
	{
		return 0;
	}

IL_005f:
	{
		IL2CPP_RUNTIME_CLASS_INIT(GUIUtility_t6_185_il2cpp_TypeInfo_var);
		int32_t L_15 = GUIUtility_get_hotControl_m6_1370(NULL /*static, unused*/, /*hidden argument*/NULL);
		int32_t L_16 = V_0;
		if ((!(((uint32_t)L_15) == ((uint32_t)L_16))))
		{
			goto IL_008c;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GUIUtility_t6_185_il2cpp_TypeInfo_var);
		GUIUtility_set_hotControl_m6_1371(NULL /*static, unused*/, 0, /*hidden argument*/NULL);
		Event_t6_154 * L_17 = Event_get_current_m6_989(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_17);
		Event_Use_m6_1027(L_17, /*hidden argument*/NULL);
		Event_t6_154 * L_18 = Event_get_current_m6_989(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_18);
		Vector2_t6_48  L_19 = Event_get_mousePosition_m6_970(L_18, /*hidden argument*/NULL);
		bool L_20 = Rect_Contains_m6_286((&___position), L_19, /*hidden argument*/NULL);
		return L_20;
	}

IL_008c:
	{
		return 0;
	}

IL_008e:
	{
		GUIStyle_t6_166 * L_21 = ___style;
		Rect_t6_52  L_22 = ___position;
		GUIContent_t6_163 * L_23 = ___content;
		int32_t L_24 = V_0;
		NullCheck(L_21);
		GUIStyle_Draw_m6_1311(L_21, L_22, L_23, L_24, /*hidden argument*/NULL);
		int32_t L_25 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(GUIUtility_t6_185_il2cpp_TypeInfo_var);
		int32_t L_26 = GUIUtility_get_hotControl_m6_1370(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_25) == ((uint32_t)L_26))))
		{
			goto IL_00b5;
		}
	}
	{
		Event_t6_154 * L_27 = Event_get_current_m6_989(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_27);
		Vector2_t6_48  L_28 = Event_get_mousePosition_m6_970(L_27, /*hidden argument*/NULL);
		bool L_29 = Rect_Contains_m6_286((&___position), L_28, /*hidden argument*/NULL);
		G_B13_0 = ((int32_t)(L_29));
		goto IL_00b6;
	}

IL_00b5:
	{
		G_B13_0 = 0;
	}

IL_00b6:
	{
		return G_B13_0;
	}

IL_00b7:
	{
		return 0;
	}
}
// System.String UnityEngine.GUI::PasswordFieldGetStrToShow(System.String,System.Char)
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern "C" String_t* GUI_PasswordFieldGetStrToShow_m6_1052 (Object_t * __this /* static, unused */, String_t* ___password, uint16_t ___maskChar, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(11);
		s_Il2CppMethodIntialized = true;
	}
	String_t* G_B4_0 = {0};
	{
		Event_t6_154 * L_0 = Event_get_current_m6_989(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_0);
		int32_t L_1 = Event_get_type_m6_1003(L_0, /*hidden argument*/NULL);
		if ((((int32_t)L_1) == ((int32_t)7)))
		{
			goto IL_001f;
		}
	}
	{
		Event_t6_154 * L_2 = Event_get_current_m6_989(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_2);
		int32_t L_3 = Event_get_type_m6_1003(L_2, /*hidden argument*/NULL);
		if (L_3)
		{
			goto IL_0035;
		}
	}

IL_001f:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_4 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->___Empty_2;
		String_t* L_5 = ___password;
		NullCheck(L_5);
		int32_t L_6 = String_get_Length_m1_428(L_5, /*hidden argument*/NULL);
		uint16_t L_7 = ___maskChar;
		NullCheck(L_4);
		String_t* L_8 = String_PadRight_m1_398(L_4, L_6, L_7, /*hidden argument*/NULL);
		G_B4_0 = L_8;
		goto IL_0036;
	}

IL_0035:
	{
		String_t* L_9 = ___password;
		G_B4_0 = L_9;
	}

IL_0036:
	{
		return G_B4_0;
	}
}
// System.Void UnityEngine.GUI::DoTextField(UnityEngine.Rect,System.Int32,UnityEngine.GUIContent,System.Boolean,System.Int32,UnityEngine.GUIStyle)
extern TypeInfo* GUI_t6_160_il2cpp_TypeInfo_var;
extern "C" void GUI_DoTextField_m6_1053 (Object_t * __this /* static, unused */, Rect_t6_52  ___position, int32_t ___id, GUIContent_t6_163 * ___content, bool ___multiline, int32_t ___maxLength, GUIStyle_t6_166 * ___style, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GUI_t6_160_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(982);
		s_Il2CppMethodIntialized = true;
	}
	{
		Rect_t6_52  L_0 = ___position;
		int32_t L_1 = ___id;
		GUIContent_t6_163 * L_2 = ___content;
		bool L_3 = ___multiline;
		int32_t L_4 = ___maxLength;
		GUIStyle_t6_166 * L_5 = ___style;
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t6_160_il2cpp_TypeInfo_var);
		GUI_DoTextField_m6_1054(NULL /*static, unused*/, L_0, L_1, L_2, L_3, L_4, L_5, (String_t*)NULL, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.GUI::DoTextField(UnityEngine.Rect,System.Int32,UnityEngine.GUIContent,System.Boolean,System.Int32,UnityEngine.GUIStyle,System.String)
extern TypeInfo* GUI_t6_160_il2cpp_TypeInfo_var;
extern "C" void GUI_DoTextField_m6_1054 (Object_t * __this /* static, unused */, Rect_t6_52  ___position, int32_t ___id, GUIContent_t6_163 * ___content, bool ___multiline, int32_t ___maxLength, GUIStyle_t6_166 * ___style, String_t* ___secureText, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GUI_t6_160_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(982);
		s_Il2CppMethodIntialized = true;
	}
	{
		Rect_t6_52  L_0 = ___position;
		int32_t L_1 = ___id;
		GUIContent_t6_163 * L_2 = ___content;
		bool L_3 = ___multiline;
		int32_t L_4 = ___maxLength;
		GUIStyle_t6_166 * L_5 = ___style;
		String_t* L_6 = ___secureText;
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t6_160_il2cpp_TypeInfo_var);
		GUI_DoTextField_m6_1055(NULL /*static, unused*/, L_0, L_1, L_2, L_3, L_4, L_5, L_6, 0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.GUI::DoTextField(UnityEngine.Rect,System.Int32,UnityEngine.GUIContent,System.Boolean,System.Int32,UnityEngine.GUIStyle,System.String,System.Char)
extern const Il2CppType* TextEditor_t6_238_0_0_0_var;
extern TypeInfo* GUIUtility_t6_185_il2cpp_TypeInfo_var;
extern TypeInfo* Type_t_il2cpp_TypeInfo_var;
extern TypeInfo* TextEditor_t6_238_il2cpp_TypeInfo_var;
extern TypeInfo* GUI_t6_160_il2cpp_TypeInfo_var;
extern "C" void GUI_DoTextField_m6_1055 (Object_t * __this /* static, unused */, Rect_t6_52  ___position, int32_t ___id, GUIContent_t6_163 * ___content, bool ___multiline, int32_t ___maxLength, GUIStyle_t6_166 * ___style, String_t* ___secureText, uint16_t ___maskChar, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		TextEditor_t6_238_0_0_0_var = il2cpp_codegen_type_from_index(986);
		GUIUtility_t6_185_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(984);
		Type_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3);
		TextEditor_t6_238_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(986);
		GUI_t6_160_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(982);
		s_Il2CppMethodIntialized = true;
	}
	TextEditor_t6_238 * V_0 = {0};
	{
		int32_t L_0 = ___maxLength;
		if ((((int32_t)L_0) < ((int32_t)0)))
		{
			goto IL_002e;
		}
	}
	{
		GUIContent_t6_163 * L_1 = ___content;
		NullCheck(L_1);
		String_t* L_2 = GUIContent_get_text_m6_1103(L_1, /*hidden argument*/NULL);
		NullCheck(L_2);
		int32_t L_3 = String_get_Length_m1_428(L_2, /*hidden argument*/NULL);
		int32_t L_4 = ___maxLength;
		if ((((int32_t)L_3) <= ((int32_t)L_4)))
		{
			goto IL_002e;
		}
	}
	{
		GUIContent_t6_163 * L_5 = ___content;
		GUIContent_t6_163 * L_6 = ___content;
		NullCheck(L_6);
		String_t* L_7 = GUIContent_get_text_m6_1103(L_6, /*hidden argument*/NULL);
		int32_t L_8 = ___maxLength;
		NullCheck(L_7);
		String_t* L_9 = String_Substring_m1_352(L_7, 0, L_8, /*hidden argument*/NULL);
		NullCheck(L_5);
		GUIContent_set_text_m6_1104(L_5, L_9, /*hidden argument*/NULL);
	}

IL_002e:
	{
		IL2CPP_RUNTIME_CLASS_INIT(GUIUtility_t6_185_il2cpp_TypeInfo_var);
		GUIUtility_CheckOnGUI_m6_1376(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_10 = Type_GetTypeFromHandle_m1_893(NULL /*static, unused*/, LoadTypeToken(TextEditor_t6_238_0_0_0_var), /*hidden argument*/NULL);
		int32_t L_11 = ___id;
		Object_t * L_12 = GUIUtility_GetStateObject_m6_1369(NULL /*static, unused*/, L_10, L_11, /*hidden argument*/NULL);
		V_0 = ((TextEditor_t6_238 *)CastclassClass(L_12, TextEditor_t6_238_il2cpp_TypeInfo_var));
		TextEditor_t6_238 * L_13 = V_0;
		NullCheck(L_13);
		GUIContent_t6_163 * L_14 = (L_13->___content_2);
		GUIContent_t6_163 * L_15 = ___content;
		NullCheck(L_15);
		String_t* L_16 = GUIContent_get_text_m6_1103(L_15, /*hidden argument*/NULL);
		NullCheck(L_14);
		GUIContent_set_text_m6_1104(L_14, L_16, /*hidden argument*/NULL);
		TextEditor_t6_238 * L_17 = V_0;
		NullCheck(L_17);
		TextEditor_SaveBackup_m6_1611(L_17, /*hidden argument*/NULL);
		TextEditor_t6_238 * L_18 = V_0;
		Rect_t6_52  L_19 = ___position;
		NullCheck(L_18);
		TextEditor_set_position_m6_1538(L_18, L_19, /*hidden argument*/NULL);
		TextEditor_t6_238 * L_20 = V_0;
		GUIStyle_t6_166 * L_21 = ___style;
		NullCheck(L_20);
		L_20->___style_3 = L_21;
		TextEditor_t6_238 * L_22 = V_0;
		bool L_23 = ___multiline;
		NullCheck(L_22);
		L_22->___multiline_4 = L_23;
		TextEditor_t6_238 * L_24 = V_0;
		int32_t L_25 = ___id;
		NullCheck(L_24);
		L_24->___controlID_1 = L_25;
		TextEditor_t6_238 * L_26 = V_0;
		NullCheck(L_26);
		TextEditor_DetectFocusChange_m6_1618(L_26, /*hidden argument*/NULL);
		bool L_27 = TouchScreenKeyboard_get_isSupported_m6_154(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (!L_27)
		{
			goto IL_00a4;
		}
	}
	{
		Rect_t6_52  L_28 = ___position;
		int32_t L_29 = ___id;
		GUIContent_t6_163 * L_30 = ___content;
		bool L_31 = ___multiline;
		int32_t L_32 = ___maxLength;
		GUIStyle_t6_166 * L_33 = ___style;
		String_t* L_34 = ___secureText;
		uint16_t L_35 = ___maskChar;
		TextEditor_t6_238 * L_36 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t6_160_il2cpp_TypeInfo_var);
		GUI_HandleTextFieldEventForTouchscreen_m6_1056(NULL /*static, unused*/, L_28, L_29, L_30, L_31, L_32, L_33, L_34, L_35, L_36, /*hidden argument*/NULL);
		goto IL_00b2;
	}

IL_00a4:
	{
		Rect_t6_52  L_37 = ___position;
		int32_t L_38 = ___id;
		GUIContent_t6_163 * L_39 = ___content;
		bool L_40 = ___multiline;
		int32_t L_41 = ___maxLength;
		GUIStyle_t6_166 * L_42 = ___style;
		TextEditor_t6_238 * L_43 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t6_160_il2cpp_TypeInfo_var);
		GUI_HandleTextFieldEventForDesktop_m6_1057(NULL /*static, unused*/, L_37, L_38, L_39, L_40, L_41, L_42, L_43, /*hidden argument*/NULL);
	}

IL_00b2:
	{
		TextEditor_t6_238 * L_44 = V_0;
		NullCheck(L_44);
		TextEditor_UpdateScrollOffsetIfNeeded_m6_1607(L_44, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.GUI::HandleTextFieldEventForTouchscreen(UnityEngine.Rect,System.Int32,UnityEngine.GUIContent,System.Boolean,System.Int32,UnityEngine.GUIStyle,System.String,System.Char,UnityEngine.TextEditor)
extern const Il2CppType* TextEditor_t6_238_0_0_0_var;
extern TypeInfo* GUIUtility_t6_185_il2cpp_TypeInfo_var;
extern TypeInfo* GUI_t6_160_il2cpp_TypeInfo_var;
extern TypeInfo* Type_t_il2cpp_TypeInfo_var;
extern TypeInfo* TextEditor_t6_238_il2cpp_TypeInfo_var;
extern "C" void GUI_HandleTextFieldEventForTouchscreen_m6_1056 (Object_t * __this /* static, unused */, Rect_t6_52  ___position, int32_t ___id, GUIContent_t6_163 * ___content, bool ___multiline, int32_t ___maxLength, GUIStyle_t6_166 * ___style, String_t* ___secureText, uint16_t ___maskChar, TextEditor_t6_238 * ___editor, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		TextEditor_t6_238_0_0_0_var = il2cpp_codegen_type_from_index(986);
		GUIUtility_t6_185_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(984);
		GUI_t6_160_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(982);
		Type_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3);
		TextEditor_t6_238_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(986);
		s_Il2CppMethodIntialized = true;
	}
	Event_t6_154 * V_0 = {0};
	TextEditor_t6_238 * V_1 = {0};
	String_t* V_2 = {0};
	int32_t V_3 = {0};
	TextEditor_t6_238 * G_B11_0 = {0};
	TextEditor_t6_238 * G_B10_0 = {0};
	String_t* G_B12_0 = {0};
	TextEditor_t6_238 * G_B12_1 = {0};
	{
		Event_t6_154 * L_0 = Event_get_current_m6_989(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_0 = L_0;
		Event_t6_154 * L_1 = V_0;
		NullCheck(L_1);
		int32_t L_2 = Event_get_type_m6_1003(L_1, /*hidden argument*/NULL);
		V_3 = L_2;
		int32_t L_3 = V_3;
		if (!L_3)
		{
			goto IL_001f;
		}
	}
	{
		int32_t L_4 = V_3;
		if ((((int32_t)L_4) == ((int32_t)7)))
		{
			goto IL_00bb;
		}
	}
	{
		goto IL_0159;
	}

IL_001f:
	{
		Event_t6_154 * L_5 = V_0;
		NullCheck(L_5);
		Vector2_t6_48  L_6 = Event_get_mousePosition_m6_970(L_5, /*hidden argument*/NULL);
		bool L_7 = Rect_Contains_m6_286((&___position), L_6, /*hidden argument*/NULL);
		if (!L_7)
		{
			goto IL_00b6;
		}
	}
	{
		int32_t L_8 = ___id;
		IL2CPP_RUNTIME_CLASS_INIT(GUIUtility_t6_185_il2cpp_TypeInfo_var);
		GUIUtility_set_hotControl_m6_1371(NULL /*static, unused*/, L_8, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t6_160_il2cpp_TypeInfo_var);
		int32_t L_9 = ((GUI_t6_160_StaticFields*)GUI_t6_160_il2cpp_TypeInfo_var->static_fields)->___s_HotTextField_2;
		if ((((int32_t)L_9) == ((int32_t)(-1))))
		{
			goto IL_006e;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t6_160_il2cpp_TypeInfo_var);
		int32_t L_10 = ((GUI_t6_160_StaticFields*)GUI_t6_160_il2cpp_TypeInfo_var->static_fields)->___s_HotTextField_2;
		int32_t L_11 = ___id;
		if ((((int32_t)L_10) == ((int32_t)L_11)))
		{
			goto IL_006e;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_12 = Type_GetTypeFromHandle_m1_893(NULL /*static, unused*/, LoadTypeToken(TextEditor_t6_238_0_0_0_var), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t6_160_il2cpp_TypeInfo_var);
		int32_t L_13 = ((GUI_t6_160_StaticFields*)GUI_t6_160_il2cpp_TypeInfo_var->static_fields)->___s_HotTextField_2;
		IL2CPP_RUNTIME_CLASS_INIT(GUIUtility_t6_185_il2cpp_TypeInfo_var);
		Object_t * L_14 = GUIUtility_GetStateObject_m6_1369(NULL /*static, unused*/, L_12, L_13, /*hidden argument*/NULL);
		V_1 = ((TextEditor_t6_238 *)CastclassClass(L_14, TextEditor_t6_238_il2cpp_TypeInfo_var));
		TextEditor_t6_238 * L_15 = V_1;
		NullCheck(L_15);
		L_15->___keyboardOnScreen_0 = (TouchScreenKeyboard_t6_45 *)NULL;
	}

IL_006e:
	{
		int32_t L_16 = ___id;
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t6_160_il2cpp_TypeInfo_var);
		((GUI_t6_160_StaticFields*)GUI_t6_160_il2cpp_TypeInfo_var->static_fields)->___s_HotTextField_2 = L_16;
		IL2CPP_RUNTIME_CLASS_INIT(GUIUtility_t6_185_il2cpp_TypeInfo_var);
		int32_t L_17 = GUIUtility_get_keyboardControl_m6_1383(NULL /*static, unused*/, /*hidden argument*/NULL);
		int32_t L_18 = ___id;
		if ((((int32_t)L_17) == ((int32_t)L_18)))
		{
			goto IL_0085;
		}
	}
	{
		int32_t L_19 = ___id;
		IL2CPP_RUNTIME_CLASS_INIT(GUIUtility_t6_185_il2cpp_TypeInfo_var);
		GUIUtility_set_keyboardControl_m6_1384(NULL /*static, unused*/, L_19, /*hidden argument*/NULL);
	}

IL_0085:
	{
		TextEditor_t6_238 * L_20 = ___editor;
		String_t* L_21 = ___secureText;
		G_B10_0 = L_20;
		if (!L_21)
		{
			G_B11_0 = L_20;
			goto IL_0095;
		}
	}
	{
		String_t* L_22 = ___secureText;
		G_B12_0 = L_22;
		G_B12_1 = G_B10_0;
		goto IL_009b;
	}

IL_0095:
	{
		GUIContent_t6_163 * L_23 = ___content;
		NullCheck(L_23);
		String_t* L_24 = GUIContent_get_text_m6_1103(L_23, /*hidden argument*/NULL);
		G_B12_0 = L_24;
		G_B12_1 = G_B11_0;
	}

IL_009b:
	{
		bool L_25 = ___multiline;
		String_t* L_26 = ___secureText;
		TouchScreenKeyboard_t6_45 * L_27 = TouchScreenKeyboard_Open_m6_155(NULL /*static, unused*/, G_B12_0, 0, 1, L_25, ((((int32_t)((((Object_t*)(String_t*)L_26) == ((Object_t*)(Object_t *)NULL))? 1 : 0)) == ((int32_t)0))? 1 : 0), /*hidden argument*/NULL);
		NullCheck(G_B12_1);
		G_B12_1->___keyboardOnScreen_0 = L_27;
		Event_t6_154 * L_28 = V_0;
		NullCheck(L_28);
		Event_Use_m6_1027(L_28, /*hidden argument*/NULL);
	}

IL_00b6:
	{
		goto IL_0159;
	}

IL_00bb:
	{
		TextEditor_t6_238 * L_29 = ___editor;
		NullCheck(L_29);
		TouchScreenKeyboard_t6_45 * L_30 = (L_29->___keyboardOnScreen_0);
		if (!L_30)
		{
			goto IL_0126;
		}
	}
	{
		GUIContent_t6_163 * L_31 = ___content;
		TextEditor_t6_238 * L_32 = ___editor;
		NullCheck(L_32);
		TouchScreenKeyboard_t6_45 * L_33 = (L_32->___keyboardOnScreen_0);
		NullCheck(L_33);
		String_t* L_34 = TouchScreenKeyboard_get_text_m6_157(L_33, /*hidden argument*/NULL);
		NullCheck(L_31);
		GUIContent_set_text_m6_1104(L_31, L_34, /*hidden argument*/NULL);
		int32_t L_35 = ___maxLength;
		if ((((int32_t)L_35) < ((int32_t)0)))
		{
			goto IL_0107;
		}
	}
	{
		GUIContent_t6_163 * L_36 = ___content;
		NullCheck(L_36);
		String_t* L_37 = GUIContent_get_text_m6_1103(L_36, /*hidden argument*/NULL);
		NullCheck(L_37);
		int32_t L_38 = String_get_Length_m1_428(L_37, /*hidden argument*/NULL);
		int32_t L_39 = ___maxLength;
		if ((((int32_t)L_38) <= ((int32_t)L_39)))
		{
			goto IL_0107;
		}
	}
	{
		GUIContent_t6_163 * L_40 = ___content;
		GUIContent_t6_163 * L_41 = ___content;
		NullCheck(L_41);
		String_t* L_42 = GUIContent_get_text_m6_1103(L_41, /*hidden argument*/NULL);
		int32_t L_43 = ___maxLength;
		NullCheck(L_42);
		String_t* L_44 = String_Substring_m1_352(L_42, 0, L_43, /*hidden argument*/NULL);
		NullCheck(L_40);
		GUIContent_set_text_m6_1104(L_40, L_44, /*hidden argument*/NULL);
	}

IL_0107:
	{
		TextEditor_t6_238 * L_45 = ___editor;
		NullCheck(L_45);
		TouchScreenKeyboard_t6_45 * L_46 = (L_45->___keyboardOnScreen_0);
		NullCheck(L_46);
		bool L_47 = TouchScreenKeyboard_get_done_m6_158(L_46, /*hidden argument*/NULL);
		if (!L_47)
		{
			goto IL_0126;
		}
	}
	{
		TextEditor_t6_238 * L_48 = ___editor;
		NullCheck(L_48);
		L_48->___keyboardOnScreen_0 = (TouchScreenKeyboard_t6_45 *)NULL;
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t6_160_il2cpp_TypeInfo_var);
		GUI_set_changed_m6_1081(NULL /*static, unused*/, 1, /*hidden argument*/NULL);
	}

IL_0126:
	{
		GUIContent_t6_163 * L_49 = ___content;
		NullCheck(L_49);
		String_t* L_50 = GUIContent_get_text_m6_1103(L_49, /*hidden argument*/NULL);
		V_2 = L_50;
		String_t* L_51 = ___secureText;
		if (!L_51)
		{
			goto IL_0142;
		}
	}
	{
		GUIContent_t6_163 * L_52 = ___content;
		String_t* L_53 = V_2;
		uint16_t L_54 = ___maskChar;
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t6_160_il2cpp_TypeInfo_var);
		String_t* L_55 = GUI_PasswordFieldGetStrToShow_m6_1052(NULL /*static, unused*/, L_53, L_54, /*hidden argument*/NULL);
		NullCheck(L_52);
		GUIContent_set_text_m6_1104(L_52, L_55, /*hidden argument*/NULL);
	}

IL_0142:
	{
		GUIStyle_t6_166 * L_56 = ___style;
		Rect_t6_52  L_57 = ___position;
		GUIContent_t6_163 * L_58 = ___content;
		int32_t L_59 = ___id;
		NullCheck(L_56);
		GUIStyle_Draw_m6_1312(L_56, L_57, L_58, L_59, 0, /*hidden argument*/NULL);
		GUIContent_t6_163 * L_60 = ___content;
		String_t* L_61 = V_2;
		NullCheck(L_60);
		GUIContent_set_text_m6_1104(L_60, L_61, /*hidden argument*/NULL);
		goto IL_0159;
	}

IL_0159:
	{
		return;
	}
}
// System.Void UnityEngine.GUI::HandleTextFieldEventForDesktop(UnityEngine.Rect,System.Int32,UnityEngine.GUIContent,System.Boolean,System.Int32,UnityEngine.GUIStyle,UnityEngine.TextEditor)
extern TypeInfo* GUIUtility_t6_185_il2cpp_TypeInfo_var;
extern TypeInfo* GUI_t6_160_il2cpp_TypeInfo_var;
extern TypeInfo* Input_t6_83_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern "C" void GUI_HandleTextFieldEventForDesktop_m6_1057 (Object_t * __this /* static, unused */, Rect_t6_52  ___position, int32_t ___id, GUIContent_t6_163 * ___content, bool ___multiline, int32_t ___maxLength, GUIStyle_t6_166 * ___style, TextEditor_t6_238 * ___editor, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GUIUtility_t6_185_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(984);
		GUI_t6_160_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(982);
		Input_t6_83_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(956);
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(11);
		s_Il2CppMethodIntialized = true;
	}
	Event_t6_154 * V_0 = {0};
	bool V_1 = false;
	uint16_t V_2 = 0x0;
	Font_t6_148 * V_3 = {0};
	int32_t V_4 = {0};
	{
		Event_t6_154 * L_0 = Event_get_current_m6_989(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_0 = L_0;
		V_1 = 0;
		Event_t6_154 * L_1 = V_0;
		NullCheck(L_1);
		int32_t L_2 = Event_get_type_m6_1003(L_1, /*hidden argument*/NULL);
		V_4 = L_2;
		int32_t L_3 = V_4;
		if (L_3 == 0)
		{
			goto IL_003c;
		}
		if (L_3 == 1)
		{
			goto IL_013c;
		}
		if (L_3 == 2)
		{
			goto IL_0271;
		}
		if (L_3 == 3)
		{
			goto IL_00f4;
		}
		if (L_3 == 4)
		{
			goto IL_0160;
		}
		if (L_3 == 5)
		{
			goto IL_0271;
		}
		if (L_3 == 6)
		{
			goto IL_0271;
		}
		if (L_3 == 7)
		{
			goto IL_0244;
		}
	}
	{
		goto IL_0271;
	}

IL_003c:
	{
		Event_t6_154 * L_4 = V_0;
		NullCheck(L_4);
		Vector2_t6_48  L_5 = Event_get_mousePosition_m6_970(L_4, /*hidden argument*/NULL);
		bool L_6 = Rect_Contains_m6_286((&___position), L_5, /*hidden argument*/NULL);
		if (!L_6)
		{
			goto IL_00ef;
		}
	}
	{
		int32_t L_7 = ___id;
		IL2CPP_RUNTIME_CLASS_INIT(GUIUtility_t6_185_il2cpp_TypeInfo_var);
		GUIUtility_set_hotControl_m6_1371(NULL /*static, unused*/, L_7, /*hidden argument*/NULL);
		int32_t L_8 = ___id;
		GUIUtility_set_keyboardControl_m6_1384(NULL /*static, unused*/, L_8, /*hidden argument*/NULL);
		TextEditor_t6_238 * L_9 = ___editor;
		NullCheck(L_9);
		L_9->___m_HasFocus_7 = 1;
		TextEditor_t6_238 * L_10 = ___editor;
		Event_t6_154 * L_11 = Event_get_current_m6_989(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_11);
		Vector2_t6_48  L_12 = Event_get_mousePosition_m6_970(L_11, /*hidden argument*/NULL);
		NullCheck(L_10);
		TextEditor_MoveCursorToPosition_m6_1572(L_10, L_12, /*hidden argument*/NULL);
		Event_t6_154 * L_13 = Event_get_current_m6_989(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_13);
		int32_t L_14 = Event_get_clickCount_m6_1018(L_13, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_14) == ((uint32_t)2))))
		{
			goto IL_00ae;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t6_160_il2cpp_TypeInfo_var);
		GUISkin_t6_161 * L_15 = GUI_get_skin_m6_1041(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_15);
		GUISettings_t6_177 * L_16 = GUISkin_get_settings_m6_1261(L_15, /*hidden argument*/NULL);
		NullCheck(L_16);
		bool L_17 = GUISettings_get_doubleClickSelectsWord_m6_1205(L_16, /*hidden argument*/NULL);
		if (!L_17)
		{
			goto IL_00ae;
		}
	}
	{
		TextEditor_t6_238 * L_18 = ___editor;
		NullCheck(L_18);
		TextEditor_SelectCurrentWord_m6_1604(L_18, /*hidden argument*/NULL);
		TextEditor_t6_238 * L_19 = ___editor;
		NullCheck(L_19);
		TextEditor_DblClickSnap_m6_1581(L_19, 0, /*hidden argument*/NULL);
		TextEditor_t6_238 * L_20 = ___editor;
		NullCheck(L_20);
		TextEditor_MouseDragSelectsWholeWords_m6_1580(L_20, 1, /*hidden argument*/NULL);
	}

IL_00ae:
	{
		Event_t6_154 * L_21 = Event_get_current_m6_989(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_21);
		int32_t L_22 = Event_get_clickCount_m6_1018(L_21, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_22) == ((uint32_t)3))))
		{
			goto IL_00e9;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t6_160_il2cpp_TypeInfo_var);
		GUISkin_t6_161 * L_23 = GUI_get_skin_m6_1041(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_23);
		GUISettings_t6_177 * L_24 = GUISkin_get_settings_m6_1261(L_23, /*hidden argument*/NULL);
		NullCheck(L_24);
		bool L_25 = GUISettings_get_tripleClickSelectsLine_m6_1206(L_24, /*hidden argument*/NULL);
		if (!L_25)
		{
			goto IL_00e9;
		}
	}
	{
		TextEditor_t6_238 * L_26 = ___editor;
		NullCheck(L_26);
		TextEditor_SelectCurrentParagraph_m6_1606(L_26, /*hidden argument*/NULL);
		TextEditor_t6_238 * L_27 = ___editor;
		NullCheck(L_27);
		TextEditor_MouseDragSelectsWholeWords_m6_1580(L_27, 1, /*hidden argument*/NULL);
		TextEditor_t6_238 * L_28 = ___editor;
		NullCheck(L_28);
		TextEditor_DblClickSnap_m6_1581(L_28, 1, /*hidden argument*/NULL);
	}

IL_00e9:
	{
		Event_t6_154 * L_29 = V_0;
		NullCheck(L_29);
		Event_Use_m6_1027(L_29, /*hidden argument*/NULL);
	}

IL_00ef:
	{
		goto IL_0271;
	}

IL_00f4:
	{
		IL2CPP_RUNTIME_CLASS_INIT(GUIUtility_t6_185_il2cpp_TypeInfo_var);
		int32_t L_30 = GUIUtility_get_hotControl_m6_1370(NULL /*static, unused*/, /*hidden argument*/NULL);
		int32_t L_31 = ___id;
		if ((!(((uint32_t)L_30) == ((uint32_t)L_31))))
		{
			goto IL_0137;
		}
	}
	{
		Event_t6_154 * L_32 = V_0;
		NullCheck(L_32);
		bool L_33 = Event_get_shift_m6_976(L_32, /*hidden argument*/NULL);
		if (!L_33)
		{
			goto IL_0120;
		}
	}
	{
		TextEditor_t6_238 * L_34 = ___editor;
		Event_t6_154 * L_35 = Event_get_current_m6_989(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_35);
		Vector2_t6_48  L_36 = Event_get_mousePosition_m6_970(L_35, /*hidden argument*/NULL);
		NullCheck(L_34);
		TextEditor_MoveCursorToPosition_m6_1572(L_34, L_36, /*hidden argument*/NULL);
		goto IL_0131;
	}

IL_0120:
	{
		TextEditor_t6_238 * L_37 = ___editor;
		Event_t6_154 * L_38 = Event_get_current_m6_989(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_38);
		Vector2_t6_48  L_39 = Event_get_mousePosition_m6_970(L_38, /*hidden argument*/NULL);
		NullCheck(L_37);
		TextEditor_SelectToPosition_m6_1573(L_37, L_39, /*hidden argument*/NULL);
	}

IL_0131:
	{
		Event_t6_154 * L_40 = V_0;
		NullCheck(L_40);
		Event_Use_m6_1027(L_40, /*hidden argument*/NULL);
	}

IL_0137:
	{
		goto IL_0271;
	}

IL_013c:
	{
		IL2CPP_RUNTIME_CLASS_INIT(GUIUtility_t6_185_il2cpp_TypeInfo_var);
		int32_t L_41 = GUIUtility_get_hotControl_m6_1370(NULL /*static, unused*/, /*hidden argument*/NULL);
		int32_t L_42 = ___id;
		if ((!(((uint32_t)L_41) == ((uint32_t)L_42))))
		{
			goto IL_015b;
		}
	}
	{
		TextEditor_t6_238 * L_43 = ___editor;
		NullCheck(L_43);
		TextEditor_MouseDragSelectsWholeWords_m6_1580(L_43, 0, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(GUIUtility_t6_185_il2cpp_TypeInfo_var);
		GUIUtility_set_hotControl_m6_1371(NULL /*static, unused*/, 0, /*hidden argument*/NULL);
		Event_t6_154 * L_44 = V_0;
		NullCheck(L_44);
		Event_Use_m6_1027(L_44, /*hidden argument*/NULL);
	}

IL_015b:
	{
		goto IL_0271;
	}

IL_0160:
	{
		IL2CPP_RUNTIME_CLASS_INIT(GUIUtility_t6_185_il2cpp_TypeInfo_var);
		int32_t L_45 = GUIUtility_get_keyboardControl_m6_1383(NULL /*static, unused*/, /*hidden argument*/NULL);
		int32_t L_46 = ___id;
		if ((((int32_t)L_45) == ((int32_t)L_46)))
		{
			goto IL_016c;
		}
	}
	{
		return;
	}

IL_016c:
	{
		TextEditor_t6_238 * L_47 = ___editor;
		Event_t6_154 * L_48 = V_0;
		NullCheck(L_47);
		bool L_49 = TextEditor_HandleKeyEvent_m6_1547(L_47, L_48, /*hidden argument*/NULL);
		if (!L_49)
		{
			goto IL_0198;
		}
	}
	{
		Event_t6_154 * L_50 = V_0;
		NullCheck(L_50);
		Event_Use_m6_1027(L_50, /*hidden argument*/NULL);
		V_1 = 1;
		GUIContent_t6_163 * L_51 = ___content;
		TextEditor_t6_238 * L_52 = ___editor;
		NullCheck(L_52);
		GUIContent_t6_163 * L_53 = (L_52->___content_2);
		NullCheck(L_53);
		String_t* L_54 = GUIContent_get_text_m6_1103(L_53, /*hidden argument*/NULL);
		NullCheck(L_51);
		GUIContent_set_text_m6_1104(L_51, L_54, /*hidden argument*/NULL);
		goto IL_0271;
	}

IL_0198:
	{
		Event_t6_154 * L_55 = V_0;
		NullCheck(L_55);
		int32_t L_56 = Event_get_keyCode_m6_1024(L_55, /*hidden argument*/NULL);
		if ((((int32_t)L_56) == ((int32_t)((int32_t)9))))
		{
			goto IL_01b2;
		}
	}
	{
		Event_t6_154 * L_57 = V_0;
		NullCheck(L_57);
		uint16_t L_58 = Event_get_character_m6_1020(L_57, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_58) == ((uint32_t)((int32_t)9)))))
		{
			goto IL_01b3;
		}
	}

IL_01b2:
	{
		return;
	}

IL_01b3:
	{
		Event_t6_154 * L_59 = V_0;
		NullCheck(L_59);
		uint16_t L_60 = Event_get_character_m6_1020(L_59, /*hidden argument*/NULL);
		V_2 = L_60;
		uint16_t L_61 = V_2;
		if ((!(((uint32_t)L_61) == ((uint32_t)((int32_t)10)))))
		{
			goto IL_01d4;
		}
	}
	{
		bool L_62 = ___multiline;
		if (L_62)
		{
			goto IL_01d4;
		}
	}
	{
		Event_t6_154 * L_63 = V_0;
		NullCheck(L_63);
		bool L_64 = Event_get_alt_m6_980(L_63, /*hidden argument*/NULL);
		if (L_64)
		{
			goto IL_01d4;
		}
	}
	{
		return;
	}

IL_01d4:
	{
		GUIStyle_t6_166 * L_65 = ___style;
		NullCheck(L_65);
		Font_t6_148 * L_66 = GUIStyle_get_font_m6_1306(L_65, /*hidden argument*/NULL);
		V_3 = L_66;
		Font_t6_148 * L_67 = V_3;
		bool L_68 = Object_op_Implicit_m6_578(NULL /*static, unused*/, L_67, /*hidden argument*/NULL);
		if (L_68)
		{
			goto IL_01f2;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t6_160_il2cpp_TypeInfo_var);
		GUISkin_t6_161 * L_69 = GUI_get_skin_m6_1041(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_69);
		Font_t6_148 * L_70 = GUISkin_get_font_m6_1217(L_69, /*hidden argument*/NULL);
		V_3 = L_70;
	}

IL_01f2:
	{
		Font_t6_148 * L_71 = V_3;
		uint16_t L_72 = V_2;
		NullCheck(L_71);
		bool L_73 = Font_HasCharacter_m6_912(L_71, L_72, /*hidden argument*/NULL);
		if (L_73)
		{
			goto IL_0206;
		}
	}
	{
		uint16_t L_74 = V_2;
		if ((!(((uint32_t)L_74) == ((uint32_t)((int32_t)10)))))
		{
			goto IL_0215;
		}
	}

IL_0206:
	{
		TextEditor_t6_238 * L_75 = ___editor;
		uint16_t L_76 = V_2;
		NullCheck(L_75);
		TextEditor_Insert_m6_1558(L_75, L_76, /*hidden argument*/NULL);
		V_1 = 1;
		goto IL_0271;
	}

IL_0215:
	{
		uint16_t L_77 = V_2;
		if (L_77)
		{
			goto IL_023f;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t6_83_il2cpp_TypeInfo_var);
		String_t* L_78 = Input_get_compositionString_m6_550(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_78);
		int32_t L_79 = String_get_Length_m1_428(L_78, /*hidden argument*/NULL);
		if ((((int32_t)L_79) <= ((int32_t)0)))
		{
			goto IL_0239;
		}
	}
	{
		TextEditor_t6_238 * L_80 = ___editor;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_81 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->___Empty_2;
		NullCheck(L_80);
		TextEditor_ReplaceSelection_m6_1557(L_80, L_81, /*hidden argument*/NULL);
		V_1 = 1;
	}

IL_0239:
	{
		Event_t6_154 * L_82 = V_0;
		NullCheck(L_82);
		Event_Use_m6_1027(L_82, /*hidden argument*/NULL);
	}

IL_023f:
	{
		goto IL_0271;
	}

IL_0244:
	{
		IL2CPP_RUNTIME_CLASS_INIT(GUIUtility_t6_185_il2cpp_TypeInfo_var);
		int32_t L_83 = GUIUtility_get_keyboardControl_m6_1383(NULL /*static, unused*/, /*hidden argument*/NULL);
		int32_t L_84 = ___id;
		if ((((int32_t)L_83) == ((int32_t)L_84)))
		{
			goto IL_025f;
		}
	}
	{
		GUIStyle_t6_166 * L_85 = ___style;
		Rect_t6_52  L_86 = ___position;
		GUIContent_t6_163 * L_87 = ___content;
		int32_t L_88 = ___id;
		NullCheck(L_85);
		GUIStyle_Draw_m6_1312(L_85, L_86, L_87, L_88, 0, /*hidden argument*/NULL);
		goto IL_026c;
	}

IL_025f:
	{
		TextEditor_t6_238 * L_89 = ___editor;
		GUIContent_t6_163 * L_90 = ___content;
		NullCheck(L_90);
		String_t* L_91 = GUIContent_get_text_m6_1103(L_90, /*hidden argument*/NULL);
		NullCheck(L_89);
		TextEditor_DrawCursor_m6_1609(L_89, L_91, /*hidden argument*/NULL);
	}

IL_026c:
	{
		goto IL_0271;
	}

IL_0271:
	{
		IL2CPP_RUNTIME_CLASS_INIT(GUIUtility_t6_185_il2cpp_TypeInfo_var);
		int32_t L_92 = GUIUtility_get_keyboardControl_m6_1383(NULL /*static, unused*/, /*hidden argument*/NULL);
		int32_t L_93 = ___id;
		if ((!(((uint32_t)L_92) == ((uint32_t)L_93))))
		{
			goto IL_0282;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GUIUtility_t6_185_il2cpp_TypeInfo_var);
		GUIUtility_set_textFieldInput_m6_1392(NULL /*static, unused*/, 1, /*hidden argument*/NULL);
	}

IL_0282:
	{
		bool L_94 = V_1;
		if (!L_94)
		{
			goto IL_02d4;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t6_160_il2cpp_TypeInfo_var);
		GUI_set_changed_m6_1081(NULL /*static, unused*/, 1, /*hidden argument*/NULL);
		GUIContent_t6_163 * L_95 = ___content;
		TextEditor_t6_238 * L_96 = ___editor;
		NullCheck(L_96);
		GUIContent_t6_163 * L_97 = (L_96->___content_2);
		NullCheck(L_97);
		String_t* L_98 = GUIContent_get_text_m6_1103(L_97, /*hidden argument*/NULL);
		NullCheck(L_95);
		GUIContent_set_text_m6_1104(L_95, L_98, /*hidden argument*/NULL);
		int32_t L_99 = ___maxLength;
		if ((((int32_t)L_99) < ((int32_t)0)))
		{
			goto IL_02ce;
		}
	}
	{
		GUIContent_t6_163 * L_100 = ___content;
		NullCheck(L_100);
		String_t* L_101 = GUIContent_get_text_m6_1103(L_100, /*hidden argument*/NULL);
		NullCheck(L_101);
		int32_t L_102 = String_get_Length_m1_428(L_101, /*hidden argument*/NULL);
		int32_t L_103 = ___maxLength;
		if ((((int32_t)L_102) <= ((int32_t)L_103)))
		{
			goto IL_02ce;
		}
	}
	{
		GUIContent_t6_163 * L_104 = ___content;
		GUIContent_t6_163 * L_105 = ___content;
		NullCheck(L_105);
		String_t* L_106 = GUIContent_get_text_m6_1103(L_105, /*hidden argument*/NULL);
		int32_t L_107 = ___maxLength;
		NullCheck(L_106);
		String_t* L_108 = String_Substring_m1_352(L_106, 0, L_107, /*hidden argument*/NULL);
		NullCheck(L_104);
		GUIContent_set_text_m6_1104(L_104, L_108, /*hidden argument*/NULL);
	}

IL_02ce:
	{
		Event_t6_154 * L_109 = V_0;
		NullCheck(L_109);
		Event_Use_m6_1027(L_109, /*hidden argument*/NULL);
	}

IL_02d4:
	{
		return;
	}
}
// System.Boolean UnityEngine.GUI::Toggle(UnityEngine.Rect,System.Boolean,UnityEngine.GUIContent,UnityEngine.GUIStyle)
extern TypeInfo* GUIUtility_t6_185_il2cpp_TypeInfo_var;
extern TypeInfo* GUI_t6_160_il2cpp_TypeInfo_var;
extern "C" bool GUI_Toggle_m6_1058 (Object_t * __this /* static, unused */, Rect_t6_52  ___position, bool ___value, GUIContent_t6_163 * ___content, GUIStyle_t6_166 * ___style, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GUIUtility_t6_185_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(984);
		GUI_t6_160_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(982);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GUIUtility_t6_185_il2cpp_TypeInfo_var);
		GUIUtility_CheckOnGUI_m6_1376(NULL /*static, unused*/, /*hidden argument*/NULL);
		Rect_t6_52  L_0 = ___position;
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t6_160_il2cpp_TypeInfo_var);
		int32_t L_1 = ((GUI_t6_160_StaticFields*)GUI_t6_160_il2cpp_TypeInfo_var->static_fields)->___s_ToggleHash_5;
		Rect_t6_52  L_2 = ___position;
		int32_t L_3 = GUIUtility_GetControlID_m6_1368(NULL /*static, unused*/, L_1, 0, L_2, /*hidden argument*/NULL);
		bool L_4 = ___value;
		GUIContent_t6_163 * L_5 = ___content;
		GUIStyle_t6_166 * L_6 = ___style;
		NullCheck(L_6);
		IntPtr_t L_7 = (L_6->___m_Ptr_0);
		bool L_8 = GUI_DoToggle_m6_1091(NULL /*static, unused*/, L_0, L_3, L_4, L_5, L_7, /*hidden argument*/NULL);
		return L_8;
	}
}
// System.Int32 UnityEngine.GUI::Toolbar(UnityEngine.Rect,System.Int32,UnityEngine.GUIContent[],UnityEngine.GUIStyle)
extern TypeInfo* GUIUtility_t6_185_il2cpp_TypeInfo_var;
extern TypeInfo* GUI_t6_160_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2735;
extern Il2CppCodeGenString* _stringLiteral2779;
extern Il2CppCodeGenString* _stringLiteral2736;
extern "C" int32_t GUI_Toolbar_m6_1059 (Object_t * __this /* static, unused */, Rect_t6_52  ___position, int32_t ___selected, GUIContentU5BU5D_t6_264* ___contents, GUIStyle_t6_166 * ___style, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GUIUtility_t6_185_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(984);
		GUI_t6_160_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(982);
		_stringLiteral2735 = il2cpp_codegen_string_literal_from_index(2735);
		_stringLiteral2779 = il2cpp_codegen_string_literal_from_index(2779);
		_stringLiteral2736 = il2cpp_codegen_string_literal_from_index(2736);
		s_Il2CppMethodIntialized = true;
	}
	GUIStyle_t6_166 * V_0 = {0};
	GUIStyle_t6_166 * V_1 = {0};
	GUIStyle_t6_166 * V_2 = {0};
	{
		IL2CPP_RUNTIME_CLASS_INIT(GUIUtility_t6_185_il2cpp_TypeInfo_var);
		GUIUtility_CheckOnGUI_m6_1376(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t6_160_il2cpp_TypeInfo_var);
		GUI_FindStyles_m6_1060(NULL /*static, unused*/, (&___style), (&V_0), (&V_1), (&V_2), _stringLiteral2735, _stringLiteral2779, _stringLiteral2736, /*hidden argument*/NULL);
		Rect_t6_52  L_0 = ___position;
		int32_t L_1 = ___selected;
		GUIContentU5BU5D_t6_264* L_2 = ___contents;
		GUIContentU5BU5D_t6_264* L_3 = ___contents;
		NullCheck(L_3);
		GUIStyle_t6_166 * L_4 = ___style;
		GUIStyle_t6_166 * L_5 = V_0;
		GUIStyle_t6_166 * L_6 = V_1;
		GUIStyle_t6_166 * L_7 = V_2;
		int32_t L_8 = GUI_DoButtonGrid_m6_1062(NULL /*static, unused*/, L_0, L_1, L_2, (((int32_t)((int32_t)(((Array_t *)L_3)->max_length)))), L_4, L_5, L_6, L_7, /*hidden argument*/NULL);
		return L_8;
	}
}
// System.Void UnityEngine.GUI::FindStyles(UnityEngine.GUIStyle&,UnityEngine.GUIStyle&,UnityEngine.GUIStyle&,UnityEngine.GUIStyle&,System.String,System.String,System.String)
extern TypeInfo* GUI_t6_160_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern "C" void GUI_FindStyles_m6_1060 (Object_t * __this /* static, unused */, GUIStyle_t6_166 ** ___style, GUIStyle_t6_166 ** ___firstStyle, GUIStyle_t6_166 ** ___midStyle, GUIStyle_t6_166 ** ___lastStyle, String_t* ___first, String_t* ___mid, String_t* ___last, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GUI_t6_160_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(982);
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(11);
		s_Il2CppMethodIntialized = true;
	}
	String_t* V_0 = {0};
	{
		GUIStyle_t6_166 ** L_0 = ___style;
		if ((*((GUIStyle_t6_166 **)L_0)))
		{
			goto IL_0013;
		}
	}
	{
		GUIStyle_t6_166 ** L_1 = ___style;
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t6_160_il2cpp_TypeInfo_var);
		GUISkin_t6_161 * L_2 = GUI_get_skin_m6_1041(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_2);
		GUIStyle_t6_166 * L_3 = GUISkin_get_button_m6_1227(L_2, /*hidden argument*/NULL);
		*((Object_t **)(L_1)) = (Object_t *)L_3;
	}

IL_0013:
	{
		GUIStyle_t6_166 ** L_4 = ___style;
		NullCheck((*((GUIStyle_t6_166 **)L_4)));
		String_t* L_5 = GUIStyle_get_name_m6_1327((*((GUIStyle_t6_166 **)L_4)), /*hidden argument*/NULL);
		V_0 = L_5;
		GUIStyle_t6_166 ** L_6 = ___midStyle;
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t6_160_il2cpp_TypeInfo_var);
		GUISkin_t6_161 * L_7 = GUI_get_skin_m6_1041(NULL /*static, unused*/, /*hidden argument*/NULL);
		String_t* L_8 = V_0;
		String_t* L_9 = ___mid;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_10 = String_Concat_m1_418(NULL /*static, unused*/, L_8, L_9, /*hidden argument*/NULL);
		NullCheck(L_7);
		GUIStyle_t6_166 * L_11 = GUISkin_FindStyle_m6_1266(L_7, L_10, /*hidden argument*/NULL);
		*((Object_t **)(L_6)) = (Object_t *)L_11;
		GUIStyle_t6_166 ** L_12 = ___midStyle;
		if ((*((GUIStyle_t6_166 **)L_12)))
		{
			goto IL_003a;
		}
	}
	{
		GUIStyle_t6_166 ** L_13 = ___midStyle;
		GUIStyle_t6_166 ** L_14 = ___style;
		*((Object_t **)(L_13)) = (Object_t *)(*((GUIStyle_t6_166 **)L_14));
	}

IL_003a:
	{
		GUIStyle_t6_166 ** L_15 = ___firstStyle;
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t6_160_il2cpp_TypeInfo_var);
		GUISkin_t6_161 * L_16 = GUI_get_skin_m6_1041(NULL /*static, unused*/, /*hidden argument*/NULL);
		String_t* L_17 = V_0;
		String_t* L_18 = ___first;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_19 = String_Concat_m1_418(NULL /*static, unused*/, L_17, L_18, /*hidden argument*/NULL);
		NullCheck(L_16);
		GUIStyle_t6_166 * L_20 = GUISkin_FindStyle_m6_1266(L_16, L_19, /*hidden argument*/NULL);
		*((Object_t **)(L_15)) = (Object_t *)L_20;
		GUIStyle_t6_166 ** L_21 = ___firstStyle;
		if ((*((GUIStyle_t6_166 **)L_21)))
		{
			goto IL_0059;
		}
	}
	{
		GUIStyle_t6_166 ** L_22 = ___firstStyle;
		GUIStyle_t6_166 ** L_23 = ___midStyle;
		*((Object_t **)(L_22)) = (Object_t *)(*((GUIStyle_t6_166 **)L_23));
	}

IL_0059:
	{
		GUIStyle_t6_166 ** L_24 = ___lastStyle;
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t6_160_il2cpp_TypeInfo_var);
		GUISkin_t6_161 * L_25 = GUI_get_skin_m6_1041(NULL /*static, unused*/, /*hidden argument*/NULL);
		String_t* L_26 = V_0;
		String_t* L_27 = ___last;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_28 = String_Concat_m1_418(NULL /*static, unused*/, L_26, L_27, /*hidden argument*/NULL);
		NullCheck(L_25);
		GUIStyle_t6_166 * L_29 = GUISkin_FindStyle_m6_1266(L_25, L_28, /*hidden argument*/NULL);
		*((Object_t **)(L_24)) = (Object_t *)L_29;
		GUIStyle_t6_166 ** L_30 = ___lastStyle;
		if ((*((GUIStyle_t6_166 **)L_30)))
		{
			goto IL_0078;
		}
	}
	{
		GUIStyle_t6_166 ** L_31 = ___lastStyle;
		GUIStyle_t6_166 ** L_32 = ___midStyle;
		*((Object_t **)(L_31)) = (Object_t *)(*((GUIStyle_t6_166 **)L_32));
	}

IL_0078:
	{
		return;
	}
}
// System.Int32 UnityEngine.GUI::CalcTotalHorizSpacing(System.Int32,UnityEngine.GUIStyle,UnityEngine.GUIStyle,UnityEngine.GUIStyle,UnityEngine.GUIStyle)
extern TypeInfo* Mathf_t6_58_il2cpp_TypeInfo_var;
extern "C" int32_t GUI_CalcTotalHorizSpacing_m6_1061 (Object_t * __this /* static, unused */, int32_t ___xCount, GUIStyle_t6_166 * ___style, GUIStyle_t6_166 * ___firstStyle, GUIStyle_t6_166 * ___midStyle, GUIStyle_t6_166 * ___lastStyle, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Mathf_t6_58_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(940);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	{
		int32_t L_0 = ___xCount;
		if ((((int32_t)L_0) >= ((int32_t)2)))
		{
			goto IL_0009;
		}
	}
	{
		return 0;
	}

IL_0009:
	{
		int32_t L_1 = ___xCount;
		if ((!(((uint32_t)L_1) == ((uint32_t)2))))
		{
			goto IL_002d;
		}
	}
	{
		GUIStyle_t6_166 * L_2 = ___firstStyle;
		NullCheck(L_2);
		RectOffset_t6_172 * L_3 = GUIStyle_get_margin_m6_1303(L_2, /*hidden argument*/NULL);
		NullCheck(L_3);
		int32_t L_4 = RectOffset_get_right_m6_1286(L_3, /*hidden argument*/NULL);
		GUIStyle_t6_166 * L_5 = ___lastStyle;
		NullCheck(L_5);
		RectOffset_t6_172 * L_6 = GUIStyle_get_margin_m6_1303(L_5, /*hidden argument*/NULL);
		NullCheck(L_6);
		int32_t L_7 = RectOffset_get_left_m6_1284(L_6, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t6_58_il2cpp_TypeInfo_var);
		int32_t L_8 = Mathf_Max_m6_384(NULL /*static, unused*/, L_4, L_7, /*hidden argument*/NULL);
		return L_8;
	}

IL_002d:
	{
		GUIStyle_t6_166 * L_9 = ___midStyle;
		NullCheck(L_9);
		RectOffset_t6_172 * L_10 = GUIStyle_get_margin_m6_1303(L_9, /*hidden argument*/NULL);
		NullCheck(L_10);
		int32_t L_11 = RectOffset_get_left_m6_1284(L_10, /*hidden argument*/NULL);
		GUIStyle_t6_166 * L_12 = ___midStyle;
		NullCheck(L_12);
		RectOffset_t6_172 * L_13 = GUIStyle_get_margin_m6_1303(L_12, /*hidden argument*/NULL);
		NullCheck(L_13);
		int32_t L_14 = RectOffset_get_right_m6_1286(L_13, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t6_58_il2cpp_TypeInfo_var);
		int32_t L_15 = Mathf_Max_m6_384(NULL /*static, unused*/, L_11, L_14, /*hidden argument*/NULL);
		V_0 = L_15;
		GUIStyle_t6_166 * L_16 = ___firstStyle;
		NullCheck(L_16);
		RectOffset_t6_172 * L_17 = GUIStyle_get_margin_m6_1303(L_16, /*hidden argument*/NULL);
		NullCheck(L_17);
		int32_t L_18 = RectOffset_get_right_m6_1286(L_17, /*hidden argument*/NULL);
		GUIStyle_t6_166 * L_19 = ___midStyle;
		NullCheck(L_19);
		RectOffset_t6_172 * L_20 = GUIStyle_get_margin_m6_1303(L_19, /*hidden argument*/NULL);
		NullCheck(L_20);
		int32_t L_21 = RectOffset_get_left_m6_1284(L_20, /*hidden argument*/NULL);
		int32_t L_22 = Mathf_Max_m6_384(NULL /*static, unused*/, L_18, L_21, /*hidden argument*/NULL);
		GUIStyle_t6_166 * L_23 = ___midStyle;
		NullCheck(L_23);
		RectOffset_t6_172 * L_24 = GUIStyle_get_margin_m6_1303(L_23, /*hidden argument*/NULL);
		NullCheck(L_24);
		int32_t L_25 = RectOffset_get_right_m6_1286(L_24, /*hidden argument*/NULL);
		GUIStyle_t6_166 * L_26 = ___lastStyle;
		NullCheck(L_26);
		RectOffset_t6_172 * L_27 = GUIStyle_get_margin_m6_1303(L_26, /*hidden argument*/NULL);
		NullCheck(L_27);
		int32_t L_28 = RectOffset_get_left_m6_1284(L_27, /*hidden argument*/NULL);
		int32_t L_29 = Mathf_Max_m6_384(NULL /*static, unused*/, L_25, L_28, /*hidden argument*/NULL);
		int32_t L_30 = V_0;
		int32_t L_31 = ___xCount;
		return ((int32_t)((int32_t)((int32_t)((int32_t)L_22+(int32_t)L_29))+(int32_t)((int32_t)((int32_t)L_30*(int32_t)((int32_t)((int32_t)L_31-(int32_t)3))))));
	}
}
// System.Int32 UnityEngine.GUI::DoButtonGrid(UnityEngine.Rect,System.Int32,UnityEngine.GUIContent[],System.Int32,UnityEngine.GUIStyle,UnityEngine.GUIStyle,UnityEngine.GUIStyle,UnityEngine.GUIStyle)
extern TypeInfo* GUIUtility_t6_185_il2cpp_TypeInfo_var;
extern TypeInfo* GUI_t6_160_il2cpp_TypeInfo_var;
extern TypeInfo* Mathf_t6_58_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2780;
extern "C" int32_t GUI_DoButtonGrid_m6_1062 (Object_t * __this /* static, unused */, Rect_t6_52  ___position, int32_t ___selected, GUIContentU5BU5D_t6_264* ___contents, int32_t ___xCount, GUIStyle_t6_166 * ___style, GUIStyle_t6_166 * ___firstStyle, GUIStyle_t6_166 * ___midStyle, GUIStyle_t6_166 * ___lastStyle, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GUIUtility_t6_185_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(984);
		GUI_t6_160_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(982);
		Mathf_t6_58_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(940);
		_stringLiteral2780 = il2cpp_codegen_string_literal_from_index(2780);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	float V_3 = 0.0f;
	float V_4 = 0.0f;
	float V_5 = 0.0f;
	float V_6 = 0.0f;
	RectU5BU5D_t6_265* V_7 = {0};
	int32_t V_8 = 0;
	GUIStyle_t6_166 * V_9 = {0};
	int32_t V_10 = 0;
	bool V_11 = false;
	int32_t V_12 = 0;
	GUIStyle_t6_166 * V_13 = {0};
	int32_t V_14 = {0};
	GUIContent_t6_163 * G_B38_0 = {0};
	Rect_t6_52  G_B38_1 = {0};
	GUIStyle_t6_166 * G_B38_2 = {0};
	GUIContent_t6_163 * G_B32_0 = {0};
	Rect_t6_52  G_B32_1 = {0};
	GUIStyle_t6_166 * G_B32_2 = {0};
	GUIContent_t6_163 * G_B34_0 = {0};
	Rect_t6_52  G_B34_1 = {0};
	GUIStyle_t6_166 * G_B34_2 = {0};
	GUIContent_t6_163 * G_B33_0 = {0};
	Rect_t6_52  G_B33_1 = {0};
	GUIStyle_t6_166 * G_B33_2 = {0};
	GUIContent_t6_163 * G_B36_0 = {0};
	Rect_t6_52  G_B36_1 = {0};
	GUIStyle_t6_166 * G_B36_2 = {0};
	GUIContent_t6_163 * G_B35_0 = {0};
	Rect_t6_52  G_B35_1 = {0};
	GUIStyle_t6_166 * G_B35_2 = {0};
	int32_t G_B37_0 = 0;
	GUIContent_t6_163 * G_B37_1 = {0};
	Rect_t6_52  G_B37_2 = {0};
	GUIStyle_t6_166 * G_B37_3 = {0};
	int32_t G_B39_0 = 0;
	GUIContent_t6_163 * G_B39_1 = {0};
	Rect_t6_52  G_B39_2 = {0};
	GUIStyle_t6_166 * G_B39_3 = {0};
	int32_t G_B41_0 = 0;
	GUIContent_t6_163 * G_B41_1 = {0};
	Rect_t6_52  G_B41_2 = {0};
	GUIStyle_t6_166 * G_B41_3 = {0};
	int32_t G_B40_0 = 0;
	GUIContent_t6_163 * G_B40_1 = {0};
	Rect_t6_52  G_B40_2 = {0};
	GUIStyle_t6_166 * G_B40_3 = {0};
	int32_t G_B42_0 = 0;
	int32_t G_B42_1 = 0;
	GUIContent_t6_163 * G_B42_2 = {0};
	Rect_t6_52  G_B42_3 = {0};
	GUIStyle_t6_166 * G_B42_4 = {0};
	GUIContent_t6_163 * G_B55_0 = {0};
	Rect_t6_52  G_B55_1 = {0};
	GUIStyle_t6_166 * G_B55_2 = {0};
	GUIContent_t6_163 * G_B49_0 = {0};
	Rect_t6_52  G_B49_1 = {0};
	GUIStyle_t6_166 * G_B49_2 = {0};
	GUIContent_t6_163 * G_B51_0 = {0};
	Rect_t6_52  G_B51_1 = {0};
	GUIStyle_t6_166 * G_B51_2 = {0};
	GUIContent_t6_163 * G_B50_0 = {0};
	Rect_t6_52  G_B50_1 = {0};
	GUIStyle_t6_166 * G_B50_2 = {0};
	GUIContent_t6_163 * G_B53_0 = {0};
	Rect_t6_52  G_B53_1 = {0};
	GUIStyle_t6_166 * G_B53_2 = {0};
	GUIContent_t6_163 * G_B52_0 = {0};
	Rect_t6_52  G_B52_1 = {0};
	GUIStyle_t6_166 * G_B52_2 = {0};
	int32_t G_B54_0 = 0;
	GUIContent_t6_163 * G_B54_1 = {0};
	Rect_t6_52  G_B54_2 = {0};
	GUIStyle_t6_166 * G_B54_3 = {0};
	int32_t G_B56_0 = 0;
	GUIContent_t6_163 * G_B56_1 = {0};
	Rect_t6_52  G_B56_2 = {0};
	GUIStyle_t6_166 * G_B56_3 = {0};
	{
		IL2CPP_RUNTIME_CLASS_INIT(GUIUtility_t6_185_il2cpp_TypeInfo_var);
		GUIUtility_CheckOnGUI_m6_1376(NULL /*static, unused*/, /*hidden argument*/NULL);
		GUIContentU5BU5D_t6_264* L_0 = ___contents;
		NullCheck(L_0);
		V_0 = (((int32_t)((int32_t)(((Array_t *)L_0)->max_length))));
		int32_t L_1 = V_0;
		if (L_1)
		{
			goto IL_0011;
		}
	}
	{
		int32_t L_2 = ___selected;
		return L_2;
	}

IL_0011:
	{
		int32_t L_3 = ___xCount;
		if ((((int32_t)L_3) > ((int32_t)0)))
		{
			goto IL_0024;
		}
	}
	{
		Debug_LogWarning_m6_491(NULL /*static, unused*/, _stringLiteral2780, /*hidden argument*/NULL);
		int32_t L_4 = ___selected;
		return L_4;
	}

IL_0024:
	{
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t6_160_il2cpp_TypeInfo_var);
		int32_t L_5 = ((GUI_t6_160_StaticFields*)GUI_t6_160_il2cpp_TypeInfo_var->static_fields)->___s_ButtonGridHash_6;
		Rect_t6_52  L_6 = ___position;
		IL2CPP_RUNTIME_CLASS_INIT(GUIUtility_t6_185_il2cpp_TypeInfo_var);
		int32_t L_7 = GUIUtility_GetControlID_m6_1368(NULL /*static, unused*/, L_5, 0, L_6, /*hidden argument*/NULL);
		V_1 = L_7;
		int32_t L_8 = V_0;
		int32_t L_9 = ___xCount;
		V_2 = ((int32_t)((int32_t)L_8/(int32_t)L_9));
		int32_t L_10 = V_0;
		int32_t L_11 = ___xCount;
		if (!((int32_t)((int32_t)L_10%(int32_t)L_11)))
		{
			goto IL_0041;
		}
	}
	{
		int32_t L_12 = V_2;
		V_2 = ((int32_t)((int32_t)L_12+(int32_t)1));
	}

IL_0041:
	{
		int32_t L_13 = ___xCount;
		GUIStyle_t6_166 * L_14 = ___style;
		GUIStyle_t6_166 * L_15 = ___firstStyle;
		GUIStyle_t6_166 * L_16 = ___midStyle;
		GUIStyle_t6_166 * L_17 = ___lastStyle;
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t6_160_il2cpp_TypeInfo_var);
		int32_t L_18 = GUI_CalcTotalHorizSpacing_m6_1061(NULL /*static, unused*/, L_13, L_14, L_15, L_16, L_17, /*hidden argument*/NULL);
		V_3 = (((float)((float)L_18)));
		GUIStyle_t6_166 * L_19 = ___style;
		NullCheck(L_19);
		RectOffset_t6_172 * L_20 = GUIStyle_get_margin_m6_1303(L_19, /*hidden argument*/NULL);
		NullCheck(L_20);
		int32_t L_21 = RectOffset_get_top_m6_1288(L_20, /*hidden argument*/NULL);
		GUIStyle_t6_166 * L_22 = ___style;
		NullCheck(L_22);
		RectOffset_t6_172 * L_23 = GUIStyle_get_margin_m6_1303(L_22, /*hidden argument*/NULL);
		NullCheck(L_23);
		int32_t L_24 = RectOffset_get_bottom_m6_1290(L_23, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t6_58_il2cpp_TypeInfo_var);
		int32_t L_25 = Mathf_Max_m6_384(NULL /*static, unused*/, L_21, L_24, /*hidden argument*/NULL);
		int32_t L_26 = V_2;
		V_4 = (((float)((float)((int32_t)((int32_t)L_25*(int32_t)((int32_t)((int32_t)L_26-(int32_t)1)))))));
		float L_27 = Rect_get_width_m6_277((&___position), /*hidden argument*/NULL);
		float L_28 = V_3;
		int32_t L_29 = ___xCount;
		V_5 = ((float)((float)((float)((float)L_27-(float)L_28))/(float)(((float)((float)L_29)))));
		float L_30 = Rect_get_height_m6_279((&___position), /*hidden argument*/NULL);
		float L_31 = V_4;
		int32_t L_32 = V_2;
		V_6 = ((float)((float)((float)((float)L_30-(float)L_31))/(float)(((float)((float)L_32)))));
		GUIStyle_t6_166 * L_33 = ___style;
		NullCheck(L_33);
		float L_34 = GUIStyle_get_fixedWidth_m6_1342(L_33, /*hidden argument*/NULL);
		if ((((float)L_34) == ((float)(0.0f))))
		{
			goto IL_00ac;
		}
	}
	{
		GUIStyle_t6_166 * L_35 = ___style;
		NullCheck(L_35);
		float L_36 = GUIStyle_get_fixedWidth_m6_1342(L_35, /*hidden argument*/NULL);
		V_5 = L_36;
	}

IL_00ac:
	{
		GUIStyle_t6_166 * L_37 = ___style;
		NullCheck(L_37);
		float L_38 = GUIStyle_get_fixedHeight_m6_1343(L_37, /*hidden argument*/NULL);
		if ((((float)L_38) == ((float)(0.0f))))
		{
			goto IL_00c6;
		}
	}
	{
		GUIStyle_t6_166 * L_39 = ___style;
		NullCheck(L_39);
		float L_40 = GUIStyle_get_fixedHeight_m6_1343(L_39, /*hidden argument*/NULL);
		V_6 = L_40;
	}

IL_00c6:
	{
		Event_t6_154 * L_41 = Event_get_current_m6_989(NULL /*static, unused*/, /*hidden argument*/NULL);
		int32_t L_42 = V_1;
		NullCheck(L_41);
		int32_t L_43 = Event_GetTypeForControl_m6_1005(L_41, L_42, /*hidden argument*/NULL);
		V_14 = L_43;
		int32_t L_44 = V_14;
		if (L_44 == 0)
		{
			goto IL_00ff;
		}
		if (L_44 == 1)
		{
			goto IL_0173;
		}
		if (L_44 == 2)
		{
			goto IL_038d;
		}
		if (L_44 == 3)
		{
			goto IL_0159;
		}
		if (L_44 == 4)
		{
			goto IL_038d;
		}
		if (L_44 == 5)
		{
			goto IL_038d;
		}
		if (L_44 == 6)
		{
			goto IL_038d;
		}
		if (L_44 == 7)
		{
			goto IL_01c7;
		}
	}
	{
		goto IL_038d;
	}

IL_00ff:
	{
		Event_t6_154 * L_45 = Event_get_current_m6_989(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_45);
		Vector2_t6_48  L_46 = Event_get_mousePosition_m6_970(L_45, /*hidden argument*/NULL);
		bool L_47 = Rect_Contains_m6_286((&___position), L_46, /*hidden argument*/NULL);
		if (!L_47)
		{
			goto IL_0154;
		}
	}
	{
		Rect_t6_52  L_48 = ___position;
		int32_t L_49 = V_0;
		int32_t L_50 = ___xCount;
		float L_51 = V_5;
		float L_52 = V_6;
		GUIStyle_t6_166 * L_53 = ___style;
		GUIStyle_t6_166 * L_54 = ___firstStyle;
		GUIStyle_t6_166 * L_55 = ___midStyle;
		GUIStyle_t6_166 * L_56 = ___lastStyle;
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t6_160_il2cpp_TypeInfo_var);
		RectU5BU5D_t6_265* L_57 = GUI_CalcMouseRects_m6_1063(NULL /*static, unused*/, L_48, L_49, L_50, L_51, L_52, L_53, L_54, L_55, L_56, 0, /*hidden argument*/NULL);
		V_7 = L_57;
		RectU5BU5D_t6_265* L_58 = V_7;
		Event_t6_154 * L_59 = Event_get_current_m6_989(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_59);
		Vector2_t6_48  L_60 = Event_get_mousePosition_m6_970(L_59, /*hidden argument*/NULL);
		int32_t L_61 = GUI_GetButtonGridMouseSelection_m6_1064(NULL /*static, unused*/, L_58, L_60, 1, /*hidden argument*/NULL);
		if ((((int32_t)L_61) == ((int32_t)(-1))))
		{
			goto IL_0154;
		}
	}
	{
		int32_t L_62 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(GUIUtility_t6_185_il2cpp_TypeInfo_var);
		GUIUtility_set_hotControl_m6_1371(NULL /*static, unused*/, L_62, /*hidden argument*/NULL);
		Event_t6_154 * L_63 = Event_get_current_m6_989(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_63);
		Event_Use_m6_1027(L_63, /*hidden argument*/NULL);
	}

IL_0154:
	{
		goto IL_038d;
	}

IL_0159:
	{
		IL2CPP_RUNTIME_CLASS_INIT(GUIUtility_t6_185_il2cpp_TypeInfo_var);
		int32_t L_64 = GUIUtility_get_hotControl_m6_1370(NULL /*static, unused*/, /*hidden argument*/NULL);
		int32_t L_65 = V_1;
		if ((!(((uint32_t)L_64) == ((uint32_t)L_65))))
		{
			goto IL_016e;
		}
	}
	{
		Event_t6_154 * L_66 = Event_get_current_m6_989(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_66);
		Event_Use_m6_1027(L_66, /*hidden argument*/NULL);
	}

IL_016e:
	{
		goto IL_038d;
	}

IL_0173:
	{
		IL2CPP_RUNTIME_CLASS_INIT(GUIUtility_t6_185_il2cpp_TypeInfo_var);
		int32_t L_67 = GUIUtility_get_hotControl_m6_1370(NULL /*static, unused*/, /*hidden argument*/NULL);
		int32_t L_68 = V_1;
		if ((!(((uint32_t)L_67) == ((uint32_t)L_68))))
		{
			goto IL_01c2;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GUIUtility_t6_185_il2cpp_TypeInfo_var);
		GUIUtility_set_hotControl_m6_1371(NULL /*static, unused*/, 0, /*hidden argument*/NULL);
		Event_t6_154 * L_69 = Event_get_current_m6_989(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_69);
		Event_Use_m6_1027(L_69, /*hidden argument*/NULL);
		Rect_t6_52  L_70 = ___position;
		int32_t L_71 = V_0;
		int32_t L_72 = ___xCount;
		float L_73 = V_5;
		float L_74 = V_6;
		GUIStyle_t6_166 * L_75 = ___style;
		GUIStyle_t6_166 * L_76 = ___firstStyle;
		GUIStyle_t6_166 * L_77 = ___midStyle;
		GUIStyle_t6_166 * L_78 = ___lastStyle;
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t6_160_il2cpp_TypeInfo_var);
		RectU5BU5D_t6_265* L_79 = GUI_CalcMouseRects_m6_1063(NULL /*static, unused*/, L_70, L_71, L_72, L_73, L_74, L_75, L_76, L_77, L_78, 0, /*hidden argument*/NULL);
		V_7 = L_79;
		RectU5BU5D_t6_265* L_80 = V_7;
		Event_t6_154 * L_81 = Event_get_current_m6_989(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_81);
		Vector2_t6_48  L_82 = Event_get_mousePosition_m6_970(L_81, /*hidden argument*/NULL);
		int32_t L_83 = GUI_GetButtonGridMouseSelection_m6_1064(NULL /*static, unused*/, L_80, L_82, 1, /*hidden argument*/NULL);
		V_8 = L_83;
		GUI_set_changed_m6_1081(NULL /*static, unused*/, 1, /*hidden argument*/NULL);
		int32_t L_84 = V_8;
		return L_84;
	}

IL_01c2:
	{
		goto IL_038d;
	}

IL_01c7:
	{
		V_9 = (GUIStyle_t6_166 *)NULL;
		Rect_t6_52  L_85 = ___position;
		Vector2_t6_48  L_86 = Vector2_get_zero_m6_182(NULL /*static, unused*/, /*hidden argument*/NULL);
		Vector2_t6_48  L_87 = Vector2_get_zero_m6_182(NULL /*static, unused*/, /*hidden argument*/NULL);
		GUIClip_Push_m6_1393(NULL /*static, unused*/, L_85, L_86, L_87, 0, /*hidden argument*/NULL);
		float L_88 = Rect_get_width_m6_277((&___position), /*hidden argument*/NULL);
		float L_89 = Rect_get_height_m6_279((&___position), /*hidden argument*/NULL);
		Rect__ctor_m6_270((&___position), (0.0f), (0.0f), L_88, L_89, /*hidden argument*/NULL);
		Rect_t6_52  L_90 = ___position;
		int32_t L_91 = V_0;
		int32_t L_92 = ___xCount;
		float L_93 = V_5;
		float L_94 = V_6;
		GUIStyle_t6_166 * L_95 = ___style;
		GUIStyle_t6_166 * L_96 = ___firstStyle;
		GUIStyle_t6_166 * L_97 = ___midStyle;
		GUIStyle_t6_166 * L_98 = ___lastStyle;
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t6_160_il2cpp_TypeInfo_var);
		RectU5BU5D_t6_265* L_99 = GUI_CalcMouseRects_m6_1063(NULL /*static, unused*/, L_90, L_91, L_92, L_93, L_94, L_95, L_96, L_97, L_98, 0, /*hidden argument*/NULL);
		V_7 = L_99;
		RectU5BU5D_t6_265* L_100 = V_7;
		Event_t6_154 * L_101 = Event_get_current_m6_989(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_101);
		Vector2_t6_48  L_102 = Event_get_mousePosition_m6_970(L_101, /*hidden argument*/NULL);
		int32_t L_103 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(GUIUtility_t6_185_il2cpp_TypeInfo_var);
		int32_t L_104 = GUIUtility_get_hotControl_m6_1370(NULL /*static, unused*/, /*hidden argument*/NULL);
		int32_t L_105 = GUI_GetButtonGridMouseSelection_m6_1064(NULL /*static, unused*/, L_100, L_102, ((((int32_t)L_103) == ((int32_t)L_104))? 1 : 0), /*hidden argument*/NULL);
		V_10 = L_105;
		Event_t6_154 * L_106 = Event_get_current_m6_989(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_106);
		Vector2_t6_48  L_107 = Event_get_mousePosition_m6_970(L_106, /*hidden argument*/NULL);
		bool L_108 = Rect_Contains_m6_286((&___position), L_107, /*hidden argument*/NULL);
		V_11 = L_108;
		bool L_109 = GUIUtility_get_mouseUsed_m6_1390(NULL /*static, unused*/, /*hidden argument*/NULL);
		bool L_110 = V_11;
		GUIUtility_set_mouseUsed_m6_1391(NULL /*static, unused*/, ((int32_t)((int32_t)L_109|(int32_t)L_110)), /*hidden argument*/NULL);
		V_12 = 0;
		goto IL_0300;
	}

IL_0254:
	{
		V_13 = (GUIStyle_t6_166 *)NULL;
		int32_t L_111 = V_12;
		if (!L_111)
		{
			goto IL_0267;
		}
	}
	{
		GUIStyle_t6_166 * L_112 = ___midStyle;
		V_13 = L_112;
		goto IL_026b;
	}

IL_0267:
	{
		GUIStyle_t6_166 * L_113 = ___firstStyle;
		V_13 = L_113;
	}

IL_026b:
	{
		int32_t L_114 = V_12;
		int32_t L_115 = V_0;
		if ((!(((uint32_t)L_114) == ((uint32_t)((int32_t)((int32_t)L_115-(int32_t)1))))))
		{
			goto IL_0279;
		}
	}
	{
		GUIStyle_t6_166 * L_116 = ___lastStyle;
		V_13 = L_116;
	}

IL_0279:
	{
		int32_t L_117 = V_0;
		if ((!(((uint32_t)L_117) == ((uint32_t)1))))
		{
			goto IL_0284;
		}
	}
	{
		GUIStyle_t6_166 * L_118 = ___style;
		V_13 = L_118;
	}

IL_0284:
	{
		int32_t L_119 = V_12;
		int32_t L_120 = ___selected;
		if ((((int32_t)L_119) == ((int32_t)L_120)))
		{
			goto IL_02f6;
		}
	}
	{
		GUIStyle_t6_166 * L_121 = V_13;
		RectU5BU5D_t6_265* L_122 = V_7;
		int32_t L_123 = V_12;
		NullCheck(L_122);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_122, L_123);
		GUIContentU5BU5D_t6_264* L_124 = ___contents;
		int32_t L_125 = V_12;
		NullCheck(L_124);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_124, L_125);
		int32_t L_126 = L_125;
		int32_t L_127 = V_12;
		int32_t L_128 = V_10;
		G_B32_0 = (*(GUIContent_t6_163 **)(GUIContent_t6_163 **)SZArrayLdElema(L_124, L_126, sizeof(GUIContent_t6_163 *)));
		G_B32_1 = (*(Rect_t6_52 *)((Rect_t6_52 *)(Rect_t6_52 *)SZArrayLdElema(L_122, L_123, sizeof(Rect_t6_52 ))));
		G_B32_2 = L_121;
		if ((!(((uint32_t)L_127) == ((uint32_t)L_128))))
		{
			G_B38_0 = (*(GUIContent_t6_163 **)(GUIContent_t6_163 **)SZArrayLdElema(L_124, L_126, sizeof(GUIContent_t6_163 *)));
			G_B38_1 = (*(Rect_t6_52 *)((Rect_t6_52 *)(Rect_t6_52 *)SZArrayLdElema(L_122, L_123, sizeof(Rect_t6_52 ))));
			G_B38_2 = L_121;
			goto IL_02d6;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t6_160_il2cpp_TypeInfo_var);
		bool L_129 = GUI_get_enabled_m6_1082(NULL /*static, unused*/, /*hidden argument*/NULL);
		G_B33_0 = G_B32_0;
		G_B33_1 = G_B32_1;
		G_B33_2 = G_B32_2;
		if (L_129)
		{
			G_B34_0 = G_B32_0;
			G_B34_1 = G_B32_1;
			G_B34_2 = G_B32_2;
			goto IL_02be;
		}
	}
	{
		int32_t L_130 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(GUIUtility_t6_185_il2cpp_TypeInfo_var);
		int32_t L_131 = GUIUtility_get_hotControl_m6_1370(NULL /*static, unused*/, /*hidden argument*/NULL);
		G_B34_0 = G_B33_0;
		G_B34_1 = G_B33_1;
		G_B34_2 = G_B33_2;
		if ((!(((uint32_t)L_130) == ((uint32_t)L_131))))
		{
			G_B38_0 = G_B33_0;
			G_B38_1 = G_B33_1;
			G_B38_2 = G_B33_2;
			goto IL_02d6;
		}
	}

IL_02be:
	{
		int32_t L_132 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(GUIUtility_t6_185_il2cpp_TypeInfo_var);
		int32_t L_133 = GUIUtility_get_hotControl_m6_1370(NULL /*static, unused*/, /*hidden argument*/NULL);
		G_B35_0 = G_B34_0;
		G_B35_1 = G_B34_1;
		G_B35_2 = G_B34_2;
		if ((((int32_t)L_132) == ((int32_t)L_133)))
		{
			G_B36_0 = G_B34_0;
			G_B36_1 = G_B34_1;
			G_B36_2 = G_B34_2;
			goto IL_02d3;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GUIUtility_t6_185_il2cpp_TypeInfo_var);
		int32_t L_134 = GUIUtility_get_hotControl_m6_1370(NULL /*static, unused*/, /*hidden argument*/NULL);
		G_B37_0 = ((((int32_t)L_134) == ((int32_t)0))? 1 : 0);
		G_B37_1 = G_B35_0;
		G_B37_2 = G_B35_1;
		G_B37_3 = G_B35_2;
		goto IL_02d4;
	}

IL_02d3:
	{
		G_B37_0 = 1;
		G_B37_1 = G_B36_0;
		G_B37_2 = G_B36_1;
		G_B37_3 = G_B36_2;
	}

IL_02d4:
	{
		G_B39_0 = G_B37_0;
		G_B39_1 = G_B37_1;
		G_B39_2 = G_B37_2;
		G_B39_3 = G_B37_3;
		goto IL_02d7;
	}

IL_02d6:
	{
		G_B39_0 = 0;
		G_B39_1 = G_B38_0;
		G_B39_2 = G_B38_1;
		G_B39_3 = G_B38_2;
	}

IL_02d7:
	{
		int32_t L_135 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(GUIUtility_t6_185_il2cpp_TypeInfo_var);
		int32_t L_136 = GUIUtility_get_hotControl_m6_1370(NULL /*static, unused*/, /*hidden argument*/NULL);
		G_B40_0 = G_B39_0;
		G_B40_1 = G_B39_1;
		G_B40_2 = G_B39_2;
		G_B40_3 = G_B39_3;
		if ((!(((uint32_t)L_135) == ((uint32_t)L_136))))
		{
			G_B41_0 = G_B39_0;
			G_B41_1 = G_B39_1;
			G_B41_2 = G_B39_2;
			G_B41_3 = G_B39_3;
			goto IL_02e9;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t6_160_il2cpp_TypeInfo_var);
		bool L_137 = GUI_get_enabled_m6_1082(NULL /*static, unused*/, /*hidden argument*/NULL);
		G_B42_0 = ((int32_t)(L_137));
		G_B42_1 = G_B40_0;
		G_B42_2 = G_B40_1;
		G_B42_3 = G_B40_2;
		G_B42_4 = G_B40_3;
		goto IL_02ea;
	}

IL_02e9:
	{
		G_B42_0 = 0;
		G_B42_1 = G_B41_0;
		G_B42_2 = G_B41_1;
		G_B42_3 = G_B41_2;
		G_B42_4 = G_B41_3;
	}

IL_02ea:
	{
		NullCheck(G_B42_4);
		GUIStyle_Draw_m6_1310(G_B42_4, G_B42_3, G_B42_2, G_B42_1, G_B42_0, 0, 0, /*hidden argument*/NULL);
		goto IL_02fa;
	}

IL_02f6:
	{
		GUIStyle_t6_166 * L_138 = V_13;
		V_9 = L_138;
	}

IL_02fa:
	{
		int32_t L_139 = V_12;
		V_12 = ((int32_t)((int32_t)L_139+(int32_t)1));
	}

IL_0300:
	{
		int32_t L_140 = V_12;
		int32_t L_141 = V_0;
		if ((((int32_t)L_140) < ((int32_t)L_141)))
		{
			goto IL_0254;
		}
	}
	{
		int32_t L_142 = ___selected;
		int32_t L_143 = V_0;
		if ((((int32_t)L_142) >= ((int32_t)L_143)))
		{
			goto IL_036d;
		}
	}
	{
		int32_t L_144 = ___selected;
		if ((((int32_t)L_144) <= ((int32_t)(-1))))
		{
			goto IL_036d;
		}
	}
	{
		GUIStyle_t6_166 * L_145 = V_9;
		RectU5BU5D_t6_265* L_146 = V_7;
		int32_t L_147 = ___selected;
		NullCheck(L_146);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_146, L_147);
		GUIContentU5BU5D_t6_264* L_148 = ___contents;
		int32_t L_149 = ___selected;
		NullCheck(L_148);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_148, L_149);
		int32_t L_150 = L_149;
		int32_t L_151 = ___selected;
		int32_t L_152 = V_10;
		G_B49_0 = (*(GUIContent_t6_163 **)(GUIContent_t6_163 **)SZArrayLdElema(L_148, L_150, sizeof(GUIContent_t6_163 *)));
		G_B49_1 = (*(Rect_t6_52 *)((Rect_t6_52 *)(Rect_t6_52 *)SZArrayLdElema(L_146, L_147, sizeof(Rect_t6_52 ))));
		G_B49_2 = L_145;
		if ((!(((uint32_t)L_151) == ((uint32_t)L_152))))
		{
			G_B55_0 = (*(GUIContent_t6_163 **)(GUIContent_t6_163 **)SZArrayLdElema(L_148, L_150, sizeof(GUIContent_t6_163 *)));
			G_B55_1 = (*(Rect_t6_52 *)((Rect_t6_52 *)(Rect_t6_52 *)SZArrayLdElema(L_146, L_147, sizeof(Rect_t6_52 ))));
			G_B55_2 = L_145;
			goto IL_035d;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t6_160_il2cpp_TypeInfo_var);
		bool L_153 = GUI_get_enabled_m6_1082(NULL /*static, unused*/, /*hidden argument*/NULL);
		G_B50_0 = G_B49_0;
		G_B50_1 = G_B49_1;
		G_B50_2 = G_B49_2;
		if (L_153)
		{
			G_B51_0 = G_B49_0;
			G_B51_1 = G_B49_1;
			G_B51_2 = G_B49_2;
			goto IL_0345;
		}
	}
	{
		int32_t L_154 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(GUIUtility_t6_185_il2cpp_TypeInfo_var);
		int32_t L_155 = GUIUtility_get_hotControl_m6_1370(NULL /*static, unused*/, /*hidden argument*/NULL);
		G_B51_0 = G_B50_0;
		G_B51_1 = G_B50_1;
		G_B51_2 = G_B50_2;
		if ((!(((uint32_t)L_154) == ((uint32_t)L_155))))
		{
			G_B55_0 = G_B50_0;
			G_B55_1 = G_B50_1;
			G_B55_2 = G_B50_2;
			goto IL_035d;
		}
	}

IL_0345:
	{
		int32_t L_156 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(GUIUtility_t6_185_il2cpp_TypeInfo_var);
		int32_t L_157 = GUIUtility_get_hotControl_m6_1370(NULL /*static, unused*/, /*hidden argument*/NULL);
		G_B52_0 = G_B51_0;
		G_B52_1 = G_B51_1;
		G_B52_2 = G_B51_2;
		if ((((int32_t)L_156) == ((int32_t)L_157)))
		{
			G_B53_0 = G_B51_0;
			G_B53_1 = G_B51_1;
			G_B53_2 = G_B51_2;
			goto IL_035a;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GUIUtility_t6_185_il2cpp_TypeInfo_var);
		int32_t L_158 = GUIUtility_get_hotControl_m6_1370(NULL /*static, unused*/, /*hidden argument*/NULL);
		G_B54_0 = ((((int32_t)L_158) == ((int32_t)0))? 1 : 0);
		G_B54_1 = G_B52_0;
		G_B54_2 = G_B52_1;
		G_B54_3 = G_B52_2;
		goto IL_035b;
	}

IL_035a:
	{
		G_B54_0 = 1;
		G_B54_1 = G_B53_0;
		G_B54_2 = G_B53_1;
		G_B54_3 = G_B53_2;
	}

IL_035b:
	{
		G_B56_0 = G_B54_0;
		G_B56_1 = G_B54_1;
		G_B56_2 = G_B54_2;
		G_B56_3 = G_B54_3;
		goto IL_035e;
	}

IL_035d:
	{
		G_B56_0 = 0;
		G_B56_1 = G_B55_0;
		G_B56_2 = G_B55_1;
		G_B56_3 = G_B55_2;
	}

IL_035e:
	{
		int32_t L_159 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(GUIUtility_t6_185_il2cpp_TypeInfo_var);
		int32_t L_160 = GUIUtility_get_hotControl_m6_1370(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(G_B56_3);
		GUIStyle_Draw_m6_1310(G_B56_3, G_B56_2, G_B56_1, G_B56_0, ((((int32_t)L_159) == ((int32_t)L_160))? 1 : 0), 1, 0, /*hidden argument*/NULL);
	}

IL_036d:
	{
		int32_t L_161 = V_10;
		if ((((int32_t)L_161) < ((int32_t)0)))
		{
			goto IL_0383;
		}
	}
	{
		GUIContentU5BU5D_t6_264* L_162 = ___contents;
		int32_t L_163 = V_10;
		NullCheck(L_162);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_162, L_163);
		int32_t L_164 = L_163;
		NullCheck((*(GUIContent_t6_163 **)(GUIContent_t6_163 **)SZArrayLdElema(L_162, L_164, sizeof(GUIContent_t6_163 *))));
		String_t* L_165 = GUIContent_get_tooltip_m6_1105((*(GUIContent_t6_163 **)(GUIContent_t6_163 **)SZArrayLdElema(L_162, L_164, sizeof(GUIContent_t6_163 *))), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t6_160_il2cpp_TypeInfo_var);
		GUI_set_tooltip_m6_1043(NULL /*static, unused*/, L_165, /*hidden argument*/NULL);
	}

IL_0383:
	{
		GUIClip_Pop_m6_1395(NULL /*static, unused*/, /*hidden argument*/NULL);
		goto IL_038d;
	}

IL_038d:
	{
		int32_t L_166 = ___selected;
		return L_166;
	}
}
// UnityEngine.Rect[] UnityEngine.GUI::CalcMouseRects(UnityEngine.Rect,System.Int32,System.Int32,System.Single,System.Single,UnityEngine.GUIStyle,UnityEngine.GUIStyle,UnityEngine.GUIStyle,UnityEngine.GUIStyle,System.Boolean)
extern TypeInfo* RectU5BU5D_t6_265_il2cpp_TypeInfo_var;
extern TypeInfo* Mathf_t6_58_il2cpp_TypeInfo_var;
extern "C" RectU5BU5D_t6_265* GUI_CalcMouseRects_m6_1063 (Object_t * __this /* static, unused */, Rect_t6_52  ___position, int32_t ___count, int32_t ___xCount, float ___elemWidth, float ___elemHeight, GUIStyle_t6_166 * ___style, GUIStyle_t6_166 * ___firstStyle, GUIStyle_t6_166 * ___midStyle, GUIStyle_t6_166 * ___lastStyle, bool ___addBorders, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		RectU5BU5D_t6_265_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(987);
		Mathf_t6_58_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(940);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	float V_2 = 0.0f;
	float V_3 = 0.0f;
	GUIStyle_t6_166 * V_4 = {0};
	RectU5BU5D_t6_265* V_5 = {0};
	int32_t V_6 = 0;
	GUIStyle_t6_166 * V_7 = {0};
	{
		V_0 = 0;
		V_1 = 0;
		float L_0 = Rect_get_xMin_m6_281((&___position), /*hidden argument*/NULL);
		V_2 = L_0;
		float L_1 = Rect_get_yMin_m6_282((&___position), /*hidden argument*/NULL);
		V_3 = L_1;
		GUIStyle_t6_166 * L_2 = ___style;
		V_4 = L_2;
		int32_t L_3 = ___count;
		V_5 = ((RectU5BU5D_t6_265*)SZArrayNew(RectU5BU5D_t6_265_il2cpp_TypeInfo_var, L_3));
		int32_t L_4 = ___count;
		if ((((int32_t)L_4) <= ((int32_t)1)))
		{
			goto IL_002b;
		}
	}
	{
		GUIStyle_t6_166 * L_5 = ___firstStyle;
		V_4 = L_5;
	}

IL_002b:
	{
		V_6 = 0;
		goto IL_0149;
	}

IL_0033:
	{
		bool L_6 = ___addBorders;
		if (L_6)
		{
			goto IL_0057;
		}
	}
	{
		RectU5BU5D_t6_265* L_7 = V_5;
		int32_t L_8 = V_6;
		NullCheck(L_7);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_7, L_8);
		float L_9 = V_2;
		float L_10 = V_3;
		float L_11 = ___elemWidth;
		float L_12 = ___elemHeight;
		Rect_t6_52  L_13 = {0};
		Rect__ctor_m6_270(&L_13, L_9, L_10, L_11, L_12, /*hidden argument*/NULL);
		(*(Rect_t6_52 *)((Rect_t6_52 *)(Rect_t6_52 *)SZArrayLdElema(L_7, L_8, sizeof(Rect_t6_52 )))) = L_13;
		goto IL_007b;
	}

IL_0057:
	{
		RectU5BU5D_t6_265* L_14 = V_5;
		int32_t L_15 = V_6;
		NullCheck(L_14);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_14, L_15);
		GUIStyle_t6_166 * L_16 = V_4;
		NullCheck(L_16);
		RectOffset_t6_172 * L_17 = GUIStyle_get_margin_m6_1303(L_16, /*hidden argument*/NULL);
		float L_18 = V_2;
		float L_19 = V_3;
		float L_20 = ___elemWidth;
		float L_21 = ___elemHeight;
		Rect_t6_52  L_22 = {0};
		Rect__ctor_m6_270(&L_22, L_18, L_19, L_20, L_21, /*hidden argument*/NULL);
		NullCheck(L_17);
		Rect_t6_52  L_23 = RectOffset_Add_m6_1294(L_17, L_22, /*hidden argument*/NULL);
		(*(Rect_t6_52 *)((Rect_t6_52 *)(Rect_t6_52 *)SZArrayLdElema(L_14, L_15, sizeof(Rect_t6_52 )))) = L_23;
	}

IL_007b:
	{
		RectU5BU5D_t6_265* L_24 = V_5;
		int32_t L_25 = V_6;
		NullCheck(L_24);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_24, L_25);
		RectU5BU5D_t6_265* L_26 = V_5;
		int32_t L_27 = V_6;
		NullCheck(L_26);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_26, L_27);
		float L_28 = Rect_get_xMax_m6_283(((Rect_t6_52 *)(Rect_t6_52 *)SZArrayLdElema(L_26, L_27, sizeof(Rect_t6_52 ))), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t6_58_il2cpp_TypeInfo_var);
		float L_29 = bankers_roundf(L_28);
		RectU5BU5D_t6_265* L_30 = V_5;
		int32_t L_31 = V_6;
		NullCheck(L_30);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_30, L_31);
		float L_32 = Rect_get_x_m6_273(((Rect_t6_52 *)(Rect_t6_52 *)SZArrayLdElema(L_30, L_31, sizeof(Rect_t6_52 ))), /*hidden argument*/NULL);
		float L_33 = bankers_roundf(L_32);
		Rect_set_width_m6_278(((Rect_t6_52 *)(Rect_t6_52 *)SZArrayLdElema(L_24, L_25, sizeof(Rect_t6_52 ))), ((float)((float)L_29-(float)L_33)), /*hidden argument*/NULL);
		RectU5BU5D_t6_265* L_34 = V_5;
		int32_t L_35 = V_6;
		NullCheck(L_34);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_34, L_35);
		RectU5BU5D_t6_265* L_36 = V_5;
		int32_t L_37 = V_6;
		NullCheck(L_36);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_36, L_37);
		float L_38 = Rect_get_x_m6_273(((Rect_t6_52 *)(Rect_t6_52 *)SZArrayLdElema(L_36, L_37, sizeof(Rect_t6_52 ))), /*hidden argument*/NULL);
		float L_39 = bankers_roundf(L_38);
		Rect_set_x_m6_274(((Rect_t6_52 *)(Rect_t6_52 *)SZArrayLdElema(L_34, L_35, sizeof(Rect_t6_52 ))), L_39, /*hidden argument*/NULL);
		GUIStyle_t6_166 * L_40 = ___midStyle;
		V_7 = L_40;
		int32_t L_41 = V_6;
		int32_t L_42 = ___count;
		if ((!(((uint32_t)L_41) == ((uint32_t)((int32_t)((int32_t)L_42-(int32_t)2))))))
		{
			goto IL_00e3;
		}
	}
	{
		GUIStyle_t6_166 * L_43 = ___lastStyle;
		V_7 = L_43;
	}

IL_00e3:
	{
		float L_44 = V_2;
		float L_45 = ___elemWidth;
		GUIStyle_t6_166 * L_46 = V_4;
		NullCheck(L_46);
		RectOffset_t6_172 * L_47 = GUIStyle_get_margin_m6_1303(L_46, /*hidden argument*/NULL);
		NullCheck(L_47);
		int32_t L_48 = RectOffset_get_right_m6_1286(L_47, /*hidden argument*/NULL);
		GUIStyle_t6_166 * L_49 = V_7;
		NullCheck(L_49);
		RectOffset_t6_172 * L_50 = GUIStyle_get_margin_m6_1303(L_49, /*hidden argument*/NULL);
		NullCheck(L_50);
		int32_t L_51 = RectOffset_get_left_m6_1284(L_50, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t6_58_il2cpp_TypeInfo_var);
		int32_t L_52 = Mathf_Max_m6_384(NULL /*static, unused*/, L_48, L_51, /*hidden argument*/NULL);
		V_2 = ((float)((float)L_44+(float)((float)((float)L_45+(float)(((float)((float)L_52)))))));
		int32_t L_53 = V_1;
		V_1 = ((int32_t)((int32_t)L_53+(int32_t)1));
		int32_t L_54 = V_1;
		int32_t L_55 = ___xCount;
		if ((((int32_t)L_54) < ((int32_t)L_55)))
		{
			goto IL_0143;
		}
	}
	{
		int32_t L_56 = V_0;
		V_0 = ((int32_t)((int32_t)L_56+(int32_t)1));
		V_1 = 0;
		float L_57 = V_3;
		float L_58 = ___elemHeight;
		GUIStyle_t6_166 * L_59 = ___style;
		NullCheck(L_59);
		RectOffset_t6_172 * L_60 = GUIStyle_get_margin_m6_1303(L_59, /*hidden argument*/NULL);
		NullCheck(L_60);
		int32_t L_61 = RectOffset_get_top_m6_1288(L_60, /*hidden argument*/NULL);
		GUIStyle_t6_166 * L_62 = ___style;
		NullCheck(L_62);
		RectOffset_t6_172 * L_63 = GUIStyle_get_margin_m6_1303(L_62, /*hidden argument*/NULL);
		NullCheck(L_63);
		int32_t L_64 = RectOffset_get_bottom_m6_1290(L_63, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t6_58_il2cpp_TypeInfo_var);
		int32_t L_65 = Mathf_Max_m6_384(NULL /*static, unused*/, L_61, L_64, /*hidden argument*/NULL);
		V_3 = ((float)((float)L_57+(float)((float)((float)L_58+(float)(((float)((float)L_65)))))));
		float L_66 = Rect_get_xMin_m6_281((&___position), /*hidden argument*/NULL);
		V_2 = L_66;
	}

IL_0143:
	{
		int32_t L_67 = V_6;
		V_6 = ((int32_t)((int32_t)L_67+(int32_t)1));
	}

IL_0149:
	{
		int32_t L_68 = V_6;
		int32_t L_69 = ___count;
		if ((((int32_t)L_68) < ((int32_t)L_69)))
		{
			goto IL_0033;
		}
	}
	{
		RectU5BU5D_t6_265* L_70 = V_5;
		return L_70;
	}
}
// System.Int32 UnityEngine.GUI::GetButtonGridMouseSelection(UnityEngine.Rect[],UnityEngine.Vector2,System.Boolean)
extern TypeInfo* Mathf_t6_58_il2cpp_TypeInfo_var;
extern "C" int32_t GUI_GetButtonGridMouseSelection_m6_1064 (Object_t * __this /* static, unused */, RectU5BU5D_t6_265* ___buttonRects, Vector2_t6_48  ___mousePos, bool ___findNearest, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Mathf_t6_58_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(940);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	float V_1 = 0.0f;
	int32_t V_2 = 0;
	int32_t V_3 = 0;
	Rect_t6_52  V_4 = {0};
	Vector2_t6_48  V_5 = {0};
	float V_6 = 0.0f;
	Vector2_t6_48  V_7 = {0};
	{
		V_0 = 0;
		goto IL_001f;
	}

IL_0007:
	{
		RectU5BU5D_t6_265* L_0 = ___buttonRects;
		int32_t L_1 = V_0;
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, L_1);
		Vector2_t6_48  L_2 = ___mousePos;
		bool L_3 = Rect_Contains_m6_286(((Rect_t6_52 *)(Rect_t6_52 *)SZArrayLdElema(L_0, L_1, sizeof(Rect_t6_52 ))), L_2, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_001b;
		}
	}
	{
		int32_t L_4 = V_0;
		return L_4;
	}

IL_001b:
	{
		int32_t L_5 = V_0;
		V_0 = ((int32_t)((int32_t)L_5+(int32_t)1));
	}

IL_001f:
	{
		int32_t L_6 = V_0;
		RectU5BU5D_t6_265* L_7 = ___buttonRects;
		NullCheck(L_7);
		if ((((int32_t)L_6) < ((int32_t)(((int32_t)((int32_t)(((Array_t *)L_7)->max_length)))))))
		{
			goto IL_0007;
		}
	}
	{
		bool L_8 = ___findNearest;
		if (L_8)
		{
			goto IL_0030;
		}
	}
	{
		return (-1);
	}

IL_0030:
	{
		V_1 = (1.0E+07f);
		V_2 = (-1);
		V_3 = 0;
		goto IL_00ac;
	}

IL_003f:
	{
		RectU5BU5D_t6_265* L_9 = ___buttonRects;
		int32_t L_10 = V_3;
		NullCheck(L_9);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_9, L_10);
		V_4 = (*(Rect_t6_52 *)((Rect_t6_52 *)(Rect_t6_52 *)SZArrayLdElema(L_9, L_10, sizeof(Rect_t6_52 ))));
		float L_11 = ((&___mousePos)->___x_1);
		float L_12 = Rect_get_xMin_m6_281((&V_4), /*hidden argument*/NULL);
		float L_13 = Rect_get_xMax_m6_283((&V_4), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t6_58_il2cpp_TypeInfo_var);
		float L_14 = Mathf_Clamp_m6_389(NULL /*static, unused*/, L_11, L_12, L_13, /*hidden argument*/NULL);
		float L_15 = ((&___mousePos)->___y_2);
		float L_16 = Rect_get_yMin_m6_282((&V_4), /*hidden argument*/NULL);
		float L_17 = Rect_get_yMax_m6_284((&V_4), /*hidden argument*/NULL);
		float L_18 = Mathf_Clamp_m6_389(NULL /*static, unused*/, L_15, L_16, L_17, /*hidden argument*/NULL);
		Vector2__ctor_m6_176((&V_5), L_14, L_18, /*hidden argument*/NULL);
		Vector2_t6_48  L_19 = ___mousePos;
		Vector2_t6_48  L_20 = V_5;
		Vector2_t6_48  L_21 = Vector2_op_Subtraction_m6_185(NULL /*static, unused*/, L_19, L_20, /*hidden argument*/NULL);
		V_7 = L_21;
		float L_22 = Vector2_get_sqrMagnitude_m6_180((&V_7), /*hidden argument*/NULL);
		V_6 = L_22;
		float L_23 = V_6;
		float L_24 = V_1;
		if ((!(((float)L_23) < ((float)L_24))))
		{
			goto IL_00a8;
		}
	}
	{
		int32_t L_25 = V_3;
		V_2 = L_25;
		float L_26 = V_6;
		V_1 = L_26;
	}

IL_00a8:
	{
		int32_t L_27 = V_3;
		V_3 = ((int32_t)((int32_t)L_27+(int32_t)1));
	}

IL_00ac:
	{
		int32_t L_28 = V_3;
		RectU5BU5D_t6_265* L_29 = ___buttonRects;
		NullCheck(L_29);
		if ((((int32_t)L_28) < ((int32_t)(((int32_t)((int32_t)(((Array_t *)L_29)->max_length)))))))
		{
			goto IL_003f;
		}
	}
	{
		int32_t L_30 = V_2;
		return L_30;
	}
}
// System.Single UnityEngine.GUI::HorizontalSlider(UnityEngine.Rect,System.Single,System.Single,System.Single,UnityEngine.GUIStyle,UnityEngine.GUIStyle)
extern TypeInfo* GUI_t6_160_il2cpp_TypeInfo_var;
extern TypeInfo* GUIUtility_t6_185_il2cpp_TypeInfo_var;
extern "C" float GUI_HorizontalSlider_m6_1065 (Object_t * __this /* static, unused */, Rect_t6_52  ___position, float ___value, float ___leftValue, float ___rightValue, GUIStyle_t6_166 * ___slider, GUIStyle_t6_166 * ___thumb, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GUI_t6_160_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(982);
		GUIUtility_t6_185_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(984);
		s_Il2CppMethodIntialized = true;
	}
	{
		Rect_t6_52  L_0 = ___position;
		float L_1 = ___value;
		float L_2 = ___leftValue;
		float L_3 = ___rightValue;
		GUIStyle_t6_166 * L_4 = ___slider;
		GUIStyle_t6_166 * L_5 = ___thumb;
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t6_160_il2cpp_TypeInfo_var);
		int32_t L_6 = ((GUI_t6_160_StaticFields*)GUI_t6_160_il2cpp_TypeInfo_var->static_fields)->___s_SliderHash_7;
		Rect_t6_52  L_7 = ___position;
		IL2CPP_RUNTIME_CLASS_INIT(GUIUtility_t6_185_il2cpp_TypeInfo_var);
		int32_t L_8 = GUIUtility_GetControlID_m6_1368(NULL /*static, unused*/, L_6, 0, L_7, /*hidden argument*/NULL);
		float L_9 = GUI_Slider_m6_1066(NULL /*static, unused*/, L_0, L_1, (0.0f), L_2, L_3, L_4, L_5, 1, L_8, /*hidden argument*/NULL);
		return L_9;
	}
}
// System.Single UnityEngine.GUI::Slider(UnityEngine.Rect,System.Single,System.Single,System.Single,System.Single,UnityEngine.GUIStyle,UnityEngine.GUIStyle,System.Boolean,System.Int32)
extern TypeInfo* GUIUtility_t6_185_il2cpp_TypeInfo_var;
extern "C" float GUI_Slider_m6_1066 (Object_t * __this /* static, unused */, Rect_t6_52  ___position, float ___value, float ___size, float ___start, float ___end, GUIStyle_t6_166 * ___slider, GUIStyle_t6_166 * ___thumb, bool ___horiz, int32_t ___id, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GUIUtility_t6_185_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(984);
		s_Il2CppMethodIntialized = true;
	}
	SliderHandler_t6_229  V_0 = {0};
	{
		IL2CPP_RUNTIME_CLASS_INIT(GUIUtility_t6_185_il2cpp_TypeInfo_var);
		GUIUtility_CheckOnGUI_m6_1376(NULL /*static, unused*/, /*hidden argument*/NULL);
		Rect_t6_52  L_0 = ___position;
		float L_1 = ___value;
		float L_2 = ___size;
		float L_3 = ___start;
		float L_4 = ___end;
		GUIStyle_t6_166 * L_5 = ___slider;
		GUIStyle_t6_166 * L_6 = ___thumb;
		bool L_7 = ___horiz;
		int32_t L_8 = ___id;
		SliderHandler__ctor_m6_1492((&V_0), L_0, L_1, L_2, L_3, L_4, L_5, L_6, L_7, L_8, /*hidden argument*/NULL);
		float L_9 = SliderHandler_Handle_m6_1493((&V_0), /*hidden argument*/NULL);
		return L_9;
	}
}
// System.Single UnityEngine.GUI::HorizontalScrollbar(UnityEngine.Rect,System.Single,System.Single,System.Single,System.Single,UnityEngine.GUIStyle)
extern TypeInfo* GUI_t6_160_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2781;
extern Il2CppCodeGenString* _stringLiteral2782;
extern Il2CppCodeGenString* _stringLiteral2783;
extern "C" float GUI_HorizontalScrollbar_m6_1067 (Object_t * __this /* static, unused */, Rect_t6_52  ___position, float ___value, float ___size, float ___leftValue, float ___rightValue, GUIStyle_t6_166 * ___style, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GUI_t6_160_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(982);
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(11);
		_stringLiteral2781 = il2cpp_codegen_string_literal_from_index(2781);
		_stringLiteral2782 = il2cpp_codegen_string_literal_from_index(2782);
		_stringLiteral2783 = il2cpp_codegen_string_literal_from_index(2783);
		s_Il2CppMethodIntialized = true;
	}
	{
		Rect_t6_52  L_0 = ___position;
		float L_1 = ___value;
		float L_2 = ___size;
		float L_3 = ___leftValue;
		float L_4 = ___rightValue;
		GUIStyle_t6_166 * L_5 = ___style;
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t6_160_il2cpp_TypeInfo_var);
		GUISkin_t6_161 * L_6 = GUI_get_skin_m6_1041(NULL /*static, unused*/, /*hidden argument*/NULL);
		GUIStyle_t6_166 * L_7 = ___style;
		NullCheck(L_7);
		String_t* L_8 = GUIStyle_get_name_m6_1327(L_7, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_9 = String_Concat_m1_418(NULL /*static, unused*/, L_8, _stringLiteral2781, /*hidden argument*/NULL);
		NullCheck(L_6);
		GUIStyle_t6_166 * L_10 = GUISkin_GetStyle_m6_1265(L_6, L_9, /*hidden argument*/NULL);
		GUISkin_t6_161 * L_11 = GUI_get_skin_m6_1041(NULL /*static, unused*/, /*hidden argument*/NULL);
		GUIStyle_t6_166 * L_12 = ___style;
		NullCheck(L_12);
		String_t* L_13 = GUIStyle_get_name_m6_1327(L_12, /*hidden argument*/NULL);
		String_t* L_14 = String_Concat_m1_418(NULL /*static, unused*/, L_13, _stringLiteral2782, /*hidden argument*/NULL);
		NullCheck(L_11);
		GUIStyle_t6_166 * L_15 = GUISkin_GetStyle_m6_1265(L_11, L_14, /*hidden argument*/NULL);
		GUISkin_t6_161 * L_16 = GUI_get_skin_m6_1041(NULL /*static, unused*/, /*hidden argument*/NULL);
		GUIStyle_t6_166 * L_17 = ___style;
		NullCheck(L_17);
		String_t* L_18 = GUIStyle_get_name_m6_1327(L_17, /*hidden argument*/NULL);
		String_t* L_19 = String_Concat_m1_418(NULL /*static, unused*/, L_18, _stringLiteral2783, /*hidden argument*/NULL);
		NullCheck(L_16);
		GUIStyle_t6_166 * L_20 = GUISkin_GetStyle_m6_1265(L_16, L_19, /*hidden argument*/NULL);
		float L_21 = GUI_Scroller_m6_1070(NULL /*static, unused*/, L_0, L_1, L_2, L_3, L_4, L_5, L_10, L_15, L_20, 1, /*hidden argument*/NULL);
		return L_21;
	}
}
// System.Boolean UnityEngine.GUI::ScrollerRepeatButton(System.Int32,UnityEngine.Rect,UnityEngine.GUIStyle)
extern TypeInfo* GUIContent_t6_163_il2cpp_TypeInfo_var;
extern TypeInfo* GUI_t6_160_il2cpp_TypeInfo_var;
extern TypeInfo* DateTime_t1_127_il2cpp_TypeInfo_var;
extern "C" bool GUI_ScrollerRepeatButton_m6_1068 (Object_t * __this /* static, unused */, int32_t ___scrollerID, Rect_t6_52  ___rect, GUIStyle_t6_166 * ___style, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GUIContent_t6_163_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(985);
		GUI_t6_160_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(982);
		DateTime_t1_127_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(172);
		s_Il2CppMethodIntialized = true;
	}
	bool V_0 = false;
	bool V_1 = false;
	DateTime_t1_127  V_2 = {0};
	DateTime_t1_127  V_3 = {0};
	{
		V_0 = 0;
		Rect_t6_52  L_0 = ___rect;
		IL2CPP_RUNTIME_CLASS_INIT(GUIContent_t6_163_il2cpp_TypeInfo_var);
		GUIContent_t6_163 * L_1 = ((GUIContent_t6_163_StaticFields*)GUIContent_t6_163_il2cpp_TypeInfo_var->static_fields)->___none_6;
		GUIStyle_t6_166 * L_2 = ___style;
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t6_160_il2cpp_TypeInfo_var);
		bool L_3 = GUI_DoRepeatButton_m6_1051(NULL /*static, unused*/, L_0, L_1, L_2, 2, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_0094;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t6_160_il2cpp_TypeInfo_var);
		int32_t L_4 = ((GUI_t6_160_StaticFields*)GUI_t6_160_il2cpp_TypeInfo_var->static_fields)->___s_ScrollControlId_1;
		int32_t L_5 = ___scrollerID;
		V_1 = ((((int32_t)((((int32_t)L_4) == ((int32_t)L_5))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		int32_t L_6 = ___scrollerID;
		((GUI_t6_160_StaticFields*)GUI_t6_160_il2cpp_TypeInfo_var->static_fields)->___s_ScrollControlId_1 = L_6;
		bool L_7 = V_1;
		if (!L_7)
		{
			goto IL_004e;
		}
	}
	{
		V_0 = 1;
		IL2CPP_RUNTIME_CLASS_INIT(DateTime_t1_127_il2cpp_TypeInfo_var);
		DateTime_t1_127  L_8 = DateTime_get_Now_m1_4932(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_2 = L_8;
		DateTime_t1_127  L_9 = DateTime_AddMilliseconds_m1_4940((&V_2), (250.0), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t6_160_il2cpp_TypeInfo_var);
		GUI_set_nextScrollStepTime_m6_1037(NULL /*static, unused*/, L_9, /*hidden argument*/NULL);
		goto IL_007f;
	}

IL_004e:
	{
		IL2CPP_RUNTIME_CLASS_INIT(DateTime_t1_127_il2cpp_TypeInfo_var);
		DateTime_t1_127  L_10 = DateTime_get_Now_m1_4932(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t6_160_il2cpp_TypeInfo_var);
		DateTime_t1_127  L_11 = GUI_get_nextScrollStepTime_m6_1036(NULL /*static, unused*/, /*hidden argument*/NULL);
		bool L_12 = DateTime_op_GreaterThanOrEqual_m1_4978(NULL /*static, unused*/, L_10, L_11, /*hidden argument*/NULL);
		if (!L_12)
		{
			goto IL_007f;
		}
	}
	{
		V_0 = 1;
		IL2CPP_RUNTIME_CLASS_INIT(DateTime_t1_127_il2cpp_TypeInfo_var);
		DateTime_t1_127  L_13 = DateTime_get_Now_m1_4932(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_3 = L_13;
		DateTime_t1_127  L_14 = DateTime_AddMilliseconds_m1_4940((&V_3), (30.0), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t6_160_il2cpp_TypeInfo_var);
		GUI_set_nextScrollStepTime_m6_1037(NULL /*static, unused*/, L_14, /*hidden argument*/NULL);
	}

IL_007f:
	{
		Event_t6_154 * L_15 = Event_get_current_m6_989(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_15);
		int32_t L_16 = Event_get_type_m6_1003(L_15, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_16) == ((uint32_t)7))))
		{
			goto IL_0094;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t6_160_il2cpp_TypeInfo_var);
		GUI_InternalRepaintEditorWindow_m6_1094(NULL /*static, unused*/, /*hidden argument*/NULL);
	}

IL_0094:
	{
		bool L_17 = V_0;
		return L_17;
	}
}
// System.Single UnityEngine.GUI::VerticalScrollbar(UnityEngine.Rect,System.Single,System.Single,System.Single,System.Single,UnityEngine.GUIStyle)
extern TypeInfo* GUI_t6_160_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2781;
extern Il2CppCodeGenString* _stringLiteral2784;
extern Il2CppCodeGenString* _stringLiteral2785;
extern "C" float GUI_VerticalScrollbar_m6_1069 (Object_t * __this /* static, unused */, Rect_t6_52  ___position, float ___value, float ___size, float ___topValue, float ___bottomValue, GUIStyle_t6_166 * ___style, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GUI_t6_160_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(982);
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(11);
		_stringLiteral2781 = il2cpp_codegen_string_literal_from_index(2781);
		_stringLiteral2784 = il2cpp_codegen_string_literal_from_index(2784);
		_stringLiteral2785 = il2cpp_codegen_string_literal_from_index(2785);
		s_Il2CppMethodIntialized = true;
	}
	{
		Rect_t6_52  L_0 = ___position;
		float L_1 = ___value;
		float L_2 = ___size;
		float L_3 = ___topValue;
		float L_4 = ___bottomValue;
		GUIStyle_t6_166 * L_5 = ___style;
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t6_160_il2cpp_TypeInfo_var);
		GUISkin_t6_161 * L_6 = GUI_get_skin_m6_1041(NULL /*static, unused*/, /*hidden argument*/NULL);
		GUIStyle_t6_166 * L_7 = ___style;
		NullCheck(L_7);
		String_t* L_8 = GUIStyle_get_name_m6_1327(L_7, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_9 = String_Concat_m1_418(NULL /*static, unused*/, L_8, _stringLiteral2781, /*hidden argument*/NULL);
		NullCheck(L_6);
		GUIStyle_t6_166 * L_10 = GUISkin_GetStyle_m6_1265(L_6, L_9, /*hidden argument*/NULL);
		GUISkin_t6_161 * L_11 = GUI_get_skin_m6_1041(NULL /*static, unused*/, /*hidden argument*/NULL);
		GUIStyle_t6_166 * L_12 = ___style;
		NullCheck(L_12);
		String_t* L_13 = GUIStyle_get_name_m6_1327(L_12, /*hidden argument*/NULL);
		String_t* L_14 = String_Concat_m1_418(NULL /*static, unused*/, L_13, _stringLiteral2784, /*hidden argument*/NULL);
		NullCheck(L_11);
		GUIStyle_t6_166 * L_15 = GUISkin_GetStyle_m6_1265(L_11, L_14, /*hidden argument*/NULL);
		GUISkin_t6_161 * L_16 = GUI_get_skin_m6_1041(NULL /*static, unused*/, /*hidden argument*/NULL);
		GUIStyle_t6_166 * L_17 = ___style;
		NullCheck(L_17);
		String_t* L_18 = GUIStyle_get_name_m6_1327(L_17, /*hidden argument*/NULL);
		String_t* L_19 = String_Concat_m1_418(NULL /*static, unused*/, L_18, _stringLiteral2785, /*hidden argument*/NULL);
		NullCheck(L_16);
		GUIStyle_t6_166 * L_20 = GUISkin_GetStyle_m6_1265(L_16, L_19, /*hidden argument*/NULL);
		float L_21 = GUI_Scroller_m6_1070(NULL /*static, unused*/, L_0, L_1, L_2, L_3, L_4, L_5, L_10, L_15, L_20, 0, /*hidden argument*/NULL);
		return L_21;
	}
}
// System.Single UnityEngine.GUI::Scroller(UnityEngine.Rect,System.Single,System.Single,System.Single,System.Single,UnityEngine.GUIStyle,UnityEngine.GUIStyle,UnityEngine.GUIStyle,UnityEngine.GUIStyle,System.Boolean)
extern TypeInfo* GUIUtility_t6_185_il2cpp_TypeInfo_var;
extern TypeInfo* GUI_t6_160_il2cpp_TypeInfo_var;
extern TypeInfo* Mathf_t6_58_il2cpp_TypeInfo_var;
extern "C" float GUI_Scroller_m6_1070 (Object_t * __this /* static, unused */, Rect_t6_52  ___position, float ___value, float ___size, float ___leftValue, float ___rightValue, GUIStyle_t6_166 * ___slider, GUIStyle_t6_166 * ___thumb, GUIStyle_t6_166 * ___leftButton, GUIStyle_t6_166 * ___rightButton, bool ___horiz, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GUIUtility_t6_185_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(984);
		GUI_t6_160_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(982);
		Mathf_t6_58_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(940);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	Rect_t6_52  V_1 = {0};
	Rect_t6_52  V_2 = {0};
	Rect_t6_52  V_3 = {0};
	bool V_4 = false;
	float G_B8_0 = 0.0f;
	float G_B8_1 = 0.0f;
	float G_B7_0 = 0.0f;
	float G_B7_1 = 0.0f;
	float G_B9_0 = 0.0f;
	float G_B9_1 = 0.0f;
	float G_B9_2 = 0.0f;
	float G_B13_0 = 0.0f;
	float G_B13_1 = 0.0f;
	float G_B12_0 = 0.0f;
	float G_B12_1 = 0.0f;
	float G_B14_0 = 0.0f;
	float G_B14_1 = 0.0f;
	float G_B14_2 = 0.0f;
	{
		IL2CPP_RUNTIME_CLASS_INIT(GUIUtility_t6_185_il2cpp_TypeInfo_var);
		GUIUtility_CheckOnGUI_m6_1376(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t6_160_il2cpp_TypeInfo_var);
		int32_t L_0 = ((GUI_t6_160_StaticFields*)GUI_t6_160_il2cpp_TypeInfo_var->static_fields)->___s_SliderHash_7;
		Rect_t6_52  L_1 = ___position;
		int32_t L_2 = GUIUtility_GetControlID_m6_1368(NULL /*static, unused*/, L_0, 2, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		bool L_3 = ___horiz;
		if (!L_3)
		{
			goto IL_00a7;
		}
	}
	{
		float L_4 = Rect_get_x_m6_273((&___position), /*hidden argument*/NULL);
		GUIStyle_t6_166 * L_5 = ___leftButton;
		NullCheck(L_5);
		float L_6 = GUIStyle_get_fixedWidth_m6_1342(L_5, /*hidden argument*/NULL);
		float L_7 = Rect_get_y_m6_275((&___position), /*hidden argument*/NULL);
		float L_8 = Rect_get_width_m6_277((&___position), /*hidden argument*/NULL);
		GUIStyle_t6_166 * L_9 = ___leftButton;
		NullCheck(L_9);
		float L_10 = GUIStyle_get_fixedWidth_m6_1342(L_9, /*hidden argument*/NULL);
		GUIStyle_t6_166 * L_11 = ___rightButton;
		NullCheck(L_11);
		float L_12 = GUIStyle_get_fixedWidth_m6_1342(L_11, /*hidden argument*/NULL);
		float L_13 = Rect_get_height_m6_279((&___position), /*hidden argument*/NULL);
		Rect__ctor_m6_270((&V_1), ((float)((float)L_4+(float)L_6)), L_7, ((float)((float)((float)((float)L_8-(float)L_10))-(float)L_12)), L_13, /*hidden argument*/NULL);
		float L_14 = Rect_get_x_m6_273((&___position), /*hidden argument*/NULL);
		float L_15 = Rect_get_y_m6_275((&___position), /*hidden argument*/NULL);
		GUIStyle_t6_166 * L_16 = ___leftButton;
		NullCheck(L_16);
		float L_17 = GUIStyle_get_fixedWidth_m6_1342(L_16, /*hidden argument*/NULL);
		float L_18 = Rect_get_height_m6_279((&___position), /*hidden argument*/NULL);
		Rect__ctor_m6_270((&V_2), L_14, L_15, L_17, L_18, /*hidden argument*/NULL);
		float L_19 = Rect_get_xMax_m6_283((&___position), /*hidden argument*/NULL);
		GUIStyle_t6_166 * L_20 = ___rightButton;
		NullCheck(L_20);
		float L_21 = GUIStyle_get_fixedWidth_m6_1342(L_20, /*hidden argument*/NULL);
		float L_22 = Rect_get_y_m6_275((&___position), /*hidden argument*/NULL);
		GUIStyle_t6_166 * L_23 = ___rightButton;
		NullCheck(L_23);
		float L_24 = GUIStyle_get_fixedWidth_m6_1342(L_23, /*hidden argument*/NULL);
		float L_25 = Rect_get_height_m6_279((&___position), /*hidden argument*/NULL);
		Rect__ctor_m6_270((&V_3), ((float)((float)L_19-(float)L_21)), L_22, L_24, L_25, /*hidden argument*/NULL);
		goto IL_0130;
	}

IL_00a7:
	{
		float L_26 = Rect_get_x_m6_273((&___position), /*hidden argument*/NULL);
		float L_27 = Rect_get_y_m6_275((&___position), /*hidden argument*/NULL);
		GUIStyle_t6_166 * L_28 = ___leftButton;
		NullCheck(L_28);
		float L_29 = GUIStyle_get_fixedHeight_m6_1343(L_28, /*hidden argument*/NULL);
		float L_30 = Rect_get_width_m6_277((&___position), /*hidden argument*/NULL);
		float L_31 = Rect_get_height_m6_279((&___position), /*hidden argument*/NULL);
		GUIStyle_t6_166 * L_32 = ___leftButton;
		NullCheck(L_32);
		float L_33 = GUIStyle_get_fixedHeight_m6_1343(L_32, /*hidden argument*/NULL);
		GUIStyle_t6_166 * L_34 = ___rightButton;
		NullCheck(L_34);
		float L_35 = GUIStyle_get_fixedHeight_m6_1343(L_34, /*hidden argument*/NULL);
		Rect__ctor_m6_270((&V_1), L_26, ((float)((float)L_27+(float)L_29)), L_30, ((float)((float)((float)((float)L_31-(float)L_33))-(float)L_35)), /*hidden argument*/NULL);
		float L_36 = Rect_get_x_m6_273((&___position), /*hidden argument*/NULL);
		float L_37 = Rect_get_y_m6_275((&___position), /*hidden argument*/NULL);
		float L_38 = Rect_get_width_m6_277((&___position), /*hidden argument*/NULL);
		GUIStyle_t6_166 * L_39 = ___leftButton;
		NullCheck(L_39);
		float L_40 = GUIStyle_get_fixedHeight_m6_1343(L_39, /*hidden argument*/NULL);
		Rect__ctor_m6_270((&V_2), L_36, L_37, L_38, L_40, /*hidden argument*/NULL);
		float L_41 = Rect_get_x_m6_273((&___position), /*hidden argument*/NULL);
		float L_42 = Rect_get_yMax_m6_284((&___position), /*hidden argument*/NULL);
		GUIStyle_t6_166 * L_43 = ___rightButton;
		NullCheck(L_43);
		float L_44 = GUIStyle_get_fixedHeight_m6_1343(L_43, /*hidden argument*/NULL);
		float L_45 = Rect_get_width_m6_277((&___position), /*hidden argument*/NULL);
		GUIStyle_t6_166 * L_46 = ___rightButton;
		NullCheck(L_46);
		float L_47 = GUIStyle_get_fixedHeight_m6_1343(L_46, /*hidden argument*/NULL);
		Rect__ctor_m6_270((&V_3), L_41, ((float)((float)L_42-(float)L_44)), L_45, L_47, /*hidden argument*/NULL);
	}

IL_0130:
	{
		Rect_t6_52  L_48 = V_1;
		float L_49 = ___value;
		float L_50 = ___size;
		float L_51 = ___leftValue;
		float L_52 = ___rightValue;
		GUIStyle_t6_166 * L_53 = ___slider;
		GUIStyle_t6_166 * L_54 = ___thumb;
		bool L_55 = ___horiz;
		int32_t L_56 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t6_160_il2cpp_TypeInfo_var);
		float L_57 = GUI_Slider_m6_1066(NULL /*static, unused*/, L_48, L_49, L_50, L_51, L_52, L_53, L_54, L_55, L_56, /*hidden argument*/NULL);
		___value = L_57;
		V_4 = 0;
		Event_t6_154 * L_58 = Event_get_current_m6_989(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_58);
		int32_t L_59 = Event_get_type_m6_1003(L_58, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_59) == ((uint32_t)1))))
		{
			goto IL_015a;
		}
	}
	{
		V_4 = 1;
	}

IL_015a:
	{
		int32_t L_60 = V_0;
		Rect_t6_52  L_61 = V_2;
		GUIStyle_t6_166 * L_62 = ___leftButton;
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t6_160_il2cpp_TypeInfo_var);
		bool L_63 = GUI_ScrollerRepeatButton_m6_1068(NULL /*static, unused*/, L_60, L_61, L_62, /*hidden argument*/NULL);
		if (!L_63)
		{
			goto IL_0189;
		}
	}
	{
		float L_64 = ___value;
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t6_160_il2cpp_TypeInfo_var);
		float L_65 = ((GUI_t6_160_StaticFields*)GUI_t6_160_il2cpp_TypeInfo_var->static_fields)->___s_ScrollStepSize_0;
		float L_66 = ___leftValue;
		float L_67 = ___rightValue;
		G_B7_0 = L_65;
		G_B7_1 = L_64;
		if ((!(((float)L_66) < ((float)L_67))))
		{
			G_B8_0 = L_65;
			G_B8_1 = L_64;
			goto IL_0180;
		}
	}
	{
		G_B9_0 = (1.0f);
		G_B9_1 = G_B7_0;
		G_B9_2 = G_B7_1;
		goto IL_0185;
	}

IL_0180:
	{
		G_B9_0 = (-1.0f);
		G_B9_1 = G_B8_0;
		G_B9_2 = G_B8_1;
	}

IL_0185:
	{
		___value = ((float)((float)G_B9_2-(float)((float)((float)G_B9_1*(float)G_B9_0))));
	}

IL_0189:
	{
		int32_t L_68 = V_0;
		Rect_t6_52  L_69 = V_3;
		GUIStyle_t6_166 * L_70 = ___rightButton;
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t6_160_il2cpp_TypeInfo_var);
		bool L_71 = GUI_ScrollerRepeatButton_m6_1068(NULL /*static, unused*/, L_68, L_69, L_70, /*hidden argument*/NULL);
		if (!L_71)
		{
			goto IL_01b8;
		}
	}
	{
		float L_72 = ___value;
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t6_160_il2cpp_TypeInfo_var);
		float L_73 = ((GUI_t6_160_StaticFields*)GUI_t6_160_il2cpp_TypeInfo_var->static_fields)->___s_ScrollStepSize_0;
		float L_74 = ___leftValue;
		float L_75 = ___rightValue;
		G_B12_0 = L_73;
		G_B12_1 = L_72;
		if ((!(((float)L_74) < ((float)L_75))))
		{
			G_B13_0 = L_73;
			G_B13_1 = L_72;
			goto IL_01af;
		}
	}
	{
		G_B14_0 = (1.0f);
		G_B14_1 = G_B12_0;
		G_B14_2 = G_B12_1;
		goto IL_01b4;
	}

IL_01af:
	{
		G_B14_0 = (-1.0f);
		G_B14_1 = G_B13_0;
		G_B14_2 = G_B13_1;
	}

IL_01b4:
	{
		___value = ((float)((float)G_B14_2+(float)((float)((float)G_B14_1*(float)G_B14_0))));
	}

IL_01b8:
	{
		bool L_76 = V_4;
		if (!L_76)
		{
			goto IL_01d6;
		}
	}
	{
		Event_t6_154 * L_77 = Event_get_current_m6_989(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_77);
		int32_t L_78 = Event_get_type_m6_1003(L_77, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_78) == ((uint32_t)((int32_t)12)))))
		{
			goto IL_01d6;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t6_160_il2cpp_TypeInfo_var);
		((GUI_t6_160_StaticFields*)GUI_t6_160_il2cpp_TypeInfo_var->static_fields)->___s_ScrollControlId_1 = 0;
	}

IL_01d6:
	{
		float L_79 = ___leftValue;
		float L_80 = ___rightValue;
		if ((!(((float)L_79) < ((float)L_80))))
		{
			goto IL_01f0;
		}
	}
	{
		float L_81 = ___value;
		float L_82 = ___leftValue;
		float L_83 = ___rightValue;
		float L_84 = ___size;
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t6_58_il2cpp_TypeInfo_var);
		float L_85 = Mathf_Clamp_m6_389(NULL /*static, unused*/, L_81, L_82, ((float)((float)L_83-(float)L_84)), /*hidden argument*/NULL);
		___value = L_85;
		goto IL_01fd;
	}

IL_01f0:
	{
		float L_86 = ___value;
		float L_87 = ___rightValue;
		float L_88 = ___leftValue;
		float L_89 = ___size;
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t6_58_il2cpp_TypeInfo_var);
		float L_90 = Mathf_Clamp_m6_389(NULL /*static, unused*/, L_86, L_87, ((float)((float)L_88-(float)L_89)), /*hidden argument*/NULL);
		___value = L_90;
	}

IL_01fd:
	{
		float L_91 = ___value;
		return L_91;
	}
}
// System.Void UnityEngine.GUI::BeginGroup(UnityEngine.Rect,UnityEngine.GUIContent,UnityEngine.GUIStyle)
extern TypeInfo* GUIUtility_t6_185_il2cpp_TypeInfo_var;
extern TypeInfo* GUI_t6_160_il2cpp_TypeInfo_var;
extern TypeInfo* GUIContent_t6_163_il2cpp_TypeInfo_var;
extern TypeInfo* GUIStyle_t6_166_il2cpp_TypeInfo_var;
extern "C" void GUI_BeginGroup_m6_1071 (Object_t * __this /* static, unused */, Rect_t6_52  ___position, GUIContent_t6_163 * ___content, GUIStyle_t6_166 * ___style, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GUIUtility_t6_185_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(984);
		GUI_t6_160_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(982);
		GUIContent_t6_163_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(985);
		GUIStyle_t6_166_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(988);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = {0};
	{
		IL2CPP_RUNTIME_CLASS_INIT(GUIUtility_t6_185_il2cpp_TypeInfo_var);
		GUIUtility_CheckOnGUI_m6_1376(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t6_160_il2cpp_TypeInfo_var);
		int32_t L_0 = ((GUI_t6_160_StaticFields*)GUI_t6_160_il2cpp_TypeInfo_var->static_fields)->___s_BeginGroupHash_8;
		int32_t L_1 = GUIUtility_GetControlID_m6_1378(NULL /*static, unused*/, L_0, 2, /*hidden argument*/NULL);
		V_0 = L_1;
		GUIContent_t6_163 * L_2 = ___content;
		IL2CPP_RUNTIME_CLASS_INIT(GUIContent_t6_163_il2cpp_TypeInfo_var);
		GUIContent_t6_163 * L_3 = ((GUIContent_t6_163_StaticFields*)GUIContent_t6_163_il2cpp_TypeInfo_var->static_fields)->___none_6;
		if ((!(((Object_t*)(GUIContent_t6_163 *)L_2) == ((Object_t*)(GUIContent_t6_163 *)L_3))))
		{
			goto IL_0027;
		}
	}
	{
		GUIStyle_t6_166 * L_4 = ___style;
		IL2CPP_RUNTIME_CLASS_INIT(GUIStyle_t6_166_il2cpp_TypeInfo_var);
		GUIStyle_t6_166 * L_5 = GUIStyle_get_none_m6_1316(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((((Object_t*)(GUIStyle_t6_166 *)L_4) == ((Object_t*)(GUIStyle_t6_166 *)L_5)))
		{
			goto IL_006d;
		}
	}

IL_0027:
	{
		Event_t6_154 * L_6 = Event_get_current_m6_989(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_6);
		int32_t L_7 = Event_get_type_m6_1003(L_6, /*hidden argument*/NULL);
		V_1 = L_7;
		int32_t L_8 = V_1;
		if ((((int32_t)L_8) == ((int32_t)7)))
		{
			goto IL_003e;
		}
	}
	{
		goto IL_004c;
	}

IL_003e:
	{
		GUIStyle_t6_166 * L_9 = ___style;
		Rect_t6_52  L_10 = ___position;
		GUIContent_t6_163 * L_11 = ___content;
		int32_t L_12 = V_0;
		NullCheck(L_9);
		GUIStyle_Draw_m6_1311(L_9, L_10, L_11, L_12, /*hidden argument*/NULL);
		goto IL_006d;
	}

IL_004c:
	{
		Event_t6_154 * L_13 = Event_get_current_m6_989(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_13);
		Vector2_t6_48  L_14 = Event_get_mousePosition_m6_970(L_13, /*hidden argument*/NULL);
		bool L_15 = Rect_Contains_m6_286((&___position), L_14, /*hidden argument*/NULL);
		if (!L_15)
		{
			goto IL_0068;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GUIUtility_t6_185_il2cpp_TypeInfo_var);
		GUIUtility_set_mouseUsed_m6_1391(NULL /*static, unused*/, 1, /*hidden argument*/NULL);
	}

IL_0068:
	{
		goto IL_006d;
	}

IL_006d:
	{
		Rect_t6_52  L_16 = ___position;
		Vector2_t6_48  L_17 = Vector2_get_zero_m6_182(NULL /*static, unused*/, /*hidden argument*/NULL);
		Vector2_t6_48  L_18 = Vector2_get_zero_m6_182(NULL /*static, unused*/, /*hidden argument*/NULL);
		GUIClip_Push_m6_1393(NULL /*static, unused*/, L_16, L_17, L_18, 0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.GUI::EndGroup()
extern TypeInfo* GUIUtility_t6_185_il2cpp_TypeInfo_var;
extern "C" void GUI_EndGroup_m6_1072 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GUIUtility_t6_185_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(984);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GUIUtility_t6_185_il2cpp_TypeInfo_var);
		GUIUtility_CheckOnGUI_m6_1376(NULL /*static, unused*/, /*hidden argument*/NULL);
		GUIClip_Pop_m6_1395(NULL /*static, unused*/, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Vector2 UnityEngine.GUI::BeginScrollView(UnityEngine.Rect,UnityEngine.Vector2,UnityEngine.Rect,System.Boolean,System.Boolean,UnityEngine.GUIStyle,UnityEngine.GUIStyle,UnityEngine.GUIStyle)
extern const Il2CppType* ScrollViewState_t6_158_0_0_0_var;
extern TypeInfo* GUIUtility_t6_185_il2cpp_TypeInfo_var;
extern TypeInfo* GUI_t6_160_il2cpp_TypeInfo_var;
extern TypeInfo* Type_t_il2cpp_TypeInfo_var;
extern TypeInfo* ScrollViewState_t6_158_il2cpp_TypeInfo_var;
extern TypeInfo* GUIStyle_t6_166_il2cpp_TypeInfo_var;
extern TypeInfo* Mathf_t6_58_il2cpp_TypeInfo_var;
extern "C" Vector2_t6_48  GUI_BeginScrollView_m6_1073 (Object_t * __this /* static, unused */, Rect_t6_52  ___position, Vector2_t6_48  ___scrollPosition, Rect_t6_52  ___viewRect, bool ___alwaysShowHorizontal, bool ___alwaysShowVertical, GUIStyle_t6_166 * ___horizontalScrollbar, GUIStyle_t6_166 * ___verticalScrollbar, GUIStyle_t6_166 * ___background, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ScrollViewState_t6_158_0_0_0_var = il2cpp_codegen_type_from_index(989);
		GUIUtility_t6_185_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(984);
		GUI_t6_160_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(982);
		Type_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3);
		ScrollViewState_t6_158_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(989);
		GUIStyle_t6_166_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(988);
		Mathf_t6_58_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(940);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	ScrollViewState_t6_158 * V_1 = {0};
	Rect_t6_52  V_2 = {0};
	bool V_3 = false;
	bool V_4 = false;
	Rect_t6_52  V_5 = {0};
	int32_t V_6 = {0};
	int32_t G_B19_0 = 0;
	bool G_B19_1 = false;
	Rect_t6_52  G_B19_2 = {0};
	GUIStyle_t6_166 * G_B19_3 = {0};
	int32_t G_B18_0 = 0;
	bool G_B18_1 = false;
	Rect_t6_52  G_B18_2 = {0};
	GUIStyle_t6_166 * G_B18_3 = {0};
	int32_t G_B20_0 = 0;
	int32_t G_B20_1 = 0;
	bool G_B20_2 = false;
	Rect_t6_52  G_B20_3 = {0};
	GUIStyle_t6_166 * G_B20_4 = {0};
	{
		IL2CPP_RUNTIME_CLASS_INIT(GUIUtility_t6_185_il2cpp_TypeInfo_var);
		GUIUtility_CheckOnGUI_m6_1376(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t6_160_il2cpp_TypeInfo_var);
		int32_t L_0 = ((GUI_t6_160_StaticFields*)GUI_t6_160_il2cpp_TypeInfo_var->static_fields)->___s_ScrollviewHash_9;
		int32_t L_1 = GUIUtility_GetControlID_m6_1378(NULL /*static, unused*/, L_0, 2, /*hidden argument*/NULL);
		V_0 = L_1;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_2 = Type_GetTypeFromHandle_m1_893(NULL /*static, unused*/, LoadTypeToken(ScrollViewState_t6_158_0_0_0_var), /*hidden argument*/NULL);
		int32_t L_3 = V_0;
		Object_t * L_4 = GUIUtility_GetStateObject_m6_1369(NULL /*static, unused*/, L_2, L_3, /*hidden argument*/NULL);
		V_1 = ((ScrollViewState_t6_158 *)CastclassSealed(L_4, ScrollViewState_t6_158_il2cpp_TypeInfo_var));
		ScrollViewState_t6_158 * L_5 = V_1;
		NullCheck(L_5);
		bool L_6 = (L_5->___apply_4);
		if (!L_6)
		{
			goto IL_0041;
		}
	}
	{
		ScrollViewState_t6_158 * L_7 = V_1;
		NullCheck(L_7);
		Vector2_t6_48  L_8 = (L_7->___scrollPosition_3);
		___scrollPosition = L_8;
		ScrollViewState_t6_158 * L_9 = V_1;
		NullCheck(L_9);
		L_9->___apply_4 = 0;
	}

IL_0041:
	{
		ScrollViewState_t6_158 * L_10 = V_1;
		Rect_t6_52  L_11 = ___position;
		NullCheck(L_10);
		L_10->___position_0 = L_11;
		ScrollViewState_t6_158 * L_12 = V_1;
		Vector2_t6_48  L_13 = ___scrollPosition;
		NullCheck(L_12);
		L_12->___scrollPosition_3 = L_13;
		ScrollViewState_t6_158 * L_14 = V_1;
		ScrollViewState_t6_158 * L_15 = V_1;
		Rect_t6_52  L_16 = ___viewRect;
		Rect_t6_52  L_17 = L_16;
		V_5 = L_17;
		NullCheck(L_15);
		L_15->___viewRect_2 = L_17;
		Rect_t6_52  L_18 = V_5;
		NullCheck(L_14);
		L_14->___visibleRect_1 = L_18;
		ScrollViewState_t6_158 * L_19 = V_1;
		NullCheck(L_19);
		Rect_t6_52 * L_20 = &(L_19->___visibleRect_1);
		float L_21 = Rect_get_width_m6_277((&___position), /*hidden argument*/NULL);
		Rect_set_width_m6_278(L_20, L_21, /*hidden argument*/NULL);
		ScrollViewState_t6_158 * L_22 = V_1;
		NullCheck(L_22);
		Rect_t6_52 * L_23 = &(L_22->___visibleRect_1);
		float L_24 = Rect_get_height_m6_279((&___position), /*hidden argument*/NULL);
		Rect_set_height_m6_280(L_23, L_24, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t6_160_il2cpp_TypeInfo_var);
		GenericStack_t6_162 * L_25 = ((GUI_t6_160_StaticFields*)GUI_t6_160_il2cpp_TypeInfo_var->static_fields)->___s_ScrollViewStates_12;
		ScrollViewState_t6_158 * L_26 = V_1;
		NullCheck(L_25);
		VirtActionInvoker1< Object_t * >::Invoke(17 /* System.Void System.Collections.Stack::Push(System.Object) */, L_25, L_26);
		Rect_t6_52  L_27 = ___position;
		Rect__ctor_m6_271((&V_2), L_27, /*hidden argument*/NULL);
		Event_t6_154 * L_28 = Event_get_current_m6_989(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_28);
		int32_t L_29 = Event_get_type_m6_1003(L_28, /*hidden argument*/NULL);
		V_6 = L_29;
		int32_t L_30 = V_6;
		if ((((int32_t)L_30) == ((int32_t)8)))
		{
			goto IL_00ba;
		}
	}
	{
		int32_t L_31 = V_6;
		if ((((int32_t)L_31) == ((int32_t)((int32_t)12))))
		{
			goto IL_0107;
		}
	}
	{
		goto IL_010c;
	}

IL_00ba:
	{
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t6_160_il2cpp_TypeInfo_var);
		int32_t L_32 = ((GUI_t6_160_StaticFields*)GUI_t6_160_il2cpp_TypeInfo_var->static_fields)->___s_SliderHash_7;
		IL2CPP_RUNTIME_CLASS_INIT(GUIUtility_t6_185_il2cpp_TypeInfo_var);
		GUIUtility_GetControlID_m6_1378(NULL /*static, unused*/, L_32, 2, /*hidden argument*/NULL);
		int32_t L_33 = ((GUI_t6_160_StaticFields*)GUI_t6_160_il2cpp_TypeInfo_var->static_fields)->___s_RepeatButtonHash_4;
		GUIUtility_GetControlID_m6_1378(NULL /*static, unused*/, L_33, 2, /*hidden argument*/NULL);
		int32_t L_34 = ((GUI_t6_160_StaticFields*)GUI_t6_160_il2cpp_TypeInfo_var->static_fields)->___s_RepeatButtonHash_4;
		GUIUtility_GetControlID_m6_1378(NULL /*static, unused*/, L_34, 2, /*hidden argument*/NULL);
		int32_t L_35 = ((GUI_t6_160_StaticFields*)GUI_t6_160_il2cpp_TypeInfo_var->static_fields)->___s_SliderHash_7;
		GUIUtility_GetControlID_m6_1378(NULL /*static, unused*/, L_35, 2, /*hidden argument*/NULL);
		int32_t L_36 = ((GUI_t6_160_StaticFields*)GUI_t6_160_il2cpp_TypeInfo_var->static_fields)->___s_RepeatButtonHash_4;
		GUIUtility_GetControlID_m6_1378(NULL /*static, unused*/, L_36, 2, /*hidden argument*/NULL);
		int32_t L_37 = ((GUI_t6_160_StaticFields*)GUI_t6_160_il2cpp_TypeInfo_var->static_fields)->___s_RepeatButtonHash_4;
		GUIUtility_GetControlID_m6_1378(NULL /*static, unused*/, L_37, 2, /*hidden argument*/NULL);
		goto IL_044a;
	}

IL_0107:
	{
		goto IL_044a;
	}

IL_010c:
	{
		bool L_38 = ___alwaysShowVertical;
		V_3 = L_38;
		bool L_39 = ___alwaysShowHorizontal;
		V_4 = L_39;
		bool L_40 = V_4;
		if (L_40)
		{
			goto IL_012c;
		}
	}
	{
		float L_41 = Rect_get_width_m6_277((&___viewRect), /*hidden argument*/NULL);
		float L_42 = Rect_get_width_m6_277((&V_2), /*hidden argument*/NULL);
		if ((!(((float)L_41) > ((float)L_42))))
		{
			goto IL_017a;
		}
	}

IL_012c:
	{
		ScrollViewState_t6_158 * L_43 = V_1;
		NullCheck(L_43);
		Rect_t6_52 * L_44 = &(L_43->___visibleRect_1);
		float L_45 = Rect_get_height_m6_279((&___position), /*hidden argument*/NULL);
		GUIStyle_t6_166 * L_46 = ___horizontalScrollbar;
		NullCheck(L_46);
		float L_47 = GUIStyle_get_fixedHeight_m6_1343(L_46, /*hidden argument*/NULL);
		GUIStyle_t6_166 * L_48 = ___horizontalScrollbar;
		NullCheck(L_48);
		RectOffset_t6_172 * L_49 = GUIStyle_get_margin_m6_1303(L_48, /*hidden argument*/NULL);
		NullCheck(L_49);
		int32_t L_50 = RectOffset_get_top_m6_1288(L_49, /*hidden argument*/NULL);
		Rect_set_height_m6_280(L_44, ((float)((float)((float)((float)L_45-(float)L_47))+(float)(((float)((float)L_50))))), /*hidden argument*/NULL);
		Rect_t6_52 * L_51 = (&V_2);
		float L_52 = Rect_get_height_m6_279(L_51, /*hidden argument*/NULL);
		GUIStyle_t6_166 * L_53 = ___horizontalScrollbar;
		NullCheck(L_53);
		float L_54 = GUIStyle_get_fixedHeight_m6_1343(L_53, /*hidden argument*/NULL);
		GUIStyle_t6_166 * L_55 = ___horizontalScrollbar;
		NullCheck(L_55);
		RectOffset_t6_172 * L_56 = GUIStyle_get_margin_m6_1303(L_55, /*hidden argument*/NULL);
		NullCheck(L_56);
		int32_t L_57 = RectOffset_get_top_m6_1288(L_56, /*hidden argument*/NULL);
		Rect_set_height_m6_280(L_51, ((float)((float)L_52-(float)((float)((float)L_54+(float)(((float)((float)L_57))))))), /*hidden argument*/NULL);
		V_4 = 1;
	}

IL_017a:
	{
		bool L_58 = V_3;
		if (L_58)
		{
			goto IL_0193;
		}
	}
	{
		float L_59 = Rect_get_height_m6_279((&___viewRect), /*hidden argument*/NULL);
		float L_60 = Rect_get_height_m6_279((&V_2), /*hidden argument*/NULL);
		if ((!(((float)L_59) > ((float)L_60))))
		{
			goto IL_0248;
		}
	}

IL_0193:
	{
		ScrollViewState_t6_158 * L_61 = V_1;
		NullCheck(L_61);
		Rect_t6_52 * L_62 = &(L_61->___visibleRect_1);
		float L_63 = Rect_get_width_m6_277((&___position), /*hidden argument*/NULL);
		GUIStyle_t6_166 * L_64 = ___verticalScrollbar;
		NullCheck(L_64);
		float L_65 = GUIStyle_get_fixedWidth_m6_1342(L_64, /*hidden argument*/NULL);
		GUIStyle_t6_166 * L_66 = ___verticalScrollbar;
		NullCheck(L_66);
		RectOffset_t6_172 * L_67 = GUIStyle_get_margin_m6_1303(L_66, /*hidden argument*/NULL);
		NullCheck(L_67);
		int32_t L_68 = RectOffset_get_left_m6_1284(L_67, /*hidden argument*/NULL);
		Rect_set_width_m6_278(L_62, ((float)((float)((float)((float)L_63-(float)L_65))+(float)(((float)((float)L_68))))), /*hidden argument*/NULL);
		Rect_t6_52 * L_69 = (&V_2);
		float L_70 = Rect_get_width_m6_277(L_69, /*hidden argument*/NULL);
		GUIStyle_t6_166 * L_71 = ___verticalScrollbar;
		NullCheck(L_71);
		float L_72 = GUIStyle_get_fixedWidth_m6_1342(L_71, /*hidden argument*/NULL);
		GUIStyle_t6_166 * L_73 = ___verticalScrollbar;
		NullCheck(L_73);
		RectOffset_t6_172 * L_74 = GUIStyle_get_margin_m6_1303(L_73, /*hidden argument*/NULL);
		NullCheck(L_74);
		int32_t L_75 = RectOffset_get_left_m6_1284(L_74, /*hidden argument*/NULL);
		Rect_set_width_m6_278(L_69, ((float)((float)L_70-(float)((float)((float)L_72+(float)(((float)((float)L_75))))))), /*hidden argument*/NULL);
		V_3 = 1;
		bool L_76 = V_4;
		if (L_76)
		{
			goto IL_0248;
		}
	}
	{
		float L_77 = Rect_get_width_m6_277((&___viewRect), /*hidden argument*/NULL);
		float L_78 = Rect_get_width_m6_277((&V_2), /*hidden argument*/NULL);
		if ((!(((float)L_77) > ((float)L_78))))
		{
			goto IL_0248;
		}
	}
	{
		ScrollViewState_t6_158 * L_79 = V_1;
		NullCheck(L_79);
		Rect_t6_52 * L_80 = &(L_79->___visibleRect_1);
		float L_81 = Rect_get_height_m6_279((&___position), /*hidden argument*/NULL);
		GUIStyle_t6_166 * L_82 = ___horizontalScrollbar;
		NullCheck(L_82);
		float L_83 = GUIStyle_get_fixedHeight_m6_1343(L_82, /*hidden argument*/NULL);
		GUIStyle_t6_166 * L_84 = ___horizontalScrollbar;
		NullCheck(L_84);
		RectOffset_t6_172 * L_85 = GUIStyle_get_margin_m6_1303(L_84, /*hidden argument*/NULL);
		NullCheck(L_85);
		int32_t L_86 = RectOffset_get_top_m6_1288(L_85, /*hidden argument*/NULL);
		Rect_set_height_m6_280(L_80, ((float)((float)((float)((float)L_81-(float)L_83))+(float)(((float)((float)L_86))))), /*hidden argument*/NULL);
		Rect_t6_52 * L_87 = (&V_2);
		float L_88 = Rect_get_height_m6_279(L_87, /*hidden argument*/NULL);
		GUIStyle_t6_166 * L_89 = ___horizontalScrollbar;
		NullCheck(L_89);
		float L_90 = GUIStyle_get_fixedHeight_m6_1343(L_89, /*hidden argument*/NULL);
		GUIStyle_t6_166 * L_91 = ___horizontalScrollbar;
		NullCheck(L_91);
		RectOffset_t6_172 * L_92 = GUIStyle_get_margin_m6_1303(L_91, /*hidden argument*/NULL);
		NullCheck(L_92);
		int32_t L_93 = RectOffset_get_top_m6_1288(L_92, /*hidden argument*/NULL);
		Rect_set_height_m6_280(L_87, ((float)((float)L_88-(float)((float)((float)L_90+(float)(((float)((float)L_93))))))), /*hidden argument*/NULL);
		V_4 = 1;
	}

IL_0248:
	{
		Event_t6_154 * L_94 = Event_get_current_m6_989(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_94);
		int32_t L_95 = Event_get_type_m6_1003(L_94, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_95) == ((uint32_t)7))))
		{
			goto IL_028a;
		}
	}
	{
		GUIStyle_t6_166 * L_96 = ___background;
		IL2CPP_RUNTIME_CLASS_INIT(GUIStyle_t6_166_il2cpp_TypeInfo_var);
		GUIStyle_t6_166 * L_97 = GUIStyle_get_none_m6_1316(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((((Object_t*)(GUIStyle_t6_166 *)L_96) == ((Object_t*)(GUIStyle_t6_166 *)L_97)))
		{
			goto IL_028a;
		}
	}
	{
		GUIStyle_t6_166 * L_98 = ___background;
		Rect_t6_52  L_99 = ___position;
		Event_t6_154 * L_100 = Event_get_current_m6_989(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_100);
		Vector2_t6_48  L_101 = Event_get_mousePosition_m6_970(L_100, /*hidden argument*/NULL);
		bool L_102 = Rect_Contains_m6_286((&___position), L_101, /*hidden argument*/NULL);
		bool L_103 = V_4;
		G_B18_0 = 0;
		G_B18_1 = L_102;
		G_B18_2 = L_99;
		G_B18_3 = L_98;
		if (!L_103)
		{
			G_B19_0 = 0;
			G_B19_1 = L_102;
			G_B19_2 = L_99;
			G_B19_3 = L_98;
			goto IL_0283;
		}
	}
	{
		bool L_104 = V_3;
		G_B20_0 = ((int32_t)(L_104));
		G_B20_1 = G_B18_0;
		G_B20_2 = G_B18_1;
		G_B20_3 = G_B18_2;
		G_B20_4 = G_B18_3;
		goto IL_0284;
	}

IL_0283:
	{
		G_B20_0 = 0;
		G_B20_1 = G_B19_0;
		G_B20_2 = G_B19_1;
		G_B20_3 = G_B19_2;
		G_B20_4 = G_B19_3;
	}

IL_0284:
	{
		NullCheck(G_B20_4);
		GUIStyle_Draw_m6_1309(G_B20_4, G_B20_3, G_B20_2, G_B20_1, G_B20_0, 0, /*hidden argument*/NULL);
	}

IL_028a:
	{
		bool L_105 = V_4;
		if (!L_105)
		{
			goto IL_02f3;
		}
	}
	{
		GUIStyle_t6_166 * L_106 = ___horizontalScrollbar;
		IL2CPP_RUNTIME_CLASS_INIT(GUIStyle_t6_166_il2cpp_TypeInfo_var);
		GUIStyle_t6_166 * L_107 = GUIStyle_get_none_m6_1316(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((((Object_t*)(GUIStyle_t6_166 *)L_106) == ((Object_t*)(GUIStyle_t6_166 *)L_107)))
		{
			goto IL_02f3;
		}
	}
	{
		float L_108 = Rect_get_x_m6_273((&___position), /*hidden argument*/NULL);
		float L_109 = Rect_get_yMax_m6_284((&___position), /*hidden argument*/NULL);
		GUIStyle_t6_166 * L_110 = ___horizontalScrollbar;
		NullCheck(L_110);
		float L_111 = GUIStyle_get_fixedHeight_m6_1343(L_110, /*hidden argument*/NULL);
		float L_112 = Rect_get_width_m6_277((&V_2), /*hidden argument*/NULL);
		GUIStyle_t6_166 * L_113 = ___horizontalScrollbar;
		NullCheck(L_113);
		float L_114 = GUIStyle_get_fixedHeight_m6_1343(L_113, /*hidden argument*/NULL);
		Rect_t6_52  L_115 = {0};
		Rect__ctor_m6_270(&L_115, L_108, ((float)((float)L_109-(float)L_111)), L_112, L_114, /*hidden argument*/NULL);
		float L_116 = ((&___scrollPosition)->___x_1);
		float L_117 = Rect_get_width_m6_277((&V_2), /*hidden argument*/NULL);
		float L_118 = Rect_get_width_m6_277((&___viewRect), /*hidden argument*/NULL);
		GUIStyle_t6_166 * L_119 = ___horizontalScrollbar;
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t6_160_il2cpp_TypeInfo_var);
		float L_120 = GUI_HorizontalScrollbar_m6_1067(NULL /*static, unused*/, L_115, L_116, L_117, (0.0f), L_118, L_119, /*hidden argument*/NULL);
		(&___scrollPosition)->___x_1 = L_120;
		goto IL_0365;
	}

IL_02f3:
	{
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t6_160_il2cpp_TypeInfo_var);
		int32_t L_121 = ((GUI_t6_160_StaticFields*)GUI_t6_160_il2cpp_TypeInfo_var->static_fields)->___s_SliderHash_7;
		IL2CPP_RUNTIME_CLASS_INIT(GUIUtility_t6_185_il2cpp_TypeInfo_var);
		GUIUtility_GetControlID_m6_1378(NULL /*static, unused*/, L_121, 2, /*hidden argument*/NULL);
		int32_t L_122 = ((GUI_t6_160_StaticFields*)GUI_t6_160_il2cpp_TypeInfo_var->static_fields)->___s_RepeatButtonHash_4;
		GUIUtility_GetControlID_m6_1378(NULL /*static, unused*/, L_122, 2, /*hidden argument*/NULL);
		int32_t L_123 = ((GUI_t6_160_StaticFields*)GUI_t6_160_il2cpp_TypeInfo_var->static_fields)->___s_RepeatButtonHash_4;
		GUIUtility_GetControlID_m6_1378(NULL /*static, unused*/, L_123, 2, /*hidden argument*/NULL);
		GUIStyle_t6_166 * L_124 = ___horizontalScrollbar;
		IL2CPP_RUNTIME_CLASS_INIT(GUIStyle_t6_166_il2cpp_TypeInfo_var);
		GUIStyle_t6_166 * L_125 = GUIStyle_get_none_m6_1316(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((((Object_t*)(GUIStyle_t6_166 *)L_124) == ((Object_t*)(GUIStyle_t6_166 *)L_125)))
		{
			goto IL_0334;
		}
	}
	{
		(&___scrollPosition)->___x_1 = (0.0f);
		goto IL_0365;
	}

IL_0334:
	{
		float L_126 = ((&___scrollPosition)->___x_1);
		float L_127 = Rect_get_width_m6_277((&___viewRect), /*hidden argument*/NULL);
		float L_128 = Rect_get_width_m6_277((&___position), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t6_58_il2cpp_TypeInfo_var);
		float L_129 = Mathf_Max_m6_383(NULL /*static, unused*/, ((float)((float)L_127-(float)L_128)), (0.0f), /*hidden argument*/NULL);
		float L_130 = Mathf_Clamp_m6_389(NULL /*static, unused*/, L_126, (0.0f), L_129, /*hidden argument*/NULL);
		(&___scrollPosition)->___x_1 = L_130;
	}

IL_0365:
	{
		bool L_131 = V_3;
		if (!L_131)
		{
			goto IL_03d3;
		}
	}
	{
		GUIStyle_t6_166 * L_132 = ___verticalScrollbar;
		IL2CPP_RUNTIME_CLASS_INIT(GUIStyle_t6_166_il2cpp_TypeInfo_var);
		GUIStyle_t6_166 * L_133 = GUIStyle_get_none_m6_1316(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((((Object_t*)(GUIStyle_t6_166 *)L_132) == ((Object_t*)(GUIStyle_t6_166 *)L_133)))
		{
			goto IL_03d3;
		}
	}
	{
		float L_134 = Rect_get_xMax_m6_283((&V_2), /*hidden argument*/NULL);
		GUIStyle_t6_166 * L_135 = ___verticalScrollbar;
		NullCheck(L_135);
		RectOffset_t6_172 * L_136 = GUIStyle_get_margin_m6_1303(L_135, /*hidden argument*/NULL);
		NullCheck(L_136);
		int32_t L_137 = RectOffset_get_left_m6_1284(L_136, /*hidden argument*/NULL);
		float L_138 = Rect_get_y_m6_275((&V_2), /*hidden argument*/NULL);
		GUIStyle_t6_166 * L_139 = ___verticalScrollbar;
		NullCheck(L_139);
		float L_140 = GUIStyle_get_fixedWidth_m6_1342(L_139, /*hidden argument*/NULL);
		float L_141 = Rect_get_height_m6_279((&V_2), /*hidden argument*/NULL);
		Rect_t6_52  L_142 = {0};
		Rect__ctor_m6_270(&L_142, ((float)((float)L_134+(float)(((float)((float)L_137))))), L_138, L_140, L_141, /*hidden argument*/NULL);
		float L_143 = ((&___scrollPosition)->___y_2);
		float L_144 = Rect_get_height_m6_279((&V_2), /*hidden argument*/NULL);
		float L_145 = Rect_get_height_m6_279((&___viewRect), /*hidden argument*/NULL);
		GUIStyle_t6_166 * L_146 = ___verticalScrollbar;
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t6_160_il2cpp_TypeInfo_var);
		float L_147 = GUI_VerticalScrollbar_m6_1069(NULL /*static, unused*/, L_142, L_143, L_144, (0.0f), L_145, L_146, /*hidden argument*/NULL);
		(&___scrollPosition)->___y_2 = L_147;
		goto IL_0445;
	}

IL_03d3:
	{
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t6_160_il2cpp_TypeInfo_var);
		int32_t L_148 = ((GUI_t6_160_StaticFields*)GUI_t6_160_il2cpp_TypeInfo_var->static_fields)->___s_SliderHash_7;
		IL2CPP_RUNTIME_CLASS_INIT(GUIUtility_t6_185_il2cpp_TypeInfo_var);
		GUIUtility_GetControlID_m6_1378(NULL /*static, unused*/, L_148, 2, /*hidden argument*/NULL);
		int32_t L_149 = ((GUI_t6_160_StaticFields*)GUI_t6_160_il2cpp_TypeInfo_var->static_fields)->___s_RepeatButtonHash_4;
		GUIUtility_GetControlID_m6_1378(NULL /*static, unused*/, L_149, 2, /*hidden argument*/NULL);
		int32_t L_150 = ((GUI_t6_160_StaticFields*)GUI_t6_160_il2cpp_TypeInfo_var->static_fields)->___s_RepeatButtonHash_4;
		GUIUtility_GetControlID_m6_1378(NULL /*static, unused*/, L_150, 2, /*hidden argument*/NULL);
		GUIStyle_t6_166 * L_151 = ___verticalScrollbar;
		IL2CPP_RUNTIME_CLASS_INIT(GUIStyle_t6_166_il2cpp_TypeInfo_var);
		GUIStyle_t6_166 * L_152 = GUIStyle_get_none_m6_1316(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((((Object_t*)(GUIStyle_t6_166 *)L_151) == ((Object_t*)(GUIStyle_t6_166 *)L_152)))
		{
			goto IL_0414;
		}
	}
	{
		(&___scrollPosition)->___y_2 = (0.0f);
		goto IL_0445;
	}

IL_0414:
	{
		float L_153 = ((&___scrollPosition)->___y_2);
		float L_154 = Rect_get_height_m6_279((&___viewRect), /*hidden argument*/NULL);
		float L_155 = Rect_get_height_m6_279((&___position), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t6_58_il2cpp_TypeInfo_var);
		float L_156 = Mathf_Max_m6_383(NULL /*static, unused*/, ((float)((float)L_154-(float)L_155)), (0.0f), /*hidden argument*/NULL);
		float L_157 = Mathf_Clamp_m6_389(NULL /*static, unused*/, L_153, (0.0f), L_156, /*hidden argument*/NULL);
		(&___scrollPosition)->___y_2 = L_157;
	}

IL_0445:
	{
		goto IL_044a;
	}

IL_044a:
	{
		Rect_t6_52  L_158 = V_2;
		float L_159 = ((&___scrollPosition)->___x_1);
		float L_160 = Rect_get_x_m6_273((&___viewRect), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t6_58_il2cpp_TypeInfo_var);
		float L_161 = bankers_roundf(((float)((float)((-L_159))-(float)L_160)));
		float L_162 = ((&___scrollPosition)->___y_2);
		float L_163 = Rect_get_y_m6_275((&___viewRect), /*hidden argument*/NULL);
		float L_164 = bankers_roundf(((float)((float)((-L_162))-(float)L_163)));
		Vector2_t6_48  L_165 = {0};
		Vector2__ctor_m6_176(&L_165, L_161, L_164, /*hidden argument*/NULL);
		Vector2_t6_48  L_166 = Vector2_get_zero_m6_182(NULL /*static, unused*/, /*hidden argument*/NULL);
		GUIClip_Push_m6_1393(NULL /*static, unused*/, L_158, L_165, L_166, 0, /*hidden argument*/NULL);
		Vector2_t6_48  L_167 = ___scrollPosition;
		return L_167;
	}
}
// System.Void UnityEngine.GUI::EndScrollView(System.Boolean)
extern TypeInfo* GUIUtility_t6_185_il2cpp_TypeInfo_var;
extern TypeInfo* GUI_t6_160_il2cpp_TypeInfo_var;
extern TypeInfo* ScrollViewState_t6_158_il2cpp_TypeInfo_var;
extern TypeInfo* Mathf_t6_58_il2cpp_TypeInfo_var;
extern "C" void GUI_EndScrollView_m6_1074 (Object_t * __this /* static, unused */, bool ___handleScrollWheel, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GUIUtility_t6_185_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(984);
		GUI_t6_160_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(982);
		ScrollViewState_t6_158_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(989);
		Mathf_t6_58_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(940);
		s_Il2CppMethodIntialized = true;
	}
	ScrollViewState_t6_158 * V_0 = {0};
	Vector2_t6_48  V_1 = {0};
	Vector2_t6_48  V_2 = {0};
	{
		IL2CPP_RUNTIME_CLASS_INIT(GUIUtility_t6_185_il2cpp_TypeInfo_var);
		GUIUtility_CheckOnGUI_m6_1376(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t6_160_il2cpp_TypeInfo_var);
		GenericStack_t6_162 * L_0 = ((GUI_t6_160_StaticFields*)GUI_t6_160_il2cpp_TypeInfo_var->static_fields)->___s_ScrollViewStates_12;
		NullCheck(L_0);
		Object_t * L_1 = (Object_t *)VirtFuncInvoker0< Object_t * >::Invoke(15 /* System.Object System.Collections.Stack::Peek() */, L_0);
		V_0 = ((ScrollViewState_t6_158 *)CastclassSealed(L_1, ScrollViewState_t6_158_il2cpp_TypeInfo_var));
		GUIClip_Pop_m6_1395(NULL /*static, unused*/, /*hidden argument*/NULL);
		GenericStack_t6_162 * L_2 = ((GUI_t6_160_StaticFields*)GUI_t6_160_il2cpp_TypeInfo_var->static_fields)->___s_ScrollViewStates_12;
		NullCheck(L_2);
		VirtFuncInvoker0< Object_t * >::Invoke(16 /* System.Object System.Collections.Stack::Pop() */, L_2);
		bool L_3 = ___handleScrollWheel;
		if (!L_3)
		{
			goto IL_0106;
		}
	}
	{
		Event_t6_154 * L_4 = Event_get_current_m6_989(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_4);
		int32_t L_5 = Event_get_type_m6_1003(L_4, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_5) == ((uint32_t)6))))
		{
			goto IL_0106;
		}
	}
	{
		ScrollViewState_t6_158 * L_6 = V_0;
		NullCheck(L_6);
		Rect_t6_52 * L_7 = &(L_6->___position_0);
		Event_t6_154 * L_8 = Event_get_current_m6_989(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_8);
		Vector2_t6_48  L_9 = Event_get_mousePosition_m6_970(L_8, /*hidden argument*/NULL);
		bool L_10 = Rect_Contains_m6_286(L_7, L_9, /*hidden argument*/NULL);
		if (!L_10)
		{
			goto IL_0106;
		}
	}
	{
		ScrollViewState_t6_158 * L_11 = V_0;
		NullCheck(L_11);
		Vector2_t6_48 * L_12 = &(L_11->___scrollPosition_3);
		ScrollViewState_t6_158 * L_13 = V_0;
		NullCheck(L_13);
		Vector2_t6_48 * L_14 = &(L_13->___scrollPosition_3);
		float L_15 = (L_14->___x_1);
		Event_t6_154 * L_16 = Event_get_current_m6_989(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_16);
		Vector2_t6_48  L_17 = Event_get_delta_m6_972(L_16, /*hidden argument*/NULL);
		V_1 = L_17;
		float L_18 = ((&V_1)->___x_1);
		ScrollViewState_t6_158 * L_19 = V_0;
		NullCheck(L_19);
		Rect_t6_52 * L_20 = &(L_19->___viewRect_2);
		float L_21 = Rect_get_width_m6_277(L_20, /*hidden argument*/NULL);
		ScrollViewState_t6_158 * L_22 = V_0;
		NullCheck(L_22);
		Rect_t6_52 * L_23 = &(L_22->___visibleRect_1);
		float L_24 = Rect_get_width_m6_277(L_23, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t6_58_il2cpp_TypeInfo_var);
		float L_25 = Mathf_Clamp_m6_389(NULL /*static, unused*/, ((float)((float)L_15+(float)((float)((float)L_18*(float)(20.0f))))), (0.0f), ((float)((float)L_21-(float)L_24)), /*hidden argument*/NULL);
		L_12->___x_1 = L_25;
		ScrollViewState_t6_158 * L_26 = V_0;
		NullCheck(L_26);
		Vector2_t6_48 * L_27 = &(L_26->___scrollPosition_3);
		ScrollViewState_t6_158 * L_28 = V_0;
		NullCheck(L_28);
		Vector2_t6_48 * L_29 = &(L_28->___scrollPosition_3);
		float L_30 = (L_29->___y_2);
		Event_t6_154 * L_31 = Event_get_current_m6_989(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_31);
		Vector2_t6_48  L_32 = Event_get_delta_m6_972(L_31, /*hidden argument*/NULL);
		V_2 = L_32;
		float L_33 = ((&V_2)->___y_2);
		ScrollViewState_t6_158 * L_34 = V_0;
		NullCheck(L_34);
		Rect_t6_52 * L_35 = &(L_34->___viewRect_2);
		float L_36 = Rect_get_height_m6_279(L_35, /*hidden argument*/NULL);
		ScrollViewState_t6_158 * L_37 = V_0;
		NullCheck(L_37);
		Rect_t6_52 * L_38 = &(L_37->___visibleRect_1);
		float L_39 = Rect_get_height_m6_279(L_38, /*hidden argument*/NULL);
		float L_40 = Mathf_Clamp_m6_389(NULL /*static, unused*/, ((float)((float)L_30+(float)((float)((float)L_33*(float)(20.0f))))), (0.0f), ((float)((float)L_36-(float)L_39)), /*hidden argument*/NULL);
		L_27->___y_2 = L_40;
		ScrollViewState_t6_158 * L_41 = V_0;
		NullCheck(L_41);
		L_41->___apply_4 = 1;
		Event_t6_154 * L_42 = Event_get_current_m6_989(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_42);
		Event_Use_m6_1027(L_42, /*hidden argument*/NULL);
	}

IL_0106:
	{
		return;
	}
}
// UnityEngine.Rect UnityEngine.GUI::Window(System.Int32,UnityEngine.Rect,UnityEngine.GUI/WindowFunction,UnityEngine.GUIContent,UnityEngine.GUIStyle)
extern TypeInfo* GUIUtility_t6_185_il2cpp_TypeInfo_var;
extern TypeInfo* GUI_t6_160_il2cpp_TypeInfo_var;
extern "C" Rect_t6_52  GUI_Window_m6_1075 (Object_t * __this /* static, unused */, int32_t ___id, Rect_t6_52  ___clientRect, WindowFunction_t6_159 * ___func, GUIContent_t6_163 * ___title, GUIStyle_t6_166 * ___style, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GUIUtility_t6_185_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(984);
		GUI_t6_160_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(982);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GUIUtility_t6_185_il2cpp_TypeInfo_var);
		GUIUtility_CheckOnGUI_m6_1376(NULL /*static, unused*/, /*hidden argument*/NULL);
		int32_t L_0 = ___id;
		Rect_t6_52  L_1 = ___clientRect;
		WindowFunction_t6_159 * L_2 = ___func;
		GUIContent_t6_163 * L_3 = ___title;
		GUIStyle_t6_166 * L_4 = ___style;
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t6_160_il2cpp_TypeInfo_var);
		GUISkin_t6_161 * L_5 = GUI_get_skin_m6_1041(NULL /*static, unused*/, /*hidden argument*/NULL);
		Rect_t6_52  L_6 = GUI_DoWindow_m6_1095(NULL /*static, unused*/, L_0, L_1, L_2, L_3, L_4, L_5, 1, /*hidden argument*/NULL);
		return L_6;
	}
}
// System.Void UnityEngine.GUI::CallWindowDelegate(UnityEngine.GUI/WindowFunction,System.Int32,UnityEngine.GUISkin,System.Int32,System.Single,System.Single,UnityEngine.GUIStyle)
extern TypeInfo* GUILayoutUtility_t6_170_il2cpp_TypeInfo_var;
extern TypeInfo* GUI_t6_160_il2cpp_TypeInfo_var;
extern TypeInfo* GUILayoutOptionU5BU5D_t6_165_il2cpp_TypeInfo_var;
extern "C" void GUI_CallWindowDelegate_m6_1076 (Object_t * __this /* static, unused */, WindowFunction_t6_159 * ___func, int32_t ___id, GUISkin_t6_161 * ____skin, int32_t ___forceRect, float ___width, float ___height, GUIStyle_t6_166 * ___style, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GUILayoutUtility_t6_170_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(990);
		GUI_t6_160_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(982);
		GUILayoutOptionU5BU5D_t6_165_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(991);
		s_Il2CppMethodIntialized = true;
	}
	GUISkin_t6_161 * V_0 = {0};
	GUILayoutOptionU5BU5D_t6_165* V_1 = {0};
	{
		int32_t L_0 = ___id;
		IL2CPP_RUNTIME_CLASS_INIT(GUILayoutUtility_t6_170_il2cpp_TypeInfo_var);
		GUILayoutUtility_SelectIDList_m6_1150(NULL /*static, unused*/, L_0, 1, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t6_160_il2cpp_TypeInfo_var);
		GUISkin_t6_161 * L_1 = GUI_get_skin_m6_1041(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_0 = L_1;
		Event_t6_154 * L_2 = Event_get_current_m6_989(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_2);
		int32_t L_3 = Event_get_type_m6_1003(L_2, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_3) == ((uint32_t)8))))
		{
			goto IL_0056;
		}
	}
	{
		int32_t L_4 = ___forceRect;
		if (!L_4)
		{
			goto IL_004d;
		}
	}
	{
		GUILayoutOptionU5BU5D_t6_165* L_5 = ((GUILayoutOptionU5BU5D_t6_165*)SZArrayNew(GUILayoutOptionU5BU5D_t6_165_il2cpp_TypeInfo_var, 2));
		float L_6 = ___width;
		GUILayoutOption_t6_176 * L_7 = GUILayout_Width_m6_1143(NULL /*static, unused*/, L_6, /*hidden argument*/NULL);
		NullCheck(L_5);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_5, 0);
		ArrayElementTypeCheck (L_5, L_7);
		*((GUILayoutOption_t6_176 **)(GUILayoutOption_t6_176 **)SZArrayLdElema(L_5, 0, sizeof(GUILayoutOption_t6_176 *))) = (GUILayoutOption_t6_176 *)L_7;
		GUILayoutOptionU5BU5D_t6_165* L_8 = L_5;
		float L_9 = ___height;
		GUILayoutOption_t6_176 * L_10 = GUILayout_Height_m6_1145(NULL /*static, unused*/, L_9, /*hidden argument*/NULL);
		NullCheck(L_8);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_8, 1);
		ArrayElementTypeCheck (L_8, L_10);
		*((GUILayoutOption_t6_176 **)(GUILayoutOption_t6_176 **)SZArrayLdElema(L_8, 1, sizeof(GUILayoutOption_t6_176 *))) = (GUILayoutOption_t6_176 *)L_10;
		V_1 = L_8;
		int32_t L_11 = ___id;
		GUIStyle_t6_166 * L_12 = ___style;
		GUILayoutOptionU5BU5D_t6_165* L_13 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(GUILayoutUtility_t6_170_il2cpp_TypeInfo_var);
		GUILayoutUtility_BeginWindow_m6_1152(NULL /*static, unused*/, L_11, L_12, L_13, /*hidden argument*/NULL);
		goto IL_0056;
	}

IL_004d:
	{
		int32_t L_14 = ___id;
		GUIStyle_t6_166 * L_15 = ___style;
		IL2CPP_RUNTIME_CLASS_INIT(GUILayoutUtility_t6_170_il2cpp_TypeInfo_var);
		GUILayoutUtility_BeginWindow_m6_1152(NULL /*static, unused*/, L_14, L_15, (GUILayoutOptionU5BU5D_t6_165*)(GUILayoutOptionU5BU5D_t6_165*)NULL, /*hidden argument*/NULL);
	}

IL_0056:
	{
		GUISkin_t6_161 * L_16 = ____skin;
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t6_160_il2cpp_TypeInfo_var);
		GUI_set_skin_m6_1040(NULL /*static, unused*/, L_16, /*hidden argument*/NULL);
		WindowFunction_t6_159 * L_17 = ___func;
		int32_t L_18 = ___id;
		NullCheck(L_17);
		WindowFunction_Invoke_m6_1032(L_17, L_18, /*hidden argument*/NULL);
		Event_t6_154 * L_19 = Event_get_current_m6_989(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_19);
		int32_t L_20 = Event_get_type_m6_1003(L_19, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_20) == ((uint32_t)8))))
		{
			goto IL_0078;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GUILayoutUtility_t6_170_il2cpp_TypeInfo_var);
		GUILayoutUtility_Layout_m6_1154(NULL /*static, unused*/, /*hidden argument*/NULL);
	}

IL_0078:
	{
		GUISkin_t6_161 * L_21 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t6_160_il2cpp_TypeInfo_var);
		GUI_set_skin_m6_1040(NULL /*static, unused*/, L_21, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.GUI::DragWindow()
extern TypeInfo* GUI_t6_160_il2cpp_TypeInfo_var;
extern "C" void GUI_DragWindow_m6_1077 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GUI_t6_160_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(982);
		s_Il2CppMethodIntialized = true;
	}
	{
		Rect_t6_52  L_0 = {0};
		Rect__ctor_m6_270(&L_0, (0.0f), (0.0f), (10000.0f), (10000.0f), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t6_160_il2cpp_TypeInfo_var);
		GUI_DragWindow_m6_1097(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.GUI::set_color(UnityEngine.Color)
extern TypeInfo* GUI_t6_160_il2cpp_TypeInfo_var;
extern "C" void GUI_set_color_m6_1078 (Object_t * __this /* static, unused */, Color_t6_40  ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GUI_t6_160_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(982);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t6_160_il2cpp_TypeInfo_var);
		GUI_INTERNAL_set_color_m6_1079(NULL /*static, unused*/, (&___value), /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.GUI::INTERNAL_set_color(UnityEngine.Color&)
extern "C" void GUI_INTERNAL_set_color_m6_1079 (Object_t * __this /* static, unused */, Color_t6_40 * ___value, const MethodInfo* method)
{
	typedef void (*GUI_INTERNAL_set_color_m6_1079_ftn) (Color_t6_40 *);
	static GUI_INTERNAL_set_color_m6_1079_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GUI_INTERNAL_set_color_m6_1079_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.GUI::INTERNAL_set_color(UnityEngine.Color&)");
	_il2cpp_icall_func(___value);
}
// System.Boolean UnityEngine.GUI::get_changed()
extern "C" bool GUI_get_changed_m6_1080 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	typedef bool (*GUI_get_changed_m6_1080_ftn) ();
	static GUI_get_changed_m6_1080_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GUI_get_changed_m6_1080_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.GUI::get_changed()");
	return _il2cpp_icall_func();
}
// System.Void UnityEngine.GUI::set_changed(System.Boolean)
extern "C" void GUI_set_changed_m6_1081 (Object_t * __this /* static, unused */, bool ___value, const MethodInfo* method)
{
	typedef void (*GUI_set_changed_m6_1081_ftn) (bool);
	static GUI_set_changed_m6_1081_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GUI_set_changed_m6_1081_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.GUI::set_changed(System.Boolean)");
	_il2cpp_icall_func(___value);
}
// System.Boolean UnityEngine.GUI::get_enabled()
extern "C" bool GUI_get_enabled_m6_1082 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	typedef bool (*GUI_get_enabled_m6_1082_ftn) ();
	static GUI_get_enabled_m6_1082_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GUI_get_enabled_m6_1082_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.GUI::get_enabled()");
	return _il2cpp_icall_func();
}
// System.Void UnityEngine.GUI::Internal_SetTooltip(System.String)
extern "C" void GUI_Internal_SetTooltip_m6_1083 (Object_t * __this /* static, unused */, String_t* ___value, const MethodInfo* method)
{
	typedef void (*GUI_Internal_SetTooltip_m6_1083_ftn) (String_t*);
	static GUI_Internal_SetTooltip_m6_1083_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GUI_Internal_SetTooltip_m6_1083_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.GUI::Internal_SetTooltip(System.String)");
	_il2cpp_icall_func(___value);
}
// System.Void UnityEngine.GUI::DoLabel(UnityEngine.Rect,UnityEngine.GUIContent,System.IntPtr)
extern TypeInfo* GUI_t6_160_il2cpp_TypeInfo_var;
extern "C" void GUI_DoLabel_m6_1084 (Object_t * __this /* static, unused */, Rect_t6_52  ___position, GUIContent_t6_163 * ___content, IntPtr_t ___style, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GUI_t6_160_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(982);
		s_Il2CppMethodIntialized = true;
	}
	{
		GUIContent_t6_163 * L_0 = ___content;
		IntPtr_t L_1 = ___style;
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t6_160_il2cpp_TypeInfo_var);
		GUI_INTERNAL_CALL_DoLabel_m6_1085(NULL /*static, unused*/, (&___position), L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.GUI::INTERNAL_CALL_DoLabel(UnityEngine.Rect&,UnityEngine.GUIContent,System.IntPtr)
extern "C" void GUI_INTERNAL_CALL_DoLabel_m6_1085 (Object_t * __this /* static, unused */, Rect_t6_52 * ___position, GUIContent_t6_163 * ___content, IntPtr_t ___style, const MethodInfo* method)
{
	typedef void (*GUI_INTERNAL_CALL_DoLabel_m6_1085_ftn) (Rect_t6_52 *, GUIContent_t6_163 *, IntPtr_t);
	static GUI_INTERNAL_CALL_DoLabel_m6_1085_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GUI_INTERNAL_CALL_DoLabel_m6_1085_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.GUI::INTERNAL_CALL_DoLabel(UnityEngine.Rect&,UnityEngine.GUIContent,System.IntPtr)");
	_il2cpp_icall_func(___position, ___content, ___style);
}
// System.Boolean UnityEngine.GUI::DoButton(UnityEngine.Rect,UnityEngine.GUIContent,System.IntPtr)
extern TypeInfo* GUI_t6_160_il2cpp_TypeInfo_var;
extern "C" bool GUI_DoButton_m6_1086 (Object_t * __this /* static, unused */, Rect_t6_52  ___position, GUIContent_t6_163 * ___content, IntPtr_t ___style, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GUI_t6_160_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(982);
		s_Il2CppMethodIntialized = true;
	}
	{
		GUIContent_t6_163 * L_0 = ___content;
		IntPtr_t L_1 = ___style;
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t6_160_il2cpp_TypeInfo_var);
		bool L_2 = GUI_INTERNAL_CALL_DoButton_m6_1087(NULL /*static, unused*/, (&___position), L_0, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// System.Boolean UnityEngine.GUI::INTERNAL_CALL_DoButton(UnityEngine.Rect&,UnityEngine.GUIContent,System.IntPtr)
extern "C" bool GUI_INTERNAL_CALL_DoButton_m6_1087 (Object_t * __this /* static, unused */, Rect_t6_52 * ___position, GUIContent_t6_163 * ___content, IntPtr_t ___style, const MethodInfo* method)
{
	typedef bool (*GUI_INTERNAL_CALL_DoButton_m6_1087_ftn) (Rect_t6_52 *, GUIContent_t6_163 *, IntPtr_t);
	static GUI_INTERNAL_CALL_DoButton_m6_1087_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GUI_INTERNAL_CALL_DoButton_m6_1087_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.GUI::INTERNAL_CALL_DoButton(UnityEngine.Rect&,UnityEngine.GUIContent,System.IntPtr)");
	return _il2cpp_icall_func(___position, ___content, ___style);
}
// System.Void UnityEngine.GUI::SetNextControlName(System.String)
extern "C" void GUI_SetNextControlName_m6_1088 (Object_t * __this /* static, unused */, String_t* ___name, const MethodInfo* method)
{
	typedef void (*GUI_SetNextControlName_m6_1088_ftn) (String_t*);
	static GUI_SetNextControlName_m6_1088_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GUI_SetNextControlName_m6_1088_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.GUI::SetNextControlName(System.String)");
	_il2cpp_icall_func(___name);
}
// System.String UnityEngine.GUI::GetNameOfFocusedControl()
extern "C" String_t* GUI_GetNameOfFocusedControl_m6_1089 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	typedef String_t* (*GUI_GetNameOfFocusedControl_m6_1089_ftn) ();
	static GUI_GetNameOfFocusedControl_m6_1089_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GUI_GetNameOfFocusedControl_m6_1089_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.GUI::GetNameOfFocusedControl()");
	return _il2cpp_icall_func();
}
// System.Void UnityEngine.GUI::FocusControl(System.String)
extern "C" void GUI_FocusControl_m6_1090 (Object_t * __this /* static, unused */, String_t* ___name, const MethodInfo* method)
{
	typedef void (*GUI_FocusControl_m6_1090_ftn) (String_t*);
	static GUI_FocusControl_m6_1090_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GUI_FocusControl_m6_1090_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.GUI::FocusControl(System.String)");
	_il2cpp_icall_func(___name);
}
// System.Boolean UnityEngine.GUI::DoToggle(UnityEngine.Rect,System.Int32,System.Boolean,UnityEngine.GUIContent,System.IntPtr)
extern TypeInfo* GUI_t6_160_il2cpp_TypeInfo_var;
extern "C" bool GUI_DoToggle_m6_1091 (Object_t * __this /* static, unused */, Rect_t6_52  ___position, int32_t ___id, bool ___value, GUIContent_t6_163 * ___content, IntPtr_t ___style, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GUI_t6_160_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(982);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = ___id;
		bool L_1 = ___value;
		GUIContent_t6_163 * L_2 = ___content;
		IntPtr_t L_3 = ___style;
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t6_160_il2cpp_TypeInfo_var);
		bool L_4 = GUI_INTERNAL_CALL_DoToggle_m6_1092(NULL /*static, unused*/, (&___position), L_0, L_1, L_2, L_3, /*hidden argument*/NULL);
		return L_4;
	}
}
// System.Boolean UnityEngine.GUI::INTERNAL_CALL_DoToggle(UnityEngine.Rect&,System.Int32,System.Boolean,UnityEngine.GUIContent,System.IntPtr)
extern "C" bool GUI_INTERNAL_CALL_DoToggle_m6_1092 (Object_t * __this /* static, unused */, Rect_t6_52 * ___position, int32_t ___id, bool ___value, GUIContent_t6_163 * ___content, IntPtr_t ___style, const MethodInfo* method)
{
	typedef bool (*GUI_INTERNAL_CALL_DoToggle_m6_1092_ftn) (Rect_t6_52 *, int32_t, bool, GUIContent_t6_163 *, IntPtr_t);
	static GUI_INTERNAL_CALL_DoToggle_m6_1092_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GUI_INTERNAL_CALL_DoToggle_m6_1092_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.GUI::INTERNAL_CALL_DoToggle(UnityEngine.Rect&,System.Int32,System.Boolean,UnityEngine.GUIContent,System.IntPtr)");
	return _il2cpp_icall_func(___position, ___id, ___value, ___content, ___style);
}
// System.Boolean UnityEngine.GUI::get_usePageScrollbars()
extern "C" bool GUI_get_usePageScrollbars_m6_1093 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	typedef bool (*GUI_get_usePageScrollbars_m6_1093_ftn) ();
	static GUI_get_usePageScrollbars_m6_1093_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GUI_get_usePageScrollbars_m6_1093_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.GUI::get_usePageScrollbars()");
	return _il2cpp_icall_func();
}
// System.Void UnityEngine.GUI::InternalRepaintEditorWindow()
extern "C" void GUI_InternalRepaintEditorWindow_m6_1094 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	typedef void (*GUI_InternalRepaintEditorWindow_m6_1094_ftn) ();
	static GUI_InternalRepaintEditorWindow_m6_1094_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GUI_InternalRepaintEditorWindow_m6_1094_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.GUI::InternalRepaintEditorWindow()");
	_il2cpp_icall_func();
}
// UnityEngine.Rect UnityEngine.GUI::DoWindow(System.Int32,UnityEngine.Rect,UnityEngine.GUI/WindowFunction,UnityEngine.GUIContent,UnityEngine.GUIStyle,UnityEngine.GUISkin,System.Boolean)
extern TypeInfo* GUI_t6_160_il2cpp_TypeInfo_var;
extern "C" Rect_t6_52  GUI_DoWindow_m6_1095 (Object_t * __this /* static, unused */, int32_t ___id, Rect_t6_52  ___clientRect, WindowFunction_t6_159 * ___func, GUIContent_t6_163 * ___title, GUIStyle_t6_166 * ___style, GUISkin_t6_161 * ___skin, bool ___forceRectOnLayout, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GUI_t6_160_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(982);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = ___id;
		WindowFunction_t6_159 * L_1 = ___func;
		GUIContent_t6_163 * L_2 = ___title;
		GUIStyle_t6_166 * L_3 = ___style;
		GUISkin_t6_161 * L_4 = ___skin;
		bool L_5 = ___forceRectOnLayout;
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t6_160_il2cpp_TypeInfo_var);
		Rect_t6_52  L_6 = GUI_INTERNAL_CALL_DoWindow_m6_1096(NULL /*static, unused*/, L_0, (&___clientRect), L_1, L_2, L_3, L_4, L_5, /*hidden argument*/NULL);
		return L_6;
	}
}
// UnityEngine.Rect UnityEngine.GUI::INTERNAL_CALL_DoWindow(System.Int32,UnityEngine.Rect&,UnityEngine.GUI/WindowFunction,UnityEngine.GUIContent,UnityEngine.GUIStyle,UnityEngine.GUISkin,System.Boolean)
extern "C" Rect_t6_52  GUI_INTERNAL_CALL_DoWindow_m6_1096 (Object_t * __this /* static, unused */, int32_t ___id, Rect_t6_52 * ___clientRect, WindowFunction_t6_159 * ___func, GUIContent_t6_163 * ___title, GUIStyle_t6_166 * ___style, GUISkin_t6_161 * ___skin, bool ___forceRectOnLayout, const MethodInfo* method)
{
	typedef Rect_t6_52  (*GUI_INTERNAL_CALL_DoWindow_m6_1096_ftn) (int32_t, Rect_t6_52 *, WindowFunction_t6_159 *, GUIContent_t6_163 *, GUIStyle_t6_166 *, GUISkin_t6_161 *, bool);
	static GUI_INTERNAL_CALL_DoWindow_m6_1096_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GUI_INTERNAL_CALL_DoWindow_m6_1096_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.GUI::INTERNAL_CALL_DoWindow(System.Int32,UnityEngine.Rect&,UnityEngine.GUI/WindowFunction,UnityEngine.GUIContent,UnityEngine.GUIStyle,UnityEngine.GUISkin,System.Boolean)");
	return _il2cpp_icall_func(___id, ___clientRect, ___func, ___title, ___style, ___skin, ___forceRectOnLayout);
}
// System.Void UnityEngine.GUI::DragWindow(UnityEngine.Rect)
extern TypeInfo* GUI_t6_160_il2cpp_TypeInfo_var;
extern "C" void GUI_DragWindow_m6_1097 (Object_t * __this /* static, unused */, Rect_t6_52  ___position, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GUI_t6_160_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(982);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t6_160_il2cpp_TypeInfo_var);
		GUI_INTERNAL_CALL_DragWindow_m6_1098(NULL /*static, unused*/, (&___position), /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.GUI::INTERNAL_CALL_DragWindow(UnityEngine.Rect&)
extern "C" void GUI_INTERNAL_CALL_DragWindow_m6_1098 (Object_t * __this /* static, unused */, Rect_t6_52 * ___position, const MethodInfo* method)
{
	typedef void (*GUI_INTERNAL_CALL_DragWindow_m6_1098_ftn) (Rect_t6_52 *);
	static GUI_INTERNAL_CALL_DragWindow_m6_1098_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GUI_INTERNAL_CALL_DragWindow_m6_1098_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.GUI::INTERNAL_CALL_DragWindow(UnityEngine.Rect&)");
	_il2cpp_icall_func(___position);
}
// System.Void UnityEngine.GUIContent::.ctor()
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern "C" void GUIContent__ctor_m6_1099 (GUIContent_t6_163 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(11);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_0 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->___Empty_2;
		__this->___m_Text_0 = L_0;
		String_t* L_1 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->___Empty_2;
		__this->___m_Tooltip_2 = L_1;
		Object__ctor_m1_0(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.GUIContent::.ctor(System.String)
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern "C" void GUIContent__ctor_m6_1100 (GUIContent_t6_163 * __this, String_t* ___text, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(11);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_0 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->___Empty_2;
		__this->___m_Text_0 = L_0;
		String_t* L_1 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->___Empty_2;
		__this->___m_Tooltip_2 = L_1;
		Object__ctor_m1_0(__this, /*hidden argument*/NULL);
		String_t* L_2 = ___text;
		__this->___m_Text_0 = L_2;
		return;
	}
}
// System.Void UnityEngine.GUIContent::.ctor(UnityEngine.GUIContent)
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern "C" void GUIContent__ctor_m6_1101 (GUIContent_t6_163 * __this, GUIContent_t6_163 * ___src, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(11);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_0 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->___Empty_2;
		__this->___m_Text_0 = L_0;
		String_t* L_1 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->___Empty_2;
		__this->___m_Tooltip_2 = L_1;
		Object__ctor_m1_0(__this, /*hidden argument*/NULL);
		GUIContent_t6_163 * L_2 = ___src;
		NullCheck(L_2);
		String_t* L_3 = (L_2->___m_Text_0);
		__this->___m_Text_0 = L_3;
		GUIContent_t6_163 * L_4 = ___src;
		NullCheck(L_4);
		Texture_t6_32 * L_5 = (L_4->___m_Image_1);
		__this->___m_Image_1 = L_5;
		GUIContent_t6_163 * L_6 = ___src;
		NullCheck(L_6);
		String_t* L_7 = (L_6->___m_Tooltip_2);
		__this->___m_Tooltip_2 = L_7;
		return;
	}
}
// System.Void UnityEngine.GUIContent::.cctor()
extern TypeInfo* GUIContent_t6_163_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern "C" void GUIContent__cctor_m6_1102 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GUIContent_t6_163_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(985);
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(11);
		s_Il2CppMethodIntialized = true;
	}
	{
		GUIContent_t6_163 * L_0 = (GUIContent_t6_163 *)il2cpp_codegen_object_new (GUIContent_t6_163_il2cpp_TypeInfo_var);
		GUIContent__ctor_m6_1099(L_0, /*hidden argument*/NULL);
		((GUIContent_t6_163_StaticFields*)GUIContent_t6_163_il2cpp_TypeInfo_var->static_fields)->___s_Text_3 = L_0;
		GUIContent_t6_163 * L_1 = (GUIContent_t6_163 *)il2cpp_codegen_object_new (GUIContent_t6_163_il2cpp_TypeInfo_var);
		GUIContent__ctor_m6_1099(L_1, /*hidden argument*/NULL);
		((GUIContent_t6_163_StaticFields*)GUIContent_t6_163_il2cpp_TypeInfo_var->static_fields)->___s_Image_4 = L_1;
		GUIContent_t6_163 * L_2 = (GUIContent_t6_163 *)il2cpp_codegen_object_new (GUIContent_t6_163_il2cpp_TypeInfo_var);
		GUIContent__ctor_m6_1099(L_2, /*hidden argument*/NULL);
		((GUIContent_t6_163_StaticFields*)GUIContent_t6_163_il2cpp_TypeInfo_var->static_fields)->___s_TextImage_5 = L_2;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_3 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->___Empty_2;
		GUIContent_t6_163 * L_4 = (GUIContent_t6_163 *)il2cpp_codegen_object_new (GUIContent_t6_163_il2cpp_TypeInfo_var);
		GUIContent__ctor_m6_1100(L_4, L_3, /*hidden argument*/NULL);
		((GUIContent_t6_163_StaticFields*)GUIContent_t6_163_il2cpp_TypeInfo_var->static_fields)->___none_6 = L_4;
		return;
	}
}
// System.String UnityEngine.GUIContent::get_text()
extern "C" String_t* GUIContent_get_text_m6_1103 (GUIContent_t6_163 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = (__this->___m_Text_0);
		return L_0;
	}
}
// System.Void UnityEngine.GUIContent::set_text(System.String)
extern "C" void GUIContent_set_text_m6_1104 (GUIContent_t6_163 * __this, String_t* ___value, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value;
		__this->___m_Text_0 = L_0;
		return;
	}
}
// System.String UnityEngine.GUIContent::get_tooltip()
extern "C" String_t* GUIContent_get_tooltip_m6_1105 (GUIContent_t6_163 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = (__this->___m_Tooltip_2);
		return L_0;
	}
}
// UnityEngine.GUIContent UnityEngine.GUIContent::Temp(System.String)
extern TypeInfo* GUIContent_t6_163_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern "C" GUIContent_t6_163 * GUIContent_Temp_m6_1106 (Object_t * __this /* static, unused */, String_t* ___t, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GUIContent_t6_163_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(985);
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(11);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GUIContent_t6_163_il2cpp_TypeInfo_var);
		GUIContent_t6_163 * L_0 = ((GUIContent_t6_163_StaticFields*)GUIContent_t6_163_il2cpp_TypeInfo_var->static_fields)->___s_Text_3;
		String_t* L_1 = ___t;
		NullCheck(L_0);
		L_0->___m_Text_0 = L_1;
		GUIContent_t6_163 * L_2 = ((GUIContent_t6_163_StaticFields*)GUIContent_t6_163_il2cpp_TypeInfo_var->static_fields)->___s_Text_3;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_3 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->___Empty_2;
		NullCheck(L_2);
		L_2->___m_Tooltip_2 = L_3;
		GUIContent_t6_163 * L_4 = ((GUIContent_t6_163_StaticFields*)GUIContent_t6_163_il2cpp_TypeInfo_var->static_fields)->___s_Text_3;
		return L_4;
	}
}
// UnityEngine.GUIContent UnityEngine.GUIContent::Temp(UnityEngine.Texture)
extern TypeInfo* GUIContent_t6_163_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern "C" GUIContent_t6_163 * GUIContent_Temp_m6_1107 (Object_t * __this /* static, unused */, Texture_t6_32 * ___i, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GUIContent_t6_163_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(985);
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(11);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GUIContent_t6_163_il2cpp_TypeInfo_var);
		GUIContent_t6_163 * L_0 = ((GUIContent_t6_163_StaticFields*)GUIContent_t6_163_il2cpp_TypeInfo_var->static_fields)->___s_Image_4;
		Texture_t6_32 * L_1 = ___i;
		NullCheck(L_0);
		L_0->___m_Image_1 = L_1;
		GUIContent_t6_163 * L_2 = ((GUIContent_t6_163_StaticFields*)GUIContent_t6_163_il2cpp_TypeInfo_var->static_fields)->___s_Image_4;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_3 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->___Empty_2;
		NullCheck(L_2);
		L_2->___m_Tooltip_2 = L_3;
		GUIContent_t6_163 * L_4 = ((GUIContent_t6_163_StaticFields*)GUIContent_t6_163_il2cpp_TypeInfo_var->static_fields)->___s_Image_4;
		return L_4;
	}
}
// System.Void UnityEngine.GUIContent::ClearStaticCache()
extern TypeInfo* GUIContent_t6_163_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern "C" void GUIContent_ClearStaticCache_m6_1108 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GUIContent_t6_163_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(985);
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(11);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GUIContent_t6_163_il2cpp_TypeInfo_var);
		GUIContent_t6_163 * L_0 = ((GUIContent_t6_163_StaticFields*)GUIContent_t6_163_il2cpp_TypeInfo_var->static_fields)->___s_Text_3;
		NullCheck(L_0);
		L_0->___m_Text_0 = (String_t*)NULL;
		GUIContent_t6_163 * L_1 = ((GUIContent_t6_163_StaticFields*)GUIContent_t6_163_il2cpp_TypeInfo_var->static_fields)->___s_Text_3;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_2 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->___Empty_2;
		NullCheck(L_1);
		L_1->___m_Tooltip_2 = L_2;
		GUIContent_t6_163 * L_3 = ((GUIContent_t6_163_StaticFields*)GUIContent_t6_163_il2cpp_TypeInfo_var->static_fields)->___s_Image_4;
		NullCheck(L_3);
		L_3->___m_Image_1 = (Texture_t6_32 *)NULL;
		GUIContent_t6_163 * L_4 = ((GUIContent_t6_163_StaticFields*)GUIContent_t6_163_il2cpp_TypeInfo_var->static_fields)->___s_Image_4;
		String_t* L_5 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->___Empty_2;
		NullCheck(L_4);
		L_4->___m_Tooltip_2 = L_5;
		GUIContent_t6_163 * L_6 = ((GUIContent_t6_163_StaticFields*)GUIContent_t6_163_il2cpp_TypeInfo_var->static_fields)->___s_TextImage_5;
		NullCheck(L_6);
		L_6->___m_Text_0 = (String_t*)NULL;
		GUIContent_t6_163 * L_7 = ((GUIContent_t6_163_StaticFields*)GUIContent_t6_163_il2cpp_TypeInfo_var->static_fields)->___s_TextImage_5;
		NullCheck(L_7);
		L_7->___m_Image_1 = (Texture_t6_32 *)NULL;
		return;
	}
}
// UnityEngine.GUIContent[] UnityEngine.GUIContent::Temp(System.String[])
extern TypeInfo* GUIContentU5BU5D_t6_264_il2cpp_TypeInfo_var;
extern TypeInfo* GUIContent_t6_163_il2cpp_TypeInfo_var;
extern "C" GUIContentU5BU5D_t6_264* GUIContent_Temp_m6_1109 (Object_t * __this /* static, unused */, StringU5BU5D_t1_202* ___texts, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GUIContentU5BU5D_t6_264_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(993);
		GUIContent_t6_163_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(985);
		s_Il2CppMethodIntialized = true;
	}
	GUIContentU5BU5D_t6_264* V_0 = {0};
	int32_t V_1 = 0;
	{
		StringU5BU5D_t1_202* L_0 = ___texts;
		NullCheck(L_0);
		V_0 = ((GUIContentU5BU5D_t6_264*)SZArrayNew(GUIContentU5BU5D_t6_264_il2cpp_TypeInfo_var, (((int32_t)((int32_t)(((Array_t *)L_0)->max_length))))));
		V_1 = 0;
		goto IL_001f;
	}

IL_0010:
	{
		GUIContentU5BU5D_t6_264* L_1 = V_0;
		int32_t L_2 = V_1;
		StringU5BU5D_t1_202* L_3 = ___texts;
		int32_t L_4 = V_1;
		NullCheck(L_3);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_3, L_4);
		int32_t L_5 = L_4;
		GUIContent_t6_163 * L_6 = (GUIContent_t6_163 *)il2cpp_codegen_object_new (GUIContent_t6_163_il2cpp_TypeInfo_var);
		GUIContent__ctor_m6_1100(L_6, (*(String_t**)(String_t**)SZArrayLdElema(L_3, L_5, sizeof(String_t*))), /*hidden argument*/NULL);
		NullCheck(L_1);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_1, L_2);
		ArrayElementTypeCheck (L_1, L_6);
		*((GUIContent_t6_163 **)(GUIContent_t6_163 **)SZArrayLdElema(L_1, L_2, sizeof(GUIContent_t6_163 *))) = (GUIContent_t6_163 *)L_6;
		int32_t L_7 = V_1;
		V_1 = ((int32_t)((int32_t)L_7+(int32_t)1));
	}

IL_001f:
	{
		int32_t L_8 = V_1;
		StringU5BU5D_t1_202* L_9 = ___texts;
		NullCheck(L_9);
		if ((((int32_t)L_8) < ((int32_t)(((int32_t)((int32_t)(((Array_t *)L_9)->max_length)))))))
		{
			goto IL_0010;
		}
	}
	{
		GUIContentU5BU5D_t6_264* L_10 = V_0;
		return L_10;
	}
}
// System.Void UnityEngine.GUILayout/LayoutedWindow::.ctor(UnityEngine.GUI/WindowFunction,UnityEngine.Rect,UnityEngine.GUIContent,UnityEngine.GUILayoutOption[],UnityEngine.GUIStyle)
extern "C" void LayoutedWindow__ctor_m6_1110 (LayoutedWindow_t6_164 * __this, WindowFunction_t6_159 * ___f, Rect_t6_52  ___screenRect, GUIContent_t6_163 * ___content, GUILayoutOptionU5BU5D_t6_165* ___options, GUIStyle_t6_166 * ___style, const MethodInfo* method)
{
	{
		Object__ctor_m1_0(__this, /*hidden argument*/NULL);
		WindowFunction_t6_159 * L_0 = ___f;
		__this->___m_Func_0 = L_0;
		Rect_t6_52  L_1 = ___screenRect;
		__this->___m_ScreenRect_1 = L_1;
		GUILayoutOptionU5BU5D_t6_165* L_2 = ___options;
		__this->___m_Options_2 = L_2;
		GUIStyle_t6_166 * L_3 = ___style;
		__this->___m_Style_3 = L_3;
		return;
	}
}
// System.Void UnityEngine.GUILayout/LayoutedWindow::DoWindow(System.Int32)
extern TypeInfo* GUILayoutUtility_t6_170_il2cpp_TypeInfo_var;
extern "C" void LayoutedWindow_DoWindow_m6_1111 (LayoutedWindow_t6_164 * __this, int32_t ___windowID, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GUILayoutUtility_t6_170_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(990);
		s_Il2CppMethodIntialized = true;
	}
	GUILayoutGroup_t6_169 * V_0 = {0};
	int32_t V_1 = {0};
	{
		IL2CPP_RUNTIME_CLASS_INIT(GUILayoutUtility_t6_170_il2cpp_TypeInfo_var);
		LayoutCache_t6_168 * L_0 = ((GUILayoutUtility_t6_170_StaticFields*)GUILayoutUtility_t6_170_il2cpp_TypeInfo_var->static_fields)->___current_2;
		NullCheck(L_0);
		GUILayoutGroup_t6_169 * L_1 = (L_0->___topLevel_0);
		V_0 = L_1;
		Event_t6_154 * L_2 = Event_get_current_m6_989(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_2);
		int32_t L_3 = Event_get_type_m6_1003(L_2, /*hidden argument*/NULL);
		V_1 = L_3;
		int32_t L_4 = V_1;
		if ((((int32_t)L_4) == ((int32_t)8)))
		{
			goto IL_0022;
		}
	}
	{
		goto IL_006b;
	}

IL_0022:
	{
		GUILayoutGroup_t6_169 * L_5 = V_0;
		NullCheck(L_5);
		L_5->___resetCoords_12 = 1;
		GUILayoutGroup_t6_169 * L_6 = V_0;
		Rect_t6_52  L_7 = (__this->___m_ScreenRect_1);
		NullCheck(L_6);
		((GUILayoutEntry_t6_171 *)L_6)->___rect_4 = L_7;
		GUILayoutOptionU5BU5D_t6_165* L_8 = (__this->___m_Options_2);
		if (!L_8)
		{
			goto IL_004c;
		}
	}
	{
		GUILayoutGroup_t6_169 * L_9 = V_0;
		GUILayoutOptionU5BU5D_t6_165* L_10 = (__this->___m_Options_2);
		NullCheck(L_9);
		VirtActionInvoker1< GUILayoutOptionU5BU5D_t6_165* >::Invoke(10 /* System.Void UnityEngine.GUILayoutGroup::ApplyOptions(UnityEngine.GUILayoutOption[]) */, L_9, L_10);
	}

IL_004c:
	{
		GUILayoutGroup_t6_169 * L_11 = V_0;
		NullCheck(L_11);
		L_11->___isWindow_15 = 1;
		GUILayoutGroup_t6_169 * L_12 = V_0;
		int32_t L_13 = ___windowID;
		NullCheck(L_12);
		L_12->___windowID_16 = L_13;
		GUILayoutGroup_t6_169 * L_14 = V_0;
		GUIStyle_t6_166 * L_15 = (__this->___m_Style_3);
		NullCheck(L_14);
		GUILayoutEntry_set_style_m6_1174(L_14, L_15, /*hidden argument*/NULL);
		goto IL_0076;
	}

IL_006b:
	{
		GUILayoutGroup_t6_169 * L_16 = V_0;
		NullCheck(L_16);
		GUILayoutGroup_ResetCursor_m6_1187(L_16, /*hidden argument*/NULL);
		goto IL_0076;
	}

IL_0076:
	{
		WindowFunction_t6_159 * L_17 = (__this->___m_Func_0);
		int32_t L_18 = ___windowID;
		NullCheck(L_17);
		WindowFunction_Invoke_m6_1032(L_17, L_18, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.GUILayout::Label(System.String,UnityEngine.GUILayoutOption[])
extern TypeInfo* GUIContent_t6_163_il2cpp_TypeInfo_var;
extern TypeInfo* GUI_t6_160_il2cpp_TypeInfo_var;
extern "C" void GUILayout_Label_m6_1112 (Object_t * __this /* static, unused */, String_t* ___text, GUILayoutOptionU5BU5D_t6_165* ___options, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GUIContent_t6_163_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(985);
		GUI_t6_160_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(982);
		s_Il2CppMethodIntialized = true;
	}
	{
		String_t* L_0 = ___text;
		IL2CPP_RUNTIME_CLASS_INIT(GUIContent_t6_163_il2cpp_TypeInfo_var);
		GUIContent_t6_163 * L_1 = GUIContent_Temp_m6_1106(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t6_160_il2cpp_TypeInfo_var);
		GUISkin_t6_161 * L_2 = GUI_get_skin_m6_1041(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_2);
		GUIStyle_t6_166 * L_3 = GUISkin_get_label_m6_1221(L_2, /*hidden argument*/NULL);
		GUILayoutOptionU5BU5D_t6_165* L_4 = ___options;
		GUILayout_DoLabel_m6_1114(NULL /*static, unused*/, L_1, L_3, L_4, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.GUILayout::Label(System.String,UnityEngine.GUIStyle,UnityEngine.GUILayoutOption[])
extern TypeInfo* GUIContent_t6_163_il2cpp_TypeInfo_var;
extern "C" void GUILayout_Label_m6_1113 (Object_t * __this /* static, unused */, String_t* ___text, GUIStyle_t6_166 * ___style, GUILayoutOptionU5BU5D_t6_165* ___options, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GUIContent_t6_163_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(985);
		s_Il2CppMethodIntialized = true;
	}
	{
		String_t* L_0 = ___text;
		IL2CPP_RUNTIME_CLASS_INIT(GUIContent_t6_163_il2cpp_TypeInfo_var);
		GUIContent_t6_163 * L_1 = GUIContent_Temp_m6_1106(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		GUIStyle_t6_166 * L_2 = ___style;
		GUILayoutOptionU5BU5D_t6_165* L_3 = ___options;
		GUILayout_DoLabel_m6_1114(NULL /*static, unused*/, L_1, L_2, L_3, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.GUILayout::DoLabel(UnityEngine.GUIContent,UnityEngine.GUIStyle,UnityEngine.GUILayoutOption[])
extern TypeInfo* GUILayoutUtility_t6_170_il2cpp_TypeInfo_var;
extern TypeInfo* GUI_t6_160_il2cpp_TypeInfo_var;
extern "C" void GUILayout_DoLabel_m6_1114 (Object_t * __this /* static, unused */, GUIContent_t6_163 * ___content, GUIStyle_t6_166 * ___style, GUILayoutOptionU5BU5D_t6_165* ___options, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GUILayoutUtility_t6_170_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(990);
		GUI_t6_160_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(982);
		s_Il2CppMethodIntialized = true;
	}
	{
		GUIContent_t6_163 * L_0 = ___content;
		GUIStyle_t6_166 * L_1 = ___style;
		GUILayoutOptionU5BU5D_t6_165* L_2 = ___options;
		IL2CPP_RUNTIME_CLASS_INIT(GUILayoutUtility_t6_170_il2cpp_TypeInfo_var);
		Rect_t6_52  L_3 = GUILayoutUtility_GetRect_m6_1162(NULL /*static, unused*/, L_0, L_1, L_2, /*hidden argument*/NULL);
		GUIContent_t6_163 * L_4 = ___content;
		GUIStyle_t6_166 * L_5 = ___style;
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t6_160_il2cpp_TypeInfo_var);
		GUI_Label_m6_1046(NULL /*static, unused*/, L_3, L_4, L_5, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean UnityEngine.GUILayout::Button(System.String,UnityEngine.GUILayoutOption[])
extern TypeInfo* GUIContent_t6_163_il2cpp_TypeInfo_var;
extern TypeInfo* GUI_t6_160_il2cpp_TypeInfo_var;
extern "C" bool GUILayout_Button_m6_1115 (Object_t * __this /* static, unused */, String_t* ___text, GUILayoutOptionU5BU5D_t6_165* ___options, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GUIContent_t6_163_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(985);
		GUI_t6_160_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(982);
		s_Il2CppMethodIntialized = true;
	}
	{
		String_t* L_0 = ___text;
		IL2CPP_RUNTIME_CLASS_INIT(GUIContent_t6_163_il2cpp_TypeInfo_var);
		GUIContent_t6_163 * L_1 = GUIContent_Temp_m6_1106(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t6_160_il2cpp_TypeInfo_var);
		GUISkin_t6_161 * L_2 = GUI_get_skin_m6_1041(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_2);
		GUIStyle_t6_166 * L_3 = GUISkin_get_button_m6_1227(L_2, /*hidden argument*/NULL);
		GUILayoutOptionU5BU5D_t6_165* L_4 = ___options;
		bool L_5 = GUILayout_DoButton_m6_1116(NULL /*static, unused*/, L_1, L_3, L_4, /*hidden argument*/NULL);
		return L_5;
	}
}
// System.Boolean UnityEngine.GUILayout::DoButton(UnityEngine.GUIContent,UnityEngine.GUIStyle,UnityEngine.GUILayoutOption[])
extern TypeInfo* GUILayoutUtility_t6_170_il2cpp_TypeInfo_var;
extern TypeInfo* GUI_t6_160_il2cpp_TypeInfo_var;
extern "C" bool GUILayout_DoButton_m6_1116 (Object_t * __this /* static, unused */, GUIContent_t6_163 * ___content, GUIStyle_t6_166 * ___style, GUILayoutOptionU5BU5D_t6_165* ___options, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GUILayoutUtility_t6_170_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(990);
		GUI_t6_160_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(982);
		s_Il2CppMethodIntialized = true;
	}
	{
		GUIContent_t6_163 * L_0 = ___content;
		GUIStyle_t6_166 * L_1 = ___style;
		GUILayoutOptionU5BU5D_t6_165* L_2 = ___options;
		IL2CPP_RUNTIME_CLASS_INIT(GUILayoutUtility_t6_170_il2cpp_TypeInfo_var);
		Rect_t6_52  L_3 = GUILayoutUtility_GetRect_m6_1162(NULL /*static, unused*/, L_0, L_1, L_2, /*hidden argument*/NULL);
		GUIContent_t6_163 * L_4 = ___content;
		GUIStyle_t6_166 * L_5 = ___style;
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t6_160_il2cpp_TypeInfo_var);
		bool L_6 = GUI_Button_m6_1050(NULL /*static, unused*/, L_3, L_4, L_5, /*hidden argument*/NULL);
		return L_6;
	}
}
// System.String UnityEngine.GUILayout::TextField(System.String,UnityEngine.GUILayoutOption[])
extern TypeInfo* GUI_t6_160_il2cpp_TypeInfo_var;
extern "C" String_t* GUILayout_TextField_m6_1117 (Object_t * __this /* static, unused */, String_t* ___text, GUILayoutOptionU5BU5D_t6_165* ___options, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GUI_t6_160_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(982);
		s_Il2CppMethodIntialized = true;
	}
	{
		String_t* L_0 = ___text;
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t6_160_il2cpp_TypeInfo_var);
		GUISkin_t6_161 * L_1 = GUI_get_skin_m6_1041(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_1);
		GUIStyle_t6_166 * L_2 = GUISkin_get_textField_m6_1223(L_1, /*hidden argument*/NULL);
		GUILayoutOptionU5BU5D_t6_165* L_3 = ___options;
		String_t* L_4 = GUILayout_DoTextField_m6_1118(NULL /*static, unused*/, L_0, (-1), 0, L_2, L_3, /*hidden argument*/NULL);
		return L_4;
	}
}
// System.String UnityEngine.GUILayout::DoTextField(System.String,System.Int32,System.Boolean,UnityEngine.GUIStyle,UnityEngine.GUILayoutOption[])
extern TypeInfo* GUIUtility_t6_185_il2cpp_TypeInfo_var;
extern TypeInfo* GUIContent_t6_163_il2cpp_TypeInfo_var;
extern TypeInfo* Input_t6_83_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* GUILayoutUtility_t6_170_il2cpp_TypeInfo_var;
extern TypeInfo* GUI_t6_160_il2cpp_TypeInfo_var;
extern "C" String_t* GUILayout_DoTextField_m6_1118 (Object_t * __this /* static, unused */, String_t* ___text, int32_t ___maxLength, bool ___multiline, GUIStyle_t6_166 * ___style, GUILayoutOptionU5BU5D_t6_165* ___options, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GUIUtility_t6_185_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(984);
		GUIContent_t6_163_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(985);
		Input_t6_83_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(956);
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(11);
		GUILayoutUtility_t6_170_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(990);
		GUI_t6_160_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(982);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	GUIContent_t6_163 * V_1 = {0};
	Rect_t6_52  V_2 = {0};
	{
		IL2CPP_RUNTIME_CLASS_INIT(GUIUtility_t6_185_il2cpp_TypeInfo_var);
		int32_t L_0 = GUIUtility_GetControlID_m6_1367(NULL /*static, unused*/, 1, /*hidden argument*/NULL);
		V_0 = L_0;
		String_t* L_1 = ___text;
		IL2CPP_RUNTIME_CLASS_INIT(GUIContent_t6_163_il2cpp_TypeInfo_var);
		GUIContent_t6_163 * L_2 = GUIContent_Temp_m6_1106(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		V_1 = L_2;
		int32_t L_3 = GUIUtility_get_keyboardControl_m6_1383(NULL /*static, unused*/, /*hidden argument*/NULL);
		int32_t L_4 = V_0;
		if ((((int32_t)L_3) == ((int32_t)L_4)))
		{
			goto IL_0025;
		}
	}
	{
		String_t* L_5 = ___text;
		IL2CPP_RUNTIME_CLASS_INIT(GUIContent_t6_163_il2cpp_TypeInfo_var);
		GUIContent_t6_163 * L_6 = GUIContent_Temp_m6_1106(NULL /*static, unused*/, L_5, /*hidden argument*/NULL);
		V_1 = L_6;
		goto IL_0036;
	}

IL_0025:
	{
		String_t* L_7 = ___text;
		IL2CPP_RUNTIME_CLASS_INIT(Input_t6_83_il2cpp_TypeInfo_var);
		String_t* L_8 = Input_get_compositionString_m6_550(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_9 = String_Concat_m1_418(NULL /*static, unused*/, L_7, L_8, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(GUIContent_t6_163_il2cpp_TypeInfo_var);
		GUIContent_t6_163 * L_10 = GUIContent_Temp_m6_1106(NULL /*static, unused*/, L_9, /*hidden argument*/NULL);
		V_1 = L_10;
	}

IL_0036:
	{
		GUIContent_t6_163 * L_11 = V_1;
		GUIStyle_t6_166 * L_12 = ___style;
		GUILayoutOptionU5BU5D_t6_165* L_13 = ___options;
		IL2CPP_RUNTIME_CLASS_INIT(GUILayoutUtility_t6_170_il2cpp_TypeInfo_var);
		Rect_t6_52  L_14 = GUILayoutUtility_GetRect_m6_1162(NULL /*static, unused*/, L_11, L_12, L_13, /*hidden argument*/NULL);
		V_2 = L_14;
		IL2CPP_RUNTIME_CLASS_INIT(GUIUtility_t6_185_il2cpp_TypeInfo_var);
		int32_t L_15 = GUIUtility_get_keyboardControl_m6_1383(NULL /*static, unused*/, /*hidden argument*/NULL);
		int32_t L_16 = V_0;
		if ((!(((uint32_t)L_15) == ((uint32_t)L_16))))
		{
			goto IL_0052;
		}
	}
	{
		String_t* L_17 = ___text;
		IL2CPP_RUNTIME_CLASS_INIT(GUIContent_t6_163_il2cpp_TypeInfo_var);
		GUIContent_t6_163 * L_18 = GUIContent_Temp_m6_1106(NULL /*static, unused*/, L_17, /*hidden argument*/NULL);
		V_1 = L_18;
	}

IL_0052:
	{
		Rect_t6_52  L_19 = V_2;
		int32_t L_20 = V_0;
		GUIContent_t6_163 * L_21 = V_1;
		bool L_22 = ___multiline;
		int32_t L_23 = ___maxLength;
		GUIStyle_t6_166 * L_24 = ___style;
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t6_160_il2cpp_TypeInfo_var);
		GUI_DoTextField_m6_1053(NULL /*static, unused*/, L_19, L_20, L_21, L_22, L_23, L_24, /*hidden argument*/NULL);
		GUIContent_t6_163 * L_25 = V_1;
		NullCheck(L_25);
		String_t* L_26 = GUIContent_get_text_m6_1103(L_25, /*hidden argument*/NULL);
		return L_26;
	}
}
// System.Boolean UnityEngine.GUILayout::Toggle(System.Boolean,System.String,UnityEngine.GUILayoutOption[])
extern TypeInfo* GUIContent_t6_163_il2cpp_TypeInfo_var;
extern TypeInfo* GUI_t6_160_il2cpp_TypeInfo_var;
extern "C" bool GUILayout_Toggle_m6_1119 (Object_t * __this /* static, unused */, bool ___value, String_t* ___text, GUILayoutOptionU5BU5D_t6_165* ___options, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GUIContent_t6_163_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(985);
		GUI_t6_160_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(982);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool L_0 = ___value;
		String_t* L_1 = ___text;
		IL2CPP_RUNTIME_CLASS_INIT(GUIContent_t6_163_il2cpp_TypeInfo_var);
		GUIContent_t6_163 * L_2 = GUIContent_Temp_m6_1106(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t6_160_il2cpp_TypeInfo_var);
		GUISkin_t6_161 * L_3 = GUI_get_skin_m6_1041(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_3);
		GUIStyle_t6_166 * L_4 = GUISkin_get_toggle_m6_1229(L_3, /*hidden argument*/NULL);
		GUILayoutOptionU5BU5D_t6_165* L_5 = ___options;
		bool L_6 = GUILayout_DoToggle_m6_1120(NULL /*static, unused*/, L_0, L_2, L_4, L_5, /*hidden argument*/NULL);
		return L_6;
	}
}
// System.Boolean UnityEngine.GUILayout::DoToggle(System.Boolean,UnityEngine.GUIContent,UnityEngine.GUIStyle,UnityEngine.GUILayoutOption[])
extern TypeInfo* GUILayoutUtility_t6_170_il2cpp_TypeInfo_var;
extern TypeInfo* GUI_t6_160_il2cpp_TypeInfo_var;
extern "C" bool GUILayout_DoToggle_m6_1120 (Object_t * __this /* static, unused */, bool ___value, GUIContent_t6_163 * ___content, GUIStyle_t6_166 * ___style, GUILayoutOptionU5BU5D_t6_165* ___options, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GUILayoutUtility_t6_170_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(990);
		GUI_t6_160_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(982);
		s_Il2CppMethodIntialized = true;
	}
	{
		GUIContent_t6_163 * L_0 = ___content;
		GUIStyle_t6_166 * L_1 = ___style;
		GUILayoutOptionU5BU5D_t6_165* L_2 = ___options;
		IL2CPP_RUNTIME_CLASS_INIT(GUILayoutUtility_t6_170_il2cpp_TypeInfo_var);
		Rect_t6_52  L_3 = GUILayoutUtility_GetRect_m6_1162(NULL /*static, unused*/, L_0, L_1, L_2, /*hidden argument*/NULL);
		bool L_4 = ___value;
		GUIContent_t6_163 * L_5 = ___content;
		GUIStyle_t6_166 * L_6 = ___style;
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t6_160_il2cpp_TypeInfo_var);
		bool L_7 = GUI_Toggle_m6_1058(NULL /*static, unused*/, L_3, L_4, L_5, L_6, /*hidden argument*/NULL);
		return L_7;
	}
}
// System.Int32 UnityEngine.GUILayout::Toolbar(System.Int32,System.String[],UnityEngine.GUILayoutOption[])
extern TypeInfo* GUIContent_t6_163_il2cpp_TypeInfo_var;
extern TypeInfo* GUI_t6_160_il2cpp_TypeInfo_var;
extern "C" int32_t GUILayout_Toolbar_m6_1121 (Object_t * __this /* static, unused */, int32_t ___selected, StringU5BU5D_t1_202* ___texts, GUILayoutOptionU5BU5D_t6_165* ___options, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GUIContent_t6_163_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(985);
		GUI_t6_160_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(982);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = ___selected;
		StringU5BU5D_t1_202* L_1 = ___texts;
		IL2CPP_RUNTIME_CLASS_INIT(GUIContent_t6_163_il2cpp_TypeInfo_var);
		GUIContentU5BU5D_t6_264* L_2 = GUIContent_Temp_m6_1109(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t6_160_il2cpp_TypeInfo_var);
		GUISkin_t6_161 * L_3 = GUI_get_skin_m6_1041(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_3);
		GUIStyle_t6_166 * L_4 = GUISkin_get_button_m6_1227(L_3, /*hidden argument*/NULL);
		GUILayoutOptionU5BU5D_t6_165* L_5 = ___options;
		int32_t L_6 = GUILayout_Toolbar_m6_1122(NULL /*static, unused*/, L_0, L_2, L_4, L_5, /*hidden argument*/NULL);
		return L_6;
	}
}
// System.Int32 UnityEngine.GUILayout::Toolbar(System.Int32,UnityEngine.GUIContent[],UnityEngine.GUIStyle,UnityEngine.GUILayoutOption[])
extern TypeInfo* GUI_t6_160_il2cpp_TypeInfo_var;
extern TypeInfo* Vector2_t6_48_il2cpp_TypeInfo_var;
extern TypeInfo* Mathf_t6_58_il2cpp_TypeInfo_var;
extern TypeInfo* GUILayoutUtility_t6_170_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2735;
extern Il2CppCodeGenString* _stringLiteral2779;
extern Il2CppCodeGenString* _stringLiteral2736;
extern "C" int32_t GUILayout_Toolbar_m6_1122 (Object_t * __this /* static, unused */, int32_t ___selected, GUIContentU5BU5D_t6_264* ___contents, GUIStyle_t6_166 * ___style, GUILayoutOptionU5BU5D_t6_165* ___options, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GUI_t6_160_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(982);
		Vector2_t6_48_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(939);
		Mathf_t6_58_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(940);
		GUILayoutUtility_t6_170_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(990);
		_stringLiteral2735 = il2cpp_codegen_string_literal_from_index(2735);
		_stringLiteral2779 = il2cpp_codegen_string_literal_from_index(2779);
		_stringLiteral2736 = il2cpp_codegen_string_literal_from_index(2736);
		s_Il2CppMethodIntialized = true;
	}
	GUIStyle_t6_166 * V_0 = {0};
	GUIStyle_t6_166 * V_1 = {0};
	GUIStyle_t6_166 * V_2 = {0};
	Vector2_t6_48  V_3 = {0};
	int32_t V_4 = 0;
	GUIStyle_t6_166 * V_5 = {0};
	GUIStyle_t6_166 * V_6 = {0};
	GUIStyle_t6_166 * V_7 = {0};
	int32_t V_8 = 0;
	int32_t V_9 = 0;
	Vector2_t6_48  V_10 = {0};
	GUIStyle_t6_166 * G_B3_0 = {0};
	GUIStyle_t6_166 * G_B6_0 = {0};
	GUIStyle_t6_166 * G_B9_0 = {0};
	{
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t6_160_il2cpp_TypeInfo_var);
		GUI_FindStyles_m6_1060(NULL /*static, unused*/, (&___style), (&V_0), (&V_1), (&V_2), _stringLiteral2735, _stringLiteral2779, _stringLiteral2736, /*hidden argument*/NULL);
		Initobj (Vector2_t6_48_il2cpp_TypeInfo_var, (&V_3));
		GUIContentU5BU5D_t6_264* L_0 = ___contents;
		NullCheck(L_0);
		V_4 = (((int32_t)((int32_t)(((Array_t *)L_0)->max_length))));
		int32_t L_1 = V_4;
		if ((((int32_t)L_1) <= ((int32_t)1)))
		{
			goto IL_0037;
		}
	}
	{
		GUIStyle_t6_166 * L_2 = V_0;
		G_B3_0 = L_2;
		goto IL_0038;
	}

IL_0037:
	{
		GUIStyle_t6_166 * L_3 = ___style;
		G_B3_0 = L_3;
	}

IL_0038:
	{
		V_5 = G_B3_0;
		int32_t L_4 = V_4;
		if ((((int32_t)L_4) <= ((int32_t)1)))
		{
			goto IL_0048;
		}
	}
	{
		GUIStyle_t6_166 * L_5 = V_1;
		G_B6_0 = L_5;
		goto IL_0049;
	}

IL_0048:
	{
		GUIStyle_t6_166 * L_6 = ___style;
		G_B6_0 = L_6;
	}

IL_0049:
	{
		V_6 = G_B6_0;
		int32_t L_7 = V_4;
		if ((((int32_t)L_7) <= ((int32_t)1)))
		{
			goto IL_0059;
		}
	}
	{
		GUIStyle_t6_166 * L_8 = V_2;
		G_B9_0 = L_8;
		goto IL_005a;
	}

IL_0059:
	{
		GUIStyle_t6_166 * L_9 = ___style;
		G_B9_0 = L_9;
	}

IL_005a:
	{
		V_7 = G_B9_0;
		GUIStyle_t6_166 * L_10 = V_5;
		NullCheck(L_10);
		RectOffset_t6_172 * L_11 = GUIStyle_get_margin_m6_1303(L_10, /*hidden argument*/NULL);
		NullCheck(L_11);
		int32_t L_12 = RectOffset_get_left_m6_1284(L_11, /*hidden argument*/NULL);
		V_8 = L_12;
		V_9 = 0;
		goto IL_012c;
	}

IL_0072:
	{
		int32_t L_13 = V_9;
		int32_t L_14 = V_4;
		if ((!(((uint32_t)L_13) == ((uint32_t)((int32_t)((int32_t)L_14-(int32_t)2))))))
		{
			goto IL_0085;
		}
	}
	{
		GUIStyle_t6_166 * L_15 = V_6;
		V_5 = L_15;
		GUIStyle_t6_166 * L_16 = V_7;
		V_6 = L_16;
	}

IL_0085:
	{
		int32_t L_17 = V_9;
		int32_t L_18 = V_4;
		if ((!(((uint32_t)L_17) == ((uint32_t)((int32_t)((int32_t)L_18-(int32_t)1))))))
		{
			goto IL_0094;
		}
	}
	{
		GUIStyle_t6_166 * L_19 = V_7;
		V_5 = L_19;
	}

IL_0094:
	{
		GUIStyle_t6_166 * L_20 = V_5;
		GUIContentU5BU5D_t6_264* L_21 = ___contents;
		int32_t L_22 = V_9;
		NullCheck(L_21);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_21, L_22);
		int32_t L_23 = L_22;
		NullCheck(L_20);
		Vector2_t6_48  L_24 = GUIStyle_CalcSize_m6_1319(L_20, (*(GUIContent_t6_163 **)(GUIContent_t6_163 **)SZArrayLdElema(L_21, L_23, sizeof(GUIContent_t6_163 *))), /*hidden argument*/NULL);
		V_10 = L_24;
		float L_25 = ((&V_10)->___x_1);
		float L_26 = ((&V_3)->___x_1);
		if ((!(((float)L_25) > ((float)L_26))))
		{
			goto IL_00c2;
		}
	}
	{
		float L_27 = ((&V_10)->___x_1);
		(&V_3)->___x_1 = L_27;
	}

IL_00c2:
	{
		float L_28 = ((&V_10)->___y_2);
		float L_29 = ((&V_3)->___y_2);
		if ((!(((float)L_28) > ((float)L_29))))
		{
			goto IL_00e3;
		}
	}
	{
		float L_30 = ((&V_10)->___y_2);
		(&V_3)->___y_2 = L_30;
	}

IL_00e3:
	{
		int32_t L_31 = V_9;
		int32_t L_32 = V_4;
		if ((!(((uint32_t)L_31) == ((uint32_t)((int32_t)((int32_t)L_32-(int32_t)1))))))
		{
			goto IL_0104;
		}
	}
	{
		int32_t L_33 = V_8;
		GUIStyle_t6_166 * L_34 = V_5;
		NullCheck(L_34);
		RectOffset_t6_172 * L_35 = GUIStyle_get_margin_m6_1303(L_34, /*hidden argument*/NULL);
		NullCheck(L_35);
		int32_t L_36 = RectOffset_get_right_m6_1286(L_35, /*hidden argument*/NULL);
		V_8 = ((int32_t)((int32_t)L_33+(int32_t)L_36));
		goto IL_0126;
	}

IL_0104:
	{
		int32_t L_37 = V_8;
		GUIStyle_t6_166 * L_38 = V_5;
		NullCheck(L_38);
		RectOffset_t6_172 * L_39 = GUIStyle_get_margin_m6_1303(L_38, /*hidden argument*/NULL);
		NullCheck(L_39);
		int32_t L_40 = RectOffset_get_right_m6_1286(L_39, /*hidden argument*/NULL);
		GUIStyle_t6_166 * L_41 = V_6;
		NullCheck(L_41);
		RectOffset_t6_172 * L_42 = GUIStyle_get_margin_m6_1303(L_41, /*hidden argument*/NULL);
		NullCheck(L_42);
		int32_t L_43 = RectOffset_get_left_m6_1284(L_42, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t6_58_il2cpp_TypeInfo_var);
		int32_t L_44 = Mathf_Max_m6_384(NULL /*static, unused*/, L_40, L_43, /*hidden argument*/NULL);
		V_8 = ((int32_t)((int32_t)L_37+(int32_t)L_44));
	}

IL_0126:
	{
		int32_t L_45 = V_9;
		V_9 = ((int32_t)((int32_t)L_45+(int32_t)1));
	}

IL_012c:
	{
		int32_t L_46 = V_9;
		GUIContentU5BU5D_t6_264* L_47 = ___contents;
		NullCheck(L_47);
		if ((((int32_t)L_46) < ((int32_t)(((int32_t)((int32_t)(((Array_t *)L_47)->max_length)))))))
		{
			goto IL_0072;
		}
	}
	{
		float L_48 = ((&V_3)->___x_1);
		GUIContentU5BU5D_t6_264* L_49 = ___contents;
		NullCheck(L_49);
		int32_t L_50 = V_8;
		(&V_3)->___x_1 = ((float)((float)((float)((float)L_48*(float)(((float)((float)(((int32_t)((int32_t)(((Array_t *)L_49)->max_length)))))))))+(float)(((float)((float)L_50)))));
		float L_51 = ((&V_3)->___x_1);
		float L_52 = ((&V_3)->___y_2);
		GUIStyle_t6_166 * L_53 = ___style;
		GUILayoutOptionU5BU5D_t6_165* L_54 = ___options;
		IL2CPP_RUNTIME_CLASS_INIT(GUILayoutUtility_t6_170_il2cpp_TypeInfo_var);
		Rect_t6_52  L_55 = GUILayoutUtility_GetRect_m6_1164(NULL /*static, unused*/, L_51, L_52, L_53, L_54, /*hidden argument*/NULL);
		int32_t L_56 = ___selected;
		GUIContentU5BU5D_t6_264* L_57 = ___contents;
		GUIStyle_t6_166 * L_58 = ___style;
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t6_160_il2cpp_TypeInfo_var);
		int32_t L_59 = GUI_Toolbar_m6_1059(NULL /*static, unused*/, L_55, L_56, L_57, L_58, /*hidden argument*/NULL);
		return L_59;
	}
}
// System.Single UnityEngine.GUILayout::HorizontalSlider(System.Single,System.Single,System.Single,UnityEngine.GUILayoutOption[])
extern TypeInfo* GUI_t6_160_il2cpp_TypeInfo_var;
extern "C" float GUILayout_HorizontalSlider_m6_1123 (Object_t * __this /* static, unused */, float ___value, float ___leftValue, float ___rightValue, GUILayoutOptionU5BU5D_t6_165* ___options, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GUI_t6_160_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(982);
		s_Il2CppMethodIntialized = true;
	}
	{
		float L_0 = ___value;
		float L_1 = ___leftValue;
		float L_2 = ___rightValue;
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t6_160_il2cpp_TypeInfo_var);
		GUISkin_t6_161 * L_3 = GUI_get_skin_m6_1041(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_3);
		GUIStyle_t6_166 * L_4 = GUISkin_get_horizontalSlider_m6_1233(L_3, /*hidden argument*/NULL);
		GUISkin_t6_161 * L_5 = GUI_get_skin_m6_1041(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_5);
		GUIStyle_t6_166 * L_6 = GUISkin_get_horizontalSliderThumb_m6_1235(L_5, /*hidden argument*/NULL);
		GUILayoutOptionU5BU5D_t6_165* L_7 = ___options;
		float L_8 = GUILayout_DoHorizontalSlider_m6_1124(NULL /*static, unused*/, L_0, L_1, L_2, L_4, L_6, L_7, /*hidden argument*/NULL);
		return L_8;
	}
}
// System.Single UnityEngine.GUILayout::DoHorizontalSlider(System.Single,System.Single,System.Single,UnityEngine.GUIStyle,UnityEngine.GUIStyle,UnityEngine.GUILayoutOption[])
extern TypeInfo* GUIContent_t6_163_il2cpp_TypeInfo_var;
extern TypeInfo* GUILayoutUtility_t6_170_il2cpp_TypeInfo_var;
extern TypeInfo* GUI_t6_160_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2786;
extern "C" float GUILayout_DoHorizontalSlider_m6_1124 (Object_t * __this /* static, unused */, float ___value, float ___leftValue, float ___rightValue, GUIStyle_t6_166 * ___slider, GUIStyle_t6_166 * ___thumb, GUILayoutOptionU5BU5D_t6_165* ___options, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GUIContent_t6_163_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(985);
		GUILayoutUtility_t6_170_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(990);
		GUI_t6_160_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(982);
		_stringLiteral2786 = il2cpp_codegen_string_literal_from_index(2786);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GUIContent_t6_163_il2cpp_TypeInfo_var);
		GUIContent_t6_163 * L_0 = GUIContent_Temp_m6_1106(NULL /*static, unused*/, _stringLiteral2786, /*hidden argument*/NULL);
		GUIStyle_t6_166 * L_1 = ___slider;
		GUILayoutOptionU5BU5D_t6_165* L_2 = ___options;
		IL2CPP_RUNTIME_CLASS_INIT(GUILayoutUtility_t6_170_il2cpp_TypeInfo_var);
		Rect_t6_52  L_3 = GUILayoutUtility_GetRect_m6_1162(NULL /*static, unused*/, L_0, L_1, L_2, /*hidden argument*/NULL);
		float L_4 = ___value;
		float L_5 = ___leftValue;
		float L_6 = ___rightValue;
		GUIStyle_t6_166 * L_7 = ___slider;
		GUIStyle_t6_166 * L_8 = ___thumb;
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t6_160_il2cpp_TypeInfo_var);
		float L_9 = GUI_HorizontalSlider_m6_1065(NULL /*static, unused*/, L_3, L_4, L_5, L_6, L_7, L_8, /*hidden argument*/NULL);
		return L_9;
	}
}
// System.Void UnityEngine.GUILayout::Space(System.Single)
extern TypeInfo* GUIUtility_t6_185_il2cpp_TypeInfo_var;
extern TypeInfo* GUILayoutUtility_t6_170_il2cpp_TypeInfo_var;
extern TypeInfo* GUILayoutOptionU5BU5D_t6_165_il2cpp_TypeInfo_var;
extern "C" void GUILayout_Space_m6_1125 (Object_t * __this /* static, unused */, float ___pixels, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GUIUtility_t6_185_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(984);
		GUILayoutUtility_t6_170_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(990);
		GUILayoutOptionU5BU5D_t6_165_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(991);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GUIUtility_t6_185_il2cpp_TypeInfo_var);
		GUIUtility_CheckOnGUI_m6_1376(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(GUILayoutUtility_t6_170_il2cpp_TypeInfo_var);
		LayoutCache_t6_168 * L_0 = ((GUILayoutUtility_t6_170_StaticFields*)GUILayoutUtility_t6_170_il2cpp_TypeInfo_var->static_fields)->___current_2;
		NullCheck(L_0);
		GUILayoutGroup_t6_169 * L_1 = (L_0->___topLevel_0);
		NullCheck(L_1);
		bool L_2 = (L_1->___isVertical_11);
		if (!L_2)
		{
			goto IL_003e;
		}
	}
	{
		float L_3 = ___pixels;
		IL2CPP_RUNTIME_CLASS_INIT(GUILayoutUtility_t6_170_il2cpp_TypeInfo_var);
		GUIStyle_t6_166 * L_4 = GUILayoutUtility_get_spaceStyle_m6_1166(NULL /*static, unused*/, /*hidden argument*/NULL);
		GUILayoutOptionU5BU5D_t6_165* L_5 = ((GUILayoutOptionU5BU5D_t6_165*)SZArrayNew(GUILayoutOptionU5BU5D_t6_165_il2cpp_TypeInfo_var, 1));
		float L_6 = ___pixels;
		GUILayoutOption_t6_176 * L_7 = GUILayout_Height_m6_1145(NULL /*static, unused*/, L_6, /*hidden argument*/NULL);
		NullCheck(L_5);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_5, 0);
		ArrayElementTypeCheck (L_5, L_7);
		*((GUILayoutOption_t6_176 **)(GUILayoutOption_t6_176 **)SZArrayLdElema(L_5, 0, sizeof(GUILayoutOption_t6_176 *))) = (GUILayoutOption_t6_176 *)L_7;
		GUILayoutUtility_GetRect_m6_1164(NULL /*static, unused*/, (0.0f), L_3, L_4, L_5, /*hidden argument*/NULL);
		goto IL_005e;
	}

IL_003e:
	{
		float L_8 = ___pixels;
		IL2CPP_RUNTIME_CLASS_INIT(GUILayoutUtility_t6_170_il2cpp_TypeInfo_var);
		GUIStyle_t6_166 * L_9 = GUILayoutUtility_get_spaceStyle_m6_1166(NULL /*static, unused*/, /*hidden argument*/NULL);
		GUILayoutOptionU5BU5D_t6_165* L_10 = ((GUILayoutOptionU5BU5D_t6_165*)SZArrayNew(GUILayoutOptionU5BU5D_t6_165_il2cpp_TypeInfo_var, 1));
		float L_11 = ___pixels;
		GUILayoutOption_t6_176 * L_12 = GUILayout_Width_m6_1143(NULL /*static, unused*/, L_11, /*hidden argument*/NULL);
		NullCheck(L_10);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_10, 0);
		ArrayElementTypeCheck (L_10, L_12);
		*((GUILayoutOption_t6_176 **)(GUILayoutOption_t6_176 **)SZArrayLdElema(L_10, 0, sizeof(GUILayoutOption_t6_176 *))) = (GUILayoutOption_t6_176 *)L_12;
		GUILayoutUtility_GetRect_m6_1164(NULL /*static, unused*/, L_8, (0.0f), L_9, L_10, /*hidden argument*/NULL);
	}

IL_005e:
	{
		return;
	}
}
// System.Void UnityEngine.GUILayout::FlexibleSpace()
extern TypeInfo* GUIUtility_t6_185_il2cpp_TypeInfo_var;
extern TypeInfo* GUILayoutUtility_t6_170_il2cpp_TypeInfo_var;
extern TypeInfo* Int32_t1_3_il2cpp_TypeInfo_var;
extern TypeInfo* GUILayoutOptionU5BU5D_t6_165_il2cpp_TypeInfo_var;
extern "C" void GUILayout_FlexibleSpace_m6_1126 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GUIUtility_t6_185_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(984);
		GUILayoutUtility_t6_170_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(990);
		Int32_t1_3_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(8);
		GUILayoutOptionU5BU5D_t6_165_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(991);
		s_Il2CppMethodIntialized = true;
	}
	GUILayoutOption_t6_176 * V_0 = {0};
	{
		IL2CPP_RUNTIME_CLASS_INIT(GUIUtility_t6_185_il2cpp_TypeInfo_var);
		GUIUtility_CheckOnGUI_m6_1376(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(GUILayoutUtility_t6_170_il2cpp_TypeInfo_var);
		LayoutCache_t6_168 * L_0 = ((GUILayoutUtility_t6_170_StaticFields*)GUILayoutUtility_t6_170_il2cpp_TypeInfo_var->static_fields)->___current_2;
		NullCheck(L_0);
		GUILayoutGroup_t6_169 * L_1 = (L_0->___topLevel_0);
		NullCheck(L_1);
		bool L_2 = (L_1->___isVertical_11);
		if (!L_2)
		{
			goto IL_0025;
		}
	}
	{
		GUILayoutOption_t6_176 * L_3 = GUILayout_ExpandHeight_m6_1147(NULL /*static, unused*/, 1, /*hidden argument*/NULL);
		V_0 = L_3;
		goto IL_002c;
	}

IL_0025:
	{
		GUILayoutOption_t6_176 * L_4 = GUILayout_ExpandWidth_m6_1146(NULL /*static, unused*/, 1, /*hidden argument*/NULL);
		V_0 = L_4;
	}

IL_002c:
	{
		GUILayoutOption_t6_176 * L_5 = V_0;
		int32_t L_6 = ((int32_t)10000);
		Object_t * L_7 = Box(Int32_t1_3_il2cpp_TypeInfo_var, &L_6);
		NullCheck(L_5);
		L_5->___value_1 = L_7;
		IL2CPP_RUNTIME_CLASS_INIT(GUILayoutUtility_t6_170_il2cpp_TypeInfo_var);
		GUIStyle_t6_166 * L_8 = GUILayoutUtility_get_spaceStyle_m6_1166(NULL /*static, unused*/, /*hidden argument*/NULL);
		GUILayoutOptionU5BU5D_t6_165* L_9 = ((GUILayoutOptionU5BU5D_t6_165*)SZArrayNew(GUILayoutOptionU5BU5D_t6_165_il2cpp_TypeInfo_var, 1));
		GUILayoutOption_t6_176 * L_10 = V_0;
		NullCheck(L_9);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_9, 0);
		ArrayElementTypeCheck (L_9, L_10);
		*((GUILayoutOption_t6_176 **)(GUILayoutOption_t6_176 **)SZArrayLdElema(L_9, 0, sizeof(GUILayoutOption_t6_176 *))) = (GUILayoutOption_t6_176 *)L_10;
		GUILayoutUtility_GetRect_m6_1164(NULL /*static, unused*/, (0.0f), (0.0f), L_8, L_9, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.GUILayout::BeginHorizontal(UnityEngine.GUILayoutOption[])
extern TypeInfo* GUIContent_t6_163_il2cpp_TypeInfo_var;
extern TypeInfo* GUIStyle_t6_166_il2cpp_TypeInfo_var;
extern "C" void GUILayout_BeginHorizontal_m6_1127 (Object_t * __this /* static, unused */, GUILayoutOptionU5BU5D_t6_165* ___options, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GUIContent_t6_163_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(985);
		GUIStyle_t6_166_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(988);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GUIContent_t6_163_il2cpp_TypeInfo_var);
		GUIContent_t6_163 * L_0 = ((GUIContent_t6_163_StaticFields*)GUIContent_t6_163_il2cpp_TypeInfo_var->static_fields)->___none_6;
		IL2CPP_RUNTIME_CLASS_INIT(GUIStyle_t6_166_il2cpp_TypeInfo_var);
		GUIStyle_t6_166 * L_1 = GUIStyle_get_none_m6_1316(NULL /*static, unused*/, /*hidden argument*/NULL);
		GUILayoutOptionU5BU5D_t6_165* L_2 = ___options;
		GUILayout_BeginHorizontal_m6_1128(NULL /*static, unused*/, L_0, L_1, L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.GUILayout::BeginHorizontal(UnityEngine.GUIContent,UnityEngine.GUIStyle,UnityEngine.GUILayoutOption[])
extern const Il2CppType* GUILayoutGroup_t6_169_0_0_0_var;
extern TypeInfo* Type_t_il2cpp_TypeInfo_var;
extern TypeInfo* GUILayoutUtility_t6_170_il2cpp_TypeInfo_var;
extern TypeInfo* GUIStyle_t6_166_il2cpp_TypeInfo_var;
extern TypeInfo* GUIContent_t6_163_il2cpp_TypeInfo_var;
extern TypeInfo* GUI_t6_160_il2cpp_TypeInfo_var;
extern "C" void GUILayout_BeginHorizontal_m6_1128 (Object_t * __this /* static, unused */, GUIContent_t6_163 * ___content, GUIStyle_t6_166 * ___style, GUILayoutOptionU5BU5D_t6_165* ___options, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GUILayoutGroup_t6_169_0_0_0_var = il2cpp_codegen_type_from_index(994);
		Type_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3);
		GUILayoutUtility_t6_170_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(990);
		GUIStyle_t6_166_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(988);
		GUIContent_t6_163_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(985);
		GUI_t6_160_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(982);
		s_Il2CppMethodIntialized = true;
	}
	GUILayoutGroup_t6_169 * V_0 = {0};
	{
		GUIStyle_t6_166 * L_0 = ___style;
		GUILayoutOptionU5BU5D_t6_165* L_1 = ___options;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_2 = Type_GetTypeFromHandle_m1_893(NULL /*static, unused*/, LoadTypeToken(GUILayoutGroup_t6_169_0_0_0_var), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(GUILayoutUtility_t6_170_il2cpp_TypeInfo_var);
		GUILayoutGroup_t6_169 * L_3 = GUILayoutUtility_BeginLayoutGroup_m6_1159(NULL /*static, unused*/, L_0, L_1, L_2, /*hidden argument*/NULL);
		V_0 = L_3;
		GUILayoutGroup_t6_169 * L_4 = V_0;
		NullCheck(L_4);
		L_4->___isVertical_11 = 0;
		GUIStyle_t6_166 * L_5 = ___style;
		IL2CPP_RUNTIME_CLASS_INIT(GUIStyle_t6_166_il2cpp_TypeInfo_var);
		GUIStyle_t6_166 * L_6 = GUIStyle_get_none_m6_1316(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((!(((Object_t*)(GUIStyle_t6_166 *)L_5) == ((Object_t*)(GUIStyle_t6_166 *)L_6))))
		{
			goto IL_002f;
		}
	}
	{
		GUIContent_t6_163 * L_7 = ___content;
		IL2CPP_RUNTIME_CLASS_INIT(GUIContent_t6_163_il2cpp_TypeInfo_var);
		GUIContent_t6_163 * L_8 = ((GUIContent_t6_163_StaticFields*)GUIContent_t6_163_il2cpp_TypeInfo_var->static_fields)->___none_6;
		if ((((Object_t*)(GUIContent_t6_163 *)L_7) == ((Object_t*)(GUIContent_t6_163 *)L_8)))
		{
			goto IL_003c;
		}
	}

IL_002f:
	{
		GUILayoutGroup_t6_169 * L_9 = V_0;
		NullCheck(L_9);
		Rect_t6_52  L_10 = (((GUILayoutEntry_t6_171 *)L_9)->___rect_4);
		GUIContent_t6_163 * L_11 = ___content;
		GUIStyle_t6_166 * L_12 = ___style;
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t6_160_il2cpp_TypeInfo_var);
		GUI_Box_m6_1048(NULL /*static, unused*/, L_10, L_11, L_12, /*hidden argument*/NULL);
	}

IL_003c:
	{
		return;
	}
}
// System.Void UnityEngine.GUILayout::EndHorizontal()
extern TypeInfo* GUILayoutUtility_t6_170_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2787;
extern "C" void GUILayout_EndHorizontal_m6_1129 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GUILayoutUtility_t6_170_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(990);
		_stringLiteral2787 = il2cpp_codegen_string_literal_from_index(2787);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GUILayoutUtility_t6_170_il2cpp_TypeInfo_var);
		GUILayoutUtility_EndGroup_m6_1153(NULL /*static, unused*/, _stringLiteral2787, /*hidden argument*/NULL);
		GUILayoutUtility_EndLayoutGroup_m6_1160(NULL /*static, unused*/, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.GUILayout::BeginVertical(UnityEngine.GUILayoutOption[])
extern TypeInfo* GUIContent_t6_163_il2cpp_TypeInfo_var;
extern TypeInfo* GUIStyle_t6_166_il2cpp_TypeInfo_var;
extern "C" void GUILayout_BeginVertical_m6_1130 (Object_t * __this /* static, unused */, GUILayoutOptionU5BU5D_t6_165* ___options, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GUIContent_t6_163_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(985);
		GUIStyle_t6_166_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(988);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GUIContent_t6_163_il2cpp_TypeInfo_var);
		GUIContent_t6_163 * L_0 = ((GUIContent_t6_163_StaticFields*)GUIContent_t6_163_il2cpp_TypeInfo_var->static_fields)->___none_6;
		IL2CPP_RUNTIME_CLASS_INIT(GUIStyle_t6_166_il2cpp_TypeInfo_var);
		GUIStyle_t6_166 * L_1 = GUIStyle_get_none_m6_1316(NULL /*static, unused*/, /*hidden argument*/NULL);
		GUILayoutOptionU5BU5D_t6_165* L_2 = ___options;
		GUILayout_BeginVertical_m6_1131(NULL /*static, unused*/, L_0, L_1, L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.GUILayout::BeginVertical(UnityEngine.GUIContent,UnityEngine.GUIStyle,UnityEngine.GUILayoutOption[])
extern const Il2CppType* GUILayoutGroup_t6_169_0_0_0_var;
extern TypeInfo* Type_t_il2cpp_TypeInfo_var;
extern TypeInfo* GUILayoutUtility_t6_170_il2cpp_TypeInfo_var;
extern TypeInfo* GUIStyle_t6_166_il2cpp_TypeInfo_var;
extern TypeInfo* GUI_t6_160_il2cpp_TypeInfo_var;
extern "C" void GUILayout_BeginVertical_m6_1131 (Object_t * __this /* static, unused */, GUIContent_t6_163 * ___content, GUIStyle_t6_166 * ___style, GUILayoutOptionU5BU5D_t6_165* ___options, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GUILayoutGroup_t6_169_0_0_0_var = il2cpp_codegen_type_from_index(994);
		Type_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3);
		GUILayoutUtility_t6_170_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(990);
		GUIStyle_t6_166_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(988);
		GUI_t6_160_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(982);
		s_Il2CppMethodIntialized = true;
	}
	GUILayoutGroup_t6_169 * V_0 = {0};
	{
		GUIStyle_t6_166 * L_0 = ___style;
		GUILayoutOptionU5BU5D_t6_165* L_1 = ___options;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_2 = Type_GetTypeFromHandle_m1_893(NULL /*static, unused*/, LoadTypeToken(GUILayoutGroup_t6_169_0_0_0_var), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(GUILayoutUtility_t6_170_il2cpp_TypeInfo_var);
		GUILayoutGroup_t6_169 * L_3 = GUILayoutUtility_BeginLayoutGroup_m6_1159(NULL /*static, unused*/, L_0, L_1, L_2, /*hidden argument*/NULL);
		V_0 = L_3;
		GUILayoutGroup_t6_169 * L_4 = V_0;
		NullCheck(L_4);
		L_4->___isVertical_11 = 1;
		GUIStyle_t6_166 * L_5 = ___style;
		IL2CPP_RUNTIME_CLASS_INIT(GUIStyle_t6_166_il2cpp_TypeInfo_var);
		GUIStyle_t6_166 * L_6 = GUIStyle_get_none_m6_1316(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((((Object_t*)(GUIStyle_t6_166 *)L_5) == ((Object_t*)(GUIStyle_t6_166 *)L_6)))
		{
			goto IL_0031;
		}
	}
	{
		GUILayoutGroup_t6_169 * L_7 = V_0;
		NullCheck(L_7);
		Rect_t6_52  L_8 = (((GUILayoutEntry_t6_171 *)L_7)->___rect_4);
		GUIContent_t6_163 * L_9 = ___content;
		GUIStyle_t6_166 * L_10 = ___style;
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t6_160_il2cpp_TypeInfo_var);
		GUI_Box_m6_1048(NULL /*static, unused*/, L_8, L_9, L_10, /*hidden argument*/NULL);
	}

IL_0031:
	{
		return;
	}
}
// System.Void UnityEngine.GUILayout::EndVertical()
extern TypeInfo* GUILayoutUtility_t6_170_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2788;
extern "C" void GUILayout_EndVertical_m6_1132 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GUILayoutUtility_t6_170_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(990);
		_stringLiteral2788 = il2cpp_codegen_string_literal_from_index(2788);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GUILayoutUtility_t6_170_il2cpp_TypeInfo_var);
		GUILayoutUtility_EndGroup_m6_1153(NULL /*static, unused*/, _stringLiteral2788, /*hidden argument*/NULL);
		GUILayoutUtility_EndLayoutGroup_m6_1160(NULL /*static, unused*/, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.GUILayout::BeginArea(UnityEngine.Rect)
extern TypeInfo* GUIContent_t6_163_il2cpp_TypeInfo_var;
extern TypeInfo* GUIStyle_t6_166_il2cpp_TypeInfo_var;
extern "C" void GUILayout_BeginArea_m6_1133 (Object_t * __this /* static, unused */, Rect_t6_52  ___screenRect, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GUIContent_t6_163_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(985);
		GUIStyle_t6_166_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(988);
		s_Il2CppMethodIntialized = true;
	}
	{
		Rect_t6_52  L_0 = ___screenRect;
		IL2CPP_RUNTIME_CLASS_INIT(GUIContent_t6_163_il2cpp_TypeInfo_var);
		GUIContent_t6_163 * L_1 = ((GUIContent_t6_163_StaticFields*)GUIContent_t6_163_il2cpp_TypeInfo_var->static_fields)->___none_6;
		IL2CPP_RUNTIME_CLASS_INIT(GUIStyle_t6_166_il2cpp_TypeInfo_var);
		GUIStyle_t6_166 * L_2 = GUIStyle_get_none_m6_1316(NULL /*static, unused*/, /*hidden argument*/NULL);
		GUILayout_BeginArea_m6_1135(NULL /*static, unused*/, L_0, L_1, L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.GUILayout::BeginArea(UnityEngine.Rect,UnityEngine.GUIStyle)
extern TypeInfo* GUIContent_t6_163_il2cpp_TypeInfo_var;
extern "C" void GUILayout_BeginArea_m6_1134 (Object_t * __this /* static, unused */, Rect_t6_52  ___screenRect, GUIStyle_t6_166 * ___style, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GUIContent_t6_163_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(985);
		s_Il2CppMethodIntialized = true;
	}
	{
		Rect_t6_52  L_0 = ___screenRect;
		IL2CPP_RUNTIME_CLASS_INIT(GUIContent_t6_163_il2cpp_TypeInfo_var);
		GUIContent_t6_163 * L_1 = ((GUIContent_t6_163_StaticFields*)GUIContent_t6_163_il2cpp_TypeInfo_var->static_fields)->___none_6;
		GUIStyle_t6_166 * L_2 = ___style;
		GUILayout_BeginArea_m6_1135(NULL /*static, unused*/, L_0, L_1, L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.GUILayout::BeginArea(UnityEngine.Rect,UnityEngine.GUIContent,UnityEngine.GUIStyle)
extern const Il2CppType* GUILayoutGroup_t6_169_0_0_0_var;
extern TypeInfo* GUIUtility_t6_185_il2cpp_TypeInfo_var;
extern TypeInfo* Type_t_il2cpp_TypeInfo_var;
extern TypeInfo* GUILayoutUtility_t6_170_il2cpp_TypeInfo_var;
extern TypeInfo* GUI_t6_160_il2cpp_TypeInfo_var;
extern "C" void GUILayout_BeginArea_m6_1135 (Object_t * __this /* static, unused */, Rect_t6_52  ___screenRect, GUIContent_t6_163 * ___content, GUIStyle_t6_166 * ___style, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GUILayoutGroup_t6_169_0_0_0_var = il2cpp_codegen_type_from_index(994);
		GUIUtility_t6_185_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(984);
		Type_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3);
		GUILayoutUtility_t6_170_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(990);
		GUI_t6_160_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(982);
		s_Il2CppMethodIntialized = true;
	}
	GUILayoutGroup_t6_169 * V_0 = {0};
	float V_1 = 0.0f;
	{
		IL2CPP_RUNTIME_CLASS_INIT(GUIUtility_t6_185_il2cpp_TypeInfo_var);
		GUIUtility_CheckOnGUI_m6_1376(NULL /*static, unused*/, /*hidden argument*/NULL);
		GUIStyle_t6_166 * L_0 = ___style;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_1 = Type_GetTypeFromHandle_m1_893(NULL /*static, unused*/, LoadTypeToken(GUILayoutGroup_t6_169_0_0_0_var), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(GUILayoutUtility_t6_170_il2cpp_TypeInfo_var);
		GUILayoutGroup_t6_169 * L_2 = GUILayoutUtility_BeginLayoutArea_m6_1161(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		Event_t6_154 * L_3 = Event_get_current_m6_989(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_3);
		int32_t L_4 = Event_get_type_m6_1003(L_3, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_4) == ((uint32_t)8))))
		{
			goto IL_0088;
		}
	}
	{
		GUILayoutGroup_t6_169 * L_5 = V_0;
		NullCheck(L_5);
		L_5->___resetCoords_12 = 1;
		GUILayoutGroup_t6_169 * L_6 = V_0;
		GUILayoutGroup_t6_169 * L_7 = V_0;
		float L_8 = Rect_get_width_m6_277((&___screenRect), /*hidden argument*/NULL);
		float L_9 = L_8;
		V_1 = L_9;
		NullCheck(L_7);
		((GUILayoutEntry_t6_171 *)L_7)->___maxWidth_1 = L_9;
		float L_10 = V_1;
		NullCheck(L_6);
		((GUILayoutEntry_t6_171 *)L_6)->___minWidth_0 = L_10;
		GUILayoutGroup_t6_169 * L_11 = V_0;
		GUILayoutGroup_t6_169 * L_12 = V_0;
		float L_13 = Rect_get_height_m6_279((&___screenRect), /*hidden argument*/NULL);
		float L_14 = L_13;
		V_1 = L_14;
		NullCheck(L_12);
		((GUILayoutEntry_t6_171 *)L_12)->___maxHeight_3 = L_14;
		float L_15 = V_1;
		NullCheck(L_11);
		((GUILayoutEntry_t6_171 *)L_11)->___minHeight_2 = L_15;
		GUILayoutGroup_t6_169 * L_16 = V_0;
		float L_17 = Rect_get_xMin_m6_281((&___screenRect), /*hidden argument*/NULL);
		float L_18 = Rect_get_yMin_m6_282((&___screenRect), /*hidden argument*/NULL);
		GUILayoutGroup_t6_169 * L_19 = V_0;
		NullCheck(L_19);
		Rect_t6_52 * L_20 = &(((GUILayoutEntry_t6_171 *)L_19)->___rect_4);
		float L_21 = Rect_get_xMax_m6_283(L_20, /*hidden argument*/NULL);
		GUILayoutGroup_t6_169 * L_22 = V_0;
		NullCheck(L_22);
		Rect_t6_52 * L_23 = &(((GUILayoutEntry_t6_171 *)L_22)->___rect_4);
		float L_24 = Rect_get_yMax_m6_284(L_23, /*hidden argument*/NULL);
		Rect_t6_52  L_25 = Rect_MinMaxRect_m6_272(NULL /*static, unused*/, L_17, L_18, L_21, L_24, /*hidden argument*/NULL);
		NullCheck(L_16);
		((GUILayoutEntry_t6_171 *)L_16)->___rect_4 = L_25;
	}

IL_0088:
	{
		GUILayoutGroup_t6_169 * L_26 = V_0;
		NullCheck(L_26);
		Rect_t6_52  L_27 = (((GUILayoutEntry_t6_171 *)L_26)->___rect_4);
		GUIContent_t6_163 * L_28 = ___content;
		GUIStyle_t6_166 * L_29 = ___style;
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t6_160_il2cpp_TypeInfo_var);
		GUI_BeginGroup_m6_1071(NULL /*static, unused*/, L_27, L_28, L_29, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.GUILayout::EndArea()
extern TypeInfo* GUIUtility_t6_185_il2cpp_TypeInfo_var;
extern TypeInfo* GUILayoutUtility_t6_170_il2cpp_TypeInfo_var;
extern TypeInfo* GUILayoutGroup_t6_169_il2cpp_TypeInfo_var;
extern TypeInfo* GUI_t6_160_il2cpp_TypeInfo_var;
extern "C" void GUILayout_EndArea_m6_1136 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GUIUtility_t6_185_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(984);
		GUILayoutUtility_t6_170_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(990);
		GUILayoutGroup_t6_169_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(994);
		GUI_t6_160_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(982);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GUIUtility_t6_185_il2cpp_TypeInfo_var);
		GUIUtility_CheckOnGUI_m6_1376(NULL /*static, unused*/, /*hidden argument*/NULL);
		Event_t6_154 * L_0 = Event_get_current_m6_989(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_0);
		int32_t L_1 = Event_get_type_m6_1003(L_0, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_1) == ((uint32_t)((int32_t)12)))))
		{
			goto IL_0017;
		}
	}
	{
		return;
	}

IL_0017:
	{
		IL2CPP_RUNTIME_CLASS_INIT(GUILayoutUtility_t6_170_il2cpp_TypeInfo_var);
		LayoutCache_t6_168 * L_2 = ((GUILayoutUtility_t6_170_StaticFields*)GUILayoutUtility_t6_170_il2cpp_TypeInfo_var->static_fields)->___current_2;
		NullCheck(L_2);
		GenericStack_t6_162 * L_3 = (L_2->___layoutGroups_1);
		NullCheck(L_3);
		VirtFuncInvoker0< Object_t * >::Invoke(16 /* System.Object System.Collections.Stack::Pop() */, L_3);
		LayoutCache_t6_168 * L_4 = ((GUILayoutUtility_t6_170_StaticFields*)GUILayoutUtility_t6_170_il2cpp_TypeInfo_var->static_fields)->___current_2;
		LayoutCache_t6_168 * L_5 = ((GUILayoutUtility_t6_170_StaticFields*)GUILayoutUtility_t6_170_il2cpp_TypeInfo_var->static_fields)->___current_2;
		NullCheck(L_5);
		GenericStack_t6_162 * L_6 = (L_5->___layoutGroups_1);
		NullCheck(L_6);
		Object_t * L_7 = (Object_t *)VirtFuncInvoker0< Object_t * >::Invoke(15 /* System.Object System.Collections.Stack::Peek() */, L_6);
		NullCheck(L_4);
		L_4->___topLevel_0 = ((GUILayoutGroup_t6_169 *)CastclassClass(L_7, GUILayoutGroup_t6_169_il2cpp_TypeInfo_var));
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t6_160_il2cpp_TypeInfo_var);
		GUI_EndGroup_m6_1072(NULL /*static, unused*/, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Vector2 UnityEngine.GUILayout::BeginScrollView(UnityEngine.Vector2,UnityEngine.GUILayoutOption[])
extern TypeInfo* GUI_t6_160_il2cpp_TypeInfo_var;
extern "C" Vector2_t6_48  GUILayout_BeginScrollView_m6_1137 (Object_t * __this /* static, unused */, Vector2_t6_48  ___scrollPosition, GUILayoutOptionU5BU5D_t6_165* ___options, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GUI_t6_160_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(982);
		s_Il2CppMethodIntialized = true;
	}
	{
		Vector2_t6_48  L_0 = ___scrollPosition;
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t6_160_il2cpp_TypeInfo_var);
		GUISkin_t6_161 * L_1 = GUI_get_skin_m6_1041(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_1);
		GUIStyle_t6_166 * L_2 = GUISkin_get_horizontalScrollbar_m6_1241(L_1, /*hidden argument*/NULL);
		GUISkin_t6_161 * L_3 = GUI_get_skin_m6_1041(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_3);
		GUIStyle_t6_166 * L_4 = GUISkin_get_verticalScrollbar_m6_1249(L_3, /*hidden argument*/NULL);
		GUISkin_t6_161 * L_5 = GUI_get_skin_m6_1041(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_5);
		GUIStyle_t6_166 * L_6 = GUISkin_get_scrollView_m6_1257(L_5, /*hidden argument*/NULL);
		GUILayoutOptionU5BU5D_t6_165* L_7 = ___options;
		Vector2_t6_48  L_8 = GUILayout_BeginScrollView_m6_1138(NULL /*static, unused*/, L_0, 0, 0, L_2, L_4, L_6, L_7, /*hidden argument*/NULL);
		return L_8;
	}
}
// UnityEngine.Vector2 UnityEngine.GUILayout::BeginScrollView(UnityEngine.Vector2,System.Boolean,System.Boolean,UnityEngine.GUIStyle,UnityEngine.GUIStyle,UnityEngine.GUIStyle,UnityEngine.GUILayoutOption[])
extern const Il2CppType* GUIScrollGroup_t6_173_0_0_0_var;
extern TypeInfo* GUIUtility_t6_185_il2cpp_TypeInfo_var;
extern TypeInfo* Type_t_il2cpp_TypeInfo_var;
extern TypeInfo* GUILayoutUtility_t6_170_il2cpp_TypeInfo_var;
extern TypeInfo* GUIScrollGroup_t6_173_il2cpp_TypeInfo_var;
extern TypeInfo* GUI_t6_160_il2cpp_TypeInfo_var;
extern "C" Vector2_t6_48  GUILayout_BeginScrollView_m6_1138 (Object_t * __this /* static, unused */, Vector2_t6_48  ___scrollPosition, bool ___alwaysShowHorizontal, bool ___alwaysShowVertical, GUIStyle_t6_166 * ___horizontalScrollbar, GUIStyle_t6_166 * ___verticalScrollbar, GUIStyle_t6_166 * ___background, GUILayoutOptionU5BU5D_t6_165* ___options, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GUIScrollGroup_t6_173_0_0_0_var = il2cpp_codegen_type_from_index(995);
		GUIUtility_t6_185_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(984);
		Type_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3);
		GUILayoutUtility_t6_170_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(990);
		GUIScrollGroup_t6_173_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(995);
		GUI_t6_160_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(982);
		s_Il2CppMethodIntialized = true;
	}
	GUIScrollGroup_t6_173 * V_0 = {0};
	int32_t V_1 = {0};
	{
		IL2CPP_RUNTIME_CLASS_INIT(GUIUtility_t6_185_il2cpp_TypeInfo_var);
		GUIUtility_CheckOnGUI_m6_1376(NULL /*static, unused*/, /*hidden argument*/NULL);
		GUIStyle_t6_166 * L_0 = ___background;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_1 = Type_GetTypeFromHandle_m1_893(NULL /*static, unused*/, LoadTypeToken(GUIScrollGroup_t6_173_0_0_0_var), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(GUILayoutUtility_t6_170_il2cpp_TypeInfo_var);
		GUILayoutGroup_t6_169 * L_2 = GUILayoutUtility_BeginLayoutGroup_m6_1159(NULL /*static, unused*/, L_0, (GUILayoutOptionU5BU5D_t6_165*)(GUILayoutOptionU5BU5D_t6_165*)NULL, L_1, /*hidden argument*/NULL);
		V_0 = ((GUIScrollGroup_t6_173 *)CastclassSealed(L_2, GUIScrollGroup_t6_173_il2cpp_TypeInfo_var));
		Event_t6_154 * L_3 = Event_get_current_m6_989(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_3);
		int32_t L_4 = Event_get_type_m6_1003(L_3, /*hidden argument*/NULL);
		V_1 = L_4;
		int32_t L_5 = V_1;
		if ((((int32_t)L_5) == ((int32_t)8)))
		{
			goto IL_0034;
		}
	}
	{
		goto IL_007a;
	}

IL_0034:
	{
		GUIScrollGroup_t6_173 * L_6 = V_0;
		NullCheck(L_6);
		((GUILayoutGroup_t6_169 *)L_6)->___resetCoords_12 = 1;
		GUIScrollGroup_t6_173 * L_7 = V_0;
		NullCheck(L_7);
		((GUILayoutGroup_t6_169 *)L_7)->___isVertical_11 = 1;
		GUIScrollGroup_t6_173 * L_8 = V_0;
		NullCheck(L_8);
		((GUILayoutEntry_t6_171 *)L_8)->___stretchWidth_5 = 1;
		GUIScrollGroup_t6_173 * L_9 = V_0;
		NullCheck(L_9);
		((GUILayoutEntry_t6_171 *)L_9)->___stretchHeight_6 = 1;
		GUIScrollGroup_t6_173 * L_10 = V_0;
		GUIStyle_t6_166 * L_11 = ___verticalScrollbar;
		NullCheck(L_10);
		L_10->___verticalScrollbar_38 = L_11;
		GUIScrollGroup_t6_173 * L_12 = V_0;
		GUIStyle_t6_166 * L_13 = ___horizontalScrollbar;
		NullCheck(L_12);
		L_12->___horizontalScrollbar_37 = L_13;
		GUIScrollGroup_t6_173 * L_14 = V_0;
		bool L_15 = ___alwaysShowVertical;
		NullCheck(L_14);
		L_14->___needsVerticalScrollbar_36 = L_15;
		GUIScrollGroup_t6_173 * L_16 = V_0;
		bool L_17 = ___alwaysShowHorizontal;
		NullCheck(L_16);
		L_16->___needsHorizontalScrollbar_35 = L_17;
		GUIScrollGroup_t6_173 * L_18 = V_0;
		GUILayoutOptionU5BU5D_t6_165* L_19 = ___options;
		NullCheck(L_18);
		VirtActionInvoker1< GUILayoutOptionU5BU5D_t6_165* >::Invoke(10 /* System.Void UnityEngine.GUILayoutGroup::ApplyOptions(UnityEngine.GUILayoutOption[]) */, L_18, L_19);
		goto IL_007f;
	}

IL_007a:
	{
		goto IL_007f;
	}

IL_007f:
	{
		GUIScrollGroup_t6_173 * L_20 = V_0;
		NullCheck(L_20);
		Rect_t6_52  L_21 = (((GUILayoutEntry_t6_171 *)L_20)->___rect_4);
		Vector2_t6_48  L_22 = ___scrollPosition;
		GUIScrollGroup_t6_173 * L_23 = V_0;
		NullCheck(L_23);
		float L_24 = (L_23->___clientWidth_31);
		GUIScrollGroup_t6_173 * L_25 = V_0;
		NullCheck(L_25);
		float L_26 = (L_25->___clientHeight_32);
		Rect_t6_52  L_27 = {0};
		Rect__ctor_m6_270(&L_27, (0.0f), (0.0f), L_24, L_26, /*hidden argument*/NULL);
		bool L_28 = ___alwaysShowHorizontal;
		bool L_29 = ___alwaysShowVertical;
		GUIStyle_t6_166 * L_30 = ___horizontalScrollbar;
		GUIStyle_t6_166 * L_31 = ___verticalScrollbar;
		GUIStyle_t6_166 * L_32 = ___background;
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t6_160_il2cpp_TypeInfo_var);
		Vector2_t6_48  L_33 = GUI_BeginScrollView_m6_1073(NULL /*static, unused*/, L_21, L_22, L_27, L_28, L_29, L_30, L_31, L_32, /*hidden argument*/NULL);
		return L_33;
	}
}
// System.Void UnityEngine.GUILayout::EndScrollView()
extern "C" void GUILayout_EndScrollView_m6_1139 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	{
		GUILayout_EndScrollView_m6_1140(NULL /*static, unused*/, 1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.GUILayout::EndScrollView(System.Boolean)
extern TypeInfo* GUILayoutUtility_t6_170_il2cpp_TypeInfo_var;
extern TypeInfo* GUI_t6_160_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2789;
extern "C" void GUILayout_EndScrollView_m6_1140 (Object_t * __this /* static, unused */, bool ___handleScrollWheel, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GUILayoutUtility_t6_170_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(990);
		GUI_t6_160_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(982);
		_stringLiteral2789 = il2cpp_codegen_string_literal_from_index(2789);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GUILayoutUtility_t6_170_il2cpp_TypeInfo_var);
		GUILayoutUtility_EndGroup_m6_1153(NULL /*static, unused*/, _stringLiteral2789, /*hidden argument*/NULL);
		GUILayoutUtility_EndLayoutGroup_m6_1160(NULL /*static, unused*/, /*hidden argument*/NULL);
		bool L_0 = ___handleScrollWheel;
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t6_160_il2cpp_TypeInfo_var);
		GUI_EndScrollView_m6_1074(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Rect UnityEngine.GUILayout::Window(System.Int32,UnityEngine.Rect,UnityEngine.GUI/WindowFunction,System.String,UnityEngine.GUILayoutOption[])
extern TypeInfo* GUIContent_t6_163_il2cpp_TypeInfo_var;
extern TypeInfo* GUI_t6_160_il2cpp_TypeInfo_var;
extern "C" Rect_t6_52  GUILayout_Window_m6_1141 (Object_t * __this /* static, unused */, int32_t ___id, Rect_t6_52  ___screenRect, WindowFunction_t6_159 * ___func, String_t* ___text, GUILayoutOptionU5BU5D_t6_165* ___options, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GUIContent_t6_163_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(985);
		GUI_t6_160_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(982);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = ___id;
		Rect_t6_52  L_1 = ___screenRect;
		WindowFunction_t6_159 * L_2 = ___func;
		String_t* L_3 = ___text;
		IL2CPP_RUNTIME_CLASS_INIT(GUIContent_t6_163_il2cpp_TypeInfo_var);
		GUIContent_t6_163 * L_4 = GUIContent_Temp_m6_1106(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t6_160_il2cpp_TypeInfo_var);
		GUISkin_t6_161 * L_5 = GUI_get_skin_m6_1041(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_5);
		GUIStyle_t6_166 * L_6 = GUISkin_get_window_m6_1231(L_5, /*hidden argument*/NULL);
		GUILayoutOptionU5BU5D_t6_165* L_7 = ___options;
		Rect_t6_52  L_8 = GUILayout_DoWindow_m6_1142(NULL /*static, unused*/, L_0, L_1, L_2, L_4, L_6, L_7, /*hidden argument*/NULL);
		return L_8;
	}
}
// UnityEngine.Rect UnityEngine.GUILayout::DoWindow(System.Int32,UnityEngine.Rect,UnityEngine.GUI/WindowFunction,UnityEngine.GUIContent,UnityEngine.GUIStyle,UnityEngine.GUILayoutOption[])
extern TypeInfo* GUIUtility_t6_185_il2cpp_TypeInfo_var;
extern TypeInfo* LayoutedWindow_t6_164_il2cpp_TypeInfo_var;
extern TypeInfo* WindowFunction_t6_159_il2cpp_TypeInfo_var;
extern TypeInfo* GUI_t6_160_il2cpp_TypeInfo_var;
extern const MethodInfo* LayoutedWindow_DoWindow_m6_1111_MethodInfo_var;
extern "C" Rect_t6_52  GUILayout_DoWindow_m6_1142 (Object_t * __this /* static, unused */, int32_t ___id, Rect_t6_52  ___screenRect, WindowFunction_t6_159 * ___func, GUIContent_t6_163 * ___content, GUIStyle_t6_166 * ___style, GUILayoutOptionU5BU5D_t6_165* ___options, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GUIUtility_t6_185_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(984);
		LayoutedWindow_t6_164_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(996);
		WindowFunction_t6_159_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(997);
		GUI_t6_160_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(982);
		LayoutedWindow_DoWindow_m6_1111_MethodInfo_var = il2cpp_codegen_method_info_from_index(162);
		s_Il2CppMethodIntialized = true;
	}
	LayoutedWindow_t6_164 * V_0 = {0};
	{
		IL2CPP_RUNTIME_CLASS_INIT(GUIUtility_t6_185_il2cpp_TypeInfo_var);
		GUIUtility_CheckOnGUI_m6_1376(NULL /*static, unused*/, /*hidden argument*/NULL);
		WindowFunction_t6_159 * L_0 = ___func;
		Rect_t6_52  L_1 = ___screenRect;
		GUIContent_t6_163 * L_2 = ___content;
		GUILayoutOptionU5BU5D_t6_165* L_3 = ___options;
		GUIStyle_t6_166 * L_4 = ___style;
		LayoutedWindow_t6_164 * L_5 = (LayoutedWindow_t6_164 *)il2cpp_codegen_object_new (LayoutedWindow_t6_164_il2cpp_TypeInfo_var);
		LayoutedWindow__ctor_m6_1110(L_5, L_0, L_1, L_2, L_3, L_4, /*hidden argument*/NULL);
		V_0 = L_5;
		int32_t L_6 = ___id;
		Rect_t6_52  L_7 = ___screenRect;
		LayoutedWindow_t6_164 * L_8 = V_0;
		IntPtr_t L_9 = { (void*)LayoutedWindow_DoWindow_m6_1111_MethodInfo_var };
		WindowFunction_t6_159 * L_10 = (WindowFunction_t6_159 *)il2cpp_codegen_object_new (WindowFunction_t6_159_il2cpp_TypeInfo_var);
		WindowFunction__ctor_m6_1031(L_10, L_8, L_9, /*hidden argument*/NULL);
		GUIContent_t6_163 * L_11 = ___content;
		GUIStyle_t6_166 * L_12 = ___style;
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t6_160_il2cpp_TypeInfo_var);
		Rect_t6_52  L_13 = GUI_Window_m6_1075(NULL /*static, unused*/, L_6, L_7, L_10, L_11, L_12, /*hidden argument*/NULL);
		return L_13;
	}
}
// UnityEngine.GUILayoutOption UnityEngine.GUILayout::Width(System.Single)
extern TypeInfo* Single_t1_17_il2cpp_TypeInfo_var;
extern TypeInfo* GUILayoutOption_t6_176_il2cpp_TypeInfo_var;
extern "C" GUILayoutOption_t6_176 * GUILayout_Width_m6_1143 (Object_t * __this /* static, unused */, float ___width, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Single_t1_17_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(43);
		GUILayoutOption_t6_176_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(992);
		s_Il2CppMethodIntialized = true;
	}
	{
		float L_0 = ___width;
		float L_1 = L_0;
		Object_t * L_2 = Box(Single_t1_17_il2cpp_TypeInfo_var, &L_1);
		GUILayoutOption_t6_176 * L_3 = (GUILayoutOption_t6_176 *)il2cpp_codegen_object_new (GUILayoutOption_t6_176_il2cpp_TypeInfo_var);
		GUILayoutOption__ctor_m6_1203(L_3, 0, L_2, /*hidden argument*/NULL);
		return L_3;
	}
}
// UnityEngine.GUILayoutOption UnityEngine.GUILayout::MinWidth(System.Single)
extern TypeInfo* Single_t1_17_il2cpp_TypeInfo_var;
extern TypeInfo* GUILayoutOption_t6_176_il2cpp_TypeInfo_var;
extern "C" GUILayoutOption_t6_176 * GUILayout_MinWidth_m6_1144 (Object_t * __this /* static, unused */, float ___minWidth, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Single_t1_17_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(43);
		GUILayoutOption_t6_176_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(992);
		s_Il2CppMethodIntialized = true;
	}
	{
		float L_0 = ___minWidth;
		float L_1 = L_0;
		Object_t * L_2 = Box(Single_t1_17_il2cpp_TypeInfo_var, &L_1);
		GUILayoutOption_t6_176 * L_3 = (GUILayoutOption_t6_176 *)il2cpp_codegen_object_new (GUILayoutOption_t6_176_il2cpp_TypeInfo_var);
		GUILayoutOption__ctor_m6_1203(L_3, 2, L_2, /*hidden argument*/NULL);
		return L_3;
	}
}
// UnityEngine.GUILayoutOption UnityEngine.GUILayout::Height(System.Single)
extern TypeInfo* Single_t1_17_il2cpp_TypeInfo_var;
extern TypeInfo* GUILayoutOption_t6_176_il2cpp_TypeInfo_var;
extern "C" GUILayoutOption_t6_176 * GUILayout_Height_m6_1145 (Object_t * __this /* static, unused */, float ___height, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Single_t1_17_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(43);
		GUILayoutOption_t6_176_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(992);
		s_Il2CppMethodIntialized = true;
	}
	{
		float L_0 = ___height;
		float L_1 = L_0;
		Object_t * L_2 = Box(Single_t1_17_il2cpp_TypeInfo_var, &L_1);
		GUILayoutOption_t6_176 * L_3 = (GUILayoutOption_t6_176 *)il2cpp_codegen_object_new (GUILayoutOption_t6_176_il2cpp_TypeInfo_var);
		GUILayoutOption__ctor_m6_1203(L_3, 1, L_2, /*hidden argument*/NULL);
		return L_3;
	}
}
// UnityEngine.GUILayoutOption UnityEngine.GUILayout::ExpandWidth(System.Boolean)
extern TypeInfo* Int32_t1_3_il2cpp_TypeInfo_var;
extern TypeInfo* GUILayoutOption_t6_176_il2cpp_TypeInfo_var;
extern "C" GUILayoutOption_t6_176 * GUILayout_ExpandWidth_m6_1146 (Object_t * __this /* static, unused */, bool ___expand, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Int32_t1_3_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(8);
		GUILayoutOption_t6_176_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(992);
		s_Il2CppMethodIntialized = true;
	}
	int32_t G_B2_0 = 0;
	int32_t G_B1_0 = 0;
	int32_t G_B3_0 = 0;
	int32_t G_B3_1 = 0;
	{
		bool L_0 = ___expand;
		G_B1_0 = 6;
		if (!L_0)
		{
			G_B2_0 = 6;
			goto IL_000d;
		}
	}
	{
		G_B3_0 = 1;
		G_B3_1 = G_B1_0;
		goto IL_000e;
	}

IL_000d:
	{
		G_B3_0 = 0;
		G_B3_1 = G_B2_0;
	}

IL_000e:
	{
		int32_t L_1 = G_B3_0;
		Object_t * L_2 = Box(Int32_t1_3_il2cpp_TypeInfo_var, &L_1);
		GUILayoutOption_t6_176 * L_3 = (GUILayoutOption_t6_176 *)il2cpp_codegen_object_new (GUILayoutOption_t6_176_il2cpp_TypeInfo_var);
		GUILayoutOption__ctor_m6_1203(L_3, G_B3_1, L_2, /*hidden argument*/NULL);
		return L_3;
	}
}
// UnityEngine.GUILayoutOption UnityEngine.GUILayout::ExpandHeight(System.Boolean)
extern TypeInfo* Int32_t1_3_il2cpp_TypeInfo_var;
extern TypeInfo* GUILayoutOption_t6_176_il2cpp_TypeInfo_var;
extern "C" GUILayoutOption_t6_176 * GUILayout_ExpandHeight_m6_1147 (Object_t * __this /* static, unused */, bool ___expand, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Int32_t1_3_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(8);
		GUILayoutOption_t6_176_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(992);
		s_Il2CppMethodIntialized = true;
	}
	int32_t G_B2_0 = 0;
	int32_t G_B1_0 = 0;
	int32_t G_B3_0 = 0;
	int32_t G_B3_1 = 0;
	{
		bool L_0 = ___expand;
		G_B1_0 = 7;
		if (!L_0)
		{
			G_B2_0 = 7;
			goto IL_000d;
		}
	}
	{
		G_B3_0 = 1;
		G_B3_1 = G_B1_0;
		goto IL_000e;
	}

IL_000d:
	{
		G_B3_0 = 0;
		G_B3_1 = G_B2_0;
	}

IL_000e:
	{
		int32_t L_1 = G_B3_0;
		Object_t * L_2 = Box(Int32_t1_3_il2cpp_TypeInfo_var, &L_1);
		GUILayoutOption_t6_176 * L_3 = (GUILayoutOption_t6_176 *)il2cpp_codegen_object_new (GUILayoutOption_t6_176_il2cpp_TypeInfo_var);
		GUILayoutOption__ctor_m6_1203(L_3, G_B3_1, L_2, /*hidden argument*/NULL);
		return L_3;
	}
}
// System.Void UnityEngine.GUILayoutUtility/LayoutCache::.ctor()
extern TypeInfo* GUILayoutGroup_t6_169_il2cpp_TypeInfo_var;
extern TypeInfo* GenericStack_t6_162_il2cpp_TypeInfo_var;
extern "C" void LayoutCache__ctor_m6_1148 (LayoutCache_t6_168 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GUILayoutGroup_t6_169_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(994);
		GenericStack_t6_162_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(983);
		s_Il2CppMethodIntialized = true;
	}
	{
		GUILayoutGroup_t6_169 * L_0 = (GUILayoutGroup_t6_169 *)il2cpp_codegen_object_new (GUILayoutGroup_t6_169_il2cpp_TypeInfo_var);
		GUILayoutGroup__ctor_m6_1183(L_0, /*hidden argument*/NULL);
		__this->___topLevel_0 = L_0;
		GenericStack_t6_162 * L_1 = (GenericStack_t6_162 *)il2cpp_codegen_object_new (GenericStack_t6_162_il2cpp_TypeInfo_var);
		GenericStack__ctor_m6_1648(L_1, /*hidden argument*/NULL);
		__this->___layoutGroups_1 = L_1;
		GUILayoutGroup_t6_169 * L_2 = (GUILayoutGroup_t6_169 *)il2cpp_codegen_object_new (GUILayoutGroup_t6_169_il2cpp_TypeInfo_var);
		GUILayoutGroup__ctor_m6_1183(L_2, /*hidden argument*/NULL);
		__this->___windows_2 = L_2;
		Object__ctor_m1_0(__this, /*hidden argument*/NULL);
		GenericStack_t6_162 * L_3 = (__this->___layoutGroups_1);
		GUILayoutGroup_t6_169 * L_4 = (__this->___topLevel_0);
		NullCheck(L_3);
		VirtActionInvoker1< Object_t * >::Invoke(17 /* System.Void System.Collections.Stack::Push(System.Object) */, L_3, L_4);
		return;
	}
}
// System.Void UnityEngine.GUILayoutUtility::.cctor()
extern TypeInfo* Dictionary_2_t1_916_il2cpp_TypeInfo_var;
extern TypeInfo* GUILayoutUtility_t6_170_il2cpp_TypeInfo_var;
extern TypeInfo* LayoutCache_t6_168_il2cpp_TypeInfo_var;
extern const MethodInfo* Dictionary_2__ctor_m1_5603_MethodInfo_var;
extern "C" void GUILayoutUtility__cctor_m6_1149 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Dictionary_2_t1_916_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(999);
		GUILayoutUtility_t6_170_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(990);
		LayoutCache_t6_168_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(998);
		Dictionary_2__ctor_m1_5603_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483811);
		s_Il2CppMethodIntialized = true;
	}
	{
		Dictionary_2_t1_916 * L_0 = (Dictionary_2_t1_916 *)il2cpp_codegen_object_new (Dictionary_2_t1_916_il2cpp_TypeInfo_var);
		Dictionary_2__ctor_m1_5603(L_0, /*hidden argument*/Dictionary_2__ctor_m1_5603_MethodInfo_var);
		((GUILayoutUtility_t6_170_StaticFields*)GUILayoutUtility_t6_170_il2cpp_TypeInfo_var->static_fields)->___s_StoredLayouts_0 = L_0;
		Dictionary_2_t1_916 * L_1 = (Dictionary_2_t1_916 *)il2cpp_codegen_object_new (Dictionary_2_t1_916_il2cpp_TypeInfo_var);
		Dictionary_2__ctor_m1_5603(L_1, /*hidden argument*/Dictionary_2__ctor_m1_5603_MethodInfo_var);
		((GUILayoutUtility_t6_170_StaticFields*)GUILayoutUtility_t6_170_il2cpp_TypeInfo_var->static_fields)->___s_StoredWindows_1 = L_1;
		LayoutCache_t6_168 * L_2 = (LayoutCache_t6_168 *)il2cpp_codegen_object_new (LayoutCache_t6_168_il2cpp_TypeInfo_var);
		LayoutCache__ctor_m6_1148(L_2, /*hidden argument*/NULL);
		((GUILayoutUtility_t6_170_StaticFields*)GUILayoutUtility_t6_170_il2cpp_TypeInfo_var->static_fields)->___current_2 = L_2;
		Rect_t6_52  L_3 = {0};
		Rect__ctor_m6_270(&L_3, (0.0f), (0.0f), (1.0f), (1.0f), /*hidden argument*/NULL);
		((GUILayoutUtility_t6_170_StaticFields*)GUILayoutUtility_t6_170_il2cpp_TypeInfo_var->static_fields)->___kDummyRect_3 = L_3;
		return;
	}
}
// UnityEngine.GUILayoutUtility/LayoutCache UnityEngine.GUILayoutUtility::SelectIDList(System.Int32,System.Boolean)
extern TypeInfo* GUILayoutUtility_t6_170_il2cpp_TypeInfo_var;
extern TypeInfo* LayoutCache_t6_168_il2cpp_TypeInfo_var;
extern "C" LayoutCache_t6_168 * GUILayoutUtility_SelectIDList_m6_1150 (Object_t * __this /* static, unused */, int32_t ___instanceID, bool ___isWindow, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GUILayoutUtility_t6_170_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(990);
		LayoutCache_t6_168_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(998);
		s_Il2CppMethodIntialized = true;
	}
	Dictionary_2_t1_916 * V_0 = {0};
	LayoutCache_t6_168 * V_1 = {0};
	Dictionary_2_t1_916 * G_B3_0 = {0};
	{
		bool L_0 = ___isWindow;
		if (!L_0)
		{
			goto IL_0010;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GUILayoutUtility_t6_170_il2cpp_TypeInfo_var);
		Dictionary_2_t1_916 * L_1 = ((GUILayoutUtility_t6_170_StaticFields*)GUILayoutUtility_t6_170_il2cpp_TypeInfo_var->static_fields)->___s_StoredWindows_1;
		G_B3_0 = L_1;
		goto IL_0015;
	}

IL_0010:
	{
		IL2CPP_RUNTIME_CLASS_INIT(GUILayoutUtility_t6_170_il2cpp_TypeInfo_var);
		Dictionary_2_t1_916 * L_2 = ((GUILayoutUtility_t6_170_StaticFields*)GUILayoutUtility_t6_170_il2cpp_TypeInfo_var->static_fields)->___s_StoredLayouts_0;
		G_B3_0 = L_2;
	}

IL_0015:
	{
		V_0 = G_B3_0;
		Dictionary_2_t1_916 * L_3 = V_0;
		int32_t L_4 = ___instanceID;
		NullCheck(L_3);
		bool L_5 = (bool)VirtFuncInvoker2< bool, int32_t, LayoutCache_t6_168 ** >::Invoke(32 /* System.Boolean System.Collections.Generic.Dictionary`2<System.Int32,UnityEngine.GUILayoutUtility/LayoutCache>::TryGetValue(!0,!1&) */, L_3, L_4, (&V_1));
		if (L_5)
		{
			goto IL_0037;
		}
	}
	{
		LayoutCache_t6_168 * L_6 = (LayoutCache_t6_168 *)il2cpp_codegen_object_new (LayoutCache_t6_168_il2cpp_TypeInfo_var);
		LayoutCache__ctor_m6_1148(L_6, /*hidden argument*/NULL);
		V_1 = L_6;
		Dictionary_2_t1_916 * L_7 = V_0;
		int32_t L_8 = ___instanceID;
		LayoutCache_t6_168 * L_9 = V_1;
		NullCheck(L_7);
		VirtActionInvoker2< int32_t, LayoutCache_t6_168 * >::Invoke(26 /* System.Void System.Collections.Generic.Dictionary`2<System.Int32,UnityEngine.GUILayoutUtility/LayoutCache>::set_Item(!0,!1) */, L_7, L_8, L_9);
		goto IL_0037;
	}

IL_0037:
	{
		IL2CPP_RUNTIME_CLASS_INIT(GUILayoutUtility_t6_170_il2cpp_TypeInfo_var);
		LayoutCache_t6_168 * L_10 = ((GUILayoutUtility_t6_170_StaticFields*)GUILayoutUtility_t6_170_il2cpp_TypeInfo_var->static_fields)->___current_2;
		LayoutCache_t6_168 * L_11 = V_1;
		NullCheck(L_11);
		GUILayoutGroup_t6_169 * L_12 = (L_11->___topLevel_0);
		NullCheck(L_10);
		L_10->___topLevel_0 = L_12;
		LayoutCache_t6_168 * L_13 = ((GUILayoutUtility_t6_170_StaticFields*)GUILayoutUtility_t6_170_il2cpp_TypeInfo_var->static_fields)->___current_2;
		LayoutCache_t6_168 * L_14 = V_1;
		NullCheck(L_14);
		GenericStack_t6_162 * L_15 = (L_14->___layoutGroups_1);
		NullCheck(L_13);
		L_13->___layoutGroups_1 = L_15;
		LayoutCache_t6_168 * L_16 = ((GUILayoutUtility_t6_170_StaticFields*)GUILayoutUtility_t6_170_il2cpp_TypeInfo_var->static_fields)->___current_2;
		LayoutCache_t6_168 * L_17 = V_1;
		NullCheck(L_17);
		GUILayoutGroup_t6_169 * L_18 = (L_17->___windows_2);
		NullCheck(L_16);
		L_16->___windows_2 = L_18;
		LayoutCache_t6_168 * L_19 = V_1;
		return L_19;
	}
}
// System.Void UnityEngine.GUILayoutUtility::Begin(System.Int32)
extern TypeInfo* GUILayoutUtility_t6_170_il2cpp_TypeInfo_var;
extern TypeInfo* GUILayoutGroup_t6_169_il2cpp_TypeInfo_var;
extern "C" void GUILayoutUtility_Begin_m6_1151 (Object_t * __this /* static, unused */, int32_t ___instanceID, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GUILayoutUtility_t6_170_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(990);
		GUILayoutGroup_t6_169_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(994);
		s_Il2CppMethodIntialized = true;
	}
	LayoutCache_t6_168 * V_0 = {0};
	GUILayoutGroup_t6_169 * V_1 = {0};
	{
		int32_t L_0 = ___instanceID;
		IL2CPP_RUNTIME_CLASS_INIT(GUILayoutUtility_t6_170_il2cpp_TypeInfo_var);
		LayoutCache_t6_168 * L_1 = GUILayoutUtility_SelectIDList_m6_1150(NULL /*static, unused*/, L_0, 0, /*hidden argument*/NULL);
		V_0 = L_1;
		Event_t6_154 * L_2 = Event_get_current_m6_989(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_2);
		int32_t L_3 = Event_get_type_m6_1003(L_2, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_3) == ((uint32_t)8))))
		{
			goto IL_0075;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GUILayoutUtility_t6_170_il2cpp_TypeInfo_var);
		LayoutCache_t6_168 * L_4 = ((GUILayoutUtility_t6_170_StaticFields*)GUILayoutUtility_t6_170_il2cpp_TypeInfo_var->static_fields)->___current_2;
		LayoutCache_t6_168 * L_5 = V_0;
		GUILayoutGroup_t6_169 * L_6 = (GUILayoutGroup_t6_169 *)il2cpp_codegen_object_new (GUILayoutGroup_t6_169_il2cpp_TypeInfo_var);
		GUILayoutGroup__ctor_m6_1183(L_6, /*hidden argument*/NULL);
		GUILayoutGroup_t6_169 * L_7 = L_6;
		V_1 = L_7;
		NullCheck(L_5);
		L_5->___topLevel_0 = L_7;
		GUILayoutGroup_t6_169 * L_8 = V_1;
		NullCheck(L_4);
		L_4->___topLevel_0 = L_8;
		LayoutCache_t6_168 * L_9 = ((GUILayoutUtility_t6_170_StaticFields*)GUILayoutUtility_t6_170_il2cpp_TypeInfo_var->static_fields)->___current_2;
		NullCheck(L_9);
		GenericStack_t6_162 * L_10 = (L_9->___layoutGroups_1);
		NullCheck(L_10);
		VirtActionInvoker0::Invoke(11 /* System.Void System.Collections.Stack::Clear() */, L_10);
		LayoutCache_t6_168 * L_11 = ((GUILayoutUtility_t6_170_StaticFields*)GUILayoutUtility_t6_170_il2cpp_TypeInfo_var->static_fields)->___current_2;
		NullCheck(L_11);
		GenericStack_t6_162 * L_12 = (L_11->___layoutGroups_1);
		LayoutCache_t6_168 * L_13 = ((GUILayoutUtility_t6_170_StaticFields*)GUILayoutUtility_t6_170_il2cpp_TypeInfo_var->static_fields)->___current_2;
		NullCheck(L_13);
		GUILayoutGroup_t6_169 * L_14 = (L_13->___topLevel_0);
		NullCheck(L_12);
		VirtActionInvoker1< Object_t * >::Invoke(17 /* System.Void System.Collections.Stack::Push(System.Object) */, L_12, L_14);
		LayoutCache_t6_168 * L_15 = ((GUILayoutUtility_t6_170_StaticFields*)GUILayoutUtility_t6_170_il2cpp_TypeInfo_var->static_fields)->___current_2;
		LayoutCache_t6_168 * L_16 = V_0;
		GUILayoutGroup_t6_169 * L_17 = (GUILayoutGroup_t6_169 *)il2cpp_codegen_object_new (GUILayoutGroup_t6_169_il2cpp_TypeInfo_var);
		GUILayoutGroup__ctor_m6_1183(L_17, /*hidden argument*/NULL);
		GUILayoutGroup_t6_169 * L_18 = L_17;
		V_1 = L_18;
		NullCheck(L_16);
		L_16->___windows_2 = L_18;
		GUILayoutGroup_t6_169 * L_19 = V_1;
		NullCheck(L_15);
		L_15->___windows_2 = L_19;
		goto IL_00a5;
	}

IL_0075:
	{
		IL2CPP_RUNTIME_CLASS_INIT(GUILayoutUtility_t6_170_il2cpp_TypeInfo_var);
		LayoutCache_t6_168 * L_20 = ((GUILayoutUtility_t6_170_StaticFields*)GUILayoutUtility_t6_170_il2cpp_TypeInfo_var->static_fields)->___current_2;
		LayoutCache_t6_168 * L_21 = V_0;
		NullCheck(L_21);
		GUILayoutGroup_t6_169 * L_22 = (L_21->___topLevel_0);
		NullCheck(L_20);
		L_20->___topLevel_0 = L_22;
		LayoutCache_t6_168 * L_23 = ((GUILayoutUtility_t6_170_StaticFields*)GUILayoutUtility_t6_170_il2cpp_TypeInfo_var->static_fields)->___current_2;
		LayoutCache_t6_168 * L_24 = V_0;
		NullCheck(L_24);
		GenericStack_t6_162 * L_25 = (L_24->___layoutGroups_1);
		NullCheck(L_23);
		L_23->___layoutGroups_1 = L_25;
		LayoutCache_t6_168 * L_26 = ((GUILayoutUtility_t6_170_StaticFields*)GUILayoutUtility_t6_170_il2cpp_TypeInfo_var->static_fields)->___current_2;
		LayoutCache_t6_168 * L_27 = V_0;
		NullCheck(L_27);
		GUILayoutGroup_t6_169 * L_28 = (L_27->___windows_2);
		NullCheck(L_26);
		L_26->___windows_2 = L_28;
	}

IL_00a5:
	{
		return;
	}
}
// System.Void UnityEngine.GUILayoutUtility::BeginWindow(System.Int32,UnityEngine.GUIStyle,UnityEngine.GUILayoutOption[])
extern TypeInfo* GUILayoutUtility_t6_170_il2cpp_TypeInfo_var;
extern TypeInfo* GUILayoutGroup_t6_169_il2cpp_TypeInfo_var;
extern "C" void GUILayoutUtility_BeginWindow_m6_1152 (Object_t * __this /* static, unused */, int32_t ___windowID, GUIStyle_t6_166 * ___style, GUILayoutOptionU5BU5D_t6_165* ___options, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GUILayoutUtility_t6_170_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(990);
		GUILayoutGroup_t6_169_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(994);
		s_Il2CppMethodIntialized = true;
	}
	LayoutCache_t6_168 * V_0 = {0};
	GUILayoutGroup_t6_169 * V_1 = {0};
	{
		int32_t L_0 = ___windowID;
		IL2CPP_RUNTIME_CLASS_INIT(GUILayoutUtility_t6_170_il2cpp_TypeInfo_var);
		LayoutCache_t6_168 * L_1 = GUILayoutUtility_SelectIDList_m6_1150(NULL /*static, unused*/, L_0, 1, /*hidden argument*/NULL);
		V_0 = L_1;
		Event_t6_154 * L_2 = Event_get_current_m6_989(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_2);
		int32_t L_3 = Event_get_type_m6_1003(L_2, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_3) == ((uint32_t)8))))
		{
			goto IL_00ab;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GUILayoutUtility_t6_170_il2cpp_TypeInfo_var);
		LayoutCache_t6_168 * L_4 = ((GUILayoutUtility_t6_170_StaticFields*)GUILayoutUtility_t6_170_il2cpp_TypeInfo_var->static_fields)->___current_2;
		LayoutCache_t6_168 * L_5 = V_0;
		GUILayoutGroup_t6_169 * L_6 = (GUILayoutGroup_t6_169 *)il2cpp_codegen_object_new (GUILayoutGroup_t6_169_il2cpp_TypeInfo_var);
		GUILayoutGroup__ctor_m6_1183(L_6, /*hidden argument*/NULL);
		GUILayoutGroup_t6_169 * L_7 = L_6;
		V_1 = L_7;
		NullCheck(L_5);
		L_5->___topLevel_0 = L_7;
		GUILayoutGroup_t6_169 * L_8 = V_1;
		NullCheck(L_4);
		L_4->___topLevel_0 = L_8;
		LayoutCache_t6_168 * L_9 = ((GUILayoutUtility_t6_170_StaticFields*)GUILayoutUtility_t6_170_il2cpp_TypeInfo_var->static_fields)->___current_2;
		NullCheck(L_9);
		GUILayoutGroup_t6_169 * L_10 = (L_9->___topLevel_0);
		GUIStyle_t6_166 * L_11 = ___style;
		NullCheck(L_10);
		GUILayoutEntry_set_style_m6_1174(L_10, L_11, /*hidden argument*/NULL);
		LayoutCache_t6_168 * L_12 = ((GUILayoutUtility_t6_170_StaticFields*)GUILayoutUtility_t6_170_il2cpp_TypeInfo_var->static_fields)->___current_2;
		NullCheck(L_12);
		GUILayoutGroup_t6_169 * L_13 = (L_12->___topLevel_0);
		int32_t L_14 = ___windowID;
		NullCheck(L_13);
		L_13->___windowID_16 = L_14;
		GUILayoutOptionU5BU5D_t6_165* L_15 = ___options;
		if (!L_15)
		{
			goto IL_0066;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GUILayoutUtility_t6_170_il2cpp_TypeInfo_var);
		LayoutCache_t6_168 * L_16 = ((GUILayoutUtility_t6_170_StaticFields*)GUILayoutUtility_t6_170_il2cpp_TypeInfo_var->static_fields)->___current_2;
		NullCheck(L_16);
		GUILayoutGroup_t6_169 * L_17 = (L_16->___topLevel_0);
		GUILayoutOptionU5BU5D_t6_165* L_18 = ___options;
		NullCheck(L_17);
		VirtActionInvoker1< GUILayoutOptionU5BU5D_t6_165* >::Invoke(10 /* System.Void UnityEngine.GUILayoutGroup::ApplyOptions(UnityEngine.GUILayoutOption[]) */, L_17, L_18);
	}

IL_0066:
	{
		IL2CPP_RUNTIME_CLASS_INIT(GUILayoutUtility_t6_170_il2cpp_TypeInfo_var);
		LayoutCache_t6_168 * L_19 = ((GUILayoutUtility_t6_170_StaticFields*)GUILayoutUtility_t6_170_il2cpp_TypeInfo_var->static_fields)->___current_2;
		NullCheck(L_19);
		GenericStack_t6_162 * L_20 = (L_19->___layoutGroups_1);
		NullCheck(L_20);
		VirtActionInvoker0::Invoke(11 /* System.Void System.Collections.Stack::Clear() */, L_20);
		LayoutCache_t6_168 * L_21 = ((GUILayoutUtility_t6_170_StaticFields*)GUILayoutUtility_t6_170_il2cpp_TypeInfo_var->static_fields)->___current_2;
		NullCheck(L_21);
		GenericStack_t6_162 * L_22 = (L_21->___layoutGroups_1);
		LayoutCache_t6_168 * L_23 = ((GUILayoutUtility_t6_170_StaticFields*)GUILayoutUtility_t6_170_il2cpp_TypeInfo_var->static_fields)->___current_2;
		NullCheck(L_23);
		GUILayoutGroup_t6_169 * L_24 = (L_23->___topLevel_0);
		NullCheck(L_22);
		VirtActionInvoker1< Object_t * >::Invoke(17 /* System.Void System.Collections.Stack::Push(System.Object) */, L_22, L_24);
		LayoutCache_t6_168 * L_25 = ((GUILayoutUtility_t6_170_StaticFields*)GUILayoutUtility_t6_170_il2cpp_TypeInfo_var->static_fields)->___current_2;
		LayoutCache_t6_168 * L_26 = V_0;
		GUILayoutGroup_t6_169 * L_27 = (GUILayoutGroup_t6_169 *)il2cpp_codegen_object_new (GUILayoutGroup_t6_169_il2cpp_TypeInfo_var);
		GUILayoutGroup__ctor_m6_1183(L_27, /*hidden argument*/NULL);
		GUILayoutGroup_t6_169 * L_28 = L_27;
		V_1 = L_28;
		NullCheck(L_26);
		L_26->___windows_2 = L_28;
		GUILayoutGroup_t6_169 * L_29 = V_1;
		NullCheck(L_25);
		L_25->___windows_2 = L_29;
		goto IL_00db;
	}

IL_00ab:
	{
		IL2CPP_RUNTIME_CLASS_INIT(GUILayoutUtility_t6_170_il2cpp_TypeInfo_var);
		LayoutCache_t6_168 * L_30 = ((GUILayoutUtility_t6_170_StaticFields*)GUILayoutUtility_t6_170_il2cpp_TypeInfo_var->static_fields)->___current_2;
		LayoutCache_t6_168 * L_31 = V_0;
		NullCheck(L_31);
		GUILayoutGroup_t6_169 * L_32 = (L_31->___topLevel_0);
		NullCheck(L_30);
		L_30->___topLevel_0 = L_32;
		LayoutCache_t6_168 * L_33 = ((GUILayoutUtility_t6_170_StaticFields*)GUILayoutUtility_t6_170_il2cpp_TypeInfo_var->static_fields)->___current_2;
		LayoutCache_t6_168 * L_34 = V_0;
		NullCheck(L_34);
		GenericStack_t6_162 * L_35 = (L_34->___layoutGroups_1);
		NullCheck(L_33);
		L_33->___layoutGroups_1 = L_35;
		LayoutCache_t6_168 * L_36 = ((GUILayoutUtility_t6_170_StaticFields*)GUILayoutUtility_t6_170_il2cpp_TypeInfo_var->static_fields)->___current_2;
		LayoutCache_t6_168 * L_37 = V_0;
		NullCheck(L_37);
		GUILayoutGroup_t6_169 * L_38 = (L_37->___windows_2);
		NullCheck(L_36);
		L_36->___windows_2 = L_38;
	}

IL_00db:
	{
		return;
	}
}
// System.Void UnityEngine.GUILayoutUtility::EndGroup(System.String)
extern "C" void GUILayoutUtility_EndGroup_m6_1153 (Object_t * __this /* static, unused */, String_t* ___groupName, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void UnityEngine.GUILayoutUtility::Layout()
extern TypeInfo* GUILayoutUtility_t6_170_il2cpp_TypeInfo_var;
extern TypeInfo* GUIUtility_t6_185_il2cpp_TypeInfo_var;
extern TypeInfo* Mathf_t6_58_il2cpp_TypeInfo_var;
extern "C" void GUILayoutUtility_Layout_m6_1154 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GUILayoutUtility_t6_170_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(990);
		GUIUtility_t6_185_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(984);
		Mathf_t6_58_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(940);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GUILayoutUtility_t6_170_il2cpp_TypeInfo_var);
		LayoutCache_t6_168 * L_0 = ((GUILayoutUtility_t6_170_StaticFields*)GUILayoutUtility_t6_170_il2cpp_TypeInfo_var->static_fields)->___current_2;
		NullCheck(L_0);
		GUILayoutGroup_t6_169 * L_1 = (L_0->___topLevel_0);
		NullCheck(L_1);
		int32_t L_2 = (L_1->___windowID_16);
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_00af;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GUILayoutUtility_t6_170_il2cpp_TypeInfo_var);
		LayoutCache_t6_168 * L_3 = ((GUILayoutUtility_t6_170_StaticFields*)GUILayoutUtility_t6_170_il2cpp_TypeInfo_var->static_fields)->___current_2;
		NullCheck(L_3);
		GUILayoutGroup_t6_169 * L_4 = (L_3->___topLevel_0);
		NullCheck(L_4);
		VirtActionInvoker0::Invoke(5 /* System.Void UnityEngine.GUILayoutGroup::CalcWidth() */, L_4);
		LayoutCache_t6_168 * L_5 = ((GUILayoutUtility_t6_170_StaticFields*)GUILayoutUtility_t6_170_il2cpp_TypeInfo_var->static_fields)->___current_2;
		NullCheck(L_5);
		GUILayoutGroup_t6_169 * L_6 = (L_5->___topLevel_0);
		int32_t L_7 = Screen_get_width_m6_121(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(GUIUtility_t6_185_il2cpp_TypeInfo_var);
		float L_8 = GUIUtility_get_pixelsPerPoint_m6_1366(NULL /*static, unused*/, /*hidden argument*/NULL);
		LayoutCache_t6_168 * L_9 = ((GUILayoutUtility_t6_170_StaticFields*)GUILayoutUtility_t6_170_il2cpp_TypeInfo_var->static_fields)->___current_2;
		NullCheck(L_9);
		GUILayoutGroup_t6_169 * L_10 = (L_9->___topLevel_0);
		NullCheck(L_10);
		float L_11 = (((GUILayoutEntry_t6_171 *)L_10)->___maxWidth_1);
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t6_58_il2cpp_TypeInfo_var);
		float L_12 = Mathf_Min_m6_381(NULL /*static, unused*/, ((float)((float)(((float)((float)L_7)))/(float)L_8)), L_11, /*hidden argument*/NULL);
		NullCheck(L_6);
		VirtActionInvoker2< float, float >::Invoke(7 /* System.Void UnityEngine.GUILayoutGroup::SetHorizontal(System.Single,System.Single) */, L_6, (0.0f), L_12);
		LayoutCache_t6_168 * L_13 = ((GUILayoutUtility_t6_170_StaticFields*)GUILayoutUtility_t6_170_il2cpp_TypeInfo_var->static_fields)->___current_2;
		NullCheck(L_13);
		GUILayoutGroup_t6_169 * L_14 = (L_13->___topLevel_0);
		NullCheck(L_14);
		VirtActionInvoker0::Invoke(6 /* System.Void UnityEngine.GUILayoutGroup::CalcHeight() */, L_14);
		LayoutCache_t6_168 * L_15 = ((GUILayoutUtility_t6_170_StaticFields*)GUILayoutUtility_t6_170_il2cpp_TypeInfo_var->static_fields)->___current_2;
		NullCheck(L_15);
		GUILayoutGroup_t6_169 * L_16 = (L_15->___topLevel_0);
		int32_t L_17 = Screen_get_height_m6_122(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_18 = GUIUtility_get_pixelsPerPoint_m6_1366(NULL /*static, unused*/, /*hidden argument*/NULL);
		LayoutCache_t6_168 * L_19 = ((GUILayoutUtility_t6_170_StaticFields*)GUILayoutUtility_t6_170_il2cpp_TypeInfo_var->static_fields)->___current_2;
		NullCheck(L_19);
		GUILayoutGroup_t6_169 * L_20 = (L_19->___topLevel_0);
		NullCheck(L_20);
		float L_21 = (((GUILayoutEntry_t6_171 *)L_20)->___maxHeight_3);
		float L_22 = Mathf_Min_m6_381(NULL /*static, unused*/, ((float)((float)(((float)((float)L_17)))/(float)L_18)), L_21, /*hidden argument*/NULL);
		NullCheck(L_16);
		VirtActionInvoker2< float, float >::Invoke(8 /* System.Void UnityEngine.GUILayoutGroup::SetVertical(System.Single,System.Single) */, L_16, (0.0f), L_22);
		LayoutCache_t6_168 * L_23 = ((GUILayoutUtility_t6_170_StaticFields*)GUILayoutUtility_t6_170_il2cpp_TypeInfo_var->static_fields)->___current_2;
		NullCheck(L_23);
		GUILayoutGroup_t6_169 * L_24 = (L_23->___windows_2);
		GUILayoutUtility_LayoutFreeGroup_m6_1156(NULL /*static, unused*/, L_24, /*hidden argument*/NULL);
		goto IL_00cd;
	}

IL_00af:
	{
		IL2CPP_RUNTIME_CLASS_INIT(GUILayoutUtility_t6_170_il2cpp_TypeInfo_var);
		LayoutCache_t6_168 * L_25 = ((GUILayoutUtility_t6_170_StaticFields*)GUILayoutUtility_t6_170_il2cpp_TypeInfo_var->static_fields)->___current_2;
		NullCheck(L_25);
		GUILayoutGroup_t6_169 * L_26 = (L_25->___topLevel_0);
		GUILayoutUtility_LayoutSingleGroup_m6_1157(NULL /*static, unused*/, L_26, /*hidden argument*/NULL);
		LayoutCache_t6_168 * L_27 = ((GUILayoutUtility_t6_170_StaticFields*)GUILayoutUtility_t6_170_il2cpp_TypeInfo_var->static_fields)->___current_2;
		NullCheck(L_27);
		GUILayoutGroup_t6_169 * L_28 = (L_27->___windows_2);
		GUILayoutUtility_LayoutFreeGroup_m6_1156(NULL /*static, unused*/, L_28, /*hidden argument*/NULL);
	}

IL_00cd:
	{
		return;
	}
}
// System.Void UnityEngine.GUILayoutUtility::LayoutFromEditorWindow()
extern TypeInfo* GUILayoutUtility_t6_170_il2cpp_TypeInfo_var;
extern TypeInfo* GUIUtility_t6_185_il2cpp_TypeInfo_var;
extern "C" void GUILayoutUtility_LayoutFromEditorWindow_m6_1155 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GUILayoutUtility_t6_170_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(990);
		GUIUtility_t6_185_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(984);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GUILayoutUtility_t6_170_il2cpp_TypeInfo_var);
		LayoutCache_t6_168 * L_0 = ((GUILayoutUtility_t6_170_StaticFields*)GUILayoutUtility_t6_170_il2cpp_TypeInfo_var->static_fields)->___current_2;
		NullCheck(L_0);
		GUILayoutGroup_t6_169 * L_1 = (L_0->___topLevel_0);
		NullCheck(L_1);
		VirtActionInvoker0::Invoke(5 /* System.Void UnityEngine.GUILayoutGroup::CalcWidth() */, L_1);
		LayoutCache_t6_168 * L_2 = ((GUILayoutUtility_t6_170_StaticFields*)GUILayoutUtility_t6_170_il2cpp_TypeInfo_var->static_fields)->___current_2;
		NullCheck(L_2);
		GUILayoutGroup_t6_169 * L_3 = (L_2->___topLevel_0);
		int32_t L_4 = Screen_get_width_m6_121(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(GUIUtility_t6_185_il2cpp_TypeInfo_var);
		float L_5 = GUIUtility_get_pixelsPerPoint_m6_1366(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_3);
		VirtActionInvoker2< float, float >::Invoke(7 /* System.Void UnityEngine.GUILayoutGroup::SetHorizontal(System.Single,System.Single) */, L_3, (0.0f), ((float)((float)(((float)((float)L_4)))/(float)L_5)));
		LayoutCache_t6_168 * L_6 = ((GUILayoutUtility_t6_170_StaticFields*)GUILayoutUtility_t6_170_il2cpp_TypeInfo_var->static_fields)->___current_2;
		NullCheck(L_6);
		GUILayoutGroup_t6_169 * L_7 = (L_6->___topLevel_0);
		NullCheck(L_7);
		VirtActionInvoker0::Invoke(6 /* System.Void UnityEngine.GUILayoutGroup::CalcHeight() */, L_7);
		LayoutCache_t6_168 * L_8 = ((GUILayoutUtility_t6_170_StaticFields*)GUILayoutUtility_t6_170_il2cpp_TypeInfo_var->static_fields)->___current_2;
		NullCheck(L_8);
		GUILayoutGroup_t6_169 * L_9 = (L_8->___topLevel_0);
		int32_t L_10 = Screen_get_height_m6_122(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_11 = GUIUtility_get_pixelsPerPoint_m6_1366(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_9);
		VirtActionInvoker2< float, float >::Invoke(8 /* System.Void UnityEngine.GUILayoutGroup::SetVertical(System.Single,System.Single) */, L_9, (0.0f), ((float)((float)(((float)((float)L_10)))/(float)L_11)));
		LayoutCache_t6_168 * L_12 = ((GUILayoutUtility_t6_170_StaticFields*)GUILayoutUtility_t6_170_il2cpp_TypeInfo_var->static_fields)->___current_2;
		NullCheck(L_12);
		GUILayoutGroup_t6_169 * L_13 = (L_12->___windows_2);
		GUILayoutUtility_LayoutFreeGroup_m6_1156(NULL /*static, unused*/, L_13, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.GUILayoutUtility::LayoutFreeGroup(UnityEngine.GUILayoutGroup)
extern TypeInfo* GUILayoutGroup_t6_169_il2cpp_TypeInfo_var;
extern TypeInfo* GUILayoutUtility_t6_170_il2cpp_TypeInfo_var;
extern TypeInfo* Enumerator_t1_927_il2cpp_TypeInfo_var;
extern TypeInfo* IDisposable_t1_835_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1_GetEnumerator_m1_5604_MethodInfo_var;
extern const MethodInfo* Enumerator_get_Current_m1_5605_MethodInfo_var;
extern const MethodInfo* Enumerator_MoveNext_m1_5606_MethodInfo_var;
extern "C" void GUILayoutUtility_LayoutFreeGroup_m6_1156 (Object_t * __this /* static, unused */, GUILayoutGroup_t6_169 * ___toplevel, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GUILayoutGroup_t6_169_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(994);
		GUILayoutUtility_t6_170_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(990);
		Enumerator_t1_927_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1001);
		IDisposable_t1_835_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(142);
		List_1_GetEnumerator_m1_5604_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483812);
		Enumerator_get_Current_m1_5605_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483813);
		Enumerator_MoveNext_m1_5606_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483814);
		s_Il2CppMethodIntialized = true;
	}
	GUILayoutGroup_t6_169 * V_0 = {0};
	Enumerator_t1_927  V_1 = {0};
	Exception_t1_33 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1_33 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		GUILayoutGroup_t6_169 * L_0 = ___toplevel;
		NullCheck(L_0);
		List_1_t1_917 * L_1 = (L_0->___entries_10);
		NullCheck(L_1);
		Enumerator_t1_927  L_2 = List_1_GetEnumerator_m1_5604(L_1, /*hidden argument*/List_1_GetEnumerator_m1_5604_MethodInfo_var);
		V_1 = L_2;
	}

IL_000c:
	try
	{ // begin try (depth: 1)
		{
			goto IL_0024;
		}

IL_0011:
		{
			GUILayoutEntry_t6_171 * L_3 = Enumerator_get_Current_m1_5605((&V_1), /*hidden argument*/Enumerator_get_Current_m1_5605_MethodInfo_var);
			V_0 = ((GUILayoutGroup_t6_169 *)CastclassClass(L_3, GUILayoutGroup_t6_169_il2cpp_TypeInfo_var));
			GUILayoutGroup_t6_169 * L_4 = V_0;
			IL2CPP_RUNTIME_CLASS_INIT(GUILayoutUtility_t6_170_il2cpp_TypeInfo_var);
			GUILayoutUtility_LayoutSingleGroup_m6_1157(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
		}

IL_0024:
		{
			bool L_5 = Enumerator_MoveNext_m1_5606((&V_1), /*hidden argument*/Enumerator_MoveNext_m1_5606_MethodInfo_var);
			if (L_5)
			{
				goto IL_0011;
			}
		}

IL_0030:
		{
			IL2CPP_LEAVE(0x41, FINALLY_0035);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1_33 *)e.ex;
		goto FINALLY_0035;
	}

FINALLY_0035:
	{ // begin finally (depth: 1)
		Enumerator_t1_927  L_6 = V_1;
		Enumerator_t1_927  L_7 = L_6;
		Object_t * L_8 = Box(Enumerator_t1_927_il2cpp_TypeInfo_var, &L_7);
		NullCheck(L_8);
		InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1_835_il2cpp_TypeInfo_var, L_8);
		IL2CPP_END_FINALLY(53)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(53)
	{
		IL2CPP_JUMP_TBL(0x41, IL_0041)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1_33 *)
	}

IL_0041:
	{
		GUILayoutGroup_t6_169 * L_9 = ___toplevel;
		NullCheck(L_9);
		GUILayoutGroup_ResetCursor_m6_1187(L_9, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.GUILayoutUtility::LayoutSingleGroup(UnityEngine.GUILayoutGroup)
extern TypeInfo* Mathf_t6_58_il2cpp_TypeInfo_var;
extern TypeInfo* GUILayoutUtility_t6_170_il2cpp_TypeInfo_var;
extern "C" void GUILayoutUtility_LayoutSingleGroup_m6_1157 (Object_t * __this /* static, unused */, GUILayoutGroup_t6_169 * ___i, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Mathf_t6_58_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(940);
		GUILayoutUtility_t6_170_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(990);
		s_Il2CppMethodIntialized = true;
	}
	float V_0 = 0.0f;
	float V_1 = 0.0f;
	float V_2 = 0.0f;
	float V_3 = 0.0f;
	Rect_t6_52  V_4 = {0};
	{
		GUILayoutGroup_t6_169 * L_0 = ___i;
		NullCheck(L_0);
		bool L_1 = (L_0->___isWindow_15);
		if (L_1)
		{
			goto IL_0074;
		}
	}
	{
		GUILayoutGroup_t6_169 * L_2 = ___i;
		NullCheck(L_2);
		float L_3 = (((GUILayoutEntry_t6_171 *)L_2)->___minWidth_0);
		V_0 = L_3;
		GUILayoutGroup_t6_169 * L_4 = ___i;
		NullCheck(L_4);
		float L_5 = (((GUILayoutEntry_t6_171 *)L_4)->___maxWidth_1);
		V_1 = L_5;
		GUILayoutGroup_t6_169 * L_6 = ___i;
		NullCheck(L_6);
		VirtActionInvoker0::Invoke(5 /* System.Void UnityEngine.GUILayoutGroup::CalcWidth() */, L_6);
		GUILayoutGroup_t6_169 * L_7 = ___i;
		GUILayoutGroup_t6_169 * L_8 = ___i;
		NullCheck(L_8);
		Rect_t6_52 * L_9 = &(((GUILayoutEntry_t6_171 *)L_8)->___rect_4);
		float L_10 = Rect_get_x_m6_273(L_9, /*hidden argument*/NULL);
		GUILayoutGroup_t6_169 * L_11 = ___i;
		NullCheck(L_11);
		float L_12 = (((GUILayoutEntry_t6_171 *)L_11)->___maxWidth_1);
		float L_13 = V_0;
		float L_14 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t6_58_il2cpp_TypeInfo_var);
		float L_15 = Mathf_Clamp_m6_389(NULL /*static, unused*/, L_12, L_13, L_14, /*hidden argument*/NULL);
		NullCheck(L_7);
		VirtActionInvoker2< float, float >::Invoke(7 /* System.Void UnityEngine.GUILayoutGroup::SetHorizontal(System.Single,System.Single) */, L_7, L_10, L_15);
		GUILayoutGroup_t6_169 * L_16 = ___i;
		NullCheck(L_16);
		float L_17 = (((GUILayoutEntry_t6_171 *)L_16)->___minHeight_2);
		V_2 = L_17;
		GUILayoutGroup_t6_169 * L_18 = ___i;
		NullCheck(L_18);
		float L_19 = (((GUILayoutEntry_t6_171 *)L_18)->___maxHeight_3);
		V_3 = L_19;
		GUILayoutGroup_t6_169 * L_20 = ___i;
		NullCheck(L_20);
		VirtActionInvoker0::Invoke(6 /* System.Void UnityEngine.GUILayoutGroup::CalcHeight() */, L_20);
		GUILayoutGroup_t6_169 * L_21 = ___i;
		GUILayoutGroup_t6_169 * L_22 = ___i;
		NullCheck(L_22);
		Rect_t6_52 * L_23 = &(((GUILayoutEntry_t6_171 *)L_22)->___rect_4);
		float L_24 = Rect_get_y_m6_275(L_23, /*hidden argument*/NULL);
		GUILayoutGroup_t6_169 * L_25 = ___i;
		NullCheck(L_25);
		float L_26 = (((GUILayoutEntry_t6_171 *)L_25)->___maxHeight_3);
		float L_27 = V_2;
		float L_28 = V_3;
		float L_29 = Mathf_Clamp_m6_389(NULL /*static, unused*/, L_26, L_27, L_28, /*hidden argument*/NULL);
		NullCheck(L_21);
		VirtActionInvoker2< float, float >::Invoke(8 /* System.Void UnityEngine.GUILayoutGroup::SetVertical(System.Single,System.Single) */, L_21, L_24, L_29);
		goto IL_00e8;
	}

IL_0074:
	{
		GUILayoutGroup_t6_169 * L_30 = ___i;
		NullCheck(L_30);
		VirtActionInvoker0::Invoke(5 /* System.Void UnityEngine.GUILayoutGroup::CalcWidth() */, L_30);
		GUILayoutGroup_t6_169 * L_31 = ___i;
		NullCheck(L_31);
		int32_t L_32 = (L_31->___windowID_16);
		IL2CPP_RUNTIME_CLASS_INIT(GUILayoutUtility_t6_170_il2cpp_TypeInfo_var);
		Rect_t6_52  L_33 = GUILayoutUtility_Internal_GetWindowRect_m6_1167(NULL /*static, unused*/, L_32, /*hidden argument*/NULL);
		V_4 = L_33;
		GUILayoutGroup_t6_169 * L_34 = ___i;
		float L_35 = Rect_get_x_m6_273((&V_4), /*hidden argument*/NULL);
		float L_36 = Rect_get_width_m6_277((&V_4), /*hidden argument*/NULL);
		GUILayoutGroup_t6_169 * L_37 = ___i;
		NullCheck(L_37);
		float L_38 = (((GUILayoutEntry_t6_171 *)L_37)->___minWidth_0);
		GUILayoutGroup_t6_169 * L_39 = ___i;
		NullCheck(L_39);
		float L_40 = (((GUILayoutEntry_t6_171 *)L_39)->___maxWidth_1);
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t6_58_il2cpp_TypeInfo_var);
		float L_41 = Mathf_Clamp_m6_389(NULL /*static, unused*/, L_36, L_38, L_40, /*hidden argument*/NULL);
		NullCheck(L_34);
		VirtActionInvoker2< float, float >::Invoke(7 /* System.Void UnityEngine.GUILayoutGroup::SetHorizontal(System.Single,System.Single) */, L_34, L_35, L_41);
		GUILayoutGroup_t6_169 * L_42 = ___i;
		NullCheck(L_42);
		VirtActionInvoker0::Invoke(6 /* System.Void UnityEngine.GUILayoutGroup::CalcHeight() */, L_42);
		GUILayoutGroup_t6_169 * L_43 = ___i;
		float L_44 = Rect_get_y_m6_275((&V_4), /*hidden argument*/NULL);
		float L_45 = Rect_get_height_m6_279((&V_4), /*hidden argument*/NULL);
		GUILayoutGroup_t6_169 * L_46 = ___i;
		NullCheck(L_46);
		float L_47 = (((GUILayoutEntry_t6_171 *)L_46)->___minHeight_2);
		GUILayoutGroup_t6_169 * L_48 = ___i;
		NullCheck(L_48);
		float L_49 = (((GUILayoutEntry_t6_171 *)L_48)->___maxHeight_3);
		float L_50 = Mathf_Clamp_m6_389(NULL /*static, unused*/, L_45, L_47, L_49, /*hidden argument*/NULL);
		NullCheck(L_43);
		VirtActionInvoker2< float, float >::Invoke(8 /* System.Void UnityEngine.GUILayoutGroup::SetVertical(System.Single,System.Single) */, L_43, L_44, L_50);
		GUILayoutGroup_t6_169 * L_51 = ___i;
		NullCheck(L_51);
		int32_t L_52 = (L_51->___windowID_16);
		GUILayoutGroup_t6_169 * L_53 = ___i;
		NullCheck(L_53);
		Rect_t6_52  L_54 = (((GUILayoutEntry_t6_171 *)L_53)->___rect_4);
		GUILayoutUtility_Internal_MoveWindow_m6_1168(NULL /*static, unused*/, L_52, L_54, /*hidden argument*/NULL);
	}

IL_00e8:
	{
		return;
	}
}
// UnityEngine.GUILayoutGroup UnityEngine.GUILayoutUtility::CreateGUILayoutGroupInstanceOfType(System.Type)
extern const Il2CppType* GUILayoutGroup_t6_169_0_0_0_var;
extern TypeInfo* Type_t_il2cpp_TypeInfo_var;
extern TypeInfo* ArgumentException_t1_646_il2cpp_TypeInfo_var;
extern TypeInfo* GUILayoutGroup_t6_169_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2790;
extern "C" GUILayoutGroup_t6_169 * GUILayoutUtility_CreateGUILayoutGroupInstanceOfType_m6_1158 (Object_t * __this /* static, unused */, Type_t * ___LayoutType, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GUILayoutGroup_t6_169_0_0_0_var = il2cpp_codegen_type_from_index(994);
		Type_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3);
		ArgumentException_t1_646_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4);
		GUILayoutGroup_t6_169_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(994);
		_stringLiteral2790 = il2cpp_codegen_string_literal_from_index(2790);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m1_893(NULL /*static, unused*/, LoadTypeToken(GUILayoutGroup_t6_169_0_0_0_var), /*hidden argument*/NULL);
		Type_t * L_1 = ___LayoutType;
		NullCheck(L_0);
		bool L_2 = (bool)VirtFuncInvoker1< bool, Type_t * >::Invoke(40 /* System.Boolean System.Type::IsAssignableFrom(System.Type) */, L_0, L_1);
		if (L_2)
		{
			goto IL_0020;
		}
	}
	{
		ArgumentException_t1_646 * L_3 = (ArgumentException_t1_646 *)il2cpp_codegen_object_new (ArgumentException_t1_646_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m1_4605(L_3, _stringLiteral2790, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_3);
	}

IL_0020:
	{
		Type_t * L_4 = ___LayoutType;
		Object_t * L_5 = Activator_CreateInstance_m1_4575(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
		return ((GUILayoutGroup_t6_169 *)CastclassClass(L_5, GUILayoutGroup_t6_169_il2cpp_TypeInfo_var));
	}
}
// UnityEngine.GUILayoutGroup UnityEngine.GUILayoutUtility::BeginLayoutGroup(UnityEngine.GUIStyle,UnityEngine.GUILayoutOption[],System.Type)
extern TypeInfo* GUILayoutUtility_t6_170_il2cpp_TypeInfo_var;
extern TypeInfo* GUILayoutGroup_t6_169_il2cpp_TypeInfo_var;
extern TypeInfo* EventType_t6_156_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* ArgumentException_t1_646_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2791;
extern "C" GUILayoutGroup_t6_169 * GUILayoutUtility_BeginLayoutGroup_m6_1159 (Object_t * __this /* static, unused */, GUIStyle_t6_166 * ___style, GUILayoutOptionU5BU5D_t6_165* ___options, Type_t * ___layoutType, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GUILayoutUtility_t6_170_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(990);
		GUILayoutGroup_t6_169_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(994);
		EventType_t6_156_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(980);
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(11);
		ArgumentException_t1_646_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4);
		_stringLiteral2791 = il2cpp_codegen_string_literal_from_index(2791);
		s_Il2CppMethodIntialized = true;
	}
	GUILayoutGroup_t6_169 * V_0 = {0};
	int32_t V_1 = {0};
	{
		Event_t6_154 * L_0 = Event_get_current_m6_989(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_0);
		int32_t L_1 = Event_get_type_m6_1003(L_0, /*hidden argument*/NULL);
		V_1 = L_1;
		int32_t L_2 = V_1;
		if ((((int32_t)L_2) == ((int32_t)8)))
		{
			goto IL_001f;
		}
	}
	{
		int32_t L_3 = V_1;
		if ((((int32_t)L_3) == ((int32_t)((int32_t)12))))
		{
			goto IL_001f;
		}
	}
	{
		goto IL_004f;
	}

IL_001f:
	{
		Type_t * L_4 = ___layoutType;
		IL2CPP_RUNTIME_CLASS_INIT(GUILayoutUtility_t6_170_il2cpp_TypeInfo_var);
		GUILayoutGroup_t6_169 * L_5 = GUILayoutUtility_CreateGUILayoutGroupInstanceOfType_m6_1158(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
		V_0 = L_5;
		GUILayoutGroup_t6_169 * L_6 = V_0;
		GUIStyle_t6_166 * L_7 = ___style;
		NullCheck(L_6);
		GUILayoutEntry_set_style_m6_1174(L_6, L_7, /*hidden argument*/NULL);
		GUILayoutOptionU5BU5D_t6_165* L_8 = ___options;
		if (!L_8)
		{
			goto IL_003a;
		}
	}
	{
		GUILayoutGroup_t6_169 * L_9 = V_0;
		GUILayoutOptionU5BU5D_t6_165* L_10 = ___options;
		NullCheck(L_9);
		VirtActionInvoker1< GUILayoutOptionU5BU5D_t6_165* >::Invoke(10 /* System.Void UnityEngine.GUILayoutGroup::ApplyOptions(UnityEngine.GUILayoutOption[]) */, L_9, L_10);
	}

IL_003a:
	{
		IL2CPP_RUNTIME_CLASS_INIT(GUILayoutUtility_t6_170_il2cpp_TypeInfo_var);
		LayoutCache_t6_168 * L_11 = ((GUILayoutUtility_t6_170_StaticFields*)GUILayoutUtility_t6_170_il2cpp_TypeInfo_var->static_fields)->___current_2;
		NullCheck(L_11);
		GUILayoutGroup_t6_169 * L_12 = (L_11->___topLevel_0);
		GUILayoutGroup_t6_169 * L_13 = V_0;
		NullCheck(L_12);
		GUILayoutGroup_Add_m6_1189(L_12, L_13, /*hidden argument*/NULL);
		goto IL_0094;
	}

IL_004f:
	{
		IL2CPP_RUNTIME_CLASS_INIT(GUILayoutUtility_t6_170_il2cpp_TypeInfo_var);
		LayoutCache_t6_168 * L_14 = ((GUILayoutUtility_t6_170_StaticFields*)GUILayoutUtility_t6_170_il2cpp_TypeInfo_var->static_fields)->___current_2;
		NullCheck(L_14);
		GUILayoutGroup_t6_169 * L_15 = (L_14->___topLevel_0);
		NullCheck(L_15);
		GUILayoutEntry_t6_171 * L_16 = GUILayoutGroup_GetNext_m6_1188(L_15, /*hidden argument*/NULL);
		V_0 = ((GUILayoutGroup_t6_169 *)IsInstClass(L_16, GUILayoutGroup_t6_169_il2cpp_TypeInfo_var));
		GUILayoutGroup_t6_169 * L_17 = V_0;
		if (L_17)
		{
			goto IL_0089;
		}
	}
	{
		Event_t6_154 * L_18 = Event_get_current_m6_989(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_18);
		int32_t L_19 = Event_get_type_m6_1003(L_18, /*hidden argument*/NULL);
		int32_t L_20 = L_19;
		Object_t * L_21 = Box(EventType_t6_156_il2cpp_TypeInfo_var, &L_20);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_22 = String_Concat_m1_416(NULL /*static, unused*/, _stringLiteral2791, L_21, /*hidden argument*/NULL);
		ArgumentException_t1_646 * L_23 = (ArgumentException_t1_646 *)il2cpp_codegen_object_new (ArgumentException_t1_646_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m1_4605(L_23, L_22, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_23);
	}

IL_0089:
	{
		GUILayoutGroup_t6_169 * L_24 = V_0;
		NullCheck(L_24);
		GUILayoutGroup_ResetCursor_m6_1187(L_24, /*hidden argument*/NULL);
		goto IL_0094;
	}

IL_0094:
	{
		IL2CPP_RUNTIME_CLASS_INIT(GUILayoutUtility_t6_170_il2cpp_TypeInfo_var);
		LayoutCache_t6_168 * L_25 = ((GUILayoutUtility_t6_170_StaticFields*)GUILayoutUtility_t6_170_il2cpp_TypeInfo_var->static_fields)->___current_2;
		NullCheck(L_25);
		GenericStack_t6_162 * L_26 = (L_25->___layoutGroups_1);
		GUILayoutGroup_t6_169 * L_27 = V_0;
		NullCheck(L_26);
		VirtActionInvoker1< Object_t * >::Invoke(17 /* System.Void System.Collections.Stack::Push(System.Object) */, L_26, L_27);
		LayoutCache_t6_168 * L_28 = ((GUILayoutUtility_t6_170_StaticFields*)GUILayoutUtility_t6_170_il2cpp_TypeInfo_var->static_fields)->___current_2;
		GUILayoutGroup_t6_169 * L_29 = V_0;
		NullCheck(L_28);
		L_28->___topLevel_0 = L_29;
		GUILayoutGroup_t6_169 * L_30 = V_0;
		return L_30;
	}
}
// System.Void UnityEngine.GUILayoutUtility::EndLayoutGroup()
extern TypeInfo* GUILayoutUtility_t6_170_il2cpp_TypeInfo_var;
extern TypeInfo* GUILayoutGroup_t6_169_il2cpp_TypeInfo_var;
extern "C" void GUILayoutUtility_EndLayoutGroup_m6_1160 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GUILayoutUtility_t6_170_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(990);
		GUILayoutGroup_t6_169_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(994);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = {0};
	{
		Event_t6_154 * L_0 = Event_get_current_m6_989(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_0);
		int32_t L_1 = Event_get_type_m6_1003(L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		IL2CPP_RUNTIME_CLASS_INIT(GUILayoutUtility_t6_170_il2cpp_TypeInfo_var);
		LayoutCache_t6_168 * L_2 = ((GUILayoutUtility_t6_170_StaticFields*)GUILayoutUtility_t6_170_il2cpp_TypeInfo_var->static_fields)->___current_2;
		NullCheck(L_2);
		GenericStack_t6_162 * L_3 = (L_2->___layoutGroups_1);
		NullCheck(L_3);
		VirtFuncInvoker0< Object_t * >::Invoke(16 /* System.Object System.Collections.Stack::Pop() */, L_3);
		LayoutCache_t6_168 * L_4 = ((GUILayoutUtility_t6_170_StaticFields*)GUILayoutUtility_t6_170_il2cpp_TypeInfo_var->static_fields)->___current_2;
		LayoutCache_t6_168 * L_5 = ((GUILayoutUtility_t6_170_StaticFields*)GUILayoutUtility_t6_170_il2cpp_TypeInfo_var->static_fields)->___current_2;
		NullCheck(L_5);
		GenericStack_t6_162 * L_6 = (L_5->___layoutGroups_1);
		NullCheck(L_6);
		Object_t * L_7 = (Object_t *)VirtFuncInvoker0< Object_t * >::Invoke(15 /* System.Object System.Collections.Stack::Peek() */, L_6);
		NullCheck(L_4);
		L_4->___topLevel_0 = ((GUILayoutGroup_t6_169 *)CastclassClass(L_7, GUILayoutGroup_t6_169_il2cpp_TypeInfo_var));
		return;
	}
}
// UnityEngine.GUILayoutGroup UnityEngine.GUILayoutUtility::BeginLayoutArea(UnityEngine.GUIStyle,System.Type)
extern TypeInfo* GUILayoutUtility_t6_170_il2cpp_TypeInfo_var;
extern TypeInfo* GUILayoutGroup_t6_169_il2cpp_TypeInfo_var;
extern TypeInfo* EventType_t6_156_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* ArgumentException_t1_646_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2791;
extern "C" GUILayoutGroup_t6_169 * GUILayoutUtility_BeginLayoutArea_m6_1161 (Object_t * __this /* static, unused */, GUIStyle_t6_166 * ___style, Type_t * ___layoutType, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GUILayoutUtility_t6_170_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(990);
		GUILayoutGroup_t6_169_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(994);
		EventType_t6_156_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(980);
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(11);
		ArgumentException_t1_646_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4);
		_stringLiteral2791 = il2cpp_codegen_string_literal_from_index(2791);
		s_Il2CppMethodIntialized = true;
	}
	GUILayoutGroup_t6_169 * V_0 = {0};
	int32_t V_1 = {0};
	{
		Event_t6_154 * L_0 = Event_get_current_m6_989(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_0);
		int32_t L_1 = Event_get_type_m6_1003(L_0, /*hidden argument*/NULL);
		V_1 = L_1;
		int32_t L_2 = V_1;
		if ((((int32_t)L_2) == ((int32_t)8)))
		{
			goto IL_001f;
		}
	}
	{
		int32_t L_3 = V_1;
		if ((((int32_t)L_3) == ((int32_t)((int32_t)12))))
		{
			goto IL_001f;
		}
	}
	{
		goto IL_0042;
	}

IL_001f:
	{
		Type_t * L_4 = ___layoutType;
		IL2CPP_RUNTIME_CLASS_INIT(GUILayoutUtility_t6_170_il2cpp_TypeInfo_var);
		GUILayoutGroup_t6_169 * L_5 = GUILayoutUtility_CreateGUILayoutGroupInstanceOfType_m6_1158(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
		V_0 = L_5;
		GUILayoutGroup_t6_169 * L_6 = V_0;
		GUIStyle_t6_166 * L_7 = ___style;
		NullCheck(L_6);
		GUILayoutEntry_set_style_m6_1174(L_6, L_7, /*hidden argument*/NULL);
		LayoutCache_t6_168 * L_8 = ((GUILayoutUtility_t6_170_StaticFields*)GUILayoutUtility_t6_170_il2cpp_TypeInfo_var->static_fields)->___current_2;
		NullCheck(L_8);
		GUILayoutGroup_t6_169 * L_9 = (L_8->___windows_2);
		GUILayoutGroup_t6_169 * L_10 = V_0;
		NullCheck(L_9);
		GUILayoutGroup_Add_m6_1189(L_9, L_10, /*hidden argument*/NULL);
		goto IL_0087;
	}

IL_0042:
	{
		IL2CPP_RUNTIME_CLASS_INIT(GUILayoutUtility_t6_170_il2cpp_TypeInfo_var);
		LayoutCache_t6_168 * L_11 = ((GUILayoutUtility_t6_170_StaticFields*)GUILayoutUtility_t6_170_il2cpp_TypeInfo_var->static_fields)->___current_2;
		NullCheck(L_11);
		GUILayoutGroup_t6_169 * L_12 = (L_11->___windows_2);
		NullCheck(L_12);
		GUILayoutEntry_t6_171 * L_13 = GUILayoutGroup_GetNext_m6_1188(L_12, /*hidden argument*/NULL);
		V_0 = ((GUILayoutGroup_t6_169 *)IsInstClass(L_13, GUILayoutGroup_t6_169_il2cpp_TypeInfo_var));
		GUILayoutGroup_t6_169 * L_14 = V_0;
		if (L_14)
		{
			goto IL_007c;
		}
	}
	{
		Event_t6_154 * L_15 = Event_get_current_m6_989(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_15);
		int32_t L_16 = Event_get_type_m6_1003(L_15, /*hidden argument*/NULL);
		int32_t L_17 = L_16;
		Object_t * L_18 = Box(EventType_t6_156_il2cpp_TypeInfo_var, &L_17);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_19 = String_Concat_m1_416(NULL /*static, unused*/, _stringLiteral2791, L_18, /*hidden argument*/NULL);
		ArgumentException_t1_646 * L_20 = (ArgumentException_t1_646 *)il2cpp_codegen_object_new (ArgumentException_t1_646_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m1_4605(L_20, L_19, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_20);
	}

IL_007c:
	{
		GUILayoutGroup_t6_169 * L_21 = V_0;
		NullCheck(L_21);
		GUILayoutGroup_ResetCursor_m6_1187(L_21, /*hidden argument*/NULL);
		goto IL_0087;
	}

IL_0087:
	{
		IL2CPP_RUNTIME_CLASS_INIT(GUILayoutUtility_t6_170_il2cpp_TypeInfo_var);
		LayoutCache_t6_168 * L_22 = ((GUILayoutUtility_t6_170_StaticFields*)GUILayoutUtility_t6_170_il2cpp_TypeInfo_var->static_fields)->___current_2;
		NullCheck(L_22);
		GenericStack_t6_162 * L_23 = (L_22->___layoutGroups_1);
		GUILayoutGroup_t6_169 * L_24 = V_0;
		NullCheck(L_23);
		VirtActionInvoker1< Object_t * >::Invoke(17 /* System.Void System.Collections.Stack::Push(System.Object) */, L_23, L_24);
		LayoutCache_t6_168 * L_25 = ((GUILayoutUtility_t6_170_StaticFields*)GUILayoutUtility_t6_170_il2cpp_TypeInfo_var->static_fields)->___current_2;
		GUILayoutGroup_t6_169 * L_26 = V_0;
		NullCheck(L_25);
		L_25->___topLevel_0 = L_26;
		GUILayoutGroup_t6_169 * L_27 = V_0;
		return L_27;
	}
}
// UnityEngine.Rect UnityEngine.GUILayoutUtility::GetRect(UnityEngine.GUIContent,UnityEngine.GUIStyle,UnityEngine.GUILayoutOption[])
extern TypeInfo* GUILayoutUtility_t6_170_il2cpp_TypeInfo_var;
extern "C" Rect_t6_52  GUILayoutUtility_GetRect_m6_1162 (Object_t * __this /* static, unused */, GUIContent_t6_163 * ___content, GUIStyle_t6_166 * ___style, GUILayoutOptionU5BU5D_t6_165* ___options, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GUILayoutUtility_t6_170_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(990);
		s_Il2CppMethodIntialized = true;
	}
	{
		GUIContent_t6_163 * L_0 = ___content;
		GUIStyle_t6_166 * L_1 = ___style;
		GUILayoutOptionU5BU5D_t6_165* L_2 = ___options;
		IL2CPP_RUNTIME_CLASS_INIT(GUILayoutUtility_t6_170_il2cpp_TypeInfo_var);
		Rect_t6_52  L_3 = GUILayoutUtility_DoGetRect_m6_1163(NULL /*static, unused*/, L_0, L_1, L_2, /*hidden argument*/NULL);
		return L_3;
	}
}
// UnityEngine.Rect UnityEngine.GUILayoutUtility::DoGetRect(UnityEngine.GUIContent,UnityEngine.GUIStyle,UnityEngine.GUILayoutOption[])
extern TypeInfo* GUIUtility_t6_185_il2cpp_TypeInfo_var;
extern TypeInfo* GUILayoutUtility_t6_170_il2cpp_TypeInfo_var;
extern TypeInfo* GUIWordWrapSizer_t6_174_il2cpp_TypeInfo_var;
extern TypeInfo* GUILayoutEntry_t6_171_il2cpp_TypeInfo_var;
extern "C" Rect_t6_52  GUILayoutUtility_DoGetRect_m6_1163 (Object_t * __this /* static, unused */, GUIContent_t6_163 * ___content, GUIStyle_t6_166 * ___style, GUILayoutOptionU5BU5D_t6_165* ___options, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GUIUtility_t6_185_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(984);
		GUILayoutUtility_t6_170_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(990);
		GUIWordWrapSizer_t6_174_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1002);
		GUILayoutEntry_t6_171_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1000);
		s_Il2CppMethodIntialized = true;
	}
	Vector2_t6_48  V_0 = {0};
	int32_t V_1 = {0};
	{
		IL2CPP_RUNTIME_CLASS_INIT(GUIUtility_t6_185_il2cpp_TypeInfo_var);
		GUIUtility_CheckOnGUI_m6_1376(NULL /*static, unused*/, /*hidden argument*/NULL);
		Event_t6_154 * L_0 = Event_get_current_m6_989(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_0);
		int32_t L_1 = Event_get_type_m6_1003(L_0, /*hidden argument*/NULL);
		V_1 = L_1;
		int32_t L_2 = V_1;
		if ((((int32_t)L_2) == ((int32_t)8)))
		{
			goto IL_0024;
		}
	}
	{
		int32_t L_3 = V_1;
		if ((((int32_t)L_3) == ((int32_t)((int32_t)12))))
		{
			goto IL_008b;
		}
	}
	{
		goto IL_0091;
	}

IL_0024:
	{
		GUIStyle_t6_166 * L_4 = ___style;
		NullCheck(L_4);
		bool L_5 = GUIStyle_get_isHeightDependantOnWidth_m6_1321(L_4, /*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_004b;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GUILayoutUtility_t6_170_il2cpp_TypeInfo_var);
		LayoutCache_t6_168 * L_6 = ((GUILayoutUtility_t6_170_StaticFields*)GUILayoutUtility_t6_170_il2cpp_TypeInfo_var->static_fields)->___current_2;
		NullCheck(L_6);
		GUILayoutGroup_t6_169 * L_7 = (L_6->___topLevel_0);
		GUIStyle_t6_166 * L_8 = ___style;
		GUIContent_t6_163 * L_9 = ___content;
		GUILayoutOptionU5BU5D_t6_165* L_10 = ___options;
		GUIWordWrapSizer_t6_174 * L_11 = (GUIWordWrapSizer_t6_174 *)il2cpp_codegen_object_new (GUIWordWrapSizer_t6_174_il2cpp_TypeInfo_var);
		GUIWordWrapSizer__ctor_m6_1200(L_11, L_8, L_9, L_10, /*hidden argument*/NULL);
		NullCheck(L_7);
		GUILayoutGroup_Add_m6_1189(L_7, L_11, /*hidden argument*/NULL);
		goto IL_0085;
	}

IL_004b:
	{
		GUIStyle_t6_166 * L_12 = ___style;
		GUIContent_t6_163 * L_13 = ___content;
		NullCheck(L_12);
		Vector2_t6_48  L_14 = GUIStyle_CalcSize_m6_1319(L_12, L_13, /*hidden argument*/NULL);
		V_0 = L_14;
		IL2CPP_RUNTIME_CLASS_INIT(GUILayoutUtility_t6_170_il2cpp_TypeInfo_var);
		LayoutCache_t6_168 * L_15 = ((GUILayoutUtility_t6_170_StaticFields*)GUILayoutUtility_t6_170_il2cpp_TypeInfo_var->static_fields)->___current_2;
		NullCheck(L_15);
		GUILayoutGroup_t6_169 * L_16 = (L_15->___topLevel_0);
		float L_17 = ((&V_0)->___x_1);
		float L_18 = ((&V_0)->___x_1);
		float L_19 = ((&V_0)->___y_2);
		float L_20 = ((&V_0)->___y_2);
		GUIStyle_t6_166 * L_21 = ___style;
		GUILayoutOptionU5BU5D_t6_165* L_22 = ___options;
		GUILayoutEntry_t6_171 * L_23 = (GUILayoutEntry_t6_171 *)il2cpp_codegen_object_new (GUILayoutEntry_t6_171_il2cpp_TypeInfo_var);
		GUILayoutEntry__ctor_m6_1171(L_23, L_17, L_18, L_19, L_20, L_21, L_22, /*hidden argument*/NULL);
		NullCheck(L_16);
		GUILayoutGroup_Add_m6_1189(L_16, L_23, /*hidden argument*/NULL);
	}

IL_0085:
	{
		IL2CPP_RUNTIME_CLASS_INIT(GUILayoutUtility_t6_170_il2cpp_TypeInfo_var);
		Rect_t6_52  L_24 = ((GUILayoutUtility_t6_170_StaticFields*)GUILayoutUtility_t6_170_il2cpp_TypeInfo_var->static_fields)->___kDummyRect_3;
		return L_24;
	}

IL_008b:
	{
		IL2CPP_RUNTIME_CLASS_INIT(GUILayoutUtility_t6_170_il2cpp_TypeInfo_var);
		Rect_t6_52  L_25 = ((GUILayoutUtility_t6_170_StaticFields*)GUILayoutUtility_t6_170_il2cpp_TypeInfo_var->static_fields)->___kDummyRect_3;
		return L_25;
	}

IL_0091:
	{
		IL2CPP_RUNTIME_CLASS_INIT(GUILayoutUtility_t6_170_il2cpp_TypeInfo_var);
		LayoutCache_t6_168 * L_26 = ((GUILayoutUtility_t6_170_StaticFields*)GUILayoutUtility_t6_170_il2cpp_TypeInfo_var->static_fields)->___current_2;
		NullCheck(L_26);
		GUILayoutGroup_t6_169 * L_27 = (L_26->___topLevel_0);
		NullCheck(L_27);
		GUILayoutEntry_t6_171 * L_28 = GUILayoutGroup_GetNext_m6_1188(L_27, /*hidden argument*/NULL);
		NullCheck(L_28);
		Rect_t6_52  L_29 = (L_28->___rect_4);
		return L_29;
	}
}
// UnityEngine.Rect UnityEngine.GUILayoutUtility::GetRect(System.Single,System.Single,UnityEngine.GUIStyle,UnityEngine.GUILayoutOption[])
extern TypeInfo* GUILayoutUtility_t6_170_il2cpp_TypeInfo_var;
extern "C" Rect_t6_52  GUILayoutUtility_GetRect_m6_1164 (Object_t * __this /* static, unused */, float ___width, float ___height, GUIStyle_t6_166 * ___style, GUILayoutOptionU5BU5D_t6_165* ___options, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GUILayoutUtility_t6_170_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(990);
		s_Il2CppMethodIntialized = true;
	}
	{
		float L_0 = ___width;
		float L_1 = ___width;
		float L_2 = ___height;
		float L_3 = ___height;
		GUIStyle_t6_166 * L_4 = ___style;
		GUILayoutOptionU5BU5D_t6_165* L_5 = ___options;
		IL2CPP_RUNTIME_CLASS_INIT(GUILayoutUtility_t6_170_il2cpp_TypeInfo_var);
		Rect_t6_52  L_6 = GUILayoutUtility_DoGetRect_m6_1165(NULL /*static, unused*/, L_0, L_1, L_2, L_3, L_4, L_5, /*hidden argument*/NULL);
		return L_6;
	}
}
// UnityEngine.Rect UnityEngine.GUILayoutUtility::DoGetRect(System.Single,System.Single,System.Single,System.Single,UnityEngine.GUIStyle,UnityEngine.GUILayoutOption[])
extern TypeInfo* GUILayoutUtility_t6_170_il2cpp_TypeInfo_var;
extern TypeInfo* GUILayoutEntry_t6_171_il2cpp_TypeInfo_var;
extern "C" Rect_t6_52  GUILayoutUtility_DoGetRect_m6_1165 (Object_t * __this /* static, unused */, float ___minWidth, float ___maxWidth, float ___minHeight, float ___maxHeight, GUIStyle_t6_166 * ___style, GUILayoutOptionU5BU5D_t6_165* ___options, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GUILayoutUtility_t6_170_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(990);
		GUILayoutEntry_t6_171_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1000);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = {0};
	{
		Event_t6_154 * L_0 = Event_get_current_m6_989(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_0);
		int32_t L_1 = Event_get_type_m6_1003(L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		int32_t L_2 = V_0;
		if ((((int32_t)L_2) == ((int32_t)8)))
		{
			goto IL_001f;
		}
	}
	{
		int32_t L_3 = V_0;
		if ((((int32_t)L_3) == ((int32_t)((int32_t)12))))
		{
			goto IL_0041;
		}
	}
	{
		goto IL_0047;
	}

IL_001f:
	{
		IL2CPP_RUNTIME_CLASS_INIT(GUILayoutUtility_t6_170_il2cpp_TypeInfo_var);
		LayoutCache_t6_168 * L_4 = ((GUILayoutUtility_t6_170_StaticFields*)GUILayoutUtility_t6_170_il2cpp_TypeInfo_var->static_fields)->___current_2;
		NullCheck(L_4);
		GUILayoutGroup_t6_169 * L_5 = (L_4->___topLevel_0);
		float L_6 = ___minWidth;
		float L_7 = ___maxWidth;
		float L_8 = ___minHeight;
		float L_9 = ___maxHeight;
		GUIStyle_t6_166 * L_10 = ___style;
		GUILayoutOptionU5BU5D_t6_165* L_11 = ___options;
		GUILayoutEntry_t6_171 * L_12 = (GUILayoutEntry_t6_171 *)il2cpp_codegen_object_new (GUILayoutEntry_t6_171_il2cpp_TypeInfo_var);
		GUILayoutEntry__ctor_m6_1171(L_12, L_6, L_7, L_8, L_9, L_10, L_11, /*hidden argument*/NULL);
		NullCheck(L_5);
		GUILayoutGroup_Add_m6_1189(L_5, L_12, /*hidden argument*/NULL);
		Rect_t6_52  L_13 = ((GUILayoutUtility_t6_170_StaticFields*)GUILayoutUtility_t6_170_il2cpp_TypeInfo_var->static_fields)->___kDummyRect_3;
		return L_13;
	}

IL_0041:
	{
		IL2CPP_RUNTIME_CLASS_INIT(GUILayoutUtility_t6_170_il2cpp_TypeInfo_var);
		Rect_t6_52  L_14 = ((GUILayoutUtility_t6_170_StaticFields*)GUILayoutUtility_t6_170_il2cpp_TypeInfo_var->static_fields)->___kDummyRect_3;
		return L_14;
	}

IL_0047:
	{
		IL2CPP_RUNTIME_CLASS_INIT(GUILayoutUtility_t6_170_il2cpp_TypeInfo_var);
		LayoutCache_t6_168 * L_15 = ((GUILayoutUtility_t6_170_StaticFields*)GUILayoutUtility_t6_170_il2cpp_TypeInfo_var->static_fields)->___current_2;
		NullCheck(L_15);
		GUILayoutGroup_t6_169 * L_16 = (L_15->___topLevel_0);
		NullCheck(L_16);
		GUILayoutEntry_t6_171 * L_17 = GUILayoutGroup_GetNext_m6_1188(L_16, /*hidden argument*/NULL);
		NullCheck(L_17);
		Rect_t6_52  L_18 = (L_17->___rect_4);
		return L_18;
	}
}
// UnityEngine.GUIStyle UnityEngine.GUILayoutUtility::get_spaceStyle()
extern TypeInfo* GUILayoutUtility_t6_170_il2cpp_TypeInfo_var;
extern TypeInfo* GUIStyle_t6_166_il2cpp_TypeInfo_var;
extern "C" GUIStyle_t6_166 * GUILayoutUtility_get_spaceStyle_m6_1166 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GUILayoutUtility_t6_170_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(990);
		GUIStyle_t6_166_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(988);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GUILayoutUtility_t6_170_il2cpp_TypeInfo_var);
		GUIStyle_t6_166 * L_0 = ((GUILayoutUtility_t6_170_StaticFields*)GUILayoutUtility_t6_170_il2cpp_TypeInfo_var->static_fields)->___s_SpaceStyle_4;
		if (L_0)
		{
			goto IL_0014;
		}
	}
	{
		GUIStyle_t6_166 * L_1 = (GUIStyle_t6_166 *)il2cpp_codegen_object_new (GUIStyle_t6_166_il2cpp_TypeInfo_var);
		GUIStyle__ctor_m6_1298(L_1, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(GUILayoutUtility_t6_170_il2cpp_TypeInfo_var);
		((GUILayoutUtility_t6_170_StaticFields*)GUILayoutUtility_t6_170_il2cpp_TypeInfo_var->static_fields)->___s_SpaceStyle_4 = L_1;
	}

IL_0014:
	{
		IL2CPP_RUNTIME_CLASS_INIT(GUILayoutUtility_t6_170_il2cpp_TypeInfo_var);
		GUIStyle_t6_166 * L_2 = ((GUILayoutUtility_t6_170_StaticFields*)GUILayoutUtility_t6_170_il2cpp_TypeInfo_var->static_fields)->___s_SpaceStyle_4;
		NullCheck(L_2);
		GUIStyle_set_stretchWidth_m6_1345(L_2, 0, /*hidden argument*/NULL);
		GUIStyle_t6_166 * L_3 = ((GUILayoutUtility_t6_170_StaticFields*)GUILayoutUtility_t6_170_il2cpp_TypeInfo_var->static_fields)->___s_SpaceStyle_4;
		return L_3;
	}
}
// UnityEngine.Rect UnityEngine.GUILayoutUtility::Internal_GetWindowRect(System.Int32)
extern "C" Rect_t6_52  GUILayoutUtility_Internal_GetWindowRect_m6_1167 (Object_t * __this /* static, unused */, int32_t ___windowID, const MethodInfo* method)
{
	typedef Rect_t6_52  (*GUILayoutUtility_Internal_GetWindowRect_m6_1167_ftn) (int32_t);
	static GUILayoutUtility_Internal_GetWindowRect_m6_1167_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GUILayoutUtility_Internal_GetWindowRect_m6_1167_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.GUILayoutUtility::Internal_GetWindowRect(System.Int32)");
	return _il2cpp_icall_func(___windowID);
}
// System.Void UnityEngine.GUILayoutUtility::Internal_MoveWindow(System.Int32,UnityEngine.Rect)
extern TypeInfo* GUILayoutUtility_t6_170_il2cpp_TypeInfo_var;
extern "C" void GUILayoutUtility_Internal_MoveWindow_m6_1168 (Object_t * __this /* static, unused */, int32_t ___windowID, Rect_t6_52  ___r, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GUILayoutUtility_t6_170_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(990);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = ___windowID;
		IL2CPP_RUNTIME_CLASS_INIT(GUILayoutUtility_t6_170_il2cpp_TypeInfo_var);
		GUILayoutUtility_INTERNAL_CALL_Internal_MoveWindow_m6_1169(NULL /*static, unused*/, L_0, (&___r), /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.GUILayoutUtility::INTERNAL_CALL_Internal_MoveWindow(System.Int32,UnityEngine.Rect&)
extern "C" void GUILayoutUtility_INTERNAL_CALL_Internal_MoveWindow_m6_1169 (Object_t * __this /* static, unused */, int32_t ___windowID, Rect_t6_52 * ___r, const MethodInfo* method)
{
	typedef void (*GUILayoutUtility_INTERNAL_CALL_Internal_MoveWindow_m6_1169_ftn) (int32_t, Rect_t6_52 *);
	static GUILayoutUtility_INTERNAL_CALL_Internal_MoveWindow_m6_1169_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GUILayoutUtility_INTERNAL_CALL_Internal_MoveWindow_m6_1169_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.GUILayoutUtility::INTERNAL_CALL_Internal_MoveWindow(System.Int32,UnityEngine.Rect&)");
	_il2cpp_icall_func(___windowID, ___r);
}
// System.Void UnityEngine.GUILayoutEntry::.ctor(System.Single,System.Single,System.Single,System.Single,UnityEngine.GUIStyle)
extern TypeInfo* GUIStyle_t6_166_il2cpp_TypeInfo_var;
extern "C" void GUILayoutEntry__ctor_m6_1170 (GUILayoutEntry_t6_171 * __this, float ____minWidth, float ____maxWidth, float ____minHeight, float ____maxHeight, GUIStyle_t6_166 * ____style, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GUIStyle_t6_166_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(988);
		s_Il2CppMethodIntialized = true;
	}
	{
		Rect_t6_52  L_0 = {0};
		Rect__ctor_m6_270(&L_0, (0.0f), (0.0f), (0.0f), (0.0f), /*hidden argument*/NULL);
		__this->___rect_4 = L_0;
		IL2CPP_RUNTIME_CLASS_INIT(GUIStyle_t6_166_il2cpp_TypeInfo_var);
		GUIStyle_t6_166 * L_1 = GUIStyle_get_none_m6_1316(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->___m_Style_7 = L_1;
		Object__ctor_m1_0(__this, /*hidden argument*/NULL);
		float L_2 = ____minWidth;
		__this->___minWidth_0 = L_2;
		float L_3 = ____maxWidth;
		__this->___maxWidth_1 = L_3;
		float L_4 = ____minHeight;
		__this->___minHeight_2 = L_4;
		float L_5 = ____maxHeight;
		__this->___maxHeight_3 = L_5;
		GUIStyle_t6_166 * L_6 = ____style;
		if (L_6)
		{
			goto IL_005b;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GUIStyle_t6_166_il2cpp_TypeInfo_var);
		GUIStyle_t6_166 * L_7 = GUIStyle_get_none_m6_1316(NULL /*static, unused*/, /*hidden argument*/NULL);
		____style = L_7;
	}

IL_005b:
	{
		GUIStyle_t6_166 * L_8 = ____style;
		GUILayoutEntry_set_style_m6_1174(__this, L_8, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.GUILayoutEntry::.ctor(System.Single,System.Single,System.Single,System.Single,UnityEngine.GUIStyle,UnityEngine.GUILayoutOption[])
extern TypeInfo* GUIStyle_t6_166_il2cpp_TypeInfo_var;
extern "C" void GUILayoutEntry__ctor_m6_1171 (GUILayoutEntry_t6_171 * __this, float ____minWidth, float ____maxWidth, float ____minHeight, float ____maxHeight, GUIStyle_t6_166 * ____style, GUILayoutOptionU5BU5D_t6_165* ___options, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GUIStyle_t6_166_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(988);
		s_Il2CppMethodIntialized = true;
	}
	{
		Rect_t6_52  L_0 = {0};
		Rect__ctor_m6_270(&L_0, (0.0f), (0.0f), (0.0f), (0.0f), /*hidden argument*/NULL);
		__this->___rect_4 = L_0;
		IL2CPP_RUNTIME_CLASS_INIT(GUIStyle_t6_166_il2cpp_TypeInfo_var);
		GUIStyle_t6_166 * L_1 = GUIStyle_get_none_m6_1316(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->___m_Style_7 = L_1;
		Object__ctor_m1_0(__this, /*hidden argument*/NULL);
		float L_2 = ____minWidth;
		__this->___minWidth_0 = L_2;
		float L_3 = ____maxWidth;
		__this->___maxWidth_1 = L_3;
		float L_4 = ____minHeight;
		__this->___minHeight_2 = L_4;
		float L_5 = ____maxHeight;
		__this->___maxHeight_3 = L_5;
		GUIStyle_t6_166 * L_6 = ____style;
		GUILayoutEntry_set_style_m6_1174(__this, L_6, /*hidden argument*/NULL);
		GUILayoutOptionU5BU5D_t6_165* L_7 = ___options;
		VirtActionInvoker1< GUILayoutOptionU5BU5D_t6_165* >::Invoke(10 /* System.Void UnityEngine.GUILayoutEntry::ApplyOptions(UnityEngine.GUILayoutOption[]) */, __this, L_7);
		return;
	}
}
// System.Void UnityEngine.GUILayoutEntry::.cctor()
extern TypeInfo* GUILayoutEntry_t6_171_il2cpp_TypeInfo_var;
extern "C" void GUILayoutEntry__cctor_m6_1172 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GUILayoutEntry_t6_171_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1000);
		s_Il2CppMethodIntialized = true;
	}
	{
		Rect_t6_52  L_0 = {0};
		Rect__ctor_m6_270(&L_0, (0.0f), (0.0f), (1.0f), (1.0f), /*hidden argument*/NULL);
		((GUILayoutEntry_t6_171_StaticFields*)GUILayoutEntry_t6_171_il2cpp_TypeInfo_var->static_fields)->___kDummyRect_8 = L_0;
		((GUILayoutEntry_t6_171_StaticFields*)GUILayoutEntry_t6_171_il2cpp_TypeInfo_var->static_fields)->___indent_9 = 0;
		return;
	}
}
// UnityEngine.GUIStyle UnityEngine.GUILayoutEntry::get_style()
extern "C" GUIStyle_t6_166 * GUILayoutEntry_get_style_m6_1173 (GUILayoutEntry_t6_171 * __this, const MethodInfo* method)
{
	{
		GUIStyle_t6_166 * L_0 = (__this->___m_Style_7);
		return L_0;
	}
}
// System.Void UnityEngine.GUILayoutEntry::set_style(UnityEngine.GUIStyle)
extern "C" void GUILayoutEntry_set_style_m6_1174 (GUILayoutEntry_t6_171 * __this, GUIStyle_t6_166 * ___value, const MethodInfo* method)
{
	{
		GUIStyle_t6_166 * L_0 = ___value;
		__this->___m_Style_7 = L_0;
		GUIStyle_t6_166 * L_1 = ___value;
		VirtActionInvoker1< GUIStyle_t6_166 * >::Invoke(9 /* System.Void UnityEngine.GUILayoutEntry::ApplyStyleSettings(UnityEngine.GUIStyle) */, __this, L_1);
		return;
	}
}
// UnityEngine.RectOffset UnityEngine.GUILayoutEntry::get_margin()
extern "C" RectOffset_t6_172 * GUILayoutEntry_get_margin_m6_1175 (GUILayoutEntry_t6_171 * __this, const MethodInfo* method)
{
	{
		GUIStyle_t6_166 * L_0 = GUILayoutEntry_get_style_m6_1173(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		RectOffset_t6_172 * L_1 = GUIStyle_get_margin_m6_1303(L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Void UnityEngine.GUILayoutEntry::CalcWidth()
extern "C" void GUILayoutEntry_CalcWidth_m6_1176 (GUILayoutEntry_t6_171 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void UnityEngine.GUILayoutEntry::CalcHeight()
extern "C" void GUILayoutEntry_CalcHeight_m6_1177 (GUILayoutEntry_t6_171 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void UnityEngine.GUILayoutEntry::SetHorizontal(System.Single,System.Single)
extern "C" void GUILayoutEntry_SetHorizontal_m6_1178 (GUILayoutEntry_t6_171 * __this, float ___x, float ___width, const MethodInfo* method)
{
	{
		Rect_t6_52 * L_0 = &(__this->___rect_4);
		float L_1 = ___x;
		Rect_set_x_m6_274(L_0, L_1, /*hidden argument*/NULL);
		Rect_t6_52 * L_2 = &(__this->___rect_4);
		float L_3 = ___width;
		Rect_set_width_m6_278(L_2, L_3, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.GUILayoutEntry::SetVertical(System.Single,System.Single)
extern "C" void GUILayoutEntry_SetVertical_m6_1179 (GUILayoutEntry_t6_171 * __this, float ___y, float ___height, const MethodInfo* method)
{
	{
		Rect_t6_52 * L_0 = &(__this->___rect_4);
		float L_1 = ___y;
		Rect_set_y_m6_276(L_0, L_1, /*hidden argument*/NULL);
		Rect_t6_52 * L_2 = &(__this->___rect_4);
		float L_3 = ___height;
		Rect_set_height_m6_280(L_2, L_3, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.GUILayoutEntry::ApplyStyleSettings(UnityEngine.GUIStyle)
extern "C" void GUILayoutEntry_ApplyStyleSettings_m6_1180 (GUILayoutEntry_t6_171 * __this, GUIStyle_t6_166 * ___style, const MethodInfo* method)
{
	GUILayoutEntry_t6_171 * G_B3_0 = {0};
	GUILayoutEntry_t6_171 * G_B1_0 = {0};
	GUILayoutEntry_t6_171 * G_B2_0 = {0};
	int32_t G_B4_0 = 0;
	GUILayoutEntry_t6_171 * G_B4_1 = {0};
	GUILayoutEntry_t6_171 * G_B7_0 = {0};
	GUILayoutEntry_t6_171 * G_B5_0 = {0};
	GUILayoutEntry_t6_171 * G_B6_0 = {0};
	int32_t G_B8_0 = 0;
	GUILayoutEntry_t6_171 * G_B8_1 = {0};
	{
		GUIStyle_t6_166 * L_0 = ___style;
		NullCheck(L_0);
		float L_1 = GUIStyle_get_fixedWidth_m6_1342(L_0, /*hidden argument*/NULL);
		G_B1_0 = __this;
		if ((!(((float)L_1) == ((float)(0.0f)))))
		{
			G_B3_0 = __this;
			goto IL_0022;
		}
	}
	{
		GUIStyle_t6_166 * L_2 = ___style;
		NullCheck(L_2);
		bool L_3 = GUIStyle_get_stretchWidth_m6_1344(L_2, /*hidden argument*/NULL);
		G_B2_0 = G_B1_0;
		if (!L_3)
		{
			G_B3_0 = G_B1_0;
			goto IL_0022;
		}
	}
	{
		G_B4_0 = 1;
		G_B4_1 = G_B2_0;
		goto IL_0023;
	}

IL_0022:
	{
		G_B4_0 = 0;
		G_B4_1 = G_B3_0;
	}

IL_0023:
	{
		NullCheck(G_B4_1);
		G_B4_1->___stretchWidth_5 = G_B4_0;
		GUIStyle_t6_166 * L_4 = ___style;
		NullCheck(L_4);
		float L_5 = GUIStyle_get_fixedHeight_m6_1343(L_4, /*hidden argument*/NULL);
		G_B5_0 = __this;
		if ((!(((float)L_5) == ((float)(0.0f)))))
		{
			G_B7_0 = __this;
			goto IL_004a;
		}
	}
	{
		GUIStyle_t6_166 * L_6 = ___style;
		NullCheck(L_6);
		bool L_7 = GUIStyle_get_stretchHeight_m6_1346(L_6, /*hidden argument*/NULL);
		G_B6_0 = G_B5_0;
		if (!L_7)
		{
			G_B7_0 = G_B5_0;
			goto IL_004a;
		}
	}
	{
		G_B8_0 = 1;
		G_B8_1 = G_B6_0;
		goto IL_004b;
	}

IL_004a:
	{
		G_B8_0 = 0;
		G_B8_1 = G_B7_0;
	}

IL_004b:
	{
		NullCheck(G_B8_1);
		G_B8_1->___stretchHeight_6 = G_B8_0;
		GUIStyle_t6_166 * L_8 = ___style;
		__this->___m_Style_7 = L_8;
		return;
	}
}
// System.Void UnityEngine.GUILayoutEntry::ApplyOptions(UnityEngine.GUILayoutOption[])
extern TypeInfo* Single_t1_17_il2cpp_TypeInfo_var;
extern TypeInfo* Int32_t1_3_il2cpp_TypeInfo_var;
extern "C" void GUILayoutEntry_ApplyOptions_m6_1181 (GUILayoutEntry_t6_171 * __this, GUILayoutOptionU5BU5D_t6_165* ___options, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Single_t1_17_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(43);
		Int32_t1_3_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(8);
		s_Il2CppMethodIntialized = true;
	}
	GUILayoutOption_t6_176 * V_0 = {0};
	GUILayoutOptionU5BU5D_t6_165* V_1 = {0};
	int32_t V_2 = 0;
	int32_t V_3 = {0};
	float V_4 = 0.0f;
	{
		GUILayoutOptionU5BU5D_t6_165* L_0 = ___options;
		if (L_0)
		{
			goto IL_0007;
		}
	}
	{
		return;
	}

IL_0007:
	{
		GUILayoutOptionU5BU5D_t6_165* L_1 = ___options;
		V_1 = L_1;
		V_2 = 0;
		goto IL_01a0;
	}

IL_0010:
	{
		GUILayoutOptionU5BU5D_t6_165* L_2 = V_1;
		int32_t L_3 = V_2;
		NullCheck(L_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_2, L_3);
		int32_t L_4 = L_3;
		V_0 = (*(GUILayoutOption_t6_176 **)(GUILayoutOption_t6_176 **)SZArrayLdElema(L_2, L_4, sizeof(GUILayoutOption_t6_176 *)));
		GUILayoutOption_t6_176 * L_5 = V_0;
		NullCheck(L_5);
		int32_t L_6 = (L_5->___type_0);
		V_3 = L_6;
		int32_t L_7 = V_3;
		if (L_7 == 0)
		{
			goto IL_0046;
		}
		if (L_7 == 1)
		{
			goto IL_006e;
		}
		if (L_7 == 2)
		{
			goto IL_0096;
		}
		if (L_7 == 3)
		{
			goto IL_00c9;
		}
		if (L_7 == 4)
		{
			goto IL_0103;
		}
		if (L_7 == 5)
		{
			goto IL_0136;
		}
		if (L_7 == 6)
		{
			goto IL_0170;
		}
		if (L_7 == 7)
		{
			goto IL_0186;
		}
	}
	{
		goto IL_019c;
	}

IL_0046:
	{
		GUILayoutOption_t6_176 * L_8 = V_0;
		NullCheck(L_8);
		Object_t * L_9 = (L_8->___value_1);
		float L_10 = ((*(float*)((float*)UnBox (L_9, Single_t1_17_il2cpp_TypeInfo_var))));
		V_4 = L_10;
		__this->___maxWidth_1 = L_10;
		float L_11 = V_4;
		__this->___minWidth_0 = L_11;
		__this->___stretchWidth_5 = 0;
		goto IL_019c;
	}

IL_006e:
	{
		GUILayoutOption_t6_176 * L_12 = V_0;
		NullCheck(L_12);
		Object_t * L_13 = (L_12->___value_1);
		float L_14 = ((*(float*)((float*)UnBox (L_13, Single_t1_17_il2cpp_TypeInfo_var))));
		V_4 = L_14;
		__this->___maxHeight_3 = L_14;
		float L_15 = V_4;
		__this->___minHeight_2 = L_15;
		__this->___stretchHeight_6 = 0;
		goto IL_019c;
	}

IL_0096:
	{
		GUILayoutOption_t6_176 * L_16 = V_0;
		NullCheck(L_16);
		Object_t * L_17 = (L_16->___value_1);
		__this->___minWidth_0 = ((*(float*)((float*)UnBox (L_17, Single_t1_17_il2cpp_TypeInfo_var))));
		float L_18 = (__this->___maxWidth_1);
		float L_19 = (__this->___minWidth_0);
		if ((!(((float)L_18) < ((float)L_19))))
		{
			goto IL_00c4;
		}
	}
	{
		float L_20 = (__this->___minWidth_0);
		__this->___maxWidth_1 = L_20;
	}

IL_00c4:
	{
		goto IL_019c;
	}

IL_00c9:
	{
		GUILayoutOption_t6_176 * L_21 = V_0;
		NullCheck(L_21);
		Object_t * L_22 = (L_21->___value_1);
		__this->___maxWidth_1 = ((*(float*)((float*)UnBox (L_22, Single_t1_17_il2cpp_TypeInfo_var))));
		float L_23 = (__this->___minWidth_0);
		float L_24 = (__this->___maxWidth_1);
		if ((!(((float)L_23) > ((float)L_24))))
		{
			goto IL_00f7;
		}
	}
	{
		float L_25 = (__this->___maxWidth_1);
		__this->___minWidth_0 = L_25;
	}

IL_00f7:
	{
		__this->___stretchWidth_5 = 0;
		goto IL_019c;
	}

IL_0103:
	{
		GUILayoutOption_t6_176 * L_26 = V_0;
		NullCheck(L_26);
		Object_t * L_27 = (L_26->___value_1);
		__this->___minHeight_2 = ((*(float*)((float*)UnBox (L_27, Single_t1_17_il2cpp_TypeInfo_var))));
		float L_28 = (__this->___maxHeight_3);
		float L_29 = (__this->___minHeight_2);
		if ((!(((float)L_28) < ((float)L_29))))
		{
			goto IL_0131;
		}
	}
	{
		float L_30 = (__this->___minHeight_2);
		__this->___maxHeight_3 = L_30;
	}

IL_0131:
	{
		goto IL_019c;
	}

IL_0136:
	{
		GUILayoutOption_t6_176 * L_31 = V_0;
		NullCheck(L_31);
		Object_t * L_32 = (L_31->___value_1);
		__this->___maxHeight_3 = ((*(float*)((float*)UnBox (L_32, Single_t1_17_il2cpp_TypeInfo_var))));
		float L_33 = (__this->___minHeight_2);
		float L_34 = (__this->___maxHeight_3);
		if ((!(((float)L_33) > ((float)L_34))))
		{
			goto IL_0164;
		}
	}
	{
		float L_35 = (__this->___maxHeight_3);
		__this->___minHeight_2 = L_35;
	}

IL_0164:
	{
		__this->___stretchHeight_6 = 0;
		goto IL_019c;
	}

IL_0170:
	{
		GUILayoutOption_t6_176 * L_36 = V_0;
		NullCheck(L_36);
		Object_t * L_37 = (L_36->___value_1);
		__this->___stretchWidth_5 = ((*(int32_t*)((int32_t*)UnBox (L_37, Int32_t1_3_il2cpp_TypeInfo_var))));
		goto IL_019c;
	}

IL_0186:
	{
		GUILayoutOption_t6_176 * L_38 = V_0;
		NullCheck(L_38);
		Object_t * L_39 = (L_38->___value_1);
		__this->___stretchHeight_6 = ((*(int32_t*)((int32_t*)UnBox (L_39, Int32_t1_3_il2cpp_TypeInfo_var))));
		goto IL_019c;
	}

IL_019c:
	{
		int32_t L_40 = V_2;
		V_2 = ((int32_t)((int32_t)L_40+(int32_t)1));
	}

IL_01a0:
	{
		int32_t L_41 = V_2;
		GUILayoutOptionU5BU5D_t6_165* L_42 = V_1;
		NullCheck(L_42);
		if ((((int32_t)L_41) < ((int32_t)(((int32_t)((int32_t)(((Array_t *)L_42)->max_length)))))))
		{
			goto IL_0010;
		}
	}
	{
		float L_43 = (__this->___maxWidth_1);
		if ((((float)L_43) == ((float)(0.0f))))
		{
			goto IL_01d6;
		}
	}
	{
		float L_44 = (__this->___maxWidth_1);
		float L_45 = (__this->___minWidth_0);
		if ((!(((float)L_44) < ((float)L_45))))
		{
			goto IL_01d6;
		}
	}
	{
		float L_46 = (__this->___minWidth_0);
		__this->___maxWidth_1 = L_46;
	}

IL_01d6:
	{
		float L_47 = (__this->___maxHeight_3);
		if ((((float)L_47) == ((float)(0.0f))))
		{
			goto IL_0203;
		}
	}
	{
		float L_48 = (__this->___maxHeight_3);
		float L_49 = (__this->___minHeight_2);
		if ((!(((float)L_48) < ((float)L_49))))
		{
			goto IL_0203;
		}
	}
	{
		float L_50 = (__this->___minHeight_2);
		__this->___maxHeight_3 = L_50;
	}

IL_0203:
	{
		return;
	}
}
// System.String UnityEngine.GUILayoutEntry::ToString()
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* GUILayoutEntry_t6_171_il2cpp_TypeInfo_var;
extern TypeInfo* ObjectU5BU5D_t1_157_il2cpp_TypeInfo_var;
extern TypeInfo* Single_t1_17_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral232;
extern Il2CppCodeGenString* _stringLiteral2792;
extern Il2CppCodeGenString* _stringLiteral2793;
extern Il2CppCodeGenString* _stringLiteral2794;
extern Il2CppCodeGenString* _stringLiteral702;
extern Il2CppCodeGenString* _stringLiteral706;
extern Il2CppCodeGenString* _stringLiteral2795;
extern "C" String_t* GUILayoutEntry_ToString_m6_1182 (GUILayoutEntry_t6_171 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(11);
		GUILayoutEntry_t6_171_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1000);
		ObjectU5BU5D_t1_157_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(19);
		Single_t1_17_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(43);
		_stringLiteral232 = il2cpp_codegen_string_literal_from_index(232);
		_stringLiteral2792 = il2cpp_codegen_string_literal_from_index(2792);
		_stringLiteral2793 = il2cpp_codegen_string_literal_from_index(2793);
		_stringLiteral2794 = il2cpp_codegen_string_literal_from_index(2794);
		_stringLiteral702 = il2cpp_codegen_string_literal_from_index(702);
		_stringLiteral706 = il2cpp_codegen_string_literal_from_index(706);
		_stringLiteral2795 = il2cpp_codegen_string_literal_from_index(2795);
		s_Il2CppMethodIntialized = true;
	}
	String_t* V_0 = {0};
	int32_t V_1 = 0;
	int32_t G_B5_0 = 0;
	ObjectU5BU5D_t1_157* G_B5_1 = {0};
	ObjectU5BU5D_t1_157* G_B5_2 = {0};
	String_t* G_B5_3 = {0};
	int32_t G_B5_4 = 0;
	ObjectU5BU5D_t1_157* G_B5_5 = {0};
	ObjectU5BU5D_t1_157* G_B5_6 = {0};
	int32_t G_B4_0 = 0;
	ObjectU5BU5D_t1_157* G_B4_1 = {0};
	ObjectU5BU5D_t1_157* G_B4_2 = {0};
	String_t* G_B4_3 = {0};
	int32_t G_B4_4 = 0;
	ObjectU5BU5D_t1_157* G_B4_5 = {0};
	ObjectU5BU5D_t1_157* G_B4_6 = {0};
	String_t* G_B6_0 = {0};
	int32_t G_B6_1 = 0;
	ObjectU5BU5D_t1_157* G_B6_2 = {0};
	ObjectU5BU5D_t1_157* G_B6_3 = {0};
	String_t* G_B6_4 = {0};
	int32_t G_B6_5 = 0;
	ObjectU5BU5D_t1_157* G_B6_6 = {0};
	ObjectU5BU5D_t1_157* G_B6_7 = {0};
	int32_t G_B8_0 = 0;
	ObjectU5BU5D_t1_157* G_B8_1 = {0};
	ObjectU5BU5D_t1_157* G_B8_2 = {0};
	int32_t G_B7_0 = 0;
	ObjectU5BU5D_t1_157* G_B7_1 = {0};
	ObjectU5BU5D_t1_157* G_B7_2 = {0};
	String_t* G_B9_0 = {0};
	int32_t G_B9_1 = 0;
	ObjectU5BU5D_t1_157* G_B9_2 = {0};
	ObjectU5BU5D_t1_157* G_B9_3 = {0};
	int32_t G_B11_0 = 0;
	ObjectU5BU5D_t1_157* G_B11_1 = {0};
	ObjectU5BU5D_t1_157* G_B11_2 = {0};
	int32_t G_B10_0 = 0;
	ObjectU5BU5D_t1_157* G_B10_1 = {0};
	ObjectU5BU5D_t1_157* G_B10_2 = {0};
	String_t* G_B12_0 = {0};
	int32_t G_B12_1 = 0;
	ObjectU5BU5D_t1_157* G_B12_2 = {0};
	ObjectU5BU5D_t1_157* G_B12_3 = {0};
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_0 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->___Empty_2;
		V_0 = L_0;
		V_1 = 0;
		goto IL_001d;
	}

IL_000d:
	{
		String_t* L_1 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_2 = String_Concat_m1_418(NULL /*static, unused*/, L_1, _stringLiteral232, /*hidden argument*/NULL);
		V_0 = L_2;
		int32_t L_3 = V_1;
		V_1 = ((int32_t)((int32_t)L_3+(int32_t)1));
	}

IL_001d:
	{
		int32_t L_4 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(GUILayoutEntry_t6_171_il2cpp_TypeInfo_var);
		int32_t L_5 = ((GUILayoutEntry_t6_171_StaticFields*)GUILayoutEntry_t6_171_il2cpp_TypeInfo_var->static_fields)->___indent_9;
		if ((((int32_t)L_4) < ((int32_t)L_5)))
		{
			goto IL_000d;
		}
	}
	{
		ObjectU5BU5D_t1_157* L_6 = ((ObjectU5BU5D_t1_157*)SZArrayNew(ObjectU5BU5D_t1_157_il2cpp_TypeInfo_var, ((int32_t)12)));
		String_t* L_7 = V_0;
		NullCheck(L_6);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_6, 0);
		ArrayElementTypeCheck (L_6, L_7);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_6, 0, sizeof(Object_t *))) = (Object_t *)L_7;
		ObjectU5BU5D_t1_157* L_8 = L_6;
		ObjectU5BU5D_t1_157* L_9 = ((ObjectU5BU5D_t1_157*)SZArrayNew(ObjectU5BU5D_t1_157_il2cpp_TypeInfo_var, 6));
		GUIStyle_t6_166 * L_10 = GUILayoutEntry_get_style_m6_1173(__this, /*hidden argument*/NULL);
		G_B4_0 = 0;
		G_B4_1 = L_9;
		G_B4_2 = L_9;
		G_B4_3 = _stringLiteral2792;
		G_B4_4 = 1;
		G_B4_5 = L_8;
		G_B4_6 = L_8;
		if (!L_10)
		{
			G_B5_0 = 0;
			G_B5_1 = L_9;
			G_B5_2 = L_9;
			G_B5_3 = _stringLiteral2792;
			G_B5_4 = 1;
			G_B5_5 = L_8;
			G_B5_6 = L_8;
			goto IL_005d;
		}
	}
	{
		GUIStyle_t6_166 * L_11 = GUILayoutEntry_get_style_m6_1173(__this, /*hidden argument*/NULL);
		NullCheck(L_11);
		String_t* L_12 = GUIStyle_get_name_m6_1327(L_11, /*hidden argument*/NULL);
		G_B6_0 = L_12;
		G_B6_1 = G_B4_0;
		G_B6_2 = G_B4_1;
		G_B6_3 = G_B4_2;
		G_B6_4 = G_B4_3;
		G_B6_5 = G_B4_4;
		G_B6_6 = G_B4_5;
		G_B6_7 = G_B4_6;
		goto IL_0062;
	}

IL_005d:
	{
		G_B6_0 = _stringLiteral2793;
		G_B6_1 = G_B5_0;
		G_B6_2 = G_B5_1;
		G_B6_3 = G_B5_2;
		G_B6_4 = G_B5_3;
		G_B6_5 = G_B5_4;
		G_B6_6 = G_B5_5;
		G_B6_7 = G_B5_6;
	}

IL_0062:
	{
		NullCheck(G_B6_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(G_B6_2, G_B6_1);
		ArrayElementTypeCheck (G_B6_2, G_B6_0);
		*((Object_t **)(Object_t **)SZArrayLdElema(G_B6_2, G_B6_1, sizeof(Object_t *))) = (Object_t *)G_B6_0;
		ObjectU5BU5D_t1_157* L_13 = G_B6_3;
		Type_t * L_14 = Object_GetType_m1_5(__this, /*hidden argument*/NULL);
		NullCheck(L_13);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_13, 1);
		ArrayElementTypeCheck (L_13, L_14);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_13, 1, sizeof(Object_t *))) = (Object_t *)L_14;
		ObjectU5BU5D_t1_157* L_15 = L_13;
		Rect_t6_52 * L_16 = &(__this->___rect_4);
		float L_17 = Rect_get_x_m6_273(L_16, /*hidden argument*/NULL);
		float L_18 = L_17;
		Object_t * L_19 = Box(Single_t1_17_il2cpp_TypeInfo_var, &L_18);
		NullCheck(L_15);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_15, 2);
		ArrayElementTypeCheck (L_15, L_19);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_15, 2, sizeof(Object_t *))) = (Object_t *)L_19;
		ObjectU5BU5D_t1_157* L_20 = L_15;
		Rect_t6_52 * L_21 = &(__this->___rect_4);
		float L_22 = Rect_get_xMax_m6_283(L_21, /*hidden argument*/NULL);
		float L_23 = L_22;
		Object_t * L_24 = Box(Single_t1_17_il2cpp_TypeInfo_var, &L_23);
		NullCheck(L_20);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_20, 3);
		ArrayElementTypeCheck (L_20, L_24);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_20, 3, sizeof(Object_t *))) = (Object_t *)L_24;
		ObjectU5BU5D_t1_157* L_25 = L_20;
		Rect_t6_52 * L_26 = &(__this->___rect_4);
		float L_27 = Rect_get_y_m6_275(L_26, /*hidden argument*/NULL);
		float L_28 = L_27;
		Object_t * L_29 = Box(Single_t1_17_il2cpp_TypeInfo_var, &L_28);
		NullCheck(L_25);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_25, 4);
		ArrayElementTypeCheck (L_25, L_29);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_25, 4, sizeof(Object_t *))) = (Object_t *)L_29;
		ObjectU5BU5D_t1_157* L_30 = L_25;
		Rect_t6_52 * L_31 = &(__this->___rect_4);
		float L_32 = Rect_get_yMax_m6_284(L_31, /*hidden argument*/NULL);
		float L_33 = L_32;
		Object_t * L_34 = Box(Single_t1_17_il2cpp_TypeInfo_var, &L_33);
		NullCheck(L_30);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_30, 5);
		ArrayElementTypeCheck (L_30, L_34);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_30, 5, sizeof(Object_t *))) = (Object_t *)L_34;
		String_t* L_35 = UnityString_Format_m6_437(NULL /*static, unused*/, G_B6_4, L_30, /*hidden argument*/NULL);
		NullCheck(G_B6_6);
		IL2CPP_ARRAY_BOUNDS_CHECK(G_B6_6, G_B6_5);
		ArrayElementTypeCheck (G_B6_6, L_35);
		*((Object_t **)(Object_t **)SZArrayLdElema(G_B6_6, G_B6_5, sizeof(Object_t *))) = (Object_t *)L_35;
		ObjectU5BU5D_t1_157* L_36 = G_B6_7;
		NullCheck(L_36);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_36, 2);
		ArrayElementTypeCheck (L_36, _stringLiteral2794);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_36, 2, sizeof(Object_t *))) = (Object_t *)_stringLiteral2794;
		ObjectU5BU5D_t1_157* L_37 = L_36;
		float L_38 = (__this->___minWidth_0);
		float L_39 = L_38;
		Object_t * L_40 = Box(Single_t1_17_il2cpp_TypeInfo_var, &L_39);
		NullCheck(L_37);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_37, 3);
		ArrayElementTypeCheck (L_37, L_40);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_37, 3, sizeof(Object_t *))) = (Object_t *)L_40;
		ObjectU5BU5D_t1_157* L_41 = L_37;
		NullCheck(L_41);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_41, 4);
		ArrayElementTypeCheck (L_41, _stringLiteral702);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_41, 4, sizeof(Object_t *))) = (Object_t *)_stringLiteral702;
		ObjectU5BU5D_t1_157* L_42 = L_41;
		float L_43 = (__this->___maxWidth_1);
		float L_44 = L_43;
		Object_t * L_45 = Box(Single_t1_17_il2cpp_TypeInfo_var, &L_44);
		NullCheck(L_42);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_42, 5);
		ArrayElementTypeCheck (L_42, L_45);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_42, 5, sizeof(Object_t *))) = (Object_t *)L_45;
		ObjectU5BU5D_t1_157* L_46 = L_42;
		int32_t L_47 = (__this->___stretchWidth_5);
		G_B7_0 = 6;
		G_B7_1 = L_46;
		G_B7_2 = L_46;
		if (!L_47)
		{
			G_B8_0 = 6;
			G_B8_1 = L_46;
			G_B8_2 = L_46;
			goto IL_0101;
		}
	}
	{
		G_B9_0 = _stringLiteral706;
		G_B9_1 = G_B7_0;
		G_B9_2 = G_B7_1;
		G_B9_3 = G_B7_2;
		goto IL_0106;
	}

IL_0101:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_48 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->___Empty_2;
		G_B9_0 = L_48;
		G_B9_1 = G_B8_0;
		G_B9_2 = G_B8_1;
		G_B9_3 = G_B8_2;
	}

IL_0106:
	{
		NullCheck(G_B9_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(G_B9_2, G_B9_1);
		ArrayElementTypeCheck (G_B9_2, G_B9_0);
		*((Object_t **)(Object_t **)SZArrayLdElema(G_B9_2, G_B9_1, sizeof(Object_t *))) = (Object_t *)G_B9_0;
		ObjectU5BU5D_t1_157* L_49 = G_B9_3;
		NullCheck(L_49);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_49, 7);
		ArrayElementTypeCheck (L_49, _stringLiteral2795);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_49, 7, sizeof(Object_t *))) = (Object_t *)_stringLiteral2795;
		ObjectU5BU5D_t1_157* L_50 = L_49;
		float L_51 = (__this->___minHeight_2);
		float L_52 = L_51;
		Object_t * L_53 = Box(Single_t1_17_il2cpp_TypeInfo_var, &L_52);
		NullCheck(L_50);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_50, 8);
		ArrayElementTypeCheck (L_50, L_53);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_50, 8, sizeof(Object_t *))) = (Object_t *)L_53;
		ObjectU5BU5D_t1_157* L_54 = L_50;
		NullCheck(L_54);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_54, ((int32_t)9));
		ArrayElementTypeCheck (L_54, _stringLiteral702);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_54, ((int32_t)9), sizeof(Object_t *))) = (Object_t *)_stringLiteral702;
		ObjectU5BU5D_t1_157* L_55 = L_54;
		float L_56 = (__this->___maxHeight_3);
		float L_57 = L_56;
		Object_t * L_58 = Box(Single_t1_17_il2cpp_TypeInfo_var, &L_57);
		NullCheck(L_55);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_55, ((int32_t)10));
		ArrayElementTypeCheck (L_55, L_58);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_55, ((int32_t)10), sizeof(Object_t *))) = (Object_t *)L_58;
		ObjectU5BU5D_t1_157* L_59 = L_55;
		int32_t L_60 = (__this->___stretchHeight_6);
		G_B10_0 = ((int32_t)11);
		G_B10_1 = L_59;
		G_B10_2 = L_59;
		if (!L_60)
		{
			G_B11_0 = ((int32_t)11);
			G_B11_1 = L_59;
			G_B11_2 = L_59;
			goto IL_014d;
		}
	}
	{
		G_B12_0 = _stringLiteral706;
		G_B12_1 = G_B10_0;
		G_B12_2 = G_B10_1;
		G_B12_3 = G_B10_2;
		goto IL_0152;
	}

IL_014d:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_61 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->___Empty_2;
		G_B12_0 = L_61;
		G_B12_1 = G_B11_0;
		G_B12_2 = G_B11_1;
		G_B12_3 = G_B11_2;
	}

IL_0152:
	{
		NullCheck(G_B12_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(G_B12_2, G_B12_1);
		ArrayElementTypeCheck (G_B12_2, G_B12_0);
		*((Object_t **)(Object_t **)SZArrayLdElema(G_B12_2, G_B12_1, sizeof(Object_t *))) = (Object_t *)G_B12_0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_62 = String_Concat_m1_421(NULL /*static, unused*/, G_B12_3, /*hidden argument*/NULL);
		return L_62;
	}
}
// System.Void UnityEngine.GUILayoutGroup::.ctor()
extern TypeInfo* List_1_t1_917_il2cpp_TypeInfo_var;
extern TypeInfo* RectOffset_t6_172_il2cpp_TypeInfo_var;
extern TypeInfo* GUIStyle_t6_166_il2cpp_TypeInfo_var;
extern TypeInfo* GUILayoutEntry_t6_171_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1__ctor_m1_5607_MethodInfo_var;
extern "C" void GUILayoutGroup__ctor_m6_1183 (GUILayoutGroup_t6_169 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		List_1_t1_917_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1003);
		RectOffset_t6_172_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1004);
		GUIStyle_t6_166_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(988);
		GUILayoutEntry_t6_171_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1000);
		List_1__ctor_m1_5607_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483815);
		s_Il2CppMethodIntialized = true;
	}
	{
		List_1_t1_917 * L_0 = (List_1_t1_917 *)il2cpp_codegen_object_new (List_1_t1_917_il2cpp_TypeInfo_var);
		List_1__ctor_m1_5607(L_0, /*hidden argument*/List_1__ctor_m1_5607_MethodInfo_var);
		__this->___entries_10 = L_0;
		__this->___isVertical_11 = 1;
		__this->___sameSize_14 = 1;
		__this->___windowID_16 = (-1);
		__this->___m_StretchableCountX_18 = ((int32_t)100);
		__this->___m_StretchableCountY_19 = ((int32_t)100);
		__this->___m_ChildMinWidth_22 = (100.0f);
		__this->___m_ChildMaxWidth_23 = (100.0f);
		__this->___m_ChildMinHeight_24 = (100.0f);
		__this->___m_ChildMaxHeight_25 = (100.0f);
		RectOffset_t6_172 * L_1 = (RectOffset_t6_172 *)il2cpp_codegen_object_new (RectOffset_t6_172_il2cpp_TypeInfo_var);
		RectOffset__ctor_m6_1277(L_1, /*hidden argument*/NULL);
		__this->___m_Margin_26 = L_1;
		IL2CPP_RUNTIME_CLASS_INIT(GUIStyle_t6_166_il2cpp_TypeInfo_var);
		GUIStyle_t6_166 * L_2 = GUIStyle_get_none_m6_1316(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(GUILayoutEntry_t6_171_il2cpp_TypeInfo_var);
		GUILayoutEntry__ctor_m6_1170(__this, (0.0f), (0.0f), (0.0f), (0.0f), L_2, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.RectOffset UnityEngine.GUILayoutGroup::get_margin()
extern "C" RectOffset_t6_172 * GUILayoutGroup_get_margin_m6_1184 (GUILayoutGroup_t6_169 * __this, const MethodInfo* method)
{
	{
		RectOffset_t6_172 * L_0 = (__this->___m_Margin_26);
		return L_0;
	}
}
// System.Void UnityEngine.GUILayoutGroup::ApplyOptions(UnityEngine.GUILayoutOption[])
extern TypeInfo* Int32_t1_3_il2cpp_TypeInfo_var;
extern "C" void GUILayoutGroup_ApplyOptions_m6_1185 (GUILayoutGroup_t6_169 * __this, GUILayoutOptionU5BU5D_t6_165* ___options, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Int32_t1_3_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(8);
		s_Il2CppMethodIntialized = true;
	}
	GUILayoutOption_t6_176 * V_0 = {0};
	GUILayoutOptionU5BU5D_t6_165* V_1 = {0};
	int32_t V_2 = 0;
	int32_t V_3 = {0};
	{
		GUILayoutOptionU5BU5D_t6_165* L_0 = ___options;
		if (L_0)
		{
			goto IL_0007;
		}
	}
	{
		return;
	}

IL_0007:
	{
		GUILayoutOptionU5BU5D_t6_165* L_1 = ___options;
		GUILayoutEntry_ApplyOptions_m6_1181(__this, L_1, /*hidden argument*/NULL);
		GUILayoutOptionU5BU5D_t6_165* L_2 = ___options;
		V_1 = L_2;
		V_2 = 0;
		goto IL_0098;
	}

IL_0017:
	{
		GUILayoutOptionU5BU5D_t6_165* L_3 = V_1;
		int32_t L_4 = V_2;
		NullCheck(L_3);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_3, L_4);
		int32_t L_5 = L_4;
		V_0 = (*(GUILayoutOption_t6_176 **)(GUILayoutOption_t6_176 **)SZArrayLdElema(L_3, L_5, sizeof(GUILayoutOption_t6_176 *)));
		GUILayoutOption_t6_176 * L_6 = V_0;
		NullCheck(L_6);
		int32_t L_7 = (L_6->___type_0);
		V_3 = L_7;
		int32_t L_8 = V_3;
		if (L_8 == 0)
		{
			goto IL_0065;
		}
		if (L_8 == 1)
		{
			goto IL_0071;
		}
		if (L_8 == 2)
		{
			goto IL_0065;
		}
		if (L_8 == 3)
		{
			goto IL_0065;
		}
		if (L_8 == 4)
		{
			goto IL_0071;
		}
		if (L_8 == 5)
		{
			goto IL_0071;
		}
		if (L_8 == 6)
		{
			goto IL_0094;
		}
		if (L_8 == 7)
		{
			goto IL_0094;
		}
		if (L_8 == 8)
		{
			goto IL_0094;
		}
		if (L_8 == 9)
		{
			goto IL_0094;
		}
		if (L_8 == 10)
		{
			goto IL_0094;
		}
		if (L_8 == 11)
		{
			goto IL_0094;
		}
		if (L_8 == 12)
		{
			goto IL_0094;
		}
		if (L_8 == 13)
		{
			goto IL_007d;
		}
	}
	{
		goto IL_0094;
	}

IL_0065:
	{
		__this->___m_UserSpecifiedHeight_21 = 1;
		goto IL_0094;
	}

IL_0071:
	{
		__this->___m_UserSpecifiedWidth_20 = 1;
		goto IL_0094;
	}

IL_007d:
	{
		GUILayoutOption_t6_176 * L_9 = V_0;
		NullCheck(L_9);
		Object_t * L_10 = (L_9->___value_1);
		__this->___spacing_13 = (((float)((float)((*(int32_t*)((int32_t*)UnBox (L_10, Int32_t1_3_il2cpp_TypeInfo_var)))))));
		goto IL_0094;
	}

IL_0094:
	{
		int32_t L_11 = V_2;
		V_2 = ((int32_t)((int32_t)L_11+(int32_t)1));
	}

IL_0098:
	{
		int32_t L_12 = V_2;
		GUILayoutOptionU5BU5D_t6_165* L_13 = V_1;
		NullCheck(L_13);
		if ((((int32_t)L_12) < ((int32_t)(((int32_t)((int32_t)(((Array_t *)L_13)->max_length)))))))
		{
			goto IL_0017;
		}
	}
	{
		return;
	}
}
// System.Void UnityEngine.GUILayoutGroup::ApplyStyleSettings(UnityEngine.GUIStyle)
extern "C" void GUILayoutGroup_ApplyStyleSettings_m6_1186 (GUILayoutGroup_t6_169 * __this, GUIStyle_t6_166 * ___style, const MethodInfo* method)
{
	RectOffset_t6_172 * V_0 = {0};
	{
		GUIStyle_t6_166 * L_0 = ___style;
		GUILayoutEntry_ApplyStyleSettings_m6_1180(__this, L_0, /*hidden argument*/NULL);
		GUIStyle_t6_166 * L_1 = ___style;
		NullCheck(L_1);
		RectOffset_t6_172 * L_2 = GUIStyle_get_margin_m6_1303(L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		RectOffset_t6_172 * L_3 = (__this->___m_Margin_26);
		RectOffset_t6_172 * L_4 = V_0;
		NullCheck(L_4);
		int32_t L_5 = RectOffset_get_left_m6_1284(L_4, /*hidden argument*/NULL);
		NullCheck(L_3);
		RectOffset_set_left_m6_1285(L_3, L_5, /*hidden argument*/NULL);
		RectOffset_t6_172 * L_6 = (__this->___m_Margin_26);
		RectOffset_t6_172 * L_7 = V_0;
		NullCheck(L_7);
		int32_t L_8 = RectOffset_get_right_m6_1286(L_7, /*hidden argument*/NULL);
		NullCheck(L_6);
		RectOffset_set_right_m6_1287(L_6, L_8, /*hidden argument*/NULL);
		RectOffset_t6_172 * L_9 = (__this->___m_Margin_26);
		RectOffset_t6_172 * L_10 = V_0;
		NullCheck(L_10);
		int32_t L_11 = RectOffset_get_top_m6_1288(L_10, /*hidden argument*/NULL);
		NullCheck(L_9);
		RectOffset_set_top_m6_1289(L_9, L_11, /*hidden argument*/NULL);
		RectOffset_t6_172 * L_12 = (__this->___m_Margin_26);
		RectOffset_t6_172 * L_13 = V_0;
		NullCheck(L_13);
		int32_t L_14 = RectOffset_get_bottom_m6_1290(L_13, /*hidden argument*/NULL);
		NullCheck(L_12);
		RectOffset_set_bottom_m6_1291(L_12, L_14, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.GUILayoutGroup::ResetCursor()
extern "C" void GUILayoutGroup_ResetCursor_m6_1187 (GUILayoutGroup_t6_169 * __this, const MethodInfo* method)
{
	{
		__this->___m_Cursor_17 = 0;
		return;
	}
}
// UnityEngine.GUILayoutEntry UnityEngine.GUILayoutGroup::GetNext()
extern TypeInfo* ObjectU5BU5D_t1_157_il2cpp_TypeInfo_var;
extern TypeInfo* Int32_t1_3_il2cpp_TypeInfo_var;
extern TypeInfo* EventType_t6_156_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* ArgumentException_t1_646_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2796;
extern Il2CppCodeGenString* _stringLiteral2797;
extern Il2CppCodeGenString* _stringLiteral2798;
extern Il2CppCodeGenString* _stringLiteral2799;
extern "C" GUILayoutEntry_t6_171 * GUILayoutGroup_GetNext_m6_1188 (GUILayoutGroup_t6_169 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ObjectU5BU5D_t1_157_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(19);
		Int32_t1_3_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(8);
		EventType_t6_156_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(980);
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(11);
		ArgumentException_t1_646_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4);
		_stringLiteral2796 = il2cpp_codegen_string_literal_from_index(2796);
		_stringLiteral2797 = il2cpp_codegen_string_literal_from_index(2797);
		_stringLiteral2798 = il2cpp_codegen_string_literal_from_index(2798);
		_stringLiteral2799 = il2cpp_codegen_string_literal_from_index(2799);
		s_Il2CppMethodIntialized = true;
	}
	GUILayoutEntry_t6_171 * V_0 = {0};
	{
		int32_t L_0 = (__this->___m_Cursor_17);
		List_1_t1_917 * L_1 = (__this->___entries_10);
		NullCheck(L_1);
		int32_t L_2 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(17 /* System.Int32 System.Collections.Generic.List`1<UnityEngine.GUILayoutEntry>::get_Count() */, L_1);
		if ((((int32_t)L_0) >= ((int32_t)L_2)))
		{
			goto IL_0038;
		}
	}
	{
		List_1_t1_917 * L_3 = (__this->___entries_10);
		int32_t L_4 = (__this->___m_Cursor_17);
		NullCheck(L_3);
		GUILayoutEntry_t6_171 * L_5 = (GUILayoutEntry_t6_171 *)VirtFuncInvoker1< GUILayoutEntry_t6_171 *, int32_t >::Invoke(28 /* !0 System.Collections.Generic.List`1<UnityEngine.GUILayoutEntry>::get_Item(System.Int32) */, L_3, L_4);
		V_0 = L_5;
		int32_t L_6 = (__this->___m_Cursor_17);
		__this->___m_Cursor_17 = ((int32_t)((int32_t)L_6+(int32_t)1));
		GUILayoutEntry_t6_171 * L_7 = V_0;
		return L_7;
	}

IL_0038:
	{
		ObjectU5BU5D_t1_157* L_8 = ((ObjectU5BU5D_t1_157*)SZArrayNew(ObjectU5BU5D_t1_157_il2cpp_TypeInfo_var, 7));
		NullCheck(L_8);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_8, 0);
		ArrayElementTypeCheck (L_8, _stringLiteral2796);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_8, 0, sizeof(Object_t *))) = (Object_t *)_stringLiteral2796;
		ObjectU5BU5D_t1_157* L_9 = L_8;
		int32_t L_10 = (__this->___m_Cursor_17);
		int32_t L_11 = L_10;
		Object_t * L_12 = Box(Int32_t1_3_il2cpp_TypeInfo_var, &L_11);
		NullCheck(L_9);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_9, 1);
		ArrayElementTypeCheck (L_9, L_12);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_9, 1, sizeof(Object_t *))) = (Object_t *)L_12;
		ObjectU5BU5D_t1_157* L_13 = L_9;
		NullCheck(L_13);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_13, 2);
		ArrayElementTypeCheck (L_13, _stringLiteral2797);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_13, 2, sizeof(Object_t *))) = (Object_t *)_stringLiteral2797;
		ObjectU5BU5D_t1_157* L_14 = L_13;
		List_1_t1_917 * L_15 = (__this->___entries_10);
		NullCheck(L_15);
		int32_t L_16 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(17 /* System.Int32 System.Collections.Generic.List`1<UnityEngine.GUILayoutEntry>::get_Count() */, L_15);
		int32_t L_17 = L_16;
		Object_t * L_18 = Box(Int32_t1_3_il2cpp_TypeInfo_var, &L_17);
		NullCheck(L_14);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_14, 3);
		ArrayElementTypeCheck (L_14, L_18);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_14, 3, sizeof(Object_t *))) = (Object_t *)L_18;
		ObjectU5BU5D_t1_157* L_19 = L_14;
		NullCheck(L_19);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_19, 4);
		ArrayElementTypeCheck (L_19, _stringLiteral2798);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_19, 4, sizeof(Object_t *))) = (Object_t *)_stringLiteral2798;
		ObjectU5BU5D_t1_157* L_20 = L_19;
		Event_t6_154 * L_21 = Event_get_current_m6_989(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_21);
		int32_t L_22 = Event_get_rawType_m6_1002(L_21, /*hidden argument*/NULL);
		int32_t L_23 = L_22;
		Object_t * L_24 = Box(EventType_t6_156_il2cpp_TypeInfo_var, &L_23);
		NullCheck(L_20);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_20, 5);
		ArrayElementTypeCheck (L_20, L_24);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_20, 5, sizeof(Object_t *))) = (Object_t *)L_24;
		ObjectU5BU5D_t1_157* L_25 = L_20;
		NullCheck(L_25);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_25, 6);
		ArrayElementTypeCheck (L_25, _stringLiteral2799);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_25, 6, sizeof(Object_t *))) = (Object_t *)_stringLiteral2799;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_26 = String_Concat_m1_421(NULL /*static, unused*/, L_25, /*hidden argument*/NULL);
		ArgumentException_t1_646 * L_27 = (ArgumentException_t1_646 *)il2cpp_codegen_object_new (ArgumentException_t1_646_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m1_4605(L_27, L_26, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_27);
	}
}
// System.Void UnityEngine.GUILayoutGroup::Add(UnityEngine.GUILayoutEntry)
extern "C" void GUILayoutGroup_Add_m6_1189 (GUILayoutGroup_t6_169 * __this, GUILayoutEntry_t6_171 * ___e, const MethodInfo* method)
{
	{
		List_1_t1_917 * L_0 = (__this->___entries_10);
		GUILayoutEntry_t6_171 * L_1 = ___e;
		NullCheck(L_0);
		VirtActionInvoker1< GUILayoutEntry_t6_171 * >::Invoke(19 /* System.Void System.Collections.Generic.List`1<UnityEngine.GUILayoutEntry>::Add(!0) */, L_0, L_1);
		return;
	}
}
// System.Void UnityEngine.GUILayoutGroup::CalcWidth()
extern TypeInfo* GUILayoutUtility_t6_170_il2cpp_TypeInfo_var;
extern TypeInfo* Mathf_t6_58_il2cpp_TypeInfo_var;
extern TypeInfo* Enumerator_t1_927_il2cpp_TypeInfo_var;
extern TypeInfo* IDisposable_t1_835_il2cpp_TypeInfo_var;
extern TypeInfo* GUIStyle_t6_166_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1_GetEnumerator_m1_5604_MethodInfo_var;
extern const MethodInfo* Enumerator_get_Current_m1_5605_MethodInfo_var;
extern const MethodInfo* Enumerator_MoveNext_m1_5606_MethodInfo_var;
extern "C" void GUILayoutGroup_CalcWidth_m6_1190 (GUILayoutGroup_t6_169 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GUILayoutUtility_t6_170_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(990);
		Mathf_t6_58_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(940);
		Enumerator_t1_927_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1001);
		IDisposable_t1_835_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(142);
		GUIStyle_t6_166_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(988);
		List_1_GetEnumerator_m1_5604_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483812);
		Enumerator_get_Current_m1_5605_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483813);
		Enumerator_MoveNext_m1_5606_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483814);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	bool V_2 = false;
	GUILayoutEntry_t6_171 * V_3 = {0};
	Enumerator_t1_927  V_4 = {0};
	RectOffset_t6_172 * V_5 = {0};
	int32_t V_6 = 0;
	GUILayoutEntry_t6_171 * V_7 = {0};
	Enumerator_t1_927  V_8 = {0};
	RectOffset_t6_172 * V_9 = {0};
	int32_t V_10 = 0;
	float V_11 = 0.0f;
	float V_12 = 0.0f;
	float V_13 = 0.0f;
	Exception_t1_33 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1_33 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	int32_t G_B22_0 = 0;
	int32_t G_B39_0 = 0;
	int32_t G_B39_1 = 0;
	GUILayoutGroup_t6_169 * G_B39_2 = {0};
	int32_t G_B38_0 = 0;
	int32_t G_B38_1 = 0;
	GUILayoutGroup_t6_169 * G_B38_2 = {0};
	int32_t G_B40_0 = 0;
	int32_t G_B40_1 = 0;
	int32_t G_B40_2 = 0;
	GUILayoutGroup_t6_169 * G_B40_3 = {0};
	{
		List_1_t1_917 * L_0 = (__this->___entries_10);
		NullCheck(L_0);
		int32_t L_1 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(17 /* System.Int32 System.Collections.Generic.List`1<UnityEngine.GUILayoutEntry>::get_Count() */, L_0);
		if (L_1)
		{
			goto IL_0033;
		}
	}
	{
		GUIStyle_t6_166 * L_2 = GUILayoutEntry_get_style_m6_1173(__this, /*hidden argument*/NULL);
		NullCheck(L_2);
		RectOffset_t6_172 * L_3 = GUIStyle_get_padding_m6_1304(L_2, /*hidden argument*/NULL);
		NullCheck(L_3);
		int32_t L_4 = RectOffset_get_horizontal_m6_1292(L_3, /*hidden argument*/NULL);
		float L_5 = (((float)((float)L_4)));
		V_13 = L_5;
		((GUILayoutEntry_t6_171 *)__this)->___minWidth_0 = L_5;
		float L_6 = V_13;
		((GUILayoutEntry_t6_171 *)__this)->___maxWidth_1 = L_6;
		return;
	}

IL_0033:
	{
		V_0 = 0;
		V_1 = 0;
		__this->___m_ChildMinWidth_22 = (0.0f);
		__this->___m_ChildMaxWidth_23 = (0.0f);
		__this->___m_StretchableCountX_18 = 0;
		V_2 = 1;
		bool L_7 = (__this->___isVertical_11);
		if (!L_7)
		{
			goto IL_016a;
		}
	}
	{
		List_1_t1_917 * L_8 = (__this->___entries_10);
		NullCheck(L_8);
		Enumerator_t1_927  L_9 = List_1_GetEnumerator_m1_5604(L_8, /*hidden argument*/List_1_GetEnumerator_m1_5604_MethodInfo_var);
		V_4 = L_9;
	}

IL_006e:
	try
	{ // begin try (depth: 1)
		{
			goto IL_0125;
		}

IL_0073:
		{
			GUILayoutEntry_t6_171 * L_10 = Enumerator_get_Current_m1_5605((&V_4), /*hidden argument*/Enumerator_get_Current_m1_5605_MethodInfo_var);
			V_3 = L_10;
			GUILayoutEntry_t6_171 * L_11 = V_3;
			NullCheck(L_11);
			VirtActionInvoker0::Invoke(5 /* System.Void UnityEngine.GUILayoutEntry::CalcWidth() */, L_11);
			GUILayoutEntry_t6_171 * L_12 = V_3;
			NullCheck(L_12);
			RectOffset_t6_172 * L_13 = (RectOffset_t6_172 *)VirtFuncInvoker0< RectOffset_t6_172 * >::Invoke(4 /* UnityEngine.RectOffset UnityEngine.GUILayoutEntry::get_margin() */, L_12);
			V_5 = L_13;
			GUILayoutEntry_t6_171 * L_14 = V_3;
			NullCheck(L_14);
			GUIStyle_t6_166 * L_15 = GUILayoutEntry_get_style_m6_1173(L_14, /*hidden argument*/NULL);
			IL2CPP_RUNTIME_CLASS_INIT(GUILayoutUtility_t6_170_il2cpp_TypeInfo_var);
			GUIStyle_t6_166 * L_16 = GUILayoutUtility_get_spaceStyle_m6_1166(NULL /*static, unused*/, /*hidden argument*/NULL);
			if ((((Object_t*)(GUIStyle_t6_166 *)L_15) == ((Object_t*)(GUIStyle_t6_166 *)L_16)))
			{
				goto IL_0112;
			}
		}

IL_0099:
		{
			bool L_17 = V_2;
			if (L_17)
			{
				goto IL_00c0;
			}
		}

IL_009f:
		{
			RectOffset_t6_172 * L_18 = V_5;
			NullCheck(L_18);
			int32_t L_19 = RectOffset_get_left_m6_1284(L_18, /*hidden argument*/NULL);
			int32_t L_20 = V_0;
			IL2CPP_RUNTIME_CLASS_INIT(Mathf_t6_58_il2cpp_TypeInfo_var);
			int32_t L_21 = Mathf_Min_m6_382(NULL /*static, unused*/, L_19, L_20, /*hidden argument*/NULL);
			V_0 = L_21;
			RectOffset_t6_172 * L_22 = V_5;
			NullCheck(L_22);
			int32_t L_23 = RectOffset_get_right_m6_1286(L_22, /*hidden argument*/NULL);
			int32_t L_24 = V_1;
			int32_t L_25 = Mathf_Min_m6_382(NULL /*static, unused*/, L_23, L_24, /*hidden argument*/NULL);
			V_1 = L_25;
			goto IL_00d2;
		}

IL_00c0:
		{
			RectOffset_t6_172 * L_26 = V_5;
			NullCheck(L_26);
			int32_t L_27 = RectOffset_get_left_m6_1284(L_26, /*hidden argument*/NULL);
			V_0 = L_27;
			RectOffset_t6_172 * L_28 = V_5;
			NullCheck(L_28);
			int32_t L_29 = RectOffset_get_right_m6_1286(L_28, /*hidden argument*/NULL);
			V_1 = L_29;
			V_2 = 0;
		}

IL_00d2:
		{
			GUILayoutEntry_t6_171 * L_30 = V_3;
			NullCheck(L_30);
			float L_31 = (L_30->___minWidth_0);
			RectOffset_t6_172 * L_32 = V_5;
			NullCheck(L_32);
			int32_t L_33 = RectOffset_get_horizontal_m6_1292(L_32, /*hidden argument*/NULL);
			float L_34 = (__this->___m_ChildMinWidth_22);
			IL2CPP_RUNTIME_CLASS_INIT(Mathf_t6_58_il2cpp_TypeInfo_var);
			float L_35 = Mathf_Max_m6_383(NULL /*static, unused*/, ((float)((float)L_31+(float)(((float)((float)L_33))))), L_34, /*hidden argument*/NULL);
			__this->___m_ChildMinWidth_22 = L_35;
			GUILayoutEntry_t6_171 * L_36 = V_3;
			NullCheck(L_36);
			float L_37 = (L_36->___maxWidth_1);
			RectOffset_t6_172 * L_38 = V_5;
			NullCheck(L_38);
			int32_t L_39 = RectOffset_get_horizontal_m6_1292(L_38, /*hidden argument*/NULL);
			float L_40 = (__this->___m_ChildMaxWidth_23);
			float L_41 = Mathf_Max_m6_383(NULL /*static, unused*/, ((float)((float)L_37+(float)(((float)((float)L_39))))), L_40, /*hidden argument*/NULL);
			__this->___m_ChildMaxWidth_23 = L_41;
		}

IL_0112:
		{
			int32_t L_42 = (__this->___m_StretchableCountX_18);
			GUILayoutEntry_t6_171 * L_43 = V_3;
			NullCheck(L_43);
			int32_t L_44 = (L_43->___stretchWidth_5);
			__this->___m_StretchableCountX_18 = ((int32_t)((int32_t)L_42+(int32_t)L_44));
		}

IL_0125:
		{
			bool L_45 = Enumerator_MoveNext_m1_5606((&V_4), /*hidden argument*/Enumerator_MoveNext_m1_5606_MethodInfo_var);
			if (L_45)
			{
				goto IL_0073;
			}
		}

IL_0131:
		{
			IL2CPP_LEAVE(0x143, FINALLY_0136);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1_33 *)e.ex;
		goto FINALLY_0136;
	}

FINALLY_0136:
	{ // begin finally (depth: 1)
		Enumerator_t1_927  L_46 = V_4;
		Enumerator_t1_927  L_47 = L_46;
		Object_t * L_48 = Box(Enumerator_t1_927_il2cpp_TypeInfo_var, &L_47);
		NullCheck(L_48);
		InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1_835_il2cpp_TypeInfo_var, L_48);
		IL2CPP_END_FINALLY(310)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(310)
	{
		IL2CPP_JUMP_TBL(0x143, IL_0143)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1_33 *)
	}

IL_0143:
	{
		float L_49 = (__this->___m_ChildMinWidth_22);
		int32_t L_50 = V_0;
		int32_t L_51 = V_1;
		__this->___m_ChildMinWidth_22 = ((float)((float)L_49-(float)(((float)((float)((int32_t)((int32_t)L_50+(int32_t)L_51)))))));
		float L_52 = (__this->___m_ChildMaxWidth_23);
		int32_t L_53 = V_0;
		int32_t L_54 = V_1;
		__this->___m_ChildMaxWidth_23 = ((float)((float)L_52-(float)(((float)((float)((int32_t)((int32_t)L_53+(int32_t)L_54)))))));
		goto IL_02ea;
	}

IL_016a:
	{
		V_6 = 0;
		List_1_t1_917 * L_55 = (__this->___entries_10);
		NullCheck(L_55);
		Enumerator_t1_927  L_56 = List_1_GetEnumerator_m1_5604(L_55, /*hidden argument*/List_1_GetEnumerator_m1_5604_MethodInfo_var);
		V_8 = L_56;
	}

IL_017a:
	try
	{ // begin try (depth: 1)
		{
			goto IL_0273;
		}

IL_017f:
		{
			GUILayoutEntry_t6_171 * L_57 = Enumerator_get_Current_m1_5605((&V_8), /*hidden argument*/Enumerator_get_Current_m1_5605_MethodInfo_var);
			V_7 = L_57;
			GUILayoutEntry_t6_171 * L_58 = V_7;
			NullCheck(L_58);
			VirtActionInvoker0::Invoke(5 /* System.Void UnityEngine.GUILayoutEntry::CalcWidth() */, L_58);
			GUILayoutEntry_t6_171 * L_59 = V_7;
			NullCheck(L_59);
			RectOffset_t6_172 * L_60 = (RectOffset_t6_172 *)VirtFuncInvoker0< RectOffset_t6_172 * >::Invoke(4 /* UnityEngine.RectOffset UnityEngine.GUILayoutEntry::get_margin() */, L_59);
			V_9 = L_60;
			GUILayoutEntry_t6_171 * L_61 = V_7;
			NullCheck(L_61);
			GUIStyle_t6_166 * L_62 = GUILayoutEntry_get_style_m6_1173(L_61, /*hidden argument*/NULL);
			IL2CPP_RUNTIME_CLASS_INIT(GUILayoutUtility_t6_170_il2cpp_TypeInfo_var);
			GUIStyle_t6_166 * L_63 = GUILayoutUtility_get_spaceStyle_m6_1166(NULL /*static, unused*/, /*hidden argument*/NULL);
			if ((((Object_t*)(GUIStyle_t6_166 *)L_62) == ((Object_t*)(GUIStyle_t6_166 *)L_63)))
			{
				goto IL_0237;
			}
		}

IL_01a9:
		{
			bool L_64 = V_2;
			if (L_64)
			{
				goto IL_01d2;
			}
		}

IL_01af:
		{
			int32_t L_65 = V_6;
			RectOffset_t6_172 * L_66 = V_9;
			NullCheck(L_66);
			int32_t L_67 = RectOffset_get_left_m6_1284(L_66, /*hidden argument*/NULL);
			if ((((int32_t)L_65) <= ((int32_t)L_67)))
			{
				goto IL_01c4;
			}
		}

IL_01bd:
		{
			int32_t L_68 = V_6;
			G_B22_0 = L_68;
			goto IL_01cb;
		}

IL_01c4:
		{
			RectOffset_t6_172 * L_69 = V_9;
			NullCheck(L_69);
			int32_t L_70 = RectOffset_get_left_m6_1284(L_69, /*hidden argument*/NULL);
			G_B22_0 = L_70;
		}

IL_01cb:
		{
			V_10 = G_B22_0;
			goto IL_01d7;
		}

IL_01d2:
		{
			V_10 = 0;
			V_2 = 0;
		}

IL_01d7:
		{
			float L_71 = (__this->___m_ChildMinWidth_22);
			GUILayoutEntry_t6_171 * L_72 = V_7;
			NullCheck(L_72);
			float L_73 = (L_72->___minWidth_0);
			float L_74 = (__this->___spacing_13);
			int32_t L_75 = V_10;
			__this->___m_ChildMinWidth_22 = ((float)((float)L_71+(float)((float)((float)((float)((float)L_73+(float)L_74))+(float)(((float)((float)L_75)))))));
			float L_76 = (__this->___m_ChildMaxWidth_23);
			GUILayoutEntry_t6_171 * L_77 = V_7;
			NullCheck(L_77);
			float L_78 = (L_77->___maxWidth_1);
			float L_79 = (__this->___spacing_13);
			int32_t L_80 = V_10;
			__this->___m_ChildMaxWidth_23 = ((float)((float)L_76+(float)((float)((float)((float)((float)L_78+(float)L_79))+(float)(((float)((float)L_80)))))));
			RectOffset_t6_172 * L_81 = V_9;
			NullCheck(L_81);
			int32_t L_82 = RectOffset_get_right_m6_1286(L_81, /*hidden argument*/NULL);
			V_6 = L_82;
			int32_t L_83 = (__this->___m_StretchableCountX_18);
			GUILayoutEntry_t6_171 * L_84 = V_7;
			NullCheck(L_84);
			int32_t L_85 = (L_84->___stretchWidth_5);
			__this->___m_StretchableCountX_18 = ((int32_t)((int32_t)L_83+(int32_t)L_85));
			goto IL_0273;
		}

IL_0237:
		{
			float L_86 = (__this->___m_ChildMinWidth_22);
			GUILayoutEntry_t6_171 * L_87 = V_7;
			NullCheck(L_87);
			float L_88 = (L_87->___minWidth_0);
			__this->___m_ChildMinWidth_22 = ((float)((float)L_86+(float)L_88));
			float L_89 = (__this->___m_ChildMaxWidth_23);
			GUILayoutEntry_t6_171 * L_90 = V_7;
			NullCheck(L_90);
			float L_91 = (L_90->___maxWidth_1);
			__this->___m_ChildMaxWidth_23 = ((float)((float)L_89+(float)L_91));
			int32_t L_92 = (__this->___m_StretchableCountX_18);
			GUILayoutEntry_t6_171 * L_93 = V_7;
			NullCheck(L_93);
			int32_t L_94 = (L_93->___stretchWidth_5);
			__this->___m_StretchableCountX_18 = ((int32_t)((int32_t)L_92+(int32_t)L_94));
		}

IL_0273:
		{
			bool L_95 = Enumerator_MoveNext_m1_5606((&V_8), /*hidden argument*/Enumerator_MoveNext_m1_5606_MethodInfo_var);
			if (L_95)
			{
				goto IL_017f;
			}
		}

IL_027f:
		{
			IL2CPP_LEAVE(0x291, FINALLY_0284);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1_33 *)e.ex;
		goto FINALLY_0284;
	}

FINALLY_0284:
	{ // begin finally (depth: 1)
		Enumerator_t1_927  L_96 = V_8;
		Enumerator_t1_927  L_97 = L_96;
		Object_t * L_98 = Box(Enumerator_t1_927_il2cpp_TypeInfo_var, &L_97);
		NullCheck(L_98);
		InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1_835_il2cpp_TypeInfo_var, L_98);
		IL2CPP_END_FINALLY(644)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(644)
	{
		IL2CPP_JUMP_TBL(0x291, IL_0291)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1_33 *)
	}

IL_0291:
	{
		float L_99 = (__this->___m_ChildMinWidth_22);
		float L_100 = (__this->___spacing_13);
		__this->___m_ChildMinWidth_22 = ((float)((float)L_99-(float)L_100));
		float L_101 = (__this->___m_ChildMaxWidth_23);
		float L_102 = (__this->___spacing_13);
		__this->___m_ChildMaxWidth_23 = ((float)((float)L_101-(float)L_102));
		List_1_t1_917 * L_103 = (__this->___entries_10);
		NullCheck(L_103);
		int32_t L_104 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(17 /* System.Int32 System.Collections.Generic.List`1<UnityEngine.GUILayoutEntry>::get_Count() */, L_103);
		if (!L_104)
		{
			goto IL_02e6;
		}
	}
	{
		List_1_t1_917 * L_105 = (__this->___entries_10);
		NullCheck(L_105);
		GUILayoutEntry_t6_171 * L_106 = (GUILayoutEntry_t6_171 *)VirtFuncInvoker1< GUILayoutEntry_t6_171 *, int32_t >::Invoke(28 /* !0 System.Collections.Generic.List`1<UnityEngine.GUILayoutEntry>::get_Item(System.Int32) */, L_105, 0);
		NullCheck(L_106);
		RectOffset_t6_172 * L_107 = (RectOffset_t6_172 *)VirtFuncInvoker0< RectOffset_t6_172 * >::Invoke(4 /* UnityEngine.RectOffset UnityEngine.GUILayoutEntry::get_margin() */, L_106);
		NullCheck(L_107);
		int32_t L_108 = RectOffset_get_left_m6_1284(L_107, /*hidden argument*/NULL);
		V_0 = L_108;
		int32_t L_109 = V_6;
		V_1 = L_109;
		goto IL_02ea;
	}

IL_02e6:
	{
		int32_t L_110 = 0;
		V_1 = L_110;
		V_0 = L_110;
	}

IL_02ea:
	{
		V_11 = (0.0f);
		V_12 = (0.0f);
		GUIStyle_t6_166 * L_111 = GUILayoutEntry_get_style_m6_1173(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(GUIStyle_t6_166_il2cpp_TypeInfo_var);
		GUIStyle_t6_166 * L_112 = GUIStyle_get_none_m6_1316(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((!(((Object_t*)(GUIStyle_t6_166 *)L_111) == ((Object_t*)(GUIStyle_t6_166 *)L_112))))
		{
			goto IL_0313;
		}
	}
	{
		bool L_113 = (__this->___m_UserSpecifiedWidth_20);
		if (!L_113)
		{
			goto IL_034a;
		}
	}

IL_0313:
	{
		GUIStyle_t6_166 * L_114 = GUILayoutEntry_get_style_m6_1173(__this, /*hidden argument*/NULL);
		NullCheck(L_114);
		RectOffset_t6_172 * L_115 = GUIStyle_get_padding_m6_1304(L_114, /*hidden argument*/NULL);
		NullCheck(L_115);
		int32_t L_116 = RectOffset_get_left_m6_1284(L_115, /*hidden argument*/NULL);
		int32_t L_117 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t6_58_il2cpp_TypeInfo_var);
		int32_t L_118 = Mathf_Max_m6_384(NULL /*static, unused*/, L_116, L_117, /*hidden argument*/NULL);
		V_11 = (((float)((float)L_118)));
		GUIStyle_t6_166 * L_119 = GUILayoutEntry_get_style_m6_1173(__this, /*hidden argument*/NULL);
		NullCheck(L_119);
		RectOffset_t6_172 * L_120 = GUIStyle_get_padding_m6_1304(L_119, /*hidden argument*/NULL);
		NullCheck(L_120);
		int32_t L_121 = RectOffset_get_right_m6_1286(L_120, /*hidden argument*/NULL);
		int32_t L_122 = V_1;
		int32_t L_123 = Mathf_Max_m6_384(NULL /*static, unused*/, L_121, L_122, /*hidden argument*/NULL);
		V_12 = (((float)((float)L_123)));
		goto IL_036c;
	}

IL_034a:
	{
		RectOffset_t6_172 * L_124 = (__this->___m_Margin_26);
		int32_t L_125 = V_0;
		NullCheck(L_124);
		RectOffset_set_left_m6_1285(L_124, L_125, /*hidden argument*/NULL);
		RectOffset_t6_172 * L_126 = (__this->___m_Margin_26);
		int32_t L_127 = V_1;
		NullCheck(L_126);
		RectOffset_set_right_m6_1287(L_126, L_127, /*hidden argument*/NULL);
		float L_128 = (0.0f);
		V_12 = L_128;
		V_11 = L_128;
	}

IL_036c:
	{
		float L_129 = (((GUILayoutEntry_t6_171 *)__this)->___minWidth_0);
		float L_130 = (__this->___m_ChildMinWidth_22);
		float L_131 = V_11;
		float L_132 = V_12;
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t6_58_il2cpp_TypeInfo_var);
		float L_133 = Mathf_Max_m6_383(NULL /*static, unused*/, L_129, ((float)((float)((float)((float)L_130+(float)L_131))+(float)L_132)), /*hidden argument*/NULL);
		((GUILayoutEntry_t6_171 *)__this)->___minWidth_0 = L_133;
		float L_134 = (((GUILayoutEntry_t6_171 *)__this)->___maxWidth_1);
		if ((!(((float)L_134) == ((float)(0.0f)))))
		{
			goto IL_03db;
		}
	}
	{
		int32_t L_135 = (((GUILayoutEntry_t6_171 *)__this)->___stretchWidth_5);
		int32_t L_136 = (__this->___m_StretchableCountX_18);
		GUIStyle_t6_166 * L_137 = GUILayoutEntry_get_style_m6_1173(__this, /*hidden argument*/NULL);
		NullCheck(L_137);
		bool L_138 = GUIStyle_get_stretchWidth_m6_1344(L_137, /*hidden argument*/NULL);
		G_B38_0 = L_136;
		G_B38_1 = L_135;
		G_B38_2 = __this;
		if (!L_138)
		{
			G_B39_0 = L_136;
			G_B39_1 = L_135;
			G_B39_2 = __this;
			goto IL_03bc;
		}
	}
	{
		G_B40_0 = 1;
		G_B40_1 = G_B38_0;
		G_B40_2 = G_B38_1;
		G_B40_3 = G_B38_2;
		goto IL_03bd;
	}

IL_03bc:
	{
		G_B40_0 = 0;
		G_B40_1 = G_B39_0;
		G_B40_2 = G_B39_1;
		G_B40_3 = G_B39_2;
	}

IL_03bd:
	{
		NullCheck(G_B40_3);
		((GUILayoutEntry_t6_171 *)G_B40_3)->___stretchWidth_5 = ((int32_t)((int32_t)G_B40_2+(int32_t)((int32_t)((int32_t)G_B40_1+(int32_t)G_B40_0))));
		float L_139 = (__this->___m_ChildMaxWidth_23);
		float L_140 = V_11;
		float L_141 = V_12;
		((GUILayoutEntry_t6_171 *)__this)->___maxWidth_1 = ((float)((float)((float)((float)L_139+(float)L_140))+(float)L_141));
		goto IL_03e2;
	}

IL_03db:
	{
		((GUILayoutEntry_t6_171 *)__this)->___stretchWidth_5 = 0;
	}

IL_03e2:
	{
		float L_142 = (((GUILayoutEntry_t6_171 *)__this)->___maxWidth_1);
		float L_143 = (((GUILayoutEntry_t6_171 *)__this)->___minWidth_0);
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t6_58_il2cpp_TypeInfo_var);
		float L_144 = Mathf_Max_m6_383(NULL /*static, unused*/, L_142, L_143, /*hidden argument*/NULL);
		((GUILayoutEntry_t6_171 *)__this)->___maxWidth_1 = L_144;
		GUIStyle_t6_166 * L_145 = GUILayoutEntry_get_style_m6_1173(__this, /*hidden argument*/NULL);
		NullCheck(L_145);
		float L_146 = GUIStyle_get_fixedWidth_m6_1342(L_145, /*hidden argument*/NULL);
		if ((((float)L_146) == ((float)(0.0f))))
		{
			goto IL_0431;
		}
	}
	{
		GUIStyle_t6_166 * L_147 = GUILayoutEntry_get_style_m6_1173(__this, /*hidden argument*/NULL);
		NullCheck(L_147);
		float L_148 = GUIStyle_get_fixedWidth_m6_1342(L_147, /*hidden argument*/NULL);
		float L_149 = L_148;
		V_13 = L_149;
		((GUILayoutEntry_t6_171 *)__this)->___minWidth_0 = L_149;
		float L_150 = V_13;
		((GUILayoutEntry_t6_171 *)__this)->___maxWidth_1 = L_150;
		((GUILayoutEntry_t6_171 *)__this)->___stretchWidth_5 = 0;
	}

IL_0431:
	{
		return;
	}
}
// System.Void UnityEngine.GUILayoutGroup::SetHorizontal(System.Single,System.Single)
extern TypeInfo* GUIStyle_t6_166_il2cpp_TypeInfo_var;
extern TypeInfo* Mathf_t6_58_il2cpp_TypeInfo_var;
extern TypeInfo* Enumerator_t1_927_il2cpp_TypeInfo_var;
extern TypeInfo* IDisposable_t1_835_il2cpp_TypeInfo_var;
extern TypeInfo* GUILayoutUtility_t6_170_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1_GetEnumerator_m1_5604_MethodInfo_var;
extern const MethodInfo* Enumerator_get_Current_m1_5605_MethodInfo_var;
extern const MethodInfo* Enumerator_MoveNext_m1_5606_MethodInfo_var;
extern "C" void GUILayoutGroup_SetHorizontal_m6_1191 (GUILayoutGroup_t6_169 * __this, float ___x, float ___width, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GUIStyle_t6_166_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(988);
		Mathf_t6_58_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(940);
		Enumerator_t1_927_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1001);
		IDisposable_t1_835_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(142);
		GUILayoutUtility_t6_170_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(990);
		List_1_GetEnumerator_m1_5604_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483812);
		Enumerator_get_Current_m1_5605_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483813);
		Enumerator_MoveNext_m1_5606_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483814);
		s_Il2CppMethodIntialized = true;
	}
	RectOffset_t6_172 * V_0 = {0};
	GUILayoutEntry_t6_171 * V_1 = {0};
	Enumerator_t1_927  V_2 = {0};
	float V_3 = 0.0f;
	float V_4 = 0.0f;
	float V_5 = 0.0f;
	float V_6 = 0.0f;
	float V_7 = 0.0f;
	GUILayoutEntry_t6_171 * V_8 = {0};
	Enumerator_t1_927  V_9 = {0};
	float V_10 = 0.0f;
	float V_11 = 0.0f;
	float V_12 = 0.0f;
	float V_13 = 0.0f;
	float V_14 = 0.0f;
	int32_t V_15 = 0;
	bool V_16 = false;
	GUILayoutEntry_t6_171 * V_17 = {0};
	Enumerator_t1_927  V_18 = {0};
	float V_19 = 0.0f;
	int32_t V_20 = 0;
	int32_t V_21 = 0;
	Exception_t1_33 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1_33 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	int32_t G_B39_0 = 0;
	{
		float L_0 = ___x;
		float L_1 = ___width;
		GUILayoutEntry_SetHorizontal_m6_1178(__this, L_0, L_1, /*hidden argument*/NULL);
		bool L_2 = (__this->___resetCoords_12);
		if (!L_2)
		{
			goto IL_001a;
		}
	}
	{
		___x = (0.0f);
	}

IL_001a:
	{
		GUIStyle_t6_166 * L_3 = GUILayoutEntry_get_style_m6_1173(__this, /*hidden argument*/NULL);
		NullCheck(L_3);
		RectOffset_t6_172 * L_4 = GUIStyle_get_padding_m6_1304(L_3, /*hidden argument*/NULL);
		V_0 = L_4;
		bool L_5 = (__this->___isVertical_11);
		if (!L_5)
		{
			goto IL_01bb;
		}
	}
	{
		GUIStyle_t6_166 * L_6 = GUILayoutEntry_get_style_m6_1173(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(GUIStyle_t6_166_il2cpp_TypeInfo_var);
		GUIStyle_t6_166 * L_7 = GUIStyle_get_none_m6_1316(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((((Object_t*)(GUIStyle_t6_166 *)L_6) == ((Object_t*)(GUIStyle_t6_166 *)L_7)))
		{
			goto IL_00eb;
		}
	}
	{
		List_1_t1_917 * L_8 = (__this->___entries_10);
		NullCheck(L_8);
		Enumerator_t1_927  L_9 = List_1_GetEnumerator_m1_5604(L_8, /*hidden argument*/List_1_GetEnumerator_m1_5604_MethodInfo_var);
		V_2 = L_9;
	}

IL_004d:
	try
	{ // begin try (depth: 1)
		{
			goto IL_00c9;
		}

IL_0052:
		{
			GUILayoutEntry_t6_171 * L_10 = Enumerator_get_Current_m1_5605((&V_2), /*hidden argument*/Enumerator_get_Current_m1_5605_MethodInfo_var);
			V_1 = L_10;
			GUILayoutEntry_t6_171 * L_11 = V_1;
			NullCheck(L_11);
			RectOffset_t6_172 * L_12 = (RectOffset_t6_172 *)VirtFuncInvoker0< RectOffset_t6_172 * >::Invoke(4 /* UnityEngine.RectOffset UnityEngine.GUILayoutEntry::get_margin() */, L_11);
			NullCheck(L_12);
			int32_t L_13 = RectOffset_get_left_m6_1284(L_12, /*hidden argument*/NULL);
			RectOffset_t6_172 * L_14 = V_0;
			NullCheck(L_14);
			int32_t L_15 = RectOffset_get_left_m6_1284(L_14, /*hidden argument*/NULL);
			IL2CPP_RUNTIME_CLASS_INIT(Mathf_t6_58_il2cpp_TypeInfo_var);
			int32_t L_16 = Mathf_Max_m6_384(NULL /*static, unused*/, L_13, L_15, /*hidden argument*/NULL);
			V_3 = (((float)((float)L_16)));
			float L_17 = ___x;
			float L_18 = V_3;
			V_4 = ((float)((float)L_17+(float)L_18));
			float L_19 = ___width;
			GUILayoutEntry_t6_171 * L_20 = V_1;
			NullCheck(L_20);
			RectOffset_t6_172 * L_21 = (RectOffset_t6_172 *)VirtFuncInvoker0< RectOffset_t6_172 * >::Invoke(4 /* UnityEngine.RectOffset UnityEngine.GUILayoutEntry::get_margin() */, L_20);
			NullCheck(L_21);
			int32_t L_22 = RectOffset_get_right_m6_1286(L_21, /*hidden argument*/NULL);
			RectOffset_t6_172 * L_23 = V_0;
			NullCheck(L_23);
			int32_t L_24 = RectOffset_get_right_m6_1286(L_23, /*hidden argument*/NULL);
			int32_t L_25 = Mathf_Max_m6_384(NULL /*static, unused*/, L_22, L_24, /*hidden argument*/NULL);
			float L_26 = V_3;
			V_5 = ((float)((float)((float)((float)L_19-(float)(((float)((float)L_25)))))-(float)L_26));
			GUILayoutEntry_t6_171 * L_27 = V_1;
			NullCheck(L_27);
			int32_t L_28 = (L_27->___stretchWidth_5);
			if (!L_28)
			{
				goto IL_00ae;
			}
		}

IL_009f:
		{
			GUILayoutEntry_t6_171 * L_29 = V_1;
			float L_30 = V_4;
			float L_31 = V_5;
			NullCheck(L_29);
			VirtActionInvoker2< float, float >::Invoke(7 /* System.Void UnityEngine.GUILayoutEntry::SetHorizontal(System.Single,System.Single) */, L_29, L_30, L_31);
			goto IL_00c9;
		}

IL_00ae:
		{
			GUILayoutEntry_t6_171 * L_32 = V_1;
			float L_33 = V_4;
			float L_34 = V_5;
			GUILayoutEntry_t6_171 * L_35 = V_1;
			NullCheck(L_35);
			float L_36 = (L_35->___minWidth_0);
			GUILayoutEntry_t6_171 * L_37 = V_1;
			NullCheck(L_37);
			float L_38 = (L_37->___maxWidth_1);
			IL2CPP_RUNTIME_CLASS_INIT(Mathf_t6_58_il2cpp_TypeInfo_var);
			float L_39 = Mathf_Clamp_m6_389(NULL /*static, unused*/, L_34, L_36, L_38, /*hidden argument*/NULL);
			NullCheck(L_32);
			VirtActionInvoker2< float, float >::Invoke(7 /* System.Void UnityEngine.GUILayoutEntry::SetHorizontal(System.Single,System.Single) */, L_32, L_33, L_39);
		}

IL_00c9:
		{
			bool L_40 = Enumerator_MoveNext_m1_5606((&V_2), /*hidden argument*/Enumerator_MoveNext_m1_5606_MethodInfo_var);
			if (L_40)
			{
				goto IL_0052;
			}
		}

IL_00d5:
		{
			IL2CPP_LEAVE(0xE6, FINALLY_00da);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1_33 *)e.ex;
		goto FINALLY_00da;
	}

FINALLY_00da:
	{ // begin finally (depth: 1)
		Enumerator_t1_927  L_41 = V_2;
		Enumerator_t1_927  L_42 = L_41;
		Object_t * L_43 = Box(Enumerator_t1_927_il2cpp_TypeInfo_var, &L_42);
		NullCheck(L_43);
		InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1_835_il2cpp_TypeInfo_var, L_43);
		IL2CPP_END_FINALLY(218)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(218)
	{
		IL2CPP_JUMP_TBL(0xE6, IL_00e6)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1_33 *)
	}

IL_00e6:
	{
		goto IL_01b6;
	}

IL_00eb:
	{
		float L_44 = ___x;
		RectOffset_t6_172 * L_45 = (RectOffset_t6_172 *)VirtFuncInvoker0< RectOffset_t6_172 * >::Invoke(4 /* UnityEngine.RectOffset UnityEngine.GUILayoutGroup::get_margin() */, __this);
		NullCheck(L_45);
		int32_t L_46 = RectOffset_get_left_m6_1284(L_45, /*hidden argument*/NULL);
		V_6 = ((float)((float)L_44-(float)(((float)((float)L_46)))));
		float L_47 = ___width;
		RectOffset_t6_172 * L_48 = (RectOffset_t6_172 *)VirtFuncInvoker0< RectOffset_t6_172 * >::Invoke(4 /* UnityEngine.RectOffset UnityEngine.GUILayoutGroup::get_margin() */, __this);
		NullCheck(L_48);
		int32_t L_49 = RectOffset_get_horizontal_m6_1292(L_48, /*hidden argument*/NULL);
		V_7 = ((float)((float)L_47+(float)(((float)((float)L_49)))));
		List_1_t1_917 * L_50 = (__this->___entries_10);
		NullCheck(L_50);
		Enumerator_t1_927  L_51 = List_1_GetEnumerator_m1_5604(L_50, /*hidden argument*/List_1_GetEnumerator_m1_5604_MethodInfo_var);
		V_9 = L_51;
	}

IL_0118:
	try
	{ // begin try (depth: 1)
		{
			goto IL_0198;
		}

IL_011d:
		{
			GUILayoutEntry_t6_171 * L_52 = Enumerator_get_Current_m1_5605((&V_9), /*hidden argument*/Enumerator_get_Current_m1_5605_MethodInfo_var);
			V_8 = L_52;
			GUILayoutEntry_t6_171 * L_53 = V_8;
			NullCheck(L_53);
			int32_t L_54 = (L_53->___stretchWidth_5);
			if (!L_54)
			{
				goto IL_015e;
			}
		}

IL_0132:
		{
			GUILayoutEntry_t6_171 * L_55 = V_8;
			float L_56 = V_6;
			GUILayoutEntry_t6_171 * L_57 = V_8;
			NullCheck(L_57);
			RectOffset_t6_172 * L_58 = (RectOffset_t6_172 *)VirtFuncInvoker0< RectOffset_t6_172 * >::Invoke(4 /* UnityEngine.RectOffset UnityEngine.GUILayoutEntry::get_margin() */, L_57);
			NullCheck(L_58);
			int32_t L_59 = RectOffset_get_left_m6_1284(L_58, /*hidden argument*/NULL);
			float L_60 = V_7;
			GUILayoutEntry_t6_171 * L_61 = V_8;
			NullCheck(L_61);
			RectOffset_t6_172 * L_62 = (RectOffset_t6_172 *)VirtFuncInvoker0< RectOffset_t6_172 * >::Invoke(4 /* UnityEngine.RectOffset UnityEngine.GUILayoutEntry::get_margin() */, L_61);
			NullCheck(L_62);
			int32_t L_63 = RectOffset_get_horizontal_m6_1292(L_62, /*hidden argument*/NULL);
			NullCheck(L_55);
			VirtActionInvoker2< float, float >::Invoke(7 /* System.Void UnityEngine.GUILayoutEntry::SetHorizontal(System.Single,System.Single) */, L_55, ((float)((float)L_56+(float)(((float)((float)L_59))))), ((float)((float)L_60-(float)(((float)((float)L_63))))));
			goto IL_0198;
		}

IL_015e:
		{
			GUILayoutEntry_t6_171 * L_64 = V_8;
			float L_65 = V_6;
			GUILayoutEntry_t6_171 * L_66 = V_8;
			NullCheck(L_66);
			RectOffset_t6_172 * L_67 = (RectOffset_t6_172 *)VirtFuncInvoker0< RectOffset_t6_172 * >::Invoke(4 /* UnityEngine.RectOffset UnityEngine.GUILayoutEntry::get_margin() */, L_66);
			NullCheck(L_67);
			int32_t L_68 = RectOffset_get_left_m6_1284(L_67, /*hidden argument*/NULL);
			float L_69 = V_7;
			GUILayoutEntry_t6_171 * L_70 = V_8;
			NullCheck(L_70);
			RectOffset_t6_172 * L_71 = (RectOffset_t6_172 *)VirtFuncInvoker0< RectOffset_t6_172 * >::Invoke(4 /* UnityEngine.RectOffset UnityEngine.GUILayoutEntry::get_margin() */, L_70);
			NullCheck(L_71);
			int32_t L_72 = RectOffset_get_horizontal_m6_1292(L_71, /*hidden argument*/NULL);
			GUILayoutEntry_t6_171 * L_73 = V_8;
			NullCheck(L_73);
			float L_74 = (L_73->___minWidth_0);
			GUILayoutEntry_t6_171 * L_75 = V_8;
			NullCheck(L_75);
			float L_76 = (L_75->___maxWidth_1);
			IL2CPP_RUNTIME_CLASS_INIT(Mathf_t6_58_il2cpp_TypeInfo_var);
			float L_77 = Mathf_Clamp_m6_389(NULL /*static, unused*/, ((float)((float)L_69-(float)(((float)((float)L_72))))), L_74, L_76, /*hidden argument*/NULL);
			NullCheck(L_64);
			VirtActionInvoker2< float, float >::Invoke(7 /* System.Void UnityEngine.GUILayoutEntry::SetHorizontal(System.Single,System.Single) */, L_64, ((float)((float)L_65+(float)(((float)((float)L_68))))), L_77);
		}

IL_0198:
		{
			bool L_78 = Enumerator_MoveNext_m1_5606((&V_9), /*hidden argument*/Enumerator_MoveNext_m1_5606_MethodInfo_var);
			if (L_78)
			{
				goto IL_011d;
			}
		}

IL_01a4:
		{
			IL2CPP_LEAVE(0x1B6, FINALLY_01a9);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1_33 *)e.ex;
		goto FINALLY_01a9;
	}

FINALLY_01a9:
	{ // begin finally (depth: 1)
		Enumerator_t1_927  L_79 = V_9;
		Enumerator_t1_927  L_80 = L_79;
		Object_t * L_81 = Box(Enumerator_t1_927_il2cpp_TypeInfo_var, &L_80);
		NullCheck(L_81);
		InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1_835_il2cpp_TypeInfo_var, L_81);
		IL2CPP_END_FINALLY(425)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(425)
	{
		IL2CPP_JUMP_TBL(0x1B6, IL_01b6)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1_33 *)
	}

IL_01b6:
	{
		goto IL_03b0;
	}

IL_01bb:
	{
		GUIStyle_t6_166 * L_82 = GUILayoutEntry_get_style_m6_1173(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(GUIStyle_t6_166_il2cpp_TypeInfo_var);
		GUIStyle_t6_166 * L_83 = GUIStyle_get_none_m6_1316(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((((Object_t*)(GUIStyle_t6_166 *)L_82) == ((Object_t*)(GUIStyle_t6_166 *)L_83)))
		{
			goto IL_0248;
		}
	}
	{
		RectOffset_t6_172 * L_84 = V_0;
		NullCheck(L_84);
		int32_t L_85 = RectOffset_get_left_m6_1284(L_84, /*hidden argument*/NULL);
		V_10 = (((float)((float)L_85)));
		RectOffset_t6_172 * L_86 = V_0;
		NullCheck(L_86);
		int32_t L_87 = RectOffset_get_right_m6_1286(L_86, /*hidden argument*/NULL);
		V_11 = (((float)((float)L_87)));
		List_1_t1_917 * L_88 = (__this->___entries_10);
		NullCheck(L_88);
		int32_t L_89 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(17 /* System.Int32 System.Collections.Generic.List`1<UnityEngine.GUILayoutEntry>::get_Count() */, L_88);
		if (!L_89)
		{
			goto IL_0239;
		}
	}
	{
		float L_90 = V_10;
		List_1_t1_917 * L_91 = (__this->___entries_10);
		NullCheck(L_91);
		GUILayoutEntry_t6_171 * L_92 = (GUILayoutEntry_t6_171 *)VirtFuncInvoker1< GUILayoutEntry_t6_171 *, int32_t >::Invoke(28 /* !0 System.Collections.Generic.List`1<UnityEngine.GUILayoutEntry>::get_Item(System.Int32) */, L_91, 0);
		NullCheck(L_92);
		RectOffset_t6_172 * L_93 = (RectOffset_t6_172 *)VirtFuncInvoker0< RectOffset_t6_172 * >::Invoke(4 /* UnityEngine.RectOffset UnityEngine.GUILayoutEntry::get_margin() */, L_92);
		NullCheck(L_93);
		int32_t L_94 = RectOffset_get_left_m6_1284(L_93, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t6_58_il2cpp_TypeInfo_var);
		float L_95 = Mathf_Max_m6_383(NULL /*static, unused*/, L_90, (((float)((float)L_94))), /*hidden argument*/NULL);
		V_10 = L_95;
		float L_96 = V_11;
		List_1_t1_917 * L_97 = (__this->___entries_10);
		List_1_t1_917 * L_98 = (__this->___entries_10);
		NullCheck(L_98);
		int32_t L_99 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(17 /* System.Int32 System.Collections.Generic.List`1<UnityEngine.GUILayoutEntry>::get_Count() */, L_98);
		NullCheck(L_97);
		GUILayoutEntry_t6_171 * L_100 = (GUILayoutEntry_t6_171 *)VirtFuncInvoker1< GUILayoutEntry_t6_171 *, int32_t >::Invoke(28 /* !0 System.Collections.Generic.List`1<UnityEngine.GUILayoutEntry>::get_Item(System.Int32) */, L_97, ((int32_t)((int32_t)L_99-(int32_t)1)));
		NullCheck(L_100);
		RectOffset_t6_172 * L_101 = (RectOffset_t6_172 *)VirtFuncInvoker0< RectOffset_t6_172 * >::Invoke(4 /* UnityEngine.RectOffset UnityEngine.GUILayoutEntry::get_margin() */, L_100);
		NullCheck(L_101);
		int32_t L_102 = RectOffset_get_right_m6_1286(L_101, /*hidden argument*/NULL);
		float L_103 = Mathf_Max_m6_383(NULL /*static, unused*/, L_96, (((float)((float)L_102))), /*hidden argument*/NULL);
		V_11 = L_103;
	}

IL_0239:
	{
		float L_104 = ___x;
		float L_105 = V_10;
		___x = ((float)((float)L_104+(float)L_105));
		float L_106 = ___width;
		float L_107 = V_11;
		float L_108 = V_10;
		___width = ((float)((float)L_106-(float)((float)((float)L_107+(float)L_108))));
	}

IL_0248:
	{
		float L_109 = ___width;
		float L_110 = (__this->___spacing_13);
		List_1_t1_917 * L_111 = (__this->___entries_10);
		NullCheck(L_111);
		int32_t L_112 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(17 /* System.Int32 System.Collections.Generic.List`1<UnityEngine.GUILayoutEntry>::get_Count() */, L_111);
		V_12 = ((float)((float)L_109-(float)((float)((float)L_110*(float)(((float)((float)((int32_t)((int32_t)L_112-(int32_t)1)))))))));
		V_13 = (0.0f);
		float L_113 = (__this->___m_ChildMinWidth_22);
		float L_114 = (__this->___m_ChildMaxWidth_23);
		if ((((float)L_113) == ((float)L_114)))
		{
			goto IL_02a1;
		}
	}
	{
		float L_115 = V_12;
		float L_116 = (__this->___m_ChildMinWidth_22);
		float L_117 = (__this->___m_ChildMaxWidth_23);
		float L_118 = (__this->___m_ChildMinWidth_22);
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t6_58_il2cpp_TypeInfo_var);
		float L_119 = Mathf_Clamp_m6_389(NULL /*static, unused*/, ((float)((float)((float)((float)L_115-(float)L_116))/(float)((float)((float)L_117-(float)L_118)))), (0.0f), (1.0f), /*hidden argument*/NULL);
		V_13 = L_119;
	}

IL_02a1:
	{
		V_14 = (0.0f);
		float L_120 = V_12;
		float L_121 = (__this->___m_ChildMaxWidth_23);
		if ((!(((float)L_120) > ((float)L_121))))
		{
			goto IL_02d4;
		}
	}
	{
		int32_t L_122 = (__this->___m_StretchableCountX_18);
		if ((((int32_t)L_122) <= ((int32_t)0)))
		{
			goto IL_02d4;
		}
	}
	{
		float L_123 = V_12;
		float L_124 = (__this->___m_ChildMaxWidth_23);
		int32_t L_125 = (__this->___m_StretchableCountX_18);
		V_14 = ((float)((float)((float)((float)L_123-(float)L_124))/(float)(((float)((float)L_125)))));
	}

IL_02d4:
	{
		V_15 = 0;
		V_16 = 1;
		List_1_t1_917 * L_126 = (__this->___entries_10);
		NullCheck(L_126);
		Enumerator_t1_927  L_127 = List_1_GetEnumerator_m1_5604(L_126, /*hidden argument*/List_1_GetEnumerator_m1_5604_MethodInfo_var);
		V_18 = L_127;
	}

IL_02e7:
	try
	{ // begin try (depth: 1)
		{
			goto IL_0392;
		}

IL_02ec:
		{
			GUILayoutEntry_t6_171 * L_128 = Enumerator_get_Current_m1_5605((&V_18), /*hidden argument*/Enumerator_get_Current_m1_5605_MethodInfo_var);
			V_17 = L_128;
			GUILayoutEntry_t6_171 * L_129 = V_17;
			NullCheck(L_129);
			float L_130 = (L_129->___minWidth_0);
			GUILayoutEntry_t6_171 * L_131 = V_17;
			NullCheck(L_131);
			float L_132 = (L_131->___maxWidth_1);
			float L_133 = V_13;
			IL2CPP_RUNTIME_CLASS_INIT(Mathf_t6_58_il2cpp_TypeInfo_var);
			float L_134 = Mathf_Lerp_m6_392(NULL /*static, unused*/, L_130, L_132, L_133, /*hidden argument*/NULL);
			V_19 = L_134;
			float L_135 = V_19;
			float L_136 = V_14;
			GUILayoutEntry_t6_171 * L_137 = V_17;
			NullCheck(L_137);
			int32_t L_138 = (L_137->___stretchWidth_5);
			V_19 = ((float)((float)L_135+(float)((float)((float)L_136*(float)(((float)((float)L_138)))))));
			GUILayoutEntry_t6_171 * L_139 = V_17;
			NullCheck(L_139);
			GUIStyle_t6_166 * L_140 = GUILayoutEntry_get_style_m6_1173(L_139, /*hidden argument*/NULL);
			IL2CPP_RUNTIME_CLASS_INIT(GUILayoutUtility_t6_170_il2cpp_TypeInfo_var);
			GUIStyle_t6_166 * L_141 = GUILayoutUtility_get_spaceStyle_m6_1166(NULL /*static, unused*/, /*hidden argument*/NULL);
			if ((((Object_t*)(GUIStyle_t6_166 *)L_140) == ((Object_t*)(GUIStyle_t6_166 *)L_141)))
			{
				goto IL_0371;
			}
		}

IL_032d:
		{
			GUILayoutEntry_t6_171 * L_142 = V_17;
			NullCheck(L_142);
			RectOffset_t6_172 * L_143 = (RectOffset_t6_172 *)VirtFuncInvoker0< RectOffset_t6_172 * >::Invoke(4 /* UnityEngine.RectOffset UnityEngine.GUILayoutEntry::get_margin() */, L_142);
			NullCheck(L_143);
			int32_t L_144 = RectOffset_get_left_m6_1284(L_143, /*hidden argument*/NULL);
			V_20 = L_144;
			bool L_145 = V_16;
			if (!L_145)
			{
				goto IL_0348;
			}
		}

IL_0342:
		{
			V_20 = 0;
			V_16 = 0;
		}

IL_0348:
		{
			int32_t L_146 = V_15;
			int32_t L_147 = V_20;
			if ((((int32_t)L_146) <= ((int32_t)L_147)))
			{
				goto IL_0358;
			}
		}

IL_0351:
		{
			int32_t L_148 = V_15;
			G_B39_0 = L_148;
			goto IL_035a;
		}

IL_0358:
		{
			int32_t L_149 = V_20;
			G_B39_0 = L_149;
		}

IL_035a:
		{
			V_21 = G_B39_0;
			float L_150 = ___x;
			int32_t L_151 = V_21;
			___x = ((float)((float)L_150+(float)(((float)((float)L_151)))));
			GUILayoutEntry_t6_171 * L_152 = V_17;
			NullCheck(L_152);
			RectOffset_t6_172 * L_153 = (RectOffset_t6_172 *)VirtFuncInvoker0< RectOffset_t6_172 * >::Invoke(4 /* UnityEngine.RectOffset UnityEngine.GUILayoutEntry::get_margin() */, L_152);
			NullCheck(L_153);
			int32_t L_154 = RectOffset_get_right_m6_1286(L_153, /*hidden argument*/NULL);
			V_15 = L_154;
		}

IL_0371:
		{
			GUILayoutEntry_t6_171 * L_155 = V_17;
			float L_156 = ___x;
			IL2CPP_RUNTIME_CLASS_INIT(Mathf_t6_58_il2cpp_TypeInfo_var);
			float L_157 = bankers_roundf(L_156);
			float L_158 = V_19;
			float L_159 = bankers_roundf(L_158);
			NullCheck(L_155);
			VirtActionInvoker2< float, float >::Invoke(7 /* System.Void UnityEngine.GUILayoutEntry::SetHorizontal(System.Single,System.Single) */, L_155, L_157, L_159);
			float L_160 = ___x;
			float L_161 = V_19;
			float L_162 = (__this->___spacing_13);
			___x = ((float)((float)L_160+(float)((float)((float)L_161+(float)L_162))));
		}

IL_0392:
		{
			bool L_163 = Enumerator_MoveNext_m1_5606((&V_18), /*hidden argument*/Enumerator_MoveNext_m1_5606_MethodInfo_var);
			if (L_163)
			{
				goto IL_02ec;
			}
		}

IL_039e:
		{
			IL2CPP_LEAVE(0x3B0, FINALLY_03a3);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1_33 *)e.ex;
		goto FINALLY_03a3;
	}

FINALLY_03a3:
	{ // begin finally (depth: 1)
		Enumerator_t1_927  L_164 = V_18;
		Enumerator_t1_927  L_165 = L_164;
		Object_t * L_166 = Box(Enumerator_t1_927_il2cpp_TypeInfo_var, &L_165);
		NullCheck(L_166);
		InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1_835_il2cpp_TypeInfo_var, L_166);
		IL2CPP_END_FINALLY(931)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(931)
	{
		IL2CPP_JUMP_TBL(0x3B0, IL_03b0)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1_33 *)
	}

IL_03b0:
	{
		return;
	}
}
// System.Void UnityEngine.GUILayoutGroup::CalcHeight()
extern TypeInfo* GUILayoutUtility_t6_170_il2cpp_TypeInfo_var;
extern TypeInfo* Mathf_t6_58_il2cpp_TypeInfo_var;
extern TypeInfo* Enumerator_t1_927_il2cpp_TypeInfo_var;
extern TypeInfo* IDisposable_t1_835_il2cpp_TypeInfo_var;
extern TypeInfo* GUIStyle_t6_166_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1_GetEnumerator_m1_5604_MethodInfo_var;
extern const MethodInfo* Enumerator_get_Current_m1_5605_MethodInfo_var;
extern const MethodInfo* Enumerator_MoveNext_m1_5606_MethodInfo_var;
extern "C" void GUILayoutGroup_CalcHeight_m6_1192 (GUILayoutGroup_t6_169 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GUILayoutUtility_t6_170_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(990);
		Mathf_t6_58_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(940);
		Enumerator_t1_927_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1001);
		IDisposable_t1_835_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(142);
		GUIStyle_t6_166_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(988);
		List_1_GetEnumerator_m1_5604_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483812);
		Enumerator_get_Current_m1_5605_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483813);
		Enumerator_MoveNext_m1_5606_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483814);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	bool V_3 = false;
	GUILayoutEntry_t6_171 * V_4 = {0};
	Enumerator_t1_927  V_5 = {0};
	RectOffset_t6_172 * V_6 = {0};
	int32_t V_7 = 0;
	bool V_8 = false;
	GUILayoutEntry_t6_171 * V_9 = {0};
	Enumerator_t1_927  V_10 = {0};
	RectOffset_t6_172 * V_11 = {0};
	float V_12 = 0.0f;
	float V_13 = 0.0f;
	float V_14 = 0.0f;
	Exception_t1_33 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1_33 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	int32_t G_B36_0 = 0;
	int32_t G_B36_1 = 0;
	GUILayoutGroup_t6_169 * G_B36_2 = {0};
	int32_t G_B35_0 = 0;
	int32_t G_B35_1 = 0;
	GUILayoutGroup_t6_169 * G_B35_2 = {0};
	int32_t G_B37_0 = 0;
	int32_t G_B37_1 = 0;
	int32_t G_B37_2 = 0;
	GUILayoutGroup_t6_169 * G_B37_3 = {0};
	{
		List_1_t1_917 * L_0 = (__this->___entries_10);
		NullCheck(L_0);
		int32_t L_1 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(17 /* System.Int32 System.Collections.Generic.List`1<UnityEngine.GUILayoutEntry>::get_Count() */, L_0);
		if (L_1)
		{
			goto IL_0033;
		}
	}
	{
		GUIStyle_t6_166 * L_2 = GUILayoutEntry_get_style_m6_1173(__this, /*hidden argument*/NULL);
		NullCheck(L_2);
		RectOffset_t6_172 * L_3 = GUIStyle_get_padding_m6_1304(L_2, /*hidden argument*/NULL);
		NullCheck(L_3);
		int32_t L_4 = RectOffset_get_vertical_m6_1293(L_3, /*hidden argument*/NULL);
		float L_5 = (((float)((float)L_4)));
		V_14 = L_5;
		((GUILayoutEntry_t6_171 *)__this)->___minHeight_2 = L_5;
		float L_6 = V_14;
		((GUILayoutEntry_t6_171 *)__this)->___maxHeight_3 = L_6;
		return;
	}

IL_0033:
	{
		V_0 = 0;
		V_1 = 0;
		__this->___m_ChildMinHeight_24 = (0.0f);
		__this->___m_ChildMaxHeight_25 = (0.0f);
		__this->___m_StretchableCountY_19 = 0;
		bool L_7 = (__this->___isVertical_11);
		if (!L_7)
		{
			goto IL_01d4;
		}
	}
	{
		V_2 = 0;
		V_3 = 1;
		List_1_t1_917 * L_8 = (__this->___entries_10);
		NullCheck(L_8);
		Enumerator_t1_927  L_9 = List_1_GetEnumerator_m1_5604(L_8, /*hidden argument*/List_1_GetEnumerator_m1_5604_MethodInfo_var);
		V_5 = L_9;
	}

IL_0070:
	try
	{ // begin try (depth: 1)
		{
			goto IL_0159;
		}

IL_0075:
		{
			GUILayoutEntry_t6_171 * L_10 = Enumerator_get_Current_m1_5605((&V_5), /*hidden argument*/Enumerator_get_Current_m1_5605_MethodInfo_var);
			V_4 = L_10;
			GUILayoutEntry_t6_171 * L_11 = V_4;
			NullCheck(L_11);
			VirtActionInvoker0::Invoke(6 /* System.Void UnityEngine.GUILayoutEntry::CalcHeight() */, L_11);
			GUILayoutEntry_t6_171 * L_12 = V_4;
			NullCheck(L_12);
			RectOffset_t6_172 * L_13 = (RectOffset_t6_172 *)VirtFuncInvoker0< RectOffset_t6_172 * >::Invoke(4 /* UnityEngine.RectOffset UnityEngine.GUILayoutEntry::get_margin() */, L_12);
			V_6 = L_13;
			GUILayoutEntry_t6_171 * L_14 = V_4;
			NullCheck(L_14);
			GUIStyle_t6_166 * L_15 = GUILayoutEntry_get_style_m6_1173(L_14, /*hidden argument*/NULL);
			IL2CPP_RUNTIME_CLASS_INIT(GUILayoutUtility_t6_170_il2cpp_TypeInfo_var);
			GUIStyle_t6_166 * L_16 = GUILayoutUtility_get_spaceStyle_m6_1166(NULL /*static, unused*/, /*hidden argument*/NULL);
			if ((((Object_t*)(GUIStyle_t6_166 *)L_15) == ((Object_t*)(GUIStyle_t6_166 *)L_16)))
			{
				goto IL_011d;
			}
		}

IL_009f:
		{
			bool L_17 = V_3;
			if (L_17)
			{
				goto IL_00b9;
			}
		}

IL_00a5:
		{
			int32_t L_18 = V_2;
			RectOffset_t6_172 * L_19 = V_6;
			NullCheck(L_19);
			int32_t L_20 = RectOffset_get_top_m6_1288(L_19, /*hidden argument*/NULL);
			IL2CPP_RUNTIME_CLASS_INIT(Mathf_t6_58_il2cpp_TypeInfo_var);
			int32_t L_21 = Mathf_Max_m6_384(NULL /*static, unused*/, L_18, L_20, /*hidden argument*/NULL);
			V_7 = L_21;
			goto IL_00be;
		}

IL_00b9:
		{
			V_7 = 0;
			V_3 = 0;
		}

IL_00be:
		{
			float L_22 = (__this->___m_ChildMinHeight_24);
			GUILayoutEntry_t6_171 * L_23 = V_4;
			NullCheck(L_23);
			float L_24 = (L_23->___minHeight_2);
			float L_25 = (__this->___spacing_13);
			int32_t L_26 = V_7;
			__this->___m_ChildMinHeight_24 = ((float)((float)L_22+(float)((float)((float)((float)((float)L_24+(float)L_25))+(float)(((float)((float)L_26)))))));
			float L_27 = (__this->___m_ChildMaxHeight_25);
			GUILayoutEntry_t6_171 * L_28 = V_4;
			NullCheck(L_28);
			float L_29 = (L_28->___maxHeight_3);
			float L_30 = (__this->___spacing_13);
			int32_t L_31 = V_7;
			__this->___m_ChildMaxHeight_25 = ((float)((float)L_27+(float)((float)((float)((float)((float)L_29+(float)L_30))+(float)(((float)((float)L_31)))))));
			RectOffset_t6_172 * L_32 = V_6;
			NullCheck(L_32);
			int32_t L_33 = RectOffset_get_bottom_m6_1290(L_32, /*hidden argument*/NULL);
			V_2 = L_33;
			int32_t L_34 = (__this->___m_StretchableCountY_19);
			GUILayoutEntry_t6_171 * L_35 = V_4;
			NullCheck(L_35);
			int32_t L_36 = (L_35->___stretchHeight_6);
			__this->___m_StretchableCountY_19 = ((int32_t)((int32_t)L_34+(int32_t)L_36));
			goto IL_0159;
		}

IL_011d:
		{
			float L_37 = (__this->___m_ChildMinHeight_24);
			GUILayoutEntry_t6_171 * L_38 = V_4;
			NullCheck(L_38);
			float L_39 = (L_38->___minHeight_2);
			__this->___m_ChildMinHeight_24 = ((float)((float)L_37+(float)L_39));
			float L_40 = (__this->___m_ChildMaxHeight_25);
			GUILayoutEntry_t6_171 * L_41 = V_4;
			NullCheck(L_41);
			float L_42 = (L_41->___maxHeight_3);
			__this->___m_ChildMaxHeight_25 = ((float)((float)L_40+(float)L_42));
			int32_t L_43 = (__this->___m_StretchableCountY_19);
			GUILayoutEntry_t6_171 * L_44 = V_4;
			NullCheck(L_44);
			int32_t L_45 = (L_44->___stretchHeight_6);
			__this->___m_StretchableCountY_19 = ((int32_t)((int32_t)L_43+(int32_t)L_45));
		}

IL_0159:
		{
			bool L_46 = Enumerator_MoveNext_m1_5606((&V_5), /*hidden argument*/Enumerator_MoveNext_m1_5606_MethodInfo_var);
			if (L_46)
			{
				goto IL_0075;
			}
		}

IL_0165:
		{
			IL2CPP_LEAVE(0x177, FINALLY_016a);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1_33 *)e.ex;
		goto FINALLY_016a;
	}

FINALLY_016a:
	{ // begin finally (depth: 1)
		Enumerator_t1_927  L_47 = V_5;
		Enumerator_t1_927  L_48 = L_47;
		Object_t * L_49 = Box(Enumerator_t1_927_il2cpp_TypeInfo_var, &L_48);
		NullCheck(L_49);
		InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1_835_il2cpp_TypeInfo_var, L_49);
		IL2CPP_END_FINALLY(362)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(362)
	{
		IL2CPP_JUMP_TBL(0x177, IL_0177)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1_33 *)
	}

IL_0177:
	{
		float L_50 = (__this->___m_ChildMinHeight_24);
		float L_51 = (__this->___spacing_13);
		__this->___m_ChildMinHeight_24 = ((float)((float)L_50-(float)L_51));
		float L_52 = (__this->___m_ChildMaxHeight_25);
		float L_53 = (__this->___spacing_13);
		__this->___m_ChildMaxHeight_25 = ((float)((float)L_52-(float)L_53));
		List_1_t1_917 * L_54 = (__this->___entries_10);
		NullCheck(L_54);
		int32_t L_55 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(17 /* System.Int32 System.Collections.Generic.List`1<UnityEngine.GUILayoutEntry>::get_Count() */, L_54);
		if (!L_55)
		{
			goto IL_01cb;
		}
	}
	{
		List_1_t1_917 * L_56 = (__this->___entries_10);
		NullCheck(L_56);
		GUILayoutEntry_t6_171 * L_57 = (GUILayoutEntry_t6_171 *)VirtFuncInvoker1< GUILayoutEntry_t6_171 *, int32_t >::Invoke(28 /* !0 System.Collections.Generic.List`1<UnityEngine.GUILayoutEntry>::get_Item(System.Int32) */, L_56, 0);
		NullCheck(L_57);
		RectOffset_t6_172 * L_58 = (RectOffset_t6_172 *)VirtFuncInvoker0< RectOffset_t6_172 * >::Invoke(4 /* UnityEngine.RectOffset UnityEngine.GUILayoutEntry::get_margin() */, L_57);
		NullCheck(L_58);
		int32_t L_59 = RectOffset_get_top_m6_1288(L_58, /*hidden argument*/NULL);
		V_0 = L_59;
		int32_t L_60 = V_2;
		V_1 = L_60;
		goto IL_01cf;
	}

IL_01cb:
	{
		int32_t L_61 = 0;
		V_0 = L_61;
		V_1 = L_61;
	}

IL_01cf:
	{
		goto IL_02b0;
	}

IL_01d4:
	{
		V_8 = 1;
		List_1_t1_917 * L_62 = (__this->___entries_10);
		NullCheck(L_62);
		Enumerator_t1_927  L_63 = List_1_GetEnumerator_m1_5604(L_62, /*hidden argument*/List_1_GetEnumerator_m1_5604_MethodInfo_var);
		V_10 = L_63;
	}

IL_01e4:
	try
	{ // begin try (depth: 1)
		{
			goto IL_0292;
		}

IL_01e9:
		{
			GUILayoutEntry_t6_171 * L_64 = Enumerator_get_Current_m1_5605((&V_10), /*hidden argument*/Enumerator_get_Current_m1_5605_MethodInfo_var);
			V_9 = L_64;
			GUILayoutEntry_t6_171 * L_65 = V_9;
			NullCheck(L_65);
			VirtActionInvoker0::Invoke(6 /* System.Void UnityEngine.GUILayoutEntry::CalcHeight() */, L_65);
			GUILayoutEntry_t6_171 * L_66 = V_9;
			NullCheck(L_66);
			RectOffset_t6_172 * L_67 = (RectOffset_t6_172 *)VirtFuncInvoker0< RectOffset_t6_172 * >::Invoke(4 /* UnityEngine.RectOffset UnityEngine.GUILayoutEntry::get_margin() */, L_66);
			V_11 = L_67;
			GUILayoutEntry_t6_171 * L_68 = V_9;
			NullCheck(L_68);
			GUIStyle_t6_166 * L_69 = GUILayoutEntry_get_style_m6_1173(L_68, /*hidden argument*/NULL);
			IL2CPP_RUNTIME_CLASS_INIT(GUILayoutUtility_t6_170_il2cpp_TypeInfo_var);
			GUIStyle_t6_166 * L_70 = GUILayoutUtility_get_spaceStyle_m6_1166(NULL /*static, unused*/, /*hidden argument*/NULL);
			if ((((Object_t*)(GUIStyle_t6_166 *)L_69) == ((Object_t*)(GUIStyle_t6_166 *)L_70)))
			{
				goto IL_027e;
			}
		}

IL_0213:
		{
			bool L_71 = V_8;
			if (L_71)
			{
				goto IL_023b;
			}
		}

IL_021a:
		{
			RectOffset_t6_172 * L_72 = V_11;
			NullCheck(L_72);
			int32_t L_73 = RectOffset_get_top_m6_1288(L_72, /*hidden argument*/NULL);
			int32_t L_74 = V_0;
			IL2CPP_RUNTIME_CLASS_INIT(Mathf_t6_58_il2cpp_TypeInfo_var);
			int32_t L_75 = Mathf_Min_m6_382(NULL /*static, unused*/, L_73, L_74, /*hidden argument*/NULL);
			V_0 = L_75;
			RectOffset_t6_172 * L_76 = V_11;
			NullCheck(L_76);
			int32_t L_77 = RectOffset_get_bottom_m6_1290(L_76, /*hidden argument*/NULL);
			int32_t L_78 = V_1;
			int32_t L_79 = Mathf_Min_m6_382(NULL /*static, unused*/, L_77, L_78, /*hidden argument*/NULL);
			V_1 = L_79;
			goto IL_024e;
		}

IL_023b:
		{
			RectOffset_t6_172 * L_80 = V_11;
			NullCheck(L_80);
			int32_t L_81 = RectOffset_get_top_m6_1288(L_80, /*hidden argument*/NULL);
			V_0 = L_81;
			RectOffset_t6_172 * L_82 = V_11;
			NullCheck(L_82);
			int32_t L_83 = RectOffset_get_bottom_m6_1290(L_82, /*hidden argument*/NULL);
			V_1 = L_83;
			V_8 = 0;
		}

IL_024e:
		{
			GUILayoutEntry_t6_171 * L_84 = V_9;
			NullCheck(L_84);
			float L_85 = (L_84->___minHeight_2);
			float L_86 = (__this->___m_ChildMinHeight_24);
			IL2CPP_RUNTIME_CLASS_INIT(Mathf_t6_58_il2cpp_TypeInfo_var);
			float L_87 = Mathf_Max_m6_383(NULL /*static, unused*/, L_85, L_86, /*hidden argument*/NULL);
			__this->___m_ChildMinHeight_24 = L_87;
			GUILayoutEntry_t6_171 * L_88 = V_9;
			NullCheck(L_88);
			float L_89 = (L_88->___maxHeight_3);
			float L_90 = (__this->___m_ChildMaxHeight_25);
			float L_91 = Mathf_Max_m6_383(NULL /*static, unused*/, L_89, L_90, /*hidden argument*/NULL);
			__this->___m_ChildMaxHeight_25 = L_91;
		}

IL_027e:
		{
			int32_t L_92 = (__this->___m_StretchableCountY_19);
			GUILayoutEntry_t6_171 * L_93 = V_9;
			NullCheck(L_93);
			int32_t L_94 = (L_93->___stretchHeight_6);
			__this->___m_StretchableCountY_19 = ((int32_t)((int32_t)L_92+(int32_t)L_94));
		}

IL_0292:
		{
			bool L_95 = Enumerator_MoveNext_m1_5606((&V_10), /*hidden argument*/Enumerator_MoveNext_m1_5606_MethodInfo_var);
			if (L_95)
			{
				goto IL_01e9;
			}
		}

IL_029e:
		{
			IL2CPP_LEAVE(0x2B0, FINALLY_02a3);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1_33 *)e.ex;
		goto FINALLY_02a3;
	}

FINALLY_02a3:
	{ // begin finally (depth: 1)
		Enumerator_t1_927  L_96 = V_10;
		Enumerator_t1_927  L_97 = L_96;
		Object_t * L_98 = Box(Enumerator_t1_927_il2cpp_TypeInfo_var, &L_97);
		NullCheck(L_98);
		InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1_835_il2cpp_TypeInfo_var, L_98);
		IL2CPP_END_FINALLY(675)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(675)
	{
		IL2CPP_JUMP_TBL(0x2B0, IL_02b0)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1_33 *)
	}

IL_02b0:
	{
		V_12 = (0.0f);
		V_13 = (0.0f);
		GUIStyle_t6_166 * L_99 = GUILayoutEntry_get_style_m6_1173(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(GUIStyle_t6_166_il2cpp_TypeInfo_var);
		GUIStyle_t6_166 * L_100 = GUIStyle_get_none_m6_1316(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((!(((Object_t*)(GUIStyle_t6_166 *)L_99) == ((Object_t*)(GUIStyle_t6_166 *)L_100))))
		{
			goto IL_02d9;
		}
	}
	{
		bool L_101 = (__this->___m_UserSpecifiedHeight_21);
		if (!L_101)
		{
			goto IL_0310;
		}
	}

IL_02d9:
	{
		GUIStyle_t6_166 * L_102 = GUILayoutEntry_get_style_m6_1173(__this, /*hidden argument*/NULL);
		NullCheck(L_102);
		RectOffset_t6_172 * L_103 = GUIStyle_get_padding_m6_1304(L_102, /*hidden argument*/NULL);
		NullCheck(L_103);
		int32_t L_104 = RectOffset_get_top_m6_1288(L_103, /*hidden argument*/NULL);
		int32_t L_105 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t6_58_il2cpp_TypeInfo_var);
		int32_t L_106 = Mathf_Max_m6_384(NULL /*static, unused*/, L_104, L_105, /*hidden argument*/NULL);
		V_12 = (((float)((float)L_106)));
		GUIStyle_t6_166 * L_107 = GUILayoutEntry_get_style_m6_1173(__this, /*hidden argument*/NULL);
		NullCheck(L_107);
		RectOffset_t6_172 * L_108 = GUIStyle_get_padding_m6_1304(L_107, /*hidden argument*/NULL);
		NullCheck(L_108);
		int32_t L_109 = RectOffset_get_bottom_m6_1290(L_108, /*hidden argument*/NULL);
		int32_t L_110 = V_1;
		int32_t L_111 = Mathf_Max_m6_384(NULL /*static, unused*/, L_109, L_110, /*hidden argument*/NULL);
		V_13 = (((float)((float)L_111)));
		goto IL_0332;
	}

IL_0310:
	{
		RectOffset_t6_172 * L_112 = (__this->___m_Margin_26);
		int32_t L_113 = V_0;
		NullCheck(L_112);
		RectOffset_set_top_m6_1289(L_112, L_113, /*hidden argument*/NULL);
		RectOffset_t6_172 * L_114 = (__this->___m_Margin_26);
		int32_t L_115 = V_1;
		NullCheck(L_114);
		RectOffset_set_bottom_m6_1291(L_114, L_115, /*hidden argument*/NULL);
		float L_116 = (0.0f);
		V_13 = L_116;
		V_12 = L_116;
	}

IL_0332:
	{
		float L_117 = (((GUILayoutEntry_t6_171 *)__this)->___minHeight_2);
		float L_118 = (__this->___m_ChildMinHeight_24);
		float L_119 = V_12;
		float L_120 = V_13;
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t6_58_il2cpp_TypeInfo_var);
		float L_121 = Mathf_Max_m6_383(NULL /*static, unused*/, L_117, ((float)((float)((float)((float)L_118+(float)L_119))+(float)L_120)), /*hidden argument*/NULL);
		((GUILayoutEntry_t6_171 *)__this)->___minHeight_2 = L_121;
		float L_122 = (((GUILayoutEntry_t6_171 *)__this)->___maxHeight_3);
		if ((!(((float)L_122) == ((float)(0.0f)))))
		{
			goto IL_03a1;
		}
	}
	{
		int32_t L_123 = (((GUILayoutEntry_t6_171 *)__this)->___stretchHeight_6);
		int32_t L_124 = (__this->___m_StretchableCountY_19);
		GUIStyle_t6_166 * L_125 = GUILayoutEntry_get_style_m6_1173(__this, /*hidden argument*/NULL);
		NullCheck(L_125);
		bool L_126 = GUIStyle_get_stretchHeight_m6_1346(L_125, /*hidden argument*/NULL);
		G_B35_0 = L_124;
		G_B35_1 = L_123;
		G_B35_2 = __this;
		if (!L_126)
		{
			G_B36_0 = L_124;
			G_B36_1 = L_123;
			G_B36_2 = __this;
			goto IL_0382;
		}
	}
	{
		G_B37_0 = 1;
		G_B37_1 = G_B35_0;
		G_B37_2 = G_B35_1;
		G_B37_3 = G_B35_2;
		goto IL_0383;
	}

IL_0382:
	{
		G_B37_0 = 0;
		G_B37_1 = G_B36_0;
		G_B37_2 = G_B36_1;
		G_B37_3 = G_B36_2;
	}

IL_0383:
	{
		NullCheck(G_B37_3);
		((GUILayoutEntry_t6_171 *)G_B37_3)->___stretchHeight_6 = ((int32_t)((int32_t)G_B37_2+(int32_t)((int32_t)((int32_t)G_B37_1+(int32_t)G_B37_0))));
		float L_127 = (__this->___m_ChildMaxHeight_25);
		float L_128 = V_12;
		float L_129 = V_13;
		((GUILayoutEntry_t6_171 *)__this)->___maxHeight_3 = ((float)((float)((float)((float)L_127+(float)L_128))+(float)L_129));
		goto IL_03a8;
	}

IL_03a1:
	{
		((GUILayoutEntry_t6_171 *)__this)->___stretchHeight_6 = 0;
	}

IL_03a8:
	{
		float L_130 = (((GUILayoutEntry_t6_171 *)__this)->___maxHeight_3);
		float L_131 = (((GUILayoutEntry_t6_171 *)__this)->___minHeight_2);
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t6_58_il2cpp_TypeInfo_var);
		float L_132 = Mathf_Max_m6_383(NULL /*static, unused*/, L_130, L_131, /*hidden argument*/NULL);
		((GUILayoutEntry_t6_171 *)__this)->___maxHeight_3 = L_132;
		GUIStyle_t6_166 * L_133 = GUILayoutEntry_get_style_m6_1173(__this, /*hidden argument*/NULL);
		NullCheck(L_133);
		float L_134 = GUIStyle_get_fixedHeight_m6_1343(L_133, /*hidden argument*/NULL);
		if ((((float)L_134) == ((float)(0.0f))))
		{
			goto IL_03f7;
		}
	}
	{
		GUIStyle_t6_166 * L_135 = GUILayoutEntry_get_style_m6_1173(__this, /*hidden argument*/NULL);
		NullCheck(L_135);
		float L_136 = GUIStyle_get_fixedHeight_m6_1343(L_135, /*hidden argument*/NULL);
		float L_137 = L_136;
		V_14 = L_137;
		((GUILayoutEntry_t6_171 *)__this)->___minHeight_2 = L_137;
		float L_138 = V_14;
		((GUILayoutEntry_t6_171 *)__this)->___maxHeight_3 = L_138;
		((GUILayoutEntry_t6_171 *)__this)->___stretchHeight_6 = 0;
	}

IL_03f7:
	{
		return;
	}
}
// System.Void UnityEngine.GUILayoutGroup::SetVertical(System.Single,System.Single)
extern TypeInfo* GUIStyle_t6_166_il2cpp_TypeInfo_var;
extern TypeInfo* Mathf_t6_58_il2cpp_TypeInfo_var;
extern TypeInfo* GUILayoutUtility_t6_170_il2cpp_TypeInfo_var;
extern TypeInfo* Enumerator_t1_927_il2cpp_TypeInfo_var;
extern TypeInfo* IDisposable_t1_835_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1_GetEnumerator_m1_5604_MethodInfo_var;
extern const MethodInfo* Enumerator_get_Current_m1_5605_MethodInfo_var;
extern const MethodInfo* Enumerator_MoveNext_m1_5606_MethodInfo_var;
extern "C" void GUILayoutGroup_SetVertical_m6_1193 (GUILayoutGroup_t6_169 * __this, float ___y, float ___height, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GUIStyle_t6_166_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(988);
		Mathf_t6_58_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(940);
		GUILayoutUtility_t6_170_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(990);
		Enumerator_t1_927_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1001);
		IDisposable_t1_835_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(142);
		List_1_GetEnumerator_m1_5604_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483812);
		Enumerator_get_Current_m1_5605_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483813);
		Enumerator_MoveNext_m1_5606_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483814);
		s_Il2CppMethodIntialized = true;
	}
	RectOffset_t6_172 * V_0 = {0};
	float V_1 = 0.0f;
	float V_2 = 0.0f;
	float V_3 = 0.0f;
	float V_4 = 0.0f;
	float V_5 = 0.0f;
	int32_t V_6 = 0;
	bool V_7 = false;
	GUILayoutEntry_t6_171 * V_8 = {0};
	Enumerator_t1_927  V_9 = {0};
	float V_10 = 0.0f;
	int32_t V_11 = 0;
	int32_t V_12 = 0;
	GUILayoutEntry_t6_171 * V_13 = {0};
	Enumerator_t1_927  V_14 = {0};
	float V_15 = 0.0f;
	float V_16 = 0.0f;
	float V_17 = 0.0f;
	float V_18 = 0.0f;
	float V_19 = 0.0f;
	GUILayoutEntry_t6_171 * V_20 = {0};
	Enumerator_t1_927  V_21 = {0};
	Exception_t1_33 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1_33 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	int32_t G_B22_0 = 0;
	{
		float L_0 = ___y;
		float L_1 = ___height;
		GUILayoutEntry_SetVertical_m6_1179(__this, L_0, L_1, /*hidden argument*/NULL);
		List_1_t1_917 * L_2 = (__this->___entries_10);
		NullCheck(L_2);
		int32_t L_3 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(17 /* System.Int32 System.Collections.Generic.List`1<UnityEngine.GUILayoutEntry>::get_Count() */, L_2);
		if (L_3)
		{
			goto IL_0019;
		}
	}
	{
		return;
	}

IL_0019:
	{
		GUIStyle_t6_166 * L_4 = GUILayoutEntry_get_style_m6_1173(__this, /*hidden argument*/NULL);
		NullCheck(L_4);
		RectOffset_t6_172 * L_5 = GUIStyle_get_padding_m6_1304(L_4, /*hidden argument*/NULL);
		V_0 = L_5;
		bool L_6 = (__this->___resetCoords_12);
		if (!L_6)
		{
			goto IL_0037;
		}
	}
	{
		___y = (0.0f);
	}

IL_0037:
	{
		bool L_7 = (__this->___isVertical_11);
		if (!L_7)
		{
			goto IL_022f;
		}
	}
	{
		GUIStyle_t6_166 * L_8 = GUILayoutEntry_get_style_m6_1173(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(GUIStyle_t6_166_il2cpp_TypeInfo_var);
		GUIStyle_t6_166 * L_9 = GUIStyle_get_none_m6_1316(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((((Object_t*)(GUIStyle_t6_166 *)L_8) == ((Object_t*)(GUIStyle_t6_166 *)L_9)))
		{
			goto IL_00c6;
		}
	}
	{
		RectOffset_t6_172 * L_10 = V_0;
		NullCheck(L_10);
		int32_t L_11 = RectOffset_get_top_m6_1288(L_10, /*hidden argument*/NULL);
		V_1 = (((float)((float)L_11)));
		RectOffset_t6_172 * L_12 = V_0;
		NullCheck(L_12);
		int32_t L_13 = RectOffset_get_bottom_m6_1290(L_12, /*hidden argument*/NULL);
		V_2 = (((float)((float)L_13)));
		List_1_t1_917 * L_14 = (__this->___entries_10);
		NullCheck(L_14);
		int32_t L_15 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(17 /* System.Int32 System.Collections.Generic.List`1<UnityEngine.GUILayoutEntry>::get_Count() */, L_14);
		if (!L_15)
		{
			goto IL_00ba;
		}
	}
	{
		float L_16 = V_1;
		List_1_t1_917 * L_17 = (__this->___entries_10);
		NullCheck(L_17);
		GUILayoutEntry_t6_171 * L_18 = (GUILayoutEntry_t6_171 *)VirtFuncInvoker1< GUILayoutEntry_t6_171 *, int32_t >::Invoke(28 /* !0 System.Collections.Generic.List`1<UnityEngine.GUILayoutEntry>::get_Item(System.Int32) */, L_17, 0);
		NullCheck(L_18);
		RectOffset_t6_172 * L_19 = (RectOffset_t6_172 *)VirtFuncInvoker0< RectOffset_t6_172 * >::Invoke(4 /* UnityEngine.RectOffset UnityEngine.GUILayoutEntry::get_margin() */, L_18);
		NullCheck(L_19);
		int32_t L_20 = RectOffset_get_top_m6_1288(L_19, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t6_58_il2cpp_TypeInfo_var);
		float L_21 = Mathf_Max_m6_383(NULL /*static, unused*/, L_16, (((float)((float)L_20))), /*hidden argument*/NULL);
		V_1 = L_21;
		float L_22 = V_2;
		List_1_t1_917 * L_23 = (__this->___entries_10);
		List_1_t1_917 * L_24 = (__this->___entries_10);
		NullCheck(L_24);
		int32_t L_25 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(17 /* System.Int32 System.Collections.Generic.List`1<UnityEngine.GUILayoutEntry>::get_Count() */, L_24);
		NullCheck(L_23);
		GUILayoutEntry_t6_171 * L_26 = (GUILayoutEntry_t6_171 *)VirtFuncInvoker1< GUILayoutEntry_t6_171 *, int32_t >::Invoke(28 /* !0 System.Collections.Generic.List`1<UnityEngine.GUILayoutEntry>::get_Item(System.Int32) */, L_23, ((int32_t)((int32_t)L_25-(int32_t)1)));
		NullCheck(L_26);
		RectOffset_t6_172 * L_27 = (RectOffset_t6_172 *)VirtFuncInvoker0< RectOffset_t6_172 * >::Invoke(4 /* UnityEngine.RectOffset UnityEngine.GUILayoutEntry::get_margin() */, L_26);
		NullCheck(L_27);
		int32_t L_28 = RectOffset_get_bottom_m6_1290(L_27, /*hidden argument*/NULL);
		float L_29 = Mathf_Max_m6_383(NULL /*static, unused*/, L_22, (((float)((float)L_28))), /*hidden argument*/NULL);
		V_2 = L_29;
	}

IL_00ba:
	{
		float L_30 = ___y;
		float L_31 = V_1;
		___y = ((float)((float)L_30+(float)L_31));
		float L_32 = ___height;
		float L_33 = V_2;
		float L_34 = V_1;
		___height = ((float)((float)L_32-(float)((float)((float)L_33+(float)L_34))));
	}

IL_00c6:
	{
		float L_35 = ___height;
		float L_36 = (__this->___spacing_13);
		List_1_t1_917 * L_37 = (__this->___entries_10);
		NullCheck(L_37);
		int32_t L_38 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(17 /* System.Int32 System.Collections.Generic.List`1<UnityEngine.GUILayoutEntry>::get_Count() */, L_37);
		V_3 = ((float)((float)L_35-(float)((float)((float)L_36*(float)(((float)((float)((int32_t)((int32_t)L_38-(int32_t)1)))))))));
		V_4 = (0.0f);
		float L_39 = (__this->___m_ChildMinHeight_24);
		float L_40 = (__this->___m_ChildMaxHeight_25);
		if ((((float)L_39) == ((float)L_40)))
		{
			goto IL_011d;
		}
	}
	{
		float L_41 = V_3;
		float L_42 = (__this->___m_ChildMinHeight_24);
		float L_43 = (__this->___m_ChildMaxHeight_25);
		float L_44 = (__this->___m_ChildMinHeight_24);
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t6_58_il2cpp_TypeInfo_var);
		float L_45 = Mathf_Clamp_m6_389(NULL /*static, unused*/, ((float)((float)((float)((float)L_41-(float)L_42))/(float)((float)((float)L_43-(float)L_44)))), (0.0f), (1.0f), /*hidden argument*/NULL);
		V_4 = L_45;
	}

IL_011d:
	{
		V_5 = (0.0f);
		float L_46 = V_3;
		float L_47 = (__this->___m_ChildMaxHeight_25);
		if ((!(((float)L_46) > ((float)L_47))))
		{
			goto IL_014e;
		}
	}
	{
		int32_t L_48 = (__this->___m_StretchableCountY_19);
		if ((((int32_t)L_48) <= ((int32_t)0)))
		{
			goto IL_014e;
		}
	}
	{
		float L_49 = V_3;
		float L_50 = (__this->___m_ChildMaxHeight_25);
		int32_t L_51 = (__this->___m_StretchableCountY_19);
		V_5 = ((float)((float)((float)((float)L_49-(float)L_50))/(float)(((float)((float)L_51)))));
	}

IL_014e:
	{
		V_6 = 0;
		V_7 = 1;
		List_1_t1_917 * L_52 = (__this->___entries_10);
		NullCheck(L_52);
		Enumerator_t1_927  L_53 = List_1_GetEnumerator_m1_5604(L_52, /*hidden argument*/List_1_GetEnumerator_m1_5604_MethodInfo_var);
		V_9 = L_53;
	}

IL_0161:
	try
	{ // begin try (depth: 1)
		{
			goto IL_020c;
		}

IL_0166:
		{
			GUILayoutEntry_t6_171 * L_54 = Enumerator_get_Current_m1_5605((&V_9), /*hidden argument*/Enumerator_get_Current_m1_5605_MethodInfo_var);
			V_8 = L_54;
			GUILayoutEntry_t6_171 * L_55 = V_8;
			NullCheck(L_55);
			float L_56 = (L_55->___minHeight_2);
			GUILayoutEntry_t6_171 * L_57 = V_8;
			NullCheck(L_57);
			float L_58 = (L_57->___maxHeight_3);
			float L_59 = V_4;
			IL2CPP_RUNTIME_CLASS_INIT(Mathf_t6_58_il2cpp_TypeInfo_var);
			float L_60 = Mathf_Lerp_m6_392(NULL /*static, unused*/, L_56, L_58, L_59, /*hidden argument*/NULL);
			V_10 = L_60;
			float L_61 = V_10;
			float L_62 = V_5;
			GUILayoutEntry_t6_171 * L_63 = V_8;
			NullCheck(L_63);
			int32_t L_64 = (L_63->___stretchHeight_6);
			V_10 = ((float)((float)L_61+(float)((float)((float)L_62*(float)(((float)((float)L_64)))))));
			GUILayoutEntry_t6_171 * L_65 = V_8;
			NullCheck(L_65);
			GUIStyle_t6_166 * L_66 = GUILayoutEntry_get_style_m6_1173(L_65, /*hidden argument*/NULL);
			IL2CPP_RUNTIME_CLASS_INIT(GUILayoutUtility_t6_170_il2cpp_TypeInfo_var);
			GUIStyle_t6_166 * L_67 = GUILayoutUtility_get_spaceStyle_m6_1166(NULL /*static, unused*/, /*hidden argument*/NULL);
			if ((((Object_t*)(GUIStyle_t6_166 *)L_66) == ((Object_t*)(GUIStyle_t6_166 *)L_67)))
			{
				goto IL_01eb;
			}
		}

IL_01a7:
		{
			GUILayoutEntry_t6_171 * L_68 = V_8;
			NullCheck(L_68);
			RectOffset_t6_172 * L_69 = (RectOffset_t6_172 *)VirtFuncInvoker0< RectOffset_t6_172 * >::Invoke(4 /* UnityEngine.RectOffset UnityEngine.GUILayoutEntry::get_margin() */, L_68);
			NullCheck(L_69);
			int32_t L_70 = RectOffset_get_top_m6_1288(L_69, /*hidden argument*/NULL);
			V_11 = L_70;
			bool L_71 = V_7;
			if (!L_71)
			{
				goto IL_01c2;
			}
		}

IL_01bc:
		{
			V_11 = 0;
			V_7 = 0;
		}

IL_01c2:
		{
			int32_t L_72 = V_6;
			int32_t L_73 = V_11;
			if ((((int32_t)L_72) <= ((int32_t)L_73)))
			{
				goto IL_01d2;
			}
		}

IL_01cb:
		{
			int32_t L_74 = V_6;
			G_B22_0 = L_74;
			goto IL_01d4;
		}

IL_01d2:
		{
			int32_t L_75 = V_11;
			G_B22_0 = L_75;
		}

IL_01d4:
		{
			V_12 = G_B22_0;
			float L_76 = ___y;
			int32_t L_77 = V_12;
			___y = ((float)((float)L_76+(float)(((float)((float)L_77)))));
			GUILayoutEntry_t6_171 * L_78 = V_8;
			NullCheck(L_78);
			RectOffset_t6_172 * L_79 = (RectOffset_t6_172 *)VirtFuncInvoker0< RectOffset_t6_172 * >::Invoke(4 /* UnityEngine.RectOffset UnityEngine.GUILayoutEntry::get_margin() */, L_78);
			NullCheck(L_79);
			int32_t L_80 = RectOffset_get_bottom_m6_1290(L_79, /*hidden argument*/NULL);
			V_6 = L_80;
		}

IL_01eb:
		{
			GUILayoutEntry_t6_171 * L_81 = V_8;
			float L_82 = ___y;
			IL2CPP_RUNTIME_CLASS_INIT(Mathf_t6_58_il2cpp_TypeInfo_var);
			float L_83 = bankers_roundf(L_82);
			float L_84 = V_10;
			float L_85 = bankers_roundf(L_84);
			NullCheck(L_81);
			VirtActionInvoker2< float, float >::Invoke(8 /* System.Void UnityEngine.GUILayoutEntry::SetVertical(System.Single,System.Single) */, L_81, L_83, L_85);
			float L_86 = ___y;
			float L_87 = V_10;
			float L_88 = (__this->___spacing_13);
			___y = ((float)((float)L_86+(float)((float)((float)L_87+(float)L_88))));
		}

IL_020c:
		{
			bool L_89 = Enumerator_MoveNext_m1_5606((&V_9), /*hidden argument*/Enumerator_MoveNext_m1_5606_MethodInfo_var);
			if (L_89)
			{
				goto IL_0166;
			}
		}

IL_0218:
		{
			IL2CPP_LEAVE(0x22A, FINALLY_021d);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1_33 *)e.ex;
		goto FINALLY_021d;
	}

FINALLY_021d:
	{ // begin finally (depth: 1)
		Enumerator_t1_927  L_90 = V_9;
		Enumerator_t1_927  L_91 = L_90;
		Object_t * L_92 = Box(Enumerator_t1_927_il2cpp_TypeInfo_var, &L_91);
		NullCheck(L_92);
		InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1_835_il2cpp_TypeInfo_var, L_92);
		IL2CPP_END_FINALLY(541)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(541)
	{
		IL2CPP_JUMP_TBL(0x22A, IL_022a)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1_33 *)
	}

IL_022a:
	{
		goto IL_03c1;
	}

IL_022f:
	{
		GUIStyle_t6_166 * L_93 = GUILayoutEntry_get_style_m6_1173(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(GUIStyle_t6_166_il2cpp_TypeInfo_var);
		GUIStyle_t6_166 * L_94 = GUIStyle_get_none_m6_1316(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((((Object_t*)(GUIStyle_t6_166 *)L_93) == ((Object_t*)(GUIStyle_t6_166 *)L_94)))
		{
			goto IL_02f6;
		}
	}
	{
		List_1_t1_917 * L_95 = (__this->___entries_10);
		NullCheck(L_95);
		Enumerator_t1_927  L_96 = List_1_GetEnumerator_m1_5604(L_95, /*hidden argument*/List_1_GetEnumerator_m1_5604_MethodInfo_var);
		V_14 = L_96;
	}

IL_024c:
	try
	{ // begin try (depth: 1)
		{
			goto IL_02d3;
		}

IL_0251:
		{
			GUILayoutEntry_t6_171 * L_97 = Enumerator_get_Current_m1_5605((&V_14), /*hidden argument*/Enumerator_get_Current_m1_5605_MethodInfo_var);
			V_13 = L_97;
			GUILayoutEntry_t6_171 * L_98 = V_13;
			NullCheck(L_98);
			RectOffset_t6_172 * L_99 = (RectOffset_t6_172 *)VirtFuncInvoker0< RectOffset_t6_172 * >::Invoke(4 /* UnityEngine.RectOffset UnityEngine.GUILayoutEntry::get_margin() */, L_98);
			NullCheck(L_99);
			int32_t L_100 = RectOffset_get_top_m6_1288(L_99, /*hidden argument*/NULL);
			RectOffset_t6_172 * L_101 = V_0;
			NullCheck(L_101);
			int32_t L_102 = RectOffset_get_top_m6_1288(L_101, /*hidden argument*/NULL);
			IL2CPP_RUNTIME_CLASS_INIT(Mathf_t6_58_il2cpp_TypeInfo_var);
			int32_t L_103 = Mathf_Max_m6_384(NULL /*static, unused*/, L_100, L_102, /*hidden argument*/NULL);
			V_15 = (((float)((float)L_103)));
			float L_104 = ___y;
			float L_105 = V_15;
			V_16 = ((float)((float)L_104+(float)L_105));
			float L_106 = ___height;
			GUILayoutEntry_t6_171 * L_107 = V_13;
			NullCheck(L_107);
			RectOffset_t6_172 * L_108 = (RectOffset_t6_172 *)VirtFuncInvoker0< RectOffset_t6_172 * >::Invoke(4 /* UnityEngine.RectOffset UnityEngine.GUILayoutEntry::get_margin() */, L_107);
			NullCheck(L_108);
			int32_t L_109 = RectOffset_get_bottom_m6_1290(L_108, /*hidden argument*/NULL);
			RectOffset_t6_172 * L_110 = V_0;
			NullCheck(L_110);
			int32_t L_111 = RectOffset_get_bottom_m6_1290(L_110, /*hidden argument*/NULL);
			int32_t L_112 = Mathf_Max_m6_384(NULL /*static, unused*/, L_109, L_111, /*hidden argument*/NULL);
			float L_113 = V_15;
			V_17 = ((float)((float)((float)((float)L_106-(float)(((float)((float)L_112)))))-(float)L_113));
			GUILayoutEntry_t6_171 * L_114 = V_13;
			NullCheck(L_114);
			int32_t L_115 = (L_114->___stretchHeight_6);
			if (!L_115)
			{
				goto IL_02b5;
			}
		}

IL_02a5:
		{
			GUILayoutEntry_t6_171 * L_116 = V_13;
			float L_117 = V_16;
			float L_118 = V_17;
			NullCheck(L_116);
			VirtActionInvoker2< float, float >::Invoke(8 /* System.Void UnityEngine.GUILayoutEntry::SetVertical(System.Single,System.Single) */, L_116, L_117, L_118);
			goto IL_02d3;
		}

IL_02b5:
		{
			GUILayoutEntry_t6_171 * L_119 = V_13;
			float L_120 = V_16;
			float L_121 = V_17;
			GUILayoutEntry_t6_171 * L_122 = V_13;
			NullCheck(L_122);
			float L_123 = (L_122->___minHeight_2);
			GUILayoutEntry_t6_171 * L_124 = V_13;
			NullCheck(L_124);
			float L_125 = (L_124->___maxHeight_3);
			IL2CPP_RUNTIME_CLASS_INIT(Mathf_t6_58_il2cpp_TypeInfo_var);
			float L_126 = Mathf_Clamp_m6_389(NULL /*static, unused*/, L_121, L_123, L_125, /*hidden argument*/NULL);
			NullCheck(L_119);
			VirtActionInvoker2< float, float >::Invoke(8 /* System.Void UnityEngine.GUILayoutEntry::SetVertical(System.Single,System.Single) */, L_119, L_120, L_126);
		}

IL_02d3:
		{
			bool L_127 = Enumerator_MoveNext_m1_5606((&V_14), /*hidden argument*/Enumerator_MoveNext_m1_5606_MethodInfo_var);
			if (L_127)
			{
				goto IL_0251;
			}
		}

IL_02df:
		{
			IL2CPP_LEAVE(0x2F1, FINALLY_02e4);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1_33 *)e.ex;
		goto FINALLY_02e4;
	}

FINALLY_02e4:
	{ // begin finally (depth: 1)
		Enumerator_t1_927  L_128 = V_14;
		Enumerator_t1_927  L_129 = L_128;
		Object_t * L_130 = Box(Enumerator_t1_927_il2cpp_TypeInfo_var, &L_129);
		NullCheck(L_130);
		InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1_835_il2cpp_TypeInfo_var, L_130);
		IL2CPP_END_FINALLY(740)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(740)
	{
		IL2CPP_JUMP_TBL(0x2F1, IL_02f1)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1_33 *)
	}

IL_02f1:
	{
		goto IL_03c1;
	}

IL_02f6:
	{
		float L_131 = ___y;
		RectOffset_t6_172 * L_132 = (RectOffset_t6_172 *)VirtFuncInvoker0< RectOffset_t6_172 * >::Invoke(4 /* UnityEngine.RectOffset UnityEngine.GUILayoutGroup::get_margin() */, __this);
		NullCheck(L_132);
		int32_t L_133 = RectOffset_get_top_m6_1288(L_132, /*hidden argument*/NULL);
		V_18 = ((float)((float)L_131-(float)(((float)((float)L_133)))));
		float L_134 = ___height;
		RectOffset_t6_172 * L_135 = (RectOffset_t6_172 *)VirtFuncInvoker0< RectOffset_t6_172 * >::Invoke(4 /* UnityEngine.RectOffset UnityEngine.GUILayoutGroup::get_margin() */, __this);
		NullCheck(L_135);
		int32_t L_136 = RectOffset_get_vertical_m6_1293(L_135, /*hidden argument*/NULL);
		V_19 = ((float)((float)L_134+(float)(((float)((float)L_136)))));
		List_1_t1_917 * L_137 = (__this->___entries_10);
		NullCheck(L_137);
		Enumerator_t1_927  L_138 = List_1_GetEnumerator_m1_5604(L_137, /*hidden argument*/List_1_GetEnumerator_m1_5604_MethodInfo_var);
		V_21 = L_138;
	}

IL_0323:
	try
	{ // begin try (depth: 1)
		{
			goto IL_03a3;
		}

IL_0328:
		{
			GUILayoutEntry_t6_171 * L_139 = Enumerator_get_Current_m1_5605((&V_21), /*hidden argument*/Enumerator_get_Current_m1_5605_MethodInfo_var);
			V_20 = L_139;
			GUILayoutEntry_t6_171 * L_140 = V_20;
			NullCheck(L_140);
			int32_t L_141 = (L_140->___stretchHeight_6);
			if (!L_141)
			{
				goto IL_0369;
			}
		}

IL_033d:
		{
			GUILayoutEntry_t6_171 * L_142 = V_20;
			float L_143 = V_18;
			GUILayoutEntry_t6_171 * L_144 = V_20;
			NullCheck(L_144);
			RectOffset_t6_172 * L_145 = (RectOffset_t6_172 *)VirtFuncInvoker0< RectOffset_t6_172 * >::Invoke(4 /* UnityEngine.RectOffset UnityEngine.GUILayoutEntry::get_margin() */, L_144);
			NullCheck(L_145);
			int32_t L_146 = RectOffset_get_top_m6_1288(L_145, /*hidden argument*/NULL);
			float L_147 = V_19;
			GUILayoutEntry_t6_171 * L_148 = V_20;
			NullCheck(L_148);
			RectOffset_t6_172 * L_149 = (RectOffset_t6_172 *)VirtFuncInvoker0< RectOffset_t6_172 * >::Invoke(4 /* UnityEngine.RectOffset UnityEngine.GUILayoutEntry::get_margin() */, L_148);
			NullCheck(L_149);
			int32_t L_150 = RectOffset_get_vertical_m6_1293(L_149, /*hidden argument*/NULL);
			NullCheck(L_142);
			VirtActionInvoker2< float, float >::Invoke(8 /* System.Void UnityEngine.GUILayoutEntry::SetVertical(System.Single,System.Single) */, L_142, ((float)((float)L_143+(float)(((float)((float)L_146))))), ((float)((float)L_147-(float)(((float)((float)L_150))))));
			goto IL_03a3;
		}

IL_0369:
		{
			GUILayoutEntry_t6_171 * L_151 = V_20;
			float L_152 = V_18;
			GUILayoutEntry_t6_171 * L_153 = V_20;
			NullCheck(L_153);
			RectOffset_t6_172 * L_154 = (RectOffset_t6_172 *)VirtFuncInvoker0< RectOffset_t6_172 * >::Invoke(4 /* UnityEngine.RectOffset UnityEngine.GUILayoutEntry::get_margin() */, L_153);
			NullCheck(L_154);
			int32_t L_155 = RectOffset_get_top_m6_1288(L_154, /*hidden argument*/NULL);
			float L_156 = V_19;
			GUILayoutEntry_t6_171 * L_157 = V_20;
			NullCheck(L_157);
			RectOffset_t6_172 * L_158 = (RectOffset_t6_172 *)VirtFuncInvoker0< RectOffset_t6_172 * >::Invoke(4 /* UnityEngine.RectOffset UnityEngine.GUILayoutEntry::get_margin() */, L_157);
			NullCheck(L_158);
			int32_t L_159 = RectOffset_get_vertical_m6_1293(L_158, /*hidden argument*/NULL);
			GUILayoutEntry_t6_171 * L_160 = V_20;
			NullCheck(L_160);
			float L_161 = (L_160->___minHeight_2);
			GUILayoutEntry_t6_171 * L_162 = V_20;
			NullCheck(L_162);
			float L_163 = (L_162->___maxHeight_3);
			IL2CPP_RUNTIME_CLASS_INIT(Mathf_t6_58_il2cpp_TypeInfo_var);
			float L_164 = Mathf_Clamp_m6_389(NULL /*static, unused*/, ((float)((float)L_156-(float)(((float)((float)L_159))))), L_161, L_163, /*hidden argument*/NULL);
			NullCheck(L_151);
			VirtActionInvoker2< float, float >::Invoke(8 /* System.Void UnityEngine.GUILayoutEntry::SetVertical(System.Single,System.Single) */, L_151, ((float)((float)L_152+(float)(((float)((float)L_155))))), L_164);
		}

IL_03a3:
		{
			bool L_165 = Enumerator_MoveNext_m1_5606((&V_21), /*hidden argument*/Enumerator_MoveNext_m1_5606_MethodInfo_var);
			if (L_165)
			{
				goto IL_0328;
			}
		}

IL_03af:
		{
			IL2CPP_LEAVE(0x3C1, FINALLY_03b4);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1_33 *)e.ex;
		goto FINALLY_03b4;
	}

FINALLY_03b4:
	{ // begin finally (depth: 1)
		Enumerator_t1_927  L_166 = V_21;
		Enumerator_t1_927  L_167 = L_166;
		Object_t * L_168 = Box(Enumerator_t1_927_il2cpp_TypeInfo_var, &L_167);
		NullCheck(L_168);
		InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1_835_il2cpp_TypeInfo_var, L_168);
		IL2CPP_END_FINALLY(948)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(948)
	{
		IL2CPP_JUMP_TBL(0x3C1, IL_03c1)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1_33 *)
	}

IL_03c1:
	{
		return;
	}
}
// System.String UnityEngine.GUILayoutGroup::ToString()
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* GUILayoutEntry_t6_171_il2cpp_TypeInfo_var;
extern TypeInfo* ObjectU5BU5D_t1_157_il2cpp_TypeInfo_var;
extern TypeInfo* Single_t1_17_il2cpp_TypeInfo_var;
extern TypeInfo* Enumerator_t1_927_il2cpp_TypeInfo_var;
extern TypeInfo* IDisposable_t1_835_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1_GetEnumerator_m1_5604_MethodInfo_var;
extern const MethodInfo* Enumerator_get_Current_m1_5605_MethodInfo_var;
extern const MethodInfo* Enumerator_MoveNext_m1_5606_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral232;
extern Il2CppCodeGenString* _stringLiteral2800;
extern Il2CppCodeGenString* _stringLiteral2801;
extern Il2CppCodeGenString* _stringLiteral2802;
extern Il2CppCodeGenString* _stringLiteral2172;
extern "C" String_t* GUILayoutGroup_ToString_m6_1194 (GUILayoutGroup_t6_169 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(11);
		GUILayoutEntry_t6_171_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1000);
		ObjectU5BU5D_t1_157_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(19);
		Single_t1_17_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(43);
		Enumerator_t1_927_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1001);
		IDisposable_t1_835_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(142);
		List_1_GetEnumerator_m1_5604_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483812);
		Enumerator_get_Current_m1_5605_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483813);
		Enumerator_MoveNext_m1_5606_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483814);
		_stringLiteral232 = il2cpp_codegen_string_literal_from_index(232);
		_stringLiteral2800 = il2cpp_codegen_string_literal_from_index(2800);
		_stringLiteral2801 = il2cpp_codegen_string_literal_from_index(2801);
		_stringLiteral2802 = il2cpp_codegen_string_literal_from_index(2802);
		_stringLiteral2172 = il2cpp_codegen_string_literal_from_index(2172);
		s_Il2CppMethodIntialized = true;
	}
	String_t* V_0 = {0};
	String_t* V_1 = {0};
	int32_t V_2 = 0;
	GUILayoutEntry_t6_171 * V_3 = {0};
	Enumerator_t1_927  V_4 = {0};
	String_t* V_5 = {0};
	Exception_t1_33 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1_33 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_0 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->___Empty_2;
		V_0 = L_0;
		String_t* L_1 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->___Empty_2;
		V_1 = L_1;
		V_2 = 0;
		goto IL_0023;
	}

IL_0013:
	{
		String_t* L_2 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_3 = String_Concat_m1_418(NULL /*static, unused*/, L_2, _stringLiteral232, /*hidden argument*/NULL);
		V_1 = L_3;
		int32_t L_4 = V_2;
		V_2 = ((int32_t)((int32_t)L_4+(int32_t)1));
	}

IL_0023:
	{
		int32_t L_5 = V_2;
		IL2CPP_RUNTIME_CLASS_INIT(GUILayoutEntry_t6_171_il2cpp_TypeInfo_var);
		int32_t L_6 = ((GUILayoutEntry_t6_171_StaticFields*)GUILayoutEntry_t6_171_il2cpp_TypeInfo_var->static_fields)->___indent_9;
		if ((((int32_t)L_5) < ((int32_t)L_6)))
		{
			goto IL_0013;
		}
	}
	{
		String_t* L_7 = V_0;
		V_5 = L_7;
		ObjectU5BU5D_t1_157* L_8 = ((ObjectU5BU5D_t1_157*)SZArrayNew(ObjectU5BU5D_t1_157_il2cpp_TypeInfo_var, 5));
		String_t* L_9 = V_5;
		NullCheck(L_8);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_8, 0);
		ArrayElementTypeCheck (L_8, L_9);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_8, 0, sizeof(Object_t *))) = (Object_t *)L_9;
		ObjectU5BU5D_t1_157* L_10 = L_8;
		String_t* L_11 = GUILayoutEntry_ToString_m6_1182(__this, /*hidden argument*/NULL);
		NullCheck(L_10);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_10, 1);
		ArrayElementTypeCheck (L_10, L_11);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_10, 1, sizeof(Object_t *))) = (Object_t *)L_11;
		ObjectU5BU5D_t1_157* L_12 = L_10;
		NullCheck(L_12);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_12, 2);
		ArrayElementTypeCheck (L_12, _stringLiteral2800);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_12, 2, sizeof(Object_t *))) = (Object_t *)_stringLiteral2800;
		ObjectU5BU5D_t1_157* L_13 = L_12;
		float L_14 = (__this->___m_ChildMinHeight_24);
		float L_15 = L_14;
		Object_t * L_16 = Box(Single_t1_17_il2cpp_TypeInfo_var, &L_15);
		NullCheck(L_13);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_13, 3);
		ArrayElementTypeCheck (L_13, L_16);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_13, 3, sizeof(Object_t *))) = (Object_t *)L_16;
		ObjectU5BU5D_t1_157* L_17 = L_13;
		NullCheck(L_17);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_17, 4);
		ArrayElementTypeCheck (L_17, _stringLiteral2801);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_17, 4, sizeof(Object_t *))) = (Object_t *)_stringLiteral2801;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_18 = String_Concat_m1_421(NULL /*static, unused*/, L_17, /*hidden argument*/NULL);
		V_0 = L_18;
		IL2CPP_RUNTIME_CLASS_INIT(GUILayoutEntry_t6_171_il2cpp_TypeInfo_var);
		int32_t L_19 = ((GUILayoutEntry_t6_171_StaticFields*)GUILayoutEntry_t6_171_il2cpp_TypeInfo_var->static_fields)->___indent_9;
		((GUILayoutEntry_t6_171_StaticFields*)GUILayoutEntry_t6_171_il2cpp_TypeInfo_var->static_fields)->___indent_9 = ((int32_t)((int32_t)L_19+(int32_t)4));
		List_1_t1_917 * L_20 = (__this->___entries_10);
		NullCheck(L_20);
		Enumerator_t1_927  L_21 = List_1_GetEnumerator_m1_5604(L_20, /*hidden argument*/List_1_GetEnumerator_m1_5604_MethodInfo_var);
		V_4 = L_21;
	}

IL_0082:
	try
	{ // begin try (depth: 1)
		{
			goto IL_00a1;
		}

IL_0087:
		{
			GUILayoutEntry_t6_171 * L_22 = Enumerator_get_Current_m1_5605((&V_4), /*hidden argument*/Enumerator_get_Current_m1_5605_MethodInfo_var);
			V_3 = L_22;
			String_t* L_23 = V_0;
			GUILayoutEntry_t6_171 * L_24 = V_3;
			NullCheck(L_24);
			String_t* L_25 = (String_t*)VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String UnityEngine.GUILayoutEntry::ToString() */, L_24);
			IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
			String_t* L_26 = String_Concat_m1_419(NULL /*static, unused*/, L_23, L_25, _stringLiteral2802, /*hidden argument*/NULL);
			V_0 = L_26;
		}

IL_00a1:
		{
			bool L_27 = Enumerator_MoveNext_m1_5606((&V_4), /*hidden argument*/Enumerator_MoveNext_m1_5606_MethodInfo_var);
			if (L_27)
			{
				goto IL_0087;
			}
		}

IL_00ad:
		{
			IL2CPP_LEAVE(0xBF, FINALLY_00b2);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1_33 *)e.ex;
		goto FINALLY_00b2;
	}

FINALLY_00b2:
	{ // begin finally (depth: 1)
		Enumerator_t1_927  L_28 = V_4;
		Enumerator_t1_927  L_29 = L_28;
		Object_t * L_30 = Box(Enumerator_t1_927_il2cpp_TypeInfo_var, &L_29);
		NullCheck(L_30);
		InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1_835_il2cpp_TypeInfo_var, L_30);
		IL2CPP_END_FINALLY(178)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(178)
	{
		IL2CPP_JUMP_TBL(0xBF, IL_00bf)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1_33 *)
	}

IL_00bf:
	{
		String_t* L_31 = V_0;
		String_t* L_32 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_33 = String_Concat_m1_419(NULL /*static, unused*/, L_31, L_32, _stringLiteral2172, /*hidden argument*/NULL);
		V_0 = L_33;
		IL2CPP_RUNTIME_CLASS_INIT(GUILayoutEntry_t6_171_il2cpp_TypeInfo_var);
		int32_t L_34 = ((GUILayoutEntry_t6_171_StaticFields*)GUILayoutEntry_t6_171_il2cpp_TypeInfo_var->static_fields)->___indent_9;
		((GUILayoutEntry_t6_171_StaticFields*)GUILayoutEntry_t6_171_il2cpp_TypeInfo_var->static_fields)->___indent_9 = ((int32_t)((int32_t)L_34-(int32_t)4));
		String_t* L_35 = V_0;
		return L_35;
	}
}
// System.Void UnityEngine.GUIScrollGroup::.ctor()
extern "C" void GUIScrollGroup__ctor_m6_1195 (GUIScrollGroup_t6_173 * __this, const MethodInfo* method)
{
	{
		__this->___allowHorizontalScroll_33 = 1;
		__this->___allowVerticalScroll_34 = 1;
		GUILayoutGroup__ctor_m6_1183(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.GUIScrollGroup::CalcWidth()
extern "C" void GUIScrollGroup_CalcWidth_m6_1196 (GUIScrollGroup_t6_173 * __this, const MethodInfo* method)
{
	float V_0 = 0.0f;
	float V_1 = 0.0f;
	{
		float L_0 = (((GUILayoutEntry_t6_171 *)__this)->___minWidth_0);
		V_0 = L_0;
		float L_1 = (((GUILayoutEntry_t6_171 *)__this)->___maxWidth_1);
		V_1 = L_1;
		bool L_2 = (__this->___allowHorizontalScroll_33);
		if (!L_2)
		{
			goto IL_002f;
		}
	}
	{
		((GUILayoutEntry_t6_171 *)__this)->___minWidth_0 = (0.0f);
		((GUILayoutEntry_t6_171 *)__this)->___maxWidth_1 = (0.0f);
	}

IL_002f:
	{
		GUILayoutGroup_CalcWidth_m6_1190(__this, /*hidden argument*/NULL);
		float L_3 = (((GUILayoutEntry_t6_171 *)__this)->___minWidth_0);
		__this->___calcMinWidth_27 = L_3;
		float L_4 = (((GUILayoutEntry_t6_171 *)__this)->___maxWidth_1);
		__this->___calcMaxWidth_28 = L_4;
		bool L_5 = (__this->___allowHorizontalScroll_33);
		if (!L_5)
		{
			goto IL_009e;
		}
	}
	{
		float L_6 = (((GUILayoutEntry_t6_171 *)__this)->___minWidth_0);
		if ((!(((float)L_6) > ((float)(32.0f)))))
		{
			goto IL_0073;
		}
	}
	{
		((GUILayoutEntry_t6_171 *)__this)->___minWidth_0 = (32.0f);
	}

IL_0073:
	{
		float L_7 = V_0;
		if ((((float)L_7) == ((float)(0.0f))))
		{
			goto IL_0085;
		}
	}
	{
		float L_8 = V_0;
		((GUILayoutEntry_t6_171 *)__this)->___minWidth_0 = L_8;
	}

IL_0085:
	{
		float L_9 = V_1;
		if ((((float)L_9) == ((float)(0.0f))))
		{
			goto IL_009e;
		}
	}
	{
		float L_10 = V_1;
		((GUILayoutEntry_t6_171 *)__this)->___maxWidth_1 = L_10;
		((GUILayoutEntry_t6_171 *)__this)->___stretchWidth_5 = 0;
	}

IL_009e:
	{
		return;
	}
}
// System.Void UnityEngine.GUIScrollGroup::SetHorizontal(System.Single,System.Single)
extern "C" void GUIScrollGroup_SetHorizontal_m6_1197 (GUIScrollGroup_t6_173 * __this, float ___x, float ___width, const MethodInfo* method)
{
	float V_0 = 0.0f;
	float G_B3_0 = 0.0f;
	{
		bool L_0 = (__this->___needsVerticalScrollbar_36);
		if (!L_0)
		{
			goto IL_002f;
		}
	}
	{
		float L_1 = ___width;
		GUIStyle_t6_166 * L_2 = (__this->___verticalScrollbar_38);
		NullCheck(L_2);
		float L_3 = GUIStyle_get_fixedWidth_m6_1342(L_2, /*hidden argument*/NULL);
		GUIStyle_t6_166 * L_4 = (__this->___verticalScrollbar_38);
		NullCheck(L_4);
		RectOffset_t6_172 * L_5 = GUIStyle_get_margin_m6_1303(L_4, /*hidden argument*/NULL);
		NullCheck(L_5);
		int32_t L_6 = RectOffset_get_left_m6_1284(L_5, /*hidden argument*/NULL);
		G_B3_0 = ((float)((float)((float)((float)L_1-(float)L_3))-(float)(((float)((float)L_6)))));
		goto IL_0030;
	}

IL_002f:
	{
		float L_7 = ___width;
		G_B3_0 = L_7;
	}

IL_0030:
	{
		V_0 = G_B3_0;
		bool L_8 = (__this->___allowHorizontalScroll_33);
		if (!L_8)
		{
			goto IL_0091;
		}
	}
	{
		float L_9 = V_0;
		float L_10 = (__this->___calcMinWidth_27);
		if ((!(((float)L_9) < ((float)L_10))))
		{
			goto IL_0091;
		}
	}
	{
		__this->___needsHorizontalScrollbar_35 = 1;
		float L_11 = (__this->___calcMinWidth_27);
		((GUILayoutEntry_t6_171 *)__this)->___minWidth_0 = L_11;
		float L_12 = (__this->___calcMaxWidth_28);
		((GUILayoutEntry_t6_171 *)__this)->___maxWidth_1 = L_12;
		float L_13 = ___x;
		float L_14 = (__this->___calcMinWidth_27);
		GUILayoutGroup_SetHorizontal_m6_1191(__this, L_13, L_14, /*hidden argument*/NULL);
		Rect_t6_52 * L_15 = &(((GUILayoutEntry_t6_171 *)__this)->___rect_4);
		float L_16 = ___width;
		Rect_set_width_m6_278(L_15, L_16, /*hidden argument*/NULL);
		float L_17 = (__this->___calcMinWidth_27);
		__this->___clientWidth_31 = L_17;
		goto IL_00d6;
	}

IL_0091:
	{
		__this->___needsHorizontalScrollbar_35 = 0;
		bool L_18 = (__this->___allowHorizontalScroll_33);
		if (!L_18)
		{
			goto IL_00bb;
		}
	}
	{
		float L_19 = (__this->___calcMinWidth_27);
		((GUILayoutEntry_t6_171 *)__this)->___minWidth_0 = L_19;
		float L_20 = (__this->___calcMaxWidth_28);
		((GUILayoutEntry_t6_171 *)__this)->___maxWidth_1 = L_20;
	}

IL_00bb:
	{
		float L_21 = ___x;
		float L_22 = V_0;
		GUILayoutGroup_SetHorizontal_m6_1191(__this, L_21, L_22, /*hidden argument*/NULL);
		Rect_t6_52 * L_23 = &(((GUILayoutEntry_t6_171 *)__this)->___rect_4);
		float L_24 = ___width;
		Rect_set_width_m6_278(L_23, L_24, /*hidden argument*/NULL);
		float L_25 = V_0;
		__this->___clientWidth_31 = L_25;
	}

IL_00d6:
	{
		return;
	}
}
// System.Void UnityEngine.GUIScrollGroup::CalcHeight()
extern "C" void GUIScrollGroup_CalcHeight_m6_1198 (GUIScrollGroup_t6_173 * __this, const MethodInfo* method)
{
	float V_0 = 0.0f;
	float V_1 = 0.0f;
	float V_2 = 0.0f;
	{
		float L_0 = (((GUILayoutEntry_t6_171 *)__this)->___minHeight_2);
		V_0 = L_0;
		float L_1 = (((GUILayoutEntry_t6_171 *)__this)->___maxHeight_3);
		V_1 = L_1;
		bool L_2 = (__this->___allowVerticalScroll_34);
		if (!L_2)
		{
			goto IL_002f;
		}
	}
	{
		((GUILayoutEntry_t6_171 *)__this)->___minHeight_2 = (0.0f);
		((GUILayoutEntry_t6_171 *)__this)->___maxHeight_3 = (0.0f);
	}

IL_002f:
	{
		GUILayoutGroup_CalcHeight_m6_1192(__this, /*hidden argument*/NULL);
		float L_3 = (((GUILayoutEntry_t6_171 *)__this)->___minHeight_2);
		__this->___calcMinHeight_29 = L_3;
		float L_4 = (((GUILayoutEntry_t6_171 *)__this)->___maxHeight_3);
		__this->___calcMaxHeight_30 = L_4;
		bool L_5 = (__this->___needsHorizontalScrollbar_35);
		if (!L_5)
		{
			goto IL_0092;
		}
	}
	{
		GUIStyle_t6_166 * L_6 = (__this->___horizontalScrollbar_37);
		NullCheck(L_6);
		float L_7 = GUIStyle_get_fixedHeight_m6_1343(L_6, /*hidden argument*/NULL);
		GUIStyle_t6_166 * L_8 = (__this->___horizontalScrollbar_37);
		NullCheck(L_8);
		RectOffset_t6_172 * L_9 = GUIStyle_get_margin_m6_1303(L_8, /*hidden argument*/NULL);
		NullCheck(L_9);
		int32_t L_10 = RectOffset_get_top_m6_1288(L_9, /*hidden argument*/NULL);
		V_2 = ((float)((float)L_7+(float)(((float)((float)L_10)))));
		float L_11 = (((GUILayoutEntry_t6_171 *)__this)->___minHeight_2);
		float L_12 = V_2;
		((GUILayoutEntry_t6_171 *)__this)->___minHeight_2 = ((float)((float)L_11+(float)L_12));
		float L_13 = (((GUILayoutEntry_t6_171 *)__this)->___maxHeight_3);
		float L_14 = V_2;
		((GUILayoutEntry_t6_171 *)__this)->___maxHeight_3 = ((float)((float)L_13+(float)L_14));
	}

IL_0092:
	{
		bool L_15 = (__this->___allowVerticalScroll_34);
		if (!L_15)
		{
			goto IL_00e3;
		}
	}
	{
		float L_16 = (((GUILayoutEntry_t6_171 *)__this)->___minHeight_2);
		if ((!(((float)L_16) > ((float)(32.0f)))))
		{
			goto IL_00b8;
		}
	}
	{
		((GUILayoutEntry_t6_171 *)__this)->___minHeight_2 = (32.0f);
	}

IL_00b8:
	{
		float L_17 = V_0;
		if ((((float)L_17) == ((float)(0.0f))))
		{
			goto IL_00ca;
		}
	}
	{
		float L_18 = V_0;
		((GUILayoutEntry_t6_171 *)__this)->___minHeight_2 = L_18;
	}

IL_00ca:
	{
		float L_19 = V_1;
		if ((((float)L_19) == ((float)(0.0f))))
		{
			goto IL_00e3;
		}
	}
	{
		float L_20 = V_1;
		((GUILayoutEntry_t6_171 *)__this)->___maxHeight_3 = L_20;
		((GUILayoutEntry_t6_171 *)__this)->___stretchHeight_6 = 0;
	}

IL_00e3:
	{
		return;
	}
}
// System.Void UnityEngine.GUIScrollGroup::SetVertical(System.Single,System.Single)
extern "C" void GUIScrollGroup_SetVertical_m6_1199 (GUIScrollGroup_t6_173 * __this, float ___y, float ___height, const MethodInfo* method)
{
	float V_0 = 0.0f;
	float V_1 = 0.0f;
	float V_2 = 0.0f;
	float V_3 = 0.0f;
	{
		float L_0 = ___height;
		V_0 = L_0;
		bool L_1 = (__this->___needsHorizontalScrollbar_35);
		if (!L_1)
		{
			goto IL_002d;
		}
	}
	{
		float L_2 = V_0;
		GUIStyle_t6_166 * L_3 = (__this->___horizontalScrollbar_37);
		NullCheck(L_3);
		float L_4 = GUIStyle_get_fixedHeight_m6_1343(L_3, /*hidden argument*/NULL);
		GUIStyle_t6_166 * L_5 = (__this->___horizontalScrollbar_37);
		NullCheck(L_5);
		RectOffset_t6_172 * L_6 = GUIStyle_get_margin_m6_1303(L_5, /*hidden argument*/NULL);
		NullCheck(L_6);
		int32_t L_7 = RectOffset_get_top_m6_1288(L_6, /*hidden argument*/NULL);
		V_0 = ((float)((float)L_2-(float)((float)((float)L_4+(float)(((float)((float)L_7)))))));
	}

IL_002d:
	{
		bool L_8 = (__this->___allowVerticalScroll_34);
		if (!L_8)
		{
			goto IL_0139;
		}
	}
	{
		float L_9 = V_0;
		float L_10 = (__this->___calcMinHeight_29);
		if ((!(((float)L_9) < ((float)L_10))))
		{
			goto IL_0139;
		}
	}
	{
		bool L_11 = (__this->___needsHorizontalScrollbar_35);
		if (L_11)
		{
			goto IL_00db;
		}
	}
	{
		bool L_12 = (__this->___needsVerticalScrollbar_36);
		if (L_12)
		{
			goto IL_00db;
		}
	}
	{
		Rect_t6_52 * L_13 = &(((GUILayoutEntry_t6_171 *)__this)->___rect_4);
		float L_14 = Rect_get_width_m6_277(L_13, /*hidden argument*/NULL);
		GUIStyle_t6_166 * L_15 = (__this->___verticalScrollbar_38);
		NullCheck(L_15);
		float L_16 = GUIStyle_get_fixedWidth_m6_1342(L_15, /*hidden argument*/NULL);
		GUIStyle_t6_166 * L_17 = (__this->___verticalScrollbar_38);
		NullCheck(L_17);
		RectOffset_t6_172 * L_18 = GUIStyle_get_margin_m6_1303(L_17, /*hidden argument*/NULL);
		NullCheck(L_18);
		int32_t L_19 = RectOffset_get_left_m6_1284(L_18, /*hidden argument*/NULL);
		__this->___clientWidth_31 = ((float)((float)((float)((float)L_14-(float)L_16))-(float)(((float)((float)L_19)))));
		float L_20 = (__this->___clientWidth_31);
		float L_21 = (__this->___calcMinWidth_27);
		if ((!(((float)L_20) < ((float)L_21))))
		{
			goto IL_00a6;
		}
	}
	{
		float L_22 = (__this->___calcMinWidth_27);
		__this->___clientWidth_31 = L_22;
	}

IL_00a6:
	{
		Rect_t6_52 * L_23 = &(((GUILayoutEntry_t6_171 *)__this)->___rect_4);
		float L_24 = Rect_get_width_m6_277(L_23, /*hidden argument*/NULL);
		V_1 = L_24;
		Rect_t6_52 * L_25 = &(((GUILayoutEntry_t6_171 *)__this)->___rect_4);
		float L_26 = Rect_get_x_m6_273(L_25, /*hidden argument*/NULL);
		float L_27 = (__this->___clientWidth_31);
		GUIScrollGroup_SetHorizontal_m6_1197(__this, L_26, L_27, /*hidden argument*/NULL);
		GUIScrollGroup_CalcHeight_m6_1198(__this, /*hidden argument*/NULL);
		Rect_t6_52 * L_28 = &(((GUILayoutEntry_t6_171 *)__this)->___rect_4);
		float L_29 = V_1;
		Rect_set_width_m6_278(L_28, L_29, /*hidden argument*/NULL);
	}

IL_00db:
	{
		float L_30 = (((GUILayoutEntry_t6_171 *)__this)->___minHeight_2);
		V_2 = L_30;
		float L_31 = (((GUILayoutEntry_t6_171 *)__this)->___maxHeight_3);
		V_3 = L_31;
		float L_32 = (__this->___calcMinHeight_29);
		((GUILayoutEntry_t6_171 *)__this)->___minHeight_2 = L_32;
		float L_33 = (__this->___calcMaxHeight_30);
		((GUILayoutEntry_t6_171 *)__this)->___maxHeight_3 = L_33;
		float L_34 = ___y;
		float L_35 = (__this->___calcMinHeight_29);
		GUILayoutGroup_SetVertical_m6_1193(__this, L_34, L_35, /*hidden argument*/NULL);
		float L_36 = V_2;
		((GUILayoutEntry_t6_171 *)__this)->___minHeight_2 = L_36;
		float L_37 = V_3;
		((GUILayoutEntry_t6_171 *)__this)->___maxHeight_3 = L_37;
		Rect_t6_52 * L_38 = &(((GUILayoutEntry_t6_171 *)__this)->___rect_4);
		float L_39 = ___height;
		Rect_set_height_m6_280(L_38, L_39, /*hidden argument*/NULL);
		float L_40 = (__this->___calcMinHeight_29);
		__this->___clientHeight_32 = L_40;
		goto IL_0177;
	}

IL_0139:
	{
		bool L_41 = (__this->___allowVerticalScroll_34);
		if (!L_41)
		{
			goto IL_015c;
		}
	}
	{
		float L_42 = (__this->___calcMinHeight_29);
		((GUILayoutEntry_t6_171 *)__this)->___minHeight_2 = L_42;
		float L_43 = (__this->___calcMaxHeight_30);
		((GUILayoutEntry_t6_171 *)__this)->___maxHeight_3 = L_43;
	}

IL_015c:
	{
		float L_44 = ___y;
		float L_45 = V_0;
		GUILayoutGroup_SetVertical_m6_1193(__this, L_44, L_45, /*hidden argument*/NULL);
		Rect_t6_52 * L_46 = &(((GUILayoutEntry_t6_171 *)__this)->___rect_4);
		float L_47 = ___height;
		Rect_set_height_m6_280(L_46, L_47, /*hidden argument*/NULL);
		float L_48 = V_0;
		__this->___clientHeight_32 = L_48;
	}

IL_0177:
	{
		return;
	}
}
// System.Void UnityEngine.GUIWordWrapSizer::.ctor(UnityEngine.GUIStyle,UnityEngine.GUIContent,UnityEngine.GUILayoutOption[])
extern TypeInfo* GUILayoutEntry_t6_171_il2cpp_TypeInfo_var;
extern TypeInfo* GUIContent_t6_163_il2cpp_TypeInfo_var;
extern "C" void GUIWordWrapSizer__ctor_m6_1200 (GUIWordWrapSizer_t6_174 * __this, GUIStyle_t6_166 * ___style, GUIContent_t6_163 * ___content, GUILayoutOptionU5BU5D_t6_165* ___options, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GUILayoutEntry_t6_171_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1000);
		GUIContent_t6_163_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(985);
		s_Il2CppMethodIntialized = true;
	}
	{
		GUIStyle_t6_166 * L_0 = ___style;
		IL2CPP_RUNTIME_CLASS_INIT(GUILayoutEntry_t6_171_il2cpp_TypeInfo_var);
		GUILayoutEntry__ctor_m6_1170(__this, (0.0f), (0.0f), (0.0f), (0.0f), L_0, /*hidden argument*/NULL);
		GUIContent_t6_163 * L_1 = ___content;
		GUIContent_t6_163 * L_2 = (GUIContent_t6_163 *)il2cpp_codegen_object_new (GUIContent_t6_163_il2cpp_TypeInfo_var);
		GUIContent__ctor_m6_1101(L_2, L_1, /*hidden argument*/NULL);
		__this->___m_Content_10 = L_2;
		GUILayoutOptionU5BU5D_t6_165* L_3 = ___options;
		VirtActionInvoker1< GUILayoutOptionU5BU5D_t6_165* >::Invoke(10 /* System.Void UnityEngine.GUILayoutEntry::ApplyOptions(UnityEngine.GUILayoutOption[]) */, __this, L_3);
		float L_4 = (((GUILayoutEntry_t6_171 *)__this)->___minHeight_2);
		__this->___m_ForcedMinHeight_11 = L_4;
		float L_5 = (((GUILayoutEntry_t6_171 *)__this)->___maxHeight_3);
		__this->___m_ForcedMaxHeight_12 = L_5;
		return;
	}
}
// System.Void UnityEngine.GUIWordWrapSizer::CalcWidth()
extern "C" void GUIWordWrapSizer_CalcWidth_m6_1201 (GUIWordWrapSizer_t6_174 * __this, const MethodInfo* method)
{
	float V_0 = 0.0f;
	float V_1 = 0.0f;
	{
		float L_0 = (((GUILayoutEntry_t6_171 *)__this)->___minWidth_0);
		if ((((float)L_0) == ((float)(0.0f))))
		{
			goto IL_0020;
		}
	}
	{
		float L_1 = (((GUILayoutEntry_t6_171 *)__this)->___maxWidth_1);
		if ((!(((float)L_1) == ((float)(0.0f)))))
		{
			goto IL_0063;
		}
	}

IL_0020:
	{
		GUIStyle_t6_166 * L_2 = GUILayoutEntry_get_style_m6_1173(__this, /*hidden argument*/NULL);
		GUIContent_t6_163 * L_3 = (__this->___m_Content_10);
		NullCheck(L_2);
		GUIStyle_CalcMinMaxWidth_m6_1322(L_2, L_3, (&V_0), (&V_1), /*hidden argument*/NULL);
		float L_4 = (((GUILayoutEntry_t6_171 *)__this)->___minWidth_0);
		if ((!(((float)L_4) == ((float)(0.0f)))))
		{
			goto IL_004c;
		}
	}
	{
		float L_5 = V_0;
		((GUILayoutEntry_t6_171 *)__this)->___minWidth_0 = L_5;
	}

IL_004c:
	{
		float L_6 = (((GUILayoutEntry_t6_171 *)__this)->___maxWidth_1);
		if ((!(((float)L_6) == ((float)(0.0f)))))
		{
			goto IL_0063;
		}
	}
	{
		float L_7 = V_1;
		((GUILayoutEntry_t6_171 *)__this)->___maxWidth_1 = L_7;
	}

IL_0063:
	{
		return;
	}
}
// System.Void UnityEngine.GUIWordWrapSizer::CalcHeight()
extern "C" void GUIWordWrapSizer_CalcHeight_m6_1202 (GUIWordWrapSizer_t6_174 * __this, const MethodInfo* method)
{
	float V_0 = 0.0f;
	{
		float L_0 = (__this->___m_ForcedMinHeight_11);
		if ((((float)L_0) == ((float)(0.0f))))
		{
			goto IL_0020;
		}
	}
	{
		float L_1 = (__this->___m_ForcedMaxHeight_12);
		if ((!(((float)L_1) == ((float)(0.0f)))))
		{
			goto IL_008d;
		}
	}

IL_0020:
	{
		GUIStyle_t6_166 * L_2 = GUILayoutEntry_get_style_m6_1173(__this, /*hidden argument*/NULL);
		GUIContent_t6_163 * L_3 = (__this->___m_Content_10);
		Rect_t6_52 * L_4 = &(((GUILayoutEntry_t6_171 *)__this)->___rect_4);
		float L_5 = Rect_get_width_m6_277(L_4, /*hidden argument*/NULL);
		NullCheck(L_2);
		float L_6 = GUIStyle_CalcHeight_m6_1320(L_2, L_3, L_5, /*hidden argument*/NULL);
		V_0 = L_6;
		float L_7 = (__this->___m_ForcedMinHeight_11);
		if ((!(((float)L_7) == ((float)(0.0f)))))
		{
			goto IL_0059;
		}
	}
	{
		float L_8 = V_0;
		((GUILayoutEntry_t6_171 *)__this)->___minHeight_2 = L_8;
		goto IL_0065;
	}

IL_0059:
	{
		float L_9 = (__this->___m_ForcedMinHeight_11);
		((GUILayoutEntry_t6_171 *)__this)->___minHeight_2 = L_9;
	}

IL_0065:
	{
		float L_10 = (__this->___m_ForcedMaxHeight_12);
		if ((!(((float)L_10) == ((float)(0.0f)))))
		{
			goto IL_0081;
		}
	}
	{
		float L_11 = V_0;
		((GUILayoutEntry_t6_171 *)__this)->___maxHeight_3 = L_11;
		goto IL_008d;
	}

IL_0081:
	{
		float L_12 = (__this->___m_ForcedMaxHeight_12);
		((GUILayoutEntry_t6_171 *)__this)->___maxHeight_3 = L_12;
	}

IL_008d:
	{
		return;
	}
}
// System.Void UnityEngine.GUILayoutOption::.ctor(UnityEngine.GUILayoutOption/Type,System.Object)
extern "C" void GUILayoutOption__ctor_m6_1203 (GUILayoutOption_t6_176 * __this, int32_t ___type, Object_t * ___value, const MethodInfo* method)
{
	{
		Object__ctor_m1_0(__this, /*hidden argument*/NULL);
		int32_t L_0 = ___type;
		__this->___type_0 = L_0;
		Object_t * L_1 = ___value;
		__this->___value_1 = L_1;
		return;
	}
}
// System.Void UnityEngine.GUISettings::.ctor()
extern "C" void GUISettings__ctor_m6_1204 (GUISettings_t6_177 * __this, const MethodInfo* method)
{
	{
		__this->___m_DoubleClickSelectsWord_0 = 1;
		__this->___m_TripleClickSelectsLine_1 = 1;
		Color_t6_40  L_0 = Color_get_white_m6_236(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->___m_CursorColor_2 = L_0;
		__this->___m_CursorFlashSpeed_3 = (-1.0f);
		Color_t6_40  L_1 = {0};
		Color__ctor_m6_230(&L_1, (0.5f), (0.5f), (1.0f), /*hidden argument*/NULL);
		__this->___m_SelectionColor_4 = L_1;
		Object__ctor_m1_0(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean UnityEngine.GUISettings::get_doubleClickSelectsWord()
extern "C" bool GUISettings_get_doubleClickSelectsWord_m6_1205 (GUISettings_t6_177 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (__this->___m_DoubleClickSelectsWord_0);
		return L_0;
	}
}
// System.Boolean UnityEngine.GUISettings::get_tripleClickSelectsLine()
extern "C" bool GUISettings_get_tripleClickSelectsLine_m6_1206 (GUISettings_t6_177 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (__this->___m_TripleClickSelectsLine_1);
		return L_0;
	}
}
// UnityEngine.Color UnityEngine.GUISettings::get_cursorColor()
extern "C" Color_t6_40  GUISettings_get_cursorColor_m6_1207 (GUISettings_t6_177 * __this, const MethodInfo* method)
{
	{
		Color_t6_40  L_0 = (__this->___m_CursorColor_2);
		return L_0;
	}
}
// System.Single UnityEngine.GUISettings::get_cursorFlashSpeed()
extern "C" float GUISettings_get_cursorFlashSpeed_m6_1208 (GUISettings_t6_177 * __this, const MethodInfo* method)
{
	{
		float L_0 = (__this->___m_CursorFlashSpeed_3);
		if ((!(((float)L_0) >= ((float)(0.0f)))))
		{
			goto IL_0017;
		}
	}
	{
		float L_1 = (__this->___m_CursorFlashSpeed_3);
		return L_1;
	}

IL_0017:
	{
		float L_2 = GUISettings_Internal_GetCursorFlashSpeed_m6_1210(NULL /*static, unused*/, /*hidden argument*/NULL);
		return L_2;
	}
}
// UnityEngine.Color UnityEngine.GUISettings::get_selectionColor()
extern "C" Color_t6_40  GUISettings_get_selectionColor_m6_1209 (GUISettings_t6_177 * __this, const MethodInfo* method)
{
	{
		Color_t6_40  L_0 = (__this->___m_SelectionColor_4);
		return L_0;
	}
}
// System.Single UnityEngine.GUISettings::Internal_GetCursorFlashSpeed()
extern "C" float GUISettings_Internal_GetCursorFlashSpeed_m6_1210 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	typedef float (*GUISettings_Internal_GetCursorFlashSpeed_m6_1210_ftn) ();
	static GUISettings_Internal_GetCursorFlashSpeed_m6_1210_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GUISettings_Internal_GetCursorFlashSpeed_m6_1210_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.GUISettings::Internal_GetCursorFlashSpeed()");
	return _il2cpp_icall_func();
}
// System.Void UnityEngine.GUISkin/SkinChangedDelegate::.ctor(System.Object,System.IntPtr)
extern "C" void SkinChangedDelegate__ctor_m6_1211 (SkinChangedDelegate_t6_178 * __this, Object_t * ___object, IntPtr_t ___method, const MethodInfo* method)
{
	__this->___method_ptr_0 = (methodPointerType)((MethodInfo*)___method.___m_value_0)->method;
	__this->___method_3 = ___method;
	__this->___m_target_2 = ___object;
}
// System.Void UnityEngine.GUISkin/SkinChangedDelegate::Invoke()
extern "C" void SkinChangedDelegate_Invoke_m6_1212 (SkinChangedDelegate_t6_178 * __this, const MethodInfo* method)
{
	if(__this->___prev_9 != NULL)
	{
		SkinChangedDelegate_Invoke_m6_1212((SkinChangedDelegate_t6_178 *)__this->___prev_9, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->___method_3.___m_value_0));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->___method_3.___m_value_0));
	if ((__this->___m_target_2 != NULL || MethodHasParameters((MethodInfo*)(__this->___method_3.___m_value_0))) && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Object_t *, Object_t * __this, const MethodInfo* method);
		((FunctionPointerType)__this->___method_ptr_0)(NULL,__this->___m_target_2,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
	else
	{
		typedef void (*FunctionPointerType) (Object_t * __this, const MethodInfo* method);
		((FunctionPointerType)__this->___method_ptr_0)(__this->___m_target_2,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
}
extern "C" void pinvoke_delegate_wrapper_SkinChangedDelegate_t6_178(Il2CppObject* delegate)
{
	typedef void (STDCALL *native_function_ptr_type)();
	native_function_ptr_type _il2cpp_pinvoke_func = ((native_function_ptr_type)((Il2CppDelegate*)delegate)->method->method);
	// Native function invocation
	_il2cpp_pinvoke_func();

}
// System.IAsyncResult UnityEngine.GUISkin/SkinChangedDelegate::BeginInvoke(System.AsyncCallback,System.Object)
extern "C" Object_t * SkinChangedDelegate_BeginInvoke_m6_1213 (SkinChangedDelegate_t6_178 * __this, AsyncCallback_t1_28 * ___callback, Object_t * ___object, const MethodInfo* method)
{
	void *__d_args[1] = {0};
	return (Object_t *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback, (Il2CppObject*)___object);
}
// System.Void UnityEngine.GUISkin/SkinChangedDelegate::EndInvoke(System.IAsyncResult)
extern "C" void SkinChangedDelegate_EndInvoke_m6_1214 (SkinChangedDelegate_t6_178 * __this, Object_t * ___result, const MethodInfo* method)
{
	il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result, 0);
}
// System.Void UnityEngine.GUISkin::.ctor()
extern TypeInfo* GUISettings_t6_177_il2cpp_TypeInfo_var;
extern TypeInfo* GUIStyleU5BU5D_t6_179_il2cpp_TypeInfo_var;
extern "C" void GUISkin__ctor_m6_1215 (GUISkin_t6_161 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GUISettings_t6_177_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1005);
		GUIStyleU5BU5D_t6_179_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1006);
		s_Il2CppMethodIntialized = true;
	}
	{
		GUISettings_t6_177 * L_0 = (GUISettings_t6_177 *)il2cpp_codegen_object_new (GUISettings_t6_177_il2cpp_TypeInfo_var);
		GUISettings__ctor_m6_1204(L_0, /*hidden argument*/NULL);
		__this->___m_Settings_24 = L_0;
		ScriptableObject__ctor_m6_16(__this, /*hidden argument*/NULL);
		__this->___m_CustomStyles_23 = ((GUIStyleU5BU5D_t6_179*)SZArrayNew(GUIStyleU5BU5D_t6_179_il2cpp_TypeInfo_var, 1));
		return;
	}
}
// System.Void UnityEngine.GUISkin::OnEnable()
extern "C" void GUISkin_OnEnable_m6_1216 (GUISkin_t6_161 * __this, const MethodInfo* method)
{
	{
		GUISkin_Apply_m6_1263(__this, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Font UnityEngine.GUISkin::get_font()
extern "C" Font_t6_148 * GUISkin_get_font_m6_1217 (GUISkin_t6_161 * __this, const MethodInfo* method)
{
	{
		Font_t6_148 * L_0 = (__this->___m_Font_2);
		return L_0;
	}
}
// System.Void UnityEngine.GUISkin::set_font(UnityEngine.Font)
extern TypeInfo* GUISkin_t6_161_il2cpp_TypeInfo_var;
extern TypeInfo* GUIStyle_t6_166_il2cpp_TypeInfo_var;
extern "C" void GUISkin_set_font_m6_1218 (GUISkin_t6_161 * __this, Font_t6_148 * ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GUISkin_t6_161_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1007);
		GUIStyle_t6_166_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(988);
		s_Il2CppMethodIntialized = true;
	}
	{
		Font_t6_148 * L_0 = ___value;
		__this->___m_Font_2 = L_0;
		GUISkin_t6_161 * L_1 = ((GUISkin_t6_161_StaticFields*)GUISkin_t6_161_il2cpp_TypeInfo_var->static_fields)->___current_28;
		bool L_2 = Object_op_Equality_m6_579(NULL /*static, unused*/, L_1, __this, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_0022;
		}
	}
	{
		Font_t6_148 * L_3 = (__this->___m_Font_2);
		IL2CPP_RUNTIME_CLASS_INIT(GUIStyle_t6_166_il2cpp_TypeInfo_var);
		GUIStyle_SetDefaultFont_m6_1357(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
	}

IL_0022:
	{
		GUISkin_Apply_m6_1263(__this, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.GUIStyle UnityEngine.GUISkin::get_box()
extern "C" GUIStyle_t6_166 * GUISkin_get_box_m6_1219 (GUISkin_t6_161 * __this, const MethodInfo* method)
{
	{
		GUIStyle_t6_166 * L_0 = (__this->___m_box_3);
		return L_0;
	}
}
// System.Void UnityEngine.GUISkin::set_box(UnityEngine.GUIStyle)
extern "C" void GUISkin_set_box_m6_1220 (GUISkin_t6_161 * __this, GUIStyle_t6_166 * ___value, const MethodInfo* method)
{
	{
		GUIStyle_t6_166 * L_0 = ___value;
		__this->___m_box_3 = L_0;
		GUISkin_Apply_m6_1263(__this, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.GUIStyle UnityEngine.GUISkin::get_label()
extern "C" GUIStyle_t6_166 * GUISkin_get_label_m6_1221 (GUISkin_t6_161 * __this, const MethodInfo* method)
{
	{
		GUIStyle_t6_166 * L_0 = (__this->___m_label_6);
		return L_0;
	}
}
// System.Void UnityEngine.GUISkin::set_label(UnityEngine.GUIStyle)
extern "C" void GUISkin_set_label_m6_1222 (GUISkin_t6_161 * __this, GUIStyle_t6_166 * ___value, const MethodInfo* method)
{
	{
		GUIStyle_t6_166 * L_0 = ___value;
		__this->___m_label_6 = L_0;
		GUISkin_Apply_m6_1263(__this, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.GUIStyle UnityEngine.GUISkin::get_textField()
extern "C" GUIStyle_t6_166 * GUISkin_get_textField_m6_1223 (GUISkin_t6_161 * __this, const MethodInfo* method)
{
	{
		GUIStyle_t6_166 * L_0 = (__this->___m_textField_7);
		return L_0;
	}
}
// System.Void UnityEngine.GUISkin::set_textField(UnityEngine.GUIStyle)
extern "C" void GUISkin_set_textField_m6_1224 (GUISkin_t6_161 * __this, GUIStyle_t6_166 * ___value, const MethodInfo* method)
{
	{
		GUIStyle_t6_166 * L_0 = ___value;
		__this->___m_textField_7 = L_0;
		GUISkin_Apply_m6_1263(__this, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.GUIStyle UnityEngine.GUISkin::get_textArea()
extern "C" GUIStyle_t6_166 * GUISkin_get_textArea_m6_1225 (GUISkin_t6_161 * __this, const MethodInfo* method)
{
	{
		GUIStyle_t6_166 * L_0 = (__this->___m_textArea_8);
		return L_0;
	}
}
// System.Void UnityEngine.GUISkin::set_textArea(UnityEngine.GUIStyle)
extern "C" void GUISkin_set_textArea_m6_1226 (GUISkin_t6_161 * __this, GUIStyle_t6_166 * ___value, const MethodInfo* method)
{
	{
		GUIStyle_t6_166 * L_0 = ___value;
		__this->___m_textArea_8 = L_0;
		GUISkin_Apply_m6_1263(__this, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.GUIStyle UnityEngine.GUISkin::get_button()
extern "C" GUIStyle_t6_166 * GUISkin_get_button_m6_1227 (GUISkin_t6_161 * __this, const MethodInfo* method)
{
	{
		GUIStyle_t6_166 * L_0 = (__this->___m_button_4);
		return L_0;
	}
}
// System.Void UnityEngine.GUISkin::set_button(UnityEngine.GUIStyle)
extern "C" void GUISkin_set_button_m6_1228 (GUISkin_t6_161 * __this, GUIStyle_t6_166 * ___value, const MethodInfo* method)
{
	{
		GUIStyle_t6_166 * L_0 = ___value;
		__this->___m_button_4 = L_0;
		GUISkin_Apply_m6_1263(__this, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.GUIStyle UnityEngine.GUISkin::get_toggle()
extern "C" GUIStyle_t6_166 * GUISkin_get_toggle_m6_1229 (GUISkin_t6_161 * __this, const MethodInfo* method)
{
	{
		GUIStyle_t6_166 * L_0 = (__this->___m_toggle_5);
		return L_0;
	}
}
// System.Void UnityEngine.GUISkin::set_toggle(UnityEngine.GUIStyle)
extern "C" void GUISkin_set_toggle_m6_1230 (GUISkin_t6_161 * __this, GUIStyle_t6_166 * ___value, const MethodInfo* method)
{
	{
		GUIStyle_t6_166 * L_0 = ___value;
		__this->___m_toggle_5 = L_0;
		GUISkin_Apply_m6_1263(__this, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.GUIStyle UnityEngine.GUISkin::get_window()
extern "C" GUIStyle_t6_166 * GUISkin_get_window_m6_1231 (GUISkin_t6_161 * __this, const MethodInfo* method)
{
	{
		GUIStyle_t6_166 * L_0 = (__this->___m_window_9);
		return L_0;
	}
}
// System.Void UnityEngine.GUISkin::set_window(UnityEngine.GUIStyle)
extern "C" void GUISkin_set_window_m6_1232 (GUISkin_t6_161 * __this, GUIStyle_t6_166 * ___value, const MethodInfo* method)
{
	{
		GUIStyle_t6_166 * L_0 = ___value;
		__this->___m_window_9 = L_0;
		GUISkin_Apply_m6_1263(__this, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.GUIStyle UnityEngine.GUISkin::get_horizontalSlider()
extern "C" GUIStyle_t6_166 * GUISkin_get_horizontalSlider_m6_1233 (GUISkin_t6_161 * __this, const MethodInfo* method)
{
	{
		GUIStyle_t6_166 * L_0 = (__this->___m_horizontalSlider_10);
		return L_0;
	}
}
// System.Void UnityEngine.GUISkin::set_horizontalSlider(UnityEngine.GUIStyle)
extern "C" void GUISkin_set_horizontalSlider_m6_1234 (GUISkin_t6_161 * __this, GUIStyle_t6_166 * ___value, const MethodInfo* method)
{
	{
		GUIStyle_t6_166 * L_0 = ___value;
		__this->___m_horizontalSlider_10 = L_0;
		GUISkin_Apply_m6_1263(__this, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.GUIStyle UnityEngine.GUISkin::get_horizontalSliderThumb()
extern "C" GUIStyle_t6_166 * GUISkin_get_horizontalSliderThumb_m6_1235 (GUISkin_t6_161 * __this, const MethodInfo* method)
{
	{
		GUIStyle_t6_166 * L_0 = (__this->___m_horizontalSliderThumb_11);
		return L_0;
	}
}
// System.Void UnityEngine.GUISkin::set_horizontalSliderThumb(UnityEngine.GUIStyle)
extern "C" void GUISkin_set_horizontalSliderThumb_m6_1236 (GUISkin_t6_161 * __this, GUIStyle_t6_166 * ___value, const MethodInfo* method)
{
	{
		GUIStyle_t6_166 * L_0 = ___value;
		__this->___m_horizontalSliderThumb_11 = L_0;
		GUISkin_Apply_m6_1263(__this, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.GUIStyle UnityEngine.GUISkin::get_verticalSlider()
extern "C" GUIStyle_t6_166 * GUISkin_get_verticalSlider_m6_1237 (GUISkin_t6_161 * __this, const MethodInfo* method)
{
	{
		GUIStyle_t6_166 * L_0 = (__this->___m_verticalSlider_12);
		return L_0;
	}
}
// System.Void UnityEngine.GUISkin::set_verticalSlider(UnityEngine.GUIStyle)
extern "C" void GUISkin_set_verticalSlider_m6_1238 (GUISkin_t6_161 * __this, GUIStyle_t6_166 * ___value, const MethodInfo* method)
{
	{
		GUIStyle_t6_166 * L_0 = ___value;
		__this->___m_verticalSlider_12 = L_0;
		GUISkin_Apply_m6_1263(__this, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.GUIStyle UnityEngine.GUISkin::get_verticalSliderThumb()
extern "C" GUIStyle_t6_166 * GUISkin_get_verticalSliderThumb_m6_1239 (GUISkin_t6_161 * __this, const MethodInfo* method)
{
	{
		GUIStyle_t6_166 * L_0 = (__this->___m_verticalSliderThumb_13);
		return L_0;
	}
}
// System.Void UnityEngine.GUISkin::set_verticalSliderThumb(UnityEngine.GUIStyle)
extern "C" void GUISkin_set_verticalSliderThumb_m6_1240 (GUISkin_t6_161 * __this, GUIStyle_t6_166 * ___value, const MethodInfo* method)
{
	{
		GUIStyle_t6_166 * L_0 = ___value;
		__this->___m_verticalSliderThumb_13 = L_0;
		GUISkin_Apply_m6_1263(__this, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.GUIStyle UnityEngine.GUISkin::get_horizontalScrollbar()
extern "C" GUIStyle_t6_166 * GUISkin_get_horizontalScrollbar_m6_1241 (GUISkin_t6_161 * __this, const MethodInfo* method)
{
	{
		GUIStyle_t6_166 * L_0 = (__this->___m_horizontalScrollbar_14);
		return L_0;
	}
}
// System.Void UnityEngine.GUISkin::set_horizontalScrollbar(UnityEngine.GUIStyle)
extern "C" void GUISkin_set_horizontalScrollbar_m6_1242 (GUISkin_t6_161 * __this, GUIStyle_t6_166 * ___value, const MethodInfo* method)
{
	{
		GUIStyle_t6_166 * L_0 = ___value;
		__this->___m_horizontalScrollbar_14 = L_0;
		GUISkin_Apply_m6_1263(__this, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.GUIStyle UnityEngine.GUISkin::get_horizontalScrollbarThumb()
extern "C" GUIStyle_t6_166 * GUISkin_get_horizontalScrollbarThumb_m6_1243 (GUISkin_t6_161 * __this, const MethodInfo* method)
{
	{
		GUIStyle_t6_166 * L_0 = (__this->___m_horizontalScrollbarThumb_15);
		return L_0;
	}
}
// System.Void UnityEngine.GUISkin::set_horizontalScrollbarThumb(UnityEngine.GUIStyle)
extern "C" void GUISkin_set_horizontalScrollbarThumb_m6_1244 (GUISkin_t6_161 * __this, GUIStyle_t6_166 * ___value, const MethodInfo* method)
{
	{
		GUIStyle_t6_166 * L_0 = ___value;
		__this->___m_horizontalScrollbarThumb_15 = L_0;
		GUISkin_Apply_m6_1263(__this, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.GUIStyle UnityEngine.GUISkin::get_horizontalScrollbarLeftButton()
extern "C" GUIStyle_t6_166 * GUISkin_get_horizontalScrollbarLeftButton_m6_1245 (GUISkin_t6_161 * __this, const MethodInfo* method)
{
	{
		GUIStyle_t6_166 * L_0 = (__this->___m_horizontalScrollbarLeftButton_16);
		return L_0;
	}
}
// System.Void UnityEngine.GUISkin::set_horizontalScrollbarLeftButton(UnityEngine.GUIStyle)
extern "C" void GUISkin_set_horizontalScrollbarLeftButton_m6_1246 (GUISkin_t6_161 * __this, GUIStyle_t6_166 * ___value, const MethodInfo* method)
{
	{
		GUIStyle_t6_166 * L_0 = ___value;
		__this->___m_horizontalScrollbarLeftButton_16 = L_0;
		GUISkin_Apply_m6_1263(__this, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.GUIStyle UnityEngine.GUISkin::get_horizontalScrollbarRightButton()
extern "C" GUIStyle_t6_166 * GUISkin_get_horizontalScrollbarRightButton_m6_1247 (GUISkin_t6_161 * __this, const MethodInfo* method)
{
	{
		GUIStyle_t6_166 * L_0 = (__this->___m_horizontalScrollbarRightButton_17);
		return L_0;
	}
}
// System.Void UnityEngine.GUISkin::set_horizontalScrollbarRightButton(UnityEngine.GUIStyle)
extern "C" void GUISkin_set_horizontalScrollbarRightButton_m6_1248 (GUISkin_t6_161 * __this, GUIStyle_t6_166 * ___value, const MethodInfo* method)
{
	{
		GUIStyle_t6_166 * L_0 = ___value;
		__this->___m_horizontalScrollbarRightButton_17 = L_0;
		GUISkin_Apply_m6_1263(__this, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.GUIStyle UnityEngine.GUISkin::get_verticalScrollbar()
extern "C" GUIStyle_t6_166 * GUISkin_get_verticalScrollbar_m6_1249 (GUISkin_t6_161 * __this, const MethodInfo* method)
{
	{
		GUIStyle_t6_166 * L_0 = (__this->___m_verticalScrollbar_18);
		return L_0;
	}
}
// System.Void UnityEngine.GUISkin::set_verticalScrollbar(UnityEngine.GUIStyle)
extern "C" void GUISkin_set_verticalScrollbar_m6_1250 (GUISkin_t6_161 * __this, GUIStyle_t6_166 * ___value, const MethodInfo* method)
{
	{
		GUIStyle_t6_166 * L_0 = ___value;
		__this->___m_verticalScrollbar_18 = L_0;
		GUISkin_Apply_m6_1263(__this, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.GUIStyle UnityEngine.GUISkin::get_verticalScrollbarThumb()
extern "C" GUIStyle_t6_166 * GUISkin_get_verticalScrollbarThumb_m6_1251 (GUISkin_t6_161 * __this, const MethodInfo* method)
{
	{
		GUIStyle_t6_166 * L_0 = (__this->___m_verticalScrollbarThumb_19);
		return L_0;
	}
}
// System.Void UnityEngine.GUISkin::set_verticalScrollbarThumb(UnityEngine.GUIStyle)
extern "C" void GUISkin_set_verticalScrollbarThumb_m6_1252 (GUISkin_t6_161 * __this, GUIStyle_t6_166 * ___value, const MethodInfo* method)
{
	{
		GUIStyle_t6_166 * L_0 = ___value;
		__this->___m_verticalScrollbarThumb_19 = L_0;
		GUISkin_Apply_m6_1263(__this, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.GUIStyle UnityEngine.GUISkin::get_verticalScrollbarUpButton()
extern "C" GUIStyle_t6_166 * GUISkin_get_verticalScrollbarUpButton_m6_1253 (GUISkin_t6_161 * __this, const MethodInfo* method)
{
	{
		GUIStyle_t6_166 * L_0 = (__this->___m_verticalScrollbarUpButton_20);
		return L_0;
	}
}
// System.Void UnityEngine.GUISkin::set_verticalScrollbarUpButton(UnityEngine.GUIStyle)
extern "C" void GUISkin_set_verticalScrollbarUpButton_m6_1254 (GUISkin_t6_161 * __this, GUIStyle_t6_166 * ___value, const MethodInfo* method)
{
	{
		GUIStyle_t6_166 * L_0 = ___value;
		__this->___m_verticalScrollbarUpButton_20 = L_0;
		GUISkin_Apply_m6_1263(__this, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.GUIStyle UnityEngine.GUISkin::get_verticalScrollbarDownButton()
extern "C" GUIStyle_t6_166 * GUISkin_get_verticalScrollbarDownButton_m6_1255 (GUISkin_t6_161 * __this, const MethodInfo* method)
{
	{
		GUIStyle_t6_166 * L_0 = (__this->___m_verticalScrollbarDownButton_21);
		return L_0;
	}
}
// System.Void UnityEngine.GUISkin::set_verticalScrollbarDownButton(UnityEngine.GUIStyle)
extern "C" void GUISkin_set_verticalScrollbarDownButton_m6_1256 (GUISkin_t6_161 * __this, GUIStyle_t6_166 * ___value, const MethodInfo* method)
{
	{
		GUIStyle_t6_166 * L_0 = ___value;
		__this->___m_verticalScrollbarDownButton_21 = L_0;
		GUISkin_Apply_m6_1263(__this, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.GUIStyle UnityEngine.GUISkin::get_scrollView()
extern "C" GUIStyle_t6_166 * GUISkin_get_scrollView_m6_1257 (GUISkin_t6_161 * __this, const MethodInfo* method)
{
	{
		GUIStyle_t6_166 * L_0 = (__this->___m_ScrollView_22);
		return L_0;
	}
}
// System.Void UnityEngine.GUISkin::set_scrollView(UnityEngine.GUIStyle)
extern "C" void GUISkin_set_scrollView_m6_1258 (GUISkin_t6_161 * __this, GUIStyle_t6_166 * ___value, const MethodInfo* method)
{
	{
		GUIStyle_t6_166 * L_0 = ___value;
		__this->___m_ScrollView_22 = L_0;
		GUISkin_Apply_m6_1263(__this, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.GUIStyle[] UnityEngine.GUISkin::get_customStyles()
extern "C" GUIStyleU5BU5D_t6_179* GUISkin_get_customStyles_m6_1259 (GUISkin_t6_161 * __this, const MethodInfo* method)
{
	{
		GUIStyleU5BU5D_t6_179* L_0 = (__this->___m_CustomStyles_23);
		return L_0;
	}
}
// System.Void UnityEngine.GUISkin::set_customStyles(UnityEngine.GUIStyle[])
extern "C" void GUISkin_set_customStyles_m6_1260 (GUISkin_t6_161 * __this, GUIStyleU5BU5D_t6_179* ___value, const MethodInfo* method)
{
	{
		GUIStyleU5BU5D_t6_179* L_0 = ___value;
		__this->___m_CustomStyles_23 = L_0;
		GUISkin_Apply_m6_1263(__this, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.GUISettings UnityEngine.GUISkin::get_settings()
extern "C" GUISettings_t6_177 * GUISkin_get_settings_m6_1261 (GUISkin_t6_161 * __this, const MethodInfo* method)
{
	{
		GUISettings_t6_177 * L_0 = (__this->___m_Settings_24);
		return L_0;
	}
}
// UnityEngine.GUIStyle UnityEngine.GUISkin::get_error()
extern TypeInfo* GUISkin_t6_161_il2cpp_TypeInfo_var;
extern TypeInfo* GUIStyle_t6_166_il2cpp_TypeInfo_var;
extern "C" GUIStyle_t6_166 * GUISkin_get_error_m6_1262 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GUISkin_t6_161_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1007);
		GUIStyle_t6_166_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(988);
		s_Il2CppMethodIntialized = true;
	}
	{
		GUIStyle_t6_166 * L_0 = ((GUISkin_t6_161_StaticFields*)GUISkin_t6_161_il2cpp_TypeInfo_var->static_fields)->___ms_Error_25;
		if (L_0)
		{
			goto IL_0014;
		}
	}
	{
		GUIStyle_t6_166 * L_1 = (GUIStyle_t6_166 *)il2cpp_codegen_object_new (GUIStyle_t6_166_il2cpp_TypeInfo_var);
		GUIStyle__ctor_m6_1298(L_1, /*hidden argument*/NULL);
		((GUISkin_t6_161_StaticFields*)GUISkin_t6_161_il2cpp_TypeInfo_var->static_fields)->___ms_Error_25 = L_1;
	}

IL_0014:
	{
		GUIStyle_t6_166 * L_2 = ((GUISkin_t6_161_StaticFields*)GUISkin_t6_161_il2cpp_TypeInfo_var->static_fields)->___ms_Error_25;
		return L_2;
	}
}
// System.Void UnityEngine.GUISkin::Apply()
extern Il2CppCodeGenString* _stringLiteral2803;
extern "C" void GUISkin_Apply_m6_1263 (GUISkin_t6_161 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		_stringLiteral2803 = il2cpp_codegen_string_literal_from_index(2803);
		s_Il2CppMethodIntialized = true;
	}
	{
		GUIStyleU5BU5D_t6_179* L_0 = (__this->___m_CustomStyles_23);
		if (L_0)
		{
			goto IL_0015;
		}
	}
	{
		Debug_Log_m6_489(NULL /*static, unused*/, _stringLiteral2803, /*hidden argument*/NULL);
	}

IL_0015:
	{
		GUISkin_BuildStyleCache_m6_1264(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.GUISkin::BuildStyleCache()
extern TypeInfo* GUIStyle_t6_166_il2cpp_TypeInfo_var;
extern TypeInfo* StringComparer_t1_761_il2cpp_TypeInfo_var;
extern TypeInfo* Dictionary_2_t1_918_il2cpp_TypeInfo_var;
extern const MethodInfo* Dictionary_2__ctor_m1_5608_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral1031;
extern Il2CppCodeGenString* _stringLiteral2804;
extern Il2CppCodeGenString* _stringLiteral2805;
extern Il2CppCodeGenString* _stringLiteral2806;
extern Il2CppCodeGenString* _stringLiteral2807;
extern Il2CppCodeGenString* _stringLiteral2808;
extern Il2CppCodeGenString* _stringLiteral2809;
extern Il2CppCodeGenString* _stringLiteral2810;
extern Il2CppCodeGenString* _stringLiteral2811;
extern Il2CppCodeGenString* _stringLiteral2812;
extern Il2CppCodeGenString* _stringLiteral2813;
extern Il2CppCodeGenString* _stringLiteral2814;
extern Il2CppCodeGenString* _stringLiteral2815;
extern Il2CppCodeGenString* _stringLiteral2816;
extern Il2CppCodeGenString* _stringLiteral2817;
extern Il2CppCodeGenString* _stringLiteral2818;
extern Il2CppCodeGenString* _stringLiteral2819;
extern Il2CppCodeGenString* _stringLiteral2820;
extern Il2CppCodeGenString* _stringLiteral2821;
extern Il2CppCodeGenString* _stringLiteral2822;
extern "C" void GUISkin_BuildStyleCache_m6_1264 (GUISkin_t6_161 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GUIStyle_t6_166_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(988);
		StringComparer_t1_761_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(607);
		Dictionary_2_t1_918_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1008);
		Dictionary_2__ctor_m1_5608_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483816);
		_stringLiteral1031 = il2cpp_codegen_string_literal_from_index(1031);
		_stringLiteral2804 = il2cpp_codegen_string_literal_from_index(2804);
		_stringLiteral2805 = il2cpp_codegen_string_literal_from_index(2805);
		_stringLiteral2806 = il2cpp_codegen_string_literal_from_index(2806);
		_stringLiteral2807 = il2cpp_codegen_string_literal_from_index(2807);
		_stringLiteral2808 = il2cpp_codegen_string_literal_from_index(2808);
		_stringLiteral2809 = il2cpp_codegen_string_literal_from_index(2809);
		_stringLiteral2810 = il2cpp_codegen_string_literal_from_index(2810);
		_stringLiteral2811 = il2cpp_codegen_string_literal_from_index(2811);
		_stringLiteral2812 = il2cpp_codegen_string_literal_from_index(2812);
		_stringLiteral2813 = il2cpp_codegen_string_literal_from_index(2813);
		_stringLiteral2814 = il2cpp_codegen_string_literal_from_index(2814);
		_stringLiteral2815 = il2cpp_codegen_string_literal_from_index(2815);
		_stringLiteral2816 = il2cpp_codegen_string_literal_from_index(2816);
		_stringLiteral2817 = il2cpp_codegen_string_literal_from_index(2817);
		_stringLiteral2818 = il2cpp_codegen_string_literal_from_index(2818);
		_stringLiteral2819 = il2cpp_codegen_string_literal_from_index(2819);
		_stringLiteral2820 = il2cpp_codegen_string_literal_from_index(2820);
		_stringLiteral2821 = il2cpp_codegen_string_literal_from_index(2821);
		_stringLiteral2822 = il2cpp_codegen_string_literal_from_index(2822);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	{
		GUIStyle_t6_166 * L_0 = (__this->___m_box_3);
		if (L_0)
		{
			goto IL_0016;
		}
	}
	{
		GUIStyle_t6_166 * L_1 = (GUIStyle_t6_166 *)il2cpp_codegen_object_new (GUIStyle_t6_166_il2cpp_TypeInfo_var);
		GUIStyle__ctor_m6_1298(L_1, /*hidden argument*/NULL);
		__this->___m_box_3 = L_1;
	}

IL_0016:
	{
		GUIStyle_t6_166 * L_2 = (__this->___m_button_4);
		if (L_2)
		{
			goto IL_002c;
		}
	}
	{
		GUIStyle_t6_166 * L_3 = (GUIStyle_t6_166 *)il2cpp_codegen_object_new (GUIStyle_t6_166_il2cpp_TypeInfo_var);
		GUIStyle__ctor_m6_1298(L_3, /*hidden argument*/NULL);
		__this->___m_button_4 = L_3;
	}

IL_002c:
	{
		GUIStyle_t6_166 * L_4 = (__this->___m_toggle_5);
		if (L_4)
		{
			goto IL_0042;
		}
	}
	{
		GUIStyle_t6_166 * L_5 = (GUIStyle_t6_166 *)il2cpp_codegen_object_new (GUIStyle_t6_166_il2cpp_TypeInfo_var);
		GUIStyle__ctor_m6_1298(L_5, /*hidden argument*/NULL);
		__this->___m_toggle_5 = L_5;
	}

IL_0042:
	{
		GUIStyle_t6_166 * L_6 = (__this->___m_label_6);
		if (L_6)
		{
			goto IL_0058;
		}
	}
	{
		GUIStyle_t6_166 * L_7 = (GUIStyle_t6_166 *)il2cpp_codegen_object_new (GUIStyle_t6_166_il2cpp_TypeInfo_var);
		GUIStyle__ctor_m6_1298(L_7, /*hidden argument*/NULL);
		__this->___m_label_6 = L_7;
	}

IL_0058:
	{
		GUIStyle_t6_166 * L_8 = (__this->___m_window_9);
		if (L_8)
		{
			goto IL_006e;
		}
	}
	{
		GUIStyle_t6_166 * L_9 = (GUIStyle_t6_166 *)il2cpp_codegen_object_new (GUIStyle_t6_166_il2cpp_TypeInfo_var);
		GUIStyle__ctor_m6_1298(L_9, /*hidden argument*/NULL);
		__this->___m_window_9 = L_9;
	}

IL_006e:
	{
		GUIStyle_t6_166 * L_10 = (__this->___m_textField_7);
		if (L_10)
		{
			goto IL_0084;
		}
	}
	{
		GUIStyle_t6_166 * L_11 = (GUIStyle_t6_166 *)il2cpp_codegen_object_new (GUIStyle_t6_166_il2cpp_TypeInfo_var);
		GUIStyle__ctor_m6_1298(L_11, /*hidden argument*/NULL);
		__this->___m_textField_7 = L_11;
	}

IL_0084:
	{
		GUIStyle_t6_166 * L_12 = (__this->___m_textArea_8);
		if (L_12)
		{
			goto IL_009a;
		}
	}
	{
		GUIStyle_t6_166 * L_13 = (GUIStyle_t6_166 *)il2cpp_codegen_object_new (GUIStyle_t6_166_il2cpp_TypeInfo_var);
		GUIStyle__ctor_m6_1298(L_13, /*hidden argument*/NULL);
		__this->___m_textArea_8 = L_13;
	}

IL_009a:
	{
		GUIStyle_t6_166 * L_14 = (__this->___m_horizontalSlider_10);
		if (L_14)
		{
			goto IL_00b0;
		}
	}
	{
		GUIStyle_t6_166 * L_15 = (GUIStyle_t6_166 *)il2cpp_codegen_object_new (GUIStyle_t6_166_il2cpp_TypeInfo_var);
		GUIStyle__ctor_m6_1298(L_15, /*hidden argument*/NULL);
		__this->___m_horizontalSlider_10 = L_15;
	}

IL_00b0:
	{
		GUIStyle_t6_166 * L_16 = (__this->___m_horizontalSliderThumb_11);
		if (L_16)
		{
			goto IL_00c6;
		}
	}
	{
		GUIStyle_t6_166 * L_17 = (GUIStyle_t6_166 *)il2cpp_codegen_object_new (GUIStyle_t6_166_il2cpp_TypeInfo_var);
		GUIStyle__ctor_m6_1298(L_17, /*hidden argument*/NULL);
		__this->___m_horizontalSliderThumb_11 = L_17;
	}

IL_00c6:
	{
		GUIStyle_t6_166 * L_18 = (__this->___m_verticalSlider_12);
		if (L_18)
		{
			goto IL_00dc;
		}
	}
	{
		GUIStyle_t6_166 * L_19 = (GUIStyle_t6_166 *)il2cpp_codegen_object_new (GUIStyle_t6_166_il2cpp_TypeInfo_var);
		GUIStyle__ctor_m6_1298(L_19, /*hidden argument*/NULL);
		__this->___m_verticalSlider_12 = L_19;
	}

IL_00dc:
	{
		GUIStyle_t6_166 * L_20 = (__this->___m_verticalSliderThumb_13);
		if (L_20)
		{
			goto IL_00f2;
		}
	}
	{
		GUIStyle_t6_166 * L_21 = (GUIStyle_t6_166 *)il2cpp_codegen_object_new (GUIStyle_t6_166_il2cpp_TypeInfo_var);
		GUIStyle__ctor_m6_1298(L_21, /*hidden argument*/NULL);
		__this->___m_verticalSliderThumb_13 = L_21;
	}

IL_00f2:
	{
		GUIStyle_t6_166 * L_22 = (__this->___m_horizontalScrollbar_14);
		if (L_22)
		{
			goto IL_0108;
		}
	}
	{
		GUIStyle_t6_166 * L_23 = (GUIStyle_t6_166 *)il2cpp_codegen_object_new (GUIStyle_t6_166_il2cpp_TypeInfo_var);
		GUIStyle__ctor_m6_1298(L_23, /*hidden argument*/NULL);
		__this->___m_horizontalScrollbar_14 = L_23;
	}

IL_0108:
	{
		GUIStyle_t6_166 * L_24 = (__this->___m_horizontalScrollbarThumb_15);
		if (L_24)
		{
			goto IL_011e;
		}
	}
	{
		GUIStyle_t6_166 * L_25 = (GUIStyle_t6_166 *)il2cpp_codegen_object_new (GUIStyle_t6_166_il2cpp_TypeInfo_var);
		GUIStyle__ctor_m6_1298(L_25, /*hidden argument*/NULL);
		__this->___m_horizontalScrollbarThumb_15 = L_25;
	}

IL_011e:
	{
		GUIStyle_t6_166 * L_26 = (__this->___m_horizontalScrollbarLeftButton_16);
		if (L_26)
		{
			goto IL_0134;
		}
	}
	{
		GUIStyle_t6_166 * L_27 = (GUIStyle_t6_166 *)il2cpp_codegen_object_new (GUIStyle_t6_166_il2cpp_TypeInfo_var);
		GUIStyle__ctor_m6_1298(L_27, /*hidden argument*/NULL);
		__this->___m_horizontalScrollbarLeftButton_16 = L_27;
	}

IL_0134:
	{
		GUIStyle_t6_166 * L_28 = (__this->___m_horizontalScrollbarRightButton_17);
		if (L_28)
		{
			goto IL_014a;
		}
	}
	{
		GUIStyle_t6_166 * L_29 = (GUIStyle_t6_166 *)il2cpp_codegen_object_new (GUIStyle_t6_166_il2cpp_TypeInfo_var);
		GUIStyle__ctor_m6_1298(L_29, /*hidden argument*/NULL);
		__this->___m_horizontalScrollbarRightButton_17 = L_29;
	}

IL_014a:
	{
		GUIStyle_t6_166 * L_30 = (__this->___m_verticalScrollbar_18);
		if (L_30)
		{
			goto IL_0160;
		}
	}
	{
		GUIStyle_t6_166 * L_31 = (GUIStyle_t6_166 *)il2cpp_codegen_object_new (GUIStyle_t6_166_il2cpp_TypeInfo_var);
		GUIStyle__ctor_m6_1298(L_31, /*hidden argument*/NULL);
		__this->___m_verticalScrollbar_18 = L_31;
	}

IL_0160:
	{
		GUIStyle_t6_166 * L_32 = (__this->___m_verticalScrollbarThumb_19);
		if (L_32)
		{
			goto IL_0176;
		}
	}
	{
		GUIStyle_t6_166 * L_33 = (GUIStyle_t6_166 *)il2cpp_codegen_object_new (GUIStyle_t6_166_il2cpp_TypeInfo_var);
		GUIStyle__ctor_m6_1298(L_33, /*hidden argument*/NULL);
		__this->___m_verticalScrollbarThumb_19 = L_33;
	}

IL_0176:
	{
		GUIStyle_t6_166 * L_34 = (__this->___m_verticalScrollbarUpButton_20);
		if (L_34)
		{
			goto IL_018c;
		}
	}
	{
		GUIStyle_t6_166 * L_35 = (GUIStyle_t6_166 *)il2cpp_codegen_object_new (GUIStyle_t6_166_il2cpp_TypeInfo_var);
		GUIStyle__ctor_m6_1298(L_35, /*hidden argument*/NULL);
		__this->___m_verticalScrollbarUpButton_20 = L_35;
	}

IL_018c:
	{
		GUIStyle_t6_166 * L_36 = (__this->___m_verticalScrollbarDownButton_21);
		if (L_36)
		{
			goto IL_01a2;
		}
	}
	{
		GUIStyle_t6_166 * L_37 = (GUIStyle_t6_166 *)il2cpp_codegen_object_new (GUIStyle_t6_166_il2cpp_TypeInfo_var);
		GUIStyle__ctor_m6_1298(L_37, /*hidden argument*/NULL);
		__this->___m_verticalScrollbarDownButton_21 = L_37;
	}

IL_01a2:
	{
		GUIStyle_t6_166 * L_38 = (__this->___m_ScrollView_22);
		if (L_38)
		{
			goto IL_01b8;
		}
	}
	{
		GUIStyle_t6_166 * L_39 = (GUIStyle_t6_166 *)il2cpp_codegen_object_new (GUIStyle_t6_166_il2cpp_TypeInfo_var);
		GUIStyle__ctor_m6_1298(L_39, /*hidden argument*/NULL);
		__this->___m_ScrollView_22 = L_39;
	}

IL_01b8:
	{
		IL2CPP_RUNTIME_CLASS_INIT(StringComparer_t1_761_il2cpp_TypeInfo_var);
		StringComparer_t1_761 * L_40 = StringComparer_get_OrdinalIgnoreCase_m1_5343(NULL /*static, unused*/, /*hidden argument*/NULL);
		Dictionary_2_t1_918 * L_41 = (Dictionary_2_t1_918 *)il2cpp_codegen_object_new (Dictionary_2_t1_918_il2cpp_TypeInfo_var);
		Dictionary_2__ctor_m1_5608(L_41, L_40, /*hidden argument*/Dictionary_2__ctor_m1_5608_MethodInfo_var);
		__this->___m_Styles_26 = L_41;
		Dictionary_2_t1_918 * L_42 = (__this->___m_Styles_26);
		GUIStyle_t6_166 * L_43 = (__this->___m_box_3);
		NullCheck(L_42);
		VirtActionInvoker2< String_t*, GUIStyle_t6_166 * >::Invoke(26 /* System.Void System.Collections.Generic.Dictionary`2<System.String,UnityEngine.GUIStyle>::set_Item(!0,!1) */, L_42, _stringLiteral1031, L_43);
		GUIStyle_t6_166 * L_44 = (__this->___m_box_3);
		NullCheck(L_44);
		GUIStyle_set_name_m6_1328(L_44, _stringLiteral1031, /*hidden argument*/NULL);
		Dictionary_2_t1_918 * L_45 = (__this->___m_Styles_26);
		GUIStyle_t6_166 * L_46 = (__this->___m_button_4);
		NullCheck(L_45);
		VirtActionInvoker2< String_t*, GUIStyle_t6_166 * >::Invoke(26 /* System.Void System.Collections.Generic.Dictionary`2<System.String,UnityEngine.GUIStyle>::set_Item(!0,!1) */, L_45, _stringLiteral2804, L_46);
		GUIStyle_t6_166 * L_47 = (__this->___m_button_4);
		NullCheck(L_47);
		GUIStyle_set_name_m6_1328(L_47, _stringLiteral2804, /*hidden argument*/NULL);
		Dictionary_2_t1_918 * L_48 = (__this->___m_Styles_26);
		GUIStyle_t6_166 * L_49 = (__this->___m_toggle_5);
		NullCheck(L_48);
		VirtActionInvoker2< String_t*, GUIStyle_t6_166 * >::Invoke(26 /* System.Void System.Collections.Generic.Dictionary`2<System.String,UnityEngine.GUIStyle>::set_Item(!0,!1) */, L_48, _stringLiteral2805, L_49);
		GUIStyle_t6_166 * L_50 = (__this->___m_toggle_5);
		NullCheck(L_50);
		GUIStyle_set_name_m6_1328(L_50, _stringLiteral2805, /*hidden argument*/NULL);
		Dictionary_2_t1_918 * L_51 = (__this->___m_Styles_26);
		GUIStyle_t6_166 * L_52 = (__this->___m_label_6);
		NullCheck(L_51);
		VirtActionInvoker2< String_t*, GUIStyle_t6_166 * >::Invoke(26 /* System.Void System.Collections.Generic.Dictionary`2<System.String,UnityEngine.GUIStyle>::set_Item(!0,!1) */, L_51, _stringLiteral2806, L_52);
		GUIStyle_t6_166 * L_53 = (__this->___m_label_6);
		NullCheck(L_53);
		GUIStyle_set_name_m6_1328(L_53, _stringLiteral2806, /*hidden argument*/NULL);
		Dictionary_2_t1_918 * L_54 = (__this->___m_Styles_26);
		GUIStyle_t6_166 * L_55 = (__this->___m_window_9);
		NullCheck(L_54);
		VirtActionInvoker2< String_t*, GUIStyle_t6_166 * >::Invoke(26 /* System.Void System.Collections.Generic.Dictionary`2<System.String,UnityEngine.GUIStyle>::set_Item(!0,!1) */, L_54, _stringLiteral2807, L_55);
		GUIStyle_t6_166 * L_56 = (__this->___m_window_9);
		NullCheck(L_56);
		GUIStyle_set_name_m6_1328(L_56, _stringLiteral2807, /*hidden argument*/NULL);
		Dictionary_2_t1_918 * L_57 = (__this->___m_Styles_26);
		GUIStyle_t6_166 * L_58 = (__this->___m_textField_7);
		NullCheck(L_57);
		VirtActionInvoker2< String_t*, GUIStyle_t6_166 * >::Invoke(26 /* System.Void System.Collections.Generic.Dictionary`2<System.String,UnityEngine.GUIStyle>::set_Item(!0,!1) */, L_57, _stringLiteral2808, L_58);
		GUIStyle_t6_166 * L_59 = (__this->___m_textField_7);
		NullCheck(L_59);
		GUIStyle_set_name_m6_1328(L_59, _stringLiteral2808, /*hidden argument*/NULL);
		Dictionary_2_t1_918 * L_60 = (__this->___m_Styles_26);
		GUIStyle_t6_166 * L_61 = (__this->___m_textArea_8);
		NullCheck(L_60);
		VirtActionInvoker2< String_t*, GUIStyle_t6_166 * >::Invoke(26 /* System.Void System.Collections.Generic.Dictionary`2<System.String,UnityEngine.GUIStyle>::set_Item(!0,!1) */, L_60, _stringLiteral2809, L_61);
		GUIStyle_t6_166 * L_62 = (__this->___m_textArea_8);
		NullCheck(L_62);
		GUIStyle_set_name_m6_1328(L_62, _stringLiteral2809, /*hidden argument*/NULL);
		Dictionary_2_t1_918 * L_63 = (__this->___m_Styles_26);
		GUIStyle_t6_166 * L_64 = (__this->___m_horizontalSlider_10);
		NullCheck(L_63);
		VirtActionInvoker2< String_t*, GUIStyle_t6_166 * >::Invoke(26 /* System.Void System.Collections.Generic.Dictionary`2<System.String,UnityEngine.GUIStyle>::set_Item(!0,!1) */, L_63, _stringLiteral2810, L_64);
		GUIStyle_t6_166 * L_65 = (__this->___m_horizontalSlider_10);
		NullCheck(L_65);
		GUIStyle_set_name_m6_1328(L_65, _stringLiteral2810, /*hidden argument*/NULL);
		Dictionary_2_t1_918 * L_66 = (__this->___m_Styles_26);
		GUIStyle_t6_166 * L_67 = (__this->___m_horizontalSliderThumb_11);
		NullCheck(L_66);
		VirtActionInvoker2< String_t*, GUIStyle_t6_166 * >::Invoke(26 /* System.Void System.Collections.Generic.Dictionary`2<System.String,UnityEngine.GUIStyle>::set_Item(!0,!1) */, L_66, _stringLiteral2811, L_67);
		GUIStyle_t6_166 * L_68 = (__this->___m_horizontalSliderThumb_11);
		NullCheck(L_68);
		GUIStyle_set_name_m6_1328(L_68, _stringLiteral2811, /*hidden argument*/NULL);
		Dictionary_2_t1_918 * L_69 = (__this->___m_Styles_26);
		GUIStyle_t6_166 * L_70 = (__this->___m_verticalSlider_12);
		NullCheck(L_69);
		VirtActionInvoker2< String_t*, GUIStyle_t6_166 * >::Invoke(26 /* System.Void System.Collections.Generic.Dictionary`2<System.String,UnityEngine.GUIStyle>::set_Item(!0,!1) */, L_69, _stringLiteral2812, L_70);
		GUIStyle_t6_166 * L_71 = (__this->___m_verticalSlider_12);
		NullCheck(L_71);
		GUIStyle_set_name_m6_1328(L_71, _stringLiteral2812, /*hidden argument*/NULL);
		Dictionary_2_t1_918 * L_72 = (__this->___m_Styles_26);
		GUIStyle_t6_166 * L_73 = (__this->___m_verticalSliderThumb_13);
		NullCheck(L_72);
		VirtActionInvoker2< String_t*, GUIStyle_t6_166 * >::Invoke(26 /* System.Void System.Collections.Generic.Dictionary`2<System.String,UnityEngine.GUIStyle>::set_Item(!0,!1) */, L_72, _stringLiteral2813, L_73);
		GUIStyle_t6_166 * L_74 = (__this->___m_verticalSliderThumb_13);
		NullCheck(L_74);
		GUIStyle_set_name_m6_1328(L_74, _stringLiteral2813, /*hidden argument*/NULL);
		Dictionary_2_t1_918 * L_75 = (__this->___m_Styles_26);
		GUIStyle_t6_166 * L_76 = (__this->___m_horizontalScrollbar_14);
		NullCheck(L_75);
		VirtActionInvoker2< String_t*, GUIStyle_t6_166 * >::Invoke(26 /* System.Void System.Collections.Generic.Dictionary`2<System.String,UnityEngine.GUIStyle>::set_Item(!0,!1) */, L_75, _stringLiteral2814, L_76);
		GUIStyle_t6_166 * L_77 = (__this->___m_horizontalScrollbar_14);
		NullCheck(L_77);
		GUIStyle_set_name_m6_1328(L_77, _stringLiteral2814, /*hidden argument*/NULL);
		Dictionary_2_t1_918 * L_78 = (__this->___m_Styles_26);
		GUIStyle_t6_166 * L_79 = (__this->___m_horizontalScrollbarThumb_15);
		NullCheck(L_78);
		VirtActionInvoker2< String_t*, GUIStyle_t6_166 * >::Invoke(26 /* System.Void System.Collections.Generic.Dictionary`2<System.String,UnityEngine.GUIStyle>::set_Item(!0,!1) */, L_78, _stringLiteral2815, L_79);
		GUIStyle_t6_166 * L_80 = (__this->___m_horizontalScrollbarThumb_15);
		NullCheck(L_80);
		GUIStyle_set_name_m6_1328(L_80, _stringLiteral2815, /*hidden argument*/NULL);
		Dictionary_2_t1_918 * L_81 = (__this->___m_Styles_26);
		GUIStyle_t6_166 * L_82 = (__this->___m_horizontalScrollbarLeftButton_16);
		NullCheck(L_81);
		VirtActionInvoker2< String_t*, GUIStyle_t6_166 * >::Invoke(26 /* System.Void System.Collections.Generic.Dictionary`2<System.String,UnityEngine.GUIStyle>::set_Item(!0,!1) */, L_81, _stringLiteral2816, L_82);
		GUIStyle_t6_166 * L_83 = (__this->___m_horizontalScrollbarLeftButton_16);
		NullCheck(L_83);
		GUIStyle_set_name_m6_1328(L_83, _stringLiteral2816, /*hidden argument*/NULL);
		Dictionary_2_t1_918 * L_84 = (__this->___m_Styles_26);
		GUIStyle_t6_166 * L_85 = (__this->___m_horizontalScrollbarRightButton_17);
		NullCheck(L_84);
		VirtActionInvoker2< String_t*, GUIStyle_t6_166 * >::Invoke(26 /* System.Void System.Collections.Generic.Dictionary`2<System.String,UnityEngine.GUIStyle>::set_Item(!0,!1) */, L_84, _stringLiteral2817, L_85);
		GUIStyle_t6_166 * L_86 = (__this->___m_horizontalScrollbarRightButton_17);
		NullCheck(L_86);
		GUIStyle_set_name_m6_1328(L_86, _stringLiteral2817, /*hidden argument*/NULL);
		Dictionary_2_t1_918 * L_87 = (__this->___m_Styles_26);
		GUIStyle_t6_166 * L_88 = (__this->___m_verticalScrollbar_18);
		NullCheck(L_87);
		VirtActionInvoker2< String_t*, GUIStyle_t6_166 * >::Invoke(26 /* System.Void System.Collections.Generic.Dictionary`2<System.String,UnityEngine.GUIStyle>::set_Item(!0,!1) */, L_87, _stringLiteral2818, L_88);
		GUIStyle_t6_166 * L_89 = (__this->___m_verticalScrollbar_18);
		NullCheck(L_89);
		GUIStyle_set_name_m6_1328(L_89, _stringLiteral2818, /*hidden argument*/NULL);
		Dictionary_2_t1_918 * L_90 = (__this->___m_Styles_26);
		GUIStyle_t6_166 * L_91 = (__this->___m_verticalScrollbarThumb_19);
		NullCheck(L_90);
		VirtActionInvoker2< String_t*, GUIStyle_t6_166 * >::Invoke(26 /* System.Void System.Collections.Generic.Dictionary`2<System.String,UnityEngine.GUIStyle>::set_Item(!0,!1) */, L_90, _stringLiteral2819, L_91);
		GUIStyle_t6_166 * L_92 = (__this->___m_verticalScrollbarThumb_19);
		NullCheck(L_92);
		GUIStyle_set_name_m6_1328(L_92, _stringLiteral2819, /*hidden argument*/NULL);
		Dictionary_2_t1_918 * L_93 = (__this->___m_Styles_26);
		GUIStyle_t6_166 * L_94 = (__this->___m_verticalScrollbarUpButton_20);
		NullCheck(L_93);
		VirtActionInvoker2< String_t*, GUIStyle_t6_166 * >::Invoke(26 /* System.Void System.Collections.Generic.Dictionary`2<System.String,UnityEngine.GUIStyle>::set_Item(!0,!1) */, L_93, _stringLiteral2820, L_94);
		GUIStyle_t6_166 * L_95 = (__this->___m_verticalScrollbarUpButton_20);
		NullCheck(L_95);
		GUIStyle_set_name_m6_1328(L_95, _stringLiteral2820, /*hidden argument*/NULL);
		Dictionary_2_t1_918 * L_96 = (__this->___m_Styles_26);
		GUIStyle_t6_166 * L_97 = (__this->___m_verticalScrollbarDownButton_21);
		NullCheck(L_96);
		VirtActionInvoker2< String_t*, GUIStyle_t6_166 * >::Invoke(26 /* System.Void System.Collections.Generic.Dictionary`2<System.String,UnityEngine.GUIStyle>::set_Item(!0,!1) */, L_96, _stringLiteral2821, L_97);
		GUIStyle_t6_166 * L_98 = (__this->___m_verticalScrollbarDownButton_21);
		NullCheck(L_98);
		GUIStyle_set_name_m6_1328(L_98, _stringLiteral2821, /*hidden argument*/NULL);
		Dictionary_2_t1_918 * L_99 = (__this->___m_Styles_26);
		GUIStyle_t6_166 * L_100 = (__this->___m_ScrollView_22);
		NullCheck(L_99);
		VirtActionInvoker2< String_t*, GUIStyle_t6_166 * >::Invoke(26 /* System.Void System.Collections.Generic.Dictionary`2<System.String,UnityEngine.GUIStyle>::set_Item(!0,!1) */, L_99, _stringLiteral2822, L_100);
		GUIStyle_t6_166 * L_101 = (__this->___m_ScrollView_22);
		NullCheck(L_101);
		GUIStyle_set_name_m6_1328(L_101, _stringLiteral2822, /*hidden argument*/NULL);
		GUIStyleU5BU5D_t6_179* L_102 = (__this->___m_CustomStyles_23);
		if (!L_102)
		{
			goto IL_0516;
		}
	}
	{
		V_0 = 0;
		goto IL_0508;
	}

IL_04d2:
	{
		GUIStyleU5BU5D_t6_179* L_103 = (__this->___m_CustomStyles_23);
		int32_t L_104 = V_0;
		NullCheck(L_103);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_103, L_104);
		int32_t L_105 = L_104;
		if ((*(GUIStyle_t6_166 **)(GUIStyle_t6_166 **)SZArrayLdElema(L_103, L_105, sizeof(GUIStyle_t6_166 *))))
		{
			goto IL_04e4;
		}
	}
	{
		goto IL_0504;
	}

IL_04e4:
	{
		Dictionary_2_t1_918 * L_106 = (__this->___m_Styles_26);
		GUIStyleU5BU5D_t6_179* L_107 = (__this->___m_CustomStyles_23);
		int32_t L_108 = V_0;
		NullCheck(L_107);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_107, L_108);
		int32_t L_109 = L_108;
		NullCheck((*(GUIStyle_t6_166 **)(GUIStyle_t6_166 **)SZArrayLdElema(L_107, L_109, sizeof(GUIStyle_t6_166 *))));
		String_t* L_110 = GUIStyle_get_name_m6_1327((*(GUIStyle_t6_166 **)(GUIStyle_t6_166 **)SZArrayLdElema(L_107, L_109, sizeof(GUIStyle_t6_166 *))), /*hidden argument*/NULL);
		GUIStyleU5BU5D_t6_179* L_111 = (__this->___m_CustomStyles_23);
		int32_t L_112 = V_0;
		NullCheck(L_111);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_111, L_112);
		int32_t L_113 = L_112;
		NullCheck(L_106);
		VirtActionInvoker2< String_t*, GUIStyle_t6_166 * >::Invoke(26 /* System.Void System.Collections.Generic.Dictionary`2<System.String,UnityEngine.GUIStyle>::set_Item(!0,!1) */, L_106, L_110, (*(GUIStyle_t6_166 **)(GUIStyle_t6_166 **)SZArrayLdElema(L_111, L_113, sizeof(GUIStyle_t6_166 *))));
	}

IL_0504:
	{
		int32_t L_114 = V_0;
		V_0 = ((int32_t)((int32_t)L_114+(int32_t)1));
	}

IL_0508:
	{
		int32_t L_115 = V_0;
		GUIStyleU5BU5D_t6_179* L_116 = (__this->___m_CustomStyles_23);
		NullCheck(L_116);
		if ((((int32_t)L_115) < ((int32_t)(((int32_t)((int32_t)(((Array_t *)L_116)->max_length)))))))
		{
			goto IL_04d2;
		}
	}

IL_0516:
	{
		GUIStyle_t6_166 * L_117 = GUISkin_get_error_m6_1262(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_117);
		GUIStyle_set_stretchHeight_m6_1347(L_117, 1, /*hidden argument*/NULL);
		GUIStyle_t6_166 * L_118 = GUISkin_get_error_m6_1262(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_118);
		GUIStyleState_t6_180 * L_119 = GUIStyle_get_normal_m6_1302(L_118, /*hidden argument*/NULL);
		Color_t6_40  L_120 = Color_get_red_m6_234(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_119);
		GUIStyleState_set_textColor_m6_1275(L_119, L_120, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.GUIStyle UnityEngine.GUISkin::GetStyle(System.String)
extern TypeInfo* ObjectU5BU5D_t1_157_il2cpp_TypeInfo_var;
extern TypeInfo* EventType_t6_156_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2823;
extern Il2CppCodeGenString* _stringLiteral2824;
extern Il2CppCodeGenString* _stringLiteral2825;
extern "C" GUIStyle_t6_166 * GUISkin_GetStyle_m6_1265 (GUISkin_t6_161 * __this, String_t* ___styleName, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ObjectU5BU5D_t1_157_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(19);
		EventType_t6_156_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(980);
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(11);
		_stringLiteral2823 = il2cpp_codegen_string_literal_from_index(2823);
		_stringLiteral2824 = il2cpp_codegen_string_literal_from_index(2824);
		_stringLiteral2825 = il2cpp_codegen_string_literal_from_index(2825);
		s_Il2CppMethodIntialized = true;
	}
	GUIStyle_t6_166 * V_0 = {0};
	{
		String_t* L_0 = ___styleName;
		GUIStyle_t6_166 * L_1 = GUISkin_FindStyle_m6_1266(__this, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		GUIStyle_t6_166 * L_2 = V_0;
		if (!L_2)
		{
			goto IL_0010;
		}
	}
	{
		GUIStyle_t6_166 * L_3 = V_0;
		return L_3;
	}

IL_0010:
	{
		ObjectU5BU5D_t1_157* L_4 = ((ObjectU5BU5D_t1_157*)SZArrayNew(ObjectU5BU5D_t1_157_il2cpp_TypeInfo_var, 6));
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, 0);
		ArrayElementTypeCheck (L_4, _stringLiteral2823);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_4, 0, sizeof(Object_t *))) = (Object_t *)_stringLiteral2823;
		ObjectU5BU5D_t1_157* L_5 = L_4;
		String_t* L_6 = ___styleName;
		NullCheck(L_5);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_5, 1);
		ArrayElementTypeCheck (L_5, L_6);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_5, 1, sizeof(Object_t *))) = (Object_t *)L_6;
		ObjectU5BU5D_t1_157* L_7 = L_5;
		NullCheck(L_7);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_7, 2);
		ArrayElementTypeCheck (L_7, _stringLiteral2824);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_7, 2, sizeof(Object_t *))) = (Object_t *)_stringLiteral2824;
		ObjectU5BU5D_t1_157* L_8 = L_7;
		String_t* L_9 = Object_get_name_m6_562(__this, /*hidden argument*/NULL);
		NullCheck(L_8);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_8, 3);
		ArrayElementTypeCheck (L_8, L_9);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_8, 3, sizeof(Object_t *))) = (Object_t *)L_9;
		ObjectU5BU5D_t1_157* L_10 = L_8;
		NullCheck(L_10);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_10, 4);
		ArrayElementTypeCheck (L_10, _stringLiteral2825);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_10, 4, sizeof(Object_t *))) = (Object_t *)_stringLiteral2825;
		ObjectU5BU5D_t1_157* L_11 = L_10;
		Event_t6_154 * L_12 = Event_get_current_m6_989(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_12);
		int32_t L_13 = Event_get_type_m6_1003(L_12, /*hidden argument*/NULL);
		int32_t L_14 = L_13;
		Object_t * L_15 = Box(EventType_t6_156_il2cpp_TypeInfo_var, &L_14);
		NullCheck(L_11);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_11, 5);
		ArrayElementTypeCheck (L_11, L_15);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_11, 5, sizeof(Object_t *))) = (Object_t *)L_15;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_16 = String_Concat_m1_421(NULL /*static, unused*/, L_11, /*hidden argument*/NULL);
		Debug_LogWarning_m6_491(NULL /*static, unused*/, L_16, /*hidden argument*/NULL);
		GUIStyle_t6_166 * L_17 = GUISkin_get_error_m6_1262(NULL /*static, unused*/, /*hidden argument*/NULL);
		return L_17;
	}
}
// UnityEngine.GUIStyle UnityEngine.GUISkin::FindStyle(System.String)
extern Il2CppCodeGenString* _stringLiteral2826;
extern "C" GUIStyle_t6_166 * GUISkin_FindStyle_m6_1266 (GUISkin_t6_161 * __this, String_t* ___styleName, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		_stringLiteral2826 = il2cpp_codegen_string_literal_from_index(2826);
		s_Il2CppMethodIntialized = true;
	}
	GUIStyle_t6_166 * V_0 = {0};
	{
		bool L_0 = Object_op_Equality_m6_579(NULL /*static, unused*/, __this, (Object_t6_5 *)NULL, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_0018;
		}
	}
	{
		Debug_LogError_m6_490(NULL /*static, unused*/, _stringLiteral2826, /*hidden argument*/NULL);
		return (GUIStyle_t6_166 *)NULL;
	}

IL_0018:
	{
		Dictionary_2_t1_918 * L_1 = (__this->___m_Styles_26);
		if (L_1)
		{
			goto IL_0029;
		}
	}
	{
		GUISkin_BuildStyleCache_m6_1264(__this, /*hidden argument*/NULL);
	}

IL_0029:
	{
		Dictionary_2_t1_918 * L_2 = (__this->___m_Styles_26);
		String_t* L_3 = ___styleName;
		NullCheck(L_2);
		bool L_4 = (bool)VirtFuncInvoker2< bool, String_t*, GUIStyle_t6_166 ** >::Invoke(32 /* System.Boolean System.Collections.Generic.Dictionary`2<System.String,UnityEngine.GUIStyle>::TryGetValue(!0,!1&) */, L_2, L_3, (&V_0));
		if (!L_4)
		{
			goto IL_003e;
		}
	}
	{
		GUIStyle_t6_166 * L_5 = V_0;
		return L_5;
	}

IL_003e:
	{
		return (GUIStyle_t6_166 *)NULL;
	}
}
// System.Void UnityEngine.GUISkin::MakeCurrent()
extern TypeInfo* GUISkin_t6_161_il2cpp_TypeInfo_var;
extern TypeInfo* GUIStyle_t6_166_il2cpp_TypeInfo_var;
extern "C" void GUISkin_MakeCurrent_m6_1267 (GUISkin_t6_161 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GUISkin_t6_161_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1007);
		GUIStyle_t6_166_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(988);
		s_Il2CppMethodIntialized = true;
	}
	{
		((GUISkin_t6_161_StaticFields*)GUISkin_t6_161_il2cpp_TypeInfo_var->static_fields)->___current_28 = __this;
		Font_t6_148 * L_0 = GUISkin_get_font_m6_1217(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(GUIStyle_t6_166_il2cpp_TypeInfo_var);
		GUIStyle_SetDefaultFont_m6_1357(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		SkinChangedDelegate_t6_178 * L_1 = ((GUISkin_t6_161_StaticFields*)GUISkin_t6_161_il2cpp_TypeInfo_var->static_fields)->___m_SkinChanged_27;
		if (!L_1)
		{
			goto IL_0025;
		}
	}
	{
		SkinChangedDelegate_t6_178 * L_2 = ((GUISkin_t6_161_StaticFields*)GUISkin_t6_161_il2cpp_TypeInfo_var->static_fields)->___m_SkinChanged_27;
		NullCheck(L_2);
		SkinChangedDelegate_Invoke_m6_1212(L_2, /*hidden argument*/NULL);
	}

IL_0025:
	{
		return;
	}
}
// System.Collections.IEnumerator UnityEngine.GUISkin::GetEnumerator()
extern TypeInfo* Enumerator_t1_929_il2cpp_TypeInfo_var;
extern const MethodInfo* Dictionary_2_get_Values_m1_5609_MethodInfo_var;
extern const MethodInfo* ValueCollection_GetEnumerator_m1_5610_MethodInfo_var;
extern "C" Object_t * GUISkin_GetEnumerator_m6_1268 (GUISkin_t6_161 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Enumerator_t1_929_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1009);
		Dictionary_2_get_Values_m1_5609_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483817);
		ValueCollection_GetEnumerator_m1_5610_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483818);
		s_Il2CppMethodIntialized = true;
	}
	{
		Dictionary_2_t1_918 * L_0 = (__this->___m_Styles_26);
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		GUISkin_BuildStyleCache_m6_1264(__this, /*hidden argument*/NULL);
	}

IL_0011:
	{
		Dictionary_2_t1_918 * L_1 = (__this->___m_Styles_26);
		NullCheck(L_1);
		ValueCollection_t1_928 * L_2 = Dictionary_2_get_Values_m1_5609(L_1, /*hidden argument*/Dictionary_2_get_Values_m1_5609_MethodInfo_var);
		NullCheck(L_2);
		Enumerator_t1_929  L_3 = ValueCollection_GetEnumerator_m1_5610(L_2, /*hidden argument*/ValueCollection_GetEnumerator_m1_5610_MethodInfo_var);
		Enumerator_t1_929  L_4 = L_3;
		Object_t * L_5 = Box(Enumerator_t1_929_il2cpp_TypeInfo_var, &L_4);
		return (Object_t *)L_5;
	}
}
// System.Void UnityEngine.GUIStyleState::.ctor()
extern "C" void GUIStyleState__ctor_m6_1269 (GUIStyleState_t6_180 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1_0(__this, /*hidden argument*/NULL);
		GUIStyleState_Init_m6_1272(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.GUIStyleState::.ctor(UnityEngine.GUIStyle,System.IntPtr)
extern "C" void GUIStyleState__ctor_m6_1270 (GUIStyleState_t6_180 * __this, GUIStyle_t6_166 * ___sourceStyle, IntPtr_t ___source, const MethodInfo* method)
{
	{
		Object__ctor_m1_0(__this, /*hidden argument*/NULL);
		GUIStyle_t6_166 * L_0 = ___sourceStyle;
		__this->___m_SourceStyle_1 = L_0;
		IntPtr_t L_1 = ___source;
		__this->___m_Ptr_0 = L_1;
		Texture2D_t6_33 * L_2 = GUIStyleState_GetBackgroundInternal_m6_1274(__this, /*hidden argument*/NULL);
		__this->___m_Background_2 = L_2;
		return;
	}
}
// System.Void UnityEngine.GUIStyleState::Finalize()
extern "C" void GUIStyleState_Finalize_m6_1271 (GUIStyleState_t6_180 * __this, const MethodInfo* method)
{
	Exception_t1_33 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1_33 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		{
			GUIStyle_t6_166 * L_0 = (__this->___m_SourceStyle_1);
			if (L_0)
			{
				goto IL_0011;
			}
		}

IL_000b:
		{
			GUIStyleState_Cleanup_m6_1273(__this, /*hidden argument*/NULL);
		}

IL_0011:
		{
			IL2CPP_LEAVE(0x1D, FINALLY_0016);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1_33 *)e.ex;
		goto FINALLY_0016;
	}

FINALLY_0016:
	{ // begin finally (depth: 1)
		Object_Finalize_m1_3(__this, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(22)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(22)
	{
		IL2CPP_JUMP_TBL(0x1D, IL_001d)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1_33 *)
	}

IL_001d:
	{
		return;
	}
}
// System.Void UnityEngine.GUIStyleState::Init()
extern "C" void GUIStyleState_Init_m6_1272 (GUIStyleState_t6_180 * __this, const MethodInfo* method)
{
	typedef void (*GUIStyleState_Init_m6_1272_ftn) (GUIStyleState_t6_180 *);
	static GUIStyleState_Init_m6_1272_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GUIStyleState_Init_m6_1272_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.GUIStyleState::Init()");
	_il2cpp_icall_func(__this);
}
// System.Void UnityEngine.GUIStyleState::Cleanup()
extern "C" void GUIStyleState_Cleanup_m6_1273 (GUIStyleState_t6_180 * __this, const MethodInfo* method)
{
	typedef void (*GUIStyleState_Cleanup_m6_1273_ftn) (GUIStyleState_t6_180 *);
	static GUIStyleState_Cleanup_m6_1273_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GUIStyleState_Cleanup_m6_1273_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.GUIStyleState::Cleanup()");
	_il2cpp_icall_func(__this);
}
// UnityEngine.Texture2D UnityEngine.GUIStyleState::GetBackgroundInternal()
extern "C" Texture2D_t6_33 * GUIStyleState_GetBackgroundInternal_m6_1274 (GUIStyleState_t6_180 * __this, const MethodInfo* method)
{
	typedef Texture2D_t6_33 * (*GUIStyleState_GetBackgroundInternal_m6_1274_ftn) (GUIStyleState_t6_180 *);
	static GUIStyleState_GetBackgroundInternal_m6_1274_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GUIStyleState_GetBackgroundInternal_m6_1274_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.GUIStyleState::GetBackgroundInternal()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.GUIStyleState::set_textColor(UnityEngine.Color)
extern "C" void GUIStyleState_set_textColor_m6_1275 (GUIStyleState_t6_180 * __this, Color_t6_40  ___value, const MethodInfo* method)
{
	{
		GUIStyleState_INTERNAL_set_textColor_m6_1276(__this, (&___value), /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.GUIStyleState::INTERNAL_set_textColor(UnityEngine.Color&)
extern "C" void GUIStyleState_INTERNAL_set_textColor_m6_1276 (GUIStyleState_t6_180 * __this, Color_t6_40 * ___value, const MethodInfo* method)
{
	typedef void (*GUIStyleState_INTERNAL_set_textColor_m6_1276_ftn) (GUIStyleState_t6_180 *, Color_t6_40 *);
	static GUIStyleState_INTERNAL_set_textColor_m6_1276_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GUIStyleState_INTERNAL_set_textColor_m6_1276_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.GUIStyleState::INTERNAL_set_textColor(UnityEngine.Color&)");
	_il2cpp_icall_func(__this, ___value);
}
// System.Void UnityEngine.RectOffset::.ctor()
extern "C" void RectOffset__ctor_m6_1277 (RectOffset_t6_172 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1_0(__this, /*hidden argument*/NULL);
		RectOffset_Init_m6_1282(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.RectOffset::.ctor(UnityEngine.GUIStyle,System.IntPtr)
extern "C" void RectOffset__ctor_m6_1278 (RectOffset_t6_172 * __this, GUIStyle_t6_166 * ___sourceStyle, IntPtr_t ___source, const MethodInfo* method)
{
	{
		Object__ctor_m1_0(__this, /*hidden argument*/NULL);
		GUIStyle_t6_166 * L_0 = ___sourceStyle;
		__this->___m_SourceStyle_1 = L_0;
		IntPtr_t L_1 = ___source;
		__this->___m_Ptr_0 = L_1;
		return;
	}
}
// System.Void UnityEngine.RectOffset::.ctor(System.Int32,System.Int32,System.Int32,System.Int32)
extern "C" void RectOffset__ctor_m6_1279 (RectOffset_t6_172 * __this, int32_t ___left, int32_t ___right, int32_t ___top, int32_t ___bottom, const MethodInfo* method)
{
	{
		Object__ctor_m1_0(__this, /*hidden argument*/NULL);
		RectOffset_Init_m6_1282(__this, /*hidden argument*/NULL);
		int32_t L_0 = ___left;
		RectOffset_set_left_m6_1285(__this, L_0, /*hidden argument*/NULL);
		int32_t L_1 = ___right;
		RectOffset_set_right_m6_1287(__this, L_1, /*hidden argument*/NULL);
		int32_t L_2 = ___top;
		RectOffset_set_top_m6_1289(__this, L_2, /*hidden argument*/NULL);
		int32_t L_3 = ___bottom;
		RectOffset_set_bottom_m6_1291(__this, L_3, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.RectOffset::Finalize()
extern "C" void RectOffset_Finalize_m6_1280 (RectOffset_t6_172 * __this, const MethodInfo* method)
{
	Exception_t1_33 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1_33 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		{
			GUIStyle_t6_166 * L_0 = (__this->___m_SourceStyle_1);
			if (L_0)
			{
				goto IL_0011;
			}
		}

IL_000b:
		{
			RectOffset_Cleanup_m6_1283(__this, /*hidden argument*/NULL);
		}

IL_0011:
		{
			IL2CPP_LEAVE(0x1D, FINALLY_0016);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1_33 *)e.ex;
		goto FINALLY_0016;
	}

FINALLY_0016:
	{ // begin finally (depth: 1)
		Object_Finalize_m1_3(__this, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(22)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(22)
	{
		IL2CPP_JUMP_TBL(0x1D, IL_001d)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1_33 *)
	}

IL_001d:
	{
		return;
	}
}
// System.String UnityEngine.RectOffset::ToString()
extern TypeInfo* ObjectU5BU5D_t1_157_il2cpp_TypeInfo_var;
extern TypeInfo* Int32_t1_3_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2827;
extern "C" String_t* RectOffset_ToString_m6_1281 (RectOffset_t6_172 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ObjectU5BU5D_t1_157_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(19);
		Int32_t1_3_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(8);
		_stringLiteral2827 = il2cpp_codegen_string_literal_from_index(2827);
		s_Il2CppMethodIntialized = true;
	}
	{
		ObjectU5BU5D_t1_157* L_0 = ((ObjectU5BU5D_t1_157*)SZArrayNew(ObjectU5BU5D_t1_157_il2cpp_TypeInfo_var, 4));
		int32_t L_1 = RectOffset_get_left_m6_1284(__this, /*hidden argument*/NULL);
		int32_t L_2 = L_1;
		Object_t * L_3 = Box(Int32_t1_3_il2cpp_TypeInfo_var, &L_2);
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, 0);
		ArrayElementTypeCheck (L_0, L_3);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_0, 0, sizeof(Object_t *))) = (Object_t *)L_3;
		ObjectU5BU5D_t1_157* L_4 = L_0;
		int32_t L_5 = RectOffset_get_right_m6_1286(__this, /*hidden argument*/NULL);
		int32_t L_6 = L_5;
		Object_t * L_7 = Box(Int32_t1_3_il2cpp_TypeInfo_var, &L_6);
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, 1);
		ArrayElementTypeCheck (L_4, L_7);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_4, 1, sizeof(Object_t *))) = (Object_t *)L_7;
		ObjectU5BU5D_t1_157* L_8 = L_4;
		int32_t L_9 = RectOffset_get_top_m6_1288(__this, /*hidden argument*/NULL);
		int32_t L_10 = L_9;
		Object_t * L_11 = Box(Int32_t1_3_il2cpp_TypeInfo_var, &L_10);
		NullCheck(L_8);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_8, 2);
		ArrayElementTypeCheck (L_8, L_11);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_8, 2, sizeof(Object_t *))) = (Object_t *)L_11;
		ObjectU5BU5D_t1_157* L_12 = L_8;
		int32_t L_13 = RectOffset_get_bottom_m6_1290(__this, /*hidden argument*/NULL);
		int32_t L_14 = L_13;
		Object_t * L_15 = Box(Int32_t1_3_il2cpp_TypeInfo_var, &L_14);
		NullCheck(L_12);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_12, 3);
		ArrayElementTypeCheck (L_12, L_15);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_12, 3, sizeof(Object_t *))) = (Object_t *)L_15;
		String_t* L_16 = UnityString_Format_m6_437(NULL /*static, unused*/, _stringLiteral2827, L_12, /*hidden argument*/NULL);
		return L_16;
	}
}
// System.Void UnityEngine.RectOffset::Init()
extern "C" void RectOffset_Init_m6_1282 (RectOffset_t6_172 * __this, const MethodInfo* method)
{
	typedef void (*RectOffset_Init_m6_1282_ftn) (RectOffset_t6_172 *);
	static RectOffset_Init_m6_1282_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RectOffset_Init_m6_1282_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RectOffset::Init()");
	_il2cpp_icall_func(__this);
}
// System.Void UnityEngine.RectOffset::Cleanup()
extern "C" void RectOffset_Cleanup_m6_1283 (RectOffset_t6_172 * __this, const MethodInfo* method)
{
	typedef void (*RectOffset_Cleanup_m6_1283_ftn) (RectOffset_t6_172 *);
	static RectOffset_Cleanup_m6_1283_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RectOffset_Cleanup_m6_1283_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RectOffset::Cleanup()");
	_il2cpp_icall_func(__this);
}
// System.Int32 UnityEngine.RectOffset::get_left()
extern "C" int32_t RectOffset_get_left_m6_1284 (RectOffset_t6_172 * __this, const MethodInfo* method)
{
	typedef int32_t (*RectOffset_get_left_m6_1284_ftn) (RectOffset_t6_172 *);
	static RectOffset_get_left_m6_1284_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RectOffset_get_left_m6_1284_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RectOffset::get_left()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.RectOffset::set_left(System.Int32)
extern "C" void RectOffset_set_left_m6_1285 (RectOffset_t6_172 * __this, int32_t ___value, const MethodInfo* method)
{
	typedef void (*RectOffset_set_left_m6_1285_ftn) (RectOffset_t6_172 *, int32_t);
	static RectOffset_set_left_m6_1285_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RectOffset_set_left_m6_1285_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RectOffset::set_left(System.Int32)");
	_il2cpp_icall_func(__this, ___value);
}
// System.Int32 UnityEngine.RectOffset::get_right()
extern "C" int32_t RectOffset_get_right_m6_1286 (RectOffset_t6_172 * __this, const MethodInfo* method)
{
	typedef int32_t (*RectOffset_get_right_m6_1286_ftn) (RectOffset_t6_172 *);
	static RectOffset_get_right_m6_1286_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RectOffset_get_right_m6_1286_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RectOffset::get_right()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.RectOffset::set_right(System.Int32)
extern "C" void RectOffset_set_right_m6_1287 (RectOffset_t6_172 * __this, int32_t ___value, const MethodInfo* method)
{
	typedef void (*RectOffset_set_right_m6_1287_ftn) (RectOffset_t6_172 *, int32_t);
	static RectOffset_set_right_m6_1287_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RectOffset_set_right_m6_1287_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RectOffset::set_right(System.Int32)");
	_il2cpp_icall_func(__this, ___value);
}
// System.Int32 UnityEngine.RectOffset::get_top()
extern "C" int32_t RectOffset_get_top_m6_1288 (RectOffset_t6_172 * __this, const MethodInfo* method)
{
	typedef int32_t (*RectOffset_get_top_m6_1288_ftn) (RectOffset_t6_172 *);
	static RectOffset_get_top_m6_1288_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RectOffset_get_top_m6_1288_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RectOffset::get_top()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.RectOffset::set_top(System.Int32)
extern "C" void RectOffset_set_top_m6_1289 (RectOffset_t6_172 * __this, int32_t ___value, const MethodInfo* method)
{
	typedef void (*RectOffset_set_top_m6_1289_ftn) (RectOffset_t6_172 *, int32_t);
	static RectOffset_set_top_m6_1289_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RectOffset_set_top_m6_1289_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RectOffset::set_top(System.Int32)");
	_il2cpp_icall_func(__this, ___value);
}
// System.Int32 UnityEngine.RectOffset::get_bottom()
extern "C" int32_t RectOffset_get_bottom_m6_1290 (RectOffset_t6_172 * __this, const MethodInfo* method)
{
	typedef int32_t (*RectOffset_get_bottom_m6_1290_ftn) (RectOffset_t6_172 *);
	static RectOffset_get_bottom_m6_1290_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RectOffset_get_bottom_m6_1290_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RectOffset::get_bottom()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.RectOffset::set_bottom(System.Int32)
extern "C" void RectOffset_set_bottom_m6_1291 (RectOffset_t6_172 * __this, int32_t ___value, const MethodInfo* method)
{
	typedef void (*RectOffset_set_bottom_m6_1291_ftn) (RectOffset_t6_172 *, int32_t);
	static RectOffset_set_bottom_m6_1291_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RectOffset_set_bottom_m6_1291_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RectOffset::set_bottom(System.Int32)");
	_il2cpp_icall_func(__this, ___value);
}
// System.Int32 UnityEngine.RectOffset::get_horizontal()
extern "C" int32_t RectOffset_get_horizontal_m6_1292 (RectOffset_t6_172 * __this, const MethodInfo* method)
{
	typedef int32_t (*RectOffset_get_horizontal_m6_1292_ftn) (RectOffset_t6_172 *);
	static RectOffset_get_horizontal_m6_1292_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RectOffset_get_horizontal_m6_1292_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RectOffset::get_horizontal()");
	return _il2cpp_icall_func(__this);
}
// System.Int32 UnityEngine.RectOffset::get_vertical()
extern "C" int32_t RectOffset_get_vertical_m6_1293 (RectOffset_t6_172 * __this, const MethodInfo* method)
{
	typedef int32_t (*RectOffset_get_vertical_m6_1293_ftn) (RectOffset_t6_172 *);
	static RectOffset_get_vertical_m6_1293_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RectOffset_get_vertical_m6_1293_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RectOffset::get_vertical()");
	return _il2cpp_icall_func(__this);
}
// UnityEngine.Rect UnityEngine.RectOffset::Add(UnityEngine.Rect)
extern "C" Rect_t6_52  RectOffset_Add_m6_1294 (RectOffset_t6_172 * __this, Rect_t6_52  ___rect, const MethodInfo* method)
{
	{
		Rect_t6_52  L_0 = RectOffset_INTERNAL_CALL_Add_m6_1295(NULL /*static, unused*/, __this, (&___rect), /*hidden argument*/NULL);
		return L_0;
	}
}
// UnityEngine.Rect UnityEngine.RectOffset::INTERNAL_CALL_Add(UnityEngine.RectOffset,UnityEngine.Rect&)
extern "C" Rect_t6_52  RectOffset_INTERNAL_CALL_Add_m6_1295 (Object_t * __this /* static, unused */, RectOffset_t6_172 * ___self, Rect_t6_52 * ___rect, const MethodInfo* method)
{
	typedef Rect_t6_52  (*RectOffset_INTERNAL_CALL_Add_m6_1295_ftn) (RectOffset_t6_172 *, Rect_t6_52 *);
	static RectOffset_INTERNAL_CALL_Add_m6_1295_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RectOffset_INTERNAL_CALL_Add_m6_1295_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RectOffset::INTERNAL_CALL_Add(UnityEngine.RectOffset,UnityEngine.Rect&)");
	return _il2cpp_icall_func(___self, ___rect);
}
// UnityEngine.Rect UnityEngine.RectOffset::Remove(UnityEngine.Rect)
extern "C" Rect_t6_52  RectOffset_Remove_m6_1296 (RectOffset_t6_172 * __this, Rect_t6_52  ___rect, const MethodInfo* method)
{
	{
		Rect_t6_52  L_0 = RectOffset_INTERNAL_CALL_Remove_m6_1297(NULL /*static, unused*/, __this, (&___rect), /*hidden argument*/NULL);
		return L_0;
	}
}
// UnityEngine.Rect UnityEngine.RectOffset::INTERNAL_CALL_Remove(UnityEngine.RectOffset,UnityEngine.Rect&)
extern "C" Rect_t6_52  RectOffset_INTERNAL_CALL_Remove_m6_1297 (Object_t * __this /* static, unused */, RectOffset_t6_172 * ___self, Rect_t6_52 * ___rect, const MethodInfo* method)
{
	typedef Rect_t6_52  (*RectOffset_INTERNAL_CALL_Remove_m6_1297_ftn) (RectOffset_t6_172 *, Rect_t6_52 *);
	static RectOffset_INTERNAL_CALL_Remove_m6_1297_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RectOffset_INTERNAL_CALL_Remove_m6_1297_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RectOffset::INTERNAL_CALL_Remove(UnityEngine.RectOffset,UnityEngine.Rect&)");
	return _il2cpp_icall_func(___self, ___rect);
}
// System.Void UnityEngine.GUIStyle::.ctor()
extern "C" void GUIStyle__ctor_m6_1298 (GUIStyle_t6_166 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1_0(__this, /*hidden argument*/NULL);
		GUIStyle_Init_m6_1324(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.GUIStyle::.ctor(UnityEngine.GUIStyle)
extern "C" void GUIStyle__ctor_m6_1299 (GUIStyle_t6_166 * __this, GUIStyle_t6_166 * ___other, const MethodInfo* method)
{
	{
		Object__ctor_m1_0(__this, /*hidden argument*/NULL);
		GUIStyle_t6_166 * L_0 = ___other;
		GUIStyle_InitCopy_m6_1325(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.GUIStyle::.cctor()
extern TypeInfo* GUIStyle_t6_166_il2cpp_TypeInfo_var;
extern "C" void GUIStyle__cctor_m6_1300 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GUIStyle_t6_166_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(988);
		s_Il2CppMethodIntialized = true;
	}
	{
		((GUIStyle_t6_166_StaticFields*)GUIStyle_t6_166_il2cpp_TypeInfo_var->static_fields)->___showKeyboardFocus_14 = 1;
		return;
	}
}
// System.Void UnityEngine.GUIStyle::Finalize()
extern "C" void GUIStyle_Finalize_m6_1301 (GUIStyle_t6_166 * __this, const MethodInfo* method)
{
	Exception_t1_33 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1_33 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		GUIStyle_Cleanup_m6_1326(__this, /*hidden argument*/NULL);
		IL2CPP_LEAVE(0x12, FINALLY_000b);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1_33 *)e.ex;
		goto FINALLY_000b;
	}

FINALLY_000b:
	{ // begin finally (depth: 1)
		Object_Finalize_m1_3(__this, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(11)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(11)
	{
		IL2CPP_JUMP_TBL(0x12, IL_0012)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1_33 *)
	}

IL_0012:
	{
		return;
	}
}
// UnityEngine.GUIStyleState UnityEngine.GUIStyle::get_normal()
extern TypeInfo* GUIStyleState_t6_180_il2cpp_TypeInfo_var;
extern "C" GUIStyleState_t6_180 * GUIStyle_get_normal_m6_1302 (GUIStyle_t6_166 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GUIStyleState_t6_180_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1010);
		s_Il2CppMethodIntialized = true;
	}
	{
		GUIStyleState_t6_180 * L_0 = (__this->___m_Normal_1);
		if (L_0)
		{
			goto IL_001e;
		}
	}
	{
		IntPtr_t L_1 = GUIStyle_GetStyleStatePtr_m6_1329(__this, 0, /*hidden argument*/NULL);
		GUIStyleState_t6_180 * L_2 = (GUIStyleState_t6_180 *)il2cpp_codegen_object_new (GUIStyleState_t6_180_il2cpp_TypeInfo_var);
		GUIStyleState__ctor_m6_1270(L_2, __this, L_1, /*hidden argument*/NULL);
		__this->___m_Normal_1 = L_2;
	}

IL_001e:
	{
		GUIStyleState_t6_180 * L_3 = (__this->___m_Normal_1);
		return L_3;
	}
}
// UnityEngine.RectOffset UnityEngine.GUIStyle::get_margin()
extern TypeInfo* RectOffset_t6_172_il2cpp_TypeInfo_var;
extern "C" RectOffset_t6_172 * GUIStyle_get_margin_m6_1303 (GUIStyle_t6_166 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		RectOffset_t6_172_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1004);
		s_Il2CppMethodIntialized = true;
	}
	{
		RectOffset_t6_172 * L_0 = (__this->___m_Margin_11);
		if (L_0)
		{
			goto IL_001e;
		}
	}
	{
		IntPtr_t L_1 = GUIStyle_GetRectOffsetPtr_m6_1330(__this, 1, /*hidden argument*/NULL);
		RectOffset_t6_172 * L_2 = (RectOffset_t6_172 *)il2cpp_codegen_object_new (RectOffset_t6_172_il2cpp_TypeInfo_var);
		RectOffset__ctor_m6_1278(L_2, __this, L_1, /*hidden argument*/NULL);
		__this->___m_Margin_11 = L_2;
	}

IL_001e:
	{
		RectOffset_t6_172 * L_3 = (__this->___m_Margin_11);
		return L_3;
	}
}
// UnityEngine.RectOffset UnityEngine.GUIStyle::get_padding()
extern TypeInfo* RectOffset_t6_172_il2cpp_TypeInfo_var;
extern "C" RectOffset_t6_172 * GUIStyle_get_padding_m6_1304 (GUIStyle_t6_166 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		RectOffset_t6_172_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1004);
		s_Il2CppMethodIntialized = true;
	}
	{
		RectOffset_t6_172 * L_0 = (__this->___m_Padding_10);
		if (L_0)
		{
			goto IL_001e;
		}
	}
	{
		IntPtr_t L_1 = GUIStyle_GetRectOffsetPtr_m6_1330(__this, 2, /*hidden argument*/NULL);
		RectOffset_t6_172 * L_2 = (RectOffset_t6_172 *)il2cpp_codegen_object_new (RectOffset_t6_172_il2cpp_TypeInfo_var);
		RectOffset__ctor_m6_1278(L_2, __this, L_1, /*hidden argument*/NULL);
		__this->___m_Padding_10 = L_2;
	}

IL_001e:
	{
		RectOffset_t6_172 * L_3 = (__this->___m_Padding_10);
		return L_3;
	}
}
// System.Void UnityEngine.GUIStyle::set_padding(UnityEngine.RectOffset)
extern "C" void GUIStyle_set_padding_m6_1305 (GUIStyle_t6_166 * __this, RectOffset_t6_172 * ___value, const MethodInfo* method)
{
	{
		RectOffset_t6_172 * L_0 = ___value;
		NullCheck(L_0);
		IntPtr_t L_1 = (L_0->___m_Ptr_0);
		GUIStyle_AssignRectOffset_m6_1331(__this, 2, L_1, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Font UnityEngine.GUIStyle::get_font()
extern "C" Font_t6_148 * GUIStyle_get_font_m6_1306 (GUIStyle_t6_166 * __this, const MethodInfo* method)
{
	{
		Font_t6_148 * L_0 = GUIStyle_GetFontInternal_m6_1349(__this, /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Single UnityEngine.GUIStyle::get_lineHeight()
extern TypeInfo* GUIStyle_t6_166_il2cpp_TypeInfo_var;
extern TypeInfo* Mathf_t6_58_il2cpp_TypeInfo_var;
extern "C" float GUIStyle_get_lineHeight_m6_1307 (GUIStyle_t6_166 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GUIStyle_t6_166_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(988);
		Mathf_t6_58_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(940);
		s_Il2CppMethodIntialized = true;
	}
	{
		IntPtr_t L_0 = (__this->___m_Ptr_0);
		IL2CPP_RUNTIME_CLASS_INIT(GUIStyle_t6_166_il2cpp_TypeInfo_var);
		float L_1 = GUIStyle_Internal_GetLineHeight_m6_1348(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t6_58_il2cpp_TypeInfo_var);
		float L_2 = bankers_roundf(L_1);
		return L_2;
	}
}
// System.Void UnityEngine.GUIStyle::Internal_Draw(System.IntPtr,UnityEngine.Rect,UnityEngine.GUIContent,System.Boolean,System.Boolean,System.Boolean,System.Boolean)
extern TypeInfo* Internal_DrawArguments_t6_187_il2cpp_TypeInfo_var;
extern TypeInfo* GUIStyle_t6_166_il2cpp_TypeInfo_var;
extern "C" void GUIStyle_Internal_Draw_m6_1308 (Object_t * __this /* static, unused */, IntPtr_t ___target, Rect_t6_52  ___position, GUIContent_t6_163 * ___content, bool ___isHover, bool ___isActive, bool ___on, bool ___hasKeyboardFocus, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Internal_DrawArguments_t6_187_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1011);
		GUIStyle_t6_166_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(988);
		s_Il2CppMethodIntialized = true;
	}
	Internal_DrawArguments_t6_187  V_0 = {0};
	Internal_DrawArguments_t6_187 * G_B2_0 = {0};
	Internal_DrawArguments_t6_187 * G_B1_0 = {0};
	int32_t G_B3_0 = 0;
	Internal_DrawArguments_t6_187 * G_B3_1 = {0};
	Internal_DrawArguments_t6_187 * G_B5_0 = {0};
	Internal_DrawArguments_t6_187 * G_B4_0 = {0};
	int32_t G_B6_0 = 0;
	Internal_DrawArguments_t6_187 * G_B6_1 = {0};
	Internal_DrawArguments_t6_187 * G_B8_0 = {0};
	Internal_DrawArguments_t6_187 * G_B7_0 = {0};
	int32_t G_B9_0 = 0;
	Internal_DrawArguments_t6_187 * G_B9_1 = {0};
	Internal_DrawArguments_t6_187 * G_B11_0 = {0};
	Internal_DrawArguments_t6_187 * G_B10_0 = {0};
	int32_t G_B12_0 = 0;
	Internal_DrawArguments_t6_187 * G_B12_1 = {0};
	{
		Initobj (Internal_DrawArguments_t6_187_il2cpp_TypeInfo_var, (&V_0));
		IntPtr_t L_0 = ___target;
		(&V_0)->___target_0 = L_0;
		Rect_t6_52  L_1 = ___position;
		(&V_0)->___position_1 = L_1;
		bool L_2 = ___isHover;
		G_B1_0 = (&V_0);
		if (!L_2)
		{
			G_B2_0 = (&V_0);
			goto IL_0026;
		}
	}
	{
		G_B3_0 = 1;
		G_B3_1 = G_B1_0;
		goto IL_0027;
	}

IL_0026:
	{
		G_B3_0 = 0;
		G_B3_1 = G_B2_0;
	}

IL_0027:
	{
		G_B3_1->___isHover_2 = G_B3_0;
		bool L_3 = ___isActive;
		G_B4_0 = (&V_0);
		if (!L_3)
		{
			G_B5_0 = (&V_0);
			goto IL_003b;
		}
	}
	{
		G_B6_0 = 1;
		G_B6_1 = G_B4_0;
		goto IL_003c;
	}

IL_003b:
	{
		G_B6_0 = 0;
		G_B6_1 = G_B5_0;
	}

IL_003c:
	{
		G_B6_1->___isActive_3 = G_B6_0;
		bool L_4 = ___on;
		G_B7_0 = (&V_0);
		if (!L_4)
		{
			G_B8_0 = (&V_0);
			goto IL_0050;
		}
	}
	{
		G_B9_0 = 1;
		G_B9_1 = G_B7_0;
		goto IL_0051;
	}

IL_0050:
	{
		G_B9_0 = 0;
		G_B9_1 = G_B8_0;
	}

IL_0051:
	{
		G_B9_1->___on_4 = G_B9_0;
		bool L_5 = ___hasKeyboardFocus;
		G_B10_0 = (&V_0);
		if (!L_5)
		{
			G_B11_0 = (&V_0);
			goto IL_0065;
		}
	}
	{
		G_B12_0 = 1;
		G_B12_1 = G_B10_0;
		goto IL_0066;
	}

IL_0065:
	{
		G_B12_0 = 0;
		G_B12_1 = G_B11_0;
	}

IL_0066:
	{
		G_B12_1->___hasKeyboardFocus_5 = G_B12_0;
		GUIContent_t6_163 * L_6 = ___content;
		IL2CPP_RUNTIME_CLASS_INIT(GUIStyle_t6_166_il2cpp_TypeInfo_var);
		GUIStyle_Internal_Draw_m6_1350(NULL /*static, unused*/, L_6, (&V_0), /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.GUIStyle::Draw(UnityEngine.Rect,System.Boolean,System.Boolean,System.Boolean,System.Boolean)
extern TypeInfo* GUIContent_t6_163_il2cpp_TypeInfo_var;
extern TypeInfo* GUIStyle_t6_166_il2cpp_TypeInfo_var;
extern "C" void GUIStyle_Draw_m6_1309 (GUIStyle_t6_166 * __this, Rect_t6_52  ___position, bool ___isHover, bool ___isActive, bool ___on, bool ___hasKeyboardFocus, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GUIContent_t6_163_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(985);
		GUIStyle_t6_166_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(988);
		s_Il2CppMethodIntialized = true;
	}
	{
		IntPtr_t L_0 = (__this->___m_Ptr_0);
		Rect_t6_52  L_1 = ___position;
		IL2CPP_RUNTIME_CLASS_INIT(GUIContent_t6_163_il2cpp_TypeInfo_var);
		GUIContent_t6_163 * L_2 = ((GUIContent_t6_163_StaticFields*)GUIContent_t6_163_il2cpp_TypeInfo_var->static_fields)->___none_6;
		bool L_3 = ___isHover;
		bool L_4 = ___isActive;
		bool L_5 = ___on;
		bool L_6 = ___hasKeyboardFocus;
		IL2CPP_RUNTIME_CLASS_INIT(GUIStyle_t6_166_il2cpp_TypeInfo_var);
		GUIStyle_Internal_Draw_m6_1308(NULL /*static, unused*/, L_0, L_1, L_2, L_3, L_4, L_5, L_6, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.GUIStyle::Draw(UnityEngine.Rect,UnityEngine.GUIContent,System.Boolean,System.Boolean,System.Boolean,System.Boolean)
extern TypeInfo* GUIStyle_t6_166_il2cpp_TypeInfo_var;
extern "C" void GUIStyle_Draw_m6_1310 (GUIStyle_t6_166 * __this, Rect_t6_52  ___position, GUIContent_t6_163 * ___content, bool ___isHover, bool ___isActive, bool ___on, bool ___hasKeyboardFocus, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GUIStyle_t6_166_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(988);
		s_Il2CppMethodIntialized = true;
	}
	{
		IntPtr_t L_0 = (__this->___m_Ptr_0);
		Rect_t6_52  L_1 = ___position;
		GUIContent_t6_163 * L_2 = ___content;
		bool L_3 = ___isHover;
		bool L_4 = ___isActive;
		bool L_5 = ___on;
		bool L_6 = ___hasKeyboardFocus;
		IL2CPP_RUNTIME_CLASS_INIT(GUIStyle_t6_166_il2cpp_TypeInfo_var);
		GUIStyle_Internal_Draw_m6_1308(NULL /*static, unused*/, L_0, L_1, L_2, L_3, L_4, L_5, L_6, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.GUIStyle::Draw(UnityEngine.Rect,UnityEngine.GUIContent,System.Int32)
extern "C" void GUIStyle_Draw_m6_1311 (GUIStyle_t6_166 * __this, Rect_t6_52  ___position, GUIContent_t6_163 * ___content, int32_t ___controlID, const MethodInfo* method)
{
	{
		Rect_t6_52  L_0 = ___position;
		GUIContent_t6_163 * L_1 = ___content;
		int32_t L_2 = ___controlID;
		GUIStyle_Draw_m6_1312(__this, L_0, L_1, L_2, 0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.GUIStyle::Draw(UnityEngine.Rect,UnityEngine.GUIContent,System.Int32,System.Boolean)
extern TypeInfo* GUIStyle_t6_166_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2828;
extern "C" void GUIStyle_Draw_m6_1312 (GUIStyle_t6_166 * __this, Rect_t6_52  ___position, GUIContent_t6_163 * ___content, int32_t ___controlID, bool ___on, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GUIStyle_t6_166_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(988);
		_stringLiteral2828 = il2cpp_codegen_string_literal_from_index(2828);
		s_Il2CppMethodIntialized = true;
	}
	{
		GUIContent_t6_163 * L_0 = ___content;
		if (!L_0)
		{
			goto IL_001b;
		}
	}
	{
		IntPtr_t L_1 = (__this->___m_Ptr_0);
		Rect_t6_52  L_2 = ___position;
		GUIContent_t6_163 * L_3 = ___content;
		int32_t L_4 = ___controlID;
		bool L_5 = ___on;
		IL2CPP_RUNTIME_CLASS_INIT(GUIStyle_t6_166_il2cpp_TypeInfo_var);
		GUIStyle_Internal_Draw2_m6_1351(NULL /*static, unused*/, L_1, L_2, L_3, L_4, L_5, /*hidden argument*/NULL);
		goto IL_0025;
	}

IL_001b:
	{
		Debug_LogError_m6_490(NULL /*static, unused*/, _stringLiteral2828, /*hidden argument*/NULL);
	}

IL_0025:
	{
		return;
	}
}
// System.Void UnityEngine.GUIStyle::DrawCursor(UnityEngine.Rect,UnityEngine.GUIContent,System.Int32,System.Int32)
extern TypeInfo* GUI_t6_160_il2cpp_TypeInfo_var;
extern TypeInfo* GUIStyle_t6_166_il2cpp_TypeInfo_var;
extern "C" void GUIStyle_DrawCursor_m6_1313 (GUIStyle_t6_166 * __this, Rect_t6_52  ___position, GUIContent_t6_163 * ___content, int32_t ___controlID, int32_t ___Character, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GUI_t6_160_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(982);
		GUIStyle_t6_166_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(988);
		s_Il2CppMethodIntialized = true;
	}
	Event_t6_154 * V_0 = {0};
	Color_t6_40  V_1 = {0};
	float V_2 = 0.0f;
	float V_3 = 0.0f;
	{
		Event_t6_154 * L_0 = Event_get_current_m6_989(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_0 = L_0;
		Event_t6_154 * L_1 = V_0;
		NullCheck(L_1);
		int32_t L_2 = Event_get_type_m6_1003(L_1, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_2) == ((uint32_t)7))))
		{
			goto IL_0083;
		}
	}
	{
		Color__ctor_m6_229((&V_1), (0.0f), (0.0f), (0.0f), (0.0f), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t6_160_il2cpp_TypeInfo_var);
		GUISkin_t6_161 * L_3 = GUI_get_skin_m6_1041(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_3);
		GUISettings_t6_177 * L_4 = GUISkin_get_settings_m6_1261(L_3, /*hidden argument*/NULL);
		NullCheck(L_4);
		float L_5 = GUISettings_get_cursorFlashSpeed_m6_1208(L_4, /*hidden argument*/NULL);
		V_2 = L_5;
		float L_6 = Time_get_realtimeSinceStartup_m6_656(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(GUIStyle_t6_166_il2cpp_TypeInfo_var);
		float L_7 = GUIStyle_Internal_GetCursorFlashOffset_m6_1353(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_8 = V_2;
		float L_9 = V_2;
		V_3 = ((float)((float)(fmodf(((float)((float)L_6-(float)L_7)), L_8))/(float)L_9));
		float L_10 = V_2;
		if ((((float)L_10) == ((float)(0.0f))))
		{
			goto IL_0063;
		}
	}
	{
		float L_11 = V_3;
		if ((!(((float)L_11) < ((float)(0.5f)))))
		{
			goto IL_0073;
		}
	}

IL_0063:
	{
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t6_160_il2cpp_TypeInfo_var);
		GUISkin_t6_161 * L_12 = GUI_get_skin_m6_1041(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_12);
		GUISettings_t6_177 * L_13 = GUISkin_get_settings_m6_1261(L_12, /*hidden argument*/NULL);
		NullCheck(L_13);
		Color_t6_40  L_14 = GUISettings_get_cursorColor_m6_1207(L_13, /*hidden argument*/NULL);
		V_1 = L_14;
	}

IL_0073:
	{
		IntPtr_t L_15 = (__this->___m_Ptr_0);
		Rect_t6_52  L_16 = ___position;
		GUIContent_t6_163 * L_17 = ___content;
		int32_t L_18 = ___Character;
		Color_t6_40  L_19 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(GUIStyle_t6_166_il2cpp_TypeInfo_var);
		GUIStyle_Internal_DrawCursor_m6_1354(NULL /*static, unused*/, L_15, L_16, L_17, L_18, L_19, /*hidden argument*/NULL);
	}

IL_0083:
	{
		return;
	}
}
// System.Void UnityEngine.GUIStyle::DrawWithTextSelection(UnityEngine.Rect,UnityEngine.GUIContent,System.Int32,System.Int32,System.Int32,System.Boolean)
extern TypeInfo* GUI_t6_160_il2cpp_TypeInfo_var;
extern TypeInfo* GUIStyle_t6_166_il2cpp_TypeInfo_var;
extern TypeInfo* Internal_DrawWithTextSelectionArguments_t6_188_il2cpp_TypeInfo_var;
extern TypeInfo* GUIUtility_t6_185_il2cpp_TypeInfo_var;
extern "C" void GUIStyle_DrawWithTextSelection_m6_1314 (GUIStyle_t6_166 * __this, Rect_t6_52  ___position, GUIContent_t6_163 * ___content, int32_t ___controlID, int32_t ___firstSelectedCharacter, int32_t ___lastSelectedCharacter, bool ___drawSelectionAsComposition, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GUI_t6_160_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(982);
		GUIStyle_t6_166_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(988);
		Internal_DrawWithTextSelectionArguments_t6_188_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1012);
		GUIUtility_t6_185_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(984);
		s_Il2CppMethodIntialized = true;
	}
	Event_t6_154 * V_0 = {0};
	Color_t6_40  V_1 = {0};
	float V_2 = 0.0f;
	float V_3 = 0.0f;
	Internal_DrawWithTextSelectionArguments_t6_188  V_4 = {0};
	Internal_DrawWithTextSelectionArguments_t6_188 * G_B5_0 = {0};
	Internal_DrawWithTextSelectionArguments_t6_188 * G_B4_0 = {0};
	int32_t G_B6_0 = 0;
	Internal_DrawWithTextSelectionArguments_t6_188 * G_B6_1 = {0};
	Internal_DrawWithTextSelectionArguments_t6_188 * G_B8_0 = {0};
	Internal_DrawWithTextSelectionArguments_t6_188 * G_B7_0 = {0};
	int32_t G_B9_0 = 0;
	Internal_DrawWithTextSelectionArguments_t6_188 * G_B9_1 = {0};
	Internal_DrawWithTextSelectionArguments_t6_188 * G_B12_0 = {0};
	Internal_DrawWithTextSelectionArguments_t6_188 * G_B10_0 = {0};
	Internal_DrawWithTextSelectionArguments_t6_188 * G_B11_0 = {0};
	int32_t G_B13_0 = 0;
	Internal_DrawWithTextSelectionArguments_t6_188 * G_B13_1 = {0};
	Internal_DrawWithTextSelectionArguments_t6_188 * G_B15_0 = {0};
	Internal_DrawWithTextSelectionArguments_t6_188 * G_B14_0 = {0};
	int32_t G_B16_0 = 0;
	Internal_DrawWithTextSelectionArguments_t6_188 * G_B16_1 = {0};
	{
		Event_t6_154 * L_0 = Event_get_current_m6_989(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_0 = L_0;
		Color__ctor_m6_229((&V_1), (0.0f), (0.0f), (0.0f), (0.0f), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t6_160_il2cpp_TypeInfo_var);
		GUISkin_t6_161 * L_1 = GUI_get_skin_m6_1041(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_1);
		GUISettings_t6_177 * L_2 = GUISkin_get_settings_m6_1261(L_1, /*hidden argument*/NULL);
		NullCheck(L_2);
		float L_3 = GUISettings_get_cursorFlashSpeed_m6_1208(L_2, /*hidden argument*/NULL);
		V_2 = L_3;
		float L_4 = Time_get_realtimeSinceStartup_m6_656(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(GUIStyle_t6_166_il2cpp_TypeInfo_var);
		float L_5 = GUIStyle_Internal_GetCursorFlashOffset_m6_1353(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_6 = V_2;
		float L_7 = V_2;
		V_3 = ((float)((float)(fmodf(((float)((float)L_4-(float)L_5)), L_6))/(float)L_7));
		float L_8 = V_2;
		if ((((float)L_8) == ((float)(0.0f))))
		{
			goto IL_0057;
		}
	}
	{
		float L_9 = V_3;
		if ((!(((float)L_9) < ((float)(0.5f)))))
		{
			goto IL_0067;
		}
	}

IL_0057:
	{
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t6_160_il2cpp_TypeInfo_var);
		GUISkin_t6_161 * L_10 = GUI_get_skin_m6_1041(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_10);
		GUISettings_t6_177 * L_11 = GUISkin_get_settings_m6_1261(L_10, /*hidden argument*/NULL);
		NullCheck(L_11);
		Color_t6_40  L_12 = GUISettings_get_cursorColor_m6_1207(L_11, /*hidden argument*/NULL);
		V_1 = L_12;
	}

IL_0067:
	{
		Initobj (Internal_DrawWithTextSelectionArguments_t6_188_il2cpp_TypeInfo_var, (&V_4));
		IntPtr_t L_13 = (__this->___m_Ptr_0);
		(&V_4)->___target_0 = L_13;
		Rect_t6_52  L_14 = ___position;
		(&V_4)->___position_1 = L_14;
		int32_t L_15 = ___firstSelectedCharacter;
		(&V_4)->___firstPos_2 = L_15;
		int32_t L_16 = ___lastSelectedCharacter;
		(&V_4)->___lastPos_3 = L_16;
		Color_t6_40  L_17 = V_1;
		(&V_4)->___cursorColor_4 = L_17;
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t6_160_il2cpp_TypeInfo_var);
		GUISkin_t6_161 * L_18 = GUI_get_skin_m6_1041(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_18);
		GUISettings_t6_177 * L_19 = GUISkin_get_settings_m6_1261(L_18, /*hidden argument*/NULL);
		NullCheck(L_19);
		Color_t6_40  L_20 = GUISettings_get_selectionColor_m6_1209(L_19, /*hidden argument*/NULL);
		(&V_4)->___selectionColor_5 = L_20;
		Event_t6_154 * L_21 = V_0;
		NullCheck(L_21);
		Vector2_t6_48  L_22 = Event_get_mousePosition_m6_970(L_21, /*hidden argument*/NULL);
		bool L_23 = Rect_Contains_m6_286((&___position), L_22, /*hidden argument*/NULL);
		G_B4_0 = (&V_4);
		if (!L_23)
		{
			G_B5_0 = (&V_4);
			goto IL_00ce;
		}
	}
	{
		G_B6_0 = 1;
		G_B6_1 = G_B4_0;
		goto IL_00cf;
	}

IL_00ce:
	{
		G_B6_0 = 0;
		G_B6_1 = G_B5_0;
	}

IL_00cf:
	{
		G_B6_1->___isHover_6 = G_B6_0;
		int32_t L_24 = ___controlID;
		IL2CPP_RUNTIME_CLASS_INIT(GUIUtility_t6_185_il2cpp_TypeInfo_var);
		int32_t L_25 = GUIUtility_get_hotControl_m6_1370(NULL /*static, unused*/, /*hidden argument*/NULL);
		G_B7_0 = (&V_4);
		if ((!(((uint32_t)L_24) == ((uint32_t)L_25))))
		{
			G_B8_0 = (&V_4);
			goto IL_00e7;
		}
	}
	{
		G_B9_0 = 1;
		G_B9_1 = G_B7_0;
		goto IL_00e8;
	}

IL_00e7:
	{
		G_B9_0 = 0;
		G_B9_1 = G_B8_0;
	}

IL_00e8:
	{
		G_B9_1->___isActive_7 = G_B9_0;
		(&V_4)->___on_8 = 0;
		int32_t L_26 = ___controlID;
		IL2CPP_RUNTIME_CLASS_INIT(GUIUtility_t6_185_il2cpp_TypeInfo_var);
		int32_t L_27 = GUIUtility_get_keyboardControl_m6_1383(NULL /*static, unused*/, /*hidden argument*/NULL);
		G_B10_0 = (&V_4);
		if ((!(((uint32_t)L_26) == ((uint32_t)L_27))))
		{
			G_B12_0 = (&V_4);
			goto IL_0112;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GUIStyle_t6_166_il2cpp_TypeInfo_var);
		bool L_28 = ((GUIStyle_t6_166_StaticFields*)GUIStyle_t6_166_il2cpp_TypeInfo_var->static_fields)->___showKeyboardFocus_14;
		G_B11_0 = G_B10_0;
		if (!L_28)
		{
			G_B12_0 = G_B10_0;
			goto IL_0112;
		}
	}
	{
		G_B13_0 = 1;
		G_B13_1 = G_B11_0;
		goto IL_0113;
	}

IL_0112:
	{
		G_B13_0 = 0;
		G_B13_1 = G_B12_0;
	}

IL_0113:
	{
		G_B13_1->___hasKeyboardFocus_9 = G_B13_0;
		bool L_29 = ___drawSelectionAsComposition;
		G_B14_0 = (&V_4);
		if (!L_29)
		{
			G_B15_0 = (&V_4);
			goto IL_0127;
		}
	}
	{
		G_B16_0 = 1;
		G_B16_1 = G_B14_0;
		goto IL_0128;
	}

IL_0127:
	{
		G_B16_0 = 0;
		G_B16_1 = G_B15_0;
	}

IL_0128:
	{
		G_B16_1->___drawSelectionAsComposition_10 = G_B16_0;
		GUIContent_t6_163 * L_30 = ___content;
		IL2CPP_RUNTIME_CLASS_INIT(GUIStyle_t6_166_il2cpp_TypeInfo_var);
		GUIStyle_Internal_DrawWithTextSelection_m6_1356(NULL /*static, unused*/, L_30, (&V_4), /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.GUIStyle::DrawWithTextSelection(UnityEngine.Rect,UnityEngine.GUIContent,System.Int32,System.Int32,System.Int32)
extern "C" void GUIStyle_DrawWithTextSelection_m6_1315 (GUIStyle_t6_166 * __this, Rect_t6_52  ___position, GUIContent_t6_163 * ___content, int32_t ___controlID, int32_t ___firstSelectedCharacter, int32_t ___lastSelectedCharacter, const MethodInfo* method)
{
	{
		Rect_t6_52  L_0 = ___position;
		GUIContent_t6_163 * L_1 = ___content;
		int32_t L_2 = ___controlID;
		int32_t L_3 = ___firstSelectedCharacter;
		int32_t L_4 = ___lastSelectedCharacter;
		GUIStyle_DrawWithTextSelection_m6_1314(__this, L_0, L_1, L_2, L_3, L_4, 0, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.GUIStyle UnityEngine.GUIStyle::get_none()
extern TypeInfo* GUIStyle_t6_166_il2cpp_TypeInfo_var;
extern "C" GUIStyle_t6_166 * GUIStyle_get_none_m6_1316 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GUIStyle_t6_166_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(988);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GUIStyle_t6_166_il2cpp_TypeInfo_var);
		GUIStyle_t6_166 * L_0 = ((GUIStyle_t6_166_StaticFields*)GUIStyle_t6_166_il2cpp_TypeInfo_var->static_fields)->___s_None_15;
		if (L_0)
		{
			goto IL_0014;
		}
	}
	{
		GUIStyle_t6_166 * L_1 = (GUIStyle_t6_166 *)il2cpp_codegen_object_new (GUIStyle_t6_166_il2cpp_TypeInfo_var);
		GUIStyle__ctor_m6_1298(L_1, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(GUIStyle_t6_166_il2cpp_TypeInfo_var);
		((GUIStyle_t6_166_StaticFields*)GUIStyle_t6_166_il2cpp_TypeInfo_var->static_fields)->___s_None_15 = L_1;
	}

IL_0014:
	{
		IL2CPP_RUNTIME_CLASS_INIT(GUIStyle_t6_166_il2cpp_TypeInfo_var);
		GUIStyle_t6_166 * L_2 = ((GUIStyle_t6_166_StaticFields*)GUIStyle_t6_166_il2cpp_TypeInfo_var->static_fields)->___s_None_15;
		return L_2;
	}
}
// UnityEngine.Vector2 UnityEngine.GUIStyle::GetCursorPixelPosition(UnityEngine.Rect,UnityEngine.GUIContent,System.Int32)
extern TypeInfo* GUIStyle_t6_166_il2cpp_TypeInfo_var;
extern "C" Vector2_t6_48  GUIStyle_GetCursorPixelPosition_m6_1317 (GUIStyle_t6_166 * __this, Rect_t6_52  ___position, GUIContent_t6_163 * ___content, int32_t ___cursorStringIndex, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GUIStyle_t6_166_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(988);
		s_Il2CppMethodIntialized = true;
	}
	Vector2_t6_48  V_0 = {0};
	{
		IntPtr_t L_0 = (__this->___m_Ptr_0);
		Rect_t6_52  L_1 = ___position;
		GUIContent_t6_163 * L_2 = ___content;
		int32_t L_3 = ___cursorStringIndex;
		IL2CPP_RUNTIME_CLASS_INIT(GUIStyle_t6_166_il2cpp_TypeInfo_var);
		GUIStyle_Internal_GetCursorPixelPosition_m6_1358(NULL /*static, unused*/, L_0, L_1, L_2, L_3, (&V_0), /*hidden argument*/NULL);
		Vector2_t6_48  L_4 = V_0;
		return L_4;
	}
}
// System.Int32 UnityEngine.GUIStyle::GetCursorStringIndex(UnityEngine.Rect,UnityEngine.GUIContent,UnityEngine.Vector2)
extern TypeInfo* GUIStyle_t6_166_il2cpp_TypeInfo_var;
extern "C" int32_t GUIStyle_GetCursorStringIndex_m6_1318 (GUIStyle_t6_166 * __this, Rect_t6_52  ___position, GUIContent_t6_163 * ___content, Vector2_t6_48  ___cursorPixelPosition, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GUIStyle_t6_166_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(988);
		s_Il2CppMethodIntialized = true;
	}
	{
		IntPtr_t L_0 = (__this->___m_Ptr_0);
		Rect_t6_52  L_1 = ___position;
		GUIContent_t6_163 * L_2 = ___content;
		Vector2_t6_48  L_3 = ___cursorPixelPosition;
		IL2CPP_RUNTIME_CLASS_INIT(GUIStyle_t6_166_il2cpp_TypeInfo_var);
		int32_t L_4 = GUIStyle_Internal_GetCursorStringIndex_m6_1360(NULL /*static, unused*/, L_0, L_1, L_2, L_3, /*hidden argument*/NULL);
		return L_4;
	}
}
// UnityEngine.Vector2 UnityEngine.GUIStyle::CalcSize(UnityEngine.GUIContent)
extern TypeInfo* GUIStyle_t6_166_il2cpp_TypeInfo_var;
extern "C" Vector2_t6_48  GUIStyle_CalcSize_m6_1319 (GUIStyle_t6_166 * __this, GUIContent_t6_163 * ___content, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GUIStyle_t6_166_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(988);
		s_Il2CppMethodIntialized = true;
	}
	Vector2_t6_48  V_0 = {0};
	{
		IntPtr_t L_0 = (__this->___m_Ptr_0);
		GUIContent_t6_163 * L_1 = ___content;
		IL2CPP_RUNTIME_CLASS_INIT(GUIStyle_t6_166_il2cpp_TypeInfo_var);
		GUIStyle_Internal_CalcSize_m6_1362(NULL /*static, unused*/, L_0, L_1, (&V_0), /*hidden argument*/NULL);
		Vector2_t6_48  L_2 = V_0;
		return L_2;
	}
}
// System.Single UnityEngine.GUIStyle::CalcHeight(UnityEngine.GUIContent,System.Single)
extern TypeInfo* GUIStyle_t6_166_il2cpp_TypeInfo_var;
extern "C" float GUIStyle_CalcHeight_m6_1320 (GUIStyle_t6_166 * __this, GUIContent_t6_163 * ___content, float ___width, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GUIStyle_t6_166_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(988);
		s_Il2CppMethodIntialized = true;
	}
	{
		IntPtr_t L_0 = (__this->___m_Ptr_0);
		GUIContent_t6_163 * L_1 = ___content;
		float L_2 = ___width;
		IL2CPP_RUNTIME_CLASS_INIT(GUIStyle_t6_166_il2cpp_TypeInfo_var);
		float L_3 = GUIStyle_Internal_CalcHeight_m6_1363(NULL /*static, unused*/, L_0, L_1, L_2, /*hidden argument*/NULL);
		return L_3;
	}
}
// System.Boolean UnityEngine.GUIStyle::get_isHeightDependantOnWidth()
extern "C" bool GUIStyle_get_isHeightDependantOnWidth_m6_1321 (GUIStyle_t6_166 * __this, const MethodInfo* method)
{
	int32_t G_B4_0 = 0;
	int32_t G_B6_0 = 0;
	{
		float L_0 = GUIStyle_get_fixedHeight_m6_1343(__this, /*hidden argument*/NULL);
		if ((!(((float)L_0) == ((float)(0.0f)))))
		{
			goto IL_002c;
		}
	}
	{
		bool L_1 = GUIStyle_get_wordWrap_m6_1334(__this, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0029;
		}
	}
	{
		int32_t L_2 = GUIStyle_get_imagePosition_m6_1332(__this, /*hidden argument*/NULL);
		G_B4_0 = ((((int32_t)((((int32_t)L_2) == ((int32_t)2))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_002a;
	}

IL_0029:
	{
		G_B4_0 = 0;
	}

IL_002a:
	{
		G_B6_0 = G_B4_0;
		goto IL_002d;
	}

IL_002c:
	{
		G_B6_0 = 0;
	}

IL_002d:
	{
		return G_B6_0;
	}
}
// System.Void UnityEngine.GUIStyle::CalcMinMaxWidth(UnityEngine.GUIContent,System.Single&,System.Single&)
extern TypeInfo* GUIStyle_t6_166_il2cpp_TypeInfo_var;
extern "C" void GUIStyle_CalcMinMaxWidth_m6_1322 (GUIStyle_t6_166 * __this, GUIContent_t6_163 * ___content, float* ___minWidth, float* ___maxWidth, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GUIStyle_t6_166_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(988);
		s_Il2CppMethodIntialized = true;
	}
	{
		IntPtr_t L_0 = (__this->___m_Ptr_0);
		GUIContent_t6_163 * L_1 = ___content;
		float* L_2 = ___minWidth;
		float* L_3 = ___maxWidth;
		IL2CPP_RUNTIME_CLASS_INIT(GUIStyle_t6_166_il2cpp_TypeInfo_var);
		GUIStyle_Internal_CalcMinMaxWidth_m6_1364(NULL /*static, unused*/, L_0, L_1, L_2, L_3, /*hidden argument*/NULL);
		return;
	}
}
// System.String UnityEngine.GUIStyle::ToString()
extern TypeInfo* ObjectU5BU5D_t1_157_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2829;
extern "C" String_t* GUIStyle_ToString_m6_1323 (GUIStyle_t6_166 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ObjectU5BU5D_t1_157_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(19);
		_stringLiteral2829 = il2cpp_codegen_string_literal_from_index(2829);
		s_Il2CppMethodIntialized = true;
	}
	{
		ObjectU5BU5D_t1_157* L_0 = ((ObjectU5BU5D_t1_157*)SZArrayNew(ObjectU5BU5D_t1_157_il2cpp_TypeInfo_var, 1));
		String_t* L_1 = GUIStyle_get_name_m6_1327(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, 0);
		ArrayElementTypeCheck (L_0, L_1);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_0, 0, sizeof(Object_t *))) = (Object_t *)L_1;
		String_t* L_2 = UnityString_Format_m6_437(NULL /*static, unused*/, _stringLiteral2829, L_0, /*hidden argument*/NULL);
		return L_2;
	}
}
// System.Void UnityEngine.GUIStyle::Init()
extern "C" void GUIStyle_Init_m6_1324 (GUIStyle_t6_166 * __this, const MethodInfo* method)
{
	typedef void (*GUIStyle_Init_m6_1324_ftn) (GUIStyle_t6_166 *);
	static GUIStyle_Init_m6_1324_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GUIStyle_Init_m6_1324_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.GUIStyle::Init()");
	_il2cpp_icall_func(__this);
}
// System.Void UnityEngine.GUIStyle::InitCopy(UnityEngine.GUIStyle)
extern "C" void GUIStyle_InitCopy_m6_1325 (GUIStyle_t6_166 * __this, GUIStyle_t6_166 * ___other, const MethodInfo* method)
{
	typedef void (*GUIStyle_InitCopy_m6_1325_ftn) (GUIStyle_t6_166 *, GUIStyle_t6_166 *);
	static GUIStyle_InitCopy_m6_1325_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GUIStyle_InitCopy_m6_1325_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.GUIStyle::InitCopy(UnityEngine.GUIStyle)");
	_il2cpp_icall_func(__this, ___other);
}
// System.Void UnityEngine.GUIStyle::Cleanup()
extern "C" void GUIStyle_Cleanup_m6_1326 (GUIStyle_t6_166 * __this, const MethodInfo* method)
{
	typedef void (*GUIStyle_Cleanup_m6_1326_ftn) (GUIStyle_t6_166 *);
	static GUIStyle_Cleanup_m6_1326_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GUIStyle_Cleanup_m6_1326_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.GUIStyle::Cleanup()");
	_il2cpp_icall_func(__this);
}
// System.String UnityEngine.GUIStyle::get_name()
extern "C" String_t* GUIStyle_get_name_m6_1327 (GUIStyle_t6_166 * __this, const MethodInfo* method)
{
	typedef String_t* (*GUIStyle_get_name_m6_1327_ftn) (GUIStyle_t6_166 *);
	static GUIStyle_get_name_m6_1327_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GUIStyle_get_name_m6_1327_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.GUIStyle::get_name()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.GUIStyle::set_name(System.String)
extern "C" void GUIStyle_set_name_m6_1328 (GUIStyle_t6_166 * __this, String_t* ___value, const MethodInfo* method)
{
	typedef void (*GUIStyle_set_name_m6_1328_ftn) (GUIStyle_t6_166 *, String_t*);
	static GUIStyle_set_name_m6_1328_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GUIStyle_set_name_m6_1328_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.GUIStyle::set_name(System.String)");
	_il2cpp_icall_func(__this, ___value);
}
// System.IntPtr UnityEngine.GUIStyle::GetStyleStatePtr(System.Int32)
extern "C" IntPtr_t GUIStyle_GetStyleStatePtr_m6_1329 (GUIStyle_t6_166 * __this, int32_t ___idx, const MethodInfo* method)
{
	typedef IntPtr_t (*GUIStyle_GetStyleStatePtr_m6_1329_ftn) (GUIStyle_t6_166 *, int32_t);
	static GUIStyle_GetStyleStatePtr_m6_1329_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GUIStyle_GetStyleStatePtr_m6_1329_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.GUIStyle::GetStyleStatePtr(System.Int32)");
	return _il2cpp_icall_func(__this, ___idx);
}
// System.IntPtr UnityEngine.GUIStyle::GetRectOffsetPtr(System.Int32)
extern "C" IntPtr_t GUIStyle_GetRectOffsetPtr_m6_1330 (GUIStyle_t6_166 * __this, int32_t ___idx, const MethodInfo* method)
{
	typedef IntPtr_t (*GUIStyle_GetRectOffsetPtr_m6_1330_ftn) (GUIStyle_t6_166 *, int32_t);
	static GUIStyle_GetRectOffsetPtr_m6_1330_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GUIStyle_GetRectOffsetPtr_m6_1330_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.GUIStyle::GetRectOffsetPtr(System.Int32)");
	return _il2cpp_icall_func(__this, ___idx);
}
// System.Void UnityEngine.GUIStyle::AssignRectOffset(System.Int32,System.IntPtr)
extern "C" void GUIStyle_AssignRectOffset_m6_1331 (GUIStyle_t6_166 * __this, int32_t ___idx, IntPtr_t ___srcRectOffset, const MethodInfo* method)
{
	typedef void (*GUIStyle_AssignRectOffset_m6_1331_ftn) (GUIStyle_t6_166 *, int32_t, IntPtr_t);
	static GUIStyle_AssignRectOffset_m6_1331_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GUIStyle_AssignRectOffset_m6_1331_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.GUIStyle::AssignRectOffset(System.Int32,System.IntPtr)");
	_il2cpp_icall_func(__this, ___idx, ___srcRectOffset);
}
// UnityEngine.ImagePosition UnityEngine.GUIStyle::get_imagePosition()
extern "C" int32_t GUIStyle_get_imagePosition_m6_1332 (GUIStyle_t6_166 * __this, const MethodInfo* method)
{
	typedef int32_t (*GUIStyle_get_imagePosition_m6_1332_ftn) (GUIStyle_t6_166 *);
	static GUIStyle_get_imagePosition_m6_1332_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GUIStyle_get_imagePosition_m6_1332_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.GUIStyle::get_imagePosition()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.GUIStyle::set_alignment(UnityEngine.TextAnchor)
extern "C" void GUIStyle_set_alignment_m6_1333 (GUIStyle_t6_166 * __this, int32_t ___value, const MethodInfo* method)
{
	typedef void (*GUIStyle_set_alignment_m6_1333_ftn) (GUIStyle_t6_166 *, int32_t);
	static GUIStyle_set_alignment_m6_1333_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GUIStyle_set_alignment_m6_1333_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.GUIStyle::set_alignment(UnityEngine.TextAnchor)");
	_il2cpp_icall_func(__this, ___value);
}
// System.Boolean UnityEngine.GUIStyle::get_wordWrap()
extern "C" bool GUIStyle_get_wordWrap_m6_1334 (GUIStyle_t6_166 * __this, const MethodInfo* method)
{
	typedef bool (*GUIStyle_get_wordWrap_m6_1334_ftn) (GUIStyle_t6_166 *);
	static GUIStyle_get_wordWrap_m6_1334_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GUIStyle_get_wordWrap_m6_1334_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.GUIStyle::get_wordWrap()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.GUIStyle::set_wordWrap(System.Boolean)
extern "C" void GUIStyle_set_wordWrap_m6_1335 (GUIStyle_t6_166 * __this, bool ___value, const MethodInfo* method)
{
	typedef void (*GUIStyle_set_wordWrap_m6_1335_ftn) (GUIStyle_t6_166 *, bool);
	static GUIStyle_set_wordWrap_m6_1335_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GUIStyle_set_wordWrap_m6_1335_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.GUIStyle::set_wordWrap(System.Boolean)");
	_il2cpp_icall_func(__this, ___value);
}
// UnityEngine.Vector2 UnityEngine.GUIStyle::get_contentOffset()
extern "C" Vector2_t6_48  GUIStyle_get_contentOffset_m6_1336 (GUIStyle_t6_166 * __this, const MethodInfo* method)
{
	Vector2_t6_48  V_0 = {0};
	{
		GUIStyle_INTERNAL_get_contentOffset_m6_1338(__this, (&V_0), /*hidden argument*/NULL);
		Vector2_t6_48  L_0 = V_0;
		return L_0;
	}
}
// System.Void UnityEngine.GUIStyle::set_contentOffset(UnityEngine.Vector2)
extern "C" void GUIStyle_set_contentOffset_m6_1337 (GUIStyle_t6_166 * __this, Vector2_t6_48  ___value, const MethodInfo* method)
{
	{
		GUIStyle_INTERNAL_set_contentOffset_m6_1339(__this, (&___value), /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.GUIStyle::INTERNAL_get_contentOffset(UnityEngine.Vector2&)
extern "C" void GUIStyle_INTERNAL_get_contentOffset_m6_1338 (GUIStyle_t6_166 * __this, Vector2_t6_48 * ___value, const MethodInfo* method)
{
	typedef void (*GUIStyle_INTERNAL_get_contentOffset_m6_1338_ftn) (GUIStyle_t6_166 *, Vector2_t6_48 *);
	static GUIStyle_INTERNAL_get_contentOffset_m6_1338_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GUIStyle_INTERNAL_get_contentOffset_m6_1338_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.GUIStyle::INTERNAL_get_contentOffset(UnityEngine.Vector2&)");
	_il2cpp_icall_func(__this, ___value);
}
// System.Void UnityEngine.GUIStyle::INTERNAL_set_contentOffset(UnityEngine.Vector2&)
extern "C" void GUIStyle_INTERNAL_set_contentOffset_m6_1339 (GUIStyle_t6_166 * __this, Vector2_t6_48 * ___value, const MethodInfo* method)
{
	typedef void (*GUIStyle_INTERNAL_set_contentOffset_m6_1339_ftn) (GUIStyle_t6_166 *, Vector2_t6_48 *);
	static GUIStyle_INTERNAL_set_contentOffset_m6_1339_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GUIStyle_INTERNAL_set_contentOffset_m6_1339_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.GUIStyle::INTERNAL_set_contentOffset(UnityEngine.Vector2&)");
	_il2cpp_icall_func(__this, ___value);
}
// System.Void UnityEngine.GUIStyle::set_Internal_clipOffset(UnityEngine.Vector2)
extern "C" void GUIStyle_set_Internal_clipOffset_m6_1340 (GUIStyle_t6_166 * __this, Vector2_t6_48  ___value, const MethodInfo* method)
{
	{
		GUIStyle_INTERNAL_set_Internal_clipOffset_m6_1341(__this, (&___value), /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.GUIStyle::INTERNAL_set_Internal_clipOffset(UnityEngine.Vector2&)
extern "C" void GUIStyle_INTERNAL_set_Internal_clipOffset_m6_1341 (GUIStyle_t6_166 * __this, Vector2_t6_48 * ___value, const MethodInfo* method)
{
	typedef void (*GUIStyle_INTERNAL_set_Internal_clipOffset_m6_1341_ftn) (GUIStyle_t6_166 *, Vector2_t6_48 *);
	static GUIStyle_INTERNAL_set_Internal_clipOffset_m6_1341_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GUIStyle_INTERNAL_set_Internal_clipOffset_m6_1341_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.GUIStyle::INTERNAL_set_Internal_clipOffset(UnityEngine.Vector2&)");
	_il2cpp_icall_func(__this, ___value);
}
// System.Single UnityEngine.GUIStyle::get_fixedWidth()
extern "C" float GUIStyle_get_fixedWidth_m6_1342 (GUIStyle_t6_166 * __this, const MethodInfo* method)
{
	typedef float (*GUIStyle_get_fixedWidth_m6_1342_ftn) (GUIStyle_t6_166 *);
	static GUIStyle_get_fixedWidth_m6_1342_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GUIStyle_get_fixedWidth_m6_1342_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.GUIStyle::get_fixedWidth()");
	return _il2cpp_icall_func(__this);
}
// System.Single UnityEngine.GUIStyle::get_fixedHeight()
extern "C" float GUIStyle_get_fixedHeight_m6_1343 (GUIStyle_t6_166 * __this, const MethodInfo* method)
{
	typedef float (*GUIStyle_get_fixedHeight_m6_1343_ftn) (GUIStyle_t6_166 *);
	static GUIStyle_get_fixedHeight_m6_1343_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GUIStyle_get_fixedHeight_m6_1343_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.GUIStyle::get_fixedHeight()");
	return _il2cpp_icall_func(__this);
}
// System.Boolean UnityEngine.GUIStyle::get_stretchWidth()
extern "C" bool GUIStyle_get_stretchWidth_m6_1344 (GUIStyle_t6_166 * __this, const MethodInfo* method)
{
	typedef bool (*GUIStyle_get_stretchWidth_m6_1344_ftn) (GUIStyle_t6_166 *);
	static GUIStyle_get_stretchWidth_m6_1344_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GUIStyle_get_stretchWidth_m6_1344_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.GUIStyle::get_stretchWidth()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.GUIStyle::set_stretchWidth(System.Boolean)
extern "C" void GUIStyle_set_stretchWidth_m6_1345 (GUIStyle_t6_166 * __this, bool ___value, const MethodInfo* method)
{
	typedef void (*GUIStyle_set_stretchWidth_m6_1345_ftn) (GUIStyle_t6_166 *, bool);
	static GUIStyle_set_stretchWidth_m6_1345_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GUIStyle_set_stretchWidth_m6_1345_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.GUIStyle::set_stretchWidth(System.Boolean)");
	_il2cpp_icall_func(__this, ___value);
}
// System.Boolean UnityEngine.GUIStyle::get_stretchHeight()
extern "C" bool GUIStyle_get_stretchHeight_m6_1346 (GUIStyle_t6_166 * __this, const MethodInfo* method)
{
	typedef bool (*GUIStyle_get_stretchHeight_m6_1346_ftn) (GUIStyle_t6_166 *);
	static GUIStyle_get_stretchHeight_m6_1346_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GUIStyle_get_stretchHeight_m6_1346_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.GUIStyle::get_stretchHeight()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.GUIStyle::set_stretchHeight(System.Boolean)
extern "C" void GUIStyle_set_stretchHeight_m6_1347 (GUIStyle_t6_166 * __this, bool ___value, const MethodInfo* method)
{
	typedef void (*GUIStyle_set_stretchHeight_m6_1347_ftn) (GUIStyle_t6_166 *, bool);
	static GUIStyle_set_stretchHeight_m6_1347_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GUIStyle_set_stretchHeight_m6_1347_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.GUIStyle::set_stretchHeight(System.Boolean)");
	_il2cpp_icall_func(__this, ___value);
}
// System.Single UnityEngine.GUIStyle::Internal_GetLineHeight(System.IntPtr)
extern "C" float GUIStyle_Internal_GetLineHeight_m6_1348 (Object_t * __this /* static, unused */, IntPtr_t ___target, const MethodInfo* method)
{
	typedef float (*GUIStyle_Internal_GetLineHeight_m6_1348_ftn) (IntPtr_t);
	static GUIStyle_Internal_GetLineHeight_m6_1348_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GUIStyle_Internal_GetLineHeight_m6_1348_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.GUIStyle::Internal_GetLineHeight(System.IntPtr)");
	return _il2cpp_icall_func(___target);
}
// UnityEngine.Font UnityEngine.GUIStyle::GetFontInternal()
extern "C" Font_t6_148 * GUIStyle_GetFontInternal_m6_1349 (GUIStyle_t6_166 * __this, const MethodInfo* method)
{
	typedef Font_t6_148 * (*GUIStyle_GetFontInternal_m6_1349_ftn) (GUIStyle_t6_166 *);
	static GUIStyle_GetFontInternal_m6_1349_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GUIStyle_GetFontInternal_m6_1349_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.GUIStyle::GetFontInternal()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.GUIStyle::Internal_Draw(UnityEngine.GUIContent,UnityEngine.Internal_DrawArguments&)
extern "C" void GUIStyle_Internal_Draw_m6_1350 (Object_t * __this /* static, unused */, GUIContent_t6_163 * ___content, Internal_DrawArguments_t6_187 * ___arguments, const MethodInfo* method)
{
	typedef void (*GUIStyle_Internal_Draw_m6_1350_ftn) (GUIContent_t6_163 *, Internal_DrawArguments_t6_187 *);
	static GUIStyle_Internal_Draw_m6_1350_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GUIStyle_Internal_Draw_m6_1350_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.GUIStyle::Internal_Draw(UnityEngine.GUIContent,UnityEngine.Internal_DrawArguments&)");
	_il2cpp_icall_func(___content, ___arguments);
}
// System.Void UnityEngine.GUIStyle::Internal_Draw2(System.IntPtr,UnityEngine.Rect,UnityEngine.GUIContent,System.Int32,System.Boolean)
extern TypeInfo* GUIStyle_t6_166_il2cpp_TypeInfo_var;
extern "C" void GUIStyle_Internal_Draw2_m6_1351 (Object_t * __this /* static, unused */, IntPtr_t ___style, Rect_t6_52  ___position, GUIContent_t6_163 * ___content, int32_t ___controlID, bool ___on, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GUIStyle_t6_166_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(988);
		s_Il2CppMethodIntialized = true;
	}
	{
		IntPtr_t L_0 = ___style;
		GUIContent_t6_163 * L_1 = ___content;
		int32_t L_2 = ___controlID;
		bool L_3 = ___on;
		IL2CPP_RUNTIME_CLASS_INIT(GUIStyle_t6_166_il2cpp_TypeInfo_var);
		GUIStyle_INTERNAL_CALL_Internal_Draw2_m6_1352(NULL /*static, unused*/, L_0, (&___position), L_1, L_2, L_3, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.GUIStyle::INTERNAL_CALL_Internal_Draw2(System.IntPtr,UnityEngine.Rect&,UnityEngine.GUIContent,System.Int32,System.Boolean)
extern "C" void GUIStyle_INTERNAL_CALL_Internal_Draw2_m6_1352 (Object_t * __this /* static, unused */, IntPtr_t ___style, Rect_t6_52 * ___position, GUIContent_t6_163 * ___content, int32_t ___controlID, bool ___on, const MethodInfo* method)
{
	typedef void (*GUIStyle_INTERNAL_CALL_Internal_Draw2_m6_1352_ftn) (IntPtr_t, Rect_t6_52 *, GUIContent_t6_163 *, int32_t, bool);
	static GUIStyle_INTERNAL_CALL_Internal_Draw2_m6_1352_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GUIStyle_INTERNAL_CALL_Internal_Draw2_m6_1352_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.GUIStyle::INTERNAL_CALL_Internal_Draw2(System.IntPtr,UnityEngine.Rect&,UnityEngine.GUIContent,System.Int32,System.Boolean)");
	_il2cpp_icall_func(___style, ___position, ___content, ___controlID, ___on);
}
// System.Single UnityEngine.GUIStyle::Internal_GetCursorFlashOffset()
extern "C" float GUIStyle_Internal_GetCursorFlashOffset_m6_1353 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	typedef float (*GUIStyle_Internal_GetCursorFlashOffset_m6_1353_ftn) ();
	static GUIStyle_Internal_GetCursorFlashOffset_m6_1353_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GUIStyle_Internal_GetCursorFlashOffset_m6_1353_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.GUIStyle::Internal_GetCursorFlashOffset()");
	return _il2cpp_icall_func();
}
// System.Void UnityEngine.GUIStyle::Internal_DrawCursor(System.IntPtr,UnityEngine.Rect,UnityEngine.GUIContent,System.Int32,UnityEngine.Color)
extern TypeInfo* GUIStyle_t6_166_il2cpp_TypeInfo_var;
extern "C" void GUIStyle_Internal_DrawCursor_m6_1354 (Object_t * __this /* static, unused */, IntPtr_t ___target, Rect_t6_52  ___position, GUIContent_t6_163 * ___content, int32_t ___pos, Color_t6_40  ___cursorColor, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GUIStyle_t6_166_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(988);
		s_Il2CppMethodIntialized = true;
	}
	{
		IntPtr_t L_0 = ___target;
		GUIContent_t6_163 * L_1 = ___content;
		int32_t L_2 = ___pos;
		IL2CPP_RUNTIME_CLASS_INIT(GUIStyle_t6_166_il2cpp_TypeInfo_var);
		GUIStyle_INTERNAL_CALL_Internal_DrawCursor_m6_1355(NULL /*static, unused*/, L_0, (&___position), L_1, L_2, (&___cursorColor), /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.GUIStyle::INTERNAL_CALL_Internal_DrawCursor(System.IntPtr,UnityEngine.Rect&,UnityEngine.GUIContent,System.Int32,UnityEngine.Color&)
extern "C" void GUIStyle_INTERNAL_CALL_Internal_DrawCursor_m6_1355 (Object_t * __this /* static, unused */, IntPtr_t ___target, Rect_t6_52 * ___position, GUIContent_t6_163 * ___content, int32_t ___pos, Color_t6_40 * ___cursorColor, const MethodInfo* method)
{
	typedef void (*GUIStyle_INTERNAL_CALL_Internal_DrawCursor_m6_1355_ftn) (IntPtr_t, Rect_t6_52 *, GUIContent_t6_163 *, int32_t, Color_t6_40 *);
	static GUIStyle_INTERNAL_CALL_Internal_DrawCursor_m6_1355_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GUIStyle_INTERNAL_CALL_Internal_DrawCursor_m6_1355_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.GUIStyle::INTERNAL_CALL_Internal_DrawCursor(System.IntPtr,UnityEngine.Rect&,UnityEngine.GUIContent,System.Int32,UnityEngine.Color&)");
	_il2cpp_icall_func(___target, ___position, ___content, ___pos, ___cursorColor);
}
// System.Void UnityEngine.GUIStyle::Internal_DrawWithTextSelection(UnityEngine.GUIContent,UnityEngine.Internal_DrawWithTextSelectionArguments&)
extern "C" void GUIStyle_Internal_DrawWithTextSelection_m6_1356 (Object_t * __this /* static, unused */, GUIContent_t6_163 * ___content, Internal_DrawWithTextSelectionArguments_t6_188 * ___arguments, const MethodInfo* method)
{
	typedef void (*GUIStyle_Internal_DrawWithTextSelection_m6_1356_ftn) (GUIContent_t6_163 *, Internal_DrawWithTextSelectionArguments_t6_188 *);
	static GUIStyle_Internal_DrawWithTextSelection_m6_1356_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GUIStyle_Internal_DrawWithTextSelection_m6_1356_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.GUIStyle::Internal_DrawWithTextSelection(UnityEngine.GUIContent,UnityEngine.Internal_DrawWithTextSelectionArguments&)");
	_il2cpp_icall_func(___content, ___arguments);
}
// System.Void UnityEngine.GUIStyle::SetDefaultFont(UnityEngine.Font)
extern "C" void GUIStyle_SetDefaultFont_m6_1357 (Object_t * __this /* static, unused */, Font_t6_148 * ___font, const MethodInfo* method)
{
	typedef void (*GUIStyle_SetDefaultFont_m6_1357_ftn) (Font_t6_148 *);
	static GUIStyle_SetDefaultFont_m6_1357_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GUIStyle_SetDefaultFont_m6_1357_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.GUIStyle::SetDefaultFont(UnityEngine.Font)");
	_il2cpp_icall_func(___font);
}
// System.Void UnityEngine.GUIStyle::Internal_GetCursorPixelPosition(System.IntPtr,UnityEngine.Rect,UnityEngine.GUIContent,System.Int32,UnityEngine.Vector2&)
extern TypeInfo* GUIStyle_t6_166_il2cpp_TypeInfo_var;
extern "C" void GUIStyle_Internal_GetCursorPixelPosition_m6_1358 (Object_t * __this /* static, unused */, IntPtr_t ___target, Rect_t6_52  ___position, GUIContent_t6_163 * ___content, int32_t ___cursorStringIndex, Vector2_t6_48 * ___ret, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GUIStyle_t6_166_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(988);
		s_Il2CppMethodIntialized = true;
	}
	{
		IntPtr_t L_0 = ___target;
		GUIContent_t6_163 * L_1 = ___content;
		int32_t L_2 = ___cursorStringIndex;
		Vector2_t6_48 * L_3 = ___ret;
		IL2CPP_RUNTIME_CLASS_INIT(GUIStyle_t6_166_il2cpp_TypeInfo_var);
		GUIStyle_INTERNAL_CALL_Internal_GetCursorPixelPosition_m6_1359(NULL /*static, unused*/, L_0, (&___position), L_1, L_2, L_3, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.GUIStyle::INTERNAL_CALL_Internal_GetCursorPixelPosition(System.IntPtr,UnityEngine.Rect&,UnityEngine.GUIContent,System.Int32,UnityEngine.Vector2&)
extern "C" void GUIStyle_INTERNAL_CALL_Internal_GetCursorPixelPosition_m6_1359 (Object_t * __this /* static, unused */, IntPtr_t ___target, Rect_t6_52 * ___position, GUIContent_t6_163 * ___content, int32_t ___cursorStringIndex, Vector2_t6_48 * ___ret, const MethodInfo* method)
{
	typedef void (*GUIStyle_INTERNAL_CALL_Internal_GetCursorPixelPosition_m6_1359_ftn) (IntPtr_t, Rect_t6_52 *, GUIContent_t6_163 *, int32_t, Vector2_t6_48 *);
	static GUIStyle_INTERNAL_CALL_Internal_GetCursorPixelPosition_m6_1359_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GUIStyle_INTERNAL_CALL_Internal_GetCursorPixelPosition_m6_1359_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.GUIStyle::INTERNAL_CALL_Internal_GetCursorPixelPosition(System.IntPtr,UnityEngine.Rect&,UnityEngine.GUIContent,System.Int32,UnityEngine.Vector2&)");
	_il2cpp_icall_func(___target, ___position, ___content, ___cursorStringIndex, ___ret);
}
// System.Int32 UnityEngine.GUIStyle::Internal_GetCursorStringIndex(System.IntPtr,UnityEngine.Rect,UnityEngine.GUIContent,UnityEngine.Vector2)
extern TypeInfo* GUIStyle_t6_166_il2cpp_TypeInfo_var;
extern "C" int32_t GUIStyle_Internal_GetCursorStringIndex_m6_1360 (Object_t * __this /* static, unused */, IntPtr_t ___target, Rect_t6_52  ___position, GUIContent_t6_163 * ___content, Vector2_t6_48  ___cursorPixelPosition, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GUIStyle_t6_166_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(988);
		s_Il2CppMethodIntialized = true;
	}
	{
		IntPtr_t L_0 = ___target;
		GUIContent_t6_163 * L_1 = ___content;
		IL2CPP_RUNTIME_CLASS_INIT(GUIStyle_t6_166_il2cpp_TypeInfo_var);
		int32_t L_2 = GUIStyle_INTERNAL_CALL_Internal_GetCursorStringIndex_m6_1361(NULL /*static, unused*/, L_0, (&___position), L_1, (&___cursorPixelPosition), /*hidden argument*/NULL);
		return L_2;
	}
}
// System.Int32 UnityEngine.GUIStyle::INTERNAL_CALL_Internal_GetCursorStringIndex(System.IntPtr,UnityEngine.Rect&,UnityEngine.GUIContent,UnityEngine.Vector2&)
extern "C" int32_t GUIStyle_INTERNAL_CALL_Internal_GetCursorStringIndex_m6_1361 (Object_t * __this /* static, unused */, IntPtr_t ___target, Rect_t6_52 * ___position, GUIContent_t6_163 * ___content, Vector2_t6_48 * ___cursorPixelPosition, const MethodInfo* method)
{
	typedef int32_t (*GUIStyle_INTERNAL_CALL_Internal_GetCursorStringIndex_m6_1361_ftn) (IntPtr_t, Rect_t6_52 *, GUIContent_t6_163 *, Vector2_t6_48 *);
	static GUIStyle_INTERNAL_CALL_Internal_GetCursorStringIndex_m6_1361_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GUIStyle_INTERNAL_CALL_Internal_GetCursorStringIndex_m6_1361_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.GUIStyle::INTERNAL_CALL_Internal_GetCursorStringIndex(System.IntPtr,UnityEngine.Rect&,UnityEngine.GUIContent,UnityEngine.Vector2&)");
	return _il2cpp_icall_func(___target, ___position, ___content, ___cursorPixelPosition);
}
// System.Void UnityEngine.GUIStyle::Internal_CalcSize(System.IntPtr,UnityEngine.GUIContent,UnityEngine.Vector2&)
extern "C" void GUIStyle_Internal_CalcSize_m6_1362 (Object_t * __this /* static, unused */, IntPtr_t ___target, GUIContent_t6_163 * ___content, Vector2_t6_48 * ___ret, const MethodInfo* method)
{
	typedef void (*GUIStyle_Internal_CalcSize_m6_1362_ftn) (IntPtr_t, GUIContent_t6_163 *, Vector2_t6_48 *);
	static GUIStyle_Internal_CalcSize_m6_1362_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GUIStyle_Internal_CalcSize_m6_1362_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.GUIStyle::Internal_CalcSize(System.IntPtr,UnityEngine.GUIContent,UnityEngine.Vector2&)");
	_il2cpp_icall_func(___target, ___content, ___ret);
}
// System.Single UnityEngine.GUIStyle::Internal_CalcHeight(System.IntPtr,UnityEngine.GUIContent,System.Single)
extern "C" float GUIStyle_Internal_CalcHeight_m6_1363 (Object_t * __this /* static, unused */, IntPtr_t ___target, GUIContent_t6_163 * ___content, float ___width, const MethodInfo* method)
{
	typedef float (*GUIStyle_Internal_CalcHeight_m6_1363_ftn) (IntPtr_t, GUIContent_t6_163 *, float);
	static GUIStyle_Internal_CalcHeight_m6_1363_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GUIStyle_Internal_CalcHeight_m6_1363_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.GUIStyle::Internal_CalcHeight(System.IntPtr,UnityEngine.GUIContent,System.Single)");
	return _il2cpp_icall_func(___target, ___content, ___width);
}
// System.Void UnityEngine.GUIStyle::Internal_CalcMinMaxWidth(System.IntPtr,UnityEngine.GUIContent,System.Single&,System.Single&)
extern "C" void GUIStyle_Internal_CalcMinMaxWidth_m6_1364 (Object_t * __this /* static, unused */, IntPtr_t ___target, GUIContent_t6_163 * ___content, float* ___minWidth, float* ___maxWidth, const MethodInfo* method)
{
	typedef void (*GUIStyle_Internal_CalcMinMaxWidth_m6_1364_ftn) (IntPtr_t, GUIContent_t6_163 *, float*, float*);
	static GUIStyle_Internal_CalcMinMaxWidth_m6_1364_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GUIStyle_Internal_CalcMinMaxWidth_m6_1364_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.GUIStyle::Internal_CalcMinMaxWidth(System.IntPtr,UnityEngine.GUIContent,System.Single&,System.Single&)");
	_il2cpp_icall_func(___target, ___content, ___minWidth, ___maxWidth);
}
// System.Void UnityEngine.GUIUtility::.cctor()
extern TypeInfo* GUIUtility_t6_185_il2cpp_TypeInfo_var;
extern "C" void GUIUtility__cctor_m6_1365 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GUIUtility_t6_185_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(984);
		s_Il2CppMethodIntialized = true;
	}
	{
		Vector2_t6_48  L_0 = Vector2_get_zero_m6_182(NULL /*static, unused*/, /*hidden argument*/NULL);
		((GUIUtility_t6_185_StaticFields*)GUIUtility_t6_185_il2cpp_TypeInfo_var->static_fields)->___s_EditorScreenPointOffset_2 = L_0;
		((GUIUtility_t6_185_StaticFields*)GUIUtility_t6_185_il2cpp_TypeInfo_var->static_fields)->___s_HasKeyboardFocus_3 = 0;
		return;
	}
}
// System.Single UnityEngine.GUIUtility::get_pixelsPerPoint()
extern TypeInfo* GUIUtility_t6_185_il2cpp_TypeInfo_var;
extern "C" float GUIUtility_get_pixelsPerPoint_m6_1366 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GUIUtility_t6_185_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(984);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GUIUtility_t6_185_il2cpp_TypeInfo_var);
		float L_0 = GUIUtility_Internal_GetPixelsPerPoint_m6_1377(NULL /*static, unused*/, /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Int32 UnityEngine.GUIUtility::GetControlID(UnityEngine.FocusType)
extern TypeInfo* GUIUtility_t6_185_il2cpp_TypeInfo_var;
extern "C" int32_t GUIUtility_GetControlID_m6_1367 (Object_t * __this /* static, unused */, int32_t ___focus, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GUIUtility_t6_185_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(984);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = ___focus;
		IL2CPP_RUNTIME_CLASS_INIT(GUIUtility_t6_185_il2cpp_TypeInfo_var);
		int32_t L_1 = GUIUtility_GetControlID_m6_1378(NULL /*static, unused*/, 0, L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Int32 UnityEngine.GUIUtility::GetControlID(System.Int32,UnityEngine.FocusType,UnityEngine.Rect)
extern TypeInfo* GUIUtility_t6_185_il2cpp_TypeInfo_var;
extern "C" int32_t GUIUtility_GetControlID_m6_1368 (Object_t * __this /* static, unused */, int32_t ___hint, int32_t ___focus, Rect_t6_52  ___position, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GUIUtility_t6_185_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(984);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = ___hint;
		int32_t L_1 = ___focus;
		Rect_t6_52  L_2 = ___position;
		IL2CPP_RUNTIME_CLASS_INIT(GUIUtility_t6_185_il2cpp_TypeInfo_var);
		int32_t L_3 = GUIUtility_Internal_GetNextControlID2_m6_1379(NULL /*static, unused*/, L_0, L_1, L_2, /*hidden argument*/NULL);
		return L_3;
	}
}
// System.Object UnityEngine.GUIUtility::GetStateObject(System.Type,System.Int32)
extern TypeInfo* GUIStateObjects_t6_212_il2cpp_TypeInfo_var;
extern "C" Object_t * GUIUtility_GetStateObject_m6_1369 (Object_t * __this /* static, unused */, Type_t * ___t, int32_t ___controlID, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GUIStateObjects_t6_212_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1013);
		s_Il2CppMethodIntialized = true;
	}
	{
		Type_t * L_0 = ___t;
		int32_t L_1 = ___controlID;
		IL2CPP_RUNTIME_CLASS_INIT(GUIStateObjects_t6_212_il2cpp_TypeInfo_var);
		Object_t * L_2 = GUIStateObjects_GetStateObject_m6_1424(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// System.Int32 UnityEngine.GUIUtility::get_hotControl()
extern TypeInfo* GUIUtility_t6_185_il2cpp_TypeInfo_var;
extern "C" int32_t GUIUtility_get_hotControl_m6_1370 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GUIUtility_t6_185_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(984);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GUIUtility_t6_185_il2cpp_TypeInfo_var);
		int32_t L_0 = GUIUtility_Internal_GetHotControl_m6_1381(NULL /*static, unused*/, /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Void UnityEngine.GUIUtility::set_hotControl(System.Int32)
extern TypeInfo* GUIUtility_t6_185_il2cpp_TypeInfo_var;
extern "C" void GUIUtility_set_hotControl_m6_1371 (Object_t * __this /* static, unused */, int32_t ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GUIUtility_t6_185_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(984);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = ___value;
		IL2CPP_RUNTIME_CLASS_INIT(GUIUtility_t6_185_il2cpp_TypeInfo_var);
		GUIUtility_Internal_SetHotControl_m6_1382(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.GUISkin UnityEngine.GUIUtility::GetDefaultSkin()
extern TypeInfo* GUIUtility_t6_185_il2cpp_TypeInfo_var;
extern "C" GUISkin_t6_161 * GUIUtility_GetDefaultSkin_m6_1372 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GUIUtility_t6_185_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(984);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GUIUtility_t6_185_il2cpp_TypeInfo_var);
		int32_t L_0 = ((GUIUtility_t6_185_StaticFields*)GUIUtility_t6_185_il2cpp_TypeInfo_var->static_fields)->___s_SkinMode_0;
		GUISkin_t6_161 * L_1 = GUIUtility_Internal_GetDefaultSkin_m6_1387(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Void UnityEngine.GUIUtility::BeginGUI(System.Int32,System.Int32,System.Int32)
extern TypeInfo* GUIUtility_t6_185_il2cpp_TypeInfo_var;
extern TypeInfo* GUI_t6_160_il2cpp_TypeInfo_var;
extern TypeInfo* GUILayoutUtility_t6_170_il2cpp_TypeInfo_var;
extern "C" void GUIUtility_BeginGUI_m6_1373 (Object_t * __this /* static, unused */, int32_t ___skinMode, int32_t ___instanceID, int32_t ___useGUILayout, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GUIUtility_t6_185_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(984);
		GUI_t6_160_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(982);
		GUILayoutUtility_t6_170_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(990);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = ___skinMode;
		IL2CPP_RUNTIME_CLASS_INIT(GUIUtility_t6_185_il2cpp_TypeInfo_var);
		((GUIUtility_t6_185_StaticFields*)GUIUtility_t6_185_il2cpp_TypeInfo_var->static_fields)->___s_SkinMode_0 = L_0;
		int32_t L_1 = ___instanceID;
		((GUIUtility_t6_185_StaticFields*)GUIUtility_t6_185_il2cpp_TypeInfo_var->static_fields)->___s_OriginalID_1 = L_1;
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t6_160_il2cpp_TypeInfo_var);
		GUI_set_skin_m6_1040(NULL /*static, unused*/, (GUISkin_t6_161 *)NULL, /*hidden argument*/NULL);
		int32_t L_2 = ___useGUILayout;
		if (!L_2)
		{
			goto IL_0026;
		}
	}
	{
		int32_t L_3 = ___instanceID;
		IL2CPP_RUNTIME_CLASS_INIT(GUILayoutUtility_t6_170_il2cpp_TypeInfo_var);
		GUILayoutUtility_SelectIDList_m6_1150(NULL /*static, unused*/, L_3, 0, /*hidden argument*/NULL);
		int32_t L_4 = ___instanceID;
		GUILayoutUtility_Begin_m6_1151(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
	}

IL_0026:
	{
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t6_160_il2cpp_TypeInfo_var);
		GUI_set_changed_m6_1081(NULL /*static, unused*/, 0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.GUIUtility::EndGUI(System.Int32)
extern TypeInfo* GUILayoutUtility_t6_170_il2cpp_TypeInfo_var;
extern TypeInfo* GUIUtility_t6_185_il2cpp_TypeInfo_var;
extern TypeInfo* GUIContent_t6_163_il2cpp_TypeInfo_var;
extern "C" void GUIUtility_EndGUI_m6_1374 (Object_t * __this /* static, unused */, int32_t ___layoutType, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GUILayoutUtility_t6_170_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(990);
		GUIUtility_t6_185_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(984);
		GUIContent_t6_163_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(985);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	Exception_t1_33 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1_33 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		{
			Event_t6_154 * L_0 = Event_get_current_m6_989(NULL /*static, unused*/, /*hidden argument*/NULL);
			NullCheck(L_0);
			int32_t L_1 = Event_get_type_m6_1003(L_0, /*hidden argument*/NULL);
			if ((!(((uint32_t)L_1) == ((uint32_t)8))))
			{
				goto IL_0042;
			}
		}

IL_0010:
		{
			int32_t L_2 = ___layoutType;
			V_0 = L_2;
			int32_t L_3 = V_0;
			if (L_3 == 0)
			{
				goto IL_0029;
			}
			if (L_3 == 1)
			{
				goto IL_002e;
			}
			if (L_3 == 2)
			{
				goto IL_0038;
			}
		}

IL_0024:
		{
			goto IL_0042;
		}

IL_0029:
		{
			goto IL_0042;
		}

IL_002e:
		{
			IL2CPP_RUNTIME_CLASS_INIT(GUILayoutUtility_t6_170_il2cpp_TypeInfo_var);
			GUILayoutUtility_Layout_m6_1154(NULL /*static, unused*/, /*hidden argument*/NULL);
			goto IL_0042;
		}

IL_0038:
		{
			IL2CPP_RUNTIME_CLASS_INIT(GUILayoutUtility_t6_170_il2cpp_TypeInfo_var);
			GUILayoutUtility_LayoutFromEditorWindow_m6_1155(NULL /*static, unused*/, /*hidden argument*/NULL);
			goto IL_0042;
		}

IL_0042:
		{
			IL2CPP_RUNTIME_CLASS_INIT(GUIUtility_t6_185_il2cpp_TypeInfo_var);
			int32_t L_4 = ((GUIUtility_t6_185_StaticFields*)GUIUtility_t6_185_il2cpp_TypeInfo_var->static_fields)->___s_OriginalID_1;
			IL2CPP_RUNTIME_CLASS_INIT(GUILayoutUtility_t6_170_il2cpp_TypeInfo_var);
			GUILayoutUtility_SelectIDList_m6_1150(NULL /*static, unused*/, L_4, 0, /*hidden argument*/NULL);
			IL2CPP_RUNTIME_CLASS_INIT(GUIContent_t6_163_il2cpp_TypeInfo_var);
			GUIContent_ClearStaticCache_m6_1108(NULL /*static, unused*/, /*hidden argument*/NULL);
			IL2CPP_LEAVE(0x5E, FINALLY_0058);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1_33 *)e.ex;
		goto FINALLY_0058;
	}

FINALLY_0058:
	{ // begin finally (depth: 1)
		IL2CPP_RUNTIME_CLASS_INIT(GUIUtility_t6_185_il2cpp_TypeInfo_var);
		GUIUtility_Internal_ExitGUI_m6_1388(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(88)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(88)
	{
		IL2CPP_JUMP_TBL(0x5E, IL_005e)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1_33 *)
	}

IL_005e:
	{
		return;
	}
}
// System.Boolean UnityEngine.GUIUtility::EndGUIFromException(System.Exception)
extern TypeInfo* ExitGUIException_t6_183_il2cpp_TypeInfo_var;
extern TypeInfo* GUIUtility_t6_185_il2cpp_TypeInfo_var;
extern "C" bool GUIUtility_EndGUIFromException_m6_1375 (Object_t * __this /* static, unused */, Exception_t1_33 * ___exception, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ExitGUIException_t6_183_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1014);
		GUIUtility_t6_185_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(984);
		s_Il2CppMethodIntialized = true;
	}
	{
		Exception_t1_33 * L_0 = ___exception;
		if (L_0)
		{
			goto IL_0008;
		}
	}
	{
		return 0;
	}

IL_0008:
	{
		Exception_t1_33 * L_1 = ___exception;
		if (((ExitGUIException_t6_183 *)IsInstSealed(L_1, ExitGUIException_t6_183_il2cpp_TypeInfo_var)))
		{
			goto IL_0025;
		}
	}
	{
		Exception_t1_33 * L_2 = ___exception;
		NullCheck(L_2);
		Exception_t1_33 * L_3 = (Exception_t1_33 *)VirtFuncInvoker0< Exception_t1_33 * >::Invoke(5 /* System.Exception System.Exception::get_InnerException() */, L_2);
		if (((ExitGUIException_t6_183 *)IsInstSealed(L_3, ExitGUIException_t6_183_il2cpp_TypeInfo_var)))
		{
			goto IL_0025;
		}
	}
	{
		return 0;
	}

IL_0025:
	{
		IL2CPP_RUNTIME_CLASS_INIT(GUIUtility_t6_185_il2cpp_TypeInfo_var);
		GUIUtility_Internal_ExitGUI_m6_1388(NULL /*static, unused*/, /*hidden argument*/NULL);
		return 1;
	}
}
// System.Void UnityEngine.GUIUtility::CheckOnGUI()
extern TypeInfo* GUIUtility_t6_185_il2cpp_TypeInfo_var;
extern TypeInfo* ArgumentException_t1_646_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2830;
extern "C" void GUIUtility_CheckOnGUI_m6_1376 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GUIUtility_t6_185_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(984);
		ArgumentException_t1_646_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4);
		_stringLiteral2830 = il2cpp_codegen_string_literal_from_index(2830);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GUIUtility_t6_185_il2cpp_TypeInfo_var);
		int32_t L_0 = GUIUtility_Internal_GetGUIDepth_m6_1389(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((((int32_t)L_0) > ((int32_t)0)))
		{
			goto IL_0016;
		}
	}
	{
		ArgumentException_t1_646 * L_1 = (ArgumentException_t1_646 *)il2cpp_codegen_object_new (ArgumentException_t1_646_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m1_4605(L_1, _stringLiteral2830, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_1);
	}

IL_0016:
	{
		return;
	}
}
// System.Single UnityEngine.GUIUtility::Internal_GetPixelsPerPoint()
extern "C" float GUIUtility_Internal_GetPixelsPerPoint_m6_1377 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	typedef float (*GUIUtility_Internal_GetPixelsPerPoint_m6_1377_ftn) ();
	static GUIUtility_Internal_GetPixelsPerPoint_m6_1377_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GUIUtility_Internal_GetPixelsPerPoint_m6_1377_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.GUIUtility::Internal_GetPixelsPerPoint()");
	return _il2cpp_icall_func();
}
// System.Int32 UnityEngine.GUIUtility::GetControlID(System.Int32,UnityEngine.FocusType)
extern "C" int32_t GUIUtility_GetControlID_m6_1378 (Object_t * __this /* static, unused */, int32_t ___hint, int32_t ___focus, const MethodInfo* method)
{
	typedef int32_t (*GUIUtility_GetControlID_m6_1378_ftn) (int32_t, int32_t);
	static GUIUtility_GetControlID_m6_1378_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GUIUtility_GetControlID_m6_1378_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.GUIUtility::GetControlID(System.Int32,UnityEngine.FocusType)");
	return _il2cpp_icall_func(___hint, ___focus);
}
// System.Int32 UnityEngine.GUIUtility::Internal_GetNextControlID2(System.Int32,UnityEngine.FocusType,UnityEngine.Rect)
extern TypeInfo* GUIUtility_t6_185_il2cpp_TypeInfo_var;
extern "C" int32_t GUIUtility_Internal_GetNextControlID2_m6_1379 (Object_t * __this /* static, unused */, int32_t ___hint, int32_t ___focusType, Rect_t6_52  ___rect, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GUIUtility_t6_185_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(984);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = ___hint;
		int32_t L_1 = ___focusType;
		IL2CPP_RUNTIME_CLASS_INIT(GUIUtility_t6_185_il2cpp_TypeInfo_var);
		int32_t L_2 = GUIUtility_INTERNAL_CALL_Internal_GetNextControlID2_m6_1380(NULL /*static, unused*/, L_0, L_1, (&___rect), /*hidden argument*/NULL);
		return L_2;
	}
}
// System.Int32 UnityEngine.GUIUtility::INTERNAL_CALL_Internal_GetNextControlID2(System.Int32,UnityEngine.FocusType,UnityEngine.Rect&)
extern "C" int32_t GUIUtility_INTERNAL_CALL_Internal_GetNextControlID2_m6_1380 (Object_t * __this /* static, unused */, int32_t ___hint, int32_t ___focusType, Rect_t6_52 * ___rect, const MethodInfo* method)
{
	typedef int32_t (*GUIUtility_INTERNAL_CALL_Internal_GetNextControlID2_m6_1380_ftn) (int32_t, int32_t, Rect_t6_52 *);
	static GUIUtility_INTERNAL_CALL_Internal_GetNextControlID2_m6_1380_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GUIUtility_INTERNAL_CALL_Internal_GetNextControlID2_m6_1380_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.GUIUtility::INTERNAL_CALL_Internal_GetNextControlID2(System.Int32,UnityEngine.FocusType,UnityEngine.Rect&)");
	return _il2cpp_icall_func(___hint, ___focusType, ___rect);
}
// System.Int32 UnityEngine.GUIUtility::Internal_GetHotControl()
extern "C" int32_t GUIUtility_Internal_GetHotControl_m6_1381 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	typedef int32_t (*GUIUtility_Internal_GetHotControl_m6_1381_ftn) ();
	static GUIUtility_Internal_GetHotControl_m6_1381_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GUIUtility_Internal_GetHotControl_m6_1381_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.GUIUtility::Internal_GetHotControl()");
	return _il2cpp_icall_func();
}
// System.Void UnityEngine.GUIUtility::Internal_SetHotControl(System.Int32)
extern "C" void GUIUtility_Internal_SetHotControl_m6_1382 (Object_t * __this /* static, unused */, int32_t ___value, const MethodInfo* method)
{
	typedef void (*GUIUtility_Internal_SetHotControl_m6_1382_ftn) (int32_t);
	static GUIUtility_Internal_SetHotControl_m6_1382_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GUIUtility_Internal_SetHotControl_m6_1382_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.GUIUtility::Internal_SetHotControl(System.Int32)");
	_il2cpp_icall_func(___value);
}
// System.Int32 UnityEngine.GUIUtility::get_keyboardControl()
extern "C" int32_t GUIUtility_get_keyboardControl_m6_1383 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	typedef int32_t (*GUIUtility_get_keyboardControl_m6_1383_ftn) ();
	static GUIUtility_get_keyboardControl_m6_1383_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GUIUtility_get_keyboardControl_m6_1383_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.GUIUtility::get_keyboardControl()");
	return _il2cpp_icall_func();
}
// System.Void UnityEngine.GUIUtility::set_keyboardControl(System.Int32)
extern "C" void GUIUtility_set_keyboardControl_m6_1384 (Object_t * __this /* static, unused */, int32_t ___value, const MethodInfo* method)
{
	typedef void (*GUIUtility_set_keyboardControl_m6_1384_ftn) (int32_t);
	static GUIUtility_set_keyboardControl_m6_1384_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GUIUtility_set_keyboardControl_m6_1384_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.GUIUtility::set_keyboardControl(System.Int32)");
	_il2cpp_icall_func(___value);
}
// System.String UnityEngine.GUIUtility::get_systemCopyBuffer()
extern "C" String_t* GUIUtility_get_systemCopyBuffer_m6_1385 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	typedef String_t* (*GUIUtility_get_systemCopyBuffer_m6_1385_ftn) ();
	static GUIUtility_get_systemCopyBuffer_m6_1385_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GUIUtility_get_systemCopyBuffer_m6_1385_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.GUIUtility::get_systemCopyBuffer()");
	return _il2cpp_icall_func();
}
// System.Void UnityEngine.GUIUtility::set_systemCopyBuffer(System.String)
extern "C" void GUIUtility_set_systemCopyBuffer_m6_1386 (Object_t * __this /* static, unused */, String_t* ___value, const MethodInfo* method)
{
	typedef void (*GUIUtility_set_systemCopyBuffer_m6_1386_ftn) (String_t*);
	static GUIUtility_set_systemCopyBuffer_m6_1386_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GUIUtility_set_systemCopyBuffer_m6_1386_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.GUIUtility::set_systemCopyBuffer(System.String)");
	_il2cpp_icall_func(___value);
}
// UnityEngine.GUISkin UnityEngine.GUIUtility::Internal_GetDefaultSkin(System.Int32)
extern "C" GUISkin_t6_161 * GUIUtility_Internal_GetDefaultSkin_m6_1387 (Object_t * __this /* static, unused */, int32_t ___skinMode, const MethodInfo* method)
{
	typedef GUISkin_t6_161 * (*GUIUtility_Internal_GetDefaultSkin_m6_1387_ftn) (int32_t);
	static GUIUtility_Internal_GetDefaultSkin_m6_1387_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GUIUtility_Internal_GetDefaultSkin_m6_1387_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.GUIUtility::Internal_GetDefaultSkin(System.Int32)");
	return _il2cpp_icall_func(___skinMode);
}
// System.Void UnityEngine.GUIUtility::Internal_ExitGUI()
extern "C" void GUIUtility_Internal_ExitGUI_m6_1388 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	typedef void (*GUIUtility_Internal_ExitGUI_m6_1388_ftn) ();
	static GUIUtility_Internal_ExitGUI_m6_1388_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GUIUtility_Internal_ExitGUI_m6_1388_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.GUIUtility::Internal_ExitGUI()");
	_il2cpp_icall_func();
}
// System.Int32 UnityEngine.GUIUtility::Internal_GetGUIDepth()
extern "C" int32_t GUIUtility_Internal_GetGUIDepth_m6_1389 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	typedef int32_t (*GUIUtility_Internal_GetGUIDepth_m6_1389_ftn) ();
	static GUIUtility_Internal_GetGUIDepth_m6_1389_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GUIUtility_Internal_GetGUIDepth_m6_1389_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.GUIUtility::Internal_GetGUIDepth()");
	return _il2cpp_icall_func();
}
// System.Boolean UnityEngine.GUIUtility::get_mouseUsed()
extern "C" bool GUIUtility_get_mouseUsed_m6_1390 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	typedef bool (*GUIUtility_get_mouseUsed_m6_1390_ftn) ();
	static GUIUtility_get_mouseUsed_m6_1390_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GUIUtility_get_mouseUsed_m6_1390_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.GUIUtility::get_mouseUsed()");
	return _il2cpp_icall_func();
}
// System.Void UnityEngine.GUIUtility::set_mouseUsed(System.Boolean)
extern "C" void GUIUtility_set_mouseUsed_m6_1391 (Object_t * __this /* static, unused */, bool ___value, const MethodInfo* method)
{
	typedef void (*GUIUtility_set_mouseUsed_m6_1391_ftn) (bool);
	static GUIUtility_set_mouseUsed_m6_1391_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GUIUtility_set_mouseUsed_m6_1391_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.GUIUtility::set_mouseUsed(System.Boolean)");
	_il2cpp_icall_func(___value);
}
// System.Void UnityEngine.GUIUtility::set_textFieldInput(System.Boolean)
extern "C" void GUIUtility_set_textFieldInput_m6_1392 (Object_t * __this /* static, unused */, bool ___value, const MethodInfo* method)
{
	typedef void (*GUIUtility_set_textFieldInput_m6_1392_ftn) (bool);
	static GUIUtility_set_textFieldInput_m6_1392_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GUIUtility_set_textFieldInput_m6_1392_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.GUIUtility::set_textFieldInput(System.Boolean)");
	_il2cpp_icall_func(___value);
}
// System.Void UnityEngine.GUIClip::Push(UnityEngine.Rect,UnityEngine.Vector2,UnityEngine.Vector2,System.Boolean)
extern "C" void GUIClip_Push_m6_1393 (Object_t * __this /* static, unused */, Rect_t6_52  ___screenRect, Vector2_t6_48  ___scrollOffset, Vector2_t6_48  ___renderOffset, bool ___resetOffset, const MethodInfo* method)
{
	{
		bool L_0 = ___resetOffset;
		GUIClip_INTERNAL_CALL_Push_m6_1394(NULL /*static, unused*/, (&___screenRect), (&___scrollOffset), (&___renderOffset), L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.GUIClip::INTERNAL_CALL_Push(UnityEngine.Rect&,UnityEngine.Vector2&,UnityEngine.Vector2&,System.Boolean)
extern "C" void GUIClip_INTERNAL_CALL_Push_m6_1394 (Object_t * __this /* static, unused */, Rect_t6_52 * ___screenRect, Vector2_t6_48 * ___scrollOffset, Vector2_t6_48 * ___renderOffset, bool ___resetOffset, const MethodInfo* method)
{
	typedef void (*GUIClip_INTERNAL_CALL_Push_m6_1394_ftn) (Rect_t6_52 *, Vector2_t6_48 *, Vector2_t6_48 *, bool);
	static GUIClip_INTERNAL_CALL_Push_m6_1394_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GUIClip_INTERNAL_CALL_Push_m6_1394_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.GUIClip::INTERNAL_CALL_Push(UnityEngine.Rect&,UnityEngine.Vector2&,UnityEngine.Vector2&,System.Boolean)");
	_il2cpp_icall_func(___screenRect, ___scrollOffset, ___renderOffset, ___resetOffset);
}
// System.Void UnityEngine.GUIClip::Pop()
extern "C" void GUIClip_Pop_m6_1395 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	typedef void (*GUIClip_Pop_m6_1395_ftn) ();
	static GUIClip_Pop_m6_1395_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GUIClip_Pop_m6_1395_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.GUIClip::Pop()");
	_il2cpp_icall_func();
}
// System.Void UnityEngine.WrapperlessIcall::.ctor()
extern "C" void WrapperlessIcall__ctor_m6_1396 (WrapperlessIcall_t6_189 * __this, const MethodInfo* method)
{
	{
		Attribute__ctor_m1_17(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.IL2CPPStructAlignmentAttribute::.ctor()
extern "C" void IL2CPPStructAlignmentAttribute__ctor_m6_1397 (IL2CPPStructAlignmentAttribute_t6_190 * __this, const MethodInfo* method)
{
	{
		Attribute__ctor_m1_17(__this, /*hidden argument*/NULL);
		__this->___Align_0 = 1;
		return;
	}
}
// System.Void UnityEngine.AttributeHelperEngine::.cctor()
extern TypeInfo* DisallowMultipleComponentU5BU5D_t6_192_il2cpp_TypeInfo_var;
extern TypeInfo* AttributeHelperEngine_t6_191_il2cpp_TypeInfo_var;
extern TypeInfo* ExecuteInEditModeU5BU5D_t6_193_il2cpp_TypeInfo_var;
extern TypeInfo* RequireComponentU5BU5D_t6_194_il2cpp_TypeInfo_var;
extern "C" void AttributeHelperEngine__cctor_m6_1398 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DisallowMultipleComponentU5BU5D_t6_192_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1015);
		AttributeHelperEngine_t6_191_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1017);
		ExecuteInEditModeU5BU5D_t6_193_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1018);
		RequireComponentU5BU5D_t6_194_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1020);
		s_Il2CppMethodIntialized = true;
	}
	{
		((AttributeHelperEngine_t6_191_StaticFields*)AttributeHelperEngine_t6_191_il2cpp_TypeInfo_var->static_fields)->____disallowMultipleComponentArray_0 = ((DisallowMultipleComponentU5BU5D_t6_192*)SZArrayNew(DisallowMultipleComponentU5BU5D_t6_192_il2cpp_TypeInfo_var, 1));
		((AttributeHelperEngine_t6_191_StaticFields*)AttributeHelperEngine_t6_191_il2cpp_TypeInfo_var->static_fields)->____executeInEditModeArray_1 = ((ExecuteInEditModeU5BU5D_t6_193*)SZArrayNew(ExecuteInEditModeU5BU5D_t6_193_il2cpp_TypeInfo_var, 1));
		((AttributeHelperEngine_t6_191_StaticFields*)AttributeHelperEngine_t6_191_il2cpp_TypeInfo_var->static_fields)->____requireComponentArray_2 = ((RequireComponentU5BU5D_t6_194*)SZArrayNew(RequireComponentU5BU5D_t6_194_il2cpp_TypeInfo_var, 1));
		return;
	}
}
// System.Type UnityEngine.AttributeHelperEngine::GetParentTypeDisallowingMultipleInclusion(System.Type)
extern const Il2CppType* MonoBehaviour_t6_80_0_0_0_var;
extern const Il2CppType* DisallowMultipleComponent_t6_195_0_0_0_var;
extern TypeInfo* Stack_1_t3_189_il2cpp_TypeInfo_var;
extern TypeInfo* Type_t_il2cpp_TypeInfo_var;
extern const MethodInfo* Stack_1__ctor_m3_1051_MethodInfo_var;
extern const MethodInfo* Stack_1_Push_m3_1052_MethodInfo_var;
extern const MethodInfo* Stack_1_Pop_m3_1053_MethodInfo_var;
extern "C" Type_t * AttributeHelperEngine_GetParentTypeDisallowingMultipleInclusion_m6_1399 (Object_t * __this /* static, unused */, Type_t * ___type, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		MonoBehaviour_t6_80_0_0_0_var = il2cpp_codegen_type_from_index(1022);
		DisallowMultipleComponent_t6_195_0_0_0_var = il2cpp_codegen_type_from_index(1016);
		Stack_1_t3_189_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1023);
		Type_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3);
		Stack_1__ctor_m3_1051_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483819);
		Stack_1_Push_m3_1052_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483820);
		Stack_1_Pop_m3_1053_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483821);
		s_Il2CppMethodIntialized = true;
	}
	Stack_1_t3_189 * V_0 = {0};
	Type_t * V_1 = {0};
	ObjectU5BU5D_t1_157* V_2 = {0};
	int32_t V_3 = 0;
	{
		Stack_1_t3_189 * L_0 = (Stack_1_t3_189 *)il2cpp_codegen_object_new (Stack_1_t3_189_il2cpp_TypeInfo_var);
		Stack_1__ctor_m3_1051(L_0, /*hidden argument*/Stack_1__ctor_m3_1051_MethodInfo_var);
		V_0 = L_0;
		goto IL_001a;
	}

IL_000b:
	{
		Stack_1_t3_189 * L_1 = V_0;
		Type_t * L_2 = ___type;
		NullCheck(L_1);
		Stack_1_Push_m3_1052(L_1, L_2, /*hidden argument*/Stack_1_Push_m3_1052_MethodInfo_var);
		Type_t * L_3 = ___type;
		NullCheck(L_3);
		Type_t * L_4 = (Type_t *)VirtFuncInvoker0< Type_t * >::Invoke(17 /* System.Type System.Type::get_BaseType() */, L_3);
		___type = L_4;
	}

IL_001a:
	{
		Type_t * L_5 = ___type;
		if (!L_5)
		{
			goto IL_0030;
		}
	}
	{
		Type_t * L_6 = ___type;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_7 = Type_GetTypeFromHandle_m1_893(NULL /*static, unused*/, LoadTypeToken(MonoBehaviour_t6_80_0_0_0_var), /*hidden argument*/NULL);
		if ((!(((Object_t*)(Type_t *)L_6) == ((Object_t*)(Type_t *)L_7))))
		{
			goto IL_000b;
		}
	}

IL_0030:
	{
		V_1 = (Type_t *)NULL;
		goto IL_005c;
	}

IL_0037:
	{
		Stack_1_t3_189 * L_8 = V_0;
		NullCheck(L_8);
		Type_t * L_9 = Stack_1_Pop_m3_1053(L_8, /*hidden argument*/Stack_1_Pop_m3_1053_MethodInfo_var);
		V_1 = L_9;
		Type_t * L_10 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_11 = Type_GetTypeFromHandle_m1_893(NULL /*static, unused*/, LoadTypeToken(DisallowMultipleComponent_t6_195_0_0_0_var), /*hidden argument*/NULL);
		NullCheck(L_10);
		ObjectU5BU5D_t1_157* L_12 = (ObjectU5BU5D_t1_157*)VirtFuncInvoker2< ObjectU5BU5D_t1_157*, Type_t *, bool >::Invoke(13 /* System.Object[] System.Reflection.MemberInfo::GetCustomAttributes(System.Type,System.Boolean) */, L_10, L_11, 0);
		V_2 = L_12;
		ObjectU5BU5D_t1_157* L_13 = V_2;
		NullCheck(L_13);
		V_3 = (((int32_t)((int32_t)(((Array_t *)L_13)->max_length))));
		int32_t L_14 = V_3;
		if (!L_14)
		{
			goto IL_005c;
		}
	}
	{
		Type_t * L_15 = V_1;
		return L_15;
	}

IL_005c:
	{
		Stack_1_t3_189 * L_16 = V_0;
		NullCheck(L_16);
		int32_t L_17 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(4 /* System.Int32 System.Collections.Generic.Stack`1<System.Type>::get_Count() */, L_16);
		if ((((int32_t)L_17) > ((int32_t)0)))
		{
			goto IL_0037;
		}
	}
	{
		return (Type_t *)NULL;
	}
}
// System.Type[] UnityEngine.AttributeHelperEngine::GetRequiredComponents(System.Type)
extern const Il2CppType* RequireComponent_t6_196_0_0_0_var;
extern const Il2CppType* MonoBehaviour_t6_80_0_0_0_var;
extern TypeInfo* Type_t_il2cpp_TypeInfo_var;
extern TypeInfo* RequireComponentU5BU5D_t6_194_il2cpp_TypeInfo_var;
extern TypeInfo* TypeU5BU5D_t1_31_il2cpp_TypeInfo_var;
extern TypeInfo* List_1_t1_930_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1__ctor_m1_5611_MethodInfo_var;
extern const MethodInfo* List_1_ToArray_m1_5612_MethodInfo_var;
extern "C" TypeU5BU5D_t1_31* AttributeHelperEngine_GetRequiredComponents_m6_1400 (Object_t * __this /* static, unused */, Type_t * ___klass, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		RequireComponent_t6_196_0_0_0_var = il2cpp_codegen_type_from_index(1021);
		MonoBehaviour_t6_80_0_0_0_var = il2cpp_codegen_type_from_index(1022);
		Type_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3);
		RequireComponentU5BU5D_t6_194_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1020);
		TypeU5BU5D_t1_31_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(57);
		List_1_t1_930_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1024);
		List_1__ctor_m1_5611_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483822);
		List_1_ToArray_m1_5612_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483823);
		s_Il2CppMethodIntialized = true;
	}
	List_1_t1_930 * V_0 = {0};
	RequireComponentU5BU5D_t6_194* V_1 = {0};
	Type_t * V_2 = {0};
	RequireComponent_t6_196 * V_3 = {0};
	RequireComponentU5BU5D_t6_194* V_4 = {0};
	int32_t V_5 = 0;
	TypeU5BU5D_t1_31* V_6 = {0};
	{
		V_0 = (List_1_t1_930 *)NULL;
		goto IL_00e0;
	}

IL_0007:
	{
		Type_t * L_0 = ___klass;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_1 = Type_GetTypeFromHandle_m1_893(NULL /*static, unused*/, LoadTypeToken(RequireComponent_t6_196_0_0_0_var), /*hidden argument*/NULL);
		NullCheck(L_0);
		ObjectU5BU5D_t1_157* L_2 = (ObjectU5BU5D_t1_157*)VirtFuncInvoker2< ObjectU5BU5D_t1_157*, Type_t *, bool >::Invoke(13 /* System.Object[] System.Reflection.MemberInfo::GetCustomAttributes(System.Type,System.Boolean) */, L_0, L_1, 0);
		V_1 = ((RequireComponentU5BU5D_t6_194*)Castclass(L_2, RequireComponentU5BU5D_t6_194_il2cpp_TypeInfo_var));
		Type_t * L_3 = ___klass;
		NullCheck(L_3);
		Type_t * L_4 = (Type_t *)VirtFuncInvoker0< Type_t * >::Invoke(17 /* System.Type System.Type::get_BaseType() */, L_3);
		V_2 = L_4;
		RequireComponentU5BU5D_t6_194* L_5 = V_1;
		V_4 = L_5;
		V_5 = 0;
		goto IL_00d2;
	}

IL_0030:
	{
		RequireComponentU5BU5D_t6_194* L_6 = V_4;
		int32_t L_7 = V_5;
		NullCheck(L_6);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_6, L_7);
		int32_t L_8 = L_7;
		V_3 = (*(RequireComponent_t6_196 **)(RequireComponent_t6_196 **)SZArrayLdElema(L_6, L_8, sizeof(RequireComponent_t6_196 *)));
		List_1_t1_930 * L_9 = V_0;
		if (L_9)
		{
			goto IL_007b;
		}
	}
	{
		RequireComponentU5BU5D_t6_194* L_10 = V_1;
		NullCheck(L_10);
		if ((!(((uint32_t)(((int32_t)((int32_t)(((Array_t *)L_10)->max_length))))) == ((uint32_t)1))))
		{
			goto IL_007b;
		}
	}
	{
		Type_t * L_11 = V_2;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_12 = Type_GetTypeFromHandle_m1_893(NULL /*static, unused*/, LoadTypeToken(MonoBehaviour_t6_80_0_0_0_var), /*hidden argument*/NULL);
		if ((!(((Object_t*)(Type_t *)L_11) == ((Object_t*)(Type_t *)L_12))))
		{
			goto IL_007b;
		}
	}
	{
		TypeU5BU5D_t1_31* L_13 = ((TypeU5BU5D_t1_31*)SZArrayNew(TypeU5BU5D_t1_31_il2cpp_TypeInfo_var, 3));
		RequireComponent_t6_196 * L_14 = V_3;
		NullCheck(L_14);
		Type_t * L_15 = (L_14->___m_Type0_0);
		NullCheck(L_13);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_13, 0);
		ArrayElementTypeCheck (L_13, L_15);
		*((Type_t **)(Type_t **)SZArrayLdElema(L_13, 0, sizeof(Type_t *))) = (Type_t *)L_15;
		TypeU5BU5D_t1_31* L_16 = L_13;
		RequireComponent_t6_196 * L_17 = V_3;
		NullCheck(L_17);
		Type_t * L_18 = (L_17->___m_Type1_1);
		NullCheck(L_16);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_16, 1);
		ArrayElementTypeCheck (L_16, L_18);
		*((Type_t **)(Type_t **)SZArrayLdElema(L_16, 1, sizeof(Type_t *))) = (Type_t *)L_18;
		TypeU5BU5D_t1_31* L_19 = L_16;
		RequireComponent_t6_196 * L_20 = V_3;
		NullCheck(L_20);
		Type_t * L_21 = (L_20->___m_Type2_2);
		NullCheck(L_19);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_19, 2);
		ArrayElementTypeCheck (L_19, L_21);
		*((Type_t **)(Type_t **)SZArrayLdElema(L_19, 2, sizeof(Type_t *))) = (Type_t *)L_21;
		V_6 = L_19;
		TypeU5BU5D_t1_31* L_22 = V_6;
		return L_22;
	}

IL_007b:
	{
		List_1_t1_930 * L_23 = V_0;
		if (L_23)
		{
			goto IL_0087;
		}
	}
	{
		List_1_t1_930 * L_24 = (List_1_t1_930 *)il2cpp_codegen_object_new (List_1_t1_930_il2cpp_TypeInfo_var);
		List_1__ctor_m1_5611(L_24, /*hidden argument*/List_1__ctor_m1_5611_MethodInfo_var);
		V_0 = L_24;
	}

IL_0087:
	{
		RequireComponent_t6_196 * L_25 = V_3;
		NullCheck(L_25);
		Type_t * L_26 = (L_25->___m_Type0_0);
		if (!L_26)
		{
			goto IL_009e;
		}
	}
	{
		List_1_t1_930 * L_27 = V_0;
		RequireComponent_t6_196 * L_28 = V_3;
		NullCheck(L_28);
		Type_t * L_29 = (L_28->___m_Type0_0);
		NullCheck(L_27);
		VirtActionInvoker1< Type_t * >::Invoke(19 /* System.Void System.Collections.Generic.List`1<System.Type>::Add(!0) */, L_27, L_29);
	}

IL_009e:
	{
		RequireComponent_t6_196 * L_30 = V_3;
		NullCheck(L_30);
		Type_t * L_31 = (L_30->___m_Type1_1);
		if (!L_31)
		{
			goto IL_00b5;
		}
	}
	{
		List_1_t1_930 * L_32 = V_0;
		RequireComponent_t6_196 * L_33 = V_3;
		NullCheck(L_33);
		Type_t * L_34 = (L_33->___m_Type1_1);
		NullCheck(L_32);
		VirtActionInvoker1< Type_t * >::Invoke(19 /* System.Void System.Collections.Generic.List`1<System.Type>::Add(!0) */, L_32, L_34);
	}

IL_00b5:
	{
		RequireComponent_t6_196 * L_35 = V_3;
		NullCheck(L_35);
		Type_t * L_36 = (L_35->___m_Type2_2);
		if (!L_36)
		{
			goto IL_00cc;
		}
	}
	{
		List_1_t1_930 * L_37 = V_0;
		RequireComponent_t6_196 * L_38 = V_3;
		NullCheck(L_38);
		Type_t * L_39 = (L_38->___m_Type2_2);
		NullCheck(L_37);
		VirtActionInvoker1< Type_t * >::Invoke(19 /* System.Void System.Collections.Generic.List`1<System.Type>::Add(!0) */, L_37, L_39);
	}

IL_00cc:
	{
		int32_t L_40 = V_5;
		V_5 = ((int32_t)((int32_t)L_40+(int32_t)1));
	}

IL_00d2:
	{
		int32_t L_41 = V_5;
		RequireComponentU5BU5D_t6_194* L_42 = V_4;
		NullCheck(L_42);
		if ((((int32_t)L_41) < ((int32_t)(((int32_t)((int32_t)(((Array_t *)L_42)->max_length)))))))
		{
			goto IL_0030;
		}
	}
	{
		Type_t * L_43 = V_2;
		___klass = L_43;
	}

IL_00e0:
	{
		Type_t * L_44 = ___klass;
		if (!L_44)
		{
			goto IL_00f6;
		}
	}
	{
		Type_t * L_45 = ___klass;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_46 = Type_GetTypeFromHandle_m1_893(NULL /*static, unused*/, LoadTypeToken(MonoBehaviour_t6_80_0_0_0_var), /*hidden argument*/NULL);
		if ((!(((Object_t*)(Type_t *)L_45) == ((Object_t*)(Type_t *)L_46))))
		{
			goto IL_0007;
		}
	}

IL_00f6:
	{
		List_1_t1_930 * L_47 = V_0;
		if (L_47)
		{
			goto IL_00fe;
		}
	}
	{
		return (TypeU5BU5D_t1_31*)NULL;
	}

IL_00fe:
	{
		List_1_t1_930 * L_48 = V_0;
		NullCheck(L_48);
		TypeU5BU5D_t1_31* L_49 = List_1_ToArray_m1_5612(L_48, /*hidden argument*/List_1_ToArray_m1_5612_MethodInfo_var);
		return L_49;
	}
}
// System.Boolean UnityEngine.AttributeHelperEngine::CheckIsEditorScript(System.Type)
extern const Il2CppType* ExecuteInEditMode_t6_198_0_0_0_var;
extern const Il2CppType* MonoBehaviour_t6_80_0_0_0_var;
extern TypeInfo* Type_t_il2cpp_TypeInfo_var;
extern "C" bool AttributeHelperEngine_CheckIsEditorScript_m6_1401 (Object_t * __this /* static, unused */, Type_t * ___klass, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ExecuteInEditMode_t6_198_0_0_0_var = il2cpp_codegen_type_from_index(1019);
		MonoBehaviour_t6_80_0_0_0_var = il2cpp_codegen_type_from_index(1022);
		Type_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3);
		s_Il2CppMethodIntialized = true;
	}
	ObjectU5BU5D_t1_157* V_0 = {0};
	int32_t V_1 = 0;
	{
		goto IL_002b;
	}

IL_0005:
	{
		Type_t * L_0 = ___klass;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_1 = Type_GetTypeFromHandle_m1_893(NULL /*static, unused*/, LoadTypeToken(ExecuteInEditMode_t6_198_0_0_0_var), /*hidden argument*/NULL);
		NullCheck(L_0);
		ObjectU5BU5D_t1_157* L_2 = (ObjectU5BU5D_t1_157*)VirtFuncInvoker2< ObjectU5BU5D_t1_157*, Type_t *, bool >::Invoke(13 /* System.Object[] System.Reflection.MemberInfo::GetCustomAttributes(System.Type,System.Boolean) */, L_0, L_1, 0);
		V_0 = L_2;
		ObjectU5BU5D_t1_157* L_3 = V_0;
		NullCheck(L_3);
		V_1 = (((int32_t)((int32_t)(((Array_t *)L_3)->max_length))));
		int32_t L_4 = V_1;
		if (!L_4)
		{
			goto IL_0023;
		}
	}
	{
		return 1;
	}

IL_0023:
	{
		Type_t * L_5 = ___klass;
		NullCheck(L_5);
		Type_t * L_6 = (Type_t *)VirtFuncInvoker0< Type_t * >::Invoke(17 /* System.Type System.Type::get_BaseType() */, L_5);
		___klass = L_6;
	}

IL_002b:
	{
		Type_t * L_7 = ___klass;
		if (!L_7)
		{
			goto IL_0041;
		}
	}
	{
		Type_t * L_8 = ___klass;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_9 = Type_GetTypeFromHandle_m1_893(NULL /*static, unused*/, LoadTypeToken(MonoBehaviour_t6_80_0_0_0_var), /*hidden argument*/NULL);
		if ((!(((Object_t*)(Type_t *)L_8) == ((Object_t*)(Type_t *)L_9))))
		{
			goto IL_0005;
		}
	}

IL_0041:
	{
		return 0;
	}
}
// System.Void UnityEngine.RequireComponent::.ctor(System.Type)
extern "C" void RequireComponent__ctor_m6_1402 (RequireComponent_t6_196 * __this, Type_t * ___requiredComponent, const MethodInfo* method)
{
	{
		Attribute__ctor_m1_17(__this, /*hidden argument*/NULL);
		Type_t * L_0 = ___requiredComponent;
		__this->___m_Type0_0 = L_0;
		return;
	}
}
// System.Void UnityEngine.AddComponentMenu::.ctor(System.String)
extern "C" void AddComponentMenu__ctor_m6_1403 (AddComponentMenu_t6_197 * __this, String_t* ___menuName, const MethodInfo* method)
{
	{
		Attribute__ctor_m1_17(__this, /*hidden argument*/NULL);
		String_t* L_0 = ___menuName;
		__this->___m_AddComponentMenu_0 = L_0;
		__this->___m_Ordering_1 = 0;
		return;
	}
}
// System.Void UnityEngine.ExecuteInEditMode::.ctor()
extern "C" void ExecuteInEditMode__ctor_m6_1404 (ExecuteInEditMode_t6_198 * __this, const MethodInfo* method)
{
	{
		Attribute__ctor_m1_17(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.HideInInspector::.ctor()
extern "C" void HideInInspector__ctor_m6_1405 (HideInInspector_t6_199 * __this, const MethodInfo* method)
{
	{
		Attribute__ctor_m1_17(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.SetupCoroutine::.ctor()
extern "C" void SetupCoroutine__ctor_m6_1406 (SetupCoroutine_t6_200 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1_0(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Object UnityEngine.SetupCoroutine::InvokeMember(System.Object,System.String,System.Object)
extern TypeInfo* ObjectU5BU5D_t1_157_il2cpp_TypeInfo_var;
extern "C" Object_t * SetupCoroutine_InvokeMember_m6_1407 (Object_t * __this /* static, unused */, Object_t * ___behaviour, String_t* ___name, Object_t * ___variable, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ObjectU5BU5D_t1_157_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(19);
		s_Il2CppMethodIntialized = true;
	}
	ObjectU5BU5D_t1_157* V_0 = {0};
	{
		V_0 = (ObjectU5BU5D_t1_157*)NULL;
		Object_t * L_0 = ___variable;
		if (!L_0)
		{
			goto IL_0013;
		}
	}
	{
		V_0 = ((ObjectU5BU5D_t1_157*)SZArrayNew(ObjectU5BU5D_t1_157_il2cpp_TypeInfo_var, 1));
		ObjectU5BU5D_t1_157* L_1 = V_0;
		Object_t * L_2 = ___variable;
		NullCheck(L_1);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_1, 0);
		ArrayElementTypeCheck (L_1, L_2);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_1, 0, sizeof(Object_t *))) = (Object_t *)L_2;
	}

IL_0013:
	{
		Object_t * L_3 = ___behaviour;
		NullCheck(L_3);
		Type_t * L_4 = Object_GetType_m1_5(L_3, /*hidden argument*/NULL);
		String_t* L_5 = ___name;
		Object_t * L_6 = ___behaviour;
		ObjectU5BU5D_t1_157* L_7 = V_0;
		NullCheck(L_4);
		Object_t * L_8 = (Object_t *)VirtFuncInvoker8< Object_t *, String_t*, int32_t, Binder_t1_325 *, Object_t *, ObjectU5BU5D_t1_157*, ParameterModifierU5BU5D_t1_808*, CultureInfo_t1_162 *, StringU5BU5D_t1_202* >::Invoke(71 /* System.Object System.Type::InvokeMember(System.String,System.Reflection.BindingFlags,System.Reflection.Binder,System.Object,System.Object[],System.Reflection.ParameterModifier[],System.Globalization.CultureInfo,System.String[]) */, L_4, L_5, ((int32_t)308), (Binder_t1_325 *)NULL, L_6, L_7, (ParameterModifierU5BU5D_t1_808*)(ParameterModifierU5BU5D_t1_808*)NULL, (CultureInfo_t1_162 *)NULL, (StringU5BU5D_t1_202*)(StringU5BU5D_t1_202*)NULL);
		return L_8;
	}
}
// System.Object UnityEngine.SetupCoroutine::InvokeStatic(System.Type,System.String,System.Object)
extern TypeInfo* ObjectU5BU5D_t1_157_il2cpp_TypeInfo_var;
extern "C" Object_t * SetupCoroutine_InvokeStatic_m6_1408 (Object_t * __this /* static, unused */, Type_t * ___klass, String_t* ___name, Object_t * ___variable, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ObjectU5BU5D_t1_157_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(19);
		s_Il2CppMethodIntialized = true;
	}
	ObjectU5BU5D_t1_157* V_0 = {0};
	{
		V_0 = (ObjectU5BU5D_t1_157*)NULL;
		Object_t * L_0 = ___variable;
		if (!L_0)
		{
			goto IL_0013;
		}
	}
	{
		V_0 = ((ObjectU5BU5D_t1_157*)SZArrayNew(ObjectU5BU5D_t1_157_il2cpp_TypeInfo_var, 1));
		ObjectU5BU5D_t1_157* L_1 = V_0;
		Object_t * L_2 = ___variable;
		NullCheck(L_1);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_1, 0);
		ArrayElementTypeCheck (L_1, L_2);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_1, 0, sizeof(Object_t *))) = (Object_t *)L_2;
	}

IL_0013:
	{
		Type_t * L_3 = ___klass;
		String_t* L_4 = ___name;
		ObjectU5BU5D_t1_157* L_5 = V_0;
		NullCheck(L_3);
		Object_t * L_6 = (Object_t *)VirtFuncInvoker8< Object_t *, String_t*, int32_t, Binder_t1_325 *, Object_t *, ObjectU5BU5D_t1_157*, ParameterModifierU5BU5D_t1_808*, CultureInfo_t1_162 *, StringU5BU5D_t1_202* >::Invoke(71 /* System.Object System.Type::InvokeMember(System.String,System.Reflection.BindingFlags,System.Reflection.Binder,System.Object,System.Object[],System.Reflection.ParameterModifier[],System.Globalization.CultureInfo,System.String[]) */, L_3, L_4, ((int32_t)312), (Binder_t1_325 *)NULL, NULL, L_5, (ParameterModifierU5BU5D_t1_808*)(ParameterModifierU5BU5D_t1_808*)NULL, (CultureInfo_t1_162 *)NULL, (StringU5BU5D_t1_202*)(StringU5BU5D_t1_202*)NULL);
		return L_6;
	}
}
// System.Void UnityEngine.WritableAttribute::.ctor()
extern "C" void WritableAttribute__ctor_m6_1409 (WritableAttribute_t6_201 * __this, const MethodInfo* method)
{
	{
		Attribute__ctor_m1_17(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.AssemblyIsEditorAssembly::.ctor()
extern "C" void AssemblyIsEditorAssembly__ctor_m6_1410 (AssemblyIsEditorAssembly_t6_202 * __this, const MethodInfo* method)
{
	{
		Attribute__ctor_m1_17(__this, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.SocialPlatforms.Impl.UserProfile UnityEngine.SocialPlatforms.GameCenter.GcUserProfileData::ToUserProfile()
extern TypeInfo* UserProfile_t6_213_il2cpp_TypeInfo_var;
extern "C" UserProfile_t6_213 * GcUserProfileData_ToUserProfile_m6_1411 (GcUserProfileData_t6_203 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		UserProfile_t6_213_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(913);
		s_Il2CppMethodIntialized = true;
	}
	String_t* G_B2_0 = {0};
	String_t* G_B2_1 = {0};
	String_t* G_B1_0 = {0};
	String_t* G_B1_1 = {0};
	int32_t G_B3_0 = 0;
	String_t* G_B3_1 = {0};
	String_t* G_B3_2 = {0};
	{
		String_t* L_0 = (__this->___userName_0);
		String_t* L_1 = (__this->___userID_1);
		int32_t L_2 = (__this->___isFriend_2);
		G_B1_0 = L_1;
		G_B1_1 = L_0;
		if ((!(((uint32_t)L_2) == ((uint32_t)1))))
		{
			G_B2_0 = L_1;
			G_B2_1 = L_0;
			goto IL_001e;
		}
	}
	{
		G_B3_0 = 1;
		G_B3_1 = G_B1_0;
		G_B3_2 = G_B1_1;
		goto IL_001f;
	}

IL_001e:
	{
		G_B3_0 = 0;
		G_B3_1 = G_B2_0;
		G_B3_2 = G_B2_1;
	}

IL_001f:
	{
		Texture2D_t6_33 * L_3 = (__this->___image_3);
		UserProfile_t6_213 * L_4 = (UserProfile_t6_213 *)il2cpp_codegen_object_new (UserProfile_t6_213_il2cpp_TypeInfo_var);
		UserProfile__ctor_m6_1431(L_4, G_B3_2, G_B3_1, G_B3_0, 3, L_3, /*hidden argument*/NULL);
		return L_4;
	}
}
// System.Void UnityEngine.SocialPlatforms.GameCenter.GcUserProfileData::AddToArray(UnityEngine.SocialPlatforms.Impl.UserProfile[]&,System.Int32)
extern Il2CppCodeGenString* _stringLiteral2831;
extern "C" void GcUserProfileData_AddToArray_m6_1412 (GcUserProfileData_t6_203 * __this, UserProfileU5BU5D_t6_20** ___array, int32_t ___number, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		_stringLiteral2831 = il2cpp_codegen_string_literal_from_index(2831);
		s_Il2CppMethodIntialized = true;
	}
	{
		UserProfileU5BU5D_t6_20** L_0 = ___array;
		NullCheck((*((UserProfileU5BU5D_t6_20**)L_0)));
		int32_t L_1 = ___number;
		if ((((int32_t)(((int32_t)((int32_t)(((Array_t *)(*((UserProfileU5BU5D_t6_20**)L_0)))->max_length))))) <= ((int32_t)L_1)))
		{
			goto IL_0020;
		}
	}
	{
		int32_t L_2 = ___number;
		if ((((int32_t)L_2) < ((int32_t)0)))
		{
			goto IL_0020;
		}
	}
	{
		UserProfileU5BU5D_t6_20** L_3 = ___array;
		int32_t L_4 = ___number;
		UserProfile_t6_213 * L_5 = GcUserProfileData_ToUserProfile_m6_1411(__this, /*hidden argument*/NULL);
		NullCheck((*((UserProfileU5BU5D_t6_20**)L_3)));
		IL2CPP_ARRAY_BOUNDS_CHECK((*((UserProfileU5BU5D_t6_20**)L_3)), L_4);
		ArrayElementTypeCheck ((*((UserProfileU5BU5D_t6_20**)L_3)), L_5);
		*((UserProfile_t6_213 **)(UserProfile_t6_213 **)SZArrayLdElema((*((UserProfileU5BU5D_t6_20**)L_3)), L_4, sizeof(UserProfile_t6_213 *))) = (UserProfile_t6_213 *)L_5;
		goto IL_002a;
	}

IL_0020:
	{
		Debug_Log_m6_489(NULL /*static, unused*/, _stringLiteral2831, /*hidden argument*/NULL);
	}

IL_002a:
	{
		return;
	}
}
// UnityEngine.SocialPlatforms.Impl.AchievementDescription UnityEngine.SocialPlatforms.GameCenter.GcAchievementDescriptionData::ToAchievementDescription()
extern TypeInfo* AchievementDescription_t6_216_il2cpp_TypeInfo_var;
extern "C" AchievementDescription_t6_216 * GcAchievementDescriptionData_ToAchievementDescription_m6_1413 (GcAchievementDescriptionData_t6_204 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		AchievementDescription_t6_216_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(910);
		s_Il2CppMethodIntialized = true;
	}
	String_t* G_B2_0 = {0};
	String_t* G_B2_1 = {0};
	Texture2D_t6_33 * G_B2_2 = {0};
	String_t* G_B2_3 = {0};
	String_t* G_B2_4 = {0};
	String_t* G_B1_0 = {0};
	String_t* G_B1_1 = {0};
	Texture2D_t6_33 * G_B1_2 = {0};
	String_t* G_B1_3 = {0};
	String_t* G_B1_4 = {0};
	int32_t G_B3_0 = 0;
	String_t* G_B3_1 = {0};
	String_t* G_B3_2 = {0};
	Texture2D_t6_33 * G_B3_3 = {0};
	String_t* G_B3_4 = {0};
	String_t* G_B3_5 = {0};
	{
		String_t* L_0 = (__this->___m_Identifier_0);
		String_t* L_1 = (__this->___m_Title_1);
		Texture2D_t6_33 * L_2 = (__this->___m_Image_2);
		String_t* L_3 = (__this->___m_AchievedDescription_3);
		String_t* L_4 = (__this->___m_UnachievedDescription_4);
		int32_t L_5 = (__this->___m_Hidden_5);
		G_B1_0 = L_4;
		G_B1_1 = L_3;
		G_B1_2 = L_2;
		G_B1_3 = L_1;
		G_B1_4 = L_0;
		if (L_5)
		{
			G_B2_0 = L_4;
			G_B2_1 = L_3;
			G_B2_2 = L_2;
			G_B2_3 = L_1;
			G_B2_4 = L_0;
			goto IL_002f;
		}
	}
	{
		G_B3_0 = 0;
		G_B3_1 = G_B1_0;
		G_B3_2 = G_B1_1;
		G_B3_3 = G_B1_2;
		G_B3_4 = G_B1_3;
		G_B3_5 = G_B1_4;
		goto IL_0030;
	}

IL_002f:
	{
		G_B3_0 = 1;
		G_B3_1 = G_B2_0;
		G_B3_2 = G_B2_1;
		G_B3_3 = G_B2_2;
		G_B3_4 = G_B2_3;
		G_B3_5 = G_B2_4;
	}

IL_0030:
	{
		int32_t L_6 = (__this->___m_Points_6);
		AchievementDescription_t6_216 * L_7 = (AchievementDescription_t6_216 *)il2cpp_codegen_object_new (AchievementDescription_t6_216_il2cpp_TypeInfo_var);
		AchievementDescription__ctor_m6_1451(L_7, G_B3_5, G_B3_4, G_B3_3, G_B3_2, G_B3_1, G_B3_0, L_6, /*hidden argument*/NULL);
		return L_7;
	}
}
// UnityEngine.SocialPlatforms.Impl.Achievement UnityEngine.SocialPlatforms.GameCenter.GcAchievementData::ToAchievement()
extern TypeInfo* Achievement_t6_215_il2cpp_TypeInfo_var;
extern "C" Achievement_t6_215 * GcAchievementData_ToAchievement_m6_1414 (GcAchievementData_t6_205 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Achievement_t6_215_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(920);
		s_Il2CppMethodIntialized = true;
	}
	DateTime_t1_127  V_0 = {0};
	double G_B2_0 = 0.0;
	String_t* G_B2_1 = {0};
	double G_B1_0 = 0.0;
	String_t* G_B1_1 = {0};
	int32_t G_B3_0 = 0;
	double G_B3_1 = 0.0;
	String_t* G_B3_2 = {0};
	int32_t G_B5_0 = 0;
	double G_B5_1 = 0.0;
	String_t* G_B5_2 = {0};
	int32_t G_B4_0 = 0;
	double G_B4_1 = 0.0;
	String_t* G_B4_2 = {0};
	int32_t G_B6_0 = 0;
	int32_t G_B6_1 = 0;
	double G_B6_2 = 0.0;
	String_t* G_B6_3 = {0};
	{
		String_t* L_0 = (__this->___m_Identifier_0);
		double L_1 = (__this->___m_PercentCompleted_1);
		int32_t L_2 = (__this->___m_Completed_2);
		G_B1_0 = L_1;
		G_B1_1 = L_0;
		if (L_2)
		{
			G_B2_0 = L_1;
			G_B2_1 = L_0;
			goto IL_001d;
		}
	}
	{
		G_B3_0 = 0;
		G_B3_1 = G_B1_0;
		G_B3_2 = G_B1_1;
		goto IL_001e;
	}

IL_001d:
	{
		G_B3_0 = 1;
		G_B3_1 = G_B2_0;
		G_B3_2 = G_B2_1;
	}

IL_001e:
	{
		int32_t L_3 = (__this->___m_Hidden_3);
		G_B4_0 = G_B3_0;
		G_B4_1 = G_B3_1;
		G_B4_2 = G_B3_2;
		if (L_3)
		{
			G_B5_0 = G_B3_0;
			G_B5_1 = G_B3_1;
			G_B5_2 = G_B3_2;
			goto IL_002f;
		}
	}
	{
		G_B6_0 = 0;
		G_B6_1 = G_B4_0;
		G_B6_2 = G_B4_1;
		G_B6_3 = G_B4_2;
		goto IL_0030;
	}

IL_002f:
	{
		G_B6_0 = 1;
		G_B6_1 = G_B5_0;
		G_B6_2 = G_B5_1;
		G_B6_3 = G_B5_2;
	}

IL_0030:
	{
		DateTime__ctor_m1_4902((&V_0), ((int32_t)1970), 1, 1, 0, 0, 0, 0, /*hidden argument*/NULL);
		int32_t L_4 = (__this->___m_LastReportedDate_4);
		DateTime_t1_127  L_5 = DateTime_AddSeconds_m1_4941((&V_0), (((double)((double)L_4))), /*hidden argument*/NULL);
		Achievement_t6_215 * L_6 = (Achievement_t6_215 *)il2cpp_codegen_object_new (Achievement_t6_215_il2cpp_TypeInfo_var);
		Achievement__ctor_m6_1440(L_6, G_B6_3, G_B6_2, G_B6_1, G_B6_0, L_5, /*hidden argument*/NULL);
		return L_6;
	}
}
// Conversion methods for marshalling of: UnityEngine.SocialPlatforms.GameCenter.GcAchievementData
extern "C" void GcAchievementData_t6_205_marshal(const GcAchievementData_t6_205& unmarshaled, GcAchievementData_t6_205_marshaled& marshaled)
{
	marshaled.___m_Identifier_0 = il2cpp_codegen_marshal_string(unmarshaled.___m_Identifier_0);
	marshaled.___m_PercentCompleted_1 = unmarshaled.___m_PercentCompleted_1;
	marshaled.___m_Completed_2 = unmarshaled.___m_Completed_2;
	marshaled.___m_Hidden_3 = unmarshaled.___m_Hidden_3;
	marshaled.___m_LastReportedDate_4 = unmarshaled.___m_LastReportedDate_4;
}
extern "C" void GcAchievementData_t6_205_marshal_back(const GcAchievementData_t6_205_marshaled& marshaled, GcAchievementData_t6_205& unmarshaled)
{
	unmarshaled.___m_Identifier_0 = il2cpp_codegen_marshal_string_result(marshaled.___m_Identifier_0);
	unmarshaled.___m_PercentCompleted_1 = marshaled.___m_PercentCompleted_1;
	unmarshaled.___m_Completed_2 = marshaled.___m_Completed_2;
	unmarshaled.___m_Hidden_3 = marshaled.___m_Hidden_3;
	unmarshaled.___m_LastReportedDate_4 = marshaled.___m_LastReportedDate_4;
}
// Conversion method for clean up from marshalling of: UnityEngine.SocialPlatforms.GameCenter.GcAchievementData
extern "C" void GcAchievementData_t6_205_marshal_cleanup(GcAchievementData_t6_205_marshaled& marshaled)
{
	il2cpp_codegen_marshal_free(marshaled.___m_Identifier_0);
	marshaled.___m_Identifier_0 = NULL;
}
// UnityEngine.SocialPlatforms.Impl.Score UnityEngine.SocialPlatforms.GameCenter.GcScoreData::ToScore()
extern TypeInfo* Score_t6_217_il2cpp_TypeInfo_var;
extern "C" Score_t6_217 * GcScoreData_ToScore_m6_1415 (GcScoreData_t6_206 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Score_t6_217_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(924);
		s_Il2CppMethodIntialized = true;
	}
	DateTime_t1_127  V_0 = {0};
	{
		String_t* L_0 = (__this->___m_Category_0);
		int32_t L_1 = (__this->___m_ValueHigh_2);
		int32_t L_2 = (__this->___m_ValueLow_1);
		String_t* L_3 = (__this->___m_PlayerID_5);
		DateTime__ctor_m1_4902((&V_0), ((int32_t)1970), 1, 1, 0, 0, 0, 0, /*hidden argument*/NULL);
		int32_t L_4 = (__this->___m_Date_3);
		DateTime_t1_127  L_5 = DateTime_AddSeconds_m1_4941((&V_0), (((double)((double)L_4))), /*hidden argument*/NULL);
		String_t* L_6 = (__this->___m_FormattedValue_4);
		int32_t L_7 = (__this->___m_Rank_6);
		Score_t6_217 * L_8 = (Score_t6_217 *)il2cpp_codegen_object_new (Score_t6_217_il2cpp_TypeInfo_var);
		Score__ctor_m6_1462(L_8, L_0, ((int64_t)((int64_t)((int64_t)((int64_t)(((int64_t)((int64_t)L_1)))<<(int32_t)((int32_t)32)))+(int64_t)(((int64_t)((int64_t)L_2))))), L_3, L_5, L_6, L_7, /*hidden argument*/NULL);
		return L_8;
	}
}
// Conversion methods for marshalling of: UnityEngine.SocialPlatforms.GameCenter.GcScoreData
extern "C" void GcScoreData_t6_206_marshal(const GcScoreData_t6_206& unmarshaled, GcScoreData_t6_206_marshaled& marshaled)
{
	marshaled.___m_Category_0 = il2cpp_codegen_marshal_string(unmarshaled.___m_Category_0);
	marshaled.___m_ValueLow_1 = unmarshaled.___m_ValueLow_1;
	marshaled.___m_ValueHigh_2 = unmarshaled.___m_ValueHigh_2;
	marshaled.___m_Date_3 = unmarshaled.___m_Date_3;
	marshaled.___m_FormattedValue_4 = il2cpp_codegen_marshal_string(unmarshaled.___m_FormattedValue_4);
	marshaled.___m_PlayerID_5 = il2cpp_codegen_marshal_string(unmarshaled.___m_PlayerID_5);
	marshaled.___m_Rank_6 = unmarshaled.___m_Rank_6;
}
extern "C" void GcScoreData_t6_206_marshal_back(const GcScoreData_t6_206_marshaled& marshaled, GcScoreData_t6_206& unmarshaled)
{
	unmarshaled.___m_Category_0 = il2cpp_codegen_marshal_string_result(marshaled.___m_Category_0);
	unmarshaled.___m_ValueLow_1 = marshaled.___m_ValueLow_1;
	unmarshaled.___m_ValueHigh_2 = marshaled.___m_ValueHigh_2;
	unmarshaled.___m_Date_3 = marshaled.___m_Date_3;
	unmarshaled.___m_FormattedValue_4 = il2cpp_codegen_marshal_string_result(marshaled.___m_FormattedValue_4);
	unmarshaled.___m_PlayerID_5 = il2cpp_codegen_marshal_string_result(marshaled.___m_PlayerID_5);
	unmarshaled.___m_Rank_6 = marshaled.___m_Rank_6;
}
// Conversion method for clean up from marshalling of: UnityEngine.SocialPlatforms.GameCenter.GcScoreData
extern "C" void GcScoreData_t6_206_marshal_cleanup(GcScoreData_t6_206_marshaled& marshaled)
{
	il2cpp_codegen_marshal_free(marshaled.___m_Category_0);
	marshaled.___m_Category_0 = NULL;
	il2cpp_codegen_marshal_free(marshaled.___m_FormattedValue_4);
	marshaled.___m_FormattedValue_4 = NULL;
	il2cpp_codegen_marshal_free(marshaled.___m_PlayerID_5);
	marshaled.___m_PlayerID_5 = NULL;
}
// System.Int32 UnityEngine.Resolution::get_width()
extern "C" int32_t Resolution_get_width_m6_1416 (Resolution_t6_207 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (__this->___m_Width_0);
		return L_0;
	}
}
// System.Void UnityEngine.Resolution::set_width(System.Int32)
extern "C" void Resolution_set_width_m6_1417 (Resolution_t6_207 * __this, int32_t ___value, const MethodInfo* method)
{
	{
		int32_t L_0 = ___value;
		__this->___m_Width_0 = L_0;
		return;
	}
}
// System.Int32 UnityEngine.Resolution::get_height()
extern "C" int32_t Resolution_get_height_m6_1418 (Resolution_t6_207 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (__this->___m_Height_1);
		return L_0;
	}
}
// System.Void UnityEngine.Resolution::set_height(System.Int32)
extern "C" void Resolution_set_height_m6_1419 (Resolution_t6_207 * __this, int32_t ___value, const MethodInfo* method)
{
	{
		int32_t L_0 = ___value;
		__this->___m_Height_1 = L_0;
		return;
	}
}
// System.Int32 UnityEngine.Resolution::get_refreshRate()
extern "C" int32_t Resolution_get_refreshRate_m6_1420 (Resolution_t6_207 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (__this->___m_RefreshRate_2);
		return L_0;
	}
}
// System.Void UnityEngine.Resolution::set_refreshRate(System.Int32)
extern "C" void Resolution_set_refreshRate_m6_1421 (Resolution_t6_207 * __this, int32_t ___value, const MethodInfo* method)
{
	{
		int32_t L_0 = ___value;
		__this->___m_RefreshRate_2 = L_0;
		return;
	}
}
// System.String UnityEngine.Resolution::ToString()
extern TypeInfo* ObjectU5BU5D_t1_157_il2cpp_TypeInfo_var;
extern TypeInfo* Int32_t1_3_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2832;
extern "C" String_t* Resolution_ToString_m6_1422 (Resolution_t6_207 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ObjectU5BU5D_t1_157_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(19);
		Int32_t1_3_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(8);
		_stringLiteral2832 = il2cpp_codegen_string_literal_from_index(2832);
		s_Il2CppMethodIntialized = true;
	}
	{
		ObjectU5BU5D_t1_157* L_0 = ((ObjectU5BU5D_t1_157*)SZArrayNew(ObjectU5BU5D_t1_157_il2cpp_TypeInfo_var, 3));
		int32_t L_1 = (__this->___m_Width_0);
		int32_t L_2 = L_1;
		Object_t * L_3 = Box(Int32_t1_3_il2cpp_TypeInfo_var, &L_2);
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, 0);
		ArrayElementTypeCheck (L_0, L_3);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_0, 0, sizeof(Object_t *))) = (Object_t *)L_3;
		ObjectU5BU5D_t1_157* L_4 = L_0;
		int32_t L_5 = (__this->___m_Height_1);
		int32_t L_6 = L_5;
		Object_t * L_7 = Box(Int32_t1_3_il2cpp_TypeInfo_var, &L_6);
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, 1);
		ArrayElementTypeCheck (L_4, L_7);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_4, 1, sizeof(Object_t *))) = (Object_t *)L_7;
		ObjectU5BU5D_t1_157* L_8 = L_4;
		int32_t L_9 = (__this->___m_RefreshRate_2);
		int32_t L_10 = L_9;
		Object_t * L_11 = Box(Int32_t1_3_il2cpp_TypeInfo_var, &L_10);
		NullCheck(L_8);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_8, 2);
		ArrayElementTypeCheck (L_8, L_11);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_8, 2, sizeof(Object_t *))) = (Object_t *)L_11;
		String_t* L_12 = UnityString_Format_m6_437(NULL /*static, unused*/, _stringLiteral2832, L_8, /*hidden argument*/NULL);
		return L_12;
	}
}
// System.Void UnityEngine.GUIStateObjects::.cctor()
extern TypeInfo* Dictionary_2_t1_919_il2cpp_TypeInfo_var;
extern TypeInfo* GUIStateObjects_t6_212_il2cpp_TypeInfo_var;
extern const MethodInfo* Dictionary_2__ctor_m1_5613_MethodInfo_var;
extern "C" void GUIStateObjects__cctor_m6_1423 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Dictionary_2_t1_919_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1025);
		GUIStateObjects_t6_212_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1013);
		Dictionary_2__ctor_m1_5613_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483824);
		s_Il2CppMethodIntialized = true;
	}
	{
		Dictionary_2_t1_919 * L_0 = (Dictionary_2_t1_919 *)il2cpp_codegen_object_new (Dictionary_2_t1_919_il2cpp_TypeInfo_var);
		Dictionary_2__ctor_m1_5613(L_0, /*hidden argument*/Dictionary_2__ctor_m1_5613_MethodInfo_var);
		((GUIStateObjects_t6_212_StaticFields*)GUIStateObjects_t6_212_il2cpp_TypeInfo_var->static_fields)->___s_StateCache_0 = L_0;
		return;
	}
}
// System.Object UnityEngine.GUIStateObjects::GetStateObject(System.Type,System.Int32)
extern TypeInfo* GUIStateObjects_t6_212_il2cpp_TypeInfo_var;
extern "C" Object_t * GUIStateObjects_GetStateObject_m6_1424 (Object_t * __this /* static, unused */, Type_t * ___t, int32_t ___controlID, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GUIStateObjects_t6_212_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1013);
		s_Il2CppMethodIntialized = true;
	}
	Object_t * V_0 = {0};
	{
		IL2CPP_RUNTIME_CLASS_INIT(GUIStateObjects_t6_212_il2cpp_TypeInfo_var);
		Dictionary_2_t1_919 * L_0 = ((GUIStateObjects_t6_212_StaticFields*)GUIStateObjects_t6_212_il2cpp_TypeInfo_var->static_fields)->___s_StateCache_0;
		int32_t L_1 = ___controlID;
		NullCheck(L_0);
		bool L_2 = (bool)VirtFuncInvoker2< bool, int32_t, Object_t ** >::Invoke(32 /* System.Boolean System.Collections.Generic.Dictionary`2<System.Int32,System.Object>::TryGetValue(!0,!1&) */, L_0, L_1, (&V_0));
		if (!L_2)
		{
			goto IL_001e;
		}
	}
	{
		Object_t * L_3 = V_0;
		NullCheck(L_3);
		Type_t * L_4 = Object_GetType_m1_5(L_3, /*hidden argument*/NULL);
		Type_t * L_5 = ___t;
		if ((((Object_t*)(Type_t *)L_4) == ((Object_t*)(Type_t *)L_5)))
		{
			goto IL_0031;
		}
	}

IL_001e:
	{
		Type_t * L_6 = ___t;
		Object_t * L_7 = Activator_CreateInstance_m1_4575(NULL /*static, unused*/, L_6, /*hidden argument*/NULL);
		V_0 = L_7;
		IL2CPP_RUNTIME_CLASS_INIT(GUIStateObjects_t6_212_il2cpp_TypeInfo_var);
		Dictionary_2_t1_919 * L_8 = ((GUIStateObjects_t6_212_StaticFields*)GUIStateObjects_t6_212_il2cpp_TypeInfo_var->static_fields)->___s_StateCache_0;
		int32_t L_9 = ___controlID;
		Object_t * L_10 = V_0;
		NullCheck(L_8);
		VirtActionInvoker2< int32_t, Object_t * >::Invoke(26 /* System.Void System.Collections.Generic.Dictionary`2<System.Int32,System.Object>::set_Item(!0,!1) */, L_8, L_9, L_10);
	}

IL_0031:
	{
		Object_t * L_11 = V_0;
		return L_11;
	}
}
// System.Void UnityEngine.SocialPlatforms.Impl.LocalUser::.ctor()
extern TypeInfo* UserProfileU5BU5D_t6_20_il2cpp_TypeInfo_var;
extern "C" void LocalUser__ctor_m6_1425 (LocalUser_t6_21 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		UserProfileU5BU5D_t6_20_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(912);
		s_Il2CppMethodIntialized = true;
	}
	{
		UserProfile__ctor_m6_1430(__this, /*hidden argument*/NULL);
		__this->___m_Friends_5 = (IUserProfileU5BU5D_t6_214*)((UserProfileU5BU5D_t6_20*)SZArrayNew(UserProfileU5BU5D_t6_20_il2cpp_TypeInfo_var, 0));
		__this->___m_Authenticated_6 = 0;
		__this->___m_Underage_7 = 0;
		return;
	}
}
// System.Void UnityEngine.SocialPlatforms.Impl.LocalUser::SetFriends(UnityEngine.SocialPlatforms.IUserProfile[])
extern "C" void LocalUser_SetFriends_m6_1426 (LocalUser_t6_21 * __this, IUserProfileU5BU5D_t6_214* ___friends, const MethodInfo* method)
{
	{
		IUserProfileU5BU5D_t6_214* L_0 = ___friends;
		__this->___m_Friends_5 = L_0;
		return;
	}
}
// System.Void UnityEngine.SocialPlatforms.Impl.LocalUser::SetAuthenticated(System.Boolean)
extern "C" void LocalUser_SetAuthenticated_m6_1427 (LocalUser_t6_21 * __this, bool ___value, const MethodInfo* method)
{
	{
		bool L_0 = ___value;
		__this->___m_Authenticated_6 = L_0;
		return;
	}
}
// System.Void UnityEngine.SocialPlatforms.Impl.LocalUser::SetUnderage(System.Boolean)
extern "C" void LocalUser_SetUnderage_m6_1428 (LocalUser_t6_21 * __this, bool ___value, const MethodInfo* method)
{
	{
		bool L_0 = ___value;
		__this->___m_Underage_7 = L_0;
		return;
	}
}
// System.Boolean UnityEngine.SocialPlatforms.Impl.LocalUser::get_authenticated()
extern "C" bool LocalUser_get_authenticated_m6_1429 (LocalUser_t6_21 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (__this->___m_Authenticated_6);
		return L_0;
	}
}
// System.Void UnityEngine.SocialPlatforms.Impl.UserProfile::.ctor()
extern TypeInfo* Texture2D_t6_33_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2833;
extern Il2CppCodeGenString* _stringLiteral140;
extern "C" void UserProfile__ctor_m6_1430 (UserProfile_t6_213 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Texture2D_t6_33_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(932);
		_stringLiteral2833 = il2cpp_codegen_string_literal_from_index(2833);
		_stringLiteral140 = il2cpp_codegen_string_literal_from_index(140);
		s_Il2CppMethodIntialized = true;
	}
	{
		Object__ctor_m1_0(__this, /*hidden argument*/NULL);
		__this->___m_UserName_0 = _stringLiteral2833;
		__this->___m_ID_1 = _stringLiteral140;
		__this->___m_IsFriend_2 = 0;
		__this->___m_State_3 = 3;
		Texture2D_t6_33 * L_0 = (Texture2D_t6_33 *)il2cpp_codegen_object_new (Texture2D_t6_33_il2cpp_TypeInfo_var);
		Texture2D__ctor_m6_130(L_0, ((int32_t)32), ((int32_t)32), /*hidden argument*/NULL);
		__this->___m_Image_4 = L_0;
		return;
	}
}
// System.Void UnityEngine.SocialPlatforms.Impl.UserProfile::.ctor(System.String,System.String,System.Boolean,UnityEngine.SocialPlatforms.UserState,UnityEngine.Texture2D)
extern "C" void UserProfile__ctor_m6_1431 (UserProfile_t6_213 * __this, String_t* ___name, String_t* ___id, bool ___friend, int32_t ___state, Texture2D_t6_33 * ___image, const MethodInfo* method)
{
	{
		Object__ctor_m1_0(__this, /*hidden argument*/NULL);
		String_t* L_0 = ___name;
		__this->___m_UserName_0 = L_0;
		String_t* L_1 = ___id;
		__this->___m_ID_1 = L_1;
		bool L_2 = ___friend;
		__this->___m_IsFriend_2 = L_2;
		int32_t L_3 = ___state;
		__this->___m_State_3 = L_3;
		Texture2D_t6_33 * L_4 = ___image;
		__this->___m_Image_4 = L_4;
		return;
	}
}
// System.String UnityEngine.SocialPlatforms.Impl.UserProfile::ToString()
extern TypeInfo* ObjectU5BU5D_t1_157_il2cpp_TypeInfo_var;
extern TypeInfo* Boolean_t1_20_il2cpp_TypeInfo_var;
extern TypeInfo* UserState_t6_225_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2834;
extern "C" String_t* UserProfile_ToString_m6_1432 (UserProfile_t6_213 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ObjectU5BU5D_t1_157_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(19);
		Boolean_t1_20_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(49);
		UserState_t6_225_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1026);
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(11);
		_stringLiteral2834 = il2cpp_codegen_string_literal_from_index(2834);
		s_Il2CppMethodIntialized = true;
	}
	{
		ObjectU5BU5D_t1_157* L_0 = ((ObjectU5BU5D_t1_157*)SZArrayNew(ObjectU5BU5D_t1_157_il2cpp_TypeInfo_var, 7));
		String_t* L_1 = (String_t*)VirtFuncInvoker0< String_t* >::Invoke(5 /* System.String UnityEngine.SocialPlatforms.Impl.UserProfile::get_id() */, __this);
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, 0);
		ArrayElementTypeCheck (L_0, L_1);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_0, 0, sizeof(Object_t *))) = (Object_t *)L_1;
		ObjectU5BU5D_t1_157* L_2 = L_0;
		NullCheck(L_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_2, 1);
		ArrayElementTypeCheck (L_2, _stringLiteral2834);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_2, 1, sizeof(Object_t *))) = (Object_t *)_stringLiteral2834;
		ObjectU5BU5D_t1_157* L_3 = L_2;
		String_t* L_4 = (String_t*)VirtFuncInvoker0< String_t* >::Invoke(4 /* System.String UnityEngine.SocialPlatforms.Impl.UserProfile::get_userName() */, __this);
		NullCheck(L_3);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_3, 2);
		ArrayElementTypeCheck (L_3, L_4);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_3, 2, sizeof(Object_t *))) = (Object_t *)L_4;
		ObjectU5BU5D_t1_157* L_5 = L_3;
		NullCheck(L_5);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_5, 3);
		ArrayElementTypeCheck (L_5, _stringLiteral2834);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_5, 3, sizeof(Object_t *))) = (Object_t *)_stringLiteral2834;
		ObjectU5BU5D_t1_157* L_6 = L_5;
		bool L_7 = (bool)VirtFuncInvoker0< bool >::Invoke(6 /* System.Boolean UnityEngine.SocialPlatforms.Impl.UserProfile::get_isFriend() */, __this);
		bool L_8 = L_7;
		Object_t * L_9 = Box(Boolean_t1_20_il2cpp_TypeInfo_var, &L_8);
		NullCheck(L_6);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_6, 4);
		ArrayElementTypeCheck (L_6, L_9);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_6, 4, sizeof(Object_t *))) = (Object_t *)L_9;
		ObjectU5BU5D_t1_157* L_10 = L_6;
		NullCheck(L_10);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_10, 5);
		ArrayElementTypeCheck (L_10, _stringLiteral2834);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_10, 5, sizeof(Object_t *))) = (Object_t *)_stringLiteral2834;
		ObjectU5BU5D_t1_157* L_11 = L_10;
		int32_t L_12 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(7 /* UnityEngine.SocialPlatforms.UserState UnityEngine.SocialPlatforms.Impl.UserProfile::get_state() */, __this);
		int32_t L_13 = L_12;
		Object_t * L_14 = Box(UserState_t6_225_il2cpp_TypeInfo_var, &L_13);
		NullCheck(L_11);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_11, 6);
		ArrayElementTypeCheck (L_11, L_14);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_11, 6, sizeof(Object_t *))) = (Object_t *)L_14;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_15 = String_Concat_m1_421(NULL /*static, unused*/, L_11, /*hidden argument*/NULL);
		return L_15;
	}
}
// System.Void UnityEngine.SocialPlatforms.Impl.UserProfile::SetUserName(System.String)
extern "C" void UserProfile_SetUserName_m6_1433 (UserProfile_t6_213 * __this, String_t* ___name, const MethodInfo* method)
{
	{
		String_t* L_0 = ___name;
		__this->___m_UserName_0 = L_0;
		return;
	}
}
// System.Void UnityEngine.SocialPlatforms.Impl.UserProfile::SetUserID(System.String)
extern "C" void UserProfile_SetUserID_m6_1434 (UserProfile_t6_213 * __this, String_t* ___id, const MethodInfo* method)
{
	{
		String_t* L_0 = ___id;
		__this->___m_ID_1 = L_0;
		return;
	}
}
// System.Void UnityEngine.SocialPlatforms.Impl.UserProfile::SetImage(UnityEngine.Texture2D)
extern "C" void UserProfile_SetImage_m6_1435 (UserProfile_t6_213 * __this, Texture2D_t6_33 * ___image, const MethodInfo* method)
{
	{
		Texture2D_t6_33 * L_0 = ___image;
		__this->___m_Image_4 = L_0;
		return;
	}
}
// System.String UnityEngine.SocialPlatforms.Impl.UserProfile::get_userName()
extern "C" String_t* UserProfile_get_userName_m6_1436 (UserProfile_t6_213 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = (__this->___m_UserName_0);
		return L_0;
	}
}
// System.String UnityEngine.SocialPlatforms.Impl.UserProfile::get_id()
extern "C" String_t* UserProfile_get_id_m6_1437 (UserProfile_t6_213 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = (__this->___m_ID_1);
		return L_0;
	}
}
// System.Boolean UnityEngine.SocialPlatforms.Impl.UserProfile::get_isFriend()
extern "C" bool UserProfile_get_isFriend_m6_1438 (UserProfile_t6_213 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (__this->___m_IsFriend_2);
		return L_0;
	}
}
// UnityEngine.SocialPlatforms.UserState UnityEngine.SocialPlatforms.Impl.UserProfile::get_state()
extern "C" int32_t UserProfile_get_state_m6_1439 (UserProfile_t6_213 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (__this->___m_State_3);
		return L_0;
	}
}
// System.Void UnityEngine.SocialPlatforms.Impl.Achievement::.ctor(System.String,System.Double,System.Boolean,System.Boolean,System.DateTime)
extern "C" void Achievement__ctor_m6_1440 (Achievement_t6_215 * __this, String_t* ___id, double ___percentCompleted, bool ___completed, bool ___hidden, DateTime_t1_127  ___lastReportedDate, const MethodInfo* method)
{
	{
		Object__ctor_m1_0(__this, /*hidden argument*/NULL);
		String_t* L_0 = ___id;
		VirtActionInvoker1< String_t* >::Invoke(5 /* System.Void UnityEngine.SocialPlatforms.Impl.Achievement::set_id(System.String) */, __this, L_0);
		double L_1 = ___percentCompleted;
		VirtActionInvoker1< double >::Invoke(7 /* System.Void UnityEngine.SocialPlatforms.Impl.Achievement::set_percentCompleted(System.Double) */, __this, L_1);
		bool L_2 = ___completed;
		__this->___m_Completed_0 = L_2;
		bool L_3 = ___hidden;
		__this->___m_Hidden_1 = L_3;
		DateTime_t1_127  L_4 = ___lastReportedDate;
		__this->___m_LastReportedDate_2 = L_4;
		return;
	}
}
// System.Void UnityEngine.SocialPlatforms.Impl.Achievement::.ctor(System.String,System.Double)
extern TypeInfo* DateTime_t1_127_il2cpp_TypeInfo_var;
extern "C" void Achievement__ctor_m6_1441 (Achievement_t6_215 * __this, String_t* ___id, double ___percent, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DateTime_t1_127_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(172);
		s_Il2CppMethodIntialized = true;
	}
	{
		Object__ctor_m1_0(__this, /*hidden argument*/NULL);
		String_t* L_0 = ___id;
		VirtActionInvoker1< String_t* >::Invoke(5 /* System.Void UnityEngine.SocialPlatforms.Impl.Achievement::set_id(System.String) */, __this, L_0);
		double L_1 = ___percent;
		VirtActionInvoker1< double >::Invoke(7 /* System.Void UnityEngine.SocialPlatforms.Impl.Achievement::set_percentCompleted(System.Double) */, __this, L_1);
		__this->___m_Hidden_1 = 0;
		__this->___m_Completed_0 = 0;
		IL2CPP_RUNTIME_CLASS_INIT(DateTime_t1_127_il2cpp_TypeInfo_var);
		DateTime_t1_127  L_2 = ((DateTime_t1_127_StaticFields*)DateTime_t1_127_il2cpp_TypeInfo_var->static_fields)->___MinValue_13;
		__this->___m_LastReportedDate_2 = L_2;
		return;
	}
}
// System.Void UnityEngine.SocialPlatforms.Impl.Achievement::.ctor()
extern Il2CppCodeGenString* _stringLiteral2447;
extern "C" void Achievement__ctor_m6_1442 (Achievement_t6_215 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		_stringLiteral2447 = il2cpp_codegen_string_literal_from_index(2447);
		s_Il2CppMethodIntialized = true;
	}
	{
		Achievement__ctor_m6_1441(__this, _stringLiteral2447, (0.0), /*hidden argument*/NULL);
		return;
	}
}
// System.String UnityEngine.SocialPlatforms.Impl.Achievement::ToString()
extern TypeInfo* ObjectU5BU5D_t1_157_il2cpp_TypeInfo_var;
extern TypeInfo* Double_t1_18_il2cpp_TypeInfo_var;
extern TypeInfo* Boolean_t1_20_il2cpp_TypeInfo_var;
extern TypeInfo* DateTime_t1_127_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2834;
extern "C" String_t* Achievement_ToString_m6_1443 (Achievement_t6_215 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ObjectU5BU5D_t1_157_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(19);
		Double_t1_18_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(44);
		Boolean_t1_20_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(49);
		DateTime_t1_127_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(172);
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(11);
		_stringLiteral2834 = il2cpp_codegen_string_literal_from_index(2834);
		s_Il2CppMethodIntialized = true;
	}
	{
		ObjectU5BU5D_t1_157* L_0 = ((ObjectU5BU5D_t1_157*)SZArrayNew(ObjectU5BU5D_t1_157_il2cpp_TypeInfo_var, ((int32_t)9)));
		String_t* L_1 = (String_t*)VirtFuncInvoker0< String_t* >::Invoke(4 /* System.String UnityEngine.SocialPlatforms.Impl.Achievement::get_id() */, __this);
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, 0);
		ArrayElementTypeCheck (L_0, L_1);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_0, 0, sizeof(Object_t *))) = (Object_t *)L_1;
		ObjectU5BU5D_t1_157* L_2 = L_0;
		NullCheck(L_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_2, 1);
		ArrayElementTypeCheck (L_2, _stringLiteral2834);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_2, 1, sizeof(Object_t *))) = (Object_t *)_stringLiteral2834;
		ObjectU5BU5D_t1_157* L_3 = L_2;
		double L_4 = (double)VirtFuncInvoker0< double >::Invoke(6 /* System.Double UnityEngine.SocialPlatforms.Impl.Achievement::get_percentCompleted() */, __this);
		double L_5 = L_4;
		Object_t * L_6 = Box(Double_t1_18_il2cpp_TypeInfo_var, &L_5);
		NullCheck(L_3);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_3, 2);
		ArrayElementTypeCheck (L_3, L_6);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_3, 2, sizeof(Object_t *))) = (Object_t *)L_6;
		ObjectU5BU5D_t1_157* L_7 = L_3;
		NullCheck(L_7);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_7, 3);
		ArrayElementTypeCheck (L_7, _stringLiteral2834);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_7, 3, sizeof(Object_t *))) = (Object_t *)_stringLiteral2834;
		ObjectU5BU5D_t1_157* L_8 = L_7;
		bool L_9 = (bool)VirtFuncInvoker0< bool >::Invoke(8 /* System.Boolean UnityEngine.SocialPlatforms.Impl.Achievement::get_completed() */, __this);
		bool L_10 = L_9;
		Object_t * L_11 = Box(Boolean_t1_20_il2cpp_TypeInfo_var, &L_10);
		NullCheck(L_8);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_8, 4);
		ArrayElementTypeCheck (L_8, L_11);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_8, 4, sizeof(Object_t *))) = (Object_t *)L_11;
		ObjectU5BU5D_t1_157* L_12 = L_8;
		NullCheck(L_12);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_12, 5);
		ArrayElementTypeCheck (L_12, _stringLiteral2834);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_12, 5, sizeof(Object_t *))) = (Object_t *)_stringLiteral2834;
		ObjectU5BU5D_t1_157* L_13 = L_12;
		bool L_14 = (bool)VirtFuncInvoker0< bool >::Invoke(9 /* System.Boolean UnityEngine.SocialPlatforms.Impl.Achievement::get_hidden() */, __this);
		bool L_15 = L_14;
		Object_t * L_16 = Box(Boolean_t1_20_il2cpp_TypeInfo_var, &L_15);
		NullCheck(L_13);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_13, 6);
		ArrayElementTypeCheck (L_13, L_16);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_13, 6, sizeof(Object_t *))) = (Object_t *)L_16;
		ObjectU5BU5D_t1_157* L_17 = L_13;
		NullCheck(L_17);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_17, 7);
		ArrayElementTypeCheck (L_17, _stringLiteral2834);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_17, 7, sizeof(Object_t *))) = (Object_t *)_stringLiteral2834;
		ObjectU5BU5D_t1_157* L_18 = L_17;
		DateTime_t1_127  L_19 = (DateTime_t1_127 )VirtFuncInvoker0< DateTime_t1_127  >::Invoke(10 /* System.DateTime UnityEngine.SocialPlatforms.Impl.Achievement::get_lastReportedDate() */, __this);
		DateTime_t1_127  L_20 = L_19;
		Object_t * L_21 = Box(DateTime_t1_127_il2cpp_TypeInfo_var, &L_20);
		NullCheck(L_18);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_18, 8);
		ArrayElementTypeCheck (L_18, L_21);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_18, 8, sizeof(Object_t *))) = (Object_t *)L_21;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_22 = String_Concat_m1_421(NULL /*static, unused*/, L_18, /*hidden argument*/NULL);
		return L_22;
	}
}
// System.String UnityEngine.SocialPlatforms.Impl.Achievement::get_id()
extern "C" String_t* Achievement_get_id_m6_1444 (Achievement_t6_215 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = (__this->___U3CidU3Ek__BackingField_3);
		return L_0;
	}
}
// System.Void UnityEngine.SocialPlatforms.Impl.Achievement::set_id(System.String)
extern "C" void Achievement_set_id_m6_1445 (Achievement_t6_215 * __this, String_t* ___value, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value;
		__this->___U3CidU3Ek__BackingField_3 = L_0;
		return;
	}
}
// System.Double UnityEngine.SocialPlatforms.Impl.Achievement::get_percentCompleted()
extern "C" double Achievement_get_percentCompleted_m6_1446 (Achievement_t6_215 * __this, const MethodInfo* method)
{
	{
		double L_0 = (__this->___U3CpercentCompletedU3Ek__BackingField_4);
		return L_0;
	}
}
// System.Void UnityEngine.SocialPlatforms.Impl.Achievement::set_percentCompleted(System.Double)
extern "C" void Achievement_set_percentCompleted_m6_1447 (Achievement_t6_215 * __this, double ___value, const MethodInfo* method)
{
	{
		double L_0 = ___value;
		__this->___U3CpercentCompletedU3Ek__BackingField_4 = L_0;
		return;
	}
}
// System.Boolean UnityEngine.SocialPlatforms.Impl.Achievement::get_completed()
extern "C" bool Achievement_get_completed_m6_1448 (Achievement_t6_215 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (__this->___m_Completed_0);
		return L_0;
	}
}
// System.Boolean UnityEngine.SocialPlatforms.Impl.Achievement::get_hidden()
extern "C" bool Achievement_get_hidden_m6_1449 (Achievement_t6_215 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (__this->___m_Hidden_1);
		return L_0;
	}
}
// System.DateTime UnityEngine.SocialPlatforms.Impl.Achievement::get_lastReportedDate()
extern "C" DateTime_t1_127  Achievement_get_lastReportedDate_m6_1450 (Achievement_t6_215 * __this, const MethodInfo* method)
{
	{
		DateTime_t1_127  L_0 = (__this->___m_LastReportedDate_2);
		return L_0;
	}
}
// System.Void UnityEngine.SocialPlatforms.Impl.AchievementDescription::.ctor(System.String,System.String,UnityEngine.Texture2D,System.String,System.String,System.Boolean,System.Int32)
extern "C" void AchievementDescription__ctor_m6_1451 (AchievementDescription_t6_216 * __this, String_t* ___id, String_t* ___title, Texture2D_t6_33 * ___image, String_t* ___achievedDescription, String_t* ___unachievedDescription, bool ___hidden, int32_t ___points, const MethodInfo* method)
{
	{
		Object__ctor_m1_0(__this, /*hidden argument*/NULL);
		String_t* L_0 = ___id;
		VirtActionInvoker1< String_t* >::Invoke(5 /* System.Void UnityEngine.SocialPlatforms.Impl.AchievementDescription::set_id(System.String) */, __this, L_0);
		String_t* L_1 = ___title;
		__this->___m_Title_0 = L_1;
		Texture2D_t6_33 * L_2 = ___image;
		__this->___m_Image_1 = L_2;
		String_t* L_3 = ___achievedDescription;
		__this->___m_AchievedDescription_2 = L_3;
		String_t* L_4 = ___unachievedDescription;
		__this->___m_UnachievedDescription_3 = L_4;
		bool L_5 = ___hidden;
		__this->___m_Hidden_4 = L_5;
		int32_t L_6 = ___points;
		__this->___m_Points_5 = L_6;
		return;
	}
}
// System.String UnityEngine.SocialPlatforms.Impl.AchievementDescription::ToString()
extern TypeInfo* ObjectU5BU5D_t1_157_il2cpp_TypeInfo_var;
extern TypeInfo* Int32_t1_3_il2cpp_TypeInfo_var;
extern TypeInfo* Boolean_t1_20_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2834;
extern "C" String_t* AchievementDescription_ToString_m6_1452 (AchievementDescription_t6_216 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ObjectU5BU5D_t1_157_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(19);
		Int32_t1_3_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(8);
		Boolean_t1_20_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(49);
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(11);
		_stringLiteral2834 = il2cpp_codegen_string_literal_from_index(2834);
		s_Il2CppMethodIntialized = true;
	}
	{
		ObjectU5BU5D_t1_157* L_0 = ((ObjectU5BU5D_t1_157*)SZArrayNew(ObjectU5BU5D_t1_157_il2cpp_TypeInfo_var, ((int32_t)11)));
		String_t* L_1 = (String_t*)VirtFuncInvoker0< String_t* >::Invoke(4 /* System.String UnityEngine.SocialPlatforms.Impl.AchievementDescription::get_id() */, __this);
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, 0);
		ArrayElementTypeCheck (L_0, L_1);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_0, 0, sizeof(Object_t *))) = (Object_t *)L_1;
		ObjectU5BU5D_t1_157* L_2 = L_0;
		NullCheck(L_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_2, 1);
		ArrayElementTypeCheck (L_2, _stringLiteral2834);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_2, 1, sizeof(Object_t *))) = (Object_t *)_stringLiteral2834;
		ObjectU5BU5D_t1_157* L_3 = L_2;
		String_t* L_4 = (String_t*)VirtFuncInvoker0< String_t* >::Invoke(6 /* System.String UnityEngine.SocialPlatforms.Impl.AchievementDescription::get_title() */, __this);
		NullCheck(L_3);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_3, 2);
		ArrayElementTypeCheck (L_3, L_4);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_3, 2, sizeof(Object_t *))) = (Object_t *)L_4;
		ObjectU5BU5D_t1_157* L_5 = L_3;
		NullCheck(L_5);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_5, 3);
		ArrayElementTypeCheck (L_5, _stringLiteral2834);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_5, 3, sizeof(Object_t *))) = (Object_t *)_stringLiteral2834;
		ObjectU5BU5D_t1_157* L_6 = L_5;
		String_t* L_7 = (String_t*)VirtFuncInvoker0< String_t* >::Invoke(7 /* System.String UnityEngine.SocialPlatforms.Impl.AchievementDescription::get_achievedDescription() */, __this);
		NullCheck(L_6);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_6, 4);
		ArrayElementTypeCheck (L_6, L_7);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_6, 4, sizeof(Object_t *))) = (Object_t *)L_7;
		ObjectU5BU5D_t1_157* L_8 = L_6;
		NullCheck(L_8);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_8, 5);
		ArrayElementTypeCheck (L_8, _stringLiteral2834);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_8, 5, sizeof(Object_t *))) = (Object_t *)_stringLiteral2834;
		ObjectU5BU5D_t1_157* L_9 = L_8;
		String_t* L_10 = (String_t*)VirtFuncInvoker0< String_t* >::Invoke(8 /* System.String UnityEngine.SocialPlatforms.Impl.AchievementDescription::get_unachievedDescription() */, __this);
		NullCheck(L_9);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_9, 6);
		ArrayElementTypeCheck (L_9, L_10);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_9, 6, sizeof(Object_t *))) = (Object_t *)L_10;
		ObjectU5BU5D_t1_157* L_11 = L_9;
		NullCheck(L_11);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_11, 7);
		ArrayElementTypeCheck (L_11, _stringLiteral2834);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_11, 7, sizeof(Object_t *))) = (Object_t *)_stringLiteral2834;
		ObjectU5BU5D_t1_157* L_12 = L_11;
		int32_t L_13 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(10 /* System.Int32 UnityEngine.SocialPlatforms.Impl.AchievementDescription::get_points() */, __this);
		int32_t L_14 = L_13;
		Object_t * L_15 = Box(Int32_t1_3_il2cpp_TypeInfo_var, &L_14);
		NullCheck(L_12);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_12, 8);
		ArrayElementTypeCheck (L_12, L_15);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_12, 8, sizeof(Object_t *))) = (Object_t *)L_15;
		ObjectU5BU5D_t1_157* L_16 = L_12;
		NullCheck(L_16);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_16, ((int32_t)9));
		ArrayElementTypeCheck (L_16, _stringLiteral2834);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_16, ((int32_t)9), sizeof(Object_t *))) = (Object_t *)_stringLiteral2834;
		ObjectU5BU5D_t1_157* L_17 = L_16;
		bool L_18 = (bool)VirtFuncInvoker0< bool >::Invoke(9 /* System.Boolean UnityEngine.SocialPlatforms.Impl.AchievementDescription::get_hidden() */, __this);
		bool L_19 = L_18;
		Object_t * L_20 = Box(Boolean_t1_20_il2cpp_TypeInfo_var, &L_19);
		NullCheck(L_17);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_17, ((int32_t)10));
		ArrayElementTypeCheck (L_17, L_20);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_17, ((int32_t)10), sizeof(Object_t *))) = (Object_t *)L_20;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_21 = String_Concat_m1_421(NULL /*static, unused*/, L_17, /*hidden argument*/NULL);
		return L_21;
	}
}
// System.Void UnityEngine.SocialPlatforms.Impl.AchievementDescription::SetImage(UnityEngine.Texture2D)
extern "C" void AchievementDescription_SetImage_m6_1453 (AchievementDescription_t6_216 * __this, Texture2D_t6_33 * ___image, const MethodInfo* method)
{
	{
		Texture2D_t6_33 * L_0 = ___image;
		__this->___m_Image_1 = L_0;
		return;
	}
}
// System.String UnityEngine.SocialPlatforms.Impl.AchievementDescription::get_id()
extern "C" String_t* AchievementDescription_get_id_m6_1454 (AchievementDescription_t6_216 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = (__this->___U3CidU3Ek__BackingField_6);
		return L_0;
	}
}
// System.Void UnityEngine.SocialPlatforms.Impl.AchievementDescription::set_id(System.String)
extern "C" void AchievementDescription_set_id_m6_1455 (AchievementDescription_t6_216 * __this, String_t* ___value, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value;
		__this->___U3CidU3Ek__BackingField_6 = L_0;
		return;
	}
}
// System.String UnityEngine.SocialPlatforms.Impl.AchievementDescription::get_title()
extern "C" String_t* AchievementDescription_get_title_m6_1456 (AchievementDescription_t6_216 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = (__this->___m_Title_0);
		return L_0;
	}
}
// System.String UnityEngine.SocialPlatforms.Impl.AchievementDescription::get_achievedDescription()
extern "C" String_t* AchievementDescription_get_achievedDescription_m6_1457 (AchievementDescription_t6_216 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = (__this->___m_AchievedDescription_2);
		return L_0;
	}
}
// System.String UnityEngine.SocialPlatforms.Impl.AchievementDescription::get_unachievedDescription()
extern "C" String_t* AchievementDescription_get_unachievedDescription_m6_1458 (AchievementDescription_t6_216 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = (__this->___m_UnachievedDescription_3);
		return L_0;
	}
}
// System.Boolean UnityEngine.SocialPlatforms.Impl.AchievementDescription::get_hidden()
extern "C" bool AchievementDescription_get_hidden_m6_1459 (AchievementDescription_t6_216 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (__this->___m_Hidden_4);
		return L_0;
	}
}
// System.Int32 UnityEngine.SocialPlatforms.Impl.AchievementDescription::get_points()
extern "C" int32_t AchievementDescription_get_points_m6_1460 (AchievementDescription_t6_216 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (__this->___m_Points_5);
		return L_0;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
