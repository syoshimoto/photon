﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// OnClickRightDestroy
struct OnClickRightDestroy_t8_13;

#include "codegen/il2cpp-codegen.h"

// System.Void OnClickRightDestroy::.ctor()
extern "C" void OnClickRightDestroy__ctor_m8_31 (OnClickRightDestroy_t8_13 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OnClickRightDestroy::OnPressRight()
extern "C" void OnClickRightDestroy_OnPressRight_m8_32 (OnClickRightDestroy_t8_13 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
