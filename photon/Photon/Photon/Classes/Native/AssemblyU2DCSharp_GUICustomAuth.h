﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// UnityEngine.GameObject
struct GameObject_t6_85;

#include "UnityEngine_UnityEngine_MonoBehaviour.h"
#include "UnityEngine_UnityEngine_Rect.h"
#include "AssemblyU2DCSharp_GUICustomAuth_GuiState.h"

// GUICustomAuth
struct  GUICustomAuth_t8_17  : public MonoBehaviour_t6_80
{
	// UnityEngine.Rect GUICustomAuth::GuiRect
	Rect_t6_52  ___GuiRect_2;
	// System.String GUICustomAuth::authName
	String_t* ___authName_3;
	// System.String GUICustomAuth::authToken
	String_t* ___authToken_4;
	// System.String GUICustomAuth::authDebugMessage
	String_t* ___authDebugMessage_5;
	// GUICustomAuth/GuiState GUICustomAuth::guiState
	int32_t ___guiState_6;
	// UnityEngine.GameObject GUICustomAuth::RootOf3dButtons
	GameObject_t6_85 * ___RootOf3dButtons_7;
};
