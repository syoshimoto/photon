﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// RPGCamera
struct RPGCamera_t8_37;

#include "codegen/il2cpp-codegen.h"

// System.Void RPGCamera::.ctor()
extern "C" void RPGCamera__ctor_m8_143 (RPGCamera_t8_37 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void RPGCamera::Start()
extern "C" void RPGCamera_Start_m8_144 (RPGCamera_t8_37 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void RPGCamera::LateUpdate()
extern "C" void RPGCamera_LateUpdate_m8_145 (RPGCamera_t8_37 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void RPGCamera::UpdateDistance()
extern "C" void RPGCamera_UpdateDistance_m8_146 (RPGCamera_t8_37 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void RPGCamera::UpdateZoom()
extern "C" void RPGCamera_UpdateZoom_m8_147 (RPGCamera_t8_37 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void RPGCamera::UpdatePosition()
extern "C" void RPGCamera_UpdatePosition_m8_148 (RPGCamera_t8_37 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void RPGCamera::UpdateRotation()
extern "C" void RPGCamera_UpdateRotation_m8_149 (RPGCamera_t8_37 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
