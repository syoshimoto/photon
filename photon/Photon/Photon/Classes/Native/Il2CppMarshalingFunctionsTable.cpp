﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif



#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"

extern "C" void Context_t1_79_marshal ();
extern "C" void Context_t1_79_marshal_back ();
extern "C" void Context_t1_79_marshal_cleanup ();
extern "C" void PreviousInfo_t1_80_marshal ();
extern "C" void PreviousInfo_t1_80_marshal_back ();
extern "C" void PreviousInfo_t1_80_marshal_cleanup ();
extern "C" void Escape_t1_81_marshal ();
extern "C" void Escape_t1_81_marshal_back ();
extern "C" void Escape_t1_81_marshal_cleanup ();
extern "C" void MonoIOStat_t1_243_marshal ();
extern "C" void MonoIOStat_t1_243_marshal_back ();
extern "C" void MonoIOStat_t1_243_marshal_cleanup ();
extern "C" void ParameterModifier_t1_352_marshal ();
extern "C" void ParameterModifier_t1_352_marshal_back ();
extern "C" void ParameterModifier_t1_352_marshal_cleanup ();
extern "C" void ResourceInfo_t1_363_marshal ();
extern "C" void ResourceInfo_t1_363_marshal_back ();
extern "C" void ResourceInfo_t1_363_marshal_cleanup ();
extern "C" void DSAParameters_t1_563_marshal ();
extern "C" void DSAParameters_t1_563_marshal_back ();
extern "C" void DSAParameters_t1_563_marshal_cleanup ();
extern "C" void RSAParameters_t1_592_marshal ();
extern "C" void RSAParameters_t1_592_marshal_back ();
extern "C" void RSAParameters_t1_592_marshal_cleanup ();
extern "C" void X509ChainStatus_t3_87_marshal ();
extern "C" void X509ChainStatus_t3_87_marshal_back ();
extern "C" void X509ChainStatus_t3_87_marshal_cleanup ();
extern "C" void IntStack_t3_135_marshal ();
extern "C" void IntStack_t3_135_marshal_back ();
extern "C" void IntStack_t3_135_marshal_cleanup ();
extern "C" void Interval_t3_141_marshal ();
extern "C" void Interval_t3_141_marshal_back ();
extern "C" void Interval_t3_141_marshal_cleanup ();
extern "C" void UriScheme_t3_169_marshal ();
extern "C" void UriScheme_t3_169_marshal_back ();
extern "C" void UriScheme_t3_169_marshal_cleanup ();
extern "C" void WaitForSeconds_t6_11_marshal ();
extern "C" void WaitForSeconds_t6_11_marshal_back ();
extern "C" void WaitForSeconds_t6_11_marshal_cleanup ();
extern "C" void Coroutine_t6_15_marshal ();
extern "C" void Coroutine_t6_15_marshal_back ();
extern "C" void Coroutine_t6_15_marshal_cleanup ();
extern "C" void ScriptableObject_t6_16_marshal ();
extern "C" void ScriptableObject_t6_16_marshal_back ();
extern "C" void ScriptableObject_t6_16_marshal_cleanup ();
extern "C" void Gradient_t6_42_marshal ();
extern "C" void Gradient_t6_42_marshal_back ();
extern "C" void Gradient_t6_42_marshal_cleanup ();
extern "C" void CacheIndex_t6_70_marshal ();
extern "C" void CacheIndex_t6_70_marshal_back ();
extern "C" void CacheIndex_t6_70_marshal_cleanup ();
extern "C" void AsyncOperation_t6_2_marshal ();
extern "C" void AsyncOperation_t6_2_marshal_back ();
extern "C" void AsyncOperation_t6_2_marshal_cleanup ();
extern "C" void Touch_t6_82_marshal ();
extern "C" void Touch_t6_82_marshal_back ();
extern "C" void Touch_t6_82_marshal_cleanup ();
extern "C" void Object_t6_5_marshal ();
extern "C" void Object_t6_5_marshal_back ();
extern "C" void Object_t6_5_marshal_cleanup ();
extern "C" void YieldInstruction_t6_12_marshal ();
extern "C" void YieldInstruction_t6_12_marshal_back ();
extern "C" void YieldInstruction_t6_12_marshal_cleanup ();
extern "C" void WebCamDevice_t6_123_marshal ();
extern "C" void WebCamDevice_t6_123_marshal_back ();
extern "C" void WebCamDevice_t6_123_marshal_cleanup ();
extern "C" void AnimationCurve_t6_132_marshal ();
extern "C" void AnimationCurve_t6_132_marshal_back ();
extern "C" void AnimationCurve_t6_132_marshal_cleanup ();
extern "C" void AnimatorTransitionInfo_t6_137_marshal ();
extern "C" void AnimatorTransitionInfo_t6_137_marshal_back ();
extern "C" void AnimatorTransitionInfo_t6_137_marshal_cleanup ();
extern "C" void SkeletonBone_t6_139_marshal ();
extern "C" void SkeletonBone_t6_139_marshal_back ();
extern "C" void SkeletonBone_t6_139_marshal_cleanup ();
extern "C" void HumanBone_t6_141_marshal ();
extern "C" void HumanBone_t6_141_marshal_back ();
extern "C" void HumanBone_t6_141_marshal_cleanup ();
extern "C" void CharacterInfo_t6_146_marshal ();
extern "C" void CharacterInfo_t6_146_marshal_back ();
extern "C" void CharacterInfo_t6_146_marshal_cleanup ();
extern "C" void Event_t6_154_marshal ();
extern "C" void Event_t6_154_marshal_back ();
extern "C" void Event_t6_154_marshal_cleanup ();
extern "C" void GcAchievementData_t6_205_marshal ();
extern "C" void GcAchievementData_t6_205_marshal_back ();
extern "C" void GcAchievementData_t6_205_marshal_cleanup ();
extern "C" void GcScoreData_t6_206_marshal ();
extern "C" void GcScoreData_t6_206_marshal_back ();
extern "C" void GcScoreData_t6_206_marshal_cleanup ();
extern "C" void TrackedReference_t6_136_marshal ();
extern "C" void TrackedReference_t6_136_marshal_back ();
extern "C" void TrackedReference_t6_136_marshal_cleanup ();
extern "C" void DemoBtn_t8_21_marshal ();
extern "C" void DemoBtn_t8_21_marshal_back ();
extern "C" void DemoBtn_t8_21_marshal_cleanup ();
extern const Il2CppMarshalingFunctions g_MarshalingFunctions[33] = 
{
	{ Context_t1_79_marshal, Context_t1_79_marshal_back, Context_t1_79_marshal_cleanup },
	{ PreviousInfo_t1_80_marshal, PreviousInfo_t1_80_marshal_back, PreviousInfo_t1_80_marshal_cleanup },
	{ Escape_t1_81_marshal, Escape_t1_81_marshal_back, Escape_t1_81_marshal_cleanup },
	{ MonoIOStat_t1_243_marshal, MonoIOStat_t1_243_marshal_back, MonoIOStat_t1_243_marshal_cleanup },
	{ ParameterModifier_t1_352_marshal, ParameterModifier_t1_352_marshal_back, ParameterModifier_t1_352_marshal_cleanup },
	{ ResourceInfo_t1_363_marshal, ResourceInfo_t1_363_marshal_back, ResourceInfo_t1_363_marshal_cleanup },
	{ DSAParameters_t1_563_marshal, DSAParameters_t1_563_marshal_back, DSAParameters_t1_563_marshal_cleanup },
	{ RSAParameters_t1_592_marshal, RSAParameters_t1_592_marshal_back, RSAParameters_t1_592_marshal_cleanup },
	{ X509ChainStatus_t3_87_marshal, X509ChainStatus_t3_87_marshal_back, X509ChainStatus_t3_87_marshal_cleanup },
	{ IntStack_t3_135_marshal, IntStack_t3_135_marshal_back, IntStack_t3_135_marshal_cleanup },
	{ Interval_t3_141_marshal, Interval_t3_141_marshal_back, Interval_t3_141_marshal_cleanup },
	{ UriScheme_t3_169_marshal, UriScheme_t3_169_marshal_back, UriScheme_t3_169_marshal_cleanup },
	{ WaitForSeconds_t6_11_marshal, WaitForSeconds_t6_11_marshal_back, WaitForSeconds_t6_11_marshal_cleanup },
	{ Coroutine_t6_15_marshal, Coroutine_t6_15_marshal_back, Coroutine_t6_15_marshal_cleanup },
	{ ScriptableObject_t6_16_marshal, ScriptableObject_t6_16_marshal_back, ScriptableObject_t6_16_marshal_cleanup },
	{ Gradient_t6_42_marshal, Gradient_t6_42_marshal_back, Gradient_t6_42_marshal_cleanup },
	{ CacheIndex_t6_70_marshal, CacheIndex_t6_70_marshal_back, CacheIndex_t6_70_marshal_cleanup },
	{ AsyncOperation_t6_2_marshal, AsyncOperation_t6_2_marshal_back, AsyncOperation_t6_2_marshal_cleanup },
	{ Touch_t6_82_marshal, Touch_t6_82_marshal_back, Touch_t6_82_marshal_cleanup },
	{ Object_t6_5_marshal, Object_t6_5_marshal_back, Object_t6_5_marshal_cleanup },
	{ YieldInstruction_t6_12_marshal, YieldInstruction_t6_12_marshal_back, YieldInstruction_t6_12_marshal_cleanup },
	{ WebCamDevice_t6_123_marshal, WebCamDevice_t6_123_marshal_back, WebCamDevice_t6_123_marshal_cleanup },
	{ AnimationCurve_t6_132_marshal, AnimationCurve_t6_132_marshal_back, AnimationCurve_t6_132_marshal_cleanup },
	{ AnimatorTransitionInfo_t6_137_marshal, AnimatorTransitionInfo_t6_137_marshal_back, AnimatorTransitionInfo_t6_137_marshal_cleanup },
	{ SkeletonBone_t6_139_marshal, SkeletonBone_t6_139_marshal_back, SkeletonBone_t6_139_marshal_cleanup },
	{ HumanBone_t6_141_marshal, HumanBone_t6_141_marshal_back, HumanBone_t6_141_marshal_cleanup },
	{ CharacterInfo_t6_146_marshal, CharacterInfo_t6_146_marshal_back, CharacterInfo_t6_146_marshal_cleanup },
	{ Event_t6_154_marshal, Event_t6_154_marshal_back, Event_t6_154_marshal_cleanup },
	{ GcAchievementData_t6_205_marshal, GcAchievementData_t6_205_marshal_back, GcAchievementData_t6_205_marshal_cleanup },
	{ GcScoreData_t6_206_marshal, GcScoreData_t6_206_marshal_back, GcScoreData_t6_206_marshal_cleanup },
	{ TrackedReference_t6_136_marshal, TrackedReference_t6_136_marshal_back, TrackedReference_t6_136_marshal_cleanup },
	{ DemoBtn_t8_21_marshal, DemoBtn_t8_21_marshal_back, DemoBtn_t8_21_marshal_cleanup },
	NULL,
};
