﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PhotonRigidbody2DView
struct PhotonRigidbody2DView_t8_136;
// PhotonStream
struct PhotonStream_t8_106;
// PhotonMessageInfo
struct PhotonMessageInfo_t8_104;

#include "codegen/il2cpp-codegen.h"

// System.Void PhotonRigidbody2DView::.ctor()
extern "C" void PhotonRigidbody2DView__ctor_m8_887 (PhotonRigidbody2DView_t8_136 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PhotonRigidbody2DView::Awake()
extern "C" void PhotonRigidbody2DView_Awake_m8_888 (PhotonRigidbody2DView_t8_136 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PhotonRigidbody2DView::OnPhotonSerializeView(PhotonStream,PhotonMessageInfo)
extern "C" void PhotonRigidbody2DView_OnPhotonSerializeView_m8_889 (PhotonRigidbody2DView_t8_136 * __this, PhotonStream_t8_106 * ___stream, PhotonMessageInfo_t8_104 * ___info, const MethodInfo* method) IL2CPP_METHOD_ATTR;
