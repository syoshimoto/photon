﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "mscorlib_System_Object.h"
#include "AssemblyU2DCSharp_PhotonTransformViewRotationModel_Interpola.h"

// PhotonTransformViewRotationModel
struct  PhotonTransformViewRotationModel_t8_139  : public Object_t
{
	// System.Boolean PhotonTransformViewRotationModel::SynchronizeEnabled
	bool ___SynchronizeEnabled_0;
	// PhotonTransformViewRotationModel/InterpolateOptions PhotonTransformViewRotationModel::InterpolateOption
	int32_t ___InterpolateOption_1;
	// System.Single PhotonTransformViewRotationModel::InterpolateRotateTowardsSpeed
	float ___InterpolateRotateTowardsSpeed_2;
	// System.Single PhotonTransformViewRotationModel::InterpolateLerpSpeed
	float ___InterpolateLerpSpeed_3;
};
