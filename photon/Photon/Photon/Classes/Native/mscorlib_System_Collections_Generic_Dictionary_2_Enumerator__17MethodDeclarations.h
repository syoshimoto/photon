﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2<ExitGames.Client.Photon.ConnectionProtocol,System.Int32>
struct Dictionary_2_t1_933;
// System.Object
struct Object_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Enumerator__17.h"
#include "mscorlib_System_Collections_DictionaryEntry.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_17.h"
#include "Photon3Unity3D_ExitGames_Client_Photon_ConnectionProtocol.h"

// System.Void System.Collections.Generic.Dictionary`2/Enumerator<ExitGames.Client.Photon.ConnectionProtocol,System.Int32>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C" void Enumerator__ctor_m1_9438_gshared (Enumerator_t1_1299 * __this, Dictionary_2_t1_933 * ___dictionary, const MethodInfo* method);
#define Enumerator__ctor_m1_9438(__this, ___dictionary, method) (( void (*) (Enumerator_t1_1299 *, Dictionary_2_t1_933 *, const MethodInfo*))Enumerator__ctor_m1_9438_gshared)(__this, ___dictionary, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<ExitGames.Client.Photon.ConnectionProtocol,System.Int32>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * Enumerator_System_Collections_IEnumerator_get_Current_m1_9439_gshared (Enumerator_t1_1299 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m1_9439(__this, method) (( Object_t * (*) (Enumerator_t1_1299 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m1_9439_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<ExitGames.Client.Photon.ConnectionProtocol,System.Int32>::System.Collections.IEnumerator.Reset()
extern "C" void Enumerator_System_Collections_IEnumerator_Reset_m1_9440_gshared (Enumerator_t1_1299 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_Reset_m1_9440(__this, method) (( void (*) (Enumerator_t1_1299 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m1_9440_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<ExitGames.Client.Photon.ConnectionProtocol,System.Int32>::System.Collections.IDictionaryEnumerator.get_Entry()
extern "C" DictionaryEntry_t1_167  Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m1_9441_gshared (Enumerator_t1_1299 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m1_9441(__this, method) (( DictionaryEntry_t1_167  (*) (Enumerator_t1_1299 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m1_9441_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<ExitGames.Client.Photon.ConnectionProtocol,System.Int32>::System.Collections.IDictionaryEnumerator.get_Key()
extern "C" Object_t * Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m1_9442_gshared (Enumerator_t1_1299 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m1_9442(__this, method) (( Object_t * (*) (Enumerator_t1_1299 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m1_9442_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<ExitGames.Client.Photon.ConnectionProtocol,System.Int32>::System.Collections.IDictionaryEnumerator.get_Value()
extern "C" Object_t * Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m1_9443_gshared (Enumerator_t1_1299 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m1_9443(__this, method) (( Object_t * (*) (Enumerator_t1_1299 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m1_9443_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<ExitGames.Client.Photon.ConnectionProtocol,System.Int32>::MoveNext()
extern "C" bool Enumerator_MoveNext_m1_9444_gshared (Enumerator_t1_1299 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m1_9444(__this, method) (( bool (*) (Enumerator_t1_1299 *, const MethodInfo*))Enumerator_MoveNext_m1_9444_gshared)(__this, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<ExitGames.Client.Photon.ConnectionProtocol,System.Int32>::get_Current()
extern "C" KeyValuePair_2_t1_1294  Enumerator_get_Current_m1_9445_gshared (Enumerator_t1_1299 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m1_9445(__this, method) (( KeyValuePair_2_t1_1294  (*) (Enumerator_t1_1299 *, const MethodInfo*))Enumerator_get_Current_m1_9445_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/Enumerator<ExitGames.Client.Photon.ConnectionProtocol,System.Int32>::get_CurrentKey()
extern "C" uint8_t Enumerator_get_CurrentKey_m1_9446_gshared (Enumerator_t1_1299 * __this, const MethodInfo* method);
#define Enumerator_get_CurrentKey_m1_9446(__this, method) (( uint8_t (*) (Enumerator_t1_1299 *, const MethodInfo*))Enumerator_get_CurrentKey_m1_9446_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/Enumerator<ExitGames.Client.Photon.ConnectionProtocol,System.Int32>::get_CurrentValue()
extern "C" int32_t Enumerator_get_CurrentValue_m1_9447_gshared (Enumerator_t1_1299 * __this, const MethodInfo* method);
#define Enumerator_get_CurrentValue_m1_9447(__this, method) (( int32_t (*) (Enumerator_t1_1299 *, const MethodInfo*))Enumerator_get_CurrentValue_m1_9447_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<ExitGames.Client.Photon.ConnectionProtocol,System.Int32>::Reset()
extern "C" void Enumerator_Reset_m1_9448_gshared (Enumerator_t1_1299 * __this, const MethodInfo* method);
#define Enumerator_Reset_m1_9448(__this, method) (( void (*) (Enumerator_t1_1299 *, const MethodInfo*))Enumerator_Reset_m1_9448_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<ExitGames.Client.Photon.ConnectionProtocol,System.Int32>::VerifyState()
extern "C" void Enumerator_VerifyState_m1_9449_gshared (Enumerator_t1_1299 * __this, const MethodInfo* method);
#define Enumerator_VerifyState_m1_9449(__this, method) (( void (*) (Enumerator_t1_1299 *, const MethodInfo*))Enumerator_VerifyState_m1_9449_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<ExitGames.Client.Photon.ConnectionProtocol,System.Int32>::VerifyCurrent()
extern "C" void Enumerator_VerifyCurrent_m1_9450_gshared (Enumerator_t1_1299 * __this, const MethodInfo* method);
#define Enumerator_VerifyCurrent_m1_9450(__this, method) (( void (*) (Enumerator_t1_1299 *, const MethodInfo*))Enumerator_VerifyCurrent_m1_9450_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<ExitGames.Client.Photon.ConnectionProtocol,System.Int32>::Dispose()
extern "C" void Enumerator_Dispose_m1_9451_gshared (Enumerator_t1_1299 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m1_9451(__this, method) (( void (*) (Enumerator_t1_1299 *, const MethodInfo*))Enumerator_Dispose_m1_9451_gshared)(__this, method)
