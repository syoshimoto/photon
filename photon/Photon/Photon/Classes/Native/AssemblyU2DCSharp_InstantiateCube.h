﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.GameObject
struct GameObject_t6_85;

#include "UnityEngine_UnityEngine_MonoBehaviour.h"

// InstantiateCube
struct  InstantiateCube_t8_9  : public MonoBehaviour_t6_80
{
	// UnityEngine.GameObject InstantiateCube::Prefab
	GameObject_t6_85 * ___Prefab_2;
	// System.Int32 InstantiateCube::InstantiateType
	int32_t ___InstantiateType_3;
	// System.Boolean InstantiateCube::showGui
	bool ___showGui_4;
};
