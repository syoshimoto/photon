﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.AssemblyIsEditorAssembly
struct AssemblyIsEditorAssembly_t6_202;

#include "codegen/il2cpp-codegen.h"

// System.Void UnityEngine.AssemblyIsEditorAssembly::.ctor()
extern "C" void AssemblyIsEditorAssembly__ctor_m6_1410 (AssemblyIsEditorAssembly_t6_202 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
