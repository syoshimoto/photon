﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>

// PhotonHandler
struct PhotonHandler_t8_111;
// System.Collections.IEnumerator
struct IEnumerator_t1_130;
// PhotonLagSimulationGui
struct PhotonLagSimulationGui_t8_112;
// ExitGames.Client.Photon.PhotonPeer
struct PhotonPeer_t5_38;
// PhotonNetwork/EventCallback
struct EventCallback_t8_113;
// System.Object
struct Object_t;
// System.IAsyncResult
struct IAsyncResult_t1_27;
// System.AsyncCallback
struct AsyncCallback_t1_28;
// System.String
struct String_t;
// AuthenticationValues
struct AuthenticationValues_t8_97;
// Room
struct Room_t8_100;
// PhotonPlayer
struct PhotonPlayer_t8_102;
// PhotonPlayer[]
struct PhotonPlayerU5BU5D_t8_101;
// System.Collections.Generic.List`1<FriendInfo>
struct List_1_t1_946;
// IPunPrefabPool
struct IPunPrefabPool_t8_103;
// System.Collections.Generic.List`1<TypedLobbyInfo>
struct List_1_t1_934;
// TypedLobby
struct TypedLobby_t8_79;
// System.String[]
struct StringU5BU5D_t1_202;
// RoomOptions
struct RoomOptions_t8_78;
// ExitGames.Client.Photon.Hashtable
struct Hashtable_t5_1;
// RoomInfo[]
struct RoomInfoU5BU5D_t8_99;
// RaiseEventOptions
struct RaiseEventOptions_t8_93;
// System.Int32[]
struct Int32U5BU5D_t1_160;
// UnityEngine.GameObject
struct GameObject_t6_85;
// System.Object[]
struct ObjectU5BU5D_t1_157;
// PhotonView
struct PhotonView_t8_3;
// System.Type
struct Type_t;
// System.Collections.Generic.HashSet`1<UnityEngine.GameObject>
struct HashSet_1_t2_16;
// PhotonStatsGui
struct PhotonStatsGui_t8_116;
// PhotonStreamQueue
struct PhotonStreamQueue_t8_117;
// PhotonStream
struct PhotonStream_t8_106;
// PhotonMessageInfo
struct PhotonMessageInfo_t8_104;
// UnityEngine.Component
struct Component_t6_26;
// UnityEngine.MonoBehaviour[]
struct MonoBehaviourU5BU5D_t6_273;
// PhotonPingManager/<PingSocket>c__Iterator1
struct U3CPingSocketU3Ec__Iterator1_t8_122;
// PhotonPingManager
struct PhotonPingManager_t8_109;
// Region
struct Region_t8_110;
// PunRPC
struct PunRPC_t8_123;
// RoomInfo
struct RoomInfo_t8_124;
// ServerSettings
struct ServerSettings_t8_115;
// PhotonAnimatorView/SynchronizedParameter
struct SynchronizedParameter_t8_128;
// PhotonAnimatorView/SynchronizedLayer
struct SynchronizedLayer_t8_129;
// PhotonAnimatorView/<DoesLayerSynchronizeTypeExist>c__AnonStorey3
struct U3CDoesLayerSynchronizeTypeExistU3Ec__AnonStorey3_t8_130;
// PhotonAnimatorView/<DoesParameterSynchronizeTypeExist>c__AnonStorey4
struct U3CDoesParameterSynchronizeTypeExistU3Ec__AnonStorey4_t8_131;
// PhotonAnimatorView/<GetLayerSynchronizeType>c__AnonStorey5
struct U3CGetLayerSynchronizeTypeU3Ec__AnonStorey5_t8_132;
// PhotonAnimatorView/<GetParameterSynchronizeType>c__AnonStorey6
struct U3CGetParameterSynchronizeTypeU3Ec__AnonStorey6_t8_133;
// PhotonAnimatorView/<SetLayerSynchronized>c__AnonStorey7
struct U3CSetLayerSynchronizedU3Ec__AnonStorey7_t8_134;
// PhotonAnimatorView/<SetParameterSynchronized>c__AnonStorey8
struct U3CSetParameterSynchronizedU3Ec__AnonStorey8_t8_135;
// PhotonAnimatorView
struct PhotonAnimatorView_t8_27;
// UnityEngine.Animator
struct Animator_t6_138;
// System.Collections.Generic.List`1<PhotonAnimatorView/SynchronizedLayer>
struct List_1_t1_950;
// System.Collections.Generic.List`1<PhotonAnimatorView/SynchronizedParameter>
struct List_1_t1_949;
// PhotonRigidbody2DView
struct PhotonRigidbody2DView_t8_136;
// UnityEngine.Rigidbody2D
struct Rigidbody2D_t6_113;
// PhotonRigidbodyView
struct PhotonRigidbodyView_t8_137;
// UnityEngine.Rigidbody
struct Rigidbody_t6_102;
// PhotonTransformView
struct PhotonTransformView_t8_39;
// PhotonTransformViewPositionControl
struct PhotonTransformViewPositionControl_t8_141;
// PhotonTransformViewPositionModel
struct PhotonTransformViewPositionModel_t8_138;
// PhotonTransformViewRotationControl
struct PhotonTransformViewRotationControl_t8_142;
// PhotonTransformViewRotationModel
struct PhotonTransformViewRotationModel_t8_139;
// PhotonTransformViewScaleControl
struct PhotonTransformViewScaleControl_t8_143;
// PhotonTransformViewScaleModel
struct PhotonTransformViewScaleModel_t8_140;
// ConnectAndJoinRandom
struct ConnectAndJoinRandom_t8_148;
// HighlightOwnedGameObj
struct HighlightOwnedGameObj_t8_149;
// InRoomChat
struct InRoomChat_t8_150;
// InRoomRoundTimer
struct InRoomRoundTimer_t8_151;
// InputToEvent
struct InputToEvent_t8_152;
// UnityEngine.Camera
struct Camera_t6_75;
// ManualPhotonViewAllocator
struct ManualPhotonViewAllocator_t8_153;
// OnClickDestroy
struct OnClickDestroy_t8_157;
// MoveByKeys
struct MoveByKeys_t8_154;
// UnityEngine.SpriteRenderer
struct SpriteRenderer_t6_69;
// OnAwakeUsePhotonView
struct OnAwakeUsePhotonView_t8_155;
// OnClickDestroy/<DestroyRpc>c__Iterator2
struct U3CDestroyRpcU3Ec__Iterator2_t8_156;
// OnClickInstantiate
struct OnClickInstantiate_t8_158;
// OnClickLoadSomething
struct OnClickLoadSomething_t8_160;
// OnJoinedInstantiate
struct OnJoinedInstantiate_t8_161;
// OnStartDelete
struct OnStartDelete_t8_162;
// PickupItem
struct PickupItem_t8_163;
// UnityEngine.Collider
struct Collider_t6_100;
// PickupItemSimple
struct PickupItemSimple_t8_164;
// PickupItemSyncer
struct PickupItemSyncer_t8_165;
// System.Single[]
struct SingleU5BU5D_t1_863;
// PointedAtGameObjectInfo
struct PointedAtGameObjectInfo_t8_166;
// PunPlayerScores
struct PunPlayerScores_t8_167;
// PunTeams
struct PunTeams_t8_170;
// QuitOnEscapeOrBack
struct QuitOnEscapeOrBack_t8_172;
// ServerTime
struct ServerTime_t8_173;
// ShowInfoOfPlayer
struct ShowInfoOfPlayer_t8_174;
// UnityEngine.MeshRenderer
struct MeshRenderer_t6_28;
// UnityEngine.TextMesh
struct TextMesh_t6_145;
// ShowStatusWhenConnecting
struct ShowStatusWhenConnecting_t8_175;
// SmoothSyncMovement
struct SmoothSyncMovement_t8_176;
// SupportLogger
struct SupportLogger_t8_177;
// SupportLogging
struct SupportLogging_t8_178;
// ExitGames.Client.DemoParticle.TimeKeeper
struct TimeKeeper_t8_179;

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array.h"
#include "AssemblyU2DCSharp_PhotonHandler.h"
#include "AssemblyU2DCSharp_PhotonHandlerMethodDeclarations.h"
#include "mscorlib_System_Void.h"
#include "AssemblyU2DCSharp_Photon_MonoBehaviourMethodDeclarations.h"
#include "AssemblyU2DCSharp_Photon_MonoBehaviour.h"
#include "AssemblyU2DCSharp_CloudRegionCode.h"
#include "UnityEngine_UnityEngine_ObjectMethodDeclarations.h"
#include "UnityEngine_UnityEngine_ComponentMethodDeclarations.h"
#include "AssemblyU2DCSharp_PhotonNetworkMethodDeclarations.h"
#include "mscorlib_System_Boolean.h"
#include "UnityEngine_UnityEngine_Object.h"
#include "UnityEngine_UnityEngine_Component.h"
#include "UnityEngine_UnityEngine_GameObject.h"
#include "mscorlib_System_Int32.h"
#include "System_System_Diagnostics_StopwatchMethodDeclarations.h"
#include "AssemblyU2DCSharp_PhotonNetwork.h"
#include "mscorlib_System_Single.h"
#include "System_System_Diagnostics_Stopwatch.h"
#include "UnityEngine_UnityEngine_DebugMethodDeclarations.h"
#include "UnityEngine_UnityEngine_TimeMethodDeclarations.h"
#include "AssemblyU2DCSharp_NetworkingPeerMethodDeclarations.h"
#include "AssemblyU2DCSharp_NetworkingPeer.h"
#include "mscorlib_System_String.h"
#include "mscorlib_System_Object.h"
#include "AssemblyU2DCSharp_PeerState.h"
#include "Photon3Unity3D_ExitGames_Client_Photon_PhotonPeer.h"
#include "Photon3Unity3D_ExitGames_Client_Photon_PhotonPeerMethodDeclarations.h"
#include "UnityEngine_UnityEngine_ApplicationMethodDeclarations.h"
#include "System_Core_System_Func_1_genMethodDeclarations.h"
#include "Photon3Unity3D_ExitGames_Client_Photon_SupportClassMethodDeclarations.h"
#include "System_Core_System_Func_1_gen.h"
#include "mscorlib_System_IntPtr.h"
#include "mscorlib_System_Int64.h"
#include "UnityEngine_UnityEngine_PlayerPrefsMethodDeclarations.h"
#include "mscorlib_System_StringMethodDeclarations.h"
#include "AssemblyU2DCSharp_RegionMethodDeclarations.h"
#include "mscorlib_System_Enum.h"
#include "mscorlib_System_EnumMethodDeclarations.h"
#include "UnityEngine_UnityEngine_MonoBehaviourMethodDeclarations.h"
#include "UnityEngine_UnityEngine_MonoBehaviour.h"
#include "UnityEngine_UnityEngine_Coroutine.h"
#include "AssemblyU2DCSharp_PhotonHandler_U3CPingAvailableRegionsCorouMethodDeclarations.h"
#include "AssemblyU2DCSharp_PhotonHandler_U3CPingAvailableRegionsCorou.h"
#include "AssemblyU2DCSharp_PhotonLagSimulationGui.h"
#include "AssemblyU2DCSharp_PhotonLagSimulationGuiMethodDeclarations.h"
#include "UnityEngine_UnityEngine_RectMethodDeclarations.h"
#include "UnityEngine_UnityEngine_Rect.h"
#include "UnityEngine_UnityEngine_GUI_WindowFunctionMethodDeclarations.h"
#include "UnityEngine_UnityEngine_GUILayoutMethodDeclarations.h"
#include "UnityEngine_UnityEngine_GUI_WindowFunction.h"
#include "UnityEngine_ArrayTypes.h"
#include "UnityEngine_UnityEngine_GUILayoutOption.h"
#include "Photon3Unity3D_ExitGames_Client_Photon_NetworkSimulationSetMethodDeclarations.h"
#include "UnityEngine_UnityEngine_GUIMethodDeclarations.h"
#include "Photon3Unity3D_ExitGames_Client_Photon_NetworkSimulationSet.h"
#include "AssemblyU2DCSharp_PhotonNetwork_EventCallback.h"
#include "AssemblyU2DCSharp_PhotonNetwork_EventCallbackMethodDeclarations.h"
#include "mscorlib_System_Byte.h"
#include "mscorlib_System_AsyncCallback.h"
#include "mscorlib_System_TypeMethodDeclarations.h"
#include "UnityEngine_UnityEngine_ResourcesMethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_gen_20MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen_19MethodDeclarations.h"
#include "UnityEngine_UnityEngine_GameObjectMethodDeclarations.h"
#include "AssemblyU2DCSharp_CustomTypesMethodDeclarations.h"
#include "Photon3Unity3D_ExitGames_Client_Photon_ConnectionProtocol.h"
#include "mscorlib_System_Type.h"
#include "mscorlib_System_RuntimeTypeHandle.h"
#include "AssemblyU2DCSharp_ServerSettings.h"
#include "AssemblyU2DCSharp_PhotonLogLevel.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_gen_20.h"
#include "AssemblyU2DCSharp_Room.h"
#include "mscorlib_System_Collections_Generic_List_1_gen_19.h"
#include "UnityEngine_UnityEngine_HideFlags.h"
#include "AssemblyU2DCSharp_ConnectionState.h"
#include "Photon3Unity3D_ExitGames_Client_Photon_PeerStateValue.h"
#include "AssemblyU2DCSharp_ServerConnection.h"
#include "AssemblyU2DCSharp_AuthenticationValues.h"
#include "AssemblyU2DCSharp_PhotonPlayer.h"
#include "Assembly-CSharp_ArrayTypes.h"
#include "mscorlib_System_Collections_Generic_List_1_gen_14.h"
#include "mscorlib_ArrayTypes.h"
#include "AssemblyU2DCSharp_PhotonNetworkingMessage.h"
#include "mscorlib_System_Collections_Generic_List_1_gen_17.h"
#include "AssemblyU2DCSharp_TypedLobby.h"
#include "mscorlib_System_Double.h"
#include "mscorlib_System_UInt32.h"
#include "mscorlib_System_EnvironmentMethodDeclarations.h"
#include "AssemblyU2DCSharp_PhotonPlayerMethodDeclarations.h"
#include "Photon3Unity3D_ExitGames_Client_Photon_DebugLevel.h"
#include "AssemblyU2DCSharp_ServerSettings_HostingOption.h"
#include "mscorlib_System_NotImplementedExceptionMethodDeclarations.h"
#include "mscorlib_System_NotImplementedException.h"
#include "Photon3Unity3D_ExitGames_Client_Photon_StatusCode.h"
#include "AssemblyU2DCSharp_RoomOptions.h"
#include "AssemblyU2DCSharp_ExitGames_Client_Photon_LoadbalancingPeer_MethodDeclarations.h"
#include "AssemblyU2DCSharp_ExitGames_Client_Photon_LoadbalancingPeer_.h"
#include "Photon3Unity3D_ExitGames_Client_Photon_Hashtable.h"
#include "AssemblyU2DCSharp_ExitGames_Client_Photon_MatchmakingMode.h"
#include "AssemblyU2DCSharp_ExitGames_Client_Photon_LoadbalancingPeer__0MethodDeclarations.h"
#include "AssemblyU2DCSharp_ExitGames_Client_Photon_LoadbalancingPeer__0.h"
#include "AssemblyU2DCSharp_RoomMethodDeclarations.h"
#include "AssemblyU2DCSharp_TypedLobbyMethodDeclarations.h"
#include "AssemblyU2DCSharp_ExitGames_Client_Photon_LoadbalancingPeer.h"
#include "AssemblyU2DCSharp_ExitGames_Client_Photon_LoadbalancingPeerMethodDeclarations.h"
#include "AssemblyU2DCSharp_RoomInfo.h"
#include "Photon3Unity3D_ExitGames_Client_Photon_HashtableMethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_gen_1MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_KeyCollecti_0MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_KeyCollectiMethodDeclarations.h"
#include "AssemblyU2DCSharp_RoomInfoMethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_KeyCollecti.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_gen_1.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_KeyCollecti_0.h"
#include "AssemblyU2DCSharp_RaiseEventOptions.h"
#include "mscorlib_System_ExceptionMethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_gen_16.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_gen_16MethodDeclarations.h"
#include "mscorlib_System_Exception.h"
#include "AssemblyU2DCSharp_PhotonView.h"
#include "UnityEngine_UnityEngine_Vector3.h"
#include "UnityEngine_UnityEngine_Quaternion.h"
#include "AssemblyU2DCSharp_ExtensionsMethodDeclarations.h"
#include "AssemblyU2DCSharp_RaiseEventOptionsMethodDeclarations.h"
#include "AssemblyU2DCSharp_PhotonTargets.h"
#include "System_Core_System_Collections_Generic_HashSet_1_gen_1.h"
#include "System_Core_System_Collections_Generic_HashSet_1_gen_1MethodDeclarations.h"
#include "mscorlib_System_Int16.h"
#include "mscorlib_System_ObjectMethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_gen_14MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_KeyCollecti_7MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_KeyCollecti_6MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_gen_14.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_KeyCollecti_6.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_KeyCollecti_7.h"
#include "AssemblyU2DCSharp_PhotonStatsGui.h"
#include "AssemblyU2DCSharp_PhotonStatsGuiMethodDeclarations.h"
#include "UnityEngine_UnityEngine_ScreenMethodDeclarations.h"
#include "UnityEngine_UnityEngine_InputMethodDeclarations.h"
#include "UnityEngine_UnityEngine_KeyCode.h"
#include "Photon3Unity3D_ExitGames_Client_Photon_TrafficStatsGameLevelMethodDeclarations.h"
#include "Photon3Unity3D_ExitGames_Client_Photon_TrafficStatsGameLevel.h"
#include "Photon3Unity3D_ExitGames_Client_Photon_TrafficStats.h"
#include "Photon3Unity3D_ExitGames_Client_Photon_TrafficStatsMethodDeclarations.h"
#include "AssemblyU2DCSharp_PhotonStreamQueue.h"
#include "AssemblyU2DCSharp_PhotonStreamQueueMethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen_13MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen_13.h"
#include "AssemblyU2DCSharp_PhotonStream.h"
#include "AssemblyU2DCSharp_PhotonStreamMethodDeclarations.h"
#include "AssemblyU2DCSharp_ViewSynchronization.h"
#include "AssemblyU2DCSharp_ViewSynchronizationMethodDeclarations.h"
#include "AssemblyU2DCSharp_OnSerializeTransform.h"
#include "AssemblyU2DCSharp_OnSerializeTransformMethodDeclarations.h"
#include "AssemblyU2DCSharp_OnSerializeRigidBody.h"
#include "AssemblyU2DCSharp_OnSerializeRigidBodyMethodDeclarations.h"
#include "AssemblyU2DCSharp_OwnershipOption.h"
#include "AssemblyU2DCSharp_OwnershipOptionMethodDeclarations.h"
#include "AssemblyU2DCSharp_PhotonViewMethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_gen_22MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_gen_22.h"
#include "AssemblyU2DCSharp_PhotonMessageInfo.h"
#include "mscorlib_System_Collections_Generic_List_1_gen_21.h"
#include "mscorlib_System_Collections_Generic_List_1_gen_21MethodDeclarations.h"
#include "UnityEngine_UnityEngine_TransformMethodDeclarations.h"
#include "UnityEngine_UnityEngine_RigidbodyMethodDeclarations.h"
#include "UnityEngine_UnityEngine_Rigidbody2DMethodDeclarations.h"
#include "UnityEngine_UnityEngine_Transform.h"
#include "UnityEngine_UnityEngine_Rigidbody.h"
#include "UnityEngine_UnityEngine_Rigidbody2D.h"
#include "UnityEngine_UnityEngine_Vector2.h"
#include "mscorlib_System_Reflection_MethodInfo.h"
#include "mscorlib_System_Reflection_MethodBase.h"
#include "mscorlib_System_Reflection_MethodBaseMethodDeclarations.h"
#include "AssemblyU2DCSharp_PhotonPingManager_U3CPingSocketU3Ec__Itera.h"
#include "AssemblyU2DCSharp_PhotonPingManager_U3CPingSocketU3Ec__IteraMethodDeclarations.h"
#include "Photon3Unity3D_ExitGames_Client_Photon_PingNativeDynamicMethodDeclarations.h"
#include "Photon3Unity3D_ExitGames_Client_Photon_PingMonoMethodDeclarations.h"
#include "mscorlib_System_ActivatorMethodDeclarations.h"
#include "AssemblyU2DCSharp_PhotonPingManagerMethodDeclarations.h"
#include "UnityEngine_UnityEngine_WaitForSecondsMethodDeclarations.h"
#include "AssemblyU2DCSharp_Region.h"
#include "AssemblyU2DCSharp_PhotonPingManager.h"
#include "Photon3Unity3D_ExitGames_Client_Photon_PingNativeDynamic.h"
#include "Photon3Unity3D_ExitGames_Client_Photon_PhotonPing.h"
#include "Photon3Unity3D_ExitGames_Client_Photon_PingMono.h"
#include "mscorlib_System_Char.h"
#include "Photon3Unity3D_ExitGames_Client_Photon_PhotonPingMethodDeclarations.h"
#include "UnityEngine_UnityEngine_WaitForSeconds.h"
#include "mscorlib_System_NotSupportedExceptionMethodDeclarations.h"
#include "mscorlib_System_NotSupportedException.h"
#include "mscorlib_System_Collections_Generic_List_1_gen_18MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_3MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_3.h"
#include "mscorlib_System_Collections_Generic_List_1_gen_18.h"
#include "System_System_Net_DnsMethodDeclarations.h"
#include "System_ArrayTypes.h"
#include "System_System_Net_IPAddress.h"
#include "System_System_Net_IPAddressMethodDeclarations.h"
#include "AssemblyU2DCSharp_PunRPC.h"
#include "AssemblyU2DCSharp_PunRPCMethodDeclarations.h"
#include "mscorlib_System_AttributeMethodDeclarations.h"
#include "mscorlib_System_Attribute.h"
#include "AssemblyU2DCSharp_RoomOptionsMethodDeclarations.h"
#include "AssemblyU2DCSharp_CloudRegionFlag.h"
#include "AssemblyU2DCSharp_ServerSettings_HostingOptionMethodDeclarations.h"
#include "AssemblyU2DCSharp_ServerSettingsMethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_genMethodDeclarations.h"
#include "UnityEngine_UnityEngine_ScriptableObjectMethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen.h"
#include "UnityEngine_UnityEngine_ScriptableObject.h"
#include "AssemblyU2DCSharp_PhotonAnimatorView_ParameterType.h"
#include "AssemblyU2DCSharp_PhotonAnimatorView_ParameterTypeMethodDeclarations.h"
#include "AssemblyU2DCSharp_PhotonAnimatorView_SynchronizeType.h"
#include "AssemblyU2DCSharp_PhotonAnimatorView_SynchronizeTypeMethodDeclarations.h"
#include "AssemblyU2DCSharp_PhotonAnimatorView_SynchronizedParameter.h"
#include "AssemblyU2DCSharp_PhotonAnimatorView_SynchronizedParameterMethodDeclarations.h"
#include "AssemblyU2DCSharp_PhotonAnimatorView_SynchronizedLayer.h"
#include "AssemblyU2DCSharp_PhotonAnimatorView_SynchronizedLayerMethodDeclarations.h"
#include "AssemblyU2DCSharp_PhotonAnimatorView_U3CDoesLayerSynchronize.h"
#include "AssemblyU2DCSharp_PhotonAnimatorView_U3CDoesLayerSynchronizeMethodDeclarations.h"
#include "AssemblyU2DCSharp_PhotonAnimatorView_U3CDoesParameterSynchro.h"
#include "AssemblyU2DCSharp_PhotonAnimatorView_U3CDoesParameterSynchroMethodDeclarations.h"
#include "AssemblyU2DCSharp_PhotonAnimatorView_U3CGetLayerSynchronizeT.h"
#include "AssemblyU2DCSharp_PhotonAnimatorView_U3CGetLayerSynchronizeTMethodDeclarations.h"
#include "AssemblyU2DCSharp_PhotonAnimatorView_U3CGetParameterSynchron.h"
#include "AssemblyU2DCSharp_PhotonAnimatorView_U3CGetParameterSynchronMethodDeclarations.h"
#include "AssemblyU2DCSharp_PhotonAnimatorView_U3CSetLayerSynchronized.h"
#include "AssemblyU2DCSharp_PhotonAnimatorView_U3CSetLayerSynchronizedMethodDeclarations.h"
#include "AssemblyU2DCSharp_PhotonAnimatorView_U3CSetParameterSynchron.h"
#include "AssemblyU2DCSharp_PhotonAnimatorView_U3CSetParameterSynchronMethodDeclarations.h"
#include "AssemblyU2DCSharp_PhotonAnimatorView.h"
#include "AssemblyU2DCSharp_PhotonAnimatorViewMethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen_15MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen_22MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen_15.h"
#include "mscorlib_System_Collections_Generic_List_1_gen_22.h"
#include "UnityEngine_UnityEngine_Animator.h"
#include "UnityEngine_UnityEngine_AnimatorMethodDeclarations.h"
#include "mscorlib_System_Predicate_1_genMethodDeclarations.h"
#include "mscorlib_System_Predicate_1_gen.h"
#include "mscorlib_System_Predicate_1_gen_0MethodDeclarations.h"
#include "mscorlib_System_Predicate_1_gen_0.h"
#include "AssemblyU2DCSharp_PhotonRigidbody2DView.h"
#include "AssemblyU2DCSharp_PhotonRigidbody2DViewMethodDeclarations.h"
#include "AssemblyU2DCSharp_PhotonRigidbodyView.h"
#include "AssemblyU2DCSharp_PhotonRigidbodyViewMethodDeclarations.h"
#include "AssemblyU2DCSharp_PhotonTransformView.h"
#include "AssemblyU2DCSharp_PhotonTransformViewMethodDeclarations.h"
#include "AssemblyU2DCSharp_PhotonTransformViewPositionModelMethodDeclarations.h"
#include "AssemblyU2DCSharp_PhotonTransformViewRotationModelMethodDeclarations.h"
#include "AssemblyU2DCSharp_PhotonTransformViewScaleModelMethodDeclarations.h"
#include "AssemblyU2DCSharp_PhotonTransformViewPositionModel.h"
#include "AssemblyU2DCSharp_PhotonTransformViewRotationModel.h"
#include "AssemblyU2DCSharp_PhotonTransformViewScaleModel.h"
#include "AssemblyU2DCSharp_PhotonTransformViewPositionControlMethodDeclarations.h"
#include "AssemblyU2DCSharp_PhotonTransformViewRotationControlMethodDeclarations.h"
#include "AssemblyU2DCSharp_PhotonTransformViewScaleControlMethodDeclarations.h"
#include "AssemblyU2DCSharp_PhotonTransformViewPositionControl.h"
#include "AssemblyU2DCSharp_PhotonTransformViewRotationControl.h"
#include "AssemblyU2DCSharp_PhotonTransformViewScaleControl.h"
#include "UnityEngine_UnityEngine_ColorMethodDeclarations.h"
#include "UnityEngine_UnityEngine_Vector3MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Color.h"
#include "System_System_Collections_Generic_Queue_1_gen_4MethodDeclarations.h"
#include "System_System_Collections_Generic_Queue_1_gen_4.h"
#include "UnityEngine_UnityEngine_MathfMethodDeclarations.h"
#include "AssemblyU2DCSharp_PhotonTransformViewPositionModel_Interpola.h"
#include "UnityEngine_UnityEngine_QuaternionMethodDeclarations.h"
#include "AssemblyU2DCSharp_PhotonTransformViewPositionModel_Extrapola.h"
#include "AssemblyU2DCSharp_PhotonTransformViewPositionModel_InterpolaMethodDeclarations.h"
#include "AssemblyU2DCSharp_PhotonTransformViewPositionModel_ExtrapolaMethodDeclarations.h"
#include "UnityEngine_UnityEngine_KeyframeMethodDeclarations.h"
#include "UnityEngine_UnityEngine_AnimationCurveMethodDeclarations.h"
#include "UnityEngine_UnityEngine_Keyframe.h"
#include "UnityEngine_UnityEngine_AnimationCurve.h"
#include "AssemblyU2DCSharp_PhotonTransformViewRotationModel_Interpola.h"
#include "AssemblyU2DCSharp_PhotonTransformViewRotationModel_InterpolaMethodDeclarations.h"
#include "AssemblyU2DCSharp_PhotonTransformViewScaleModel_InterpolateO.h"
#include "AssemblyU2DCSharp_PhotonTransformViewScaleModel_InterpolateOMethodDeclarations.h"
#include "AssemblyU2DCSharp_ConnectAndJoinRandom.h"
#include "AssemblyU2DCSharp_ConnectAndJoinRandomMethodDeclarations.h"
#include "AssemblyU2DCSharp_DisconnectCause.h"
#include "AssemblyU2DCSharp_HighlightOwnedGameObj.h"
#include "AssemblyU2DCSharp_HighlightOwnedGameObjMethodDeclarations.h"
#include "AssemblyU2DCSharp_InRoomChat.h"
#include "AssemblyU2DCSharp_InRoomChatMethodDeclarations.h"
#include "UnityEngine_UnityEngine_Vector2MethodDeclarations.h"
#include "UnityEngine_UnityEngine_EventMethodDeclarations.h"
#include "UnityEngine_UnityEngine_Event.h"
#include "UnityEngine_UnityEngine_EventType.h"
#include "AssemblyU2DCSharp_InRoomRoundTimer.h"
#include "AssemblyU2DCSharp_InRoomRoundTimerMethodDeclarations.h"
#include "AssemblyU2DCSharp_InputToEvent.h"
#include "AssemblyU2DCSharp_InputToEventMethodDeclarations.h"
#include "UnityEngine_UnityEngine_Camera.h"
#include "UnityEngine_UnityEngine_TouchMethodDeclarations.h"
#include "UnityEngine_UnityEngine_Touch.h"
#include "UnityEngine_UnityEngine_TouchPhase.h"
#include "UnityEngine_UnityEngine_SendMessageOptions.h"
#include "UnityEngine_UnityEngine_CameraMethodDeclarations.h"
#include "UnityEngine_UnityEngine_PhysicsMethodDeclarations.h"
#include "UnityEngine_UnityEngine_RaycastHitMethodDeclarations.h"
#include "UnityEngine_UnityEngine_RaycastHit.h"
#include "UnityEngine_UnityEngine_Ray.h"
#include "UnityEngine_UnityEngine_Collider.h"
#include "AssemblyU2DCSharp_ManualPhotonViewAllocator.h"
#include "AssemblyU2DCSharp_ManualPhotonViewAllocatorMethodDeclarations.h"
#include "AssemblyU2DCSharp_OnClickDestroy.h"
#include "AssemblyU2DCSharp_MoveByKeys.h"
#include "AssemblyU2DCSharp_MoveByKeysMethodDeclarations.h"
#include "UnityEngine_UnityEngine_SpriteRenderer.h"
#include "AssemblyU2DCSharp_OnAwakeUsePhotonView.h"
#include "AssemblyU2DCSharp_OnAwakeUsePhotonViewMethodDeclarations.h"
#include "AssemblyU2DCSharp_OnClickDestroy_U3CDestroyRpcU3Ec__Iterator.h"
#include "AssemblyU2DCSharp_OnClickDestroy_U3CDestroyRpcU3Ec__IteratorMethodDeclarations.h"
#include "AssemblyU2DCSharp_OnClickDestroyMethodDeclarations.h"
#include "AssemblyU2DCSharp_OnClickInstantiate.h"
#include "AssemblyU2DCSharp_OnClickInstantiateMethodDeclarations.h"
#include "AssemblyU2DCSharp_OnClickLoadSomething_ResourceTypeOption.h"
#include "AssemblyU2DCSharp_OnClickLoadSomething_ResourceTypeOptionMethodDeclarations.h"
#include "AssemblyU2DCSharp_OnClickLoadSomething.h"
#include "AssemblyU2DCSharp_OnClickLoadSomethingMethodDeclarations.h"
#include "AssemblyU2DCSharp_OnJoinedInstantiate.h"
#include "AssemblyU2DCSharp_OnJoinedInstantiateMethodDeclarations.h"
#include "UnityEngine_UnityEngine_RandomMethodDeclarations.h"
#include "AssemblyU2DCSharp_OnStartDelete.h"
#include "AssemblyU2DCSharp_OnStartDeleteMethodDeclarations.h"
#include "AssemblyU2DCSharp_PickupItem.h"
#include "AssemblyU2DCSharp_PickupItemMethodDeclarations.h"
#include "System_Core_System_Collections_Generic_HashSet_1_genMethodDeclarations.h"
#include "System_Core_System_Collections_Generic_HashSet_1_gen.h"
#include "AssemblyU2DCSharp_GameObjectExtensionsMethodDeclarations.h"
#include "AssemblyU2DCSharp_PhotonMessageInfoMethodDeclarations.h"
#include "AssemblyU2DCSharp_PickupItemSimple.h"
#include "AssemblyU2DCSharp_PickupItemSimpleMethodDeclarations.h"
#include "AssemblyU2DCSharp_PickupItemSyncer.h"
#include "AssemblyU2DCSharp_PickupItemSyncerMethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen_23MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen_23.h"
#include "AssemblyU2DCSharp_PointedAtGameObjectInfo.h"
#include "AssemblyU2DCSharp_PointedAtGameObjectInfoMethodDeclarations.h"
#include "AssemblyU2DCSharp_PunPlayerScores.h"
#include "AssemblyU2DCSharp_PunPlayerScoresMethodDeclarations.h"
#include "AssemblyU2DCSharp_ScoreExtensions.h"
#include "AssemblyU2DCSharp_ScoreExtensionsMethodDeclarations.h"
#include "AssemblyU2DCSharp_PunTeams_Team.h"
#include "AssemblyU2DCSharp_PunTeams_TeamMethodDeclarations.h"
#include "AssemblyU2DCSharp_PunTeams.h"
#include "AssemblyU2DCSharp_PunTeamsMethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_gen_13MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen_16MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_gen_13.h"
#include "mscorlib_System_ArrayMethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen_16.h"
#include "AssemblyU2DCSharp_TeamExtensionsMethodDeclarations.h"
#include "AssemblyU2DCSharp_TeamExtensions.h"
#include "AssemblyU2DCSharp_QuitOnEscapeOrBack.h"
#include "AssemblyU2DCSharp_QuitOnEscapeOrBackMethodDeclarations.h"
#include "AssemblyU2DCSharp_ServerTime.h"
#include "AssemblyU2DCSharp_ServerTimeMethodDeclarations.h"
#include "AssemblyU2DCSharp_ShowInfoOfPlayer.h"
#include "AssemblyU2DCSharp_ShowInfoOfPlayerMethodDeclarations.h"
#include "UnityEngine_UnityEngine_FontMethodDeclarations.h"
#include "UnityEngine_UnityEngine_RendererMethodDeclarations.h"
#include "UnityEngine_UnityEngine_TextMeshMethodDeclarations.h"
#include "UnityEngine_UnityEngine_MeshRenderer.h"
#include "UnityEngine_UnityEngine_Font.h"
#include "UnityEngine_UnityEngine_TextMesh.h"
#include "UnityEngine_UnityEngine_Material.h"
#include "UnityEngine_UnityEngine_Renderer.h"
#include "UnityEngine_UnityEngine_TextAnchor.h"
#include "AssemblyU2DCSharp_ShowStatusWhenConnecting.h"
#include "AssemblyU2DCSharp_ShowStatusWhenConnectingMethodDeclarations.h"
#include "UnityEngine_UnityEngine_GUISkinMethodDeclarations.h"
#include "UnityEngine_UnityEngine_BehaviourMethodDeclarations.h"
#include "UnityEngine_UnityEngine_GUISkin.h"
#include "UnityEngine_UnityEngine_GUIStyle.h"
#include "UnityEngine_UnityEngine_Behaviour.h"
#include "AssemblyU2DCSharp_SmoothSyncMovement.h"
#include "AssemblyU2DCSharp_SmoothSyncMovementMethodDeclarations.h"
#include "AssemblyU2DCSharp_SupportLogger.h"
#include "AssemblyU2DCSharp_SupportLoggerMethodDeclarations.h"
#include "AssemblyU2DCSharp_SupportLogging.h"
#include "AssemblyU2DCSharp_SupportLoggingMethodDeclarations.h"
#include "mscorlib_System_Text_StringBuilderMethodDeclarations.h"
#include "mscorlib_System_Text_StringBuilder.h"
#include "AssemblyU2DCSharp_ExitGames_Client_DemoParticle_TimeKeeper.h"
#include "AssemblyU2DCSharp_ExitGames_Client_DemoParticle_TimeKeeperMethodDeclarations.h"

// !!0 UnityEngine.GameObject::AddComponent<System.Object>()
extern "C" Object_t * GameObject_AddComponent_TisObject_t_m6_1695_gshared (GameObject_t6_85 * __this, const MethodInfo* method);
#define GameObject_AddComponent_TisObject_t_m6_1695(__this, method) (( Object_t * (*) (GameObject_t6_85 *, const MethodInfo*))GameObject_AddComponent_TisObject_t_m6_1695_gshared)(__this, method)
// !!0 UnityEngine.GameObject::AddComponent<PhotonHandler>()
#define GameObject_AddComponent_TisPhotonHandler_t8_111_m6_1687(__this, method) (( PhotonHandler_t8_111 * (*) (GameObject_t6_85 *, const MethodInfo*))GameObject_AddComponent_TisObject_t_m6_1695_gshared)(__this, method)
// !!0 UnityEngine.GameObject::GetComponent<System.Object>()
extern "C" Object_t * GameObject_GetComponent_TisObject_t_m6_1683_gshared (GameObject_t6_85 * __this, const MethodInfo* method);
#define GameObject_GetComponent_TisObject_t_m6_1683(__this, method) (( Object_t * (*) (GameObject_t6_85 *, const MethodInfo*))GameObject_GetComponent_TisObject_t_m6_1683_gshared)(__this, method)
// !!0 UnityEngine.GameObject::GetComponent<PhotonView>()
#define GameObject_GetComponent_TisPhotonView_t8_3_m6_1666(__this, method) (( PhotonView_t8_3 * (*) (GameObject_t6_85 *, const MethodInfo*))GameObject_GetComponent_TisObject_t_m6_1683_gshared)(__this, method)
// !!0[] UnityEngine.Component::GetComponents<System.Object>()
extern "C" ObjectU5BU5D_t1_157* Component_GetComponents_TisObject_t_m6_1696_gshared (Component_t6_26 * __this, const MethodInfo* method);
#define Component_GetComponents_TisObject_t_m6_1696(__this, method) (( ObjectU5BU5D_t1_157* (*) (Component_t6_26 *, const MethodInfo*))Component_GetComponents_TisObject_t_m6_1696_gshared)(__this, method)
// !!0[] UnityEngine.Component::GetComponents<UnityEngine.MonoBehaviour>()
#define Component_GetComponents_TisMonoBehaviour_t6_80_m6_1688(__this, method) (( MonoBehaviourU5BU5D_t6_273* (*) (Component_t6_26 *, const MethodInfo*))Component_GetComponents_TisObject_t_m6_1696_gshared)(__this, method)
// !!0 UnityEngine.Component::GetComponent<System.Object>()
extern "C" Object_t * Component_GetComponent_TisObject_t_m6_1655_gshared (Component_t6_26 * __this, const MethodInfo* method);
#define Component_GetComponent_TisObject_t_m6_1655(__this, method) (( Object_t * (*) (Component_t6_26 *, const MethodInfo*))Component_GetComponent_TisObject_t_m6_1655_gshared)(__this, method)
// !!0 UnityEngine.Component::GetComponent<PhotonView>()
#define Component_GetComponent_TisPhotonView_t8_3_m6_1658(__this, method) (( PhotonView_t8_3 * (*) (Component_t6_26 *, const MethodInfo*))Component_GetComponent_TisObject_t_m6_1655_gshared)(__this, method)
// !!0 UnityEngine.Component::GetComponent<UnityEngine.Animator>()
#define Component_GetComponent_TisAnimator_t6_138_m6_1656(__this, method) (( Animator_t6_138 * (*) (Component_t6_26 *, const MethodInfo*))Component_GetComponent_TisObject_t_m6_1655_gshared)(__this, method)
// !!0 UnityEngine.Component::GetComponent<UnityEngine.Rigidbody2D>()
#define Component_GetComponent_TisRigidbody2D_t6_113_m6_1657(__this, method) (( Rigidbody2D_t6_113 * (*) (Component_t6_26 *, const MethodInfo*))Component_GetComponent_TisObject_t_m6_1655_gshared)(__this, method)
// !!0 UnityEngine.Component::GetComponent<UnityEngine.Rigidbody>()
#define Component_GetComponent_TisRigidbody_t6_102_m6_1659(__this, method) (( Rigidbody_t6_102 * (*) (Component_t6_26 *, const MethodInfo*))Component_GetComponent_TisObject_t_m6_1655_gshared)(__this, method)
// !!0 UnityEngine.Object::Instantiate<System.Object>(!!0)
extern "C" Object_t * Object_Instantiate_TisObject_t_m6_1697_gshared (Object_t * __this /* static, unused */, Object_t * p0, const MethodInfo* method);
#define Object_Instantiate_TisObject_t_m6_1697(__this /* static, unused */, p0, method) (( Object_t * (*) (Object_t * /* static, unused */, Object_t *, const MethodInfo*))Object_Instantiate_TisObject_t_m6_1697_gshared)(__this /* static, unused */, p0, method)
// !!0 UnityEngine.Object::Instantiate<UnityEngine.GameObject>(!!0)
#define Object_Instantiate_TisGameObject_t6_85_m6_1689(__this /* static, unused */, p0, method) (( GameObject_t6_85 * (*) (Object_t * /* static, unused */, GameObject_t6_85 *, const MethodInfo*))Object_Instantiate_TisObject_t_m6_1697_gshared)(__this /* static, unused */, p0, method)
// !!0 UnityEngine.Component::GetComponent<UnityEngine.Camera>()
#define Component_GetComponent_TisCamera_t6_75_m6_1665(__this, method) (( Camera_t6_75 * (*) (Component_t6_26 *, const MethodInfo*))Component_GetComponent_TisObject_t_m6_1655_gshared)(__this, method)
// !!0 UnityEngine.GameObject::GetComponent<OnClickDestroy>()
#define GameObject_GetComponent_TisOnClickDestroy_t8_157_m6_1690(__this, method) (( OnClickDestroy_t8_157 * (*) (GameObject_t6_85 *, const MethodInfo*))GameObject_GetComponent_TisObject_t_m6_1683_gshared)(__this, method)
// !!0 UnityEngine.Component::GetComponent<UnityEngine.SpriteRenderer>()
#define Component_GetComponent_TisSpriteRenderer_t6_69_m6_1691(__this, method) (( SpriteRenderer_t6_69 * (*) (Component_t6_26 *, const MethodInfo*))Component_GetComponent_TisObject_t_m6_1655_gshared)(__this, method)
// !!0 UnityEngine.Component::GetComponent<PickupItem>()
#define Component_GetComponent_TisPickupItem_t8_163_m6_1673(__this, method) (( PickupItem_t8_163 * (*) (Component_t6_26 *, const MethodInfo*))Component_GetComponent_TisObject_t_m6_1655_gshared)(__this, method)
// !!0 UnityEngine.GameObject::AddComponent<UnityEngine.MeshRenderer>()
#define GameObject_AddComponent_TisMeshRenderer_t6_28_m6_1692(__this, method) (( MeshRenderer_t6_28 * (*) (GameObject_t6_85 *, const MethodInfo*))GameObject_AddComponent_TisObject_t_m6_1695_gshared)(__this, method)
// !!0 UnityEngine.GameObject::AddComponent<UnityEngine.TextMesh>()
#define GameObject_AddComponent_TisTextMesh_t6_145_m6_1693(__this, method) (( TextMesh_t6_145 * (*) (GameObject_t6_85 *, const MethodInfo*))GameObject_AddComponent_TisObject_t_m6_1695_gshared)(__this, method)
// !!0 UnityEngine.GameObject::AddComponent<SupportLogging>()
#define GameObject_AddComponent_TisSupportLogging_t8_178_m6_1694(__this, method) (( SupportLogging_t8_178 * (*) (GameObject_t6_85 *, const MethodInfo*))GameObject_AddComponent_TisObject_t_m6_1695_gshared)(__this, method)
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void PhotonHandler::.ctor()
extern "C" void PhotonHandler__ctor_m8_571 (PhotonHandler_t8_111 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m8_494(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void PhotonHandler::.cctor()
extern TypeInfo* PhotonHandler_t8_111_il2cpp_TypeInfo_var;
extern "C" void PhotonHandler__cctor_m8_572 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		PhotonHandler_t8_111_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1120);
		s_Il2CppMethodIntialized = true;
	}
	{
		((PhotonHandler_t8_111_StaticFields*)PhotonHandler_t8_111_il2cpp_TypeInfo_var->static_fields)->___BestRegionCodeCurrently_12 = 4;
		return;
	}
}
// System.Void PhotonHandler::Awake()
extern TypeInfo* PhotonHandler_t8_111_il2cpp_TypeInfo_var;
extern TypeInfo* PhotonNetwork_t8_114_il2cpp_TypeInfo_var;
extern "C" void PhotonHandler_Awake_m8_573 (PhotonHandler_t8_111 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		PhotonHandler_t8_111_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1120);
		PhotonNetwork_t8_114_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1058);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(PhotonHandler_t8_111_il2cpp_TypeInfo_var);
		PhotonHandler_t8_111 * L_0 = ((PhotonHandler_t8_111_StaticFields*)PhotonHandler_t8_111_il2cpp_TypeInfo_var->static_fields)->___SP_3;
		bool L_1 = Object_op_Inequality_m6_580(NULL /*static, unused*/, L_0, (Object_t6_5 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0044;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(PhotonHandler_t8_111_il2cpp_TypeInfo_var);
		PhotonHandler_t8_111 * L_2 = ((PhotonHandler_t8_111_StaticFields*)PhotonHandler_t8_111_il2cpp_TypeInfo_var->static_fields)->___SP_3;
		bool L_3 = Object_op_Inequality_m6_580(NULL /*static, unused*/, L_2, __this, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_0044;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(PhotonHandler_t8_111_il2cpp_TypeInfo_var);
		PhotonHandler_t8_111 * L_4 = ((PhotonHandler_t8_111_StaticFields*)PhotonHandler_t8_111_il2cpp_TypeInfo_var->static_fields)->___SP_3;
		NullCheck(L_4);
		GameObject_t6_85 * L_5 = Component_get_gameObject_m6_583(L_4, /*hidden argument*/NULL);
		bool L_6 = Object_op_Inequality_m6_580(NULL /*static, unused*/, L_5, (Object_t6_5 *)NULL, /*hidden argument*/NULL);
		if (!L_6)
		{
			goto IL_0044;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(PhotonHandler_t8_111_il2cpp_TypeInfo_var);
		PhotonHandler_t8_111 * L_7 = ((PhotonHandler_t8_111_StaticFields*)PhotonHandler_t8_111_il2cpp_TypeInfo_var->static_fields)->___SP_3;
		NullCheck(L_7);
		GameObject_t6_85 * L_8 = Component_get_gameObject_m6_583(L_7, /*hidden argument*/NULL);
		Object_DestroyImmediate_m6_560(NULL /*static, unused*/, L_8, /*hidden argument*/NULL);
	}

IL_0044:
	{
		IL2CPP_RUNTIME_CLASS_INIT(PhotonHandler_t8_111_il2cpp_TypeInfo_var);
		((PhotonHandler_t8_111_StaticFields*)PhotonHandler_t8_111_il2cpp_TypeInfo_var->static_fields)->___SP_3 = __this;
		GameObject_t6_85 * L_9 = Component_get_gameObject_m6_583(__this, /*hidden argument*/NULL);
		Object_DontDestroyOnLoad_m6_564(NULL /*static, unused*/, L_9, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(PhotonNetwork_t8_114_il2cpp_TypeInfo_var);
		int32_t L_10 = PhotonNetwork_get_sendRate_m8_638(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->___updateInterval_4 = ((int32_t)((int32_t)((int32_t)1000)/(int32_t)L_10));
		int32_t L_11 = PhotonNetwork_get_sendRateOnSerialize_m8_640(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->___updateIntervalOnSerialize_5 = ((int32_t)((int32_t)((int32_t)1000)/(int32_t)L_11));
		PhotonHandler_StartFallbackSendAckThread_m8_581(NULL /*static, unused*/, /*hidden argument*/NULL);
		return;
	}
}
// System.Void PhotonHandler::OnApplicationQuit()
extern TypeInfo* PhotonHandler_t8_111_il2cpp_TypeInfo_var;
extern TypeInfo* PhotonNetwork_t8_114_il2cpp_TypeInfo_var;
extern "C" void PhotonHandler_OnApplicationQuit_m8_574 (PhotonHandler_t8_111 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		PhotonHandler_t8_111_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1120);
		PhotonNetwork_t8_114_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1058);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(PhotonHandler_t8_111_il2cpp_TypeInfo_var);
		((PhotonHandler_t8_111_StaticFields*)PhotonHandler_t8_111_il2cpp_TypeInfo_var->static_fields)->___AppQuits_10 = 1;
		PhotonHandler_StopFallbackSendAckThread_m8_582(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(PhotonNetwork_t8_114_il2cpp_TypeInfo_var);
		PhotonNetwork_Disconnect_m8_676(NULL /*static, unused*/, /*hidden argument*/NULL);
		return;
	}
}
// System.Void PhotonHandler::OnApplicationPause(System.Boolean)
extern TypeInfo* PhotonNetwork_t8_114_il2cpp_TypeInfo_var;
extern TypeInfo* PhotonHandler_t8_111_il2cpp_TypeInfo_var;
extern TypeInfo* Stopwatch_t3_20_il2cpp_TypeInfo_var;
extern "C" void PhotonHandler_OnApplicationPause_m8_575 (PhotonHandler_t8_111 * __this, bool ___pause, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		PhotonNetwork_t8_114_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1058);
		PhotonHandler_t8_111_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1120);
		Stopwatch_t3_20_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(629);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(PhotonNetwork_t8_114_il2cpp_TypeInfo_var);
		float L_0 = ((PhotonNetwork_t8_114_StaticFields*)PhotonNetwork_t8_114_il2cpp_TypeInfo_var->static_fields)->___BackgroundTimeout_25;
		if ((!(((float)L_0) > ((float)(0.001f)))))
		{
			goto IL_004c;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(PhotonHandler_t8_111_il2cpp_TypeInfo_var);
		Stopwatch_t3_20 * L_1 = ((PhotonHandler_t8_111_StaticFields*)PhotonHandler_t8_111_il2cpp_TypeInfo_var->static_fields)->___timerToStopConnectionInBackground_9;
		if (L_1)
		{
			goto IL_0023;
		}
	}
	{
		Stopwatch_t3_20 * L_2 = (Stopwatch_t3_20 *)il2cpp_codegen_object_new (Stopwatch_t3_20_il2cpp_TypeInfo_var);
		Stopwatch__ctor_m3_106(L_2, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(PhotonHandler_t8_111_il2cpp_TypeInfo_var);
		((PhotonHandler_t8_111_StaticFields*)PhotonHandler_t8_111_il2cpp_TypeInfo_var->static_fields)->___timerToStopConnectionInBackground_9 = L_2;
	}

IL_0023:
	{
		bool L_3 = ___pause;
		if (!L_3)
		{
			goto IL_0042;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(PhotonHandler_t8_111_il2cpp_TypeInfo_var);
		Stopwatch_t3_20 * L_4 = ((PhotonHandler_t8_111_StaticFields*)PhotonHandler_t8_111_il2cpp_TypeInfo_var->static_fields)->___timerToStopConnectionInBackground_9;
		NullCheck(L_4);
		Stopwatch_Reset_m3_112(L_4, /*hidden argument*/NULL);
		Stopwatch_t3_20 * L_5 = ((PhotonHandler_t8_111_StaticFields*)PhotonHandler_t8_111_il2cpp_TypeInfo_var->static_fields)->___timerToStopConnectionInBackground_9;
		NullCheck(L_5);
		Stopwatch_Start_m3_113(L_5, /*hidden argument*/NULL);
		goto IL_004c;
	}

IL_0042:
	{
		IL2CPP_RUNTIME_CLASS_INIT(PhotonHandler_t8_111_il2cpp_TypeInfo_var);
		Stopwatch_t3_20 * L_6 = ((PhotonHandler_t8_111_StaticFields*)PhotonHandler_t8_111_il2cpp_TypeInfo_var->static_fields)->___timerToStopConnectionInBackground_9;
		NullCheck(L_6);
		Stopwatch_Stop_m3_114(L_6, /*hidden argument*/NULL);
	}

IL_004c:
	{
		return;
	}
}
// System.Void PhotonHandler::OnDestroy()
extern TypeInfo* PhotonHandler_t8_111_il2cpp_TypeInfo_var;
extern "C" void PhotonHandler_OnDestroy_m8_576 (PhotonHandler_t8_111 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		PhotonHandler_t8_111_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1120);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(PhotonHandler_t8_111_il2cpp_TypeInfo_var);
		PhotonHandler_StopFallbackSendAckThread_m8_582(NULL /*static, unused*/, /*hidden argument*/NULL);
		return;
	}
}
// System.Void PhotonHandler::Update()
extern TypeInfo* PhotonNetwork_t8_114_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3385;
extern "C" void PhotonHandler_Update_m8_577 (PhotonHandler_t8_111 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		PhotonNetwork_t8_114_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1058);
		_stringLiteral3385 = il2cpp_codegen_string_literal_from_index(3385);
		s_Il2CppMethodIntialized = true;
	}
	bool V_0 = false;
	int32_t V_1 = 0;
	bool V_2 = false;
	{
		IL2CPP_RUNTIME_CLASS_INIT(PhotonNetwork_t8_114_il2cpp_TypeInfo_var);
		NetworkingPeer_t8_98 * L_0 = ((PhotonNetwork_t8_114_StaticFields*)PhotonNetwork_t8_114_il2cpp_TypeInfo_var->static_fields)->___networkingPeer_4;
		if (L_0)
		{
			goto IL_0015;
		}
	}
	{
		Debug_LogError_m6_490(NULL /*static, unused*/, _stringLiteral3385, /*hidden argument*/NULL);
		return;
	}

IL_0015:
	{
		IL2CPP_RUNTIME_CLASS_INIT(PhotonNetwork_t8_114_il2cpp_TypeInfo_var);
		int32_t L_1 = PhotonNetwork_get_connectionStateDetailed_m8_607(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((((int32_t)L_1) == ((int32_t)1)))
		{
			goto IL_0036;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(PhotonNetwork_t8_114_il2cpp_TypeInfo_var);
		int32_t L_2 = PhotonNetwork_get_connectionStateDetailed_m8_607(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((((int32_t)L_2) == ((int32_t)((int32_t)15))))
		{
			goto IL_0036;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(PhotonNetwork_t8_114_il2cpp_TypeInfo_var);
		bool L_3 = PhotonNetwork_get_offlineMode_m8_623(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_0037;
		}
	}

IL_0036:
	{
		return;
	}

IL_0037:
	{
		IL2CPP_RUNTIME_CLASS_INIT(PhotonNetwork_t8_114_il2cpp_TypeInfo_var);
		bool L_4 = PhotonNetwork_get_isMessageQueueRunning_m8_642(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (L_4)
		{
			goto IL_0042;
		}
	}
	{
		return;
	}

IL_0042:
	{
		V_0 = 1;
		goto IL_0054;
	}

IL_0049:
	{
		IL2CPP_RUNTIME_CLASS_INIT(PhotonNetwork_t8_114_il2cpp_TypeInfo_var);
		NetworkingPeer_t8_98 * L_5 = ((PhotonNetwork_t8_114_StaticFields*)PhotonNetwork_t8_114_il2cpp_TypeInfo_var->static_fields)->___networkingPeer_4;
		NullCheck(L_5);
		bool L_6 = (bool)VirtFuncInvoker0< bool >::Invoke(13 /* System.Boolean ExitGames.Client.Photon.PhotonPeer::DispatchIncomingCommands() */, L_5);
		V_0 = L_6;
	}

IL_0054:
	{
		IL2CPP_RUNTIME_CLASS_INIT(PhotonNetwork_t8_114_il2cpp_TypeInfo_var);
		bool L_7 = PhotonNetwork_get_isMessageQueueRunning_m8_642(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (!L_7)
		{
			goto IL_0064;
		}
	}
	{
		bool L_8 = V_0;
		if (L_8)
		{
			goto IL_0049;
		}
	}

IL_0064:
	{
		float L_9 = Time_get_realtimeSinceStartup_m6_656(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_1 = (((int32_t)((int32_t)((float)((float)L_9*(float)(1000.0f))))));
		IL2CPP_RUNTIME_CLASS_INIT(PhotonNetwork_t8_114_il2cpp_TypeInfo_var);
		bool L_10 = PhotonNetwork_get_isMessageQueueRunning_m8_642(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (!L_10)
		{
			goto IL_00a6;
		}
	}
	{
		int32_t L_11 = V_1;
		int32_t L_12 = (__this->___nextSendTickCountOnSerialize_7);
		if ((((int32_t)L_11) <= ((int32_t)L_12)))
		{
			goto IL_00a6;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(PhotonNetwork_t8_114_il2cpp_TypeInfo_var);
		NetworkingPeer_t8_98 * L_13 = ((PhotonNetwork_t8_114_StaticFields*)PhotonNetwork_t8_114_il2cpp_TypeInfo_var->static_fields)->___networkingPeer_4;
		NullCheck(L_13);
		NetworkingPeer_RunViewUpdate_m8_482(L_13, /*hidden argument*/NULL);
		int32_t L_14 = V_1;
		int32_t L_15 = (__this->___updateIntervalOnSerialize_5);
		__this->___nextSendTickCountOnSerialize_7 = ((int32_t)((int32_t)L_14+(int32_t)L_15));
		__this->___nextSendTickCount_6 = 0;
	}

IL_00a6:
	{
		float L_16 = Time_get_realtimeSinceStartup_m6_656(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_1 = (((int32_t)((int32_t)((float)((float)L_16*(float)(1000.0f))))));
		int32_t L_17 = V_1;
		int32_t L_18 = (__this->___nextSendTickCount_6);
		if ((((int32_t)L_17) <= ((int32_t)L_18)))
		{
			goto IL_00ef;
		}
	}
	{
		V_2 = 1;
		goto IL_00d1;
	}

IL_00c6:
	{
		IL2CPP_RUNTIME_CLASS_INIT(PhotonNetwork_t8_114_il2cpp_TypeInfo_var);
		NetworkingPeer_t8_98 * L_19 = ((PhotonNetwork_t8_114_StaticFields*)PhotonNetwork_t8_114_il2cpp_TypeInfo_var->static_fields)->___networkingPeer_4;
		NullCheck(L_19);
		bool L_20 = (bool)VirtFuncInvoker0< bool >::Invoke(11 /* System.Boolean ExitGames.Client.Photon.PhotonPeer::SendOutgoingCommands() */, L_19);
		V_2 = L_20;
	}

IL_00d1:
	{
		IL2CPP_RUNTIME_CLASS_INIT(PhotonNetwork_t8_114_il2cpp_TypeInfo_var);
		bool L_21 = PhotonNetwork_get_isMessageQueueRunning_m8_642(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (!L_21)
		{
			goto IL_00e1;
		}
	}
	{
		bool L_22 = V_2;
		if (L_22)
		{
			goto IL_00c6;
		}
	}

IL_00e1:
	{
		int32_t L_23 = V_1;
		int32_t L_24 = (__this->___updateInterval_4);
		__this->___nextSendTickCount_6 = ((int32_t)((int32_t)L_23+(int32_t)L_24));
	}

IL_00ef:
	{
		return;
	}
}
// System.Void PhotonHandler::OnLevelWasLoaded(System.Int32)
extern TypeInfo* PhotonNetwork_t8_114_il2cpp_TypeInfo_var;
extern "C" void PhotonHandler_OnLevelWasLoaded_m8_578 (PhotonHandler_t8_111 * __this, int32_t ___level, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		PhotonNetwork_t8_114_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1058);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(PhotonNetwork_t8_114_il2cpp_TypeInfo_var);
		NetworkingPeer_t8_98 * L_0 = ((PhotonNetwork_t8_114_StaticFields*)PhotonNetwork_t8_114_il2cpp_TypeInfo_var->static_fields)->___networkingPeer_4;
		NullCheck(L_0);
		NetworkingPeer_NewSceneLoaded_m8_481(L_0, /*hidden argument*/NULL);
		NetworkingPeer_t8_98 * L_1 = ((PhotonNetwork_t8_114_StaticFields*)PhotonNetwork_t8_114_il2cpp_TypeInfo_var->static_fields)->___networkingPeer_4;
		String_t* L_2 = Application_get_loadedLevelName_m6_447(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_1);
		NetworkingPeer_SetLevelInPropsIfSynced_m8_491(L_1, L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Void PhotonHandler::OnJoinedRoom()
extern TypeInfo* PhotonNetwork_t8_114_il2cpp_TypeInfo_var;
extern "C" void PhotonHandler_OnJoinedRoom_m8_579 (PhotonHandler_t8_111 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		PhotonNetwork_t8_114_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1058);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(PhotonNetwork_t8_114_il2cpp_TypeInfo_var);
		NetworkingPeer_t8_98 * L_0 = ((PhotonNetwork_t8_114_StaticFields*)PhotonNetwork_t8_114_il2cpp_TypeInfo_var->static_fields)->___networkingPeer_4;
		NullCheck(L_0);
		NetworkingPeer_LoadLevelIfSynced_m8_490(L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void PhotonHandler::OnCreatedRoom()
extern TypeInfo* PhotonNetwork_t8_114_il2cpp_TypeInfo_var;
extern "C" void PhotonHandler_OnCreatedRoom_m8_580 (PhotonHandler_t8_111 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		PhotonNetwork_t8_114_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1058);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(PhotonNetwork_t8_114_il2cpp_TypeInfo_var);
		NetworkingPeer_t8_98 * L_0 = ((PhotonNetwork_t8_114_StaticFields*)PhotonNetwork_t8_114_il2cpp_TypeInfo_var->static_fields)->___networkingPeer_4;
		String_t* L_1 = Application_get_loadedLevelName_m6_447(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_0);
		NetworkingPeer_SetLevelInPropsIfSynced_m8_491(L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void PhotonHandler::StartFallbackSendAckThread()
extern TypeInfo* PhotonHandler_t8_111_il2cpp_TypeInfo_var;
extern TypeInfo* Func_1_t2_14_il2cpp_TypeInfo_var;
extern TypeInfo* SupportClass_t5_57_il2cpp_TypeInfo_var;
extern const MethodInfo* PhotonHandler_FallbackSendAckThread_m8_583_MethodInfo_var;
extern const MethodInfo* Func_1__ctor_m2_58_MethodInfo_var;
extern "C" void PhotonHandler_StartFallbackSendAckThread_m8_581 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		PhotonHandler_t8_111_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1120);
		Func_1_t2_14_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1153);
		SupportClass_t5_57_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(864);
		PhotonHandler_FallbackSendAckThread_m8_583_MethodInfo_var = il2cpp_codegen_method_info_from_index(286);
		Func_1__ctor_m2_58_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483935);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(PhotonHandler_t8_111_il2cpp_TypeInfo_var);
		bool L_0 = ((PhotonHandler_t8_111_StaticFields*)PhotonHandler_t8_111_il2cpp_TypeInfo_var->static_fields)->___sendThreadShouldRun_8;
		if (!L_0)
		{
			goto IL_000b;
		}
	}
	{
		return;
	}

IL_000b:
	{
		IL2CPP_RUNTIME_CLASS_INIT(PhotonHandler_t8_111_il2cpp_TypeInfo_var);
		((PhotonHandler_t8_111_StaticFields*)PhotonHandler_t8_111_il2cpp_TypeInfo_var->static_fields)->___sendThreadShouldRun_8 = 1;
		IntPtr_t L_1 = { (void*)PhotonHandler_FallbackSendAckThread_m8_583_MethodInfo_var };
		Func_1_t2_14 * L_2 = (Func_1_t2_14 *)il2cpp_codegen_object_new (Func_1_t2_14_il2cpp_TypeInfo_var);
		Func_1__ctor_m2_58(L_2, NULL, L_1, /*hidden argument*/Func_1__ctor_m2_58_MethodInfo_var);
		IL2CPP_RUNTIME_CLASS_INIT(SupportClass_t5_57_il2cpp_TypeInfo_var);
		SupportClass_CallInBackground_m5_368(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Void PhotonHandler::StopFallbackSendAckThread()
extern TypeInfo* PhotonHandler_t8_111_il2cpp_TypeInfo_var;
extern "C" void PhotonHandler_StopFallbackSendAckThread_m8_582 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		PhotonHandler_t8_111_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1120);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(PhotonHandler_t8_111_il2cpp_TypeInfo_var);
		((PhotonHandler_t8_111_StaticFields*)PhotonHandler_t8_111_il2cpp_TypeInfo_var->static_fields)->___sendThreadShouldRun_8 = 0;
		return;
	}
}
// System.Boolean PhotonHandler::FallbackSendAckThread()
extern TypeInfo* PhotonHandler_t8_111_il2cpp_TypeInfo_var;
extern TypeInfo* PhotonNetwork_t8_114_il2cpp_TypeInfo_var;
extern "C" bool PhotonHandler_FallbackSendAckThread_m8_583 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		PhotonHandler_t8_111_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1120);
		PhotonNetwork_t8_114_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1058);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(PhotonHandler_t8_111_il2cpp_TypeInfo_var);
		bool L_0 = ((PhotonHandler_t8_111_StaticFields*)PhotonHandler_t8_111_il2cpp_TypeInfo_var->static_fields)->___sendThreadShouldRun_8;
		if (!L_0)
		{
			goto IL_0072;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(PhotonNetwork_t8_114_il2cpp_TypeInfo_var);
		NetworkingPeer_t8_98 * L_1 = ((PhotonNetwork_t8_114_StaticFields*)PhotonNetwork_t8_114_il2cpp_TypeInfo_var->static_fields)->___networkingPeer_4;
		if (!L_1)
		{
			goto IL_0072;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(PhotonNetwork_t8_114_il2cpp_TypeInfo_var);
		NetworkingPeer_t8_98 * L_2 = ((PhotonNetwork_t8_114_StaticFields*)PhotonNetwork_t8_114_il2cpp_TypeInfo_var->static_fields)->___networkingPeer_4;
		NullCheck(L_2);
		VirtFuncInvoker0< bool >::Invoke(12 /* System.Boolean ExitGames.Client.Photon.PhotonPeer::SendAcksOnly() */, L_2);
		IL2CPP_RUNTIME_CLASS_INIT(PhotonHandler_t8_111_il2cpp_TypeInfo_var);
		Stopwatch_t3_20 * L_3 = ((PhotonHandler_t8_111_StaticFields*)PhotonHandler_t8_111_il2cpp_TypeInfo_var->static_fields)->___timerToStopConnectionInBackground_9;
		if (!L_3)
		{
			goto IL_0072;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(PhotonNetwork_t8_114_il2cpp_TypeInfo_var);
		float L_4 = ((PhotonNetwork_t8_114_StaticFields*)PhotonNetwork_t8_114_il2cpp_TypeInfo_var->static_fields)->___BackgroundTimeout_25;
		if ((!(((float)L_4) > ((float)(0.001f)))))
		{
			goto IL_0072;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(PhotonHandler_t8_111_il2cpp_TypeInfo_var);
		Stopwatch_t3_20 * L_5 = ((PhotonHandler_t8_111_StaticFields*)PhotonHandler_t8_111_il2cpp_TypeInfo_var->static_fields)->___timerToStopConnectionInBackground_9;
		NullCheck(L_5);
		int64_t L_6 = Stopwatch_get_ElapsedMilliseconds_m3_110(L_5, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(PhotonNetwork_t8_114_il2cpp_TypeInfo_var);
		float L_7 = ((PhotonNetwork_t8_114_StaticFields*)PhotonNetwork_t8_114_il2cpp_TypeInfo_var->static_fields)->___BackgroundTimeout_25;
		if ((!(((float)(((float)((float)L_6)))) > ((float)((float)((float)L_7*(float)(1000.0f)))))))
		{
			goto IL_0072;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(PhotonHandler_t8_111_il2cpp_TypeInfo_var);
		Stopwatch_t3_20 * L_8 = ((PhotonHandler_t8_111_StaticFields*)PhotonHandler_t8_111_il2cpp_TypeInfo_var->static_fields)->___timerToStopConnectionInBackground_9;
		NullCheck(L_8);
		Stopwatch_Stop_m3_114(L_8, /*hidden argument*/NULL);
		Stopwatch_t3_20 * L_9 = ((PhotonHandler_t8_111_StaticFields*)PhotonHandler_t8_111_il2cpp_TypeInfo_var->static_fields)->___timerToStopConnectionInBackground_9;
		NullCheck(L_9);
		Stopwatch_Reset_m3_112(L_9, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(PhotonNetwork_t8_114_il2cpp_TypeInfo_var);
		PhotonNetwork_Disconnect_m8_676(NULL /*static, unused*/, /*hidden argument*/NULL);
		bool L_10 = ((PhotonHandler_t8_111_StaticFields*)PhotonHandler_t8_111_il2cpp_TypeInfo_var->static_fields)->___sendThreadShouldRun_8;
		return L_10;
	}

IL_0072:
	{
		IL2CPP_RUNTIME_CLASS_INIT(PhotonHandler_t8_111_il2cpp_TypeInfo_var);
		bool L_11 = ((PhotonHandler_t8_111_StaticFields*)PhotonHandler_t8_111_il2cpp_TypeInfo_var->static_fields)->___sendThreadShouldRun_8;
		return L_11;
	}
}
// CloudRegionCode PhotonHandler::get_BestRegionCodeInPreferences()
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3386;
extern "C" int32_t PhotonHandler_get_BestRegionCodeInPreferences_m8_584 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(11);
		_stringLiteral3386 = il2cpp_codegen_string_literal_from_index(3386);
		s_Il2CppMethodIntialized = true;
	}
	String_t* V_0 = {0};
	int32_t V_1 = {0};
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_0 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->___Empty_2;
		String_t* L_1 = PlayerPrefs_GetString_m6_666(NULL /*static, unused*/, _stringLiteral3386, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		String_t* L_2 = V_0;
		bool L_3 = String_IsNullOrEmpty_m1_397(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		if (L_3)
		{
			goto IL_0024;
		}
	}
	{
		String_t* L_4 = V_0;
		int32_t L_5 = Region_Parse_m8_846(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
		V_1 = L_5;
		int32_t L_6 = V_1;
		return L_6;
	}

IL_0024:
	{
		return (int32_t)(4);
	}
}
// System.Void PhotonHandler::set_BestRegionCodeInPreferences(CloudRegionCode)
extern TypeInfo* CloudRegionCode_t8_65_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3386;
extern "C" void PhotonHandler_set_BestRegionCodeInPreferences_m8_585 (Object_t * __this /* static, unused */, int32_t ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CloudRegionCode_t8_65_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1123);
		_stringLiteral3386 = il2cpp_codegen_string_literal_from_index(3386);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = ___value;
		if ((!(((uint32_t)L_0) == ((uint32_t)4))))
		{
			goto IL_0016;
		}
	}
	{
		PlayerPrefs_DeleteKey_m6_668(NULL /*static, unused*/, _stringLiteral3386, /*hidden argument*/NULL);
		goto IL_002b;
	}

IL_0016:
	{
		int32_t L_1 = ___value;
		int32_t L_2 = L_1;
		Object_t * L_3 = Box(CloudRegionCode_t8_65_il2cpp_TypeInfo_var, &L_2);
		NullCheck(L_3);
		String_t* L_4 = (String_t*)VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Enum::ToString() */, L_3);
		PlayerPrefs_SetString_m6_665(NULL /*static, unused*/, _stringLiteral3386, L_4, /*hidden argument*/NULL);
	}

IL_002b:
	{
		return;
	}
}
// System.Void PhotonHandler::PingAvailableRegionsAndConnectToBest()
extern TypeInfo* PhotonHandler_t8_111_il2cpp_TypeInfo_var;
extern "C" void PhotonHandler_PingAvailableRegionsAndConnectToBest_m8_586 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		PhotonHandler_t8_111_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1120);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(PhotonHandler_t8_111_il2cpp_TypeInfo_var);
		PhotonHandler_t8_111 * L_0 = ((PhotonHandler_t8_111_StaticFields*)PhotonHandler_t8_111_il2cpp_TypeInfo_var->static_fields)->___SP_3;
		PhotonHandler_t8_111 * L_1 = ((PhotonHandler_t8_111_StaticFields*)PhotonHandler_t8_111_il2cpp_TypeInfo_var->static_fields)->___SP_3;
		NullCheck(L_1);
		Object_t * L_2 = PhotonHandler_PingAvailableRegionsCoroutine_m8_587(L_1, 1, /*hidden argument*/NULL);
		NullCheck(L_0);
		MonoBehaviour_StartCoroutine_m6_530(L_0, L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Collections.IEnumerator PhotonHandler::PingAvailableRegionsCoroutine(System.Boolean)
extern TypeInfo* U3CPingAvailableRegionsCoroutineU3Ec__Iterator0_t8_108_il2cpp_TypeInfo_var;
extern "C" Object_t * PhotonHandler_PingAvailableRegionsCoroutine_m8_587 (PhotonHandler_t8_111 * __this, bool ___connectToBest, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		U3CPingAvailableRegionsCoroutineU3Ec__Iterator0_t8_108_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1154);
		s_Il2CppMethodIntialized = true;
	}
	U3CPingAvailableRegionsCoroutineU3Ec__Iterator0_t8_108 * V_0 = {0};
	{
		U3CPingAvailableRegionsCoroutineU3Ec__Iterator0_t8_108 * L_0 = (U3CPingAvailableRegionsCoroutineU3Ec__Iterator0_t8_108 *)il2cpp_codegen_object_new (U3CPingAvailableRegionsCoroutineU3Ec__Iterator0_t8_108_il2cpp_TypeInfo_var);
		U3CPingAvailableRegionsCoroutineU3Ec__Iterator0__ctor_m8_565(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CPingAvailableRegionsCoroutineU3Ec__Iterator0_t8_108 * L_1 = V_0;
		bool L_2 = ___connectToBest;
		NullCheck(L_1);
		L_1->___connectToBest_4 = L_2;
		U3CPingAvailableRegionsCoroutineU3Ec__Iterator0_t8_108 * L_3 = V_0;
		bool L_4 = ___connectToBest;
		NullCheck(L_3);
		L_3->___U3CU24U3EconnectToBest_7 = L_4;
		U3CPingAvailableRegionsCoroutineU3Ec__Iterator0_t8_108 * L_5 = V_0;
		return L_5;
	}
}
// System.Void PhotonLagSimulationGui::.ctor()
extern "C" void PhotonLagSimulationGui__ctor_m8_588 (PhotonLagSimulationGui_t8_112 * __this, const MethodInfo* method)
{
	{
		Rect_t6_52  L_0 = {0};
		Rect__ctor_m6_270(&L_0, (0.0f), (100.0f), (120.0f), (100.0f), /*hidden argument*/NULL);
		__this->___WindowRect_2 = L_0;
		__this->___WindowId_3 = ((int32_t)101);
		__this->___Visible_4 = 1;
		MonoBehaviour__ctor_m6_525(__this, /*hidden argument*/NULL);
		return;
	}
}
// ExitGames.Client.Photon.PhotonPeer PhotonLagSimulationGui::get_Peer()
extern "C" PhotonPeer_t5_38 * PhotonLagSimulationGui_get_Peer_m8_589 (PhotonLagSimulationGui_t8_112 * __this, const MethodInfo* method)
{
	{
		PhotonPeer_t5_38 * L_0 = (__this->___U3CPeerU3Ek__BackingField_5);
		return L_0;
	}
}
// System.Void PhotonLagSimulationGui::set_Peer(ExitGames.Client.Photon.PhotonPeer)
extern "C" void PhotonLagSimulationGui_set_Peer_m8_590 (PhotonLagSimulationGui_t8_112 * __this, PhotonPeer_t5_38 * ___value, const MethodInfo* method)
{
	{
		PhotonPeer_t5_38 * L_0 = ___value;
		__this->___U3CPeerU3Ek__BackingField_5 = L_0;
		return;
	}
}
// System.Void PhotonLagSimulationGui::Start()
extern TypeInfo* PhotonNetwork_t8_114_il2cpp_TypeInfo_var;
extern "C" void PhotonLagSimulationGui_Start_m8_591 (PhotonLagSimulationGui_t8_112 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		PhotonNetwork_t8_114_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1058);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(PhotonNetwork_t8_114_il2cpp_TypeInfo_var);
		NetworkingPeer_t8_98 * L_0 = ((PhotonNetwork_t8_114_StaticFields*)PhotonNetwork_t8_114_il2cpp_TypeInfo_var->static_fields)->___networkingPeer_4;
		PhotonLagSimulationGui_set_Peer_m8_590(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void PhotonLagSimulationGui::OnGUI()
extern TypeInfo* WindowFunction_t6_159_il2cpp_TypeInfo_var;
extern TypeInfo* GUILayoutOptionU5BU5D_t6_165_il2cpp_TypeInfo_var;
extern const MethodInfo* PhotonLagSimulationGui_NetSimHasNoPeerWindow_m8_593_MethodInfo_var;
extern const MethodInfo* PhotonLagSimulationGui_NetSimWindow_m8_594_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral3394;
extern "C" void PhotonLagSimulationGui_OnGUI_m8_592 (PhotonLagSimulationGui_t8_112 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WindowFunction_t6_159_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(997);
		GUILayoutOptionU5BU5D_t6_165_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(991);
		PhotonLagSimulationGui_NetSimHasNoPeerWindow_m8_593_MethodInfo_var = il2cpp_codegen_method_info_from_index(288);
		PhotonLagSimulationGui_NetSimWindow_m8_594_MethodInfo_var = il2cpp_codegen_method_info_from_index(289);
		_stringLiteral3394 = il2cpp_codegen_string_literal_from_index(3394);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool L_0 = (__this->___Visible_4);
		if (L_0)
		{
			goto IL_000c;
		}
	}
	{
		return;
	}

IL_000c:
	{
		PhotonPeer_t5_38 * L_1 = PhotonLagSimulationGui_get_Peer_m8_589(__this, /*hidden argument*/NULL);
		if (L_1)
		{
			goto IL_004a;
		}
	}
	{
		int32_t L_2 = (__this->___WindowId_3);
		Rect_t6_52  L_3 = (__this->___WindowRect_2);
		IntPtr_t L_4 = { (void*)PhotonLagSimulationGui_NetSimHasNoPeerWindow_m8_593_MethodInfo_var };
		WindowFunction_t6_159 * L_5 = (WindowFunction_t6_159 *)il2cpp_codegen_object_new (WindowFunction_t6_159_il2cpp_TypeInfo_var);
		WindowFunction__ctor_m6_1031(L_5, __this, L_4, /*hidden argument*/NULL);
		Rect_t6_52  L_6 = GUILayout_Window_m6_1141(NULL /*static, unused*/, L_2, L_3, L_5, _stringLiteral3394, ((GUILayoutOptionU5BU5D_t6_165*)SZArrayNew(GUILayoutOptionU5BU5D_t6_165_il2cpp_TypeInfo_var, 0)), /*hidden argument*/NULL);
		__this->___WindowRect_2 = L_6;
		goto IL_0078;
	}

IL_004a:
	{
		int32_t L_7 = (__this->___WindowId_3);
		Rect_t6_52  L_8 = (__this->___WindowRect_2);
		IntPtr_t L_9 = { (void*)PhotonLagSimulationGui_NetSimWindow_m8_594_MethodInfo_var };
		WindowFunction_t6_159 * L_10 = (WindowFunction_t6_159 *)il2cpp_codegen_object_new (WindowFunction_t6_159_il2cpp_TypeInfo_var);
		WindowFunction__ctor_m6_1031(L_10, __this, L_9, /*hidden argument*/NULL);
		Rect_t6_52  L_11 = GUILayout_Window_m6_1141(NULL /*static, unused*/, L_7, L_8, L_10, _stringLiteral3394, ((GUILayoutOptionU5BU5D_t6_165*)SZArrayNew(GUILayoutOptionU5BU5D_t6_165_il2cpp_TypeInfo_var, 0)), /*hidden argument*/NULL);
		__this->___WindowRect_2 = L_11;
	}

IL_0078:
	{
		return;
	}
}
// System.Void PhotonLagSimulationGui::NetSimHasNoPeerWindow(System.Int32)
extern TypeInfo* GUILayoutOptionU5BU5D_t6_165_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3395;
extern "C" void PhotonLagSimulationGui_NetSimHasNoPeerWindow_m8_593 (PhotonLagSimulationGui_t8_112 * __this, int32_t ___windowId, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GUILayoutOptionU5BU5D_t6_165_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(991);
		_stringLiteral3395 = il2cpp_codegen_string_literal_from_index(3395);
		s_Il2CppMethodIntialized = true;
	}
	{
		GUILayout_Label_m6_1112(NULL /*static, unused*/, _stringLiteral3395, ((GUILayoutOptionU5BU5D_t6_165*)SZArrayNew(GUILayoutOptionU5BU5D_t6_165_il2cpp_TypeInfo_var, 0)), /*hidden argument*/NULL);
		return;
	}
}
// System.Void PhotonLagSimulationGui::NetSimWindow(System.Int32)
extern TypeInfo* Int32_t1_3_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* GUILayoutOptionU5BU5D_t6_165_il2cpp_TypeInfo_var;
extern TypeInfo* Single_t1_17_il2cpp_TypeInfo_var;
extern TypeInfo* GUI_t6_160_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3396;
extern Il2CppCodeGenString* _stringLiteral3397;
extern Il2CppCodeGenString* _stringLiteral3398;
extern Il2CppCodeGenString* _stringLiteral3399;
extern Il2CppCodeGenString* _stringLiteral3400;
extern "C" void PhotonLagSimulationGui_NetSimWindow_m8_594 (PhotonLagSimulationGui_t8_112 * __this, int32_t ___windowId, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Int32_t1_3_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(8);
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(11);
		GUILayoutOptionU5BU5D_t6_165_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(991);
		Single_t1_17_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(43);
		GUI_t6_160_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(982);
		_stringLiteral3396 = il2cpp_codegen_string_literal_from_index(3396);
		_stringLiteral3397 = il2cpp_codegen_string_literal_from_index(3397);
		_stringLiteral3398 = il2cpp_codegen_string_literal_from_index(3398);
		_stringLiteral3399 = il2cpp_codegen_string_literal_from_index(3399);
		_stringLiteral3400 = il2cpp_codegen_string_literal_from_index(3400);
		s_Il2CppMethodIntialized = true;
	}
	bool V_0 = false;
	bool V_1 = false;
	float V_2 = 0.0f;
	float V_3 = 0.0f;
	float V_4 = 0.0f;
	{
		PhotonPeer_t5_38 * L_0 = PhotonLagSimulationGui_get_Peer_m8_589(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		int32_t L_1 = PhotonPeer_get_RoundTripTime_m5_224(L_0, /*hidden argument*/NULL);
		int32_t L_2 = L_1;
		Object_t * L_3 = Box(Int32_t1_3_il2cpp_TypeInfo_var, &L_2);
		PhotonPeer_t5_38 * L_4 = PhotonLagSimulationGui_get_Peer_m8_589(__this, /*hidden argument*/NULL);
		NullCheck(L_4);
		int32_t L_5 = PhotonPeer_get_RoundTripTimeVariance_m5_225(L_4, /*hidden argument*/NULL);
		int32_t L_6 = L_5;
		Object_t * L_7 = Box(Int32_t1_3_il2cpp_TypeInfo_var, &L_6);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_8 = String_Format_m1_411(NULL /*static, unused*/, _stringLiteral3396, L_3, L_7, /*hidden argument*/NULL);
		GUILayout_Label_m6_1112(NULL /*static, unused*/, L_8, ((GUILayoutOptionU5BU5D_t6_165*)SZArrayNew(GUILayoutOptionU5BU5D_t6_165_il2cpp_TypeInfo_var, 0)), /*hidden argument*/NULL);
		PhotonPeer_t5_38 * L_9 = PhotonLagSimulationGui_get_Peer_m8_589(__this, /*hidden argument*/NULL);
		NullCheck(L_9);
		bool L_10 = (bool)VirtFuncInvoker0< bool >::Invoke(4 /* System.Boolean ExitGames.Client.Photon.PhotonPeer::get_IsSimulationEnabled() */, L_9);
		V_0 = L_10;
		bool L_11 = V_0;
		bool L_12 = GUILayout_Toggle_m6_1119(NULL /*static, unused*/, L_11, _stringLiteral3397, ((GUILayoutOptionU5BU5D_t6_165*)SZArrayNew(GUILayoutOptionU5BU5D_t6_165_il2cpp_TypeInfo_var, 0)), /*hidden argument*/NULL);
		V_1 = L_12;
		bool L_13 = V_1;
		bool L_14 = V_0;
		if ((((int32_t)L_13) == ((int32_t)L_14)))
		{
			goto IL_0066;
		}
	}
	{
		PhotonPeer_t5_38 * L_15 = PhotonLagSimulationGui_get_Peer_m8_589(__this, /*hidden argument*/NULL);
		bool L_16 = V_1;
		NullCheck(L_15);
		VirtActionInvoker1< bool >::Invoke(5 /* System.Void ExitGames.Client.Photon.PhotonPeer::set_IsSimulationEnabled(System.Boolean) */, L_15, L_16);
	}

IL_0066:
	{
		PhotonPeer_t5_38 * L_17 = PhotonLagSimulationGui_get_Peer_m8_589(__this, /*hidden argument*/NULL);
		NullCheck(L_17);
		NetworkSimulationSet_t5_16 * L_18 = PhotonPeer_get_NetworkSimulationSettings_m5_231(L_17, /*hidden argument*/NULL);
		NullCheck(L_18);
		int32_t L_19 = NetworkSimulationSet_get_IncomingLag_m5_174(L_18, /*hidden argument*/NULL);
		V_2 = (((float)((float)L_19)));
		float L_20 = V_2;
		float L_21 = L_20;
		Object_t * L_22 = Box(Single_t1_17_il2cpp_TypeInfo_var, &L_21);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_23 = String_Concat_m1_416(NULL /*static, unused*/, _stringLiteral3398, L_22, /*hidden argument*/NULL);
		GUILayout_Label_m6_1112(NULL /*static, unused*/, L_23, ((GUILayoutOptionU5BU5D_t6_165*)SZArrayNew(GUILayoutOptionU5BU5D_t6_165_il2cpp_TypeInfo_var, 0)), /*hidden argument*/NULL);
		float L_24 = V_2;
		float L_25 = GUILayout_HorizontalSlider_m6_1123(NULL /*static, unused*/, L_24, (0.0f), (500.0f), ((GUILayoutOptionU5BU5D_t6_165*)SZArrayNew(GUILayoutOptionU5BU5D_t6_165_il2cpp_TypeInfo_var, 0)), /*hidden argument*/NULL);
		V_2 = L_25;
		PhotonPeer_t5_38 * L_26 = PhotonLagSimulationGui_get_Peer_m8_589(__this, /*hidden argument*/NULL);
		NullCheck(L_26);
		NetworkSimulationSet_t5_16 * L_27 = PhotonPeer_get_NetworkSimulationSettings_m5_231(L_26, /*hidden argument*/NULL);
		float L_28 = V_2;
		NullCheck(L_27);
		NetworkSimulationSet_set_IncomingLag_m5_175(L_27, (((int32_t)((int32_t)L_28))), /*hidden argument*/NULL);
		PhotonPeer_t5_38 * L_29 = PhotonLagSimulationGui_get_Peer_m8_589(__this, /*hidden argument*/NULL);
		NullCheck(L_29);
		NetworkSimulationSet_t5_16 * L_30 = PhotonPeer_get_NetworkSimulationSettings_m5_231(L_29, /*hidden argument*/NULL);
		float L_31 = V_2;
		NullCheck(L_30);
		NetworkSimulationSet_set_OutgoingLag_m5_169(L_30, (((int32_t)((int32_t)L_31))), /*hidden argument*/NULL);
		PhotonPeer_t5_38 * L_32 = PhotonLagSimulationGui_get_Peer_m8_589(__this, /*hidden argument*/NULL);
		NullCheck(L_32);
		NetworkSimulationSet_t5_16 * L_33 = PhotonPeer_get_NetworkSimulationSettings_m5_231(L_32, /*hidden argument*/NULL);
		NullCheck(L_33);
		int32_t L_34 = NetworkSimulationSet_get_IncomingJitter_m5_176(L_33, /*hidden argument*/NULL);
		V_3 = (((float)((float)L_34)));
		float L_35 = V_3;
		float L_36 = L_35;
		Object_t * L_37 = Box(Single_t1_17_il2cpp_TypeInfo_var, &L_36);
		String_t* L_38 = String_Concat_m1_416(NULL /*static, unused*/, _stringLiteral3399, L_37, /*hidden argument*/NULL);
		GUILayout_Label_m6_1112(NULL /*static, unused*/, L_38, ((GUILayoutOptionU5BU5D_t6_165*)SZArrayNew(GUILayoutOptionU5BU5D_t6_165_il2cpp_TypeInfo_var, 0)), /*hidden argument*/NULL);
		float L_39 = V_3;
		float L_40 = GUILayout_HorizontalSlider_m6_1123(NULL /*static, unused*/, L_39, (0.0f), (100.0f), ((GUILayoutOptionU5BU5D_t6_165*)SZArrayNew(GUILayoutOptionU5BU5D_t6_165_il2cpp_TypeInfo_var, 0)), /*hidden argument*/NULL);
		V_3 = L_40;
		PhotonPeer_t5_38 * L_41 = PhotonLagSimulationGui_get_Peer_m8_589(__this, /*hidden argument*/NULL);
		NullCheck(L_41);
		NetworkSimulationSet_t5_16 * L_42 = PhotonPeer_get_NetworkSimulationSettings_m5_231(L_41, /*hidden argument*/NULL);
		float L_43 = V_3;
		NullCheck(L_42);
		NetworkSimulationSet_set_IncomingJitter_m5_177(L_42, (((int32_t)((int32_t)L_43))), /*hidden argument*/NULL);
		PhotonPeer_t5_38 * L_44 = PhotonLagSimulationGui_get_Peer_m8_589(__this, /*hidden argument*/NULL);
		NullCheck(L_44);
		NetworkSimulationSet_t5_16 * L_45 = PhotonPeer_get_NetworkSimulationSettings_m5_231(L_44, /*hidden argument*/NULL);
		float L_46 = V_3;
		NullCheck(L_45);
		NetworkSimulationSet_set_OutgoingJitter_m5_171(L_45, (((int32_t)((int32_t)L_46))), /*hidden argument*/NULL);
		PhotonPeer_t5_38 * L_47 = PhotonLagSimulationGui_get_Peer_m8_589(__this, /*hidden argument*/NULL);
		NullCheck(L_47);
		NetworkSimulationSet_t5_16 * L_48 = PhotonPeer_get_NetworkSimulationSettings_m5_231(L_47, /*hidden argument*/NULL);
		NullCheck(L_48);
		int32_t L_49 = NetworkSimulationSet_get_IncomingLossPercentage_m5_178(L_48, /*hidden argument*/NULL);
		V_4 = (((float)((float)L_49)));
		float L_50 = V_4;
		float L_51 = L_50;
		Object_t * L_52 = Box(Single_t1_17_il2cpp_TypeInfo_var, &L_51);
		String_t* L_53 = String_Concat_m1_416(NULL /*static, unused*/, _stringLiteral3400, L_52, /*hidden argument*/NULL);
		GUILayout_Label_m6_1112(NULL /*static, unused*/, L_53, ((GUILayoutOptionU5BU5D_t6_165*)SZArrayNew(GUILayoutOptionU5BU5D_t6_165_il2cpp_TypeInfo_var, 0)), /*hidden argument*/NULL);
		float L_54 = V_4;
		float L_55 = GUILayout_HorizontalSlider_m6_1123(NULL /*static, unused*/, L_54, (0.0f), (10.0f), ((GUILayoutOptionU5BU5D_t6_165*)SZArrayNew(GUILayoutOptionU5BU5D_t6_165_il2cpp_TypeInfo_var, 0)), /*hidden argument*/NULL);
		V_4 = L_55;
		PhotonPeer_t5_38 * L_56 = PhotonLagSimulationGui_get_Peer_m8_589(__this, /*hidden argument*/NULL);
		NullCheck(L_56);
		NetworkSimulationSet_t5_16 * L_57 = PhotonPeer_get_NetworkSimulationSettings_m5_231(L_56, /*hidden argument*/NULL);
		float L_58 = V_4;
		NullCheck(L_57);
		NetworkSimulationSet_set_IncomingLossPercentage_m5_179(L_57, (((int32_t)((int32_t)L_58))), /*hidden argument*/NULL);
		PhotonPeer_t5_38 * L_59 = PhotonLagSimulationGui_get_Peer_m8_589(__this, /*hidden argument*/NULL);
		NullCheck(L_59);
		NetworkSimulationSet_t5_16 * L_60 = PhotonPeer_get_NetworkSimulationSettings_m5_231(L_59, /*hidden argument*/NULL);
		float L_61 = V_4;
		NullCheck(L_60);
		NetworkSimulationSet_set_OutgoingLossPercentage_m5_173(L_60, (((int32_t)((int32_t)L_61))), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t6_160_il2cpp_TypeInfo_var);
		bool L_62 = GUI_get_changed_m6_1080(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (!L_62)
		{
			goto IL_01be;
		}
	}
	{
		Rect_t6_52 * L_63 = &(__this->___WindowRect_2);
		Rect_set_height_m6_280(L_63, (100.0f), /*hidden argument*/NULL);
	}

IL_01be:
	{
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t6_160_il2cpp_TypeInfo_var);
		GUI_DragWindow_m6_1077(NULL /*static, unused*/, /*hidden argument*/NULL);
		return;
	}
}
// System.Void PhotonNetwork/EventCallback::.ctor(System.Object,System.IntPtr)
extern "C" void EventCallback__ctor_m8_595 (EventCallback_t8_113 * __this, Object_t * ___object, IntPtr_t ___method, const MethodInfo* method)
{
	__this->___method_ptr_0 = (methodPointerType)((MethodInfo*)___method.___m_value_0)->method;
	__this->___method_3 = ___method;
	__this->___m_target_2 = ___object;
}
// System.Void PhotonNetwork/EventCallback::Invoke(System.Byte,System.Object,System.Int32)
extern "C" void EventCallback_Invoke_m8_596 (EventCallback_t8_113 * __this, uint8_t ___eventCode, Object_t * ___content, int32_t ___senderId, const MethodInfo* method)
{
	if(__this->___prev_9 != NULL)
	{
		EventCallback_Invoke_m8_596((EventCallback_t8_113 *)__this->___prev_9,___eventCode, ___content, ___senderId, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->___method_3.___m_value_0));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->___method_3.___m_value_0));
	if (__this->___m_target_2 != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Object_t *, Object_t * __this, uint8_t ___eventCode, Object_t * ___content, int32_t ___senderId, const MethodInfo* method);
		((FunctionPointerType)__this->___method_ptr_0)(NULL,__this->___m_target_2,___eventCode, ___content, ___senderId,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
	else
	{
		typedef void (*FunctionPointerType) (Object_t * __this, uint8_t ___eventCode, Object_t * ___content, int32_t ___senderId, const MethodInfo* method);
		((FunctionPointerType)__this->___method_ptr_0)(__this->___m_target_2,___eventCode, ___content, ___senderId,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
}
extern "C" void pinvoke_delegate_wrapper_EventCallback_t8_113(Il2CppObject* delegate, uint8_t ___eventCode, Object_t * ___content, int32_t ___senderId)
{
	// Marshaling of parameter '___content' to native representation
	Object_t * ____content_marshaled = { 0 };
	il2cpp_codegen_raise_exception((Il2CppCodeGenException*)il2cpp_codegen_get_not_supported_exception("Cannot marshal type 'System.Object'."));
}
// System.IAsyncResult PhotonNetwork/EventCallback::BeginInvoke(System.Byte,System.Object,System.Int32,System.AsyncCallback,System.Object)
extern TypeInfo* Byte_t1_11_il2cpp_TypeInfo_var;
extern TypeInfo* Int32_t1_3_il2cpp_TypeInfo_var;
extern "C" Object_t * EventCallback_BeginInvoke_m8_597 (EventCallback_t8_113 * __this, uint8_t ___eventCode, Object_t * ___content, int32_t ___senderId, AsyncCallback_t1_28 * ___callback, Object_t * ___object, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Byte_t1_11_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(22);
		Int32_t1_3_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(8);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[4] = {0};
	__d_args[0] = Box(Byte_t1_11_il2cpp_TypeInfo_var, &___eventCode);
	__d_args[1] = ___content;
	__d_args[2] = Box(Int32_t1_3_il2cpp_TypeInfo_var, &___senderId);
	return (Object_t *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback, (Il2CppObject*)___object);
}
// System.Void PhotonNetwork/EventCallback::EndInvoke(System.IAsyncResult)
extern "C" void EventCallback_EndInvoke_m8_598 (EventCallback_t8_113 * __this, Object_t * ___result, const MethodInfo* method)
{
	il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result, 0);
}
// System.Void PhotonNetwork::.cctor()
extern const Il2CppType* ServerSettings_t8_115_0_0_0_var;
extern const Il2CppType* MonoBehaviour_t6_80_0_0_0_var;
extern TypeInfo* PhotonNetwork_t8_114_il2cpp_TypeInfo_var;
extern TypeInfo* Type_t_il2cpp_TypeInfo_var;
extern TypeInfo* ServerSettings_t8_115_il2cpp_TypeInfo_var;
extern TypeInfo* Dictionary_2_t1_942_il2cpp_TypeInfo_var;
extern TypeInfo* List_1_t1_945_il2cpp_TypeInfo_var;
extern TypeInfo* GameObject_t6_85_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* NetworkingPeer_t8_98_il2cpp_TypeInfo_var;
extern TypeInfo* CustomTypes_t8_61_il2cpp_TypeInfo_var;
extern const MethodInfo* Dictionary_2__ctor_m1_5641_MethodInfo_var;
extern const MethodInfo* List_1__ctor_m1_5642_MethodInfo_var;
extern const MethodInfo* GameObject_AddComponent_TisPhotonHandler_t8_111_m6_1687_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral3401;
extern Il2CppCodeGenString* _stringLiteral3402;
extern "C" void PhotonNetwork__cctor_m8_599 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ServerSettings_t8_115_0_0_0_var = il2cpp_codegen_type_from_index(1155);
		MonoBehaviour_t6_80_0_0_0_var = il2cpp_codegen_type_from_index(1022);
		PhotonNetwork_t8_114_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1058);
		Type_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3);
		ServerSettings_t8_115_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1155);
		Dictionary_2_t1_942_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1122);
		List_1_t1_945_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1124);
		GameObject_t6_85_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1082);
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(11);
		NetworkingPeer_t8_98_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1121);
		CustomTypes_t8_61_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1101);
		Dictionary_2__ctor_m1_5641_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483892);
		List_1__ctor_m1_5642_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483893);
		GameObject_AddComponent_TisPhotonHandler_t8_111_m6_1687_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483938);
		_stringLiteral3401 = il2cpp_codegen_string_literal_from_index(3401);
		_stringLiteral3402 = il2cpp_codegen_string_literal_from_index(3402);
		s_Il2CppMethodIntialized = true;
	}
	GameObject_t6_85 * V_0 = {0};
	uint8_t V_1 = {0};
	{
		((PhotonNetwork_t8_114_StaticFields*)PhotonNetwork_t8_114_il2cpp_TypeInfo_var->static_fields)->___MAX_VIEW_IDS_5 = ((int32_t)1000);
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m1_893(NULL /*static, unused*/, LoadTypeToken(ServerSettings_t8_115_0_0_0_var), /*hidden argument*/NULL);
		Object_t6_5 * L_1 = Resources_Load_m6_409(NULL /*static, unused*/, _stringLiteral3401, L_0, /*hidden argument*/NULL);
		((PhotonNetwork_t8_114_StaticFields*)PhotonNetwork_t8_114_il2cpp_TypeInfo_var->static_fields)->___PhotonServerSettings_6 = ((ServerSettings_t8_115 *)CastclassClass(L_1, ServerSettings_t8_115_il2cpp_TypeInfo_var));
		((PhotonNetwork_t8_114_StaticFields*)PhotonNetwork_t8_114_il2cpp_TypeInfo_var->static_fields)->___InstantiateInRoomOnly_7 = 1;
		((PhotonNetwork_t8_114_StaticFields*)PhotonNetwork_t8_114_il2cpp_TypeInfo_var->static_fields)->___logLevel_8 = 0;
		((PhotonNetwork_t8_114_StaticFields*)PhotonNetwork_t8_114_il2cpp_TypeInfo_var->static_fields)->___precisionForVectorSynchronization_9 = (9.9E-05f);
		((PhotonNetwork_t8_114_StaticFields*)PhotonNetwork_t8_114_il2cpp_TypeInfo_var->static_fields)->___precisionForQuaternionSynchronization_10 = (1.0f);
		((PhotonNetwork_t8_114_StaticFields*)PhotonNetwork_t8_114_il2cpp_TypeInfo_var->static_fields)->___precisionForFloatSynchronization_11 = (0.01f);
		((PhotonNetwork_t8_114_StaticFields*)PhotonNetwork_t8_114_il2cpp_TypeInfo_var->static_fields)->___UsePrefabCache_13 = 1;
		Dictionary_2_t1_942 * L_2 = (Dictionary_2_t1_942 *)il2cpp_codegen_object_new (Dictionary_2_t1_942_il2cpp_TypeInfo_var);
		Dictionary_2__ctor_m1_5641(L_2, /*hidden argument*/Dictionary_2__ctor_m1_5641_MethodInfo_var);
		((PhotonNetwork_t8_114_StaticFields*)PhotonNetwork_t8_114_il2cpp_TypeInfo_var->static_fields)->___PrefabCache_14 = L_2;
		Type_t * L_3 = Type_GetTypeFromHandle_m1_893(NULL /*static, unused*/, LoadTypeToken(MonoBehaviour_t6_80_0_0_0_var), /*hidden argument*/NULL);
		((PhotonNetwork_t8_114_StaticFields*)PhotonNetwork_t8_114_il2cpp_TypeInfo_var->static_fields)->___SendMonoMessageTargetType_16 = L_3;
		((PhotonNetwork_t8_114_StaticFields*)PhotonNetwork_t8_114_il2cpp_TypeInfo_var->static_fields)->___isOfflineMode_17 = 0;
		((PhotonNetwork_t8_114_StaticFields*)PhotonNetwork_t8_114_il2cpp_TypeInfo_var->static_fields)->___offlineModeRoom_18 = (Room_t8_100 *)NULL;
		((PhotonNetwork_t8_114_StaticFields*)PhotonNetwork_t8_114_il2cpp_TypeInfo_var->static_fields)->____mAutomaticallySyncScene_20 = 0;
		((PhotonNetwork_t8_114_StaticFields*)PhotonNetwork_t8_114_il2cpp_TypeInfo_var->static_fields)->___m_autoCleanUpPlayerObjects_21 = 1;
		((PhotonNetwork_t8_114_StaticFields*)PhotonNetwork_t8_114_il2cpp_TypeInfo_var->static_fields)->___sendInterval_22 = ((int32_t)50);
		((PhotonNetwork_t8_114_StaticFields*)PhotonNetwork_t8_114_il2cpp_TypeInfo_var->static_fields)->___sendIntervalOnSerialize_23 = ((int32_t)100);
		((PhotonNetwork_t8_114_StaticFields*)PhotonNetwork_t8_114_il2cpp_TypeInfo_var->static_fields)->___m_isMessageQueueRunning_24 = 1;
		((PhotonNetwork_t8_114_StaticFields*)PhotonNetwork_t8_114_il2cpp_TypeInfo_var->static_fields)->___BackgroundTimeout_25 = (0.0f);
		((PhotonNetwork_t8_114_StaticFields*)PhotonNetwork_t8_114_il2cpp_TypeInfo_var->static_fields)->___lastUsedViewSubId_27 = 0;
		((PhotonNetwork_t8_114_StaticFields*)PhotonNetwork_t8_114_il2cpp_TypeInfo_var->static_fields)->___lastUsedViewSubIdStatic_28 = 0;
		List_1_t1_945 * L_4 = (List_1_t1_945 *)il2cpp_codegen_object_new (List_1_t1_945_il2cpp_TypeInfo_var);
		List_1__ctor_m1_5642(L_4, /*hidden argument*/List_1__ctor_m1_5642_MethodInfo_var);
		((PhotonNetwork_t8_114_StaticFields*)PhotonNetwork_t8_114_il2cpp_TypeInfo_var->static_fields)->___manuallyAllocatedViewIds_29 = L_4;
		Application_set_runInBackground_m6_453(NULL /*static, unused*/, 1, /*hidden argument*/NULL);
		GameObject_t6_85 * L_5 = (GameObject_t6_85 *)il2cpp_codegen_object_new (GameObject_t6_85_il2cpp_TypeInfo_var);
		GameObject__ctor_m6_590(L_5, /*hidden argument*/NULL);
		V_0 = L_5;
		GameObject_t6_85 * L_6 = V_0;
		NullCheck(L_6);
		PhotonHandler_t8_111 * L_7 = GameObject_AddComponent_TisPhotonHandler_t8_111_m6_1687(L_6, /*hidden argument*/GameObject_AddComponent_TisPhotonHandler_t8_111_m6_1687_MethodInfo_var);
		((PhotonNetwork_t8_114_StaticFields*)PhotonNetwork_t8_114_il2cpp_TypeInfo_var->static_fields)->___photonMono_3 = L_7;
		GameObject_t6_85 * L_8 = V_0;
		NullCheck(L_8);
		Object_set_name_m6_563(L_8, _stringLiteral3402, /*hidden argument*/NULL);
		GameObject_t6_85 * L_9 = V_0;
		NullCheck(L_9);
		Object_set_hideFlags_m6_565(L_9, 1, /*hidden argument*/NULL);
		ServerSettings_t8_115 * L_10 = ((PhotonNetwork_t8_114_StaticFields*)PhotonNetwork_t8_114_il2cpp_TypeInfo_var->static_fields)->___PhotonServerSettings_6;
		NullCheck(L_10);
		uint8_t L_11 = (L_10->___Protocol_3);
		V_1 = L_11;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_12 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->___Empty_2;
		uint8_t L_13 = V_1;
		NetworkingPeer_t8_98 * L_14 = (NetworkingPeer_t8_98 *)il2cpp_codegen_object_new (NetworkingPeer_t8_98_il2cpp_TypeInfo_var);
		NetworkingPeer__ctor_m8_367(L_14, L_12, L_13, /*hidden argument*/NULL);
		((PhotonNetwork_t8_114_StaticFields*)PhotonNetwork_t8_114_il2cpp_TypeInfo_var->static_fields)->___networkingPeer_4 = L_14;
		NetworkingPeer_t8_98 * L_15 = ((PhotonNetwork_t8_114_StaticFields*)PhotonNetwork_t8_114_il2cpp_TypeInfo_var->static_fields)->___networkingPeer_4;
		NullCheck(L_15);
		PhotonPeer_set_QuickResendAttempts_m5_210(L_15, 2, /*hidden argument*/NULL);
		NetworkingPeer_t8_98 * L_16 = ((PhotonNetwork_t8_114_StaticFields*)PhotonNetwork_t8_114_il2cpp_TypeInfo_var->static_fields)->___networkingPeer_4;
		NullCheck(L_16);
		PhotonPeer_set_SentCountAllowance_m5_219(L_16, 7, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(CustomTypes_t8_61_il2cpp_TypeInfo_var);
		CustomTypes_Register_m8_282(NULL /*static, unused*/, /*hidden argument*/NULL);
		return;
	}
}
// System.String PhotonNetwork::get_gameVersion()
extern TypeInfo* PhotonNetwork_t8_114_il2cpp_TypeInfo_var;
extern "C" String_t* PhotonNetwork_get_gameVersion_m8_600 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		PhotonNetwork_t8_114_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1058);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(PhotonNetwork_t8_114_il2cpp_TypeInfo_var);
		String_t* L_0 = ((PhotonNetwork_t8_114_StaticFields*)PhotonNetwork_t8_114_il2cpp_TypeInfo_var->static_fields)->___U3CgameVersionU3Ek__BackingField_30;
		return L_0;
	}
}
// System.Void PhotonNetwork::set_gameVersion(System.String)
extern TypeInfo* PhotonNetwork_t8_114_il2cpp_TypeInfo_var;
extern "C" void PhotonNetwork_set_gameVersion_m8_601 (Object_t * __this /* static, unused */, String_t* ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		PhotonNetwork_t8_114_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1058);
		s_Il2CppMethodIntialized = true;
	}
	{
		String_t* L_0 = ___value;
		IL2CPP_RUNTIME_CLASS_INIT(PhotonNetwork_t8_114_il2cpp_TypeInfo_var);
		((PhotonNetwork_t8_114_StaticFields*)PhotonNetwork_t8_114_il2cpp_TypeInfo_var->static_fields)->___U3CgameVersionU3Ek__BackingField_30 = L_0;
		return;
	}
}
// System.String PhotonNetwork::get_ServerAddress()
extern TypeInfo* PhotonNetwork_t8_114_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3403;
extern "C" String_t* PhotonNetwork_get_ServerAddress_m8_602 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		PhotonNetwork_t8_114_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1058);
		_stringLiteral3403 = il2cpp_codegen_string_literal_from_index(3403);
		s_Il2CppMethodIntialized = true;
	}
	String_t* G_B3_0 = {0};
	{
		IL2CPP_RUNTIME_CLASS_INIT(PhotonNetwork_t8_114_il2cpp_TypeInfo_var);
		NetworkingPeer_t8_98 * L_0 = ((PhotonNetwork_t8_114_StaticFields*)PhotonNetwork_t8_114_il2cpp_TypeInfo_var->static_fields)->___networkingPeer_4;
		if (!L_0)
		{
			goto IL_0019;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(PhotonNetwork_t8_114_il2cpp_TypeInfo_var);
		NetworkingPeer_t8_98 * L_1 = ((PhotonNetwork_t8_114_StaticFields*)PhotonNetwork_t8_114_il2cpp_TypeInfo_var->static_fields)->___networkingPeer_4;
		NullCheck(L_1);
		String_t* L_2 = PhotonPeer_get_ServerAddress_m5_227(L_1, /*hidden argument*/NULL);
		G_B3_0 = L_2;
		goto IL_001e;
	}

IL_0019:
	{
		G_B3_0 = _stringLiteral3403;
	}

IL_001e:
	{
		return G_B3_0;
	}
}
// System.Boolean PhotonNetwork::get_connected()
extern TypeInfo* PhotonNetwork_t8_114_il2cpp_TypeInfo_var;
extern "C" bool PhotonNetwork_get_connected_m8_603 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		PhotonNetwork_t8_114_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1058);
		s_Il2CppMethodIntialized = true;
	}
	int32_t G_B10_0 = 0;
	{
		IL2CPP_RUNTIME_CLASS_INIT(PhotonNetwork_t8_114_il2cpp_TypeInfo_var);
		bool L_0 = PhotonNetwork_get_offlineMode_m8_623(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_000c;
		}
	}
	{
		return 1;
	}

IL_000c:
	{
		IL2CPP_RUNTIME_CLASS_INIT(PhotonNetwork_t8_114_il2cpp_TypeInfo_var);
		NetworkingPeer_t8_98 * L_1 = ((PhotonNetwork_t8_114_StaticFields*)PhotonNetwork_t8_114_il2cpp_TypeInfo_var->static_fields)->___networkingPeer_4;
		if (L_1)
		{
			goto IL_0018;
		}
	}
	{
		return 0;
	}

IL_0018:
	{
		IL2CPP_RUNTIME_CLASS_INIT(PhotonNetwork_t8_114_il2cpp_TypeInfo_var);
		NetworkingPeer_t8_98 * L_2 = ((PhotonNetwork_t8_114_StaticFields*)PhotonNetwork_t8_114_il2cpp_TypeInfo_var->static_fields)->___networkingPeer_4;
		NullCheck(L_2);
		bool L_3 = (L_2->___IsInitialConnect_11);
		if (L_3)
		{
			goto IL_006c;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(PhotonNetwork_t8_114_il2cpp_TypeInfo_var);
		NetworkingPeer_t8_98 * L_4 = ((PhotonNetwork_t8_114_StaticFields*)PhotonNetwork_t8_114_il2cpp_TypeInfo_var->static_fields)->___networkingPeer_4;
		NullCheck(L_4);
		int32_t L_5 = NetworkingPeer_get_State_m8_379(L_4, /*hidden argument*/NULL);
		if ((((int32_t)L_5) == ((int32_t)1)))
		{
			goto IL_006c;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(PhotonNetwork_t8_114_il2cpp_TypeInfo_var);
		NetworkingPeer_t8_98 * L_6 = ((PhotonNetwork_t8_114_StaticFields*)PhotonNetwork_t8_114_il2cpp_TypeInfo_var->static_fields)->___networkingPeer_4;
		NullCheck(L_6);
		int32_t L_7 = NetworkingPeer_get_State_m8_379(L_6, /*hidden argument*/NULL);
		if ((((int32_t)L_7) == ((int32_t)((int32_t)15))))
		{
			goto IL_006c;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(PhotonNetwork_t8_114_il2cpp_TypeInfo_var);
		NetworkingPeer_t8_98 * L_8 = ((PhotonNetwork_t8_114_StaticFields*)PhotonNetwork_t8_114_il2cpp_TypeInfo_var->static_fields)->___networkingPeer_4;
		NullCheck(L_8);
		int32_t L_9 = NetworkingPeer_get_State_m8_379(L_8, /*hidden argument*/NULL);
		if ((((int32_t)L_9) == ((int32_t)((int32_t)14))))
		{
			goto IL_006c;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(PhotonNetwork_t8_114_il2cpp_TypeInfo_var);
		NetworkingPeer_t8_98 * L_10 = ((PhotonNetwork_t8_114_StaticFields*)PhotonNetwork_t8_114_il2cpp_TypeInfo_var->static_fields)->___networkingPeer_4;
		NullCheck(L_10);
		int32_t L_11 = NetworkingPeer_get_State_m8_379(L_10, /*hidden argument*/NULL);
		G_B10_0 = ((((int32_t)((((int32_t)L_11) == ((int32_t)((int32_t)17)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_006d;
	}

IL_006c:
	{
		G_B10_0 = 0;
	}

IL_006d:
	{
		return G_B10_0;
	}
}
// System.Boolean PhotonNetwork::get_connecting()
extern TypeInfo* PhotonNetwork_t8_114_il2cpp_TypeInfo_var;
extern "C" bool PhotonNetwork_get_connecting_m8_604 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		PhotonNetwork_t8_114_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1058);
		s_Il2CppMethodIntialized = true;
	}
	int32_t G_B3_0 = 0;
	{
		IL2CPP_RUNTIME_CLASS_INIT(PhotonNetwork_t8_114_il2cpp_TypeInfo_var);
		NetworkingPeer_t8_98 * L_0 = ((PhotonNetwork_t8_114_StaticFields*)PhotonNetwork_t8_114_il2cpp_TypeInfo_var->static_fields)->___networkingPeer_4;
		NullCheck(L_0);
		bool L_1 = (L_0->___IsInitialConnect_11);
		if (!L_1)
		{
			goto IL_0019;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(PhotonNetwork_t8_114_il2cpp_TypeInfo_var);
		bool L_2 = PhotonNetwork_get_offlineMode_m8_623(NULL /*static, unused*/, /*hidden argument*/NULL);
		G_B3_0 = ((((int32_t)L_2) == ((int32_t)0))? 1 : 0);
		goto IL_001a;
	}

IL_0019:
	{
		G_B3_0 = 0;
	}

IL_001a:
	{
		return G_B3_0;
	}
}
// System.Boolean PhotonNetwork::get_connectedAndReady()
extern TypeInfo* PhotonNetwork_t8_114_il2cpp_TypeInfo_var;
extern "C" bool PhotonNetwork_get_connectedAndReady_m8_605 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		PhotonNetwork_t8_114_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1058);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = {0};
	{
		IL2CPP_RUNTIME_CLASS_INIT(PhotonNetwork_t8_114_il2cpp_TypeInfo_var);
		bool L_0 = PhotonNetwork_get_connected_m8_603(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (L_0)
		{
			goto IL_000c;
		}
	}
	{
		return 0;
	}

IL_000c:
	{
		IL2CPP_RUNTIME_CLASS_INIT(PhotonNetwork_t8_114_il2cpp_TypeInfo_var);
		bool L_1 = PhotonNetwork_get_offlineMode_m8_623(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0018;
		}
	}
	{
		return 1;
	}

IL_0018:
	{
		IL2CPP_RUNTIME_CLASS_INIT(PhotonNetwork_t8_114_il2cpp_TypeInfo_var);
		int32_t L_2 = PhotonNetwork_get_connectionStateDetailed_m8_607(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_0 = L_2;
		int32_t L_3 = V_0;
		if (((int32_t)((int32_t)L_3-(int32_t)((int32_t)12))) == 0)
		{
			goto IL_006b;
		}
		if (((int32_t)((int32_t)L_3-(int32_t)((int32_t)12))) == 1)
		{
			goto IL_004b;
		}
		if (((int32_t)((int32_t)L_3-(int32_t)((int32_t)12))) == 2)
		{
			goto IL_006b;
		}
		if (((int32_t)((int32_t)L_3-(int32_t)((int32_t)12))) == 3)
		{
			goto IL_006b;
		}
		if (((int32_t)((int32_t)L_3-(int32_t)((int32_t)12))) == 4)
		{
			goto IL_004b;
		}
		if (((int32_t)((int32_t)L_3-(int32_t)((int32_t)12))) == 5)
		{
			goto IL_006b;
		}
		if (((int32_t)((int32_t)L_3-(int32_t)((int32_t)12))) == 6)
		{
			goto IL_004b;
		}
		if (((int32_t)((int32_t)L_3-(int32_t)((int32_t)12))) == 7)
		{
			goto IL_004b;
		}
		if (((int32_t)((int32_t)L_3-(int32_t)((int32_t)12))) == 8)
		{
			goto IL_006b;
		}
	}

IL_004b:
	{
		int32_t L_4 = V_0;
		if (((int32_t)((int32_t)L_4-(int32_t)6)) == 0)
		{
			goto IL_006b;
		}
		if (((int32_t)((int32_t)L_4-(int32_t)6)) == 1)
		{
			goto IL_005f;
		}
		if (((int32_t)((int32_t)L_4-(int32_t)6)) == 2)
		{
			goto IL_006b;
		}
	}

IL_005f:
	{
		int32_t L_5 = V_0;
		if ((((int32_t)L_5) == ((int32_t)1)))
		{
			goto IL_006b;
		}
	}
	{
		goto IL_006d;
	}

IL_006b:
	{
		return 0;
	}

IL_006d:
	{
		return 1;
	}
}
// ConnectionState PhotonNetwork::get_connectionState()
extern TypeInfo* PhotonNetwork_t8_114_il2cpp_TypeInfo_var;
extern "C" int32_t PhotonNetwork_get_connectionState_m8_606 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		PhotonNetwork_t8_114_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1058);
		s_Il2CppMethodIntialized = true;
	}
	uint8_t V_0 = {0};
	{
		IL2CPP_RUNTIME_CLASS_INIT(PhotonNetwork_t8_114_il2cpp_TypeInfo_var);
		bool L_0 = PhotonNetwork_get_offlineMode_m8_623(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_000c;
		}
	}
	{
		return (int32_t)(2);
	}

IL_000c:
	{
		IL2CPP_RUNTIME_CLASS_INIT(PhotonNetwork_t8_114_il2cpp_TypeInfo_var);
		NetworkingPeer_t8_98 * L_1 = ((PhotonNetwork_t8_114_StaticFields*)PhotonNetwork_t8_114_il2cpp_TypeInfo_var->static_fields)->___networkingPeer_4;
		if (L_1)
		{
			goto IL_0018;
		}
	}
	{
		return (int32_t)(0);
	}

IL_0018:
	{
		IL2CPP_RUNTIME_CLASS_INIT(PhotonNetwork_t8_114_il2cpp_TypeInfo_var);
		NetworkingPeer_t8_98 * L_2 = ((PhotonNetwork_t8_114_StaticFields*)PhotonNetwork_t8_114_il2cpp_TypeInfo_var->static_fields)->___networkingPeer_4;
		NullCheck(L_2);
		uint8_t L_3 = PhotonPeer_get_PeerState_m5_211(L_2, /*hidden argument*/NULL);
		V_0 = L_3;
		uint8_t L_4 = V_0;
		if (L_4 == 0)
		{
			goto IL_004a;
		}
		if (L_4 == 1)
		{
			goto IL_004c;
		}
		if (L_4 == 2)
		{
			goto IL_003d;
		}
		if (L_4 == 3)
		{
			goto IL_004e;
		}
		if (L_4 == 4)
		{
			goto IL_0050;
		}
	}

IL_003d:
	{
		uint8_t L_5 = V_0;
		if ((((int32_t)L_5) == ((int32_t)((int32_t)10))))
		{
			goto IL_0052;
		}
	}
	{
		goto IL_0054;
	}

IL_004a:
	{
		return (int32_t)(0);
	}

IL_004c:
	{
		return (int32_t)(1);
	}

IL_004e:
	{
		return (int32_t)(2);
	}

IL_0050:
	{
		return (int32_t)(3);
	}

IL_0052:
	{
		return (int32_t)(4);
	}

IL_0054:
	{
		return (int32_t)(0);
	}
}
// PeerState PhotonNetwork::get_connectionStateDetailed()
extern TypeInfo* PhotonNetwork_t8_114_il2cpp_TypeInfo_var;
extern "C" int32_t PhotonNetwork_get_connectionStateDetailed_m8_607 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		PhotonNetwork_t8_114_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1058);
		s_Il2CppMethodIntialized = true;
	}
	int32_t G_B4_0 = 0;
	{
		IL2CPP_RUNTIME_CLASS_INIT(PhotonNetwork_t8_114_il2cpp_TypeInfo_var);
		bool L_0 = PhotonNetwork_get_offlineMode_m8_623(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_001e;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(PhotonNetwork_t8_114_il2cpp_TypeInfo_var);
		Room_t8_100 * L_1 = ((PhotonNetwork_t8_114_StaticFields*)PhotonNetwork_t8_114_il2cpp_TypeInfo_var->static_fields)->___offlineModeRoom_18;
		if (!L_1)
		{
			goto IL_001b;
		}
	}
	{
		G_B4_0 = ((int32_t)9);
		goto IL_001d;
	}

IL_001b:
	{
		G_B4_0 = ((int32_t)16);
	}

IL_001d:
	{
		return (int32_t)(G_B4_0);
	}

IL_001e:
	{
		IL2CPP_RUNTIME_CLASS_INIT(PhotonNetwork_t8_114_il2cpp_TypeInfo_var);
		NetworkingPeer_t8_98 * L_2 = ((PhotonNetwork_t8_114_StaticFields*)PhotonNetwork_t8_114_il2cpp_TypeInfo_var->static_fields)->___networkingPeer_4;
		if (L_2)
		{
			goto IL_002b;
		}
	}
	{
		return (int32_t)(((int32_t)15));
	}

IL_002b:
	{
		IL2CPP_RUNTIME_CLASS_INIT(PhotonNetwork_t8_114_il2cpp_TypeInfo_var);
		NetworkingPeer_t8_98 * L_3 = ((PhotonNetwork_t8_114_StaticFields*)PhotonNetwork_t8_114_il2cpp_TypeInfo_var->static_fields)->___networkingPeer_4;
		NullCheck(L_3);
		int32_t L_4 = NetworkingPeer_get_State_m8_379(L_3, /*hidden argument*/NULL);
		return L_4;
	}
}
// ServerConnection PhotonNetwork::get_Server()
extern TypeInfo* PhotonNetwork_t8_114_il2cpp_TypeInfo_var;
extern "C" int32_t PhotonNetwork_get_Server_m8_608 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		PhotonNetwork_t8_114_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1058);
		s_Il2CppMethodIntialized = true;
	}
	int32_t G_B3_0 = 0;
	{
		IL2CPP_RUNTIME_CLASS_INIT(PhotonNetwork_t8_114_il2cpp_TypeInfo_var);
		NetworkingPeer_t8_98 * L_0 = ((PhotonNetwork_t8_114_StaticFields*)PhotonNetwork_t8_114_il2cpp_TypeInfo_var->static_fields)->___networkingPeer_4;
		if (!L_0)
		{
			goto IL_0019;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(PhotonNetwork_t8_114_il2cpp_TypeInfo_var);
		NetworkingPeer_t8_98 * L_1 = ((PhotonNetwork_t8_114_StaticFields*)PhotonNetwork_t8_114_il2cpp_TypeInfo_var->static_fields)->___networkingPeer_4;
		NullCheck(L_1);
		int32_t L_2 = NetworkingPeer_get_server_m8_377(L_1, /*hidden argument*/NULL);
		G_B3_0 = ((int32_t)(L_2));
		goto IL_001a;
	}

IL_0019:
	{
		G_B3_0 = 2;
	}

IL_001a:
	{
		return (int32_t)(G_B3_0);
	}
}
// AuthenticationValues PhotonNetwork::get_AuthValues()
extern TypeInfo* PhotonNetwork_t8_114_il2cpp_TypeInfo_var;
extern "C" AuthenticationValues_t8_97 * PhotonNetwork_get_AuthValues_m8_609 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		PhotonNetwork_t8_114_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1058);
		s_Il2CppMethodIntialized = true;
	}
	AuthenticationValues_t8_97 * G_B3_0 = {0};
	{
		IL2CPP_RUNTIME_CLASS_INIT(PhotonNetwork_t8_114_il2cpp_TypeInfo_var);
		NetworkingPeer_t8_98 * L_0 = ((PhotonNetwork_t8_114_StaticFields*)PhotonNetwork_t8_114_il2cpp_TypeInfo_var->static_fields)->___networkingPeer_4;
		if (!L_0)
		{
			goto IL_0019;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(PhotonNetwork_t8_114_il2cpp_TypeInfo_var);
		NetworkingPeer_t8_98 * L_1 = ((PhotonNetwork_t8_114_StaticFields*)PhotonNetwork_t8_114_il2cpp_TypeInfo_var->static_fields)->___networkingPeer_4;
		NullCheck(L_1);
		AuthenticationValues_t8_97 * L_2 = NetworkingPeer_get_CustomAuthenticationValues_m8_370(L_1, /*hidden argument*/NULL);
		G_B3_0 = L_2;
		goto IL_001a;
	}

IL_0019:
	{
		G_B3_0 = ((AuthenticationValues_t8_97 *)(NULL));
	}

IL_001a:
	{
		return G_B3_0;
	}
}
// System.Void PhotonNetwork::set_AuthValues(AuthenticationValues)
extern TypeInfo* PhotonNetwork_t8_114_il2cpp_TypeInfo_var;
extern "C" void PhotonNetwork_set_AuthValues_m8_610 (Object_t * __this /* static, unused */, AuthenticationValues_t8_97 * ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		PhotonNetwork_t8_114_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1058);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(PhotonNetwork_t8_114_il2cpp_TypeInfo_var);
		NetworkingPeer_t8_98 * L_0 = ((PhotonNetwork_t8_114_StaticFields*)PhotonNetwork_t8_114_il2cpp_TypeInfo_var->static_fields)->___networkingPeer_4;
		if (!L_0)
		{
			goto IL_0015;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(PhotonNetwork_t8_114_il2cpp_TypeInfo_var);
		NetworkingPeer_t8_98 * L_1 = ((PhotonNetwork_t8_114_StaticFields*)PhotonNetwork_t8_114_il2cpp_TypeInfo_var->static_fields)->___networkingPeer_4;
		AuthenticationValues_t8_97 * L_2 = ___value;
		NullCheck(L_1);
		NetworkingPeer_set_CustomAuthenticationValues_m8_371(L_1, L_2, /*hidden argument*/NULL);
	}

IL_0015:
	{
		return;
	}
}
// Room PhotonNetwork::get_room()
extern TypeInfo* PhotonNetwork_t8_114_il2cpp_TypeInfo_var;
extern "C" Room_t8_100 * PhotonNetwork_get_room_m8_611 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		PhotonNetwork_t8_114_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1058);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(PhotonNetwork_t8_114_il2cpp_TypeInfo_var);
		bool L_0 = ((PhotonNetwork_t8_114_StaticFields*)PhotonNetwork_t8_114_il2cpp_TypeInfo_var->static_fields)->___isOfflineMode_17;
		if (!L_0)
		{
			goto IL_0010;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(PhotonNetwork_t8_114_il2cpp_TypeInfo_var);
		Room_t8_100 * L_1 = ((PhotonNetwork_t8_114_StaticFields*)PhotonNetwork_t8_114_il2cpp_TypeInfo_var->static_fields)->___offlineModeRoom_18;
		return L_1;
	}

IL_0010:
	{
		IL2CPP_RUNTIME_CLASS_INIT(PhotonNetwork_t8_114_il2cpp_TypeInfo_var);
		NetworkingPeer_t8_98 * L_2 = ((PhotonNetwork_t8_114_StaticFields*)PhotonNetwork_t8_114_il2cpp_TypeInfo_var->static_fields)->___networkingPeer_4;
		NullCheck(L_2);
		Room_t8_100 * L_3 = NetworkingPeer_get_CurrentGame_m8_400(L_2, /*hidden argument*/NULL);
		return L_3;
	}
}
// PhotonPlayer PhotonNetwork::get_player()
extern TypeInfo* PhotonNetwork_t8_114_il2cpp_TypeInfo_var;
extern "C" PhotonPlayer_t8_102 * PhotonNetwork_get_player_m8_612 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		PhotonNetwork_t8_114_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1058);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(PhotonNetwork_t8_114_il2cpp_TypeInfo_var);
		NetworkingPeer_t8_98 * L_0 = ((PhotonNetwork_t8_114_StaticFields*)PhotonNetwork_t8_114_il2cpp_TypeInfo_var->static_fields)->___networkingPeer_4;
		if (L_0)
		{
			goto IL_000c;
		}
	}
	{
		return (PhotonPlayer_t8_102 *)NULL;
	}

IL_000c:
	{
		IL2CPP_RUNTIME_CLASS_INIT(PhotonNetwork_t8_114_il2cpp_TypeInfo_var);
		NetworkingPeer_t8_98 * L_1 = ((PhotonNetwork_t8_114_StaticFields*)PhotonNetwork_t8_114_il2cpp_TypeInfo_var->static_fields)->___networkingPeer_4;
		NullCheck(L_1);
		PhotonPlayer_t8_102 * L_2 = NetworkingPeer_get_mLocalActor_m8_402(L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// PhotonPlayer PhotonNetwork::get_masterClient()
extern TypeInfo* PhotonNetwork_t8_114_il2cpp_TypeInfo_var;
extern "C" PhotonPlayer_t8_102 * PhotonNetwork_get_masterClient_m8_613 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		PhotonNetwork_t8_114_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1058);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(PhotonNetwork_t8_114_il2cpp_TypeInfo_var);
		bool L_0 = PhotonNetwork_get_offlineMode_m8_623(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_0010;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(PhotonNetwork_t8_114_il2cpp_TypeInfo_var);
		PhotonPlayer_t8_102 * L_1 = PhotonNetwork_get_player_m8_612(NULL /*static, unused*/, /*hidden argument*/NULL);
		return L_1;
	}

IL_0010:
	{
		IL2CPP_RUNTIME_CLASS_INIT(PhotonNetwork_t8_114_il2cpp_TypeInfo_var);
		NetworkingPeer_t8_98 * L_2 = ((PhotonNetwork_t8_114_StaticFields*)PhotonNetwork_t8_114_il2cpp_TypeInfo_var->static_fields)->___networkingPeer_4;
		if (L_2)
		{
			goto IL_001c;
		}
	}
	{
		return (PhotonPlayer_t8_102 *)NULL;
	}

IL_001c:
	{
		IL2CPP_RUNTIME_CLASS_INIT(PhotonNetwork_t8_114_il2cpp_TypeInfo_var);
		NetworkingPeer_t8_98 * L_3 = ((PhotonNetwork_t8_114_StaticFields*)PhotonNetwork_t8_114_il2cpp_TypeInfo_var->static_fields)->___networkingPeer_4;
		NetworkingPeer_t8_98 * L_4 = ((PhotonNetwork_t8_114_StaticFields*)PhotonNetwork_t8_114_il2cpp_TypeInfo_var->static_fields)->___networkingPeer_4;
		NullCheck(L_4);
		int32_t L_5 = NetworkingPeer_get_mMasterClientId_m8_404(L_4, /*hidden argument*/NULL);
		NullCheck(L_3);
		PhotonPlayer_t8_102 * L_6 = NetworkingPeer_GetPlayerWithId_m8_429(L_3, L_5, /*hidden argument*/NULL);
		return L_6;
	}
}
// System.String PhotonNetwork::get_playerName()
extern TypeInfo* PhotonNetwork_t8_114_il2cpp_TypeInfo_var;
extern "C" String_t* PhotonNetwork_get_playerName_m8_614 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		PhotonNetwork_t8_114_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1058);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(PhotonNetwork_t8_114_il2cpp_TypeInfo_var);
		NetworkingPeer_t8_98 * L_0 = ((PhotonNetwork_t8_114_StaticFields*)PhotonNetwork_t8_114_il2cpp_TypeInfo_var->static_fields)->___networkingPeer_4;
		NullCheck(L_0);
		String_t* L_1 = NetworkingPeer_get_PlayerName_m8_398(L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Void PhotonNetwork::set_playerName(System.String)
extern TypeInfo* PhotonNetwork_t8_114_il2cpp_TypeInfo_var;
extern "C" void PhotonNetwork_set_playerName_m8_615 (Object_t * __this /* static, unused */, String_t* ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		PhotonNetwork_t8_114_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1058);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(PhotonNetwork_t8_114_il2cpp_TypeInfo_var);
		NetworkingPeer_t8_98 * L_0 = ((PhotonNetwork_t8_114_StaticFields*)PhotonNetwork_t8_114_il2cpp_TypeInfo_var->static_fields)->___networkingPeer_4;
		String_t* L_1 = ___value;
		NullCheck(L_0);
		NetworkingPeer_set_PlayerName_m8_399(L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// PhotonPlayer[] PhotonNetwork::get_playerList()
extern TypeInfo* PhotonNetwork_t8_114_il2cpp_TypeInfo_var;
extern TypeInfo* PhotonPlayerU5BU5D_t8_101_il2cpp_TypeInfo_var;
extern "C" PhotonPlayerU5BU5D_t8_101* PhotonNetwork_get_playerList_m8_616 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		PhotonNetwork_t8_114_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1058);
		PhotonPlayerU5BU5D_t8_101_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1114);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(PhotonNetwork_t8_114_il2cpp_TypeInfo_var);
		NetworkingPeer_t8_98 * L_0 = ((PhotonNetwork_t8_114_StaticFields*)PhotonNetwork_t8_114_il2cpp_TypeInfo_var->static_fields)->___networkingPeer_4;
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		return ((PhotonPlayerU5BU5D_t8_101*)SZArrayNew(PhotonPlayerU5BU5D_t8_101_il2cpp_TypeInfo_var, 0));
	}

IL_0011:
	{
		IL2CPP_RUNTIME_CLASS_INIT(PhotonNetwork_t8_114_il2cpp_TypeInfo_var);
		NetworkingPeer_t8_98 * L_1 = ((PhotonNetwork_t8_114_StaticFields*)PhotonNetwork_t8_114_il2cpp_TypeInfo_var->static_fields)->___networkingPeer_4;
		NullCheck(L_1);
		PhotonPlayerU5BU5D_t8_101* L_2 = (L_1->___mPlayerListCopy_27);
		return L_2;
	}
}
// PhotonPlayer[] PhotonNetwork::get_otherPlayers()
extern TypeInfo* PhotonNetwork_t8_114_il2cpp_TypeInfo_var;
extern TypeInfo* PhotonPlayerU5BU5D_t8_101_il2cpp_TypeInfo_var;
extern "C" PhotonPlayerU5BU5D_t8_101* PhotonNetwork_get_otherPlayers_m8_617 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		PhotonNetwork_t8_114_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1058);
		PhotonPlayerU5BU5D_t8_101_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1114);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(PhotonNetwork_t8_114_il2cpp_TypeInfo_var);
		NetworkingPeer_t8_98 * L_0 = ((PhotonNetwork_t8_114_StaticFields*)PhotonNetwork_t8_114_il2cpp_TypeInfo_var->static_fields)->___networkingPeer_4;
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		return ((PhotonPlayerU5BU5D_t8_101*)SZArrayNew(PhotonPlayerU5BU5D_t8_101_il2cpp_TypeInfo_var, 0));
	}

IL_0011:
	{
		IL2CPP_RUNTIME_CLASS_INIT(PhotonNetwork_t8_114_il2cpp_TypeInfo_var);
		NetworkingPeer_t8_98 * L_1 = ((PhotonNetwork_t8_114_StaticFields*)PhotonNetwork_t8_114_il2cpp_TypeInfo_var->static_fields)->___networkingPeer_4;
		NullCheck(L_1);
		PhotonPlayerU5BU5D_t8_101* L_2 = (L_1->___mOtherPlayerListCopy_26);
		return L_2;
	}
}
// System.Collections.Generic.List`1<FriendInfo> PhotonNetwork::get_Friends()
extern TypeInfo* PhotonNetwork_t8_114_il2cpp_TypeInfo_var;
extern "C" List_1_t1_946 * PhotonNetwork_get_Friends_m8_618 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		PhotonNetwork_t8_114_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1058);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(PhotonNetwork_t8_114_il2cpp_TypeInfo_var);
		List_1_t1_946 * L_0 = ((PhotonNetwork_t8_114_StaticFields*)PhotonNetwork_t8_114_il2cpp_TypeInfo_var->static_fields)->___U3CFriendsU3Ek__BackingField_31;
		return L_0;
	}
}
// System.Void PhotonNetwork::set_Friends(System.Collections.Generic.List`1<FriendInfo>)
extern TypeInfo* PhotonNetwork_t8_114_il2cpp_TypeInfo_var;
extern "C" void PhotonNetwork_set_Friends_m8_619 (Object_t * __this /* static, unused */, List_1_t1_946 * ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		PhotonNetwork_t8_114_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1058);
		s_Il2CppMethodIntialized = true;
	}
	{
		List_1_t1_946 * L_0 = ___value;
		IL2CPP_RUNTIME_CLASS_INIT(PhotonNetwork_t8_114_il2cpp_TypeInfo_var);
		((PhotonNetwork_t8_114_StaticFields*)PhotonNetwork_t8_114_il2cpp_TypeInfo_var->static_fields)->___U3CFriendsU3Ek__BackingField_31 = L_0;
		return;
	}
}
// System.Int32 PhotonNetwork::get_FriendsListAge()
extern TypeInfo* PhotonNetwork_t8_114_il2cpp_TypeInfo_var;
extern "C" int32_t PhotonNetwork_get_FriendsListAge_m8_620 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		PhotonNetwork_t8_114_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1058);
		s_Il2CppMethodIntialized = true;
	}
	int32_t G_B3_0 = 0;
	{
		IL2CPP_RUNTIME_CLASS_INIT(PhotonNetwork_t8_114_il2cpp_TypeInfo_var);
		NetworkingPeer_t8_98 * L_0 = ((PhotonNetwork_t8_114_StaticFields*)PhotonNetwork_t8_114_il2cpp_TypeInfo_var->static_fields)->___networkingPeer_4;
		if (!L_0)
		{
			goto IL_0019;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(PhotonNetwork_t8_114_il2cpp_TypeInfo_var);
		NetworkingPeer_t8_98 * L_1 = ((PhotonNetwork_t8_114_StaticFields*)PhotonNetwork_t8_114_il2cpp_TypeInfo_var->static_fields)->___networkingPeer_4;
		NullCheck(L_1);
		int32_t L_2 = NetworkingPeer_get_FriendsListAge_m8_397(L_1, /*hidden argument*/NULL);
		G_B3_0 = L_2;
		goto IL_001a;
	}

IL_0019:
	{
		G_B3_0 = 0;
	}

IL_001a:
	{
		return G_B3_0;
	}
}
// IPunPrefabPool PhotonNetwork::get_PrefabPool()
extern TypeInfo* PhotonNetwork_t8_114_il2cpp_TypeInfo_var;
extern "C" Object_t * PhotonNetwork_get_PrefabPool_m8_621 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		PhotonNetwork_t8_114_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1058);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(PhotonNetwork_t8_114_il2cpp_TypeInfo_var);
		NetworkingPeer_t8_98 * L_0 = ((PhotonNetwork_t8_114_StaticFields*)PhotonNetwork_t8_114_il2cpp_TypeInfo_var->static_fields)->___networkingPeer_4;
		NullCheck(L_0);
		Object_t * L_1 = (L_0->___ObjectPool_37);
		return L_1;
	}
}
// System.Void PhotonNetwork::set_PrefabPool(IPunPrefabPool)
extern TypeInfo* PhotonNetwork_t8_114_il2cpp_TypeInfo_var;
extern "C" void PhotonNetwork_set_PrefabPool_m8_622 (Object_t * __this /* static, unused */, Object_t * ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		PhotonNetwork_t8_114_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1058);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(PhotonNetwork_t8_114_il2cpp_TypeInfo_var);
		NetworkingPeer_t8_98 * L_0 = ((PhotonNetwork_t8_114_StaticFields*)PhotonNetwork_t8_114_il2cpp_TypeInfo_var->static_fields)->___networkingPeer_4;
		Object_t * L_1 = ___value;
		NullCheck(L_0);
		L_0->___ObjectPool_37 = L_1;
		return;
	}
}
// System.Boolean PhotonNetwork::get_offlineMode()
extern TypeInfo* PhotonNetwork_t8_114_il2cpp_TypeInfo_var;
extern "C" bool PhotonNetwork_get_offlineMode_m8_623 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		PhotonNetwork_t8_114_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1058);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(PhotonNetwork_t8_114_il2cpp_TypeInfo_var);
		bool L_0 = ((PhotonNetwork_t8_114_StaticFields*)PhotonNetwork_t8_114_il2cpp_TypeInfo_var->static_fields)->___isOfflineMode_17;
		return L_0;
	}
}
// System.Void PhotonNetwork::set_offlineMode(System.Boolean)
extern TypeInfo* PhotonNetwork_t8_114_il2cpp_TypeInfo_var;
extern TypeInfo* ObjectU5BU5D_t1_157_il2cpp_TypeInfo_var;
extern TypeInfo* NetworkingPeer_t8_98_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3404;
extern "C" void PhotonNetwork_set_offlineMode_m8_624 (Object_t * __this /* static, unused */, bool ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		PhotonNetwork_t8_114_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1058);
		ObjectU5BU5D_t1_157_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(19);
		NetworkingPeer_t8_98_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1121);
		_stringLiteral3404 = il2cpp_codegen_string_literal_from_index(3404);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool L_0 = ___value;
		IL2CPP_RUNTIME_CLASS_INIT(PhotonNetwork_t8_114_il2cpp_TypeInfo_var);
		bool L_1 = ((PhotonNetwork_t8_114_StaticFields*)PhotonNetwork_t8_114_il2cpp_TypeInfo_var->static_fields)->___isOfflineMode_17;
		if ((!(((uint32_t)L_0) == ((uint32_t)L_1))))
		{
			goto IL_000c;
		}
	}
	{
		return;
	}

IL_000c:
	{
		bool L_2 = ___value;
		if (!L_2)
		{
			goto IL_0027;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(PhotonNetwork_t8_114_il2cpp_TypeInfo_var);
		bool L_3 = PhotonNetwork_get_connected_m8_603(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_0027;
		}
	}
	{
		Debug_LogError_m6_490(NULL /*static, unused*/, _stringLiteral3404, /*hidden argument*/NULL);
		return;
	}

IL_0027:
	{
		IL2CPP_RUNTIME_CLASS_INIT(PhotonNetwork_t8_114_il2cpp_TypeInfo_var);
		NetworkingPeer_t8_98 * L_4 = ((PhotonNetwork_t8_114_StaticFields*)PhotonNetwork_t8_114_il2cpp_TypeInfo_var->static_fields)->___networkingPeer_4;
		NullCheck(L_4);
		uint8_t L_5 = PhotonPeer_get_PeerState_m5_211(L_4, /*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_0040;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(PhotonNetwork_t8_114_il2cpp_TypeInfo_var);
		NetworkingPeer_t8_98 * L_6 = ((PhotonNetwork_t8_114_StaticFields*)PhotonNetwork_t8_114_il2cpp_TypeInfo_var->static_fields)->___networkingPeer_4;
		NullCheck(L_6);
		VirtActionInvoker0::Invoke(7 /* System.Void NetworkingPeer::Disconnect() */, L_6);
	}

IL_0040:
	{
		bool L_7 = ___value;
		IL2CPP_RUNTIME_CLASS_INIT(PhotonNetwork_t8_114_il2cpp_TypeInfo_var);
		((PhotonNetwork_t8_114_StaticFields*)PhotonNetwork_t8_114_il2cpp_TypeInfo_var->static_fields)->___isOfflineMode_17 = L_7;
		bool L_8 = ((PhotonNetwork_t8_114_StaticFields*)PhotonNetwork_t8_114_il2cpp_TypeInfo_var->static_fields)->___isOfflineMode_17;
		if (!L_8)
		{
			goto IL_006d;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(PhotonNetwork_t8_114_il2cpp_TypeInfo_var);
		NetworkingPeer_t8_98 * L_9 = ((PhotonNetwork_t8_114_StaticFields*)PhotonNetwork_t8_114_il2cpp_TypeInfo_var->static_fields)->___networkingPeer_4;
		NullCheck(L_9);
		NetworkingPeer_ChangeLocalID_m8_433(L_9, (-1), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(NetworkingPeer_t8_98_il2cpp_TypeInfo_var);
		NetworkingPeer_SendMonoMessage_m8_446(NULL /*static, unused*/, ((int32_t)16), ((ObjectU5BU5D_t1_157*)SZArrayNew(ObjectU5BU5D_t1_157_il2cpp_TypeInfo_var, 0)), /*hidden argument*/NULL);
		goto IL_007e;
	}

IL_006d:
	{
		IL2CPP_RUNTIME_CLASS_INIT(PhotonNetwork_t8_114_il2cpp_TypeInfo_var);
		((PhotonNetwork_t8_114_StaticFields*)PhotonNetwork_t8_114_il2cpp_TypeInfo_var->static_fields)->___offlineModeRoom_18 = (Room_t8_100 *)NULL;
		NetworkingPeer_t8_98 * L_10 = ((PhotonNetwork_t8_114_StaticFields*)PhotonNetwork_t8_114_il2cpp_TypeInfo_var->static_fields)->___networkingPeer_4;
		NullCheck(L_10);
		NetworkingPeer_ChangeLocalID_m8_433(L_10, (-1), /*hidden argument*/NULL);
	}

IL_007e:
	{
		return;
	}
}
// System.Boolean PhotonNetwork::get_automaticallySyncScene()
extern TypeInfo* PhotonNetwork_t8_114_il2cpp_TypeInfo_var;
extern "C" bool PhotonNetwork_get_automaticallySyncScene_m8_625 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		PhotonNetwork_t8_114_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1058);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(PhotonNetwork_t8_114_il2cpp_TypeInfo_var);
		bool L_0 = ((PhotonNetwork_t8_114_StaticFields*)PhotonNetwork_t8_114_il2cpp_TypeInfo_var->static_fields)->____mAutomaticallySyncScene_20;
		return L_0;
	}
}
// System.Void PhotonNetwork::set_automaticallySyncScene(System.Boolean)
extern TypeInfo* PhotonNetwork_t8_114_il2cpp_TypeInfo_var;
extern "C" void PhotonNetwork_set_automaticallySyncScene_m8_626 (Object_t * __this /* static, unused */, bool ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		PhotonNetwork_t8_114_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1058);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool L_0 = ___value;
		IL2CPP_RUNTIME_CLASS_INIT(PhotonNetwork_t8_114_il2cpp_TypeInfo_var);
		((PhotonNetwork_t8_114_StaticFields*)PhotonNetwork_t8_114_il2cpp_TypeInfo_var->static_fields)->____mAutomaticallySyncScene_20 = L_0;
		bool L_1 = ((PhotonNetwork_t8_114_StaticFields*)PhotonNetwork_t8_114_il2cpp_TypeInfo_var->static_fields)->____mAutomaticallySyncScene_20;
		if (!L_1)
		{
			goto IL_0024;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(PhotonNetwork_t8_114_il2cpp_TypeInfo_var);
		Room_t8_100 * L_2 = PhotonNetwork_get_room_m8_611(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_0024;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(PhotonNetwork_t8_114_il2cpp_TypeInfo_var);
		NetworkingPeer_t8_98 * L_3 = ((PhotonNetwork_t8_114_StaticFields*)PhotonNetwork_t8_114_il2cpp_TypeInfo_var->static_fields)->___networkingPeer_4;
		NullCheck(L_3);
		NetworkingPeer_LoadLevelIfSynced_m8_490(L_3, /*hidden argument*/NULL);
	}

IL_0024:
	{
		return;
	}
}
// System.Boolean PhotonNetwork::get_autoCleanUpPlayerObjects()
extern TypeInfo* PhotonNetwork_t8_114_il2cpp_TypeInfo_var;
extern "C" bool PhotonNetwork_get_autoCleanUpPlayerObjects_m8_627 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		PhotonNetwork_t8_114_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1058);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(PhotonNetwork_t8_114_il2cpp_TypeInfo_var);
		bool L_0 = ((PhotonNetwork_t8_114_StaticFields*)PhotonNetwork_t8_114_il2cpp_TypeInfo_var->static_fields)->___m_autoCleanUpPlayerObjects_21;
		return L_0;
	}
}
// System.Void PhotonNetwork::set_autoCleanUpPlayerObjects(System.Boolean)
extern TypeInfo* PhotonNetwork_t8_114_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3405;
extern "C" void PhotonNetwork_set_autoCleanUpPlayerObjects_m8_628 (Object_t * __this /* static, unused */, bool ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		PhotonNetwork_t8_114_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1058);
		_stringLiteral3405 = il2cpp_codegen_string_literal_from_index(3405);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(PhotonNetwork_t8_114_il2cpp_TypeInfo_var);
		Room_t8_100 * L_0 = PhotonNetwork_get_room_m8_611(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_0019;
		}
	}
	{
		Debug_LogError_m6_490(NULL /*static, unused*/, _stringLiteral3405, /*hidden argument*/NULL);
		goto IL_001f;
	}

IL_0019:
	{
		bool L_1 = ___value;
		IL2CPP_RUNTIME_CLASS_INIT(PhotonNetwork_t8_114_il2cpp_TypeInfo_var);
		((PhotonNetwork_t8_114_StaticFields*)PhotonNetwork_t8_114_il2cpp_TypeInfo_var->static_fields)->___m_autoCleanUpPlayerObjects_21 = L_1;
	}

IL_001f:
	{
		return;
	}
}
// System.Boolean PhotonNetwork::get_autoJoinLobby()
extern TypeInfo* PhotonNetwork_t8_114_il2cpp_TypeInfo_var;
extern "C" bool PhotonNetwork_get_autoJoinLobby_m8_629 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		PhotonNetwork_t8_114_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1058);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(PhotonNetwork_t8_114_il2cpp_TypeInfo_var);
		ServerSettings_t8_115 * L_0 = ((PhotonNetwork_t8_114_StaticFields*)PhotonNetwork_t8_114_il2cpp_TypeInfo_var->static_fields)->___PhotonServerSettings_6;
		NullCheck(L_0);
		bool L_1 = (L_0->___JoinLobby_9);
		return L_1;
	}
}
// System.Void PhotonNetwork::set_autoJoinLobby(System.Boolean)
extern TypeInfo* PhotonNetwork_t8_114_il2cpp_TypeInfo_var;
extern "C" void PhotonNetwork_set_autoJoinLobby_m8_630 (Object_t * __this /* static, unused */, bool ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		PhotonNetwork_t8_114_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1058);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(PhotonNetwork_t8_114_il2cpp_TypeInfo_var);
		ServerSettings_t8_115 * L_0 = ((PhotonNetwork_t8_114_StaticFields*)PhotonNetwork_t8_114_il2cpp_TypeInfo_var->static_fields)->___PhotonServerSettings_6;
		bool L_1 = ___value;
		NullCheck(L_0);
		L_0->___JoinLobby_9 = L_1;
		return;
	}
}
// System.Boolean PhotonNetwork::get_EnableLobbyStatistics()
extern TypeInfo* PhotonNetwork_t8_114_il2cpp_TypeInfo_var;
extern "C" bool PhotonNetwork_get_EnableLobbyStatistics_m8_631 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		PhotonNetwork_t8_114_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1058);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(PhotonNetwork_t8_114_il2cpp_TypeInfo_var);
		ServerSettings_t8_115 * L_0 = ((PhotonNetwork_t8_114_StaticFields*)PhotonNetwork_t8_114_il2cpp_TypeInfo_var->static_fields)->___PhotonServerSettings_6;
		NullCheck(L_0);
		bool L_1 = (L_0->___EnableLobbyStatistics_10);
		return L_1;
	}
}
// System.Void PhotonNetwork::set_EnableLobbyStatistics(System.Boolean)
extern TypeInfo* PhotonNetwork_t8_114_il2cpp_TypeInfo_var;
extern "C" void PhotonNetwork_set_EnableLobbyStatistics_m8_632 (Object_t * __this /* static, unused */, bool ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		PhotonNetwork_t8_114_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1058);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(PhotonNetwork_t8_114_il2cpp_TypeInfo_var);
		ServerSettings_t8_115 * L_0 = ((PhotonNetwork_t8_114_StaticFields*)PhotonNetwork_t8_114_il2cpp_TypeInfo_var->static_fields)->___PhotonServerSettings_6;
		bool L_1 = ___value;
		NullCheck(L_0);
		L_0->___EnableLobbyStatistics_10 = L_1;
		return;
	}
}
// System.Collections.Generic.List`1<TypedLobbyInfo> PhotonNetwork::get_LobbyStatistics()
extern TypeInfo* PhotonNetwork_t8_114_il2cpp_TypeInfo_var;
extern "C" List_1_t1_934 * PhotonNetwork_get_LobbyStatistics_m8_633 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		PhotonNetwork_t8_114_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1058);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(PhotonNetwork_t8_114_il2cpp_TypeInfo_var);
		NetworkingPeer_t8_98 * L_0 = ((PhotonNetwork_t8_114_StaticFields*)PhotonNetwork_t8_114_il2cpp_TypeInfo_var->static_fields)->___networkingPeer_4;
		NullCheck(L_0);
		List_1_t1_934 * L_1 = (L_0->___LobbyStatistics_13);
		return L_1;
	}
}
// System.Void PhotonNetwork::set_LobbyStatistics(System.Collections.Generic.List`1<TypedLobbyInfo>)
extern TypeInfo* PhotonNetwork_t8_114_il2cpp_TypeInfo_var;
extern "C" void PhotonNetwork_set_LobbyStatistics_m8_634 (Object_t * __this /* static, unused */, List_1_t1_934 * ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		PhotonNetwork_t8_114_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1058);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(PhotonNetwork_t8_114_il2cpp_TypeInfo_var);
		NetworkingPeer_t8_98 * L_0 = ((PhotonNetwork_t8_114_StaticFields*)PhotonNetwork_t8_114_il2cpp_TypeInfo_var->static_fields)->___networkingPeer_4;
		List_1_t1_934 * L_1 = ___value;
		NullCheck(L_0);
		L_0->___LobbyStatistics_13 = L_1;
		return;
	}
}
// System.Boolean PhotonNetwork::get_insideLobby()
extern TypeInfo* PhotonNetwork_t8_114_il2cpp_TypeInfo_var;
extern "C" bool PhotonNetwork_get_insideLobby_m8_635 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		PhotonNetwork_t8_114_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1058);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(PhotonNetwork_t8_114_il2cpp_TypeInfo_var);
		NetworkingPeer_t8_98 * L_0 = ((PhotonNetwork_t8_114_StaticFields*)PhotonNetwork_t8_114_il2cpp_TypeInfo_var->static_fields)->___networkingPeer_4;
		NullCheck(L_0);
		bool L_1 = (L_0->___insideLobby_14);
		return L_1;
	}
}
// TypedLobby PhotonNetwork::get_lobby()
extern TypeInfo* PhotonNetwork_t8_114_il2cpp_TypeInfo_var;
extern "C" TypedLobby_t8_79 * PhotonNetwork_get_lobby_m8_636 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		PhotonNetwork_t8_114_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1058);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(PhotonNetwork_t8_114_il2cpp_TypeInfo_var);
		NetworkingPeer_t8_98 * L_0 = ((PhotonNetwork_t8_114_StaticFields*)PhotonNetwork_t8_114_il2cpp_TypeInfo_var->static_fields)->___networkingPeer_4;
		NullCheck(L_0);
		TypedLobby_t8_79 * L_1 = NetworkingPeer_get_lobby_m8_389(L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Void PhotonNetwork::set_lobby(TypedLobby)
extern TypeInfo* PhotonNetwork_t8_114_il2cpp_TypeInfo_var;
extern "C" void PhotonNetwork_set_lobby_m8_637 (Object_t * __this /* static, unused */, TypedLobby_t8_79 * ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		PhotonNetwork_t8_114_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1058);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(PhotonNetwork_t8_114_il2cpp_TypeInfo_var);
		NetworkingPeer_t8_98 * L_0 = ((PhotonNetwork_t8_114_StaticFields*)PhotonNetwork_t8_114_il2cpp_TypeInfo_var->static_fields)->___networkingPeer_4;
		TypedLobby_t8_79 * L_1 = ___value;
		NullCheck(L_0);
		NetworkingPeer_set_lobby_m8_390(L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Int32 PhotonNetwork::get_sendRate()
extern TypeInfo* PhotonNetwork_t8_114_il2cpp_TypeInfo_var;
extern "C" int32_t PhotonNetwork_get_sendRate_m8_638 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		PhotonNetwork_t8_114_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1058);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(PhotonNetwork_t8_114_il2cpp_TypeInfo_var);
		int32_t L_0 = ((PhotonNetwork_t8_114_StaticFields*)PhotonNetwork_t8_114_il2cpp_TypeInfo_var->static_fields)->___sendInterval_22;
		return ((int32_t)((int32_t)((int32_t)1000)/(int32_t)L_0));
	}
}
// System.Void PhotonNetwork::set_sendRate(System.Int32)
extern TypeInfo* PhotonNetwork_t8_114_il2cpp_TypeInfo_var;
extern "C" void PhotonNetwork_set_sendRate_m8_639 (Object_t * __this /* static, unused */, int32_t ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		PhotonNetwork_t8_114_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1058);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = ___value;
		IL2CPP_RUNTIME_CLASS_INIT(PhotonNetwork_t8_114_il2cpp_TypeInfo_var);
		((PhotonNetwork_t8_114_StaticFields*)PhotonNetwork_t8_114_il2cpp_TypeInfo_var->static_fields)->___sendInterval_22 = ((int32_t)((int32_t)((int32_t)1000)/(int32_t)L_0));
		PhotonHandler_t8_111 * L_1 = ((PhotonNetwork_t8_114_StaticFields*)PhotonNetwork_t8_114_il2cpp_TypeInfo_var->static_fields)->___photonMono_3;
		bool L_2 = Object_op_Inequality_m6_580(NULL /*static, unused*/, L_1, (Object_t6_5 *)NULL, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_002b;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(PhotonNetwork_t8_114_il2cpp_TypeInfo_var);
		PhotonHandler_t8_111 * L_3 = ((PhotonNetwork_t8_114_StaticFields*)PhotonNetwork_t8_114_il2cpp_TypeInfo_var->static_fields)->___photonMono_3;
		int32_t L_4 = ((PhotonNetwork_t8_114_StaticFields*)PhotonNetwork_t8_114_il2cpp_TypeInfo_var->static_fields)->___sendInterval_22;
		NullCheck(L_3);
		L_3->___updateInterval_4 = L_4;
	}

IL_002b:
	{
		int32_t L_5 = ___value;
		IL2CPP_RUNTIME_CLASS_INIT(PhotonNetwork_t8_114_il2cpp_TypeInfo_var);
		int32_t L_6 = PhotonNetwork_get_sendRateOnSerialize_m8_640(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((((int32_t)L_5) >= ((int32_t)L_6)))
		{
			goto IL_003c;
		}
	}
	{
		int32_t L_7 = ___value;
		IL2CPP_RUNTIME_CLASS_INIT(PhotonNetwork_t8_114_il2cpp_TypeInfo_var);
		PhotonNetwork_set_sendRateOnSerialize_m8_641(NULL /*static, unused*/, L_7, /*hidden argument*/NULL);
	}

IL_003c:
	{
		return;
	}
}
// System.Int32 PhotonNetwork::get_sendRateOnSerialize()
extern TypeInfo* PhotonNetwork_t8_114_il2cpp_TypeInfo_var;
extern "C" int32_t PhotonNetwork_get_sendRateOnSerialize_m8_640 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		PhotonNetwork_t8_114_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1058);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(PhotonNetwork_t8_114_il2cpp_TypeInfo_var);
		int32_t L_0 = ((PhotonNetwork_t8_114_StaticFields*)PhotonNetwork_t8_114_il2cpp_TypeInfo_var->static_fields)->___sendIntervalOnSerialize_23;
		return ((int32_t)((int32_t)((int32_t)1000)/(int32_t)L_0));
	}
}
// System.Void PhotonNetwork::set_sendRateOnSerialize(System.Int32)
extern TypeInfo* PhotonNetwork_t8_114_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3406;
extern "C" void PhotonNetwork_set_sendRateOnSerialize_m8_641 (Object_t * __this /* static, unused */, int32_t ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		PhotonNetwork_t8_114_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1058);
		_stringLiteral3406 = il2cpp_codegen_string_literal_from_index(3406);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = ___value;
		IL2CPP_RUNTIME_CLASS_INIT(PhotonNetwork_t8_114_il2cpp_TypeInfo_var);
		int32_t L_1 = PhotonNetwork_get_sendRate_m8_638(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((((int32_t)L_0) <= ((int32_t)L_1)))
		{
			goto IL_001c;
		}
	}
	{
		Debug_LogError_m6_490(NULL /*static, unused*/, _stringLiteral3406, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(PhotonNetwork_t8_114_il2cpp_TypeInfo_var);
		int32_t L_2 = PhotonNetwork_get_sendRate_m8_638(NULL /*static, unused*/, /*hidden argument*/NULL);
		___value = L_2;
	}

IL_001c:
	{
		int32_t L_3 = ___value;
		IL2CPP_RUNTIME_CLASS_INIT(PhotonNetwork_t8_114_il2cpp_TypeInfo_var);
		((PhotonNetwork_t8_114_StaticFields*)PhotonNetwork_t8_114_il2cpp_TypeInfo_var->static_fields)->___sendIntervalOnSerialize_23 = ((int32_t)((int32_t)((int32_t)1000)/(int32_t)L_3));
		PhotonHandler_t8_111 * L_4 = ((PhotonNetwork_t8_114_StaticFields*)PhotonNetwork_t8_114_il2cpp_TypeInfo_var->static_fields)->___photonMono_3;
		bool L_5 = Object_op_Inequality_m6_580(NULL /*static, unused*/, L_4, (Object_t6_5 *)NULL, /*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_0047;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(PhotonNetwork_t8_114_il2cpp_TypeInfo_var);
		PhotonHandler_t8_111 * L_6 = ((PhotonNetwork_t8_114_StaticFields*)PhotonNetwork_t8_114_il2cpp_TypeInfo_var->static_fields)->___photonMono_3;
		int32_t L_7 = ((PhotonNetwork_t8_114_StaticFields*)PhotonNetwork_t8_114_il2cpp_TypeInfo_var->static_fields)->___sendIntervalOnSerialize_23;
		NullCheck(L_6);
		L_6->___updateIntervalOnSerialize_5 = L_7;
	}

IL_0047:
	{
		return;
	}
}
// System.Boolean PhotonNetwork::get_isMessageQueueRunning()
extern TypeInfo* PhotonNetwork_t8_114_il2cpp_TypeInfo_var;
extern "C" bool PhotonNetwork_get_isMessageQueueRunning_m8_642 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		PhotonNetwork_t8_114_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1058);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(PhotonNetwork_t8_114_il2cpp_TypeInfo_var);
		bool L_0 = ((PhotonNetwork_t8_114_StaticFields*)PhotonNetwork_t8_114_il2cpp_TypeInfo_var->static_fields)->___m_isMessageQueueRunning_24;
		return L_0;
	}
}
// System.Void PhotonNetwork::set_isMessageQueueRunning(System.Boolean)
extern TypeInfo* PhotonHandler_t8_111_il2cpp_TypeInfo_var;
extern TypeInfo* PhotonNetwork_t8_114_il2cpp_TypeInfo_var;
extern "C" void PhotonNetwork_set_isMessageQueueRunning_m8_643 (Object_t * __this /* static, unused */, bool ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		PhotonHandler_t8_111_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1120);
		PhotonNetwork_t8_114_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1058);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool L_0 = ___value;
		if (!L_0)
		{
			goto IL_000b;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(PhotonHandler_t8_111_il2cpp_TypeInfo_var);
		PhotonHandler_StartFallbackSendAckThread_m8_581(NULL /*static, unused*/, /*hidden argument*/NULL);
	}

IL_000b:
	{
		IL2CPP_RUNTIME_CLASS_INIT(PhotonNetwork_t8_114_il2cpp_TypeInfo_var);
		NetworkingPeer_t8_98 * L_1 = ((PhotonNetwork_t8_114_StaticFields*)PhotonNetwork_t8_114_il2cpp_TypeInfo_var->static_fields)->___networkingPeer_4;
		bool L_2 = ___value;
		NullCheck(L_1);
		PhotonPeer_set_IsSendingOnlyAcks_m5_233(L_1, ((((int32_t)L_2) == ((int32_t)0))? 1 : 0), /*hidden argument*/NULL);
		bool L_3 = ___value;
		((PhotonNetwork_t8_114_StaticFields*)PhotonNetwork_t8_114_il2cpp_TypeInfo_var->static_fields)->___m_isMessageQueueRunning_24 = L_3;
		return;
	}
}
// System.Int32 PhotonNetwork::get_unreliableCommandsLimit()
extern TypeInfo* PhotonNetwork_t8_114_il2cpp_TypeInfo_var;
extern "C" int32_t PhotonNetwork_get_unreliableCommandsLimit_m8_644 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		PhotonNetwork_t8_114_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1058);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(PhotonNetwork_t8_114_il2cpp_TypeInfo_var);
		NetworkingPeer_t8_98 * L_0 = ((PhotonNetwork_t8_114_StaticFields*)PhotonNetwork_t8_114_il2cpp_TypeInfo_var->static_fields)->___networkingPeer_4;
		NullCheck(L_0);
		int32_t L_1 = PhotonPeer_get_LimitOfUnreliableCommands_m5_212(L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Void PhotonNetwork::set_unreliableCommandsLimit(System.Int32)
extern TypeInfo* PhotonNetwork_t8_114_il2cpp_TypeInfo_var;
extern "C" void PhotonNetwork_set_unreliableCommandsLimit_m8_645 (Object_t * __this /* static, unused */, int32_t ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		PhotonNetwork_t8_114_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1058);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(PhotonNetwork_t8_114_il2cpp_TypeInfo_var);
		NetworkingPeer_t8_98 * L_0 = ((PhotonNetwork_t8_114_StaticFields*)PhotonNetwork_t8_114_il2cpp_TypeInfo_var->static_fields)->___networkingPeer_4;
		int32_t L_1 = ___value;
		NullCheck(L_0);
		PhotonPeer_set_LimitOfUnreliableCommands_m5_213(L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Double PhotonNetwork::get_time()
extern TypeInfo* PhotonNetwork_t8_114_il2cpp_TypeInfo_var;
extern "C" double PhotonNetwork_get_time_m8_646 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		PhotonNetwork_t8_114_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1058);
		s_Il2CppMethodIntialized = true;
	}
	uint32_t V_0 = 0;
	double V_1 = 0.0;
	{
		IL2CPP_RUNTIME_CLASS_INIT(PhotonNetwork_t8_114_il2cpp_TypeInfo_var);
		bool L_0 = PhotonNetwork_get_offlineMode_m8_623(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_0011;
		}
	}
	{
		float L_1 = Time_get_time_m6_652(NULL /*static, unused*/, /*hidden argument*/NULL);
		return (((double)((double)L_1)));
	}

IL_0011:
	{
		IL2CPP_RUNTIME_CLASS_INIT(PhotonNetwork_t8_114_il2cpp_TypeInfo_var);
		NetworkingPeer_t8_98 * L_2 = ((PhotonNetwork_t8_114_StaticFields*)PhotonNetwork_t8_114_il2cpp_TypeInfo_var->static_fields)->___networkingPeer_4;
		NullCheck(L_2);
		int32_t L_3 = PhotonPeer_get_ServerTimeInMilliSeconds_m5_223(L_2, /*hidden argument*/NULL);
		V_0 = L_3;
		uint32_t L_4 = V_0;
		V_1 = (((double)((double)(((double)((double)L_4))))));
		double L_5 = V_1;
		return ((double)((double)L_5/(double)(1000.0)));
	}
}
// System.Int32 PhotonNetwork::get_ServerTimestamp()
extern TypeInfo* PhotonNetwork_t8_114_il2cpp_TypeInfo_var;
extern "C" int32_t PhotonNetwork_get_ServerTimestamp_m8_647 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		PhotonNetwork_t8_114_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1058);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(PhotonNetwork_t8_114_il2cpp_TypeInfo_var);
		bool L_0 = PhotonNetwork_get_offlineMode_m8_623(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_0010;
		}
	}
	{
		int32_t L_1 = Environment_get_TickCount_m1_5041(NULL /*static, unused*/, /*hidden argument*/NULL);
		return L_1;
	}

IL_0010:
	{
		IL2CPP_RUNTIME_CLASS_INIT(PhotonNetwork_t8_114_il2cpp_TypeInfo_var);
		NetworkingPeer_t8_98 * L_2 = ((PhotonNetwork_t8_114_StaticFields*)PhotonNetwork_t8_114_il2cpp_TypeInfo_var->static_fields)->___networkingPeer_4;
		NullCheck(L_2);
		int32_t L_3 = PhotonPeer_get_ServerTimeInMilliSeconds_m5_223(L_2, /*hidden argument*/NULL);
		return L_3;
	}
}
// System.Boolean PhotonNetwork::get_isMasterClient()
extern TypeInfo* PhotonNetwork_t8_114_il2cpp_TypeInfo_var;
extern "C" bool PhotonNetwork_get_isMasterClient_m8_648 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		PhotonNetwork_t8_114_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1058);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(PhotonNetwork_t8_114_il2cpp_TypeInfo_var);
		bool L_0 = PhotonNetwork_get_offlineMode_m8_623(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_000c;
		}
	}
	{
		return 1;
	}

IL_000c:
	{
		IL2CPP_RUNTIME_CLASS_INIT(PhotonNetwork_t8_114_il2cpp_TypeInfo_var);
		NetworkingPeer_t8_98 * L_1 = ((PhotonNetwork_t8_114_StaticFields*)PhotonNetwork_t8_114_il2cpp_TypeInfo_var->static_fields)->___networkingPeer_4;
		NullCheck(L_1);
		int32_t L_2 = NetworkingPeer_get_mMasterClientId_m8_404(L_1, /*hidden argument*/NULL);
		PhotonPlayer_t8_102 * L_3 = PhotonNetwork_get_player_m8_612(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_3);
		int32_t L_4 = PhotonPlayer_get_ID_m8_730(L_3, /*hidden argument*/NULL);
		return ((((int32_t)L_2) == ((int32_t)L_4))? 1 : 0);
	}
}
// System.Boolean PhotonNetwork::get_inRoom()
extern TypeInfo* PhotonNetwork_t8_114_il2cpp_TypeInfo_var;
extern "C" bool PhotonNetwork_get_inRoom_m8_649 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		PhotonNetwork_t8_114_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1058);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(PhotonNetwork_t8_114_il2cpp_TypeInfo_var);
		int32_t L_0 = PhotonNetwork_get_connectionStateDetailed_m8_607(NULL /*static, unused*/, /*hidden argument*/NULL);
		return ((((int32_t)L_0) == ((int32_t)((int32_t)9)))? 1 : 0);
	}
}
// System.Boolean PhotonNetwork::get_isNonMasterClientInRoom()
extern TypeInfo* PhotonNetwork_t8_114_il2cpp_TypeInfo_var;
extern "C" bool PhotonNetwork_get_isNonMasterClientInRoom_m8_650 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		PhotonNetwork_t8_114_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1058);
		s_Il2CppMethodIntialized = true;
	}
	int32_t G_B3_0 = 0;
	{
		IL2CPP_RUNTIME_CLASS_INIT(PhotonNetwork_t8_114_il2cpp_TypeInfo_var);
		bool L_0 = PhotonNetwork_get_isMasterClient_m8_648(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (L_0)
		{
			goto IL_0017;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(PhotonNetwork_t8_114_il2cpp_TypeInfo_var);
		Room_t8_100 * L_1 = PhotonNetwork_get_room_m8_611(NULL /*static, unused*/, /*hidden argument*/NULL);
		G_B3_0 = ((((int32_t)((((Object_t*)(Room_t8_100 *)L_1) == ((Object_t*)(Object_t *)NULL))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0018;
	}

IL_0017:
	{
		G_B3_0 = 0;
	}

IL_0018:
	{
		return G_B3_0;
	}
}
// System.Int32 PhotonNetwork::get_countOfPlayersOnMaster()
extern TypeInfo* PhotonNetwork_t8_114_il2cpp_TypeInfo_var;
extern "C" int32_t PhotonNetwork_get_countOfPlayersOnMaster_m8_651 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		PhotonNetwork_t8_114_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1058);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(PhotonNetwork_t8_114_il2cpp_TypeInfo_var);
		NetworkingPeer_t8_98 * L_0 = ((PhotonNetwork_t8_114_StaticFields*)PhotonNetwork_t8_114_il2cpp_TypeInfo_var->static_fields)->___networkingPeer_4;
		NullCheck(L_0);
		int32_t L_1 = NetworkingPeer_get_mPlayersOnMasterCount_m8_391(L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Int32 PhotonNetwork::get_countOfPlayersInRooms()
extern TypeInfo* PhotonNetwork_t8_114_il2cpp_TypeInfo_var;
extern "C" int32_t PhotonNetwork_get_countOfPlayersInRooms_m8_652 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		PhotonNetwork_t8_114_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1058);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(PhotonNetwork_t8_114_il2cpp_TypeInfo_var);
		NetworkingPeer_t8_98 * L_0 = ((PhotonNetwork_t8_114_StaticFields*)PhotonNetwork_t8_114_il2cpp_TypeInfo_var->static_fields)->___networkingPeer_4;
		NullCheck(L_0);
		int32_t L_1 = NetworkingPeer_get_mPlayersInRoomsCount_m8_395(L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Int32 PhotonNetwork::get_countOfPlayers()
extern TypeInfo* PhotonNetwork_t8_114_il2cpp_TypeInfo_var;
extern "C" int32_t PhotonNetwork_get_countOfPlayers_m8_653 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		PhotonNetwork_t8_114_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1058);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(PhotonNetwork_t8_114_il2cpp_TypeInfo_var);
		NetworkingPeer_t8_98 * L_0 = ((PhotonNetwork_t8_114_StaticFields*)PhotonNetwork_t8_114_il2cpp_TypeInfo_var->static_fields)->___networkingPeer_4;
		NullCheck(L_0);
		int32_t L_1 = NetworkingPeer_get_mPlayersInRoomsCount_m8_395(L_0, /*hidden argument*/NULL);
		NetworkingPeer_t8_98 * L_2 = ((PhotonNetwork_t8_114_StaticFields*)PhotonNetwork_t8_114_il2cpp_TypeInfo_var->static_fields)->___networkingPeer_4;
		NullCheck(L_2);
		int32_t L_3 = NetworkingPeer_get_mPlayersOnMasterCount_m8_391(L_2, /*hidden argument*/NULL);
		return ((int32_t)((int32_t)L_1+(int32_t)L_3));
	}
}
// System.Int32 PhotonNetwork::get_countOfRooms()
extern TypeInfo* PhotonNetwork_t8_114_il2cpp_TypeInfo_var;
extern "C" int32_t PhotonNetwork_get_countOfRooms_m8_654 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		PhotonNetwork_t8_114_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1058);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(PhotonNetwork_t8_114_il2cpp_TypeInfo_var);
		NetworkingPeer_t8_98 * L_0 = ((PhotonNetwork_t8_114_StaticFields*)PhotonNetwork_t8_114_il2cpp_TypeInfo_var->static_fields)->___networkingPeer_4;
		NullCheck(L_0);
		int32_t L_1 = NetworkingPeer_get_mGameCount_m8_393(L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Boolean PhotonNetwork::get_NetworkStatisticsEnabled()
extern TypeInfo* PhotonNetwork_t8_114_il2cpp_TypeInfo_var;
extern "C" bool PhotonNetwork_get_NetworkStatisticsEnabled_m8_655 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		PhotonNetwork_t8_114_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1058);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(PhotonNetwork_t8_114_il2cpp_TypeInfo_var);
		NetworkingPeer_t8_98 * L_0 = ((PhotonNetwork_t8_114_StaticFields*)PhotonNetwork_t8_114_il2cpp_TypeInfo_var->static_fields)->___networkingPeer_4;
		NullCheck(L_0);
		bool L_1 = PhotonPeer_get_TrafficStatsEnabled_m5_202(L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Void PhotonNetwork::set_NetworkStatisticsEnabled(System.Boolean)
extern TypeInfo* PhotonNetwork_t8_114_il2cpp_TypeInfo_var;
extern "C" void PhotonNetwork_set_NetworkStatisticsEnabled_m8_656 (Object_t * __this /* static, unused */, bool ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		PhotonNetwork_t8_114_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1058);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(PhotonNetwork_t8_114_il2cpp_TypeInfo_var);
		NetworkingPeer_t8_98 * L_0 = ((PhotonNetwork_t8_114_StaticFields*)PhotonNetwork_t8_114_il2cpp_TypeInfo_var->static_fields)->___networkingPeer_4;
		bool L_1 = ___value;
		NullCheck(L_0);
		PhotonPeer_set_TrafficStatsEnabled_m5_203(L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Int32 PhotonNetwork::get_ResentReliableCommands()
extern TypeInfo* PhotonNetwork_t8_114_il2cpp_TypeInfo_var;
extern "C" int32_t PhotonNetwork_get_ResentReliableCommands_m8_657 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		PhotonNetwork_t8_114_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1058);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(PhotonNetwork_t8_114_il2cpp_TypeInfo_var);
		NetworkingPeer_t8_98 * L_0 = ((PhotonNetwork_t8_114_StaticFields*)PhotonNetwork_t8_114_il2cpp_TypeInfo_var->static_fields)->___networkingPeer_4;
		NullCheck(L_0);
		int32_t L_1 = PhotonPeer_get_ResentReliableCommands_m5_217(L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Boolean PhotonNetwork::get_CrcCheckEnabled()
extern TypeInfo* PhotonNetwork_t8_114_il2cpp_TypeInfo_var;
extern "C" bool PhotonNetwork_get_CrcCheckEnabled_m8_658 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		PhotonNetwork_t8_114_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1058);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(PhotonNetwork_t8_114_il2cpp_TypeInfo_var);
		NetworkingPeer_t8_98 * L_0 = ((PhotonNetwork_t8_114_StaticFields*)PhotonNetwork_t8_114_il2cpp_TypeInfo_var->static_fields)->___networkingPeer_4;
		NullCheck(L_0);
		bool L_1 = PhotonPeer_get_CrcEnabled_m5_214(L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Void PhotonNetwork::set_CrcCheckEnabled(System.Boolean)
extern TypeInfo* PhotonNetwork_t8_114_il2cpp_TypeInfo_var;
extern TypeInfo* Boolean_t1_20_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3407;
extern "C" void PhotonNetwork_set_CrcCheckEnabled_m8_659 (Object_t * __this /* static, unused */, bool ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		PhotonNetwork_t8_114_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1058);
		Boolean_t1_20_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(49);
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(11);
		_stringLiteral3407 = il2cpp_codegen_string_literal_from_index(3407);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(PhotonNetwork_t8_114_il2cpp_TypeInfo_var);
		bool L_0 = PhotonNetwork_get_connected_m8_603(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (L_0)
		{
			goto IL_0024;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(PhotonNetwork_t8_114_il2cpp_TypeInfo_var);
		bool L_1 = PhotonNetwork_get_connecting_m8_604(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (L_1)
		{
			goto IL_0024;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(PhotonNetwork_t8_114_il2cpp_TypeInfo_var);
		NetworkingPeer_t8_98 * L_2 = ((PhotonNetwork_t8_114_StaticFields*)PhotonNetwork_t8_114_il2cpp_TypeInfo_var->static_fields)->___networkingPeer_4;
		bool L_3 = ___value;
		NullCheck(L_2);
		PhotonPeer_set_CrcEnabled_m5_215(L_2, L_3, /*hidden argument*/NULL);
		goto IL_0042;
	}

IL_0024:
	{
		IL2CPP_RUNTIME_CLASS_INIT(PhotonNetwork_t8_114_il2cpp_TypeInfo_var);
		NetworkingPeer_t8_98 * L_4 = ((PhotonNetwork_t8_114_StaticFields*)PhotonNetwork_t8_114_il2cpp_TypeInfo_var->static_fields)->___networkingPeer_4;
		NullCheck(L_4);
		bool L_5 = PhotonPeer_get_CrcEnabled_m5_214(L_4, /*hidden argument*/NULL);
		bool L_6 = L_5;
		Object_t * L_7 = Box(Boolean_t1_20_il2cpp_TypeInfo_var, &L_6);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_8 = String_Concat_m1_416(NULL /*static, unused*/, _stringLiteral3407, L_7, /*hidden argument*/NULL);
		Debug_Log_m6_489(NULL /*static, unused*/, L_8, /*hidden argument*/NULL);
	}

IL_0042:
	{
		return;
	}
}
// System.Int32 PhotonNetwork::get_PacketLossByCrcCheck()
extern TypeInfo* PhotonNetwork_t8_114_il2cpp_TypeInfo_var;
extern "C" int32_t PhotonNetwork_get_PacketLossByCrcCheck_m8_660 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		PhotonNetwork_t8_114_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1058);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(PhotonNetwork_t8_114_il2cpp_TypeInfo_var);
		NetworkingPeer_t8_98 * L_0 = ((PhotonNetwork_t8_114_StaticFields*)PhotonNetwork_t8_114_il2cpp_TypeInfo_var->static_fields)->___networkingPeer_4;
		NullCheck(L_0);
		int32_t L_1 = PhotonPeer_get_PacketLossByCrc_m5_216(L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Int32 PhotonNetwork::get_MaxResendsBeforeDisconnect()
extern TypeInfo* PhotonNetwork_t8_114_il2cpp_TypeInfo_var;
extern "C" int32_t PhotonNetwork_get_MaxResendsBeforeDisconnect_m8_661 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		PhotonNetwork_t8_114_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1058);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(PhotonNetwork_t8_114_il2cpp_TypeInfo_var);
		NetworkingPeer_t8_98 * L_0 = ((PhotonNetwork_t8_114_StaticFields*)PhotonNetwork_t8_114_il2cpp_TypeInfo_var->static_fields)->___networkingPeer_4;
		NullCheck(L_0);
		int32_t L_1 = PhotonPeer_get_SentCountAllowance_m5_218(L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Void PhotonNetwork::set_MaxResendsBeforeDisconnect(System.Int32)
extern TypeInfo* PhotonNetwork_t8_114_il2cpp_TypeInfo_var;
extern "C" void PhotonNetwork_set_MaxResendsBeforeDisconnect_m8_662 (Object_t * __this /* static, unused */, int32_t ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		PhotonNetwork_t8_114_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1058);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = ___value;
		if ((((int32_t)L_0) >= ((int32_t)3)))
		{
			goto IL_000a;
		}
	}
	{
		___value = 3;
	}

IL_000a:
	{
		int32_t L_1 = ___value;
		if ((((int32_t)L_1) <= ((int32_t)((int32_t)10))))
		{
			goto IL_0016;
		}
	}
	{
		___value = ((int32_t)10);
	}

IL_0016:
	{
		IL2CPP_RUNTIME_CLASS_INIT(PhotonNetwork_t8_114_il2cpp_TypeInfo_var);
		NetworkingPeer_t8_98 * L_2 = ((PhotonNetwork_t8_114_StaticFields*)PhotonNetwork_t8_114_il2cpp_TypeInfo_var->static_fields)->___networkingPeer_4;
		int32_t L_3 = ___value;
		NullCheck(L_2);
		PhotonPeer_set_SentCountAllowance_m5_219(L_2, L_3, /*hidden argument*/NULL);
		return;
	}
}
// System.Int32 PhotonNetwork::get_QuickResends()
extern TypeInfo* PhotonNetwork_t8_114_il2cpp_TypeInfo_var;
extern "C" int32_t PhotonNetwork_get_QuickResends_m8_663 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		PhotonNetwork_t8_114_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1058);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(PhotonNetwork_t8_114_il2cpp_TypeInfo_var);
		NetworkingPeer_t8_98 * L_0 = ((PhotonNetwork_t8_114_StaticFields*)PhotonNetwork_t8_114_il2cpp_TypeInfo_var->static_fields)->___networkingPeer_4;
		NullCheck(L_0);
		uint8_t L_1 = PhotonPeer_get_QuickResendAttempts_m5_209(L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Void PhotonNetwork::set_QuickResends(System.Int32)
extern TypeInfo* PhotonNetwork_t8_114_il2cpp_TypeInfo_var;
extern "C" void PhotonNetwork_set_QuickResends_m8_664 (Object_t * __this /* static, unused */, int32_t ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		PhotonNetwork_t8_114_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1058);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = ___value;
		if ((((int32_t)L_0) >= ((int32_t)0)))
		{
			goto IL_000a;
		}
	}
	{
		___value = 0;
	}

IL_000a:
	{
		int32_t L_1 = ___value;
		if ((((int32_t)L_1) <= ((int32_t)3)))
		{
			goto IL_0014;
		}
	}
	{
		___value = 3;
	}

IL_0014:
	{
		IL2CPP_RUNTIME_CLASS_INIT(PhotonNetwork_t8_114_il2cpp_TypeInfo_var);
		NetworkingPeer_t8_98 * L_2 = ((PhotonNetwork_t8_114_StaticFields*)PhotonNetwork_t8_114_il2cpp_TypeInfo_var->static_fields)->___networkingPeer_4;
		int32_t L_3 = ___value;
		NullCheck(L_2);
		PhotonPeer_set_QuickResendAttempts_m5_210(L_2, (((int32_t)((uint8_t)L_3))), /*hidden argument*/NULL);
		return;
	}
}
// System.Void PhotonNetwork::SwitchToProtocol(ExitGames.Client.Photon.ConnectionProtocol)
extern TypeInfo* PhotonNetwork_t8_114_il2cpp_TypeInfo_var;
extern TypeInfo* Object_t_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* NetworkingPeer_t8_98_il2cpp_TypeInfo_var;
extern TypeInfo* ConnectionProtocol_t5_36_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3408;
extern Il2CppCodeGenString* _stringLiteral49;
extern "C" void PhotonNetwork_SwitchToProtocol_m8_665 (Object_t * __this /* static, unused */, uint8_t ___cp, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		PhotonNetwork_t8_114_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1058);
		Object_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(0);
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(11);
		NetworkingPeer_t8_98_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1121);
		ConnectionProtocol_t5_36_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1056);
		_stringLiteral3408 = il2cpp_codegen_string_literal_from_index(3408);
		_stringLiteral49 = il2cpp_codegen_string_literal_from_index(49);
		s_Il2CppMethodIntialized = true;
	}
	NetworkingPeer_t8_98 * V_0 = {0};
	Exception_t1_33 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1_33 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		IL2CPP_RUNTIME_CLASS_INIT(PhotonNetwork_t8_114_il2cpp_TypeInfo_var);
		NetworkingPeer_t8_98 * L_0 = ((PhotonNetwork_t8_114_StaticFields*)PhotonNetwork_t8_114_il2cpp_TypeInfo_var->static_fields)->___networkingPeer_4;
		NullCheck(L_0);
		uint8_t L_1 = PhotonPeer_get_UsedProtocol_m5_228(L_0, /*hidden argument*/NULL);
		uint8_t L_2 = ___cp;
		if ((!(((uint32_t)L_1) == ((uint32_t)L_2))))
		{
			goto IL_0011;
		}
	}
	{
		return;
	}

IL_0011:
	try
	{ // begin try (depth: 1)
		IL2CPP_RUNTIME_CLASS_INIT(PhotonNetwork_t8_114_il2cpp_TypeInfo_var);
		NetworkingPeer_t8_98 * L_3 = ((PhotonNetwork_t8_114_StaticFields*)PhotonNetwork_t8_114_il2cpp_TypeInfo_var->static_fields)->___networkingPeer_4;
		NullCheck(L_3);
		VirtActionInvoker0::Invoke(7 /* System.Void NetworkingPeer::Disconnect() */, L_3);
		NetworkingPeer_t8_98 * L_4 = ((PhotonNetwork_t8_114_StaticFields*)PhotonNetwork_t8_114_il2cpp_TypeInfo_var->static_fields)->___networkingPeer_4;
		NullCheck(L_4);
		VirtActionInvoker0::Invoke(8 /* System.Void ExitGames.Client.Photon.PhotonPeer::StopThread() */, L_4);
		goto IL_0030;
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t1_33 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (Object_t_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_002a;
		throw e;
	}

CATCH_002a:
	{ // begin catch(System.Object)
		goto IL_0030;
	} // end catch (depth: 1)

IL_0030:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_5 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->___Empty_2;
		uint8_t L_6 = ___cp;
		NetworkingPeer_t8_98 * L_7 = (NetworkingPeer_t8_98 *)il2cpp_codegen_object_new (NetworkingPeer_t8_98_il2cpp_TypeInfo_var);
		NetworkingPeer__ctor_m8_367(L_7, L_5, L_6, /*hidden argument*/NULL);
		V_0 = L_7;
		NetworkingPeer_t8_98 * L_8 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(PhotonNetwork_t8_114_il2cpp_TypeInfo_var);
		NetworkingPeer_t8_98 * L_9 = ((PhotonNetwork_t8_114_StaticFields*)PhotonNetwork_t8_114_il2cpp_TypeInfo_var->static_fields)->___networkingPeer_4;
		NullCheck(L_9);
		AuthenticationValues_t8_97 * L_10 = NetworkingPeer_get_CustomAuthenticationValues_m8_370(L_9, /*hidden argument*/NULL);
		NullCheck(L_8);
		NetworkingPeer_set_CustomAuthenticationValues_m8_371(L_8, L_10, /*hidden argument*/NULL);
		NetworkingPeer_t8_98 * L_11 = V_0;
		NetworkingPeer_t8_98 * L_12 = ((PhotonNetwork_t8_114_StaticFields*)PhotonNetwork_t8_114_il2cpp_TypeInfo_var->static_fields)->___networkingPeer_4;
		NullCheck(L_12);
		String_t* L_13 = NetworkingPeer_get_PlayerName_m8_398(L_12, /*hidden argument*/NULL);
		NullCheck(L_11);
		NetworkingPeer_set_PlayerName_m8_399(L_11, L_13, /*hidden argument*/NULL);
		NetworkingPeer_t8_98 * L_14 = V_0;
		NetworkingPeer_t8_98 * L_15 = ((PhotonNetwork_t8_114_StaticFields*)PhotonNetwork_t8_114_il2cpp_TypeInfo_var->static_fields)->___networkingPeer_4;
		NullCheck(L_15);
		PhotonPlayer_t8_102 * L_16 = NetworkingPeer_get_mLocalActor_m8_402(L_15, /*hidden argument*/NULL);
		NullCheck(L_14);
		NetworkingPeer_set_mLocalActor_m8_403(L_14, L_16, /*hidden argument*/NULL);
		NetworkingPeer_t8_98 * L_17 = V_0;
		NetworkingPeer_t8_98 * L_18 = ((PhotonNetwork_t8_114_StaticFields*)PhotonNetwork_t8_114_il2cpp_TypeInfo_var->static_fields)->___networkingPeer_4;
		NullCheck(L_18);
		uint8_t L_19 = PhotonPeer_get_DebugOut_m5_199(L_18, /*hidden argument*/NULL);
		NullCheck(L_17);
		PhotonPeer_set_DebugOut_m5_198(L_17, L_19, /*hidden argument*/NULL);
		NetworkingPeer_t8_98 * L_20 = V_0;
		NetworkingPeer_t8_98 * L_21 = ((PhotonNetwork_t8_114_StaticFields*)PhotonNetwork_t8_114_il2cpp_TypeInfo_var->static_fields)->___networkingPeer_4;
		NullCheck(L_21);
		bool L_22 = PhotonPeer_get_CrcEnabled_m5_214(L_21, /*hidden argument*/NULL);
		NullCheck(L_20);
		PhotonPeer_set_CrcEnabled_m5_215(L_20, L_22, /*hidden argument*/NULL);
		NetworkingPeer_t8_98 * L_23 = V_0;
		NetworkingPeer_t8_98 * L_24 = ((PhotonNetwork_t8_114_StaticFields*)PhotonNetwork_t8_114_il2cpp_TypeInfo_var->static_fields)->___networkingPeer_4;
		NullCheck(L_24);
		uint8_t L_25 = PhotonPeer_get_QuickResendAttempts_m5_209(L_24, /*hidden argument*/NULL);
		NullCheck(L_23);
		PhotonPeer_set_QuickResendAttempts_m5_210(L_23, L_25, /*hidden argument*/NULL);
		NetworkingPeer_t8_98 * L_26 = V_0;
		NetworkingPeer_t8_98 * L_27 = ((PhotonNetwork_t8_114_StaticFields*)PhotonNetwork_t8_114_il2cpp_TypeInfo_var->static_fields)->___networkingPeer_4;
		NullCheck(L_27);
		int32_t L_28 = PhotonPeer_get_DisconnectTimeout_m5_221(L_27, /*hidden argument*/NULL);
		NullCheck(L_26);
		PhotonPeer_set_DisconnectTimeout_m5_222(L_26, L_28, /*hidden argument*/NULL);
		NetworkingPeer_t8_98 * L_29 = V_0;
		NetworkingPeer_t8_98 * L_30 = ((PhotonNetwork_t8_114_StaticFields*)PhotonNetwork_t8_114_il2cpp_TypeInfo_var->static_fields)->___networkingPeer_4;
		NullCheck(L_30);
		TypedLobby_t8_79 * L_31 = NetworkingPeer_get_lobby_m8_389(L_30, /*hidden argument*/NULL);
		NullCheck(L_29);
		NetworkingPeer_set_lobby_m8_390(L_29, L_31, /*hidden argument*/NULL);
		NetworkingPeer_t8_98 * L_32 = V_0;
		NetworkingPeer_t8_98 * L_33 = ((PhotonNetwork_t8_114_StaticFields*)PhotonNetwork_t8_114_il2cpp_TypeInfo_var->static_fields)->___networkingPeer_4;
		NullCheck(L_33);
		int32_t L_34 = PhotonPeer_get_LimitOfUnreliableCommands_m5_212(L_33, /*hidden argument*/NULL);
		NullCheck(L_32);
		PhotonPeer_set_LimitOfUnreliableCommands_m5_213(L_32, L_34, /*hidden argument*/NULL);
		NetworkingPeer_t8_98 * L_35 = V_0;
		NetworkingPeer_t8_98 * L_36 = ((PhotonNetwork_t8_114_StaticFields*)PhotonNetwork_t8_114_il2cpp_TypeInfo_var->static_fields)->___networkingPeer_4;
		NullCheck(L_36);
		int32_t L_37 = PhotonPeer_get_SentCountAllowance_m5_218(L_36, /*hidden argument*/NULL);
		NullCheck(L_35);
		PhotonPeer_set_SentCountAllowance_m5_219(L_35, L_37, /*hidden argument*/NULL);
		NetworkingPeer_t8_98 * L_38 = V_0;
		NetworkingPeer_t8_98 * L_39 = ((PhotonNetwork_t8_114_StaticFields*)PhotonNetwork_t8_114_il2cpp_TypeInfo_var->static_fields)->___networkingPeer_4;
		NullCheck(L_39);
		bool L_40 = PhotonPeer_get_TrafficStatsEnabled_m5_202(L_39, /*hidden argument*/NULL);
		NullCheck(L_38);
		PhotonPeer_set_TrafficStatsEnabled_m5_203(L_38, L_40, /*hidden argument*/NULL);
		NetworkingPeer_t8_98 * L_41 = V_0;
		((PhotonNetwork_t8_114_StaticFields*)PhotonNetwork_t8_114_il2cpp_TypeInfo_var->static_fields)->___networkingPeer_4 = L_41;
		uint8_t L_42 = ___cp;
		uint8_t L_43 = L_42;
		Object_t * L_44 = Box(ConnectionProtocol_t5_36_il2cpp_TypeInfo_var, &L_43);
		String_t* L_45 = String_Concat_m1_417(NULL /*static, unused*/, _stringLiteral3408, L_44, _stringLiteral49, /*hidden argument*/NULL);
		Debug_LogWarning_m6_491(NULL /*static, unused*/, L_45, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean PhotonNetwork::ConnectUsingSettings(System.String)
extern TypeInfo* PhotonNetwork_t8_114_il2cpp_TypeInfo_var;
extern TypeInfo* PeerStateValue_t5_35_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* Int32_t1_3_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3409;
extern Il2CppCodeGenString* _stringLiteral3410;
extern Il2CppCodeGenString* _stringLiteral3411;
extern Il2CppCodeGenString* _stringLiteral3412;
extern Il2CppCodeGenString* _stringLiteral633;
extern "C" bool PhotonNetwork_ConnectUsingSettings_m8_666 (Object_t * __this /* static, unused */, String_t* ___gameVersion, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		PhotonNetwork_t8_114_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1058);
		PeerStateValue_t5_35_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1156);
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(11);
		Int32_t1_3_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(8);
		_stringLiteral3409 = il2cpp_codegen_string_literal_from_index(3409);
		_stringLiteral3410 = il2cpp_codegen_string_literal_from_index(3410);
		_stringLiteral3411 = il2cpp_codegen_string_literal_from_index(3411);
		_stringLiteral3412 = il2cpp_codegen_string_literal_from_index(3412);
		_stringLiteral633 = il2cpp_codegen_string_literal_from_index(633);
		s_Il2CppMethodIntialized = true;
	}
	NetworkingPeer_t8_98 * G_B13_0 = {0};
	NetworkingPeer_t8_98 * G_B12_0 = {0};
	String_t* G_B14_0 = {0};
	NetworkingPeer_t8_98 * G_B14_1 = {0};
	{
		IL2CPP_RUNTIME_CLASS_INIT(PhotonNetwork_t8_114_il2cpp_TypeInfo_var);
		NetworkingPeer_t8_98 * L_0 = ((PhotonNetwork_t8_114_StaticFields*)PhotonNetwork_t8_114_il2cpp_TypeInfo_var->static_fields)->___networkingPeer_4;
		NullCheck(L_0);
		uint8_t L_1 = PhotonPeer_get_PeerState_m5_211(L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_002f;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(PhotonNetwork_t8_114_il2cpp_TypeInfo_var);
		NetworkingPeer_t8_98 * L_2 = ((PhotonNetwork_t8_114_StaticFields*)PhotonNetwork_t8_114_il2cpp_TypeInfo_var->static_fields)->___networkingPeer_4;
		NullCheck(L_2);
		uint8_t L_3 = PhotonPeer_get_PeerState_m5_211(L_2, /*hidden argument*/NULL);
		uint8_t L_4 = L_3;
		Object_t * L_5 = Box(PeerStateValue_t5_35_il2cpp_TypeInfo_var, &L_4);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_6 = String_Concat_m1_416(NULL /*static, unused*/, _stringLiteral3409, L_5, /*hidden argument*/NULL);
		Debug_LogWarning_m6_491(NULL /*static, unused*/, L_6, /*hidden argument*/NULL);
		return 0;
	}

IL_002f:
	{
		IL2CPP_RUNTIME_CLASS_INIT(PhotonNetwork_t8_114_il2cpp_TypeInfo_var);
		ServerSettings_t8_115 * L_7 = ((PhotonNetwork_t8_114_StaticFields*)PhotonNetwork_t8_114_il2cpp_TypeInfo_var->static_fields)->___PhotonServerSettings_6;
		bool L_8 = Object_op_Equality_m6_579(NULL /*static, unused*/, L_7, (Object_t6_5 *)NULL, /*hidden argument*/NULL);
		if (!L_8)
		{
			goto IL_004b;
		}
	}
	{
		Debug_LogError_m6_490(NULL /*static, unused*/, _stringLiteral3410, /*hidden argument*/NULL);
		return 0;
	}

IL_004b:
	{
		IL2CPP_RUNTIME_CLASS_INIT(PhotonNetwork_t8_114_il2cpp_TypeInfo_var);
		ServerSettings_t8_115 * L_9 = ((PhotonNetwork_t8_114_StaticFields*)PhotonNetwork_t8_114_il2cpp_TypeInfo_var->static_fields)->___PhotonServerSettings_6;
		NullCheck(L_9);
		int32_t L_10 = (L_9->___HostType_2);
		if (L_10)
		{
			goto IL_0066;
		}
	}
	{
		Debug_LogError_m6_490(NULL /*static, unused*/, _stringLiteral3411, /*hidden argument*/NULL);
		return 0;
	}

IL_0066:
	{
		IL2CPP_RUNTIME_CLASS_INIT(PhotonNetwork_t8_114_il2cpp_TypeInfo_var);
		ServerSettings_t8_115 * L_11 = ((PhotonNetwork_t8_114_StaticFields*)PhotonNetwork_t8_114_il2cpp_TypeInfo_var->static_fields)->___PhotonServerSettings_6;
		NullCheck(L_11);
		uint8_t L_12 = (L_11->___Protocol_3);
		PhotonNetwork_SwitchToProtocol_m8_665(NULL /*static, unused*/, L_12, /*hidden argument*/NULL);
		NetworkingPeer_t8_98 * L_13 = ((PhotonNetwork_t8_114_StaticFields*)PhotonNetwork_t8_114_il2cpp_TypeInfo_var->static_fields)->___networkingPeer_4;
		ServerSettings_t8_115 * L_14 = ((PhotonNetwork_t8_114_StaticFields*)PhotonNetwork_t8_114_il2cpp_TypeInfo_var->static_fields)->___PhotonServerSettings_6;
		NullCheck(L_14);
		String_t* L_15 = (L_14->___AppID_6);
		String_t* L_16 = ___gameVersion;
		NullCheck(L_13);
		NetworkingPeer_SetApp_m8_492(L_13, L_15, L_16, /*hidden argument*/NULL);
		ServerSettings_t8_115 * L_17 = ((PhotonNetwork_t8_114_StaticFields*)PhotonNetwork_t8_114_il2cpp_TypeInfo_var->static_fields)->___PhotonServerSettings_6;
		NullCheck(L_17);
		int32_t L_18 = (L_17->___HostType_2);
		if ((!(((uint32_t)L_18) == ((uint32_t)3))))
		{
			goto IL_00a2;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(PhotonNetwork_t8_114_il2cpp_TypeInfo_var);
		PhotonNetwork_set_offlineMode_m8_624(NULL /*static, unused*/, 1, /*hidden argument*/NULL);
		return 1;
	}

IL_00a2:
	{
		IL2CPP_RUNTIME_CLASS_INIT(PhotonNetwork_t8_114_il2cpp_TypeInfo_var);
		bool L_19 = PhotonNetwork_get_offlineMode_m8_623(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (!L_19)
		{
			goto IL_00b6;
		}
	}
	{
		Debug_LogWarning_m6_491(NULL /*static, unused*/, _stringLiteral3412, /*hidden argument*/NULL);
	}

IL_00b6:
	{
		IL2CPP_RUNTIME_CLASS_INIT(PhotonNetwork_t8_114_il2cpp_TypeInfo_var);
		PhotonNetwork_set_offlineMode_m8_624(NULL /*static, unused*/, 0, /*hidden argument*/NULL);
		PhotonNetwork_set_isMessageQueueRunning_m8_643(NULL /*static, unused*/, 1, /*hidden argument*/NULL);
		NetworkingPeer_t8_98 * L_20 = ((PhotonNetwork_t8_114_StaticFields*)PhotonNetwork_t8_114_il2cpp_TypeInfo_var->static_fields)->___networkingPeer_4;
		NullCheck(L_20);
		L_20->___IsInitialConnect_11 = 1;
		ServerSettings_t8_115 * L_21 = ((PhotonNetwork_t8_114_StaticFields*)PhotonNetwork_t8_114_il2cpp_TypeInfo_var->static_fields)->___PhotonServerSettings_6;
		NullCheck(L_21);
		int32_t L_22 = (L_21->___HostType_2);
		if ((!(((uint32_t)L_22) == ((uint32_t)2))))
		{
			goto IL_0149;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(PhotonNetwork_t8_114_il2cpp_TypeInfo_var);
		NetworkingPeer_t8_98 * L_23 = ((PhotonNetwork_t8_114_StaticFields*)PhotonNetwork_t8_114_il2cpp_TypeInfo_var->static_fields)->___networkingPeer_4;
		NullCheck(L_23);
		NetworkingPeer_set_IsUsingNameServer_m8_382(L_23, 0, /*hidden argument*/NULL);
		NetworkingPeer_t8_98 * L_24 = ((PhotonNetwork_t8_114_StaticFields*)PhotonNetwork_t8_114_il2cpp_TypeInfo_var->static_fields)->___networkingPeer_4;
		ServerSettings_t8_115 * L_25 = ((PhotonNetwork_t8_114_StaticFields*)PhotonNetwork_t8_114_il2cpp_TypeInfo_var->static_fields)->___PhotonServerSettings_6;
		NullCheck(L_25);
		int32_t L_26 = (L_25->___ServerPort_5);
		G_B12_0 = L_24;
		if (L_26)
		{
			G_B13_0 = L_24;
			goto IL_010b;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(PhotonNetwork_t8_114_il2cpp_TypeInfo_var);
		ServerSettings_t8_115 * L_27 = ((PhotonNetwork_t8_114_StaticFields*)PhotonNetwork_t8_114_il2cpp_TypeInfo_var->static_fields)->___PhotonServerSettings_6;
		NullCheck(L_27);
		String_t* L_28 = (L_27->___ServerAddress_4);
		G_B14_0 = L_28;
		G_B14_1 = G_B12_0;
		goto IL_012e;
	}

IL_010b:
	{
		IL2CPP_RUNTIME_CLASS_INIT(PhotonNetwork_t8_114_il2cpp_TypeInfo_var);
		ServerSettings_t8_115 * L_29 = ((PhotonNetwork_t8_114_StaticFields*)PhotonNetwork_t8_114_il2cpp_TypeInfo_var->static_fields)->___PhotonServerSettings_6;
		NullCheck(L_29);
		String_t* L_30 = (L_29->___ServerAddress_4);
		ServerSettings_t8_115 * L_31 = ((PhotonNetwork_t8_114_StaticFields*)PhotonNetwork_t8_114_il2cpp_TypeInfo_var->static_fields)->___PhotonServerSettings_6;
		NullCheck(L_31);
		int32_t L_32 = (L_31->___ServerPort_5);
		int32_t L_33 = L_32;
		Object_t * L_34 = Box(Int32_t1_3_il2cpp_TypeInfo_var, &L_33);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_35 = String_Concat_m1_417(NULL /*static, unused*/, L_30, _stringLiteral633, L_34, /*hidden argument*/NULL);
		G_B14_0 = L_35;
		G_B14_1 = G_B13_0;
	}

IL_012e:
	{
		NullCheck(G_B14_1);
		NetworkingPeer_set_MasterServerAddress_m8_374(G_B14_1, G_B14_0, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(PhotonNetwork_t8_114_il2cpp_TypeInfo_var);
		NetworkingPeer_t8_98 * L_36 = ((PhotonNetwork_t8_114_StaticFields*)PhotonNetwork_t8_114_il2cpp_TypeInfo_var->static_fields)->___networkingPeer_4;
		NetworkingPeer_t8_98 * L_37 = ((PhotonNetwork_t8_114_StaticFields*)PhotonNetwork_t8_114_il2cpp_TypeInfo_var->static_fields)->___networkingPeer_4;
		NullCheck(L_37);
		String_t* L_38 = NetworkingPeer_get_MasterServerAddress_m8_373(L_37, /*hidden argument*/NULL);
		NullCheck(L_36);
		bool L_39 = NetworkingPeer_Connect_m8_408(L_36, L_38, 0, /*hidden argument*/NULL);
		return L_39;
	}

IL_0149:
	{
		IL2CPP_RUNTIME_CLASS_INIT(PhotonNetwork_t8_114_il2cpp_TypeInfo_var);
		ServerSettings_t8_115 * L_40 = ((PhotonNetwork_t8_114_StaticFields*)PhotonNetwork_t8_114_il2cpp_TypeInfo_var->static_fields)->___PhotonServerSettings_6;
		NullCheck(L_40);
		int32_t L_41 = (L_40->___HostType_2);
		if ((!(((uint32_t)L_41) == ((uint32_t)4))))
		{
			goto IL_0160;
		}
	}
	{
		String_t* L_42 = ___gameVersion;
		IL2CPP_RUNTIME_CLASS_INIT(PhotonNetwork_t8_114_il2cpp_TypeInfo_var);
		bool L_43 = PhotonNetwork_ConnectToBestCloudServer_m8_668(NULL /*static, unused*/, L_42, /*hidden argument*/NULL);
		return L_43;
	}

IL_0160:
	{
		IL2CPP_RUNTIME_CLASS_INIT(PhotonNetwork_t8_114_il2cpp_TypeInfo_var);
		NetworkingPeer_t8_98 * L_44 = ((PhotonNetwork_t8_114_StaticFields*)PhotonNetwork_t8_114_il2cpp_TypeInfo_var->static_fields)->___networkingPeer_4;
		ServerSettings_t8_115 * L_45 = ((PhotonNetwork_t8_114_StaticFields*)PhotonNetwork_t8_114_il2cpp_TypeInfo_var->static_fields)->___PhotonServerSettings_6;
		NullCheck(L_45);
		int32_t L_46 = (L_45->___PreferredRegion_7);
		NullCheck(L_44);
		bool L_47 = NetworkingPeer_ConnectToRegionMaster_m8_410(L_44, L_46, /*hidden argument*/NULL);
		return L_47;
	}
}
// System.Boolean PhotonNetwork::ConnectToMaster(System.String,System.Int32,System.String,System.String)
extern TypeInfo* PhotonNetwork_t8_114_il2cpp_TypeInfo_var;
extern TypeInfo* PeerStateValue_t5_35_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* Int32_t1_3_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3413;
extern Il2CppCodeGenString* _stringLiteral3414;
extern Il2CppCodeGenString* _stringLiteral3415;
extern Il2CppCodeGenString* _stringLiteral633;
extern "C" bool PhotonNetwork_ConnectToMaster_m8_667 (Object_t * __this /* static, unused */, String_t* ___masterServerAddress, int32_t ___port, String_t* ___appID, String_t* ___gameVersion, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		PhotonNetwork_t8_114_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1058);
		PeerStateValue_t5_35_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1156);
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(11);
		Int32_t1_3_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(8);
		_stringLiteral3413 = il2cpp_codegen_string_literal_from_index(3413);
		_stringLiteral3414 = il2cpp_codegen_string_literal_from_index(3414);
		_stringLiteral3415 = il2cpp_codegen_string_literal_from_index(3415);
		_stringLiteral633 = il2cpp_codegen_string_literal_from_index(633);
		s_Il2CppMethodIntialized = true;
	}
	NetworkingPeer_t8_98 * G_B8_0 = {0};
	NetworkingPeer_t8_98 * G_B7_0 = {0};
	String_t* G_B9_0 = {0};
	NetworkingPeer_t8_98 * G_B9_1 = {0};
	{
		IL2CPP_RUNTIME_CLASS_INIT(PhotonNetwork_t8_114_il2cpp_TypeInfo_var);
		NetworkingPeer_t8_98 * L_0 = ((PhotonNetwork_t8_114_StaticFields*)PhotonNetwork_t8_114_il2cpp_TypeInfo_var->static_fields)->___networkingPeer_4;
		NullCheck(L_0);
		uint8_t L_1 = PhotonPeer_get_PeerState_m5_211(L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_002f;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(PhotonNetwork_t8_114_il2cpp_TypeInfo_var);
		NetworkingPeer_t8_98 * L_2 = ((PhotonNetwork_t8_114_StaticFields*)PhotonNetwork_t8_114_il2cpp_TypeInfo_var->static_fields)->___networkingPeer_4;
		NullCheck(L_2);
		uint8_t L_3 = PhotonPeer_get_PeerState_m5_211(L_2, /*hidden argument*/NULL);
		uint8_t L_4 = L_3;
		Object_t * L_5 = Box(PeerStateValue_t5_35_il2cpp_TypeInfo_var, &L_4);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_6 = String_Concat_m1_416(NULL /*static, unused*/, _stringLiteral3413, L_5, /*hidden argument*/NULL);
		Debug_LogWarning_m6_491(NULL /*static, unused*/, L_6, /*hidden argument*/NULL);
		return 0;
	}

IL_002f:
	{
		IL2CPP_RUNTIME_CLASS_INIT(PhotonNetwork_t8_114_il2cpp_TypeInfo_var);
		bool L_7 = PhotonNetwork_get_offlineMode_m8_623(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (!L_7)
		{
			goto IL_0049;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(PhotonNetwork_t8_114_il2cpp_TypeInfo_var);
		PhotonNetwork_set_offlineMode_m8_624(NULL /*static, unused*/, 0, /*hidden argument*/NULL);
		Debug_LogWarning_m6_491(NULL /*static, unused*/, _stringLiteral3414, /*hidden argument*/NULL);
	}

IL_0049:
	{
		IL2CPP_RUNTIME_CLASS_INIT(PhotonNetwork_t8_114_il2cpp_TypeInfo_var);
		bool L_8 = PhotonNetwork_get_isMessageQueueRunning_m8_642(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (L_8)
		{
			goto IL_0063;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(PhotonNetwork_t8_114_il2cpp_TypeInfo_var);
		PhotonNetwork_set_isMessageQueueRunning_m8_643(NULL /*static, unused*/, 1, /*hidden argument*/NULL);
		Debug_LogWarning_m6_491(NULL /*static, unused*/, _stringLiteral3415, /*hidden argument*/NULL);
	}

IL_0063:
	{
		IL2CPP_RUNTIME_CLASS_INIT(PhotonNetwork_t8_114_il2cpp_TypeInfo_var);
		NetworkingPeer_t8_98 * L_9 = ((PhotonNetwork_t8_114_StaticFields*)PhotonNetwork_t8_114_il2cpp_TypeInfo_var->static_fields)->___networkingPeer_4;
		String_t* L_10 = ___appID;
		String_t* L_11 = ___gameVersion;
		NullCheck(L_9);
		NetworkingPeer_SetApp_m8_492(L_9, L_10, L_11, /*hidden argument*/NULL);
		NetworkingPeer_t8_98 * L_12 = ((PhotonNetwork_t8_114_StaticFields*)PhotonNetwork_t8_114_il2cpp_TypeInfo_var->static_fields)->___networkingPeer_4;
		NullCheck(L_12);
		NetworkingPeer_set_IsUsingNameServer_m8_382(L_12, 0, /*hidden argument*/NULL);
		NetworkingPeer_t8_98 * L_13 = ((PhotonNetwork_t8_114_StaticFields*)PhotonNetwork_t8_114_il2cpp_TypeInfo_var->static_fields)->___networkingPeer_4;
		NullCheck(L_13);
		L_13->___IsInitialConnect_11 = 1;
		NetworkingPeer_t8_98 * L_14 = ((PhotonNetwork_t8_114_StaticFields*)PhotonNetwork_t8_114_il2cpp_TypeInfo_var->static_fields)->___networkingPeer_4;
		int32_t L_15 = ___port;
		G_B7_0 = L_14;
		if (L_15)
		{
			G_B8_0 = L_14;
			goto IL_0096;
		}
	}
	{
		String_t* L_16 = ___masterServerAddress;
		G_B9_0 = L_16;
		G_B9_1 = G_B7_0;
		goto IL_00a7;
	}

IL_0096:
	{
		String_t* L_17 = ___masterServerAddress;
		int32_t L_18 = ___port;
		int32_t L_19 = L_18;
		Object_t * L_20 = Box(Int32_t1_3_il2cpp_TypeInfo_var, &L_19);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_21 = String_Concat_m1_417(NULL /*static, unused*/, L_17, _stringLiteral633, L_20, /*hidden argument*/NULL);
		G_B9_0 = L_21;
		G_B9_1 = G_B8_0;
	}

IL_00a7:
	{
		NullCheck(G_B9_1);
		NetworkingPeer_set_MasterServerAddress_m8_374(G_B9_1, G_B9_0, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(PhotonNetwork_t8_114_il2cpp_TypeInfo_var);
		NetworkingPeer_t8_98 * L_22 = ((PhotonNetwork_t8_114_StaticFields*)PhotonNetwork_t8_114_il2cpp_TypeInfo_var->static_fields)->___networkingPeer_4;
		NetworkingPeer_t8_98 * L_23 = ((PhotonNetwork_t8_114_StaticFields*)PhotonNetwork_t8_114_il2cpp_TypeInfo_var->static_fields)->___networkingPeer_4;
		NullCheck(L_23);
		String_t* L_24 = NetworkingPeer_get_MasterServerAddress_m8_373(L_23, /*hidden argument*/NULL);
		NullCheck(L_22);
		bool L_25 = NetworkingPeer_Connect_m8_408(L_22, L_24, 0, /*hidden argument*/NULL);
		return L_25;
	}
}
// System.Boolean PhotonNetwork::ConnectToBestCloudServer(System.String)
extern TypeInfo* PhotonNetwork_t8_114_il2cpp_TypeInfo_var;
extern TypeInfo* PeerStateValue_t5_35_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* PhotonHandler_t8_111_il2cpp_TypeInfo_var;
extern TypeInfo* CloudRegionCode_t8_65_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3416;
extern Il2CppCodeGenString* _stringLiteral3410;
extern Il2CppCodeGenString* _stringLiteral3417;
extern "C" bool PhotonNetwork_ConnectToBestCloudServer_m8_668 (Object_t * __this /* static, unused */, String_t* ___gameVersion, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		PhotonNetwork_t8_114_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1058);
		PeerStateValue_t5_35_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1156);
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(11);
		PhotonHandler_t8_111_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1120);
		CloudRegionCode_t8_65_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1123);
		_stringLiteral3416 = il2cpp_codegen_string_literal_from_index(3416);
		_stringLiteral3410 = il2cpp_codegen_string_literal_from_index(3410);
		_stringLiteral3417 = il2cpp_codegen_string_literal_from_index(3417);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = {0};
	bool V_1 = false;
	{
		IL2CPP_RUNTIME_CLASS_INIT(PhotonNetwork_t8_114_il2cpp_TypeInfo_var);
		NetworkingPeer_t8_98 * L_0 = ((PhotonNetwork_t8_114_StaticFields*)PhotonNetwork_t8_114_il2cpp_TypeInfo_var->static_fields)->___networkingPeer_4;
		NullCheck(L_0);
		uint8_t L_1 = PhotonPeer_get_PeerState_m5_211(L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_002f;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(PhotonNetwork_t8_114_il2cpp_TypeInfo_var);
		NetworkingPeer_t8_98 * L_2 = ((PhotonNetwork_t8_114_StaticFields*)PhotonNetwork_t8_114_il2cpp_TypeInfo_var->static_fields)->___networkingPeer_4;
		NullCheck(L_2);
		uint8_t L_3 = PhotonPeer_get_PeerState_m5_211(L_2, /*hidden argument*/NULL);
		uint8_t L_4 = L_3;
		Object_t * L_5 = Box(PeerStateValue_t5_35_il2cpp_TypeInfo_var, &L_4);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_6 = String_Concat_m1_416(NULL /*static, unused*/, _stringLiteral3416, L_5, /*hidden argument*/NULL);
		Debug_LogWarning_m6_491(NULL /*static, unused*/, L_6, /*hidden argument*/NULL);
		return 0;
	}

IL_002f:
	{
		IL2CPP_RUNTIME_CLASS_INIT(PhotonNetwork_t8_114_il2cpp_TypeInfo_var);
		ServerSettings_t8_115 * L_7 = ((PhotonNetwork_t8_114_StaticFields*)PhotonNetwork_t8_114_il2cpp_TypeInfo_var->static_fields)->___PhotonServerSettings_6;
		bool L_8 = Object_op_Equality_m6_579(NULL /*static, unused*/, L_7, (Object_t6_5 *)NULL, /*hidden argument*/NULL);
		if (!L_8)
		{
			goto IL_004b;
		}
	}
	{
		Debug_LogError_m6_490(NULL /*static, unused*/, _stringLiteral3410, /*hidden argument*/NULL);
		return 0;
	}

IL_004b:
	{
		IL2CPP_RUNTIME_CLASS_INIT(PhotonNetwork_t8_114_il2cpp_TypeInfo_var);
		ServerSettings_t8_115 * L_9 = ((PhotonNetwork_t8_114_StaticFields*)PhotonNetwork_t8_114_il2cpp_TypeInfo_var->static_fields)->___PhotonServerSettings_6;
		NullCheck(L_9);
		int32_t L_10 = (L_9->___HostType_2);
		if ((!(((uint32_t)L_10) == ((uint32_t)3))))
		{
			goto IL_0062;
		}
	}
	{
		String_t* L_11 = ___gameVersion;
		IL2CPP_RUNTIME_CLASS_INIT(PhotonNetwork_t8_114_il2cpp_TypeInfo_var);
		bool L_12 = PhotonNetwork_ConnectUsingSettings_m8_666(NULL /*static, unused*/, L_11, /*hidden argument*/NULL);
		return L_12;
	}

IL_0062:
	{
		IL2CPP_RUNTIME_CLASS_INIT(PhotonNetwork_t8_114_il2cpp_TypeInfo_var);
		NetworkingPeer_t8_98 * L_13 = ((PhotonNetwork_t8_114_StaticFields*)PhotonNetwork_t8_114_il2cpp_TypeInfo_var->static_fields)->___networkingPeer_4;
		NullCheck(L_13);
		L_13->___IsInitialConnect_11 = 1;
		NetworkingPeer_t8_98 * L_14 = ((PhotonNetwork_t8_114_StaticFields*)PhotonNetwork_t8_114_il2cpp_TypeInfo_var->static_fields)->___networkingPeer_4;
		ServerSettings_t8_115 * L_15 = ((PhotonNetwork_t8_114_StaticFields*)PhotonNetwork_t8_114_il2cpp_TypeInfo_var->static_fields)->___PhotonServerSettings_6;
		NullCheck(L_15);
		String_t* L_16 = (L_15->___AppID_6);
		String_t* L_17 = ___gameVersion;
		NullCheck(L_14);
		NetworkingPeer_SetApp_m8_492(L_14, L_16, L_17, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(PhotonHandler_t8_111_il2cpp_TypeInfo_var);
		int32_t L_18 = PhotonHandler_get_BestRegionCodeInPreferences_m8_584(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_0 = L_18;
		int32_t L_19 = V_0;
		if ((((int32_t)L_19) == ((int32_t)4)))
		{
			goto IL_00b0;
		}
	}
	{
		int32_t L_20 = V_0;
		int32_t L_21 = L_20;
		Object_t * L_22 = Box(CloudRegionCode_t8_65_il2cpp_TypeInfo_var, &L_21);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_23 = String_Concat_m1_416(NULL /*static, unused*/, _stringLiteral3417, L_22, /*hidden argument*/NULL);
		Debug_Log_m6_489(NULL /*static, unused*/, L_23, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(PhotonNetwork_t8_114_il2cpp_TypeInfo_var);
		NetworkingPeer_t8_98 * L_24 = ((PhotonNetwork_t8_114_StaticFields*)PhotonNetwork_t8_114_il2cpp_TypeInfo_var->static_fields)->___networkingPeer_4;
		int32_t L_25 = V_0;
		NullCheck(L_24);
		bool L_26 = NetworkingPeer_ConnectToRegionMaster_m8_410(L_24, L_25, /*hidden argument*/NULL);
		return L_26;
	}

IL_00b0:
	{
		IL2CPP_RUNTIME_CLASS_INIT(PhotonNetwork_t8_114_il2cpp_TypeInfo_var);
		NetworkingPeer_t8_98 * L_27 = ((PhotonNetwork_t8_114_StaticFields*)PhotonNetwork_t8_114_il2cpp_TypeInfo_var->static_fields)->___networkingPeer_4;
		NullCheck(L_27);
		bool L_28 = NetworkingPeer_ConnectToNameServer_m8_409(L_27, /*hidden argument*/NULL);
		V_1 = L_28;
		bool L_29 = V_1;
		return L_29;
	}
}
// System.Boolean PhotonNetwork::ConnectToRegion(CloudRegionCode,System.String)
extern TypeInfo* PhotonNetwork_t8_114_il2cpp_TypeInfo_var;
extern TypeInfo* PeerStateValue_t5_35_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* CloudRegionCode_t8_65_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3418;
extern Il2CppCodeGenString* _stringLiteral3419;
extern Il2CppCodeGenString* _stringLiteral3420;
extern "C" bool PhotonNetwork_ConnectToRegion_m8_669 (Object_t * __this /* static, unused */, int32_t ___region, String_t* ___gameVersion, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		PhotonNetwork_t8_114_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1058);
		PeerStateValue_t5_35_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1156);
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(11);
		CloudRegionCode_t8_65_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1123);
		_stringLiteral3418 = il2cpp_codegen_string_literal_from_index(3418);
		_stringLiteral3419 = il2cpp_codegen_string_literal_from_index(3419);
		_stringLiteral3420 = il2cpp_codegen_string_literal_from_index(3420);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(PhotonNetwork_t8_114_il2cpp_TypeInfo_var);
		NetworkingPeer_t8_98 * L_0 = ((PhotonNetwork_t8_114_StaticFields*)PhotonNetwork_t8_114_il2cpp_TypeInfo_var->static_fields)->___networkingPeer_4;
		NullCheck(L_0);
		uint8_t L_1 = PhotonPeer_get_PeerState_m5_211(L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_002f;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(PhotonNetwork_t8_114_il2cpp_TypeInfo_var);
		NetworkingPeer_t8_98 * L_2 = ((PhotonNetwork_t8_114_StaticFields*)PhotonNetwork_t8_114_il2cpp_TypeInfo_var->static_fields)->___networkingPeer_4;
		NullCheck(L_2);
		uint8_t L_3 = PhotonPeer_get_PeerState_m5_211(L_2, /*hidden argument*/NULL);
		uint8_t L_4 = L_3;
		Object_t * L_5 = Box(PeerStateValue_t5_35_il2cpp_TypeInfo_var, &L_4);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_6 = String_Concat_m1_416(NULL /*static, unused*/, _stringLiteral3418, L_5, /*hidden argument*/NULL);
		Debug_LogWarning_m6_491(NULL /*static, unused*/, L_6, /*hidden argument*/NULL);
		return 0;
	}

IL_002f:
	{
		IL2CPP_RUNTIME_CLASS_INIT(PhotonNetwork_t8_114_il2cpp_TypeInfo_var);
		ServerSettings_t8_115 * L_7 = ((PhotonNetwork_t8_114_StaticFields*)PhotonNetwork_t8_114_il2cpp_TypeInfo_var->static_fields)->___PhotonServerSettings_6;
		bool L_8 = Object_op_Equality_m6_579(NULL /*static, unused*/, L_7, (Object_t6_5 *)NULL, /*hidden argument*/NULL);
		if (!L_8)
		{
			goto IL_004b;
		}
	}
	{
		Debug_LogError_m6_490(NULL /*static, unused*/, _stringLiteral3419, /*hidden argument*/NULL);
		return 0;
	}

IL_004b:
	{
		IL2CPP_RUNTIME_CLASS_INIT(PhotonNetwork_t8_114_il2cpp_TypeInfo_var);
		ServerSettings_t8_115 * L_9 = ((PhotonNetwork_t8_114_StaticFields*)PhotonNetwork_t8_114_il2cpp_TypeInfo_var->static_fields)->___PhotonServerSettings_6;
		NullCheck(L_9);
		int32_t L_10 = (L_9->___HostType_2);
		if ((!(((uint32_t)L_10) == ((uint32_t)3))))
		{
			goto IL_0062;
		}
	}
	{
		String_t* L_11 = ___gameVersion;
		IL2CPP_RUNTIME_CLASS_INIT(PhotonNetwork_t8_114_il2cpp_TypeInfo_var);
		bool L_12 = PhotonNetwork_ConnectUsingSettings_m8_666(NULL /*static, unused*/, L_11, /*hidden argument*/NULL);
		return L_12;
	}

IL_0062:
	{
		IL2CPP_RUNTIME_CLASS_INIT(PhotonNetwork_t8_114_il2cpp_TypeInfo_var);
		NetworkingPeer_t8_98 * L_13 = ((PhotonNetwork_t8_114_StaticFields*)PhotonNetwork_t8_114_il2cpp_TypeInfo_var->static_fields)->___networkingPeer_4;
		NullCheck(L_13);
		L_13->___IsInitialConnect_11 = 1;
		NetworkingPeer_t8_98 * L_14 = ((PhotonNetwork_t8_114_StaticFields*)PhotonNetwork_t8_114_il2cpp_TypeInfo_var->static_fields)->___networkingPeer_4;
		ServerSettings_t8_115 * L_15 = ((PhotonNetwork_t8_114_StaticFields*)PhotonNetwork_t8_114_il2cpp_TypeInfo_var->static_fields)->___PhotonServerSettings_6;
		NullCheck(L_15);
		String_t* L_16 = (L_15->___AppID_6);
		String_t* L_17 = ___gameVersion;
		NullCheck(L_14);
		NetworkingPeer_SetApp_m8_492(L_14, L_16, L_17, /*hidden argument*/NULL);
		int32_t L_18 = ___region;
		if ((((int32_t)L_18) == ((int32_t)4)))
		{
			goto IL_00aa;
		}
	}
	{
		int32_t L_19 = ___region;
		int32_t L_20 = L_19;
		Object_t * L_21 = Box(CloudRegionCode_t8_65_il2cpp_TypeInfo_var, &L_20);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_22 = String_Concat_m1_416(NULL /*static, unused*/, _stringLiteral3420, L_21, /*hidden argument*/NULL);
		Debug_Log_m6_489(NULL /*static, unused*/, L_22, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(PhotonNetwork_t8_114_il2cpp_TypeInfo_var);
		NetworkingPeer_t8_98 * L_23 = ((PhotonNetwork_t8_114_StaticFields*)PhotonNetwork_t8_114_il2cpp_TypeInfo_var->static_fields)->___networkingPeer_4;
		int32_t L_24 = ___region;
		NullCheck(L_23);
		bool L_25 = NetworkingPeer_ConnectToRegionMaster_m8_410(L_23, L_24, /*hidden argument*/NULL);
		return L_25;
	}

IL_00aa:
	{
		return 0;
	}
}
// System.Void PhotonNetwork::OverrideBestCloudServer(CloudRegionCode)
extern TypeInfo* PhotonHandler_t8_111_il2cpp_TypeInfo_var;
extern "C" void PhotonNetwork_OverrideBestCloudServer_m8_670 (Object_t * __this /* static, unused */, int32_t ___region, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		PhotonHandler_t8_111_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1120);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = ___region;
		IL2CPP_RUNTIME_CLASS_INIT(PhotonHandler_t8_111_il2cpp_TypeInfo_var);
		PhotonHandler_set_BestRegionCodeInPreferences_m8_585(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void PhotonNetwork::RefreshCloudServerRating()
extern TypeInfo* NotImplementedException_t1_748_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3421;
extern "C" void PhotonNetwork_RefreshCloudServerRating_m8_671 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NotImplementedException_t1_748_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(113);
		_stringLiteral3421 = il2cpp_codegen_string_literal_from_index(3421);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotImplementedException_t1_748 * L_0 = (NotImplementedException_t1_748 *)il2cpp_codegen_object_new (NotImplementedException_t1_748_il2cpp_TypeInfo_var);
		NotImplementedException__ctor_m1_5203(L_0, _stringLiteral3421, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_0);
	}
}
// System.Void PhotonNetwork::NetworkStatisticsReset()
extern TypeInfo* PhotonNetwork_t8_114_il2cpp_TypeInfo_var;
extern "C" void PhotonNetwork_NetworkStatisticsReset_m8_672 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		PhotonNetwork_t8_114_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1058);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(PhotonNetwork_t8_114_il2cpp_TypeInfo_var);
		NetworkingPeer_t8_98 * L_0 = ((PhotonNetwork_t8_114_StaticFields*)PhotonNetwork_t8_114_il2cpp_TypeInfo_var->static_fields)->___networkingPeer_4;
		NullCheck(L_0);
		PhotonPeer_TrafficStatsReset_m5_205(L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.String PhotonNetwork::NetworkStatisticsToString()
extern TypeInfo* PhotonNetwork_t8_114_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3422;
extern "C" String_t* PhotonNetwork_NetworkStatisticsToString_m8_673 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		PhotonNetwork_t8_114_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1058);
		_stringLiteral3422 = il2cpp_codegen_string_literal_from_index(3422);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(PhotonNetwork_t8_114_il2cpp_TypeInfo_var);
		NetworkingPeer_t8_98 * L_0 = ((PhotonNetwork_t8_114_StaticFields*)PhotonNetwork_t8_114_il2cpp_TypeInfo_var->static_fields)->___networkingPeer_4;
		if (!L_0)
		{
			goto IL_0014;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(PhotonNetwork_t8_114_il2cpp_TypeInfo_var);
		bool L_1 = PhotonNetwork_get_offlineMode_m8_623(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_001a;
		}
	}

IL_0014:
	{
		return _stringLiteral3422;
	}

IL_001a:
	{
		IL2CPP_RUNTIME_CLASS_INIT(PhotonNetwork_t8_114_il2cpp_TypeInfo_var);
		NetworkingPeer_t8_98 * L_2 = ((PhotonNetwork_t8_114_StaticFields*)PhotonNetwork_t8_114_il2cpp_TypeInfo_var->static_fields)->___networkingPeer_4;
		NullCheck(L_2);
		String_t* L_3 = PhotonPeer_VitalStatsToString_m5_245(L_2, 0, /*hidden argument*/NULL);
		return L_3;
	}
}
// System.Void PhotonNetwork::InitializeSecurity()
extern "C" void PhotonNetwork_InitializeSecurity_m8_674 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Boolean PhotonNetwork::VerifyCanUseNetwork()
extern TypeInfo* PhotonNetwork_t8_114_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3423;
extern "C" bool PhotonNetwork_VerifyCanUseNetwork_m8_675 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		PhotonNetwork_t8_114_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1058);
		_stringLiteral3423 = il2cpp_codegen_string_literal_from_index(3423);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(PhotonNetwork_t8_114_il2cpp_TypeInfo_var);
		bool L_0 = PhotonNetwork_get_connected_m8_603(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_000c;
		}
	}
	{
		return 1;
	}

IL_000c:
	{
		Debug_LogError_m6_490(NULL /*static, unused*/, _stringLiteral3423, /*hidden argument*/NULL);
		return 0;
	}
}
// System.Void PhotonNetwork::Disconnect()
extern TypeInfo* PhotonNetwork_t8_114_il2cpp_TypeInfo_var;
extern "C" void PhotonNetwork_Disconnect_m8_676 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		PhotonNetwork_t8_114_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1058);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(PhotonNetwork_t8_114_il2cpp_TypeInfo_var);
		bool L_0 = PhotonNetwork_get_offlineMode_m8_623(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_0032;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(PhotonNetwork_t8_114_il2cpp_TypeInfo_var);
		PhotonNetwork_set_offlineMode_m8_624(NULL /*static, unused*/, 0, /*hidden argument*/NULL);
		((PhotonNetwork_t8_114_StaticFields*)PhotonNetwork_t8_114_il2cpp_TypeInfo_var->static_fields)->___offlineModeRoom_18 = (Room_t8_100 *)NULL;
		NetworkingPeer_t8_98 * L_1 = ((PhotonNetwork_t8_114_StaticFields*)PhotonNetwork_t8_114_il2cpp_TypeInfo_var->static_fields)->___networkingPeer_4;
		NullCheck(L_1);
		NetworkingPeer_set_State_m8_380(L_1, ((int32_t)14), /*hidden argument*/NULL);
		NetworkingPeer_t8_98 * L_2 = ((PhotonNetwork_t8_114_StaticFields*)PhotonNetwork_t8_114_il2cpp_TypeInfo_var->static_fields)->___networkingPeer_4;
		NullCheck(L_2);
		VirtActionInvoker1< int32_t >::Invoke(30 /* System.Void NetworkingPeer::OnStatusChanged(ExitGames.Client.Photon.StatusCode) */, L_2, ((int32_t)1025));
		return;
	}

IL_0032:
	{
		IL2CPP_RUNTIME_CLASS_INIT(PhotonNetwork_t8_114_il2cpp_TypeInfo_var);
		NetworkingPeer_t8_98 * L_3 = ((PhotonNetwork_t8_114_StaticFields*)PhotonNetwork_t8_114_il2cpp_TypeInfo_var->static_fields)->___networkingPeer_4;
		if (L_3)
		{
			goto IL_003d;
		}
	}
	{
		return;
	}

IL_003d:
	{
		IL2CPP_RUNTIME_CLASS_INIT(PhotonNetwork_t8_114_il2cpp_TypeInfo_var);
		NetworkingPeer_t8_98 * L_4 = ((PhotonNetwork_t8_114_StaticFields*)PhotonNetwork_t8_114_il2cpp_TypeInfo_var->static_fields)->___networkingPeer_4;
		NullCheck(L_4);
		VirtActionInvoker0::Invoke(7 /* System.Void NetworkingPeer::Disconnect() */, L_4);
		return;
	}
}
// System.Boolean PhotonNetwork::FindFriends(System.String[])
extern TypeInfo* PhotonNetwork_t8_114_il2cpp_TypeInfo_var;
extern "C" bool PhotonNetwork_FindFriends_m8_677 (Object_t * __this /* static, unused */, StringU5BU5D_t1_202* ___friendsToFind, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		PhotonNetwork_t8_114_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1058);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(PhotonNetwork_t8_114_il2cpp_TypeInfo_var);
		NetworkingPeer_t8_98 * L_0 = ((PhotonNetwork_t8_114_StaticFields*)PhotonNetwork_t8_114_il2cpp_TypeInfo_var->static_fields)->___networkingPeer_4;
		if (!L_0)
		{
			goto IL_0014;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(PhotonNetwork_t8_114_il2cpp_TypeInfo_var);
		bool L_1 = ((PhotonNetwork_t8_114_StaticFields*)PhotonNetwork_t8_114_il2cpp_TypeInfo_var->static_fields)->___isOfflineMode_17;
		if (!L_1)
		{
			goto IL_0016;
		}
	}

IL_0014:
	{
		return 0;
	}

IL_0016:
	{
		IL2CPP_RUNTIME_CLASS_INIT(PhotonNetwork_t8_114_il2cpp_TypeInfo_var);
		NetworkingPeer_t8_98 * L_2 = ((PhotonNetwork_t8_114_StaticFields*)PhotonNetwork_t8_114_il2cpp_TypeInfo_var->static_fields)->___networkingPeer_4;
		StringU5BU5D_t1_202* L_3 = ___friendsToFind;
		NullCheck(L_2);
		bool L_4 = (bool)VirtFuncInvoker1< bool, StringU5BU5D_t1_202* >::Invoke(24 /* System.Boolean NetworkingPeer::OpFindFriends(System.String[]) */, L_2, L_3);
		return L_4;
	}
}
// System.Boolean PhotonNetwork::CreateRoom(System.String)
extern TypeInfo* PhotonNetwork_t8_114_il2cpp_TypeInfo_var;
extern "C" bool PhotonNetwork_CreateRoom_m8_678 (Object_t * __this /* static, unused */, String_t* ___roomName, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		PhotonNetwork_t8_114_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1058);
		s_Il2CppMethodIntialized = true;
	}
	{
		String_t* L_0 = ___roomName;
		IL2CPP_RUNTIME_CLASS_INIT(PhotonNetwork_t8_114_il2cpp_TypeInfo_var);
		bool L_1 = PhotonNetwork_CreateRoom_m8_680(NULL /*static, unused*/, L_0, (RoomOptions_t8_78 *)NULL, (TypedLobby_t8_79 *)NULL, (StringU5BU5D_t1_202*)(StringU5BU5D_t1_202*)NULL, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Boolean PhotonNetwork::CreateRoom(System.String,RoomOptions,TypedLobby)
extern TypeInfo* PhotonNetwork_t8_114_il2cpp_TypeInfo_var;
extern "C" bool PhotonNetwork_CreateRoom_m8_679 (Object_t * __this /* static, unused */, String_t* ___roomName, RoomOptions_t8_78 * ___roomOptions, TypedLobby_t8_79 * ___typedLobby, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		PhotonNetwork_t8_114_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1058);
		s_Il2CppMethodIntialized = true;
	}
	{
		String_t* L_0 = ___roomName;
		RoomOptions_t8_78 * L_1 = ___roomOptions;
		TypedLobby_t8_79 * L_2 = ___typedLobby;
		IL2CPP_RUNTIME_CLASS_INIT(PhotonNetwork_t8_114_il2cpp_TypeInfo_var);
		bool L_3 = PhotonNetwork_CreateRoom_m8_680(NULL /*static, unused*/, L_0, L_1, L_2, (StringU5BU5D_t1_202*)(StringU5BU5D_t1_202*)NULL, /*hidden argument*/NULL);
		return L_3;
	}
}
// System.Boolean PhotonNetwork::CreateRoom(System.String,RoomOptions,TypedLobby,System.String[])
extern TypeInfo* PhotonNetwork_t8_114_il2cpp_TypeInfo_var;
extern TypeInfo* EnterRoomParams_t8_77_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3424;
extern Il2CppCodeGenString* _stringLiteral3425;
extern "C" bool PhotonNetwork_CreateRoom_m8_680 (Object_t * __this /* static, unused */, String_t* ___roomName, RoomOptions_t8_78 * ___roomOptions, TypedLobby_t8_79 * ___typedLobby, StringU5BU5D_t1_202* ___expectedUsers, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		PhotonNetwork_t8_114_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1058);
		EnterRoomParams_t8_77_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1131);
		_stringLiteral3424 = il2cpp_codegen_string_literal_from_index(3424);
		_stringLiteral3425 = il2cpp_codegen_string_literal_from_index(3425);
		s_Il2CppMethodIntialized = true;
	}
	EnterRoomParams_t8_77 * V_0 = {0};
	TypedLobby_t8_79 * G_B11_0 = {0};
	TypedLobby_t8_79 * G_B8_0 = {0};
	{
		IL2CPP_RUNTIME_CLASS_INIT(PhotonNetwork_t8_114_il2cpp_TypeInfo_var);
		bool L_0 = PhotonNetwork_get_offlineMode_m8_623(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_002a;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(PhotonNetwork_t8_114_il2cpp_TypeInfo_var);
		Room_t8_100 * L_1 = ((PhotonNetwork_t8_114_StaticFields*)PhotonNetwork_t8_114_il2cpp_TypeInfo_var->static_fields)->___offlineModeRoom_18;
		if (!L_1)
		{
			goto IL_0020;
		}
	}
	{
		Debug_LogError_m6_490(NULL /*static, unused*/, _stringLiteral3424, /*hidden argument*/NULL);
		return 0;
	}

IL_0020:
	{
		String_t* L_2 = ___roomName;
		RoomOptions_t8_78 * L_3 = ___roomOptions;
		IL2CPP_RUNTIME_CLASS_INIT(PhotonNetwork_t8_114_il2cpp_TypeInfo_var);
		PhotonNetwork_EnterOfflineRoom_m8_686(NULL /*static, unused*/, L_2, L_3, 1, /*hidden argument*/NULL);
		return 1;
	}

IL_002a:
	{
		IL2CPP_RUNTIME_CLASS_INIT(PhotonNetwork_t8_114_il2cpp_TypeInfo_var);
		NetworkingPeer_t8_98 * L_4 = ((PhotonNetwork_t8_114_StaticFields*)PhotonNetwork_t8_114_il2cpp_TypeInfo_var->static_fields)->___networkingPeer_4;
		NullCheck(L_4);
		int32_t L_5 = NetworkingPeer_get_server_m8_377(L_4, /*hidden argument*/NULL);
		if (L_5)
		{
			goto IL_0043;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(PhotonNetwork_t8_114_il2cpp_TypeInfo_var);
		bool L_6 = PhotonNetwork_get_connectedAndReady_m8_605(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (L_6)
		{
			goto IL_004f;
		}
	}

IL_0043:
	{
		Debug_LogError_m6_490(NULL /*static, unused*/, _stringLiteral3425, /*hidden argument*/NULL);
		return 0;
	}

IL_004f:
	{
		TypedLobby_t8_79 * L_7 = ___typedLobby;
		TypedLobby_t8_79 * L_8 = L_7;
		G_B8_0 = L_8;
		if (L_8)
		{
			G_B11_0 = L_8;
			goto IL_0076;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(PhotonNetwork_t8_114_il2cpp_TypeInfo_var);
		NetworkingPeer_t8_98 * L_9 = ((PhotonNetwork_t8_114_StaticFields*)PhotonNetwork_t8_114_il2cpp_TypeInfo_var->static_fields)->___networkingPeer_4;
		NullCheck(L_9);
		bool L_10 = (L_9->___insideLobby_14);
		if (!L_10)
		{
			goto IL_0075;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(PhotonNetwork_t8_114_il2cpp_TypeInfo_var);
		NetworkingPeer_t8_98 * L_11 = ((PhotonNetwork_t8_114_StaticFields*)PhotonNetwork_t8_114_il2cpp_TypeInfo_var->static_fields)->___networkingPeer_4;
		NullCheck(L_11);
		TypedLobby_t8_79 * L_12 = NetworkingPeer_get_lobby_m8_389(L_11, /*hidden argument*/NULL);
		G_B11_0 = L_12;
		goto IL_0076;
	}

IL_0075:
	{
		G_B11_0 = ((TypedLobby_t8_79 *)(NULL));
	}

IL_0076:
	{
		___typedLobby = G_B11_0;
		EnterRoomParams_t8_77 * L_13 = (EnterRoomParams_t8_77 *)il2cpp_codegen_object_new (EnterRoomParams_t8_77_il2cpp_TypeInfo_var);
		EnterRoomParams__ctor_m8_315(L_13, /*hidden argument*/NULL);
		V_0 = L_13;
		EnterRoomParams_t8_77 * L_14 = V_0;
		String_t* L_15 = ___roomName;
		NullCheck(L_14);
		L_14->___RoomName_0 = L_15;
		EnterRoomParams_t8_77 * L_16 = V_0;
		RoomOptions_t8_78 * L_17 = ___roomOptions;
		NullCheck(L_16);
		L_16->___RoomOptions_1 = L_17;
		EnterRoomParams_t8_77 * L_18 = V_0;
		TypedLobby_t8_79 * L_19 = ___typedLobby;
		NullCheck(L_18);
		L_18->___Lobby_2 = L_19;
		IL2CPP_RUNTIME_CLASS_INIT(PhotonNetwork_t8_114_il2cpp_TypeInfo_var);
		NetworkingPeer_t8_98 * L_20 = ((PhotonNetwork_t8_114_StaticFields*)PhotonNetwork_t8_114_il2cpp_TypeInfo_var->static_fields)->___networkingPeer_4;
		EnterRoomParams_t8_77 * L_21 = V_0;
		NullCheck(L_20);
		bool L_22 = NetworkingPeer_OpCreateGame_m8_434(L_20, L_21, /*hidden argument*/NULL);
		return L_22;
	}
}
// System.Boolean PhotonNetwork::JoinRoom(System.String)
extern TypeInfo* PhotonNetwork_t8_114_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* EnterRoomParams_t8_77_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3426;
extern Il2CppCodeGenString* _stringLiteral3427;
extern Il2CppCodeGenString* _stringLiteral3428;
extern "C" bool PhotonNetwork_JoinRoom_m8_681 (Object_t * __this /* static, unused */, String_t* ___roomName, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		PhotonNetwork_t8_114_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1058);
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(11);
		EnterRoomParams_t8_77_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1131);
		_stringLiteral3426 = il2cpp_codegen_string_literal_from_index(3426);
		_stringLiteral3427 = il2cpp_codegen_string_literal_from_index(3427);
		_stringLiteral3428 = il2cpp_codegen_string_literal_from_index(3428);
		s_Il2CppMethodIntialized = true;
	}
	EnterRoomParams_t8_77 * V_0 = {0};
	{
		IL2CPP_RUNTIME_CLASS_INIT(PhotonNetwork_t8_114_il2cpp_TypeInfo_var);
		bool L_0 = PhotonNetwork_get_offlineMode_m8_623(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_002a;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(PhotonNetwork_t8_114_il2cpp_TypeInfo_var);
		Room_t8_100 * L_1 = ((PhotonNetwork_t8_114_StaticFields*)PhotonNetwork_t8_114_il2cpp_TypeInfo_var->static_fields)->___offlineModeRoom_18;
		if (!L_1)
		{
			goto IL_0020;
		}
	}
	{
		Debug_LogError_m6_490(NULL /*static, unused*/, _stringLiteral3426, /*hidden argument*/NULL);
		return 0;
	}

IL_0020:
	{
		String_t* L_2 = ___roomName;
		IL2CPP_RUNTIME_CLASS_INIT(PhotonNetwork_t8_114_il2cpp_TypeInfo_var);
		PhotonNetwork_EnterOfflineRoom_m8_686(NULL /*static, unused*/, L_2, (RoomOptions_t8_78 *)NULL, 1, /*hidden argument*/NULL);
		return 1;
	}

IL_002a:
	{
		IL2CPP_RUNTIME_CLASS_INIT(PhotonNetwork_t8_114_il2cpp_TypeInfo_var);
		NetworkingPeer_t8_98 * L_3 = ((PhotonNetwork_t8_114_StaticFields*)PhotonNetwork_t8_114_il2cpp_TypeInfo_var->static_fields)->___networkingPeer_4;
		NullCheck(L_3);
		int32_t L_4 = NetworkingPeer_get_server_m8_377(L_3, /*hidden argument*/NULL);
		if (L_4)
		{
			goto IL_0043;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(PhotonNetwork_t8_114_il2cpp_TypeInfo_var);
		bool L_5 = PhotonNetwork_get_connectedAndReady_m8_605(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (L_5)
		{
			goto IL_004f;
		}
	}

IL_0043:
	{
		Debug_LogError_m6_490(NULL /*static, unused*/, _stringLiteral3427, /*hidden argument*/NULL);
		return 0;
	}

IL_004f:
	{
		String_t* L_6 = ___roomName;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_7 = String_IsNullOrEmpty_m1_397(NULL /*static, unused*/, L_6, /*hidden argument*/NULL);
		if (!L_7)
		{
			goto IL_0066;
		}
	}
	{
		Debug_LogError_m6_490(NULL /*static, unused*/, _stringLiteral3428, /*hidden argument*/NULL);
		return 0;
	}

IL_0066:
	{
		EnterRoomParams_t8_77 * L_8 = (EnterRoomParams_t8_77 *)il2cpp_codegen_object_new (EnterRoomParams_t8_77_il2cpp_TypeInfo_var);
		EnterRoomParams__ctor_m8_315(L_8, /*hidden argument*/NULL);
		V_0 = L_8;
		EnterRoomParams_t8_77 * L_9 = V_0;
		String_t* L_10 = ___roomName;
		NullCheck(L_9);
		L_9->___RoomName_0 = L_10;
		IL2CPP_RUNTIME_CLASS_INIT(PhotonNetwork_t8_114_il2cpp_TypeInfo_var);
		NetworkingPeer_t8_98 * L_11 = ((PhotonNetwork_t8_114_StaticFields*)PhotonNetwork_t8_114_il2cpp_TypeInfo_var->static_fields)->___networkingPeer_4;
		EnterRoomParams_t8_77 * L_12 = V_0;
		NullCheck(L_11);
		bool L_13 = (bool)VirtFuncInvoker1< bool, EnterRoomParams_t8_77 * >::Invoke(21 /* System.Boolean NetworkingPeer::OpJoinRoom(ExitGames.Client.Photon.LoadbalancingPeer/EnterRoomParams) */, L_11, L_12);
		return L_13;
	}
}
// System.Boolean PhotonNetwork::JoinOrCreateRoom(System.String,RoomOptions,TypedLobby)
extern TypeInfo* PhotonNetwork_t8_114_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* EnterRoomParams_t8_77_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3429;
extern Il2CppCodeGenString* _stringLiteral3430;
extern Il2CppCodeGenString* _stringLiteral3431;
extern "C" bool PhotonNetwork_JoinOrCreateRoom_m8_682 (Object_t * __this /* static, unused */, String_t* ___roomName, RoomOptions_t8_78 * ___roomOptions, TypedLobby_t8_79 * ___typedLobby, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		PhotonNetwork_t8_114_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1058);
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(11);
		EnterRoomParams_t8_77_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1131);
		_stringLiteral3429 = il2cpp_codegen_string_literal_from_index(3429);
		_stringLiteral3430 = il2cpp_codegen_string_literal_from_index(3430);
		_stringLiteral3431 = il2cpp_codegen_string_literal_from_index(3431);
		s_Il2CppMethodIntialized = true;
	}
	EnterRoomParams_t8_77 * V_0 = {0};
	TypedLobby_t8_79 * G_B13_0 = {0};
	TypedLobby_t8_79 * G_B10_0 = {0};
	{
		IL2CPP_RUNTIME_CLASS_INIT(PhotonNetwork_t8_114_il2cpp_TypeInfo_var);
		bool L_0 = PhotonNetwork_get_offlineMode_m8_623(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_002a;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(PhotonNetwork_t8_114_il2cpp_TypeInfo_var);
		Room_t8_100 * L_1 = ((PhotonNetwork_t8_114_StaticFields*)PhotonNetwork_t8_114_il2cpp_TypeInfo_var->static_fields)->___offlineModeRoom_18;
		if (!L_1)
		{
			goto IL_0020;
		}
	}
	{
		Debug_LogError_m6_490(NULL /*static, unused*/, _stringLiteral3429, /*hidden argument*/NULL);
		return 0;
	}

IL_0020:
	{
		String_t* L_2 = ___roomName;
		RoomOptions_t8_78 * L_3 = ___roomOptions;
		IL2CPP_RUNTIME_CLASS_INIT(PhotonNetwork_t8_114_il2cpp_TypeInfo_var);
		PhotonNetwork_EnterOfflineRoom_m8_686(NULL /*static, unused*/, L_2, L_3, 1, /*hidden argument*/NULL);
		return 1;
	}

IL_002a:
	{
		IL2CPP_RUNTIME_CLASS_INIT(PhotonNetwork_t8_114_il2cpp_TypeInfo_var);
		NetworkingPeer_t8_98 * L_4 = ((PhotonNetwork_t8_114_StaticFields*)PhotonNetwork_t8_114_il2cpp_TypeInfo_var->static_fields)->___networkingPeer_4;
		NullCheck(L_4);
		int32_t L_5 = NetworkingPeer_get_server_m8_377(L_4, /*hidden argument*/NULL);
		if (L_5)
		{
			goto IL_0043;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(PhotonNetwork_t8_114_il2cpp_TypeInfo_var);
		bool L_6 = PhotonNetwork_get_connectedAndReady_m8_605(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (L_6)
		{
			goto IL_004f;
		}
	}

IL_0043:
	{
		Debug_LogError_m6_490(NULL /*static, unused*/, _stringLiteral3430, /*hidden argument*/NULL);
		return 0;
	}

IL_004f:
	{
		String_t* L_7 = ___roomName;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_8 = String_IsNullOrEmpty_m1_397(NULL /*static, unused*/, L_7, /*hidden argument*/NULL);
		if (!L_8)
		{
			goto IL_0066;
		}
	}
	{
		Debug_LogError_m6_490(NULL /*static, unused*/, _stringLiteral3431, /*hidden argument*/NULL);
		return 0;
	}

IL_0066:
	{
		TypedLobby_t8_79 * L_9 = ___typedLobby;
		TypedLobby_t8_79 * L_10 = L_9;
		G_B10_0 = L_10;
		if (L_10)
		{
			G_B13_0 = L_10;
			goto IL_008d;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(PhotonNetwork_t8_114_il2cpp_TypeInfo_var);
		NetworkingPeer_t8_98 * L_11 = ((PhotonNetwork_t8_114_StaticFields*)PhotonNetwork_t8_114_il2cpp_TypeInfo_var->static_fields)->___networkingPeer_4;
		NullCheck(L_11);
		bool L_12 = (L_11->___insideLobby_14);
		if (!L_12)
		{
			goto IL_008c;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(PhotonNetwork_t8_114_il2cpp_TypeInfo_var);
		NetworkingPeer_t8_98 * L_13 = ((PhotonNetwork_t8_114_StaticFields*)PhotonNetwork_t8_114_il2cpp_TypeInfo_var->static_fields)->___networkingPeer_4;
		NullCheck(L_13);
		TypedLobby_t8_79 * L_14 = NetworkingPeer_get_lobby_m8_389(L_13, /*hidden argument*/NULL);
		G_B13_0 = L_14;
		goto IL_008d;
	}

IL_008c:
	{
		G_B13_0 = ((TypedLobby_t8_79 *)(NULL));
	}

IL_008d:
	{
		___typedLobby = G_B13_0;
		EnterRoomParams_t8_77 * L_15 = (EnterRoomParams_t8_77 *)il2cpp_codegen_object_new (EnterRoomParams_t8_77_il2cpp_TypeInfo_var);
		EnterRoomParams__ctor_m8_315(L_15, /*hidden argument*/NULL);
		V_0 = L_15;
		EnterRoomParams_t8_77 * L_16 = V_0;
		String_t* L_17 = ___roomName;
		NullCheck(L_16);
		L_16->___RoomName_0 = L_17;
		EnterRoomParams_t8_77 * L_18 = V_0;
		RoomOptions_t8_78 * L_19 = ___roomOptions;
		NullCheck(L_18);
		L_18->___RoomOptions_1 = L_19;
		EnterRoomParams_t8_77 * L_20 = V_0;
		TypedLobby_t8_79 * L_21 = ___typedLobby;
		NullCheck(L_20);
		L_20->___Lobby_2 = L_21;
		EnterRoomParams_t8_77 * L_22 = V_0;
		NullCheck(L_22);
		L_22->___CreateIfNotExists_5 = 1;
		EnterRoomParams_t8_77 * L_23 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(PhotonNetwork_t8_114_il2cpp_TypeInfo_var);
		PhotonPlayer_t8_102 * L_24 = PhotonNetwork_get_player_m8_612(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_24);
		Hashtable_t5_1 * L_25 = PhotonPlayer_get_customProperties_m8_734(L_24, /*hidden argument*/NULL);
		NullCheck(L_23);
		L_23->___PlayerProperties_3 = L_25;
		NetworkingPeer_t8_98 * L_26 = ((PhotonNetwork_t8_114_StaticFields*)PhotonNetwork_t8_114_il2cpp_TypeInfo_var->static_fields)->___networkingPeer_4;
		EnterRoomParams_t8_77 * L_27 = V_0;
		NullCheck(L_26);
		bool L_28 = (bool)VirtFuncInvoker1< bool, EnterRoomParams_t8_77 * >::Invoke(21 /* System.Boolean NetworkingPeer::OpJoinRoom(ExitGames.Client.Photon.LoadbalancingPeer/EnterRoomParams) */, L_26, L_27);
		return L_28;
	}
}
// System.Boolean PhotonNetwork::JoinRandomRoom()
extern TypeInfo* PhotonNetwork_t8_114_il2cpp_TypeInfo_var;
extern "C" bool PhotonNetwork_JoinRandomRoom_m8_683 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		PhotonNetwork_t8_114_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1058);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(PhotonNetwork_t8_114_il2cpp_TypeInfo_var);
		bool L_0 = PhotonNetwork_JoinRandomRoom_m8_685(NULL /*static, unused*/, (Hashtable_t5_1 *)NULL, 0, 0, (TypedLobby_t8_79 *)NULL, (String_t*)NULL, /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Boolean PhotonNetwork::JoinRandomRoom(ExitGames.Client.Photon.Hashtable,System.Byte)
extern TypeInfo* PhotonNetwork_t8_114_il2cpp_TypeInfo_var;
extern "C" bool PhotonNetwork_JoinRandomRoom_m8_684 (Object_t * __this /* static, unused */, Hashtable_t5_1 * ___expectedCustomRoomProperties, uint8_t ___expectedMaxPlayers, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		PhotonNetwork_t8_114_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1058);
		s_Il2CppMethodIntialized = true;
	}
	{
		Hashtable_t5_1 * L_0 = ___expectedCustomRoomProperties;
		uint8_t L_1 = ___expectedMaxPlayers;
		IL2CPP_RUNTIME_CLASS_INIT(PhotonNetwork_t8_114_il2cpp_TypeInfo_var);
		bool L_2 = PhotonNetwork_JoinRandomRoom_m8_685(NULL /*static, unused*/, L_0, L_1, 0, (TypedLobby_t8_79 *)NULL, (String_t*)NULL, /*hidden argument*/NULL);
		return L_2;
	}
}
// System.Boolean PhotonNetwork::JoinRandomRoom(ExitGames.Client.Photon.Hashtable,System.Byte,ExitGames.Client.Photon.MatchmakingMode,TypedLobby,System.String)
extern TypeInfo* PhotonNetwork_t8_114_il2cpp_TypeInfo_var;
extern TypeInfo* OpJoinRandomRoomParams_t8_80_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3432;
extern Il2CppCodeGenString* _stringLiteral3433;
extern Il2CppCodeGenString* _stringLiteral3434;
extern "C" bool PhotonNetwork_JoinRandomRoom_m8_685 (Object_t * __this /* static, unused */, Hashtable_t5_1 * ___expectedCustomRoomProperties, uint8_t ___expectedMaxPlayers, uint8_t ___matchingType, TypedLobby_t8_79 * ___typedLobby, String_t* ___sqlLobbyFilter, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		PhotonNetwork_t8_114_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1058);
		OpJoinRandomRoomParams_t8_80_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1157);
		_stringLiteral3432 = il2cpp_codegen_string_literal_from_index(3432);
		_stringLiteral3433 = il2cpp_codegen_string_literal_from_index(3433);
		_stringLiteral3434 = il2cpp_codegen_string_literal_from_index(3434);
		s_Il2CppMethodIntialized = true;
	}
	OpJoinRandomRoomParams_t8_80 * V_0 = {0};
	TypedLobby_t8_79 * G_B11_0 = {0};
	TypedLobby_t8_79 * G_B8_0 = {0};
	{
		IL2CPP_RUNTIME_CLASS_INIT(PhotonNetwork_t8_114_il2cpp_TypeInfo_var);
		bool L_0 = PhotonNetwork_get_offlineMode_m8_623(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_002e;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(PhotonNetwork_t8_114_il2cpp_TypeInfo_var);
		Room_t8_100 * L_1 = ((PhotonNetwork_t8_114_StaticFields*)PhotonNetwork_t8_114_il2cpp_TypeInfo_var->static_fields)->___offlineModeRoom_18;
		if (!L_1)
		{
			goto IL_0020;
		}
	}
	{
		Debug_LogError_m6_490(NULL /*static, unused*/, _stringLiteral3432, /*hidden argument*/NULL);
		return 0;
	}

IL_0020:
	{
		IL2CPP_RUNTIME_CLASS_INIT(PhotonNetwork_t8_114_il2cpp_TypeInfo_var);
		PhotonNetwork_EnterOfflineRoom_m8_686(NULL /*static, unused*/, _stringLiteral3433, (RoomOptions_t8_78 *)NULL, 1, /*hidden argument*/NULL);
		return 1;
	}

IL_002e:
	{
		IL2CPP_RUNTIME_CLASS_INIT(PhotonNetwork_t8_114_il2cpp_TypeInfo_var);
		NetworkingPeer_t8_98 * L_2 = ((PhotonNetwork_t8_114_StaticFields*)PhotonNetwork_t8_114_il2cpp_TypeInfo_var->static_fields)->___networkingPeer_4;
		NullCheck(L_2);
		int32_t L_3 = NetworkingPeer_get_server_m8_377(L_2, /*hidden argument*/NULL);
		if (L_3)
		{
			goto IL_0047;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(PhotonNetwork_t8_114_il2cpp_TypeInfo_var);
		bool L_4 = PhotonNetwork_get_connectedAndReady_m8_605(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (L_4)
		{
			goto IL_0053;
		}
	}

IL_0047:
	{
		Debug_LogError_m6_490(NULL /*static, unused*/, _stringLiteral3434, /*hidden argument*/NULL);
		return 0;
	}

IL_0053:
	{
		TypedLobby_t8_79 * L_5 = ___typedLobby;
		TypedLobby_t8_79 * L_6 = L_5;
		G_B8_0 = L_6;
		if (L_6)
		{
			G_B11_0 = L_6;
			goto IL_007a;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(PhotonNetwork_t8_114_il2cpp_TypeInfo_var);
		NetworkingPeer_t8_98 * L_7 = ((PhotonNetwork_t8_114_StaticFields*)PhotonNetwork_t8_114_il2cpp_TypeInfo_var->static_fields)->___networkingPeer_4;
		NullCheck(L_7);
		bool L_8 = (L_7->___insideLobby_14);
		if (!L_8)
		{
			goto IL_0079;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(PhotonNetwork_t8_114_il2cpp_TypeInfo_var);
		NetworkingPeer_t8_98 * L_9 = ((PhotonNetwork_t8_114_StaticFields*)PhotonNetwork_t8_114_il2cpp_TypeInfo_var->static_fields)->___networkingPeer_4;
		NullCheck(L_9);
		TypedLobby_t8_79 * L_10 = NetworkingPeer_get_lobby_m8_389(L_9, /*hidden argument*/NULL);
		G_B11_0 = L_10;
		goto IL_007a;
	}

IL_0079:
	{
		G_B11_0 = ((TypedLobby_t8_79 *)(NULL));
	}

IL_007a:
	{
		___typedLobby = G_B11_0;
		OpJoinRandomRoomParams_t8_80 * L_11 = (OpJoinRandomRoomParams_t8_80 *)il2cpp_codegen_object_new (OpJoinRandomRoomParams_t8_80_il2cpp_TypeInfo_var);
		OpJoinRandomRoomParams__ctor_m8_316(L_11, /*hidden argument*/NULL);
		V_0 = L_11;
		OpJoinRandomRoomParams_t8_80 * L_12 = V_0;
		Hashtable_t5_1 * L_13 = ___expectedCustomRoomProperties;
		NullCheck(L_12);
		L_12->___ExpectedCustomRoomProperties_0 = L_13;
		OpJoinRandomRoomParams_t8_80 * L_14 = V_0;
		uint8_t L_15 = ___expectedMaxPlayers;
		NullCheck(L_14);
		L_14->___ExpectedMaxPlayers_1 = L_15;
		OpJoinRandomRoomParams_t8_80 * L_16 = V_0;
		uint8_t L_17 = ___matchingType;
		NullCheck(L_16);
		L_16->___MatchingType_2 = L_17;
		OpJoinRandomRoomParams_t8_80 * L_18 = V_0;
		TypedLobby_t8_79 * L_19 = ___typedLobby;
		NullCheck(L_18);
		L_18->___TypedLobby_3 = L_19;
		OpJoinRandomRoomParams_t8_80 * L_20 = V_0;
		String_t* L_21 = ___sqlLobbyFilter;
		NullCheck(L_20);
		L_20->___SqlLobbyFilter_4 = L_21;
		IL2CPP_RUNTIME_CLASS_INIT(PhotonNetwork_t8_114_il2cpp_TypeInfo_var);
		NetworkingPeer_t8_98 * L_22 = ((PhotonNetwork_t8_114_StaticFields*)PhotonNetwork_t8_114_il2cpp_TypeInfo_var->static_fields)->___networkingPeer_4;
		OpJoinRandomRoomParams_t8_80 * L_23 = V_0;
		NullCheck(L_22);
		bool L_24 = (bool)VirtFuncInvoker1< bool, OpJoinRandomRoomParams_t8_80 * >::Invoke(22 /* System.Boolean NetworkingPeer::OpJoinRandomRoom(ExitGames.Client.Photon.LoadbalancingPeer/OpJoinRandomRoomParams) */, L_22, L_23);
		return L_24;
	}
}
// System.Void PhotonNetwork::EnterOfflineRoom(System.String,RoomOptions,System.Boolean)
extern TypeInfo* Room_t8_100_il2cpp_TypeInfo_var;
extern TypeInfo* PhotonNetwork_t8_114_il2cpp_TypeInfo_var;
extern TypeInfo* ObjectU5BU5D_t1_157_il2cpp_TypeInfo_var;
extern TypeInfo* NetworkingPeer_t8_98_il2cpp_TypeInfo_var;
extern "C" void PhotonNetwork_EnterOfflineRoom_m8_686 (Object_t * __this /* static, unused */, String_t* ___roomName, RoomOptions_t8_78 * ___roomOptions, bool ___createdRoom, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Room_t8_100_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1130);
		PhotonNetwork_t8_114_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1058);
		ObjectU5BU5D_t1_157_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(19);
		NetworkingPeer_t8_98_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1121);
		s_Il2CppMethodIntialized = true;
	}
	{
		String_t* L_0 = ___roomName;
		RoomOptions_t8_78 * L_1 = ___roomOptions;
		Room_t8_100 * L_2 = (Room_t8_100 *)il2cpp_codegen_object_new (Room_t8_100_il2cpp_TypeInfo_var);
		Room__ctor_m8_807(L_2, L_0, L_1, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(PhotonNetwork_t8_114_il2cpp_TypeInfo_var);
		((PhotonNetwork_t8_114_StaticFields*)PhotonNetwork_t8_114_il2cpp_TypeInfo_var->static_fields)->___offlineModeRoom_18 = L_2;
		NetworkingPeer_t8_98 * L_3 = ((PhotonNetwork_t8_114_StaticFields*)PhotonNetwork_t8_114_il2cpp_TypeInfo_var->static_fields)->___networkingPeer_4;
		NullCheck(L_3);
		NetworkingPeer_ChangeLocalID_m8_433(L_3, 1, /*hidden argument*/NULL);
		Room_t8_100 * L_4 = ((PhotonNetwork_t8_114_StaticFields*)PhotonNetwork_t8_114_il2cpp_TypeInfo_var->static_fields)->___offlineModeRoom_18;
		NullCheck(L_4);
		Room_set_masterClientId_m8_821(L_4, 1, /*hidden argument*/NULL);
		bool L_5 = ___createdRoom;
		if (!L_5)
		{
			goto IL_0034;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(NetworkingPeer_t8_98_il2cpp_TypeInfo_var);
		NetworkingPeer_SendMonoMessage_m8_446(NULL /*static, unused*/, 5, ((ObjectU5BU5D_t1_157*)SZArrayNew(ObjectU5BU5D_t1_157_il2cpp_TypeInfo_var, 0)), /*hidden argument*/NULL);
	}

IL_0034:
	{
		IL2CPP_RUNTIME_CLASS_INIT(NetworkingPeer_t8_98_il2cpp_TypeInfo_var);
		NetworkingPeer_SendMonoMessage_m8_446(NULL /*static, unused*/, ((int32_t)12), ((ObjectU5BU5D_t1_157*)SZArrayNew(ObjectU5BU5D_t1_157_il2cpp_TypeInfo_var, 0)), /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean PhotonNetwork::JoinLobby()
extern TypeInfo* PhotonNetwork_t8_114_il2cpp_TypeInfo_var;
extern "C" bool PhotonNetwork_JoinLobby_m8_687 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		PhotonNetwork_t8_114_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1058);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(PhotonNetwork_t8_114_il2cpp_TypeInfo_var);
		bool L_0 = PhotonNetwork_JoinLobby_m8_688(NULL /*static, unused*/, (TypedLobby_t8_79 *)NULL, /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Boolean PhotonNetwork::JoinLobby(TypedLobby)
extern TypeInfo* PhotonNetwork_t8_114_il2cpp_TypeInfo_var;
extern TypeInfo* TypedLobby_t8_79_il2cpp_TypeInfo_var;
extern "C" bool PhotonNetwork_JoinLobby_m8_688 (Object_t * __this /* static, unused */, TypedLobby_t8_79 * ___typedLobby, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		PhotonNetwork_t8_114_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1058);
		TypedLobby_t8_79_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1105);
		s_Il2CppMethodIntialized = true;
	}
	bool V_0 = false;
	{
		IL2CPP_RUNTIME_CLASS_INIT(PhotonNetwork_t8_114_il2cpp_TypeInfo_var);
		bool L_0 = PhotonNetwork_get_connected_m8_603(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_0040;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(PhotonNetwork_t8_114_il2cpp_TypeInfo_var);
		int32_t L_1 = PhotonNetwork_get_Server_m8_608(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (L_1)
		{
			goto IL_0040;
		}
	}
	{
		TypedLobby_t8_79 * L_2 = ___typedLobby;
		if (L_2)
		{
			goto IL_0021;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(TypedLobby_t8_79_il2cpp_TypeInfo_var);
		TypedLobby_t8_79 * L_3 = ((TypedLobby_t8_79_StaticFields*)TypedLobby_t8_79_il2cpp_TypeInfo_var->static_fields)->___Default_2;
		___typedLobby = L_3;
	}

IL_0021:
	{
		IL2CPP_RUNTIME_CLASS_INIT(PhotonNetwork_t8_114_il2cpp_TypeInfo_var);
		NetworkingPeer_t8_98 * L_4 = ((PhotonNetwork_t8_114_StaticFields*)PhotonNetwork_t8_114_il2cpp_TypeInfo_var->static_fields)->___networkingPeer_4;
		TypedLobby_t8_79 * L_5 = ___typedLobby;
		NullCheck(L_4);
		bool L_6 = (bool)VirtFuncInvoker1< bool, TypedLobby_t8_79 * >::Invoke(18 /* System.Boolean ExitGames.Client.Photon.LoadbalancingPeer::OpJoinLobby(TypedLobby) */, L_4, L_5);
		V_0 = L_6;
		bool L_7 = V_0;
		if (!L_7)
		{
			goto IL_003e;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(PhotonNetwork_t8_114_il2cpp_TypeInfo_var);
		NetworkingPeer_t8_98 * L_8 = ((PhotonNetwork_t8_114_StaticFields*)PhotonNetwork_t8_114_il2cpp_TypeInfo_var->static_fields)->___networkingPeer_4;
		TypedLobby_t8_79 * L_9 = ___typedLobby;
		NullCheck(L_8);
		NetworkingPeer_set_lobby_m8_390(L_8, L_9, /*hidden argument*/NULL);
	}

IL_003e:
	{
		bool L_10 = V_0;
		return L_10;
	}

IL_0040:
	{
		return 0;
	}
}
// System.Boolean PhotonNetwork::LeaveLobby()
extern TypeInfo* PhotonNetwork_t8_114_il2cpp_TypeInfo_var;
extern "C" bool PhotonNetwork_LeaveLobby_m8_689 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		PhotonNetwork_t8_114_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1058);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(PhotonNetwork_t8_114_il2cpp_TypeInfo_var);
		bool L_0 = PhotonNetwork_get_connected_m8_603(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_001f;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(PhotonNetwork_t8_114_il2cpp_TypeInfo_var);
		int32_t L_1 = PhotonNetwork_get_Server_m8_608(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (L_1)
		{
			goto IL_001f;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(PhotonNetwork_t8_114_il2cpp_TypeInfo_var);
		NetworkingPeer_t8_98 * L_2 = ((PhotonNetwork_t8_114_StaticFields*)PhotonNetwork_t8_114_il2cpp_TypeInfo_var->static_fields)->___networkingPeer_4;
		NullCheck(L_2);
		bool L_3 = (bool)VirtFuncInvoker0< bool >::Invoke(19 /* System.Boolean ExitGames.Client.Photon.LoadbalancingPeer::OpLeaveLobby() */, L_2);
		return L_3;
	}

IL_001f:
	{
		return 0;
	}
}
// System.Boolean PhotonNetwork::LeaveRoom()
extern TypeInfo* PhotonNetwork_t8_114_il2cpp_TypeInfo_var;
extern TypeInfo* ObjectU5BU5D_t1_157_il2cpp_TypeInfo_var;
extern TypeInfo* NetworkingPeer_t8_98_il2cpp_TypeInfo_var;
extern TypeInfo* PeerState_t8_69_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3435;
extern "C" bool PhotonNetwork_LeaveRoom_m8_690 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		PhotonNetwork_t8_114_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1058);
		ObjectU5BU5D_t1_157_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(19);
		NetworkingPeer_t8_98_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1121);
		PeerState_t8_69_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1061);
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(11);
		_stringLiteral3435 = il2cpp_codegen_string_literal_from_index(3435);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(PhotonNetwork_t8_114_il2cpp_TypeInfo_var);
		bool L_0 = PhotonNetwork_get_offlineMode_m8_623(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_0021;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(PhotonNetwork_t8_114_il2cpp_TypeInfo_var);
		((PhotonNetwork_t8_114_StaticFields*)PhotonNetwork_t8_114_il2cpp_TypeInfo_var->static_fields)->___offlineModeRoom_18 = (Room_t8_100 *)NULL;
		IL2CPP_RUNTIME_CLASS_INIT(NetworkingPeer_t8_98_il2cpp_TypeInfo_var);
		NetworkingPeer_SendMonoMessage_m8_446(NULL /*static, unused*/, 1, ((ObjectU5BU5D_t1_157*)SZArrayNew(ObjectU5BU5D_t1_157_il2cpp_TypeInfo_var, 0)), /*hidden argument*/NULL);
		goto IL_004f;
	}

IL_0021:
	{
		IL2CPP_RUNTIME_CLASS_INIT(PhotonNetwork_t8_114_il2cpp_TypeInfo_var);
		Room_t8_100 * L_1 = PhotonNetwork_get_room_m8_611(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (L_1)
		{
			goto IL_0044;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(PhotonNetwork_t8_114_il2cpp_TypeInfo_var);
		int32_t L_2 = PhotonNetwork_get_connectionStateDetailed_m8_607(NULL /*static, unused*/, /*hidden argument*/NULL);
		int32_t L_3 = L_2;
		Object_t * L_4 = Box(PeerState_t8_69_il2cpp_TypeInfo_var, &L_3);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_5 = String_Concat_m1_416(NULL /*static, unused*/, _stringLiteral3435, L_4, /*hidden argument*/NULL);
		Debug_LogWarning_m6_491(NULL /*static, unused*/, L_5, /*hidden argument*/NULL);
	}

IL_0044:
	{
		IL2CPP_RUNTIME_CLASS_INIT(PhotonNetwork_t8_114_il2cpp_TypeInfo_var);
		NetworkingPeer_t8_98 * L_6 = ((PhotonNetwork_t8_114_StaticFields*)PhotonNetwork_t8_114_il2cpp_TypeInfo_var->static_fields)->___networkingPeer_4;
		NullCheck(L_6);
		bool L_7 = (bool)VirtFuncInvoker0< bool >::Invoke(32 /* System.Boolean NetworkingPeer::OpLeave() */, L_6);
		return L_7;
	}

IL_004f:
	{
		return 1;
	}
}
// RoomInfo[] PhotonNetwork::GetRoomList()
extern TypeInfo* PhotonNetwork_t8_114_il2cpp_TypeInfo_var;
extern TypeInfo* RoomInfoU5BU5D_t8_99_il2cpp_TypeInfo_var;
extern "C" RoomInfoU5BU5D_t8_99* PhotonNetwork_GetRoomList_m8_691 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		PhotonNetwork_t8_114_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1058);
		RoomInfoU5BU5D_t8_99_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1112);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(PhotonNetwork_t8_114_il2cpp_TypeInfo_var);
		bool L_0 = PhotonNetwork_get_offlineMode_m8_623(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (L_0)
		{
			goto IL_0014;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(PhotonNetwork_t8_114_il2cpp_TypeInfo_var);
		NetworkingPeer_t8_98 * L_1 = ((PhotonNetwork_t8_114_StaticFields*)PhotonNetwork_t8_114_il2cpp_TypeInfo_var->static_fields)->___networkingPeer_4;
		if (L_1)
		{
			goto IL_001b;
		}
	}

IL_0014:
	{
		return ((RoomInfoU5BU5D_t8_99*)SZArrayNew(RoomInfoU5BU5D_t8_99_il2cpp_TypeInfo_var, 0));
	}

IL_001b:
	{
		IL2CPP_RUNTIME_CLASS_INIT(PhotonNetwork_t8_114_il2cpp_TypeInfo_var);
		NetworkingPeer_t8_98 * L_2 = ((PhotonNetwork_t8_114_StaticFields*)PhotonNetwork_t8_114_il2cpp_TypeInfo_var->static_fields)->___networkingPeer_4;
		NullCheck(L_2);
		RoomInfoU5BU5D_t8_99* L_3 = (L_2->___mGameListCopy_16);
		return L_3;
	}
}
// System.Void PhotonNetwork::SetPlayerCustomProperties(ExitGames.Client.Photon.Hashtable)
extern TypeInfo* Hashtable_t5_1_il2cpp_TypeInfo_var;
extern TypeInfo* PhotonNetwork_t8_114_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* Enumerator_t1_894_il2cpp_TypeInfo_var;
extern TypeInfo* IDisposable_t1_835_il2cpp_TypeInfo_var;
extern const MethodInfo* Dictionary_2_get_Keys_m1_5550_MethodInfo_var;
extern const MethodInfo* KeyCollection_GetEnumerator_m1_5551_MethodInfo_var;
extern const MethodInfo* Enumerator_get_Current_m1_5552_MethodInfo_var;
extern const MethodInfo* Enumerator_MoveNext_m1_5553_MethodInfo_var;
extern "C" void PhotonNetwork_SetPlayerCustomProperties_m8_692 (Object_t * __this /* static, unused */, Hashtable_t5_1 * ___customProperties, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Hashtable_t5_1_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(891);
		PhotonNetwork_t8_114_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1058);
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(11);
		Enumerator_t1_894_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1128);
		IDisposable_t1_835_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(142);
		Dictionary_2_get_Keys_m1_5550_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483707);
		KeyCollection_GetEnumerator_m1_5551_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483708);
		Enumerator_get_Current_m1_5552_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483709);
		Enumerator_MoveNext_m1_5553_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483710);
		s_Il2CppMethodIntialized = true;
	}
	Object_t * V_0 = {0};
	Enumerator_t1_894  V_1 = {0};
	Exception_t1_33 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1_33 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		Hashtable_t5_1 * L_0 = ___customProperties;
		if (L_0)
		{
			goto IL_0059;
		}
	}
	{
		Hashtable_t5_1 * L_1 = (Hashtable_t5_1 *)il2cpp_codegen_object_new (Hashtable_t5_1_il2cpp_TypeInfo_var);
		Hashtable__ctor_m5_0(L_1, /*hidden argument*/NULL);
		___customProperties = L_1;
		IL2CPP_RUNTIME_CLASS_INIT(PhotonNetwork_t8_114_il2cpp_TypeInfo_var);
		PhotonPlayer_t8_102 * L_2 = PhotonNetwork_get_player_m8_612(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_2);
		Hashtable_t5_1 * L_3 = PhotonPlayer_get_customProperties_m8_734(L_2, /*hidden argument*/NULL);
		NullCheck(L_3);
		KeyCollection_t1_895 * L_4 = Dictionary_2_get_Keys_m1_5550(L_3, /*hidden argument*/Dictionary_2_get_Keys_m1_5550_MethodInfo_var);
		NullCheck(L_4);
		Enumerator_t1_894  L_5 = KeyCollection_GetEnumerator_m1_5551(L_4, /*hidden argument*/KeyCollection_GetEnumerator_m1_5551_MethodInfo_var);
		V_1 = L_5;
	}

IL_0022:
	try
	{ // begin try (depth: 1)
		{
			goto IL_003c;
		}

IL_0027:
		{
			Object_t * L_6 = Enumerator_get_Current_m1_5552((&V_1), /*hidden argument*/Enumerator_get_Current_m1_5552_MethodInfo_var);
			V_0 = L_6;
			Hashtable_t5_1 * L_7 = ___customProperties;
			Object_t * L_8 = V_0;
			NullCheck(L_7);
			Hashtable_set_Item_m5_3(L_7, ((String_t*)CastclassSealed(L_8, String_t_il2cpp_TypeInfo_var)), NULL, /*hidden argument*/NULL);
		}

IL_003c:
		{
			bool L_9 = Enumerator_MoveNext_m1_5553((&V_1), /*hidden argument*/Enumerator_MoveNext_m1_5553_MethodInfo_var);
			if (L_9)
			{
				goto IL_0027;
			}
		}

IL_0048:
		{
			IL2CPP_LEAVE(0x59, FINALLY_004d);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1_33 *)e.ex;
		goto FINALLY_004d;
	}

FINALLY_004d:
	{ // begin finally (depth: 1)
		Enumerator_t1_894  L_10 = V_1;
		Enumerator_t1_894  L_11 = L_10;
		Object_t * L_12 = Box(Enumerator_t1_894_il2cpp_TypeInfo_var, &L_11);
		NullCheck(L_12);
		InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1_835_il2cpp_TypeInfo_var, L_12);
		IL2CPP_END_FINALLY(77)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(77)
	{
		IL2CPP_JUMP_TBL(0x59, IL_0059)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1_33 *)
	}

IL_0059:
	{
		IL2CPP_RUNTIME_CLASS_INIT(PhotonNetwork_t8_114_il2cpp_TypeInfo_var);
		Room_t8_100 * L_13 = PhotonNetwork_get_room_m8_611(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (!L_13)
		{
			goto IL_0084;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(PhotonNetwork_t8_114_il2cpp_TypeInfo_var);
		Room_t8_100 * L_14 = PhotonNetwork_get_room_m8_611(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_14);
		bool L_15 = RoomInfo_get_isLocalClientInside_m8_835(L_14, /*hidden argument*/NULL);
		if (!L_15)
		{
			goto IL_0084;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(PhotonNetwork_t8_114_il2cpp_TypeInfo_var);
		PhotonPlayer_t8_102 * L_16 = PhotonNetwork_get_player_m8_612(NULL /*static, unused*/, /*hidden argument*/NULL);
		Hashtable_t5_1 * L_17 = ___customProperties;
		NullCheck(L_16);
		PhotonPlayer_SetCustomProperties_m8_741(L_16, L_17, (Hashtable_t5_1 *)NULL, 0, /*hidden argument*/NULL);
		goto IL_008f;
	}

IL_0084:
	{
		IL2CPP_RUNTIME_CLASS_INIT(PhotonNetwork_t8_114_il2cpp_TypeInfo_var);
		PhotonPlayer_t8_102 * L_18 = PhotonNetwork_get_player_m8_612(NULL /*static, unused*/, /*hidden argument*/NULL);
		Hashtable_t5_1 * L_19 = ___customProperties;
		NullCheck(L_18);
		PhotonPlayer_InternalCacheProperties_m8_740(L_18, L_19, /*hidden argument*/NULL);
	}

IL_008f:
	{
		return;
	}
}
// System.Void PhotonNetwork::RemovePlayerCustomProperties(System.String[])
extern TypeInfo* PhotonNetwork_t8_114_il2cpp_TypeInfo_var;
extern TypeInfo* Hashtable_t5_1_il2cpp_TypeInfo_var;
extern "C" void PhotonNetwork_RemovePlayerCustomProperties_m8_693 (Object_t * __this /* static, unused */, StringU5BU5D_t1_202* ___customPropertiesToDelete, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		PhotonNetwork_t8_114_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1058);
		Hashtable_t5_1_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(891);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	String_t* V_1 = {0};
	{
		StringU5BU5D_t1_202* L_0 = ___customPropertiesToDelete;
		if (!L_0)
		{
			goto IL_001d;
		}
	}
	{
		StringU5BU5D_t1_202* L_1 = ___customPropertiesToDelete;
		NullCheck(L_1);
		if (!(((int32_t)((int32_t)(((Array_t *)L_1)->max_length)))))
		{
			goto IL_001d;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(PhotonNetwork_t8_114_il2cpp_TypeInfo_var);
		PhotonPlayer_t8_102 * L_2 = PhotonNetwork_get_player_m8_612(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_2);
		Hashtable_t5_1 * L_3 = PhotonPlayer_get_customProperties_m8_734(L_2, /*hidden argument*/NULL);
		if (L_3)
		{
			goto IL_002d;
		}
	}

IL_001d:
	{
		IL2CPP_RUNTIME_CLASS_INIT(PhotonNetwork_t8_114_il2cpp_TypeInfo_var);
		PhotonPlayer_t8_102 * L_4 = PhotonNetwork_get_player_m8_612(NULL /*static, unused*/, /*hidden argument*/NULL);
		Hashtable_t5_1 * L_5 = (Hashtable_t5_1 *)il2cpp_codegen_object_new (Hashtable_t5_1_il2cpp_TypeInfo_var);
		Hashtable__ctor_m5_0(L_5, /*hidden argument*/NULL);
		NullCheck(L_4);
		PhotonPlayer_set_customProperties_m8_735(L_4, L_5, /*hidden argument*/NULL);
		return;
	}

IL_002d:
	{
		V_0 = 0;
		goto IL_0062;
	}

IL_0034:
	{
		StringU5BU5D_t1_202* L_6 = ___customPropertiesToDelete;
		int32_t L_7 = V_0;
		NullCheck(L_6);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_6, L_7);
		int32_t L_8 = L_7;
		V_1 = (*(String_t**)(String_t**)SZArrayLdElema(L_6, L_8, sizeof(String_t*)));
		IL2CPP_RUNTIME_CLASS_INIT(PhotonNetwork_t8_114_il2cpp_TypeInfo_var);
		PhotonPlayer_t8_102 * L_9 = PhotonNetwork_get_player_m8_612(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_9);
		Hashtable_t5_1 * L_10 = PhotonPlayer_get_customProperties_m8_734(L_9, /*hidden argument*/NULL);
		String_t* L_11 = V_1;
		NullCheck(L_10);
		bool L_12 = (bool)VirtFuncInvoker1< bool, Object_t * >::Invoke(28 /* System.Boolean System.Collections.Generic.Dictionary`2<System.Object,System.Object>::ContainsKey(!0) */, L_10, L_11);
		if (!L_12)
		{
			goto IL_005e;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(PhotonNetwork_t8_114_il2cpp_TypeInfo_var);
		PhotonPlayer_t8_102 * L_13 = PhotonNetwork_get_player_m8_612(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_13);
		Hashtable_t5_1 * L_14 = PhotonPlayer_get_customProperties_m8_734(L_13, /*hidden argument*/NULL);
		String_t* L_15 = V_1;
		NullCheck(L_14);
		VirtFuncInvoker1< bool, Object_t * >::Invoke(31 /* System.Boolean System.Collections.Generic.Dictionary`2<System.Object,System.Object>::Remove(!0) */, L_14, L_15);
	}

IL_005e:
	{
		int32_t L_16 = V_0;
		V_0 = ((int32_t)((int32_t)L_16+(int32_t)1));
	}

IL_0062:
	{
		int32_t L_17 = V_0;
		StringU5BU5D_t1_202* L_18 = ___customPropertiesToDelete;
		NullCheck(L_18);
		if ((((int32_t)L_17) < ((int32_t)(((int32_t)((int32_t)(((Array_t *)L_18)->max_length)))))))
		{
			goto IL_0034;
		}
	}
	{
		return;
	}
}
// System.Boolean PhotonNetwork::RaiseEvent(System.Byte,System.Object,System.Boolean,RaiseEventOptions)
extern TypeInfo* PhotonNetwork_t8_114_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3436;
extern "C" bool PhotonNetwork_RaiseEvent_m8_694 (Object_t * __this /* static, unused */, uint8_t ___eventCode, Object_t * ___eventContent, bool ___sendReliable, RaiseEventOptions_t8_93 * ___options, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		PhotonNetwork_t8_114_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1058);
		_stringLiteral3436 = il2cpp_codegen_string_literal_from_index(3436);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(PhotonNetwork_t8_114_il2cpp_TypeInfo_var);
		bool L_0 = PhotonNetwork_get_inRoom_m8_649(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_0015;
		}
	}
	{
		uint8_t L_1 = ___eventCode;
		if ((((int32_t)L_1) < ((int32_t)((int32_t)200))))
		{
			goto IL_0021;
		}
	}

IL_0015:
	{
		Debug_LogWarning_m6_491(NULL /*static, unused*/, _stringLiteral3436, /*hidden argument*/NULL);
		return 0;
	}

IL_0021:
	{
		IL2CPP_RUNTIME_CLASS_INIT(PhotonNetwork_t8_114_il2cpp_TypeInfo_var);
		NetworkingPeer_t8_98 * L_2 = ((PhotonNetwork_t8_114_StaticFields*)PhotonNetwork_t8_114_il2cpp_TypeInfo_var->static_fields)->___networkingPeer_4;
		uint8_t L_3 = ___eventCode;
		Object_t * L_4 = ___eventContent;
		bool L_5 = ___sendReliable;
		RaiseEventOptions_t8_93 * L_6 = ___options;
		NullCheck(L_2);
		bool L_7 = (bool)VirtFuncInvoker4< bool, uint8_t, Object_t *, bool, RaiseEventOptions_t8_93 * >::Invoke(27 /* System.Boolean NetworkingPeer::OpRaiseEvent(System.Byte,System.Object,System.Boolean,RaiseEventOptions) */, L_2, L_3, L_4, L_5, L_6);
		return L_7;
	}
}
// System.Int32 PhotonNetwork::AllocateViewID()
extern TypeInfo* PhotonNetwork_t8_114_il2cpp_TypeInfo_var;
extern "C" int32_t PhotonNetwork_AllocateViewID_m8_695 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		PhotonNetwork_t8_114_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1058);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	{
		IL2CPP_RUNTIME_CLASS_INIT(PhotonNetwork_t8_114_il2cpp_TypeInfo_var);
		PhotonPlayer_t8_102 * L_0 = PhotonNetwork_get_player_m8_612(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_0);
		int32_t L_1 = PhotonPlayer_get_ID_m8_730(L_0, /*hidden argument*/NULL);
		int32_t L_2 = PhotonNetwork_AllocateViewID_m8_697(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		List_1_t1_945 * L_3 = ((PhotonNetwork_t8_114_StaticFields*)PhotonNetwork_t8_114_il2cpp_TypeInfo_var->static_fields)->___manuallyAllocatedViewIds_29;
		int32_t L_4 = V_0;
		NullCheck(L_3);
		VirtActionInvoker1< int32_t >::Invoke(19 /* System.Void System.Collections.Generic.List`1<System.Int32>::Add(!0) */, L_3, L_4);
		int32_t L_5 = V_0;
		return L_5;
	}
}
// System.Int32 PhotonNetwork::AllocateSceneViewID()
extern TypeInfo* PhotonNetwork_t8_114_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3437;
extern "C" int32_t PhotonNetwork_AllocateSceneViewID_m8_696 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		PhotonNetwork_t8_114_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1058);
		_stringLiteral3437 = il2cpp_codegen_string_literal_from_index(3437);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	{
		IL2CPP_RUNTIME_CLASS_INIT(PhotonNetwork_t8_114_il2cpp_TypeInfo_var);
		bool L_0 = PhotonNetwork_get_isMasterClient_m8_648(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (L_0)
		{
			goto IL_0016;
		}
	}
	{
		Debug_LogError_m6_490(NULL /*static, unused*/, _stringLiteral3437, /*hidden argument*/NULL);
		return (-1);
	}

IL_0016:
	{
		IL2CPP_RUNTIME_CLASS_INIT(PhotonNetwork_t8_114_il2cpp_TypeInfo_var);
		int32_t L_1 = PhotonNetwork_AllocateViewID_m8_697(NULL /*static, unused*/, 0, /*hidden argument*/NULL);
		V_0 = L_1;
		List_1_t1_945 * L_2 = ((PhotonNetwork_t8_114_StaticFields*)PhotonNetwork_t8_114_il2cpp_TypeInfo_var->static_fields)->___manuallyAllocatedViewIds_29;
		int32_t L_3 = V_0;
		NullCheck(L_2);
		VirtActionInvoker1< int32_t >::Invoke(19 /* System.Void System.Collections.Generic.List`1<System.Int32>::Add(!0) */, L_2, L_3);
		int32_t L_4 = V_0;
		return L_4;
	}
}
// System.Int32 PhotonNetwork::AllocateViewID(System.Int32)
extern TypeInfo* PhotonNetwork_t8_114_il2cpp_TypeInfo_var;
extern TypeInfo* Int32_t1_3_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* Exception_t1_33_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3438;
extern Il2CppCodeGenString* _stringLiteral3439;
extern "C" int32_t PhotonNetwork_AllocateViewID_m8_697 (Object_t * __this /* static, unused */, int32_t ___ownerId, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		PhotonNetwork_t8_114_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1058);
		Int32_t1_3_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(8);
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(11);
		Exception_t1_33_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(45);
		_stringLiteral3438 = il2cpp_codegen_string_literal_from_index(3438);
		_stringLiteral3439 = il2cpp_codegen_string_literal_from_index(3439);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	int32_t V_3 = 0;
	int32_t V_4 = 0;
	int32_t V_5 = 0;
	int32_t V_6 = 0;
	int32_t V_7 = 0;
	{
		int32_t L_0 = ___ownerId;
		if (L_0)
		{
			goto IL_0076;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(PhotonNetwork_t8_114_il2cpp_TypeInfo_var);
		int32_t L_1 = ((PhotonNetwork_t8_114_StaticFields*)PhotonNetwork_t8_114_il2cpp_TypeInfo_var->static_fields)->___lastUsedViewSubIdStatic_28;
		V_0 = L_1;
		int32_t L_2 = ___ownerId;
		int32_t L_3 = ((PhotonNetwork_t8_114_StaticFields*)PhotonNetwork_t8_114_il2cpp_TypeInfo_var->static_fields)->___MAX_VIEW_IDS_5;
		V_2 = ((int32_t)((int32_t)L_2*(int32_t)L_3));
		V_3 = 1;
		goto IL_0055;
	}

IL_001b:
	{
		int32_t L_4 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(PhotonNetwork_t8_114_il2cpp_TypeInfo_var);
		int32_t L_5 = ((PhotonNetwork_t8_114_StaticFields*)PhotonNetwork_t8_114_il2cpp_TypeInfo_var->static_fields)->___MAX_VIEW_IDS_5;
		V_0 = ((int32_t)((int32_t)((int32_t)((int32_t)L_4+(int32_t)1))%(int32_t)L_5));
		int32_t L_6 = V_0;
		if (L_6)
		{
			goto IL_0030;
		}
	}
	{
		goto IL_0051;
	}

IL_0030:
	{
		int32_t L_7 = V_0;
		int32_t L_8 = V_2;
		V_1 = ((int32_t)((int32_t)L_7+(int32_t)L_8));
		IL2CPP_RUNTIME_CLASS_INIT(PhotonNetwork_t8_114_il2cpp_TypeInfo_var);
		NetworkingPeer_t8_98 * L_9 = ((PhotonNetwork_t8_114_StaticFields*)PhotonNetwork_t8_114_il2cpp_TypeInfo_var->static_fields)->___networkingPeer_4;
		NullCheck(L_9);
		Dictionary_2_t1_937 * L_10 = (L_9->___photonViewList_31);
		int32_t L_11 = V_1;
		NullCheck(L_10);
		bool L_12 = (bool)VirtFuncInvoker1< bool, int32_t >::Invoke(28 /* System.Boolean System.Collections.Generic.Dictionary`2<System.Int32,PhotonView>::ContainsKey(!0) */, L_10, L_11);
		if (L_12)
		{
			goto IL_0051;
		}
	}
	{
		int32_t L_13 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(PhotonNetwork_t8_114_il2cpp_TypeInfo_var);
		((PhotonNetwork_t8_114_StaticFields*)PhotonNetwork_t8_114_il2cpp_TypeInfo_var->static_fields)->___lastUsedViewSubIdStatic_28 = L_13;
		int32_t L_14 = V_1;
		return L_14;
	}

IL_0051:
	{
		int32_t L_15 = V_3;
		V_3 = ((int32_t)((int32_t)L_15+(int32_t)1));
	}

IL_0055:
	{
		int32_t L_16 = V_3;
		IL2CPP_RUNTIME_CLASS_INIT(PhotonNetwork_t8_114_il2cpp_TypeInfo_var);
		int32_t L_17 = ((PhotonNetwork_t8_114_StaticFields*)PhotonNetwork_t8_114_il2cpp_TypeInfo_var->static_fields)->___MAX_VIEW_IDS_5;
		if ((((int32_t)L_16) < ((int32_t)L_17)))
		{
			goto IL_001b;
		}
	}
	{
		int32_t L_18 = ___ownerId;
		int32_t L_19 = L_18;
		Object_t * L_20 = Box(Int32_t1_3_il2cpp_TypeInfo_var, &L_19);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_21 = String_Format_m1_410(NULL /*static, unused*/, _stringLiteral3438, L_20, /*hidden argument*/NULL);
		Exception_t1_33 * L_22 = (Exception_t1_33 *)il2cpp_codegen_object_new (Exception_t1_33_il2cpp_TypeInfo_var);
		Exception__ctor_m1_932(L_22, L_21, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_22);
	}

IL_0076:
	{
		IL2CPP_RUNTIME_CLASS_INIT(PhotonNetwork_t8_114_il2cpp_TypeInfo_var);
		int32_t L_23 = ((PhotonNetwork_t8_114_StaticFields*)PhotonNetwork_t8_114_il2cpp_TypeInfo_var->static_fields)->___lastUsedViewSubId_27;
		V_4 = L_23;
		int32_t L_24 = ___ownerId;
		int32_t L_25 = ((PhotonNetwork_t8_114_StaticFields*)PhotonNetwork_t8_114_il2cpp_TypeInfo_var->static_fields)->___MAX_VIEW_IDS_5;
		V_6 = ((int32_t)((int32_t)L_24*(int32_t)L_25));
		V_7 = 1;
		goto IL_00e4;
	}

IL_008e:
	{
		int32_t L_26 = V_4;
		IL2CPP_RUNTIME_CLASS_INIT(PhotonNetwork_t8_114_il2cpp_TypeInfo_var);
		int32_t L_27 = ((PhotonNetwork_t8_114_StaticFields*)PhotonNetwork_t8_114_il2cpp_TypeInfo_var->static_fields)->___MAX_VIEW_IDS_5;
		V_4 = ((int32_t)((int32_t)((int32_t)((int32_t)L_26+(int32_t)1))%(int32_t)L_27));
		int32_t L_28 = V_4;
		if (L_28)
		{
			goto IL_00a6;
		}
	}
	{
		goto IL_00de;
	}

IL_00a6:
	{
		int32_t L_29 = V_4;
		int32_t L_30 = V_6;
		V_5 = ((int32_t)((int32_t)L_29+(int32_t)L_30));
		IL2CPP_RUNTIME_CLASS_INIT(PhotonNetwork_t8_114_il2cpp_TypeInfo_var);
		NetworkingPeer_t8_98 * L_31 = ((PhotonNetwork_t8_114_StaticFields*)PhotonNetwork_t8_114_il2cpp_TypeInfo_var->static_fields)->___networkingPeer_4;
		NullCheck(L_31);
		Dictionary_2_t1_937 * L_32 = (L_31->___photonViewList_31);
		int32_t L_33 = V_5;
		NullCheck(L_32);
		bool L_34 = (bool)VirtFuncInvoker1< bool, int32_t >::Invoke(28 /* System.Boolean System.Collections.Generic.Dictionary`2<System.Int32,PhotonView>::ContainsKey(!0) */, L_32, L_33);
		if (L_34)
		{
			goto IL_00de;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(PhotonNetwork_t8_114_il2cpp_TypeInfo_var);
		List_1_t1_945 * L_35 = ((PhotonNetwork_t8_114_StaticFields*)PhotonNetwork_t8_114_il2cpp_TypeInfo_var->static_fields)->___manuallyAllocatedViewIds_29;
		int32_t L_36 = V_5;
		NullCheck(L_35);
		bool L_37 = (bool)VirtFuncInvoker1< bool, int32_t >::Invoke(21 /* System.Boolean System.Collections.Generic.List`1<System.Int32>::Contains(!0) */, L_35, L_36);
		if (L_37)
		{
			goto IL_00de;
		}
	}
	{
		int32_t L_38 = V_4;
		IL2CPP_RUNTIME_CLASS_INIT(PhotonNetwork_t8_114_il2cpp_TypeInfo_var);
		((PhotonNetwork_t8_114_StaticFields*)PhotonNetwork_t8_114_il2cpp_TypeInfo_var->static_fields)->___lastUsedViewSubId_27 = L_38;
		int32_t L_39 = V_5;
		return L_39;
	}

IL_00de:
	{
		int32_t L_40 = V_7;
		V_7 = ((int32_t)((int32_t)L_40+(int32_t)1));
	}

IL_00e4:
	{
		int32_t L_41 = V_7;
		IL2CPP_RUNTIME_CLASS_INIT(PhotonNetwork_t8_114_il2cpp_TypeInfo_var);
		int32_t L_42 = ((PhotonNetwork_t8_114_StaticFields*)PhotonNetwork_t8_114_il2cpp_TypeInfo_var->static_fields)->___MAX_VIEW_IDS_5;
		if ((((int32_t)L_41) < ((int32_t)L_42)))
		{
			goto IL_008e;
		}
	}
	{
		int32_t L_43 = ___ownerId;
		int32_t L_44 = L_43;
		Object_t * L_45 = Box(Int32_t1_3_il2cpp_TypeInfo_var, &L_44);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_46 = String_Format_m1_410(NULL /*static, unused*/, _stringLiteral3439, L_45, /*hidden argument*/NULL);
		Exception_t1_33 * L_47 = (Exception_t1_33 *)il2cpp_codegen_object_new (Exception_t1_33_il2cpp_TypeInfo_var);
		Exception__ctor_m1_932(L_47, L_46, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_47);
	}
}
// System.Int32[] PhotonNetwork::AllocateSceneViewIDs(System.Int32)
extern TypeInfo* Int32U5BU5D_t1_160_il2cpp_TypeInfo_var;
extern TypeInfo* PhotonNetwork_t8_114_il2cpp_TypeInfo_var;
extern "C" Int32U5BU5D_t1_160* PhotonNetwork_AllocateSceneViewIDs_m8_698 (Object_t * __this /* static, unused */, int32_t ___countOfNewViews, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Int32U5BU5D_t1_160_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(47);
		PhotonNetwork_t8_114_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1058);
		s_Il2CppMethodIntialized = true;
	}
	Int32U5BU5D_t1_160* V_0 = {0};
	int32_t V_1 = 0;
	{
		int32_t L_0 = ___countOfNewViews;
		V_0 = ((Int32U5BU5D_t1_160*)SZArrayNew(Int32U5BU5D_t1_160_il2cpp_TypeInfo_var, L_0));
		V_1 = 0;
		goto IL_001b;
	}

IL_000e:
	{
		Int32U5BU5D_t1_160* L_1 = V_0;
		int32_t L_2 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(PhotonNetwork_t8_114_il2cpp_TypeInfo_var);
		int32_t L_3 = PhotonNetwork_AllocateViewID_m8_697(NULL /*static, unused*/, 0, /*hidden argument*/NULL);
		NullCheck(L_1);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_1, L_2);
		*((int32_t*)(int32_t*)SZArrayLdElema(L_1, L_2, sizeof(int32_t))) = (int32_t)L_3;
		int32_t L_4 = V_1;
		V_1 = ((int32_t)((int32_t)L_4+(int32_t)1));
	}

IL_001b:
	{
		int32_t L_5 = V_1;
		int32_t L_6 = ___countOfNewViews;
		if ((((int32_t)L_5) < ((int32_t)L_6)))
		{
			goto IL_000e;
		}
	}
	{
		Int32U5BU5D_t1_160* L_7 = V_0;
		return L_7;
	}
}
// System.Void PhotonNetwork::UnAllocateViewID(System.Int32)
extern TypeInfo* PhotonNetwork_t8_114_il2cpp_TypeInfo_var;
extern TypeInfo* Int32_t1_3_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3440;
extern "C" void PhotonNetwork_UnAllocateViewID_m8_699 (Object_t * __this /* static, unused */, int32_t ___viewID, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		PhotonNetwork_t8_114_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1058);
		Int32_t1_3_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(8);
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(11);
		_stringLiteral3440 = il2cpp_codegen_string_literal_from_index(3440);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(PhotonNetwork_t8_114_il2cpp_TypeInfo_var);
		List_1_t1_945 * L_0 = ((PhotonNetwork_t8_114_StaticFields*)PhotonNetwork_t8_114_il2cpp_TypeInfo_var->static_fields)->___manuallyAllocatedViewIds_29;
		int32_t L_1 = ___viewID;
		NullCheck(L_0);
		VirtFuncInvoker1< bool, int32_t >::Invoke(23 /* System.Boolean System.Collections.Generic.List`1<System.Int32>::Remove(!0) */, L_0, L_1);
		NetworkingPeer_t8_98 * L_2 = ((PhotonNetwork_t8_114_StaticFields*)PhotonNetwork_t8_114_il2cpp_TypeInfo_var->static_fields)->___networkingPeer_4;
		NullCheck(L_2);
		Dictionary_2_t1_937 * L_3 = (L_2->___photonViewList_31);
		int32_t L_4 = ___viewID;
		NullCheck(L_3);
		bool L_5 = (bool)VirtFuncInvoker1< bool, int32_t >::Invoke(28 /* System.Boolean System.Collections.Generic.Dictionary`2<System.Int32,PhotonView>::ContainsKey(!0) */, L_3, L_4);
		if (!L_5)
		{
			goto IL_0046;
		}
	}
	{
		int32_t L_6 = ___viewID;
		int32_t L_7 = L_6;
		Object_t * L_8 = Box(Int32_t1_3_il2cpp_TypeInfo_var, &L_7);
		IL2CPP_RUNTIME_CLASS_INIT(PhotonNetwork_t8_114_il2cpp_TypeInfo_var);
		NetworkingPeer_t8_98 * L_9 = ((PhotonNetwork_t8_114_StaticFields*)PhotonNetwork_t8_114_il2cpp_TypeInfo_var->static_fields)->___networkingPeer_4;
		NullCheck(L_9);
		Dictionary_2_t1_937 * L_10 = (L_9->___photonViewList_31);
		int32_t L_11 = ___viewID;
		NullCheck(L_10);
		PhotonView_t8_3 * L_12 = (PhotonView_t8_3 *)VirtFuncInvoker1< PhotonView_t8_3 *, int32_t >::Invoke(25 /* !1 System.Collections.Generic.Dictionary`2<System.Int32,PhotonView>::get_Item(!0) */, L_10, L_11);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_13 = String_Format_m1_411(NULL /*static, unused*/, _stringLiteral3440, L_8, L_12, /*hidden argument*/NULL);
		Debug_LogWarning_m6_491(NULL /*static, unused*/, L_13, /*hidden argument*/NULL);
	}

IL_0046:
	{
		return;
	}
}
// UnityEngine.GameObject PhotonNetwork::Instantiate(System.String,UnityEngine.Vector3,UnityEngine.Quaternion,System.Int32)
extern TypeInfo* PhotonNetwork_t8_114_il2cpp_TypeInfo_var;
extern "C" GameObject_t6_85 * PhotonNetwork_Instantiate_m8_700 (Object_t * __this /* static, unused */, String_t* ___prefabName, Vector3_t6_49  ___position, Quaternion_t6_51  ___rotation, int32_t ___group, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		PhotonNetwork_t8_114_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1058);
		s_Il2CppMethodIntialized = true;
	}
	{
		String_t* L_0 = ___prefabName;
		Vector3_t6_49  L_1 = ___position;
		Quaternion_t6_51  L_2 = ___rotation;
		int32_t L_3 = ___group;
		IL2CPP_RUNTIME_CLASS_INIT(PhotonNetwork_t8_114_il2cpp_TypeInfo_var);
		GameObject_t6_85 * L_4 = PhotonNetwork_Instantiate_m8_701(NULL /*static, unused*/, L_0, L_1, L_2, L_3, (ObjectU5BU5D_t1_157*)(ObjectU5BU5D_t1_157*)NULL, /*hidden argument*/NULL);
		return L_4;
	}
}
// UnityEngine.GameObject PhotonNetwork::Instantiate(System.String,UnityEngine.Vector3,UnityEngine.Quaternion,System.Int32,System.Object[])
extern const Il2CppType* GameObject_t6_85_0_0_0_var;
extern TypeInfo* PhotonNetwork_t8_114_il2cpp_TypeInfo_var;
extern TypeInfo* ObjectU5BU5D_t1_157_il2cpp_TypeInfo_var;
extern TypeInfo* PeerState_t8_69_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* Type_t_il2cpp_TypeInfo_var;
extern TypeInfo* GameObject_t6_85_il2cpp_TypeInfo_var;
extern TypeInfo* Int32U5BU5D_t1_160_il2cpp_TypeInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisPhotonView_t8_3_m6_1666_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral3441;
extern Il2CppCodeGenString* _stringLiteral3442;
extern Il2CppCodeGenString* _stringLiteral3443;
extern Il2CppCodeGenString* _stringLiteral3444;
extern Il2CppCodeGenString* _stringLiteral3445;
extern "C" GameObject_t6_85 * PhotonNetwork_Instantiate_m8_701 (Object_t * __this /* static, unused */, String_t* ___prefabName, Vector3_t6_49  ___position, Quaternion_t6_51  ___rotation, int32_t ___group, ObjectU5BU5D_t1_157* ___data, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GameObject_t6_85_0_0_0_var = il2cpp_codegen_type_from_index(1082);
		PhotonNetwork_t8_114_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1058);
		ObjectU5BU5D_t1_157_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(19);
		PeerState_t8_69_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1061);
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(11);
		Type_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3);
		GameObject_t6_85_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1082);
		Int32U5BU5D_t1_160_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(47);
		GameObject_GetComponent_TisPhotonView_t8_3_m6_1666_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483849);
		_stringLiteral3441 = il2cpp_codegen_string_literal_from_index(3441);
		_stringLiteral3442 = il2cpp_codegen_string_literal_from_index(3442);
		_stringLiteral3443 = il2cpp_codegen_string_literal_from_index(3443);
		_stringLiteral3444 = il2cpp_codegen_string_literal_from_index(3444);
		_stringLiteral3445 = il2cpp_codegen_string_literal_from_index(3445);
		s_Il2CppMethodIntialized = true;
	}
	GameObject_t6_85 * V_0 = {0};
	ComponentU5BU5D_t6_274* V_1 = {0};
	Int32U5BU5D_t1_160* V_2 = {0};
	int32_t V_3 = 0;
	Hashtable_t5_1 * V_4 = {0};
	{
		IL2CPP_RUNTIME_CLASS_INIT(PhotonNetwork_t8_114_il2cpp_TypeInfo_var);
		bool L_0 = PhotonNetwork_get_connected_m8_603(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_001e;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(PhotonNetwork_t8_114_il2cpp_TypeInfo_var);
		bool L_1 = ((PhotonNetwork_t8_114_StaticFields*)PhotonNetwork_t8_114_il2cpp_TypeInfo_var->static_fields)->___InstantiateInRoomOnly_7;
		if (!L_1)
		{
			goto IL_0051;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(PhotonNetwork_t8_114_il2cpp_TypeInfo_var);
		bool L_2 = PhotonNetwork_get_inRoom_m8_649(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (L_2)
		{
			goto IL_0051;
		}
	}

IL_001e:
	{
		ObjectU5BU5D_t1_157* L_3 = ((ObjectU5BU5D_t1_157*)SZArrayNew(ObjectU5BU5D_t1_157_il2cpp_TypeInfo_var, 4));
		NullCheck(L_3);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_3, 0);
		ArrayElementTypeCheck (L_3, _stringLiteral3441);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_3, 0, sizeof(Object_t *))) = (Object_t *)_stringLiteral3441;
		ObjectU5BU5D_t1_157* L_4 = L_3;
		String_t* L_5 = ___prefabName;
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, 1);
		ArrayElementTypeCheck (L_4, L_5);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_4, 1, sizeof(Object_t *))) = (Object_t *)L_5;
		ObjectU5BU5D_t1_157* L_6 = L_4;
		NullCheck(L_6);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_6, 2);
		ArrayElementTypeCheck (L_6, _stringLiteral3442);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_6, 2, sizeof(Object_t *))) = (Object_t *)_stringLiteral3442;
		ObjectU5BU5D_t1_157* L_7 = L_6;
		IL2CPP_RUNTIME_CLASS_INIT(PhotonNetwork_t8_114_il2cpp_TypeInfo_var);
		int32_t L_8 = PhotonNetwork_get_connectionStateDetailed_m8_607(NULL /*static, unused*/, /*hidden argument*/NULL);
		int32_t L_9 = L_8;
		Object_t * L_10 = Box(PeerState_t8_69_il2cpp_TypeInfo_var, &L_9);
		NullCheck(L_7);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_7, 3);
		ArrayElementTypeCheck (L_7, L_10);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_7, 3, sizeof(Object_t *))) = (Object_t *)L_10;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_11 = String_Concat_m1_421(NULL /*static, unused*/, L_7, /*hidden argument*/NULL);
		Debug_LogError_m6_490(NULL /*static, unused*/, L_11, /*hidden argument*/NULL);
		return (GameObject_t6_85 *)NULL;
	}

IL_0051:
	{
		IL2CPP_RUNTIME_CLASS_INIT(PhotonNetwork_t8_114_il2cpp_TypeInfo_var);
		bool L_12 = ((PhotonNetwork_t8_114_StaticFields*)PhotonNetwork_t8_114_il2cpp_TypeInfo_var->static_fields)->___UsePrefabCache_13;
		if (!L_12)
		{
			goto IL_006d;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(PhotonNetwork_t8_114_il2cpp_TypeInfo_var);
		Dictionary_2_t1_942 * L_13 = ((PhotonNetwork_t8_114_StaticFields*)PhotonNetwork_t8_114_il2cpp_TypeInfo_var->static_fields)->___PrefabCache_14;
		String_t* L_14 = ___prefabName;
		NullCheck(L_13);
		bool L_15 = (bool)VirtFuncInvoker2< bool, String_t*, GameObject_t6_85 ** >::Invoke(32 /* System.Boolean System.Collections.Generic.Dictionary`2<System.String,UnityEngine.GameObject>::TryGetValue(!0,!1&) */, L_13, L_14, (&V_0));
		if (L_15)
		{
			goto IL_0099;
		}
	}

IL_006d:
	{
		String_t* L_16 = ___prefabName;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_17 = Type_GetTypeFromHandle_m1_893(NULL /*static, unused*/, LoadTypeToken(GameObject_t6_85_0_0_0_var), /*hidden argument*/NULL);
		Object_t6_5 * L_18 = Resources_Load_m6_409(NULL /*static, unused*/, L_16, L_17, /*hidden argument*/NULL);
		V_0 = ((GameObject_t6_85 *)CastclassSealed(L_18, GameObject_t6_85_il2cpp_TypeInfo_var));
		IL2CPP_RUNTIME_CLASS_INIT(PhotonNetwork_t8_114_il2cpp_TypeInfo_var);
		bool L_19 = ((PhotonNetwork_t8_114_StaticFields*)PhotonNetwork_t8_114_il2cpp_TypeInfo_var->static_fields)->___UsePrefabCache_13;
		if (!L_19)
		{
			goto IL_0099;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(PhotonNetwork_t8_114_il2cpp_TypeInfo_var);
		Dictionary_2_t1_942 * L_20 = ((PhotonNetwork_t8_114_StaticFields*)PhotonNetwork_t8_114_il2cpp_TypeInfo_var->static_fields)->___PrefabCache_14;
		String_t* L_21 = ___prefabName;
		GameObject_t6_85 * L_22 = V_0;
		NullCheck(L_20);
		VirtActionInvoker2< String_t*, GameObject_t6_85 * >::Invoke(27 /* System.Void System.Collections.Generic.Dictionary`2<System.String,UnityEngine.GameObject>::Add(!0,!1) */, L_20, L_21, L_22);
	}

IL_0099:
	{
		GameObject_t6_85 * L_23 = V_0;
		bool L_24 = Object_op_Equality_m6_579(NULL /*static, unused*/, L_23, (Object_t6_5 *)NULL, /*hidden argument*/NULL);
		if (!L_24)
		{
			goto IL_00bc;
		}
	}
	{
		String_t* L_25 = ___prefabName;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_26 = String_Concat_m1_419(NULL /*static, unused*/, _stringLiteral3441, L_25, _stringLiteral3443, /*hidden argument*/NULL);
		Debug_LogError_m6_490(NULL /*static, unused*/, L_26, /*hidden argument*/NULL);
		return (GameObject_t6_85 *)NULL;
	}

IL_00bc:
	{
		GameObject_t6_85 * L_27 = V_0;
		NullCheck(L_27);
		PhotonView_t8_3 * L_28 = GameObject_GetComponent_TisPhotonView_t8_3_m6_1666(L_27, /*hidden argument*/GameObject_GetComponent_TisPhotonView_t8_3_m6_1666_MethodInfo_var);
		bool L_29 = Object_op_Equality_m6_579(NULL /*static, unused*/, L_28, (Object_t6_5 *)NULL, /*hidden argument*/NULL);
		if (!L_29)
		{
			goto IL_00e4;
		}
	}
	{
		String_t* L_30 = ___prefabName;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_31 = String_Concat_m1_419(NULL /*static, unused*/, _stringLiteral3444, L_30, _stringLiteral3445, /*hidden argument*/NULL);
		Debug_LogError_m6_490(NULL /*static, unused*/, L_31, /*hidden argument*/NULL);
		return (GameObject_t6_85 *)NULL;
	}

IL_00e4:
	{
		GameObject_t6_85 * L_32 = V_0;
		PhotonViewU5BU5D_t8_180* L_33 = Extensions_GetPhotonViewsInChildren_m8_291(NULL /*static, unused*/, L_32, /*hidden argument*/NULL);
		V_1 = (ComponentU5BU5D_t6_274*)L_33;
		ComponentU5BU5D_t6_274* L_34 = V_1;
		NullCheck(L_34);
		V_2 = ((Int32U5BU5D_t1_160*)SZArrayNew(Int32U5BU5D_t1_160_il2cpp_TypeInfo_var, (((int32_t)((int32_t)(((Array_t *)L_34)->max_length))))));
		V_3 = 0;
		goto IL_0111;
	}

IL_00fb:
	{
		Int32U5BU5D_t1_160* L_35 = V_2;
		int32_t L_36 = V_3;
		IL2CPP_RUNTIME_CLASS_INIT(PhotonNetwork_t8_114_il2cpp_TypeInfo_var);
		PhotonPlayer_t8_102 * L_37 = PhotonNetwork_get_player_m8_612(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_37);
		int32_t L_38 = PhotonPlayer_get_ID_m8_730(L_37, /*hidden argument*/NULL);
		int32_t L_39 = PhotonNetwork_AllocateViewID_m8_697(NULL /*static, unused*/, L_38, /*hidden argument*/NULL);
		NullCheck(L_35);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_35, L_36);
		*((int32_t*)(int32_t*)SZArrayLdElema(L_35, L_36, sizeof(int32_t))) = (int32_t)L_39;
		int32_t L_40 = V_3;
		V_3 = ((int32_t)((int32_t)L_40+(int32_t)1));
	}

IL_0111:
	{
		int32_t L_41 = V_3;
		Int32U5BU5D_t1_160* L_42 = V_2;
		NullCheck(L_42);
		if ((((int32_t)L_41) < ((int32_t)(((int32_t)((int32_t)(((Array_t *)L_42)->max_length)))))))
		{
			goto IL_00fb;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(PhotonNetwork_t8_114_il2cpp_TypeInfo_var);
		NetworkingPeer_t8_98 * L_43 = ((PhotonNetwork_t8_114_StaticFields*)PhotonNetwork_t8_114_il2cpp_TypeInfo_var->static_fields)->___networkingPeer_4;
		String_t* L_44 = ___prefabName;
		Vector3_t6_49  L_45 = ___position;
		Quaternion_t6_51  L_46 = ___rotation;
		int32_t L_47 = ___group;
		Int32U5BU5D_t1_160* L_48 = V_2;
		ObjectU5BU5D_t1_157* L_49 = ___data;
		NullCheck(L_43);
		Hashtable_t5_1 * L_50 = NetworkingPeer_SendInstantiate_m8_449(L_43, L_44, L_45, L_46, L_47, L_48, L_49, 0, /*hidden argument*/NULL);
		V_4 = L_50;
		NetworkingPeer_t8_98 * L_51 = ((PhotonNetwork_t8_114_StaticFields*)PhotonNetwork_t8_114_il2cpp_TypeInfo_var->static_fields)->___networkingPeer_4;
		Hashtable_t5_1 * L_52 = V_4;
		NetworkingPeer_t8_98 * L_53 = ((PhotonNetwork_t8_114_StaticFields*)PhotonNetwork_t8_114_il2cpp_TypeInfo_var->static_fields)->___networkingPeer_4;
		NullCheck(L_53);
		PhotonPlayer_t8_102 * L_54 = NetworkingPeer_get_mLocalActor_m8_402(L_53, /*hidden argument*/NULL);
		GameObject_t6_85 * L_55 = V_0;
		NullCheck(L_51);
		GameObject_t6_85 * L_56 = NetworkingPeer_DoInstantiate_m8_450(L_51, L_52, L_54, L_55, /*hidden argument*/NULL);
		return L_56;
	}
}
// UnityEngine.GameObject PhotonNetwork::InstantiateSceneObject(System.String,UnityEngine.Vector3,UnityEngine.Quaternion,System.Int32,System.Object[])
extern const Il2CppType* GameObject_t6_85_0_0_0_var;
extern TypeInfo* PhotonNetwork_t8_114_il2cpp_TypeInfo_var;
extern TypeInfo* ObjectU5BU5D_t1_157_il2cpp_TypeInfo_var;
extern TypeInfo* PeerState_t8_69_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* Type_t_il2cpp_TypeInfo_var;
extern TypeInfo* GameObject_t6_85_il2cpp_TypeInfo_var;
extern TypeInfo* Int32_t1_3_il2cpp_TypeInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisPhotonView_t8_3_m6_1666_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral3446;
extern Il2CppCodeGenString* _stringLiteral3442;
extern Il2CppCodeGenString* _stringLiteral3447;
extern Il2CppCodeGenString* _stringLiteral3443;
extern Il2CppCodeGenString* _stringLiteral3448;
extern Il2CppCodeGenString* _stringLiteral3445;
extern Il2CppCodeGenString* _stringLiteral3449;
extern "C" GameObject_t6_85 * PhotonNetwork_InstantiateSceneObject_m8_702 (Object_t * __this /* static, unused */, String_t* ___prefabName, Vector3_t6_49  ___position, Quaternion_t6_51  ___rotation, int32_t ___group, ObjectU5BU5D_t1_157* ___data, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GameObject_t6_85_0_0_0_var = il2cpp_codegen_type_from_index(1082);
		PhotonNetwork_t8_114_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1058);
		ObjectU5BU5D_t1_157_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(19);
		PeerState_t8_69_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1061);
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(11);
		Type_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3);
		GameObject_t6_85_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1082);
		Int32_t1_3_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(8);
		GameObject_GetComponent_TisPhotonView_t8_3_m6_1666_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483849);
		_stringLiteral3446 = il2cpp_codegen_string_literal_from_index(3446);
		_stringLiteral3442 = il2cpp_codegen_string_literal_from_index(3442);
		_stringLiteral3447 = il2cpp_codegen_string_literal_from_index(3447);
		_stringLiteral3443 = il2cpp_codegen_string_literal_from_index(3443);
		_stringLiteral3448 = il2cpp_codegen_string_literal_from_index(3448);
		_stringLiteral3445 = il2cpp_codegen_string_literal_from_index(3445);
		_stringLiteral3449 = il2cpp_codegen_string_literal_from_index(3449);
		s_Il2CppMethodIntialized = true;
	}
	GameObject_t6_85 * V_0 = {0};
	ComponentU5BU5D_t6_274* V_1 = {0};
	Int32U5BU5D_t1_160* V_2 = {0};
	Hashtable_t5_1 * V_3 = {0};
	{
		IL2CPP_RUNTIME_CLASS_INIT(PhotonNetwork_t8_114_il2cpp_TypeInfo_var);
		bool L_0 = PhotonNetwork_get_connected_m8_603(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_001e;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(PhotonNetwork_t8_114_il2cpp_TypeInfo_var);
		bool L_1 = ((PhotonNetwork_t8_114_StaticFields*)PhotonNetwork_t8_114_il2cpp_TypeInfo_var->static_fields)->___InstantiateInRoomOnly_7;
		if (!L_1)
		{
			goto IL_0051;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(PhotonNetwork_t8_114_il2cpp_TypeInfo_var);
		bool L_2 = PhotonNetwork_get_inRoom_m8_649(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (L_2)
		{
			goto IL_0051;
		}
	}

IL_001e:
	{
		ObjectU5BU5D_t1_157* L_3 = ((ObjectU5BU5D_t1_157*)SZArrayNew(ObjectU5BU5D_t1_157_il2cpp_TypeInfo_var, 4));
		NullCheck(L_3);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_3, 0);
		ArrayElementTypeCheck (L_3, _stringLiteral3446);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_3, 0, sizeof(Object_t *))) = (Object_t *)_stringLiteral3446;
		ObjectU5BU5D_t1_157* L_4 = L_3;
		String_t* L_5 = ___prefabName;
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, 1);
		ArrayElementTypeCheck (L_4, L_5);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_4, 1, sizeof(Object_t *))) = (Object_t *)L_5;
		ObjectU5BU5D_t1_157* L_6 = L_4;
		NullCheck(L_6);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_6, 2);
		ArrayElementTypeCheck (L_6, _stringLiteral3442);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_6, 2, sizeof(Object_t *))) = (Object_t *)_stringLiteral3442;
		ObjectU5BU5D_t1_157* L_7 = L_6;
		IL2CPP_RUNTIME_CLASS_INIT(PhotonNetwork_t8_114_il2cpp_TypeInfo_var);
		int32_t L_8 = PhotonNetwork_get_connectionStateDetailed_m8_607(NULL /*static, unused*/, /*hidden argument*/NULL);
		int32_t L_9 = L_8;
		Object_t * L_10 = Box(PeerState_t8_69_il2cpp_TypeInfo_var, &L_9);
		NullCheck(L_7);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_7, 3);
		ArrayElementTypeCheck (L_7, L_10);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_7, 3, sizeof(Object_t *))) = (Object_t *)L_10;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_11 = String_Concat_m1_421(NULL /*static, unused*/, L_7, /*hidden argument*/NULL);
		Debug_LogError_m6_490(NULL /*static, unused*/, L_11, /*hidden argument*/NULL);
		return (GameObject_t6_85 *)NULL;
	}

IL_0051:
	{
		IL2CPP_RUNTIME_CLASS_INIT(PhotonNetwork_t8_114_il2cpp_TypeInfo_var);
		bool L_12 = PhotonNetwork_get_isMasterClient_m8_648(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (L_12)
		{
			goto IL_0072;
		}
	}
	{
		String_t* L_13 = ___prefabName;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_14 = String_Concat_m1_419(NULL /*static, unused*/, _stringLiteral3446, L_13, _stringLiteral3447, /*hidden argument*/NULL);
		Debug_LogError_m6_490(NULL /*static, unused*/, L_14, /*hidden argument*/NULL);
		return (GameObject_t6_85 *)NULL;
	}

IL_0072:
	{
		IL2CPP_RUNTIME_CLASS_INIT(PhotonNetwork_t8_114_il2cpp_TypeInfo_var);
		bool L_15 = ((PhotonNetwork_t8_114_StaticFields*)PhotonNetwork_t8_114_il2cpp_TypeInfo_var->static_fields)->___UsePrefabCache_13;
		if (!L_15)
		{
			goto IL_008e;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(PhotonNetwork_t8_114_il2cpp_TypeInfo_var);
		Dictionary_2_t1_942 * L_16 = ((PhotonNetwork_t8_114_StaticFields*)PhotonNetwork_t8_114_il2cpp_TypeInfo_var->static_fields)->___PrefabCache_14;
		String_t* L_17 = ___prefabName;
		NullCheck(L_16);
		bool L_18 = (bool)VirtFuncInvoker2< bool, String_t*, GameObject_t6_85 ** >::Invoke(32 /* System.Boolean System.Collections.Generic.Dictionary`2<System.String,UnityEngine.GameObject>::TryGetValue(!0,!1&) */, L_16, L_17, (&V_0));
		if (L_18)
		{
			goto IL_00ba;
		}
	}

IL_008e:
	{
		String_t* L_19 = ___prefabName;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_20 = Type_GetTypeFromHandle_m1_893(NULL /*static, unused*/, LoadTypeToken(GameObject_t6_85_0_0_0_var), /*hidden argument*/NULL);
		Object_t6_5 * L_21 = Resources_Load_m6_409(NULL /*static, unused*/, L_19, L_20, /*hidden argument*/NULL);
		V_0 = ((GameObject_t6_85 *)CastclassSealed(L_21, GameObject_t6_85_il2cpp_TypeInfo_var));
		IL2CPP_RUNTIME_CLASS_INIT(PhotonNetwork_t8_114_il2cpp_TypeInfo_var);
		bool L_22 = ((PhotonNetwork_t8_114_StaticFields*)PhotonNetwork_t8_114_il2cpp_TypeInfo_var->static_fields)->___UsePrefabCache_13;
		if (!L_22)
		{
			goto IL_00ba;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(PhotonNetwork_t8_114_il2cpp_TypeInfo_var);
		Dictionary_2_t1_942 * L_23 = ((PhotonNetwork_t8_114_StaticFields*)PhotonNetwork_t8_114_il2cpp_TypeInfo_var->static_fields)->___PrefabCache_14;
		String_t* L_24 = ___prefabName;
		GameObject_t6_85 * L_25 = V_0;
		NullCheck(L_23);
		VirtActionInvoker2< String_t*, GameObject_t6_85 * >::Invoke(27 /* System.Void System.Collections.Generic.Dictionary`2<System.String,UnityEngine.GameObject>::Add(!0,!1) */, L_23, L_24, L_25);
	}

IL_00ba:
	{
		GameObject_t6_85 * L_26 = V_0;
		bool L_27 = Object_op_Equality_m6_579(NULL /*static, unused*/, L_26, (Object_t6_5 *)NULL, /*hidden argument*/NULL);
		if (!L_27)
		{
			goto IL_00dd;
		}
	}
	{
		String_t* L_28 = ___prefabName;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_29 = String_Concat_m1_419(NULL /*static, unused*/, _stringLiteral3446, L_28, _stringLiteral3443, /*hidden argument*/NULL);
		Debug_LogError_m6_490(NULL /*static, unused*/, L_29, /*hidden argument*/NULL);
		return (GameObject_t6_85 *)NULL;
	}

IL_00dd:
	{
		GameObject_t6_85 * L_30 = V_0;
		NullCheck(L_30);
		PhotonView_t8_3 * L_31 = GameObject_GetComponent_TisPhotonView_t8_3_m6_1666(L_30, /*hidden argument*/GameObject_GetComponent_TisPhotonView_t8_3_m6_1666_MethodInfo_var);
		bool L_32 = Object_op_Equality_m6_579(NULL /*static, unused*/, L_31, (Object_t6_5 *)NULL, /*hidden argument*/NULL);
		if (!L_32)
		{
			goto IL_0105;
		}
	}
	{
		String_t* L_33 = ___prefabName;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_34 = String_Concat_m1_419(NULL /*static, unused*/, _stringLiteral3448, L_33, _stringLiteral3445, /*hidden argument*/NULL);
		Debug_LogError_m6_490(NULL /*static, unused*/, L_34, /*hidden argument*/NULL);
		return (GameObject_t6_85 *)NULL;
	}

IL_0105:
	{
		GameObject_t6_85 * L_35 = V_0;
		PhotonViewU5BU5D_t8_180* L_36 = Extensions_GetPhotonViewsInChildren_m8_291(NULL /*static, unused*/, L_35, /*hidden argument*/NULL);
		V_1 = (ComponentU5BU5D_t6_274*)L_36;
		ComponentU5BU5D_t6_274* L_37 = V_1;
		NullCheck(L_37);
		IL2CPP_RUNTIME_CLASS_INIT(PhotonNetwork_t8_114_il2cpp_TypeInfo_var);
		Int32U5BU5D_t1_160* L_38 = PhotonNetwork_AllocateSceneViewIDs_m8_698(NULL /*static, unused*/, (((int32_t)((int32_t)(((Array_t *)L_37)->max_length)))), /*hidden argument*/NULL);
		V_2 = L_38;
		Int32U5BU5D_t1_160* L_39 = V_2;
		if (L_39)
		{
			goto IL_014e;
		}
	}
	{
		ObjectU5BU5D_t1_157* L_40 = ((ObjectU5BU5D_t1_157*)SZArrayNew(ObjectU5BU5D_t1_157_il2cpp_TypeInfo_var, 4));
		NullCheck(L_40);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_40, 0);
		ArrayElementTypeCheck (L_40, _stringLiteral3446);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_40, 0, sizeof(Object_t *))) = (Object_t *)_stringLiteral3446;
		ObjectU5BU5D_t1_157* L_41 = L_40;
		String_t* L_42 = ___prefabName;
		NullCheck(L_41);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_41, 1);
		ArrayElementTypeCheck (L_41, L_42);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_41, 1, sizeof(Object_t *))) = (Object_t *)L_42;
		ObjectU5BU5D_t1_157* L_43 = L_41;
		NullCheck(L_43);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_43, 2);
		ArrayElementTypeCheck (L_43, _stringLiteral3449);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_43, 2, sizeof(Object_t *))) = (Object_t *)_stringLiteral3449;
		ObjectU5BU5D_t1_157* L_44 = L_43;
		IL2CPP_RUNTIME_CLASS_INIT(PhotonNetwork_t8_114_il2cpp_TypeInfo_var);
		int32_t L_45 = ((PhotonNetwork_t8_114_StaticFields*)PhotonNetwork_t8_114_il2cpp_TypeInfo_var->static_fields)->___MAX_VIEW_IDS_5;
		int32_t L_46 = L_45;
		Object_t * L_47 = Box(Int32_t1_3_il2cpp_TypeInfo_var, &L_46);
		NullCheck(L_44);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_44, 3);
		ArrayElementTypeCheck (L_44, L_47);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_44, 3, sizeof(Object_t *))) = (Object_t *)L_47;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_48 = String_Concat_m1_421(NULL /*static, unused*/, L_44, /*hidden argument*/NULL);
		Debug_LogError_m6_490(NULL /*static, unused*/, L_48, /*hidden argument*/NULL);
		return (GameObject_t6_85 *)NULL;
	}

IL_014e:
	{
		IL2CPP_RUNTIME_CLASS_INIT(PhotonNetwork_t8_114_il2cpp_TypeInfo_var);
		NetworkingPeer_t8_98 * L_49 = ((PhotonNetwork_t8_114_StaticFields*)PhotonNetwork_t8_114_il2cpp_TypeInfo_var->static_fields)->___networkingPeer_4;
		String_t* L_50 = ___prefabName;
		Vector3_t6_49  L_51 = ___position;
		Quaternion_t6_51  L_52 = ___rotation;
		int32_t L_53 = ___group;
		Int32U5BU5D_t1_160* L_54 = V_2;
		ObjectU5BU5D_t1_157* L_55 = ___data;
		NullCheck(L_49);
		Hashtable_t5_1 * L_56 = NetworkingPeer_SendInstantiate_m8_449(L_49, L_50, L_51, L_52, L_53, L_54, L_55, 1, /*hidden argument*/NULL);
		V_3 = L_56;
		NetworkingPeer_t8_98 * L_57 = ((PhotonNetwork_t8_114_StaticFields*)PhotonNetwork_t8_114_il2cpp_TypeInfo_var->static_fields)->___networkingPeer_4;
		Hashtable_t5_1 * L_58 = V_3;
		NetworkingPeer_t8_98 * L_59 = ((PhotonNetwork_t8_114_StaticFields*)PhotonNetwork_t8_114_il2cpp_TypeInfo_var->static_fields)->___networkingPeer_4;
		NullCheck(L_59);
		PhotonPlayer_t8_102 * L_60 = NetworkingPeer_get_mLocalActor_m8_402(L_59, /*hidden argument*/NULL);
		GameObject_t6_85 * L_61 = V_0;
		NullCheck(L_57);
		GameObject_t6_85 * L_62 = NetworkingPeer_DoInstantiate_m8_450(L_57, L_58, L_60, L_61, /*hidden argument*/NULL);
		return L_62;
	}
}
// System.Int32 PhotonNetwork::GetPing()
extern TypeInfo* PhotonNetwork_t8_114_il2cpp_TypeInfo_var;
extern "C" int32_t PhotonNetwork_GetPing_m8_703 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		PhotonNetwork_t8_114_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1058);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(PhotonNetwork_t8_114_il2cpp_TypeInfo_var);
		NetworkingPeer_t8_98 * L_0 = ((PhotonNetwork_t8_114_StaticFields*)PhotonNetwork_t8_114_il2cpp_TypeInfo_var->static_fields)->___networkingPeer_4;
		NullCheck(L_0);
		int32_t L_1 = PhotonPeer_get_RoundTripTime_m5_224(L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Void PhotonNetwork::FetchServerTimestamp()
extern TypeInfo* PhotonNetwork_t8_114_il2cpp_TypeInfo_var;
extern "C" void PhotonNetwork_FetchServerTimestamp_m8_704 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		PhotonNetwork_t8_114_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1058);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(PhotonNetwork_t8_114_il2cpp_TypeInfo_var);
		NetworkingPeer_t8_98 * L_0 = ((PhotonNetwork_t8_114_StaticFields*)PhotonNetwork_t8_114_il2cpp_TypeInfo_var->static_fields)->___networkingPeer_4;
		if (!L_0)
		{
			goto IL_0014;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(PhotonNetwork_t8_114_il2cpp_TypeInfo_var);
		NetworkingPeer_t8_98 * L_1 = ((PhotonNetwork_t8_114_StaticFields*)PhotonNetwork_t8_114_il2cpp_TypeInfo_var->static_fields)->___networkingPeer_4;
		NullCheck(L_1);
		VirtActionInvoker0::Invoke(9 /* System.Void ExitGames.Client.Photon.PhotonPeer::FetchServerTimestamp() */, L_1);
	}

IL_0014:
	{
		return;
	}
}
// System.Void PhotonNetwork::SendOutgoingCommands()
extern TypeInfo* PhotonNetwork_t8_114_il2cpp_TypeInfo_var;
extern "C" void PhotonNetwork_SendOutgoingCommands_m8_705 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		PhotonNetwork_t8_114_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1058);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(PhotonNetwork_t8_114_il2cpp_TypeInfo_var);
		bool L_0 = PhotonNetwork_VerifyCanUseNetwork_m8_675(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (L_0)
		{
			goto IL_000b;
		}
	}
	{
		return;
	}

IL_000b:
	{
		goto IL_0010;
	}

IL_0010:
	{
		IL2CPP_RUNTIME_CLASS_INIT(PhotonNetwork_t8_114_il2cpp_TypeInfo_var);
		NetworkingPeer_t8_98 * L_1 = ((PhotonNetwork_t8_114_StaticFields*)PhotonNetwork_t8_114_il2cpp_TypeInfo_var->static_fields)->___networkingPeer_4;
		NullCheck(L_1);
		bool L_2 = (bool)VirtFuncInvoker0< bool >::Invoke(11 /* System.Boolean ExitGames.Client.Photon.PhotonPeer::SendOutgoingCommands() */, L_1);
		if (L_2)
		{
			goto IL_0010;
		}
	}
	{
		return;
	}
}
// System.Boolean PhotonNetwork::CloseConnection(PhotonPlayer)
extern TypeInfo* PhotonNetwork_t8_114_il2cpp_TypeInfo_var;
extern TypeInfo* RaiseEventOptions_t8_93_il2cpp_TypeInfo_var;
extern TypeInfo* Int32U5BU5D_t1_160_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3450;
extern Il2CppCodeGenString* _stringLiteral3451;
extern "C" bool PhotonNetwork_CloseConnection_m8_706 (Object_t * __this /* static, unused */, PhotonPlayer_t8_102 * ___kickPlayer, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		PhotonNetwork_t8_114_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1058);
		RaiseEventOptions_t8_93_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1104);
		Int32U5BU5D_t1_160_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(47);
		_stringLiteral3450 = il2cpp_codegen_string_literal_from_index(3450);
		_stringLiteral3451 = il2cpp_codegen_string_literal_from_index(3451);
		s_Il2CppMethodIntialized = true;
	}
	RaiseEventOptions_t8_93 * V_0 = {0};
	RaiseEventOptions_t8_93 * V_1 = {0};
	{
		IL2CPP_RUNTIME_CLASS_INIT(PhotonNetwork_t8_114_il2cpp_TypeInfo_var);
		bool L_0 = PhotonNetwork_VerifyCanUseNetwork_m8_675(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (L_0)
		{
			goto IL_000c;
		}
	}
	{
		return 0;
	}

IL_000c:
	{
		IL2CPP_RUNTIME_CLASS_INIT(PhotonNetwork_t8_114_il2cpp_TypeInfo_var);
		PhotonPlayer_t8_102 * L_1 = PhotonNetwork_get_player_m8_612(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_1);
		bool L_2 = PhotonPlayer_get_isMasterClient_m8_733(L_1, /*hidden argument*/NULL);
		if (L_2)
		{
			goto IL_0027;
		}
	}
	{
		Debug_LogError_m6_490(NULL /*static, unused*/, _stringLiteral3450, /*hidden argument*/NULL);
		return 0;
	}

IL_0027:
	{
		PhotonPlayer_t8_102 * L_3 = ___kickPlayer;
		if (L_3)
		{
			goto IL_0039;
		}
	}
	{
		Debug_LogError_m6_490(NULL /*static, unused*/, _stringLiteral3451, /*hidden argument*/NULL);
		return 0;
	}

IL_0039:
	{
		RaiseEventOptions_t8_93 * L_4 = (RaiseEventOptions_t8_93 *)il2cpp_codegen_object_new (RaiseEventOptions_t8_93_il2cpp_TypeInfo_var);
		RaiseEventOptions__ctor_m8_342(L_4, /*hidden argument*/NULL);
		V_1 = L_4;
		RaiseEventOptions_t8_93 * L_5 = V_1;
		Int32U5BU5D_t1_160* L_6 = ((Int32U5BU5D_t1_160*)SZArrayNew(Int32U5BU5D_t1_160_il2cpp_TypeInfo_var, 1));
		PhotonPlayer_t8_102 * L_7 = ___kickPlayer;
		NullCheck(L_7);
		int32_t L_8 = PhotonPlayer_get_ID_m8_730(L_7, /*hidden argument*/NULL);
		NullCheck(L_6);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_6, 0);
		*((int32_t*)(int32_t*)SZArrayLdElema(L_6, 0, sizeof(int32_t))) = (int32_t)L_8;
		NullCheck(L_5);
		L_5->___TargetActors_3 = L_6;
		RaiseEventOptions_t8_93 * L_9 = V_1;
		V_0 = L_9;
		IL2CPP_RUNTIME_CLASS_INIT(PhotonNetwork_t8_114_il2cpp_TypeInfo_var);
		NetworkingPeer_t8_98 * L_10 = ((PhotonNetwork_t8_114_StaticFields*)PhotonNetwork_t8_114_il2cpp_TypeInfo_var->static_fields)->___networkingPeer_4;
		RaiseEventOptions_t8_93 * L_11 = V_0;
		NullCheck(L_10);
		bool L_12 = (bool)VirtFuncInvoker4< bool, uint8_t, Object_t *, bool, RaiseEventOptions_t8_93 * >::Invoke(27 /* System.Boolean NetworkingPeer::OpRaiseEvent(System.Byte,System.Object,System.Boolean,RaiseEventOptions) */, L_10, ((int32_t)203), NULL, 1, L_11);
		return L_12;
	}
}
// System.Boolean PhotonNetwork::SetMasterClient(PhotonPlayer)
extern TypeInfo* PhotonNetwork_t8_114_il2cpp_TypeInfo_var;
extern TypeInfo* Hashtable_t5_1_il2cpp_TypeInfo_var;
extern TypeInfo* Byte_t1_11_il2cpp_TypeInfo_var;
extern TypeInfo* Int32_t1_3_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3452;
extern "C" bool PhotonNetwork_SetMasterClient_m8_707 (Object_t * __this /* static, unused */, PhotonPlayer_t8_102 * ___masterClientPlayer, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		PhotonNetwork_t8_114_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1058);
		Hashtable_t5_1_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(891);
		Byte_t1_11_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(22);
		Int32_t1_3_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(8);
		_stringLiteral3452 = il2cpp_codegen_string_literal_from_index(3452);
		s_Il2CppMethodIntialized = true;
	}
	Hashtable_t5_1 * V_0 = {0};
	Hashtable_t5_1 * V_1 = {0};
	Hashtable_t5_1 * V_2 = {0};
	{
		IL2CPP_RUNTIME_CLASS_INIT(PhotonNetwork_t8_114_il2cpp_TypeInfo_var);
		bool L_0 = PhotonNetwork_get_inRoom_m8_649(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_001e;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(PhotonNetwork_t8_114_il2cpp_TypeInfo_var);
		bool L_1 = PhotonNetwork_VerifyCanUseNetwork_m8_675(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_001e;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(PhotonNetwork_t8_114_il2cpp_TypeInfo_var);
		bool L_2 = PhotonNetwork_get_offlineMode_m8_623(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_0035;
		}
	}

IL_001e:
	{
		IL2CPP_RUNTIME_CLASS_INIT(PhotonNetwork_t8_114_il2cpp_TypeInfo_var);
		int32_t L_3 = ((PhotonNetwork_t8_114_StaticFields*)PhotonNetwork_t8_114_il2cpp_TypeInfo_var->static_fields)->___logLevel_8;
		if ((!(((uint32_t)L_3) == ((uint32_t)1))))
		{
			goto IL_0033;
		}
	}
	{
		Debug_Log_m6_489(NULL /*static, unused*/, _stringLiteral3452, /*hidden argument*/NULL);
	}

IL_0033:
	{
		return 0;
	}

IL_0035:
	{
		IL2CPP_RUNTIME_CLASS_INIT(PhotonNetwork_t8_114_il2cpp_TypeInfo_var);
		Room_t8_100 * L_4 = PhotonNetwork_get_room_m8_611(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_4);
		bool L_5 = RoomInfo_get_serverSideMasterClient_m8_829(L_4, /*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_009c;
		}
	}
	{
		Hashtable_t5_1 * L_6 = (Hashtable_t5_1 *)il2cpp_codegen_object_new (Hashtable_t5_1_il2cpp_TypeInfo_var);
		Hashtable__ctor_m5_0(L_6, /*hidden argument*/NULL);
		V_2 = L_6;
		Hashtable_t5_1 * L_7 = V_2;
		uint8_t L_8 = ((int32_t)248);
		Object_t * L_9 = Box(Byte_t1_11_il2cpp_TypeInfo_var, &L_8);
		PhotonPlayer_t8_102 * L_10 = ___masterClientPlayer;
		NullCheck(L_10);
		int32_t L_11 = PhotonPlayer_get_ID_m8_730(L_10, /*hidden argument*/NULL);
		int32_t L_12 = L_11;
		Object_t * L_13 = Box(Int32_t1_3_il2cpp_TypeInfo_var, &L_12);
		NullCheck(L_7);
		VirtActionInvoker2< Object_t *, Object_t * >::Invoke(27 /* System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Object>::Add(!0,!1) */, L_7, L_9, L_13);
		Hashtable_t5_1 * L_14 = V_2;
		V_0 = L_14;
		Hashtable_t5_1 * L_15 = (Hashtable_t5_1 *)il2cpp_codegen_object_new (Hashtable_t5_1_il2cpp_TypeInfo_var);
		Hashtable__ctor_m5_0(L_15, /*hidden argument*/NULL);
		V_2 = L_15;
		Hashtable_t5_1 * L_16 = V_2;
		uint8_t L_17 = ((int32_t)248);
		Object_t * L_18 = Box(Byte_t1_11_il2cpp_TypeInfo_var, &L_17);
		IL2CPP_RUNTIME_CLASS_INIT(PhotonNetwork_t8_114_il2cpp_TypeInfo_var);
		NetworkingPeer_t8_98 * L_19 = ((PhotonNetwork_t8_114_StaticFields*)PhotonNetwork_t8_114_il2cpp_TypeInfo_var->static_fields)->___networkingPeer_4;
		NullCheck(L_19);
		int32_t L_20 = NetworkingPeer_get_mMasterClientId_m8_404(L_19, /*hidden argument*/NULL);
		int32_t L_21 = L_20;
		Object_t * L_22 = Box(Int32_t1_3_il2cpp_TypeInfo_var, &L_21);
		NullCheck(L_16);
		VirtActionInvoker2< Object_t *, Object_t * >::Invoke(27 /* System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Object>::Add(!0,!1) */, L_16, L_18, L_22);
		Hashtable_t5_1 * L_23 = V_2;
		V_1 = L_23;
		NetworkingPeer_t8_98 * L_24 = ((PhotonNetwork_t8_114_StaticFields*)PhotonNetwork_t8_114_il2cpp_TypeInfo_var->static_fields)->___networkingPeer_4;
		Hashtable_t5_1 * L_25 = V_0;
		Hashtable_t5_1 * L_26 = V_1;
		NullCheck(L_24);
		bool L_27 = LoadbalancingPeer_OpSetPropertiesOfRoom_m8_332(L_24, L_25, L_26, 0, /*hidden argument*/NULL);
		return L_27;
	}

IL_009c:
	{
		IL2CPP_RUNTIME_CLASS_INIT(PhotonNetwork_t8_114_il2cpp_TypeInfo_var);
		bool L_28 = PhotonNetwork_get_isMasterClient_m8_648(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (L_28)
		{
			goto IL_00a8;
		}
	}
	{
		return 0;
	}

IL_00a8:
	{
		IL2CPP_RUNTIME_CLASS_INIT(PhotonNetwork_t8_114_il2cpp_TypeInfo_var);
		NetworkingPeer_t8_98 * L_29 = ((PhotonNetwork_t8_114_StaticFields*)PhotonNetwork_t8_114_il2cpp_TypeInfo_var->static_fields)->___networkingPeer_4;
		PhotonPlayer_t8_102 * L_30 = ___masterClientPlayer;
		NullCheck(L_30);
		int32_t L_31 = PhotonPlayer_get_ID_m8_730(L_30, /*hidden argument*/NULL);
		NullCheck(L_29);
		bool L_32 = NetworkingPeer_SetMasterClient_m8_426(L_29, L_31, 1, /*hidden argument*/NULL);
		return L_32;
	}
}
// System.Void PhotonNetwork::Destroy(PhotonView)
extern TypeInfo* PhotonNetwork_t8_114_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3453;
extern "C" void PhotonNetwork_Destroy_m8_708 (Object_t * __this /* static, unused */, PhotonView_t8_3 * ___targetView, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		PhotonNetwork_t8_114_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1058);
		_stringLiteral3453 = il2cpp_codegen_string_literal_from_index(3453);
		s_Il2CppMethodIntialized = true;
	}
	{
		PhotonView_t8_3 * L_0 = ___targetView;
		bool L_1 = Object_op_Inequality_m6_580(NULL /*static, unused*/, L_0, (Object_t6_5 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0029;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(PhotonNetwork_t8_114_il2cpp_TypeInfo_var);
		NetworkingPeer_t8_98 * L_2 = ((PhotonNetwork_t8_114_StaticFields*)PhotonNetwork_t8_114_il2cpp_TypeInfo_var->static_fields)->___networkingPeer_4;
		PhotonView_t8_3 * L_3 = ___targetView;
		NullCheck(L_3);
		GameObject_t6_85 * L_4 = Component_get_gameObject_m6_583(L_3, /*hidden argument*/NULL);
		bool L_5 = PhotonNetwork_get_inRoom_m8_649(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_2);
		NetworkingPeer_RemoveInstantiatedGO_m8_456(L_2, L_4, ((((int32_t)L_5) == ((int32_t)0))? 1 : 0), /*hidden argument*/NULL);
		goto IL_0033;
	}

IL_0029:
	{
		Debug_LogError_m6_490(NULL /*static, unused*/, _stringLiteral3453, /*hidden argument*/NULL);
	}

IL_0033:
	{
		return;
	}
}
// System.Void PhotonNetwork::Destroy(UnityEngine.GameObject)
extern TypeInfo* PhotonNetwork_t8_114_il2cpp_TypeInfo_var;
extern "C" void PhotonNetwork_Destroy_m8_709 (Object_t * __this /* static, unused */, GameObject_t6_85 * ___targetGo, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		PhotonNetwork_t8_114_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1058);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(PhotonNetwork_t8_114_il2cpp_TypeInfo_var);
		NetworkingPeer_t8_98 * L_0 = ((PhotonNetwork_t8_114_StaticFields*)PhotonNetwork_t8_114_il2cpp_TypeInfo_var->static_fields)->___networkingPeer_4;
		GameObject_t6_85 * L_1 = ___targetGo;
		bool L_2 = PhotonNetwork_get_inRoom_m8_649(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_0);
		NetworkingPeer_RemoveInstantiatedGO_m8_456(L_0, L_1, ((((int32_t)L_2) == ((int32_t)0))? 1 : 0), /*hidden argument*/NULL);
		return;
	}
}
// System.Void PhotonNetwork::DestroyPlayerObjects(PhotonPlayer)
extern TypeInfo* PhotonNetwork_t8_114_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3454;
extern "C" void PhotonNetwork_DestroyPlayerObjects_m8_710 (Object_t * __this /* static, unused */, PhotonPlayer_t8_102 * ___targetPlayer, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		PhotonNetwork_t8_114_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1058);
		_stringLiteral3454 = il2cpp_codegen_string_literal_from_index(3454);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(PhotonNetwork_t8_114_il2cpp_TypeInfo_var);
		PhotonPlayer_t8_102 * L_0 = PhotonNetwork_get_player_m8_612(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (L_0)
		{
			goto IL_0014;
		}
	}
	{
		Debug_LogError_m6_490(NULL /*static, unused*/, _stringLiteral3454, /*hidden argument*/NULL);
	}

IL_0014:
	{
		PhotonPlayer_t8_102 * L_1 = ___targetPlayer;
		NullCheck(L_1);
		int32_t L_2 = PhotonPlayer_get_ID_m8_730(L_1, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(PhotonNetwork_t8_114_il2cpp_TypeInfo_var);
		PhotonNetwork_DestroyPlayerObjects_m8_711(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Void PhotonNetwork::DestroyPlayerObjects(System.Int32)
extern TypeInfo* PhotonNetwork_t8_114_il2cpp_TypeInfo_var;
extern TypeInfo* Boolean_t1_20_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3455;
extern "C" void PhotonNetwork_DestroyPlayerObjects_m8_711 (Object_t * __this /* static, unused */, int32_t ___targetPlayerId, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		PhotonNetwork_t8_114_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1058);
		Boolean_t1_20_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(49);
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(11);
		_stringLiteral3455 = il2cpp_codegen_string_literal_from_index(3455);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(PhotonNetwork_t8_114_il2cpp_TypeInfo_var);
		bool L_0 = PhotonNetwork_VerifyCanUseNetwork_m8_675(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (L_0)
		{
			goto IL_000b;
		}
	}
	{
		return;
	}

IL_000b:
	{
		IL2CPP_RUNTIME_CLASS_INIT(PhotonNetwork_t8_114_il2cpp_TypeInfo_var);
		PhotonPlayer_t8_102 * L_1 = PhotonNetwork_get_player_m8_612(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_1);
		bool L_2 = PhotonPlayer_get_isMasterClient_m8_733(L_1, /*hidden argument*/NULL);
		if (L_2)
		{
			goto IL_002a;
		}
	}
	{
		int32_t L_3 = ___targetPlayerId;
		IL2CPP_RUNTIME_CLASS_INIT(PhotonNetwork_t8_114_il2cpp_TypeInfo_var);
		PhotonPlayer_t8_102 * L_4 = PhotonNetwork_get_player_m8_612(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_4);
		int32_t L_5 = PhotonPlayer_get_ID_m8_730(L_4, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_3) == ((uint32_t)L_5))))
		{
			goto IL_003b;
		}
	}

IL_002a:
	{
		IL2CPP_RUNTIME_CLASS_INIT(PhotonNetwork_t8_114_il2cpp_TypeInfo_var);
		NetworkingPeer_t8_98 * L_6 = ((PhotonNetwork_t8_114_StaticFields*)PhotonNetwork_t8_114_il2cpp_TypeInfo_var->static_fields)->___networkingPeer_4;
		int32_t L_7 = ___targetPlayerId;
		NullCheck(L_6);
		NetworkingPeer_DestroyPlayerObjects_m8_454(L_6, L_7, 0, /*hidden argument*/NULL);
		goto IL_0054;
	}

IL_003b:
	{
		IL2CPP_RUNTIME_CLASS_INIT(PhotonNetwork_t8_114_il2cpp_TypeInfo_var);
		bool L_8 = PhotonNetwork_get_isMasterClient_m8_648(NULL /*static, unused*/, /*hidden argument*/NULL);
		bool L_9 = L_8;
		Object_t * L_10 = Box(Boolean_t1_20_il2cpp_TypeInfo_var, &L_9);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_11 = String_Concat_m1_416(NULL /*static, unused*/, _stringLiteral3455, L_10, /*hidden argument*/NULL);
		Debug_LogError_m6_490(NULL /*static, unused*/, L_11, /*hidden argument*/NULL);
	}

IL_0054:
	{
		return;
	}
}
// System.Void PhotonNetwork::DestroyAll()
extern TypeInfo* PhotonNetwork_t8_114_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3456;
extern "C" void PhotonNetwork_DestroyAll_m8_712 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		PhotonNetwork_t8_114_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1058);
		_stringLiteral3456 = il2cpp_codegen_string_literal_from_index(3456);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(PhotonNetwork_t8_114_il2cpp_TypeInfo_var);
		bool L_0 = PhotonNetwork_get_isMasterClient_m8_648(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_001a;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(PhotonNetwork_t8_114_il2cpp_TypeInfo_var);
		NetworkingPeer_t8_98 * L_1 = ((PhotonNetwork_t8_114_StaticFields*)PhotonNetwork_t8_114_il2cpp_TypeInfo_var->static_fields)->___networkingPeer_4;
		NullCheck(L_1);
		NetworkingPeer_DestroyAll_m8_455(L_1, 0, /*hidden argument*/NULL);
		goto IL_0024;
	}

IL_001a:
	{
		Debug_LogError_m6_490(NULL /*static, unused*/, _stringLiteral3456, /*hidden argument*/NULL);
	}

IL_0024:
	{
		return;
	}
}
// System.Void PhotonNetwork::RemoveRPCs(PhotonPlayer)
extern TypeInfo* PhotonNetwork_t8_114_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3457;
extern "C" void PhotonNetwork_RemoveRPCs_m8_713 (Object_t * __this /* static, unused */, PhotonPlayer_t8_102 * ___targetPlayer, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		PhotonNetwork_t8_114_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1058);
		_stringLiteral3457 = il2cpp_codegen_string_literal_from_index(3457);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(PhotonNetwork_t8_114_il2cpp_TypeInfo_var);
		bool L_0 = PhotonNetwork_VerifyCanUseNetwork_m8_675(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (L_0)
		{
			goto IL_000b;
		}
	}
	{
		return;
	}

IL_000b:
	{
		PhotonPlayer_t8_102 * L_1 = ___targetPlayer;
		NullCheck(L_1);
		bool L_2 = (L_1->___isLocal_2);
		if (L_2)
		{
			goto IL_002b;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(PhotonNetwork_t8_114_il2cpp_TypeInfo_var);
		bool L_3 = PhotonNetwork_get_isMasterClient_m8_648(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (L_3)
		{
			goto IL_002b;
		}
	}
	{
		Debug_LogError_m6_490(NULL /*static, unused*/, _stringLiteral3457, /*hidden argument*/NULL);
		return;
	}

IL_002b:
	{
		IL2CPP_RUNTIME_CLASS_INIT(PhotonNetwork_t8_114_il2cpp_TypeInfo_var);
		NetworkingPeer_t8_98 * L_4 = ((PhotonNetwork_t8_114_StaticFields*)PhotonNetwork_t8_114_il2cpp_TypeInfo_var->static_fields)->___networkingPeer_4;
		PhotonPlayer_t8_102 * L_5 = ___targetPlayer;
		NullCheck(L_5);
		int32_t L_6 = PhotonPlayer_get_ID_m8_730(L_5, /*hidden argument*/NULL);
		NullCheck(L_4);
		NetworkingPeer_OpCleanRpcBuffer_m8_467(L_4, L_6, /*hidden argument*/NULL);
		return;
	}
}
// System.Void PhotonNetwork::RemoveRPCs(PhotonView)
extern TypeInfo* PhotonNetwork_t8_114_il2cpp_TypeInfo_var;
extern "C" void PhotonNetwork_RemoveRPCs_m8_714 (Object_t * __this /* static, unused */, PhotonView_t8_3 * ___targetPhotonView, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		PhotonNetwork_t8_114_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1058);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(PhotonNetwork_t8_114_il2cpp_TypeInfo_var);
		bool L_0 = PhotonNetwork_VerifyCanUseNetwork_m8_675(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (L_0)
		{
			goto IL_000b;
		}
	}
	{
		return;
	}

IL_000b:
	{
		IL2CPP_RUNTIME_CLASS_INIT(PhotonNetwork_t8_114_il2cpp_TypeInfo_var);
		NetworkingPeer_t8_98 * L_1 = ((PhotonNetwork_t8_114_StaticFields*)PhotonNetwork_t8_114_il2cpp_TypeInfo_var->static_fields)->___networkingPeer_4;
		PhotonView_t8_3 * L_2 = ___targetPhotonView;
		NullCheck(L_1);
		NetworkingPeer_CleanRpcBufferIfMine_m8_471(L_1, L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Void PhotonNetwork::RemoveRPCsInGroup(System.Int32)
extern TypeInfo* PhotonNetwork_t8_114_il2cpp_TypeInfo_var;
extern "C" void PhotonNetwork_RemoveRPCsInGroup_m8_715 (Object_t * __this /* static, unused */, int32_t ___targetGroup, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		PhotonNetwork_t8_114_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1058);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(PhotonNetwork_t8_114_il2cpp_TypeInfo_var);
		bool L_0 = PhotonNetwork_VerifyCanUseNetwork_m8_675(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (L_0)
		{
			goto IL_000b;
		}
	}
	{
		return;
	}

IL_000b:
	{
		IL2CPP_RUNTIME_CLASS_INIT(PhotonNetwork_t8_114_il2cpp_TypeInfo_var);
		NetworkingPeer_t8_98 * L_1 = ((PhotonNetwork_t8_114_StaticFields*)PhotonNetwork_t8_114_il2cpp_TypeInfo_var->static_fields)->___networkingPeer_4;
		int32_t L_2 = ___targetGroup;
		NullCheck(L_1);
		NetworkingPeer_RemoveRPCsInGroup_m8_473(L_1, L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Void PhotonNetwork::RPC(PhotonView,System.String,PhotonTargets,System.Boolean,System.Object[])
extern TypeInfo* PhotonNetwork_t8_114_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3458;
extern Il2CppCodeGenString* _stringLiteral3459;
extern Il2CppCodeGenString* _stringLiteral3460;
extern "C" void PhotonNetwork_RPC_m8_716 (Object_t * __this /* static, unused */, PhotonView_t8_3 * ___view, String_t* ___methodName, int32_t ___target, bool ___encrypt, ObjectU5BU5D_t1_157* ___parameters, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		PhotonNetwork_t8_114_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1058);
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(11);
		_stringLiteral3458 = il2cpp_codegen_string_literal_from_index(3458);
		_stringLiteral3459 = il2cpp_codegen_string_literal_from_index(3459);
		_stringLiteral3460 = il2cpp_codegen_string_literal_from_index(3460);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(PhotonNetwork_t8_114_il2cpp_TypeInfo_var);
		bool L_0 = PhotonNetwork_VerifyCanUseNetwork_m8_675(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (L_0)
		{
			goto IL_000b;
		}
	}
	{
		return;
	}

IL_000b:
	{
		IL2CPP_RUNTIME_CLASS_INIT(PhotonNetwork_t8_114_il2cpp_TypeInfo_var);
		Room_t8_100 * L_1 = PhotonNetwork_get_room_m8_611(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (L_1)
		{
			goto IL_0020;
		}
	}
	{
		Debug_LogWarning_m6_491(NULL /*static, unused*/, _stringLiteral3458, /*hidden argument*/NULL);
		return;
	}

IL_0020:
	{
		IL2CPP_RUNTIME_CLASS_INIT(PhotonNetwork_t8_114_il2cpp_TypeInfo_var);
		NetworkingPeer_t8_98 * L_2 = ((PhotonNetwork_t8_114_StaticFields*)PhotonNetwork_t8_114_il2cpp_TypeInfo_var->static_fields)->___networkingPeer_4;
		if (!L_2)
		{
			goto IL_006e;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(PhotonNetwork_t8_114_il2cpp_TypeInfo_var);
		NetworkingPeer_t8_98 * L_3 = ((PhotonNetwork_t8_114_StaticFields*)PhotonNetwork_t8_114_il2cpp_TypeInfo_var->static_fields)->___networkingPeer_4;
		NullCheck(L_3);
		bool L_4 = (L_3->___hasSwitchedMC_28);
		if (!L_4)
		{
			goto IL_0059;
		}
	}
	{
		int32_t L_5 = ___target;
		if ((!(((uint32_t)L_5) == ((uint32_t)2))))
		{
			goto IL_0059;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(PhotonNetwork_t8_114_il2cpp_TypeInfo_var);
		NetworkingPeer_t8_98 * L_6 = ((PhotonNetwork_t8_114_StaticFields*)PhotonNetwork_t8_114_il2cpp_TypeInfo_var->static_fields)->___networkingPeer_4;
		PhotonView_t8_3 * L_7 = ___view;
		String_t* L_8 = ___methodName;
		PhotonPlayer_t8_102 * L_9 = PhotonNetwork_get_masterClient_m8_613(NULL /*static, unused*/, /*hidden argument*/NULL);
		bool L_10 = ___encrypt;
		ObjectU5BU5D_t1_157* L_11 = ___parameters;
		NullCheck(L_6);
		NetworkingPeer_RPC_m8_475(L_6, L_7, L_8, L_9, L_10, L_11, /*hidden argument*/NULL);
		goto IL_0069;
	}

IL_0059:
	{
		IL2CPP_RUNTIME_CLASS_INIT(PhotonNetwork_t8_114_il2cpp_TypeInfo_var);
		NetworkingPeer_t8_98 * L_12 = ((PhotonNetwork_t8_114_StaticFields*)PhotonNetwork_t8_114_il2cpp_TypeInfo_var->static_fields)->___networkingPeer_4;
		PhotonView_t8_3 * L_13 = ___view;
		String_t* L_14 = ___methodName;
		int32_t L_15 = ___target;
		bool L_16 = ___encrypt;
		ObjectU5BU5D_t1_157* L_17 = ___parameters;
		NullCheck(L_12);
		NetworkingPeer_RPC_m8_476(L_12, L_13, L_14, L_15, L_16, L_17, /*hidden argument*/NULL);
	}

IL_0069:
	{
		goto IL_0083;
	}

IL_006e:
	{
		String_t* L_18 = ___methodName;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_19 = String_Concat_m1_419(NULL /*static, unused*/, _stringLiteral3459, L_18, _stringLiteral3460, /*hidden argument*/NULL);
		Debug_LogWarning_m6_491(NULL /*static, unused*/, L_19, /*hidden argument*/NULL);
	}

IL_0083:
	{
		return;
	}
}
// System.Void PhotonNetwork::RPC(PhotonView,System.String,PhotonPlayer,System.Boolean,System.Object[])
extern TypeInfo* PhotonNetwork_t8_114_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3461;
extern Il2CppCodeGenString* _stringLiteral3462;
extern Il2CppCodeGenString* _stringLiteral431;
extern Il2CppCodeGenString* _stringLiteral3459;
extern Il2CppCodeGenString* _stringLiteral3460;
extern "C" void PhotonNetwork_RPC_m8_717 (Object_t * __this /* static, unused */, PhotonView_t8_3 * ___view, String_t* ___methodName, PhotonPlayer_t8_102 * ___targetPlayer, bool ___encrpyt, ObjectU5BU5D_t1_157* ___parameters, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		PhotonNetwork_t8_114_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1058);
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(11);
		_stringLiteral3461 = il2cpp_codegen_string_literal_from_index(3461);
		_stringLiteral3462 = il2cpp_codegen_string_literal_from_index(3462);
		_stringLiteral431 = il2cpp_codegen_string_literal_from_index(431);
		_stringLiteral3459 = il2cpp_codegen_string_literal_from_index(3459);
		_stringLiteral3460 = il2cpp_codegen_string_literal_from_index(3460);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(PhotonNetwork_t8_114_il2cpp_TypeInfo_var);
		bool L_0 = PhotonNetwork_VerifyCanUseNetwork_m8_675(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (L_0)
		{
			goto IL_000b;
		}
	}
	{
		return;
	}

IL_000b:
	{
		IL2CPP_RUNTIME_CLASS_INIT(PhotonNetwork_t8_114_il2cpp_TypeInfo_var);
		Room_t8_100 * L_1 = PhotonNetwork_get_room_m8_611(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (L_1)
		{
			goto IL_0020;
		}
	}
	{
		Debug_LogWarning_m6_491(NULL /*static, unused*/, _stringLiteral3461, /*hidden argument*/NULL);
		return;
	}

IL_0020:
	{
		IL2CPP_RUNTIME_CLASS_INIT(PhotonNetwork_t8_114_il2cpp_TypeInfo_var);
		PhotonPlayer_t8_102 * L_2 = PhotonNetwork_get_player_m8_612(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (L_2)
		{
			goto IL_003f;
		}
	}
	{
		String_t* L_3 = ___methodName;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_4 = String_Concat_m1_419(NULL /*static, unused*/, _stringLiteral3462, L_3, _stringLiteral431, /*hidden argument*/NULL);
		Debug_LogError_m6_490(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
	}

IL_003f:
	{
		IL2CPP_RUNTIME_CLASS_INIT(PhotonNetwork_t8_114_il2cpp_TypeInfo_var);
		NetworkingPeer_t8_98 * L_5 = ((PhotonNetwork_t8_114_StaticFields*)PhotonNetwork_t8_114_il2cpp_TypeInfo_var->static_fields)->___networkingPeer_4;
		if (!L_5)
		{
			goto IL_005e;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(PhotonNetwork_t8_114_il2cpp_TypeInfo_var);
		NetworkingPeer_t8_98 * L_6 = ((PhotonNetwork_t8_114_StaticFields*)PhotonNetwork_t8_114_il2cpp_TypeInfo_var->static_fields)->___networkingPeer_4;
		PhotonView_t8_3 * L_7 = ___view;
		String_t* L_8 = ___methodName;
		PhotonPlayer_t8_102 * L_9 = ___targetPlayer;
		bool L_10 = ___encrpyt;
		ObjectU5BU5D_t1_157* L_11 = ___parameters;
		NullCheck(L_6);
		NetworkingPeer_RPC_m8_475(L_6, L_7, L_8, L_9, L_10, L_11, /*hidden argument*/NULL);
		goto IL_0073;
	}

IL_005e:
	{
		String_t* L_12 = ___methodName;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_13 = String_Concat_m1_419(NULL /*static, unused*/, _stringLiteral3459, L_12, _stringLiteral3460, /*hidden argument*/NULL);
		Debug_LogWarning_m6_491(NULL /*static, unused*/, L_13, /*hidden argument*/NULL);
	}

IL_0073:
	{
		return;
	}
}
// System.Void PhotonNetwork::CacheSendMonoMessageTargets(System.Type)
extern TypeInfo* PhotonNetwork_t8_114_il2cpp_TypeInfo_var;
extern "C" void PhotonNetwork_CacheSendMonoMessageTargets_m8_718 (Object_t * __this /* static, unused */, Type_t * ___type, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		PhotonNetwork_t8_114_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1058);
		s_Il2CppMethodIntialized = true;
	}
	{
		Type_t * L_0 = ___type;
		if (L_0)
		{
			goto IL_000d;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(PhotonNetwork_t8_114_il2cpp_TypeInfo_var);
		Type_t * L_1 = ((PhotonNetwork_t8_114_StaticFields*)PhotonNetwork_t8_114_il2cpp_TypeInfo_var->static_fields)->___SendMonoMessageTargetType_16;
		___type = L_1;
	}

IL_000d:
	{
		Type_t * L_2 = ___type;
		IL2CPP_RUNTIME_CLASS_INIT(PhotonNetwork_t8_114_il2cpp_TypeInfo_var);
		HashSet_1_t2_16 * L_3 = PhotonNetwork_FindGameObjectsWithComponent_m8_719(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		((PhotonNetwork_t8_114_StaticFields*)PhotonNetwork_t8_114_il2cpp_TypeInfo_var->static_fields)->___SendMonoMessageTargets_15 = L_3;
		return;
	}
}
// System.Collections.Generic.HashSet`1<UnityEngine.GameObject> PhotonNetwork::FindGameObjectsWithComponent(System.Type)
extern TypeInfo* HashSet_1_t2_16_il2cpp_TypeInfo_var;
extern TypeInfo* ComponentU5BU5D_t6_274_il2cpp_TypeInfo_var;
extern const MethodInfo* HashSet_1__ctor_m2_52_MethodInfo_var;
extern const MethodInfo* HashSet_1_Add_m2_53_MethodInfo_var;
extern "C" HashSet_1_t2_16 * PhotonNetwork_FindGameObjectsWithComponent_m8_719 (Object_t * __this /* static, unused */, Type_t * ___type, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		HashSet_1_t2_16_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1125);
		ComponentU5BU5D_t6_274_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1158);
		HashSet_1__ctor_m2_52_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483894);
		HashSet_1_Add_m2_53_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483898);
		s_Il2CppMethodIntialized = true;
	}
	HashSet_1_t2_16 * V_0 = {0};
	ComponentU5BU5D_t6_274* V_1 = {0};
	int32_t V_2 = 0;
	{
		HashSet_1_t2_16 * L_0 = (HashSet_1_t2_16 *)il2cpp_codegen_object_new (HashSet_1_t2_16_il2cpp_TypeInfo_var);
		HashSet_1__ctor_m2_52(L_0, /*hidden argument*/HashSet_1__ctor_m2_52_MethodInfo_var);
		V_0 = L_0;
		Type_t * L_1 = ___type;
		ObjectU5BU5D_t6_252* L_2 = Object_FindObjectsOfType_m6_561(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		V_1 = ((ComponentU5BU5D_t6_274*)Castclass(L_2, ComponentU5BU5D_t6_274_il2cpp_TypeInfo_var));
		V_2 = 0;
		goto IL_002c;
	}

IL_0019:
	{
		HashSet_1_t2_16 * L_3 = V_0;
		ComponentU5BU5D_t6_274* L_4 = V_1;
		int32_t L_5 = V_2;
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, L_5);
		int32_t L_6 = L_5;
		NullCheck((*(Component_t6_26 **)(Component_t6_26 **)SZArrayLdElema(L_4, L_6, sizeof(Component_t6_26 *))));
		GameObject_t6_85 * L_7 = Component_get_gameObject_m6_583((*(Component_t6_26 **)(Component_t6_26 **)SZArrayLdElema(L_4, L_6, sizeof(Component_t6_26 *))), /*hidden argument*/NULL);
		NullCheck(L_3);
		HashSet_1_Add_m2_53(L_3, L_7, /*hidden argument*/HashSet_1_Add_m2_53_MethodInfo_var);
		int32_t L_8 = V_2;
		V_2 = ((int32_t)((int32_t)L_8+(int32_t)1));
	}

IL_002c:
	{
		int32_t L_9 = V_2;
		ComponentU5BU5D_t6_274* L_10 = V_1;
		NullCheck(L_10);
		if ((((int32_t)L_9) < ((int32_t)(((int32_t)((int32_t)(((Array_t *)L_10)->max_length)))))))
		{
			goto IL_0019;
		}
	}
	{
		HashSet_1_t2_16 * L_11 = V_0;
		return L_11;
	}
}
// System.Void PhotonNetwork::SetReceivingEnabled(System.Int32,System.Boolean)
extern TypeInfo* PhotonNetwork_t8_114_il2cpp_TypeInfo_var;
extern "C" void PhotonNetwork_SetReceivingEnabled_m8_720 (Object_t * __this /* static, unused */, int32_t ___group, bool ___enabled, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		PhotonNetwork_t8_114_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1058);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(PhotonNetwork_t8_114_il2cpp_TypeInfo_var);
		bool L_0 = PhotonNetwork_VerifyCanUseNetwork_m8_675(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (L_0)
		{
			goto IL_000b;
		}
	}
	{
		return;
	}

IL_000b:
	{
		IL2CPP_RUNTIME_CLASS_INIT(PhotonNetwork_t8_114_il2cpp_TypeInfo_var);
		NetworkingPeer_t8_98 * L_1 = ((PhotonNetwork_t8_114_StaticFields*)PhotonNetwork_t8_114_il2cpp_TypeInfo_var->static_fields)->___networkingPeer_4;
		int32_t L_2 = ___group;
		bool L_3 = ___enabled;
		NullCheck(L_1);
		NetworkingPeer_SetReceivingEnabled_m8_477(L_1, L_2, L_3, /*hidden argument*/NULL);
		return;
	}
}
// System.Void PhotonNetwork::SetReceivingEnabled(System.Int32[],System.Int32[])
extern TypeInfo* PhotonNetwork_t8_114_il2cpp_TypeInfo_var;
extern "C" void PhotonNetwork_SetReceivingEnabled_m8_721 (Object_t * __this /* static, unused */, Int32U5BU5D_t1_160* ___enableGroups, Int32U5BU5D_t1_160* ___disableGroups, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		PhotonNetwork_t8_114_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1058);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(PhotonNetwork_t8_114_il2cpp_TypeInfo_var);
		bool L_0 = PhotonNetwork_VerifyCanUseNetwork_m8_675(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (L_0)
		{
			goto IL_000b;
		}
	}
	{
		return;
	}

IL_000b:
	{
		IL2CPP_RUNTIME_CLASS_INIT(PhotonNetwork_t8_114_il2cpp_TypeInfo_var);
		NetworkingPeer_t8_98 * L_1 = ((PhotonNetwork_t8_114_StaticFields*)PhotonNetwork_t8_114_il2cpp_TypeInfo_var->static_fields)->___networkingPeer_4;
		Int32U5BU5D_t1_160* L_2 = ___enableGroups;
		Int32U5BU5D_t1_160* L_3 = ___disableGroups;
		NullCheck(L_1);
		NetworkingPeer_SetReceivingEnabled_m8_478(L_1, L_2, L_3, /*hidden argument*/NULL);
		return;
	}
}
// System.Void PhotonNetwork::SetSendingEnabled(System.Int32,System.Boolean)
extern TypeInfo* PhotonNetwork_t8_114_il2cpp_TypeInfo_var;
extern "C" void PhotonNetwork_SetSendingEnabled_m8_722 (Object_t * __this /* static, unused */, int32_t ___group, bool ___enabled, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		PhotonNetwork_t8_114_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1058);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(PhotonNetwork_t8_114_il2cpp_TypeInfo_var);
		bool L_0 = PhotonNetwork_VerifyCanUseNetwork_m8_675(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (L_0)
		{
			goto IL_000b;
		}
	}
	{
		return;
	}

IL_000b:
	{
		IL2CPP_RUNTIME_CLASS_INIT(PhotonNetwork_t8_114_il2cpp_TypeInfo_var);
		NetworkingPeer_t8_98 * L_1 = ((PhotonNetwork_t8_114_StaticFields*)PhotonNetwork_t8_114_il2cpp_TypeInfo_var->static_fields)->___networkingPeer_4;
		int32_t L_2 = ___group;
		bool L_3 = ___enabled;
		NullCheck(L_1);
		NetworkingPeer_SetSendingEnabled_m8_479(L_1, L_2, L_3, /*hidden argument*/NULL);
		return;
	}
}
// System.Void PhotonNetwork::SetSendingEnabled(System.Int32[],System.Int32[])
extern TypeInfo* PhotonNetwork_t8_114_il2cpp_TypeInfo_var;
extern "C" void PhotonNetwork_SetSendingEnabled_m8_723 (Object_t * __this /* static, unused */, Int32U5BU5D_t1_160* ___enableGroups, Int32U5BU5D_t1_160* ___disableGroups, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		PhotonNetwork_t8_114_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1058);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(PhotonNetwork_t8_114_il2cpp_TypeInfo_var);
		bool L_0 = PhotonNetwork_VerifyCanUseNetwork_m8_675(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (L_0)
		{
			goto IL_000b;
		}
	}
	{
		return;
	}

IL_000b:
	{
		IL2CPP_RUNTIME_CLASS_INIT(PhotonNetwork_t8_114_il2cpp_TypeInfo_var);
		NetworkingPeer_t8_98 * L_1 = ((PhotonNetwork_t8_114_StaticFields*)PhotonNetwork_t8_114_il2cpp_TypeInfo_var->static_fields)->___networkingPeer_4;
		Int32U5BU5D_t1_160* L_2 = ___enableGroups;
		Int32U5BU5D_t1_160* L_3 = ___disableGroups;
		NullCheck(L_1);
		NetworkingPeer_SetSendingEnabled_m8_480(L_1, L_2, L_3, /*hidden argument*/NULL);
		return;
	}
}
// System.Void PhotonNetwork::SetLevelPrefix(System.Int16)
extern TypeInfo* PhotonNetwork_t8_114_il2cpp_TypeInfo_var;
extern "C" void PhotonNetwork_SetLevelPrefix_m8_724 (Object_t * __this /* static, unused */, int16_t ___prefix, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		PhotonNetwork_t8_114_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1058);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(PhotonNetwork_t8_114_il2cpp_TypeInfo_var);
		bool L_0 = PhotonNetwork_VerifyCanUseNetwork_m8_675(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (L_0)
		{
			goto IL_000b;
		}
	}
	{
		return;
	}

IL_000b:
	{
		IL2CPP_RUNTIME_CLASS_INIT(PhotonNetwork_t8_114_il2cpp_TypeInfo_var);
		NetworkingPeer_t8_98 * L_1 = ((PhotonNetwork_t8_114_StaticFields*)PhotonNetwork_t8_114_il2cpp_TypeInfo_var->static_fields)->___networkingPeer_4;
		int16_t L_2 = ___prefix;
		NullCheck(L_1);
		NetworkingPeer_SetLevelPrefix_m8_474(L_1, L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Void PhotonNetwork::LoadLevel(System.Int32)
extern TypeInfo* PhotonNetwork_t8_114_il2cpp_TypeInfo_var;
extern TypeInfo* Int32_t1_3_il2cpp_TypeInfo_var;
extern "C" void PhotonNetwork_LoadLevel_m8_725 (Object_t * __this /* static, unused */, int32_t ___levelNumber, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		PhotonNetwork_t8_114_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1058);
		Int32_t1_3_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(8);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(PhotonNetwork_t8_114_il2cpp_TypeInfo_var);
		NetworkingPeer_t8_98 * L_0 = ((PhotonNetwork_t8_114_StaticFields*)PhotonNetwork_t8_114_il2cpp_TypeInfo_var->static_fields)->___networkingPeer_4;
		int32_t L_1 = ___levelNumber;
		int32_t L_2 = L_1;
		Object_t * L_3 = Box(Int32_t1_3_il2cpp_TypeInfo_var, &L_2);
		NullCheck(L_0);
		NetworkingPeer_SetLevelInPropsIfSynced_m8_491(L_0, L_3, /*hidden argument*/NULL);
		PhotonNetwork_set_isMessageQueueRunning_m8_643(NULL /*static, unused*/, 0, /*hidden argument*/NULL);
		NetworkingPeer_t8_98 * L_4 = ((PhotonNetwork_t8_114_StaticFields*)PhotonNetwork_t8_114_il2cpp_TypeInfo_var->static_fields)->___networkingPeer_4;
		NullCheck(L_4);
		L_4->___loadingLevelAndPausedNetwork_35 = 1;
		int32_t L_5 = ___levelNumber;
		Application_LoadLevel_m6_448(NULL /*static, unused*/, L_5, /*hidden argument*/NULL);
		return;
	}
}
// System.Void PhotonNetwork::LoadLevel(System.String)
extern TypeInfo* PhotonNetwork_t8_114_il2cpp_TypeInfo_var;
extern "C" void PhotonNetwork_LoadLevel_m8_726 (Object_t * __this /* static, unused */, String_t* ___levelName, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		PhotonNetwork_t8_114_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1058);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(PhotonNetwork_t8_114_il2cpp_TypeInfo_var);
		NetworkingPeer_t8_98 * L_0 = ((PhotonNetwork_t8_114_StaticFields*)PhotonNetwork_t8_114_il2cpp_TypeInfo_var->static_fields)->___networkingPeer_4;
		String_t* L_1 = ___levelName;
		NullCheck(L_0);
		NetworkingPeer_SetLevelInPropsIfSynced_m8_491(L_0, L_1, /*hidden argument*/NULL);
		PhotonNetwork_set_isMessageQueueRunning_m8_643(NULL /*static, unused*/, 0, /*hidden argument*/NULL);
		NetworkingPeer_t8_98 * L_2 = ((PhotonNetwork_t8_114_StaticFields*)PhotonNetwork_t8_114_il2cpp_TypeInfo_var->static_fields)->___networkingPeer_4;
		NullCheck(L_2);
		L_2->___loadingLevelAndPausedNetwork_35 = 1;
		String_t* L_3 = ___levelName;
		Application_LoadLevel_m6_449(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean PhotonNetwork::WebRpc(System.String,System.Object)
extern TypeInfo* PhotonNetwork_t8_114_il2cpp_TypeInfo_var;
extern "C" bool PhotonNetwork_WebRpc_m8_727 (Object_t * __this /* static, unused */, String_t* ___name, Object_t * ___parameters, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		PhotonNetwork_t8_114_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1058);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(PhotonNetwork_t8_114_il2cpp_TypeInfo_var);
		NetworkingPeer_t8_98 * L_0 = ((PhotonNetwork_t8_114_StaticFields*)PhotonNetwork_t8_114_il2cpp_TypeInfo_var->static_fields)->___networkingPeer_4;
		String_t* L_1 = ___name;
		Object_t * L_2 = ___parameters;
		NullCheck(L_0);
		bool L_3 = NetworkingPeer_WebRpc_m8_493(L_0, L_1, L_2, /*hidden argument*/NULL);
		return L_3;
	}
}
// System.Void PhotonPlayer::.ctor(System.Boolean,System.Int32,System.String)
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* Hashtable_t5_1_il2cpp_TypeInfo_var;
extern "C" void PhotonPlayer__ctor_m8_728 (PhotonPlayer_t8_102 * __this, bool ___isLocal, int32_t ___actorID, String_t* ___name, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(11);
		Hashtable_t5_1_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(891);
		s_Il2CppMethodIntialized = true;
	}
	{
		__this->___actorID_0 = (-1);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_0 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->___Empty_2;
		__this->___nameField_1 = L_0;
		Object__ctor_m1_0(__this, /*hidden argument*/NULL);
		Hashtable_t5_1 * L_1 = (Hashtable_t5_1 *)il2cpp_codegen_object_new (Hashtable_t5_1_il2cpp_TypeInfo_var);
		Hashtable__ctor_m5_0(L_1, /*hidden argument*/NULL);
		PhotonPlayer_set_customProperties_m8_735(__this, L_1, /*hidden argument*/NULL);
		bool L_2 = ___isLocal;
		__this->___isLocal_2 = L_2;
		int32_t L_3 = ___actorID;
		__this->___actorID_0 = L_3;
		String_t* L_4 = ___name;
		__this->___nameField_1 = L_4;
		return;
	}
}
// System.Void PhotonPlayer::.ctor(System.Boolean,System.Int32,ExitGames.Client.Photon.Hashtable)
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* Hashtable_t5_1_il2cpp_TypeInfo_var;
extern "C" void PhotonPlayer__ctor_m8_729 (PhotonPlayer_t8_102 * __this, bool ___isLocal, int32_t ___actorID, Hashtable_t5_1 * ___properties, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(11);
		Hashtable_t5_1_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(891);
		s_Il2CppMethodIntialized = true;
	}
	{
		__this->___actorID_0 = (-1);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_0 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->___Empty_2;
		__this->___nameField_1 = L_0;
		Object__ctor_m1_0(__this, /*hidden argument*/NULL);
		Hashtable_t5_1 * L_1 = (Hashtable_t5_1 *)il2cpp_codegen_object_new (Hashtable_t5_1_il2cpp_TypeInfo_var);
		Hashtable__ctor_m5_0(L_1, /*hidden argument*/NULL);
		PhotonPlayer_set_customProperties_m8_735(__this, L_1, /*hidden argument*/NULL);
		bool L_2 = ___isLocal;
		__this->___isLocal_2 = L_2;
		int32_t L_3 = ___actorID;
		__this->___actorID_0 = L_3;
		Hashtable_t5_1 * L_4 = ___properties;
		PhotonPlayer_InternalCacheProperties_m8_740(__this, L_4, /*hidden argument*/NULL);
		return;
	}
}
// System.Int32 PhotonPlayer::get_ID()
extern "C" int32_t PhotonPlayer_get_ID_m8_730 (PhotonPlayer_t8_102 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (__this->___actorID_0);
		return L_0;
	}
}
// System.String PhotonPlayer::get_name()
extern "C" String_t* PhotonPlayer_get_name_m8_731 (PhotonPlayer_t8_102 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = (__this->___nameField_1);
		return L_0;
	}
}
// System.Void PhotonPlayer::set_name(System.String)
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* PhotonNetwork_t8_114_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3463;
extern "C" void PhotonPlayer_set_name_m8_732 (PhotonPlayer_t8_102 * __this, String_t* ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(11);
		PhotonNetwork_t8_114_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1058);
		_stringLiteral3463 = il2cpp_codegen_string_literal_from_index(3463);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool L_0 = (__this->___isLocal_2);
		if (L_0)
		{
			goto IL_0016;
		}
	}
	{
		Debug_LogError_m6_490(NULL /*static, unused*/, _stringLiteral3463, /*hidden argument*/NULL);
		return;
	}

IL_0016:
	{
		String_t* L_1 = ___value;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_2 = String_IsNullOrEmpty_m1_397(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		if (L_2)
		{
			goto IL_0032;
		}
	}
	{
		String_t* L_3 = ___value;
		String_t* L_4 = (__this->___nameField_1);
		NullCheck(L_3);
		bool L_5 = String_Equals_m1_340(L_3, L_4, /*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_0033;
		}
	}

IL_0032:
	{
		return;
	}

IL_0033:
	{
		String_t* L_6 = ___value;
		__this->___nameField_1 = L_6;
		String_t* L_7 = ___value;
		IL2CPP_RUNTIME_CLASS_INIT(PhotonNetwork_t8_114_il2cpp_TypeInfo_var);
		PhotonNetwork_set_playerName_m8_615(NULL /*static, unused*/, L_7, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean PhotonPlayer::get_isMasterClient()
extern TypeInfo* PhotonNetwork_t8_114_il2cpp_TypeInfo_var;
extern "C" bool PhotonPlayer_get_isMasterClient_m8_733 (PhotonPlayer_t8_102 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		PhotonNetwork_t8_114_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1058);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(PhotonNetwork_t8_114_il2cpp_TypeInfo_var);
		NetworkingPeer_t8_98 * L_0 = ((PhotonNetwork_t8_114_StaticFields*)PhotonNetwork_t8_114_il2cpp_TypeInfo_var->static_fields)->___networkingPeer_4;
		NullCheck(L_0);
		int32_t L_1 = NetworkingPeer_get_mMasterClientId_m8_404(L_0, /*hidden argument*/NULL);
		int32_t L_2 = PhotonPlayer_get_ID_m8_730(__this, /*hidden argument*/NULL);
		return ((((int32_t)L_1) == ((int32_t)L_2))? 1 : 0);
	}
}
// ExitGames.Client.Photon.Hashtable PhotonPlayer::get_customProperties()
extern "C" Hashtable_t5_1 * PhotonPlayer_get_customProperties_m8_734 (PhotonPlayer_t8_102 * __this, const MethodInfo* method)
{
	{
		Hashtable_t5_1 * L_0 = (__this->___U3CcustomPropertiesU3Ek__BackingField_4);
		return L_0;
	}
}
// System.Void PhotonPlayer::set_customProperties(ExitGames.Client.Photon.Hashtable)
extern "C" void PhotonPlayer_set_customProperties_m8_735 (PhotonPlayer_t8_102 * __this, Hashtable_t5_1 * ___value, const MethodInfo* method)
{
	{
		Hashtable_t5_1 * L_0 = ___value;
		__this->___U3CcustomPropertiesU3Ek__BackingField_4 = L_0;
		return;
	}
}
// ExitGames.Client.Photon.Hashtable PhotonPlayer::get_allProperties()
extern TypeInfo* Hashtable_t5_1_il2cpp_TypeInfo_var;
extern TypeInfo* Byte_t1_11_il2cpp_TypeInfo_var;
extern "C" Hashtable_t5_1 * PhotonPlayer_get_allProperties_m8_736 (PhotonPlayer_t8_102 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Hashtable_t5_1_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(891);
		Byte_t1_11_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(22);
		s_Il2CppMethodIntialized = true;
	}
	Hashtable_t5_1 * V_0 = {0};
	{
		Hashtable_t5_1 * L_0 = (Hashtable_t5_1 *)il2cpp_codegen_object_new (Hashtable_t5_1_il2cpp_TypeInfo_var);
		Hashtable__ctor_m5_0(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		Hashtable_t5_1 * L_1 = V_0;
		Hashtable_t5_1 * L_2 = PhotonPlayer_get_customProperties_m8_734(__this, /*hidden argument*/NULL);
		Extensions_Merge_m8_297(NULL /*static, unused*/, L_1, L_2, /*hidden argument*/NULL);
		Hashtable_t5_1 * L_3 = V_0;
		uint8_t L_4 = ((int32_t)255);
		Object_t * L_5 = Box(Byte_t1_11_il2cpp_TypeInfo_var, &L_4);
		String_t* L_6 = PhotonPlayer_get_name_m8_731(__this, /*hidden argument*/NULL);
		NullCheck(L_3);
		Hashtable_set_Item_m5_3(L_3, L_5, L_6, /*hidden argument*/NULL);
		Hashtable_t5_1 * L_7 = V_0;
		return L_7;
	}
}
// System.Boolean PhotonPlayer::Equals(System.Object)
extern TypeInfo* PhotonPlayer_t8_102_il2cpp_TypeInfo_var;
extern "C" bool PhotonPlayer_Equals_m8_737 (PhotonPlayer_t8_102 * __this, Object_t * ___p, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		PhotonPlayer_t8_102_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1064);
		s_Il2CppMethodIntialized = true;
	}
	PhotonPlayer_t8_102 * V_0 = {0};
	int32_t G_B3_0 = 0;
	{
		Object_t * L_0 = ___p;
		V_0 = ((PhotonPlayer_t8_102 *)IsInstClass(L_0, PhotonPlayer_t8_102_il2cpp_TypeInfo_var));
		PhotonPlayer_t8_102 * L_1 = V_0;
		if (!L_1)
		{
			goto IL_001d;
		}
	}
	{
		int32_t L_2 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(2 /* System.Int32 PhotonPlayer::GetHashCode() */, __this);
		PhotonPlayer_t8_102 * L_3 = V_0;
		NullCheck(L_3);
		int32_t L_4 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(2 /* System.Int32 PhotonPlayer::GetHashCode() */, L_3);
		G_B3_0 = ((((int32_t)L_2) == ((int32_t)L_4))? 1 : 0);
		goto IL_001e;
	}

IL_001d:
	{
		G_B3_0 = 0;
	}

IL_001e:
	{
		return G_B3_0;
	}
}
// System.Int32 PhotonPlayer::GetHashCode()
extern "C" int32_t PhotonPlayer_GetHashCode_m8_738 (PhotonPlayer_t8_102 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = PhotonPlayer_get_ID_m8_730(__this, /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Void PhotonPlayer::InternalChangeLocalID(System.Int32)
extern Il2CppCodeGenString* _stringLiteral3464;
extern "C" void PhotonPlayer_InternalChangeLocalID_m8_739 (PhotonPlayer_t8_102 * __this, int32_t ___newID, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		_stringLiteral3464 = il2cpp_codegen_string_literal_from_index(3464);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool L_0 = (__this->___isLocal_2);
		if (L_0)
		{
			goto IL_0016;
		}
	}
	{
		Debug_LogError_m6_490(NULL /*static, unused*/, _stringLiteral3464, /*hidden argument*/NULL);
		return;
	}

IL_0016:
	{
		int32_t L_1 = ___newID;
		__this->___actorID_0 = L_1;
		return;
	}
}
// System.Void PhotonPlayer::InternalCacheProperties(ExitGames.Client.Photon.Hashtable)
extern TypeInfo* Byte_t1_11_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern "C" void PhotonPlayer_InternalCacheProperties_m8_740 (PhotonPlayer_t8_102 * __this, Hashtable_t5_1 * ___properties, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Byte_t1_11_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(22);
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(11);
		s_Il2CppMethodIntialized = true;
	}
	{
		Hashtable_t5_1 * L_0 = ___properties;
		if (!L_0)
		{
			goto IL_0022;
		}
	}
	{
		Hashtable_t5_1 * L_1 = ___properties;
		NullCheck(L_1);
		int32_t L_2 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(9 /* System.Int32 System.Collections.Generic.Dictionary`2<System.Object,System.Object>::get_Count() */, L_1);
		if (!L_2)
		{
			goto IL_0022;
		}
	}
	{
		Hashtable_t5_1 * L_3 = PhotonPlayer_get_customProperties_m8_734(__this, /*hidden argument*/NULL);
		Hashtable_t5_1 * L_4 = ___properties;
		NullCheck(L_3);
		bool L_5 = (bool)VirtFuncInvoker1< bool, Object_t * >::Invoke(0 /* System.Boolean System.Object::Equals(System.Object) */, L_3, L_4);
		if (!L_5)
		{
			goto IL_0023;
		}
	}

IL_0022:
	{
		return;
	}

IL_0023:
	{
		Hashtable_t5_1 * L_6 = ___properties;
		uint8_t L_7 = ((int32_t)255);
		Object_t * L_8 = Box(Byte_t1_11_il2cpp_TypeInfo_var, &L_7);
		NullCheck(L_6);
		bool L_9 = (bool)VirtFuncInvoker1< bool, Object_t * >::Invoke(28 /* System.Boolean System.Collections.Generic.Dictionary`2<System.Object,System.Object>::ContainsKey(!0) */, L_6, L_8);
		if (!L_9)
		{
			goto IL_0053;
		}
	}
	{
		Hashtable_t5_1 * L_10 = ___properties;
		uint8_t L_11 = ((int32_t)255);
		Object_t * L_12 = Box(Byte_t1_11_il2cpp_TypeInfo_var, &L_11);
		NullCheck(L_10);
		Object_t * L_13 = Hashtable_get_Item_m5_2(L_10, L_12, /*hidden argument*/NULL);
		__this->___nameField_1 = ((String_t*)CastclassSealed(L_13, String_t_il2cpp_TypeInfo_var));
	}

IL_0053:
	{
		Hashtable_t5_1 * L_14 = ___properties;
		uint8_t L_15 = ((int32_t)254);
		Object_t * L_16 = Box(Byte_t1_11_il2cpp_TypeInfo_var, &L_15);
		NullCheck(L_14);
		bool L_17 = (bool)VirtFuncInvoker1< bool, Object_t * >::Invoke(28 /* System.Boolean System.Collections.Generic.Dictionary`2<System.Object,System.Object>::ContainsKey(!0) */, L_14, L_16);
		if (!L_17)
		{
			goto IL_0068;
		}
	}

IL_0068:
	{
		Hashtable_t5_1 * L_18 = PhotonPlayer_get_customProperties_m8_734(__this, /*hidden argument*/NULL);
		Hashtable_t5_1 * L_19 = ___properties;
		Extensions_MergeStringKeys_m8_298(NULL /*static, unused*/, L_18, L_19, /*hidden argument*/NULL);
		Hashtable_t5_1 * L_20 = PhotonPlayer_get_customProperties_m8_734(__this, /*hidden argument*/NULL);
		Extensions_StripKeysWithNullValues_m8_301(NULL /*static, unused*/, L_20, /*hidden argument*/NULL);
		return;
	}
}
// System.Void PhotonPlayer::SetCustomProperties(ExitGames.Client.Photon.Hashtable,ExitGames.Client.Photon.Hashtable,System.Boolean)
extern TypeInfo* PhotonNetwork_t8_114_il2cpp_TypeInfo_var;
extern TypeInfo* ObjectU5BU5D_t1_157_il2cpp_TypeInfo_var;
extern TypeInfo* NetworkingPeer_t8_98_il2cpp_TypeInfo_var;
extern "C" void PhotonPlayer_SetCustomProperties_m8_741 (PhotonPlayer_t8_102 * __this, Hashtable_t5_1 * ___propertiesToSet, Hashtable_t5_1 * ___expectedValues, bool ___webForward, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		PhotonNetwork_t8_114_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1058);
		ObjectU5BU5D_t1_157_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(19);
		NetworkingPeer_t8_98_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1121);
		s_Il2CppMethodIntialized = true;
	}
	Hashtable_t5_1 * V_0 = {0};
	Hashtable_t5_1 * V_1 = {0};
	bool V_2 = false;
	bool V_3 = false;
	int32_t G_B5_0 = 0;
	int32_t G_B8_0 = 0;
	{
		Hashtable_t5_1 * L_0 = ___propertiesToSet;
		if (L_0)
		{
			goto IL_0007;
		}
	}
	{
		return;
	}

IL_0007:
	{
		Hashtable_t5_1 * L_1 = ___propertiesToSet;
		Hashtable_t5_1 * L_2 = Extensions_StripToStringKeys_m8_300(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		Hashtable_t5_1 * L_3 = ___expectedValues;
		Hashtable_t5_1 * L_4 = Extensions_StripToStringKeys_m8_300(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		V_1 = L_4;
		Hashtable_t5_1 * L_5 = V_1;
		if (!L_5)
		{
			goto IL_0026;
		}
	}
	{
		Hashtable_t5_1 * L_6 = V_1;
		NullCheck(L_6);
		int32_t L_7 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(9 /* System.Int32 System.Collections.Generic.Dictionary`2<System.Object,System.Object>::get_Count() */, L_6);
		G_B5_0 = ((((int32_t)L_7) == ((int32_t)0))? 1 : 0);
		goto IL_0027;
	}

IL_0026:
	{
		G_B5_0 = 1;
	}

IL_0027:
	{
		V_2 = G_B5_0;
		int32_t L_8 = (__this->___actorID_0);
		if ((((int32_t)L_8) <= ((int32_t)0)))
		{
			goto IL_003e;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(PhotonNetwork_t8_114_il2cpp_TypeInfo_var);
		bool L_9 = PhotonNetwork_get_offlineMode_m8_623(NULL /*static, unused*/, /*hidden argument*/NULL);
		G_B8_0 = ((((int32_t)L_9) == ((int32_t)0))? 1 : 0);
		goto IL_003f;
	}

IL_003e:
	{
		G_B8_0 = 0;
	}

IL_003f:
	{
		V_3 = G_B8_0;
		bool L_10 = V_3;
		if (!L_10)
		{
			goto IL_005a;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(PhotonNetwork_t8_114_il2cpp_TypeInfo_var);
		NetworkingPeer_t8_98 * L_11 = ((PhotonNetwork_t8_114_StaticFields*)PhotonNetwork_t8_114_il2cpp_TypeInfo_var->static_fields)->___networkingPeer_4;
		int32_t L_12 = (__this->___actorID_0);
		Hashtable_t5_1 * L_13 = V_0;
		Hashtable_t5_1 * L_14 = V_1;
		bool L_15 = ___webForward;
		NullCheck(L_11);
		LoadbalancingPeer_OpSetPropertiesOfActor_m8_329(L_11, L_12, L_13, L_14, L_15, /*hidden argument*/NULL);
	}

IL_005a:
	{
		bool L_16 = V_3;
		if (!L_16)
		{
			goto IL_0066;
		}
	}
	{
		bool L_17 = V_2;
		if (!L_17)
		{
			goto IL_0082;
		}
	}

IL_0066:
	{
		Hashtable_t5_1 * L_18 = V_0;
		PhotonPlayer_InternalCacheProperties_m8_740(__this, L_18, /*hidden argument*/NULL);
		ObjectU5BU5D_t1_157* L_19 = ((ObjectU5BU5D_t1_157*)SZArrayNew(ObjectU5BU5D_t1_157_il2cpp_TypeInfo_var, 2));
		NullCheck(L_19);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_19, 0);
		ArrayElementTypeCheck (L_19, __this);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_19, 0, sizeof(Object_t *))) = (Object_t *)__this;
		ObjectU5BU5D_t1_157* L_20 = L_19;
		Hashtable_t5_1 * L_21 = V_0;
		NullCheck(L_20);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_20, 1);
		ArrayElementTypeCheck (L_20, L_21);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_20, 1, sizeof(Object_t *))) = (Object_t *)L_21;
		IL2CPP_RUNTIME_CLASS_INIT(NetworkingPeer_t8_98_il2cpp_TypeInfo_var);
		NetworkingPeer_SendMonoMessage_m8_446(NULL /*static, unused*/, ((int32_t)21), L_20, /*hidden argument*/NULL);
	}

IL_0082:
	{
		return;
	}
}
// PhotonPlayer PhotonPlayer::Find(System.Int32)
extern TypeInfo* PhotonNetwork_t8_114_il2cpp_TypeInfo_var;
extern "C" PhotonPlayer_t8_102 * PhotonPlayer_Find_m8_742 (Object_t * __this /* static, unused */, int32_t ___ID, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		PhotonNetwork_t8_114_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1058);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(PhotonNetwork_t8_114_il2cpp_TypeInfo_var);
		NetworkingPeer_t8_98 * L_0 = ((PhotonNetwork_t8_114_StaticFields*)PhotonNetwork_t8_114_il2cpp_TypeInfo_var->static_fields)->___networkingPeer_4;
		if (!L_0)
		{
			goto IL_0016;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(PhotonNetwork_t8_114_il2cpp_TypeInfo_var);
		NetworkingPeer_t8_98 * L_1 = ((PhotonNetwork_t8_114_StaticFields*)PhotonNetwork_t8_114_il2cpp_TypeInfo_var->static_fields)->___networkingPeer_4;
		int32_t L_2 = ___ID;
		NullCheck(L_1);
		PhotonPlayer_t8_102 * L_3 = NetworkingPeer_GetPlayerWithId_m8_429(L_1, L_2, /*hidden argument*/NULL);
		return L_3;
	}

IL_0016:
	{
		return (PhotonPlayer_t8_102 *)NULL;
	}
}
// PhotonPlayer PhotonPlayer::Get(System.Int32)
extern "C" PhotonPlayer_t8_102 * PhotonPlayer_Get_m8_743 (PhotonPlayer_t8_102 * __this, int32_t ___id, const MethodInfo* method)
{
	{
		int32_t L_0 = ___id;
		PhotonPlayer_t8_102 * L_1 = PhotonPlayer_Find_m8_742(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// PhotonPlayer PhotonPlayer::GetNext()
extern "C" PhotonPlayer_t8_102 * PhotonPlayer_GetNext_m8_744 (PhotonPlayer_t8_102 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = PhotonPlayer_get_ID_m8_730(__this, /*hidden argument*/NULL);
		PhotonPlayer_t8_102 * L_1 = PhotonPlayer_GetNextFor_m8_746(__this, L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// PhotonPlayer PhotonPlayer::GetNextFor(PhotonPlayer)
extern "C" PhotonPlayer_t8_102 * PhotonPlayer_GetNextFor_m8_745 (PhotonPlayer_t8_102 * __this, PhotonPlayer_t8_102 * ___currentPlayer, const MethodInfo* method)
{
	{
		PhotonPlayer_t8_102 * L_0 = ___currentPlayer;
		if (L_0)
		{
			goto IL_0008;
		}
	}
	{
		return (PhotonPlayer_t8_102 *)NULL;
	}

IL_0008:
	{
		PhotonPlayer_t8_102 * L_1 = ___currentPlayer;
		NullCheck(L_1);
		int32_t L_2 = PhotonPlayer_get_ID_m8_730(L_1, /*hidden argument*/NULL);
		PhotonPlayer_t8_102 * L_3 = PhotonPlayer_GetNextFor_m8_746(__this, L_2, /*hidden argument*/NULL);
		return L_3;
	}
}
// PhotonPlayer PhotonPlayer::GetNextFor(System.Int32)
extern TypeInfo* PhotonNetwork_t8_114_il2cpp_TypeInfo_var;
extern TypeInfo* Enumerator_t1_961_il2cpp_TypeInfo_var;
extern TypeInfo* IDisposable_t1_835_il2cpp_TypeInfo_var;
extern const MethodInfo* Dictionary_2_get_Keys_m1_5651_MethodInfo_var;
extern const MethodInfo* KeyCollection_GetEnumerator_m1_5652_MethodInfo_var;
extern const MethodInfo* Enumerator_get_Current_m1_5653_MethodInfo_var;
extern const MethodInfo* Enumerator_MoveNext_m1_5654_MethodInfo_var;
extern "C" PhotonPlayer_t8_102 * PhotonPlayer_GetNextFor_m8_746 (PhotonPlayer_t8_102 * __this, int32_t ___currentPlayerId, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		PhotonNetwork_t8_114_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1058);
		Enumerator_t1_961_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1129);
		IDisposable_t1_835_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(142);
		Dictionary_2_get_Keys_m1_5651_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483907);
		KeyCollection_GetEnumerator_m1_5652_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483908);
		Enumerator_get_Current_m1_5653_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483909);
		Enumerator_MoveNext_m1_5654_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483910);
		s_Il2CppMethodIntialized = true;
	}
	Dictionary_2_t1_936 * V_0 = {0};
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	int32_t V_3 = 0;
	Enumerator_t1_961  V_4 = {0};
	Exception_t1_33 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1_33 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	PhotonPlayer_t8_102 * G_B17_0 = {0};
	{
		IL2CPP_RUNTIME_CLASS_INIT(PhotonNetwork_t8_114_il2cpp_TypeInfo_var);
		NetworkingPeer_t8_98 * L_0 = ((PhotonNetwork_t8_114_StaticFields*)PhotonNetwork_t8_114_il2cpp_TypeInfo_var->static_fields)->___networkingPeer_4;
		if (!L_0)
		{
			goto IL_002e;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(PhotonNetwork_t8_114_il2cpp_TypeInfo_var);
		NetworkingPeer_t8_98 * L_1 = ((PhotonNetwork_t8_114_StaticFields*)PhotonNetwork_t8_114_il2cpp_TypeInfo_var->static_fields)->___networkingPeer_4;
		NullCheck(L_1);
		Dictionary_2_t1_936 * L_2 = (L_1->___mActors_25);
		if (!L_2)
		{
			goto IL_002e;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(PhotonNetwork_t8_114_il2cpp_TypeInfo_var);
		NetworkingPeer_t8_98 * L_3 = ((PhotonNetwork_t8_114_StaticFields*)PhotonNetwork_t8_114_il2cpp_TypeInfo_var->static_fields)->___networkingPeer_4;
		NullCheck(L_3);
		Dictionary_2_t1_936 * L_4 = (L_3->___mActors_25);
		NullCheck(L_4);
		int32_t L_5 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(9 /* System.Int32 System.Collections.Generic.Dictionary`2<System.Int32,PhotonPlayer>::get_Count() */, L_4);
		if ((((int32_t)L_5) >= ((int32_t)2)))
		{
			goto IL_0030;
		}
	}

IL_002e:
	{
		return (PhotonPlayer_t8_102 *)NULL;
	}

IL_0030:
	{
		IL2CPP_RUNTIME_CLASS_INIT(PhotonNetwork_t8_114_il2cpp_TypeInfo_var);
		NetworkingPeer_t8_98 * L_6 = ((PhotonNetwork_t8_114_StaticFields*)PhotonNetwork_t8_114_il2cpp_TypeInfo_var->static_fields)->___networkingPeer_4;
		NullCheck(L_6);
		Dictionary_2_t1_936 * L_7 = (L_6->___mActors_25);
		V_0 = L_7;
		V_1 = ((int32_t)2147483647);
		int32_t L_8 = ___currentPlayerId;
		V_2 = L_8;
		Dictionary_2_t1_936 * L_9 = V_0;
		NullCheck(L_9);
		KeyCollection_t1_962 * L_10 = Dictionary_2_get_Keys_m1_5651(L_9, /*hidden argument*/Dictionary_2_get_Keys_m1_5651_MethodInfo_var);
		NullCheck(L_10);
		Enumerator_t1_961  L_11 = KeyCollection_GetEnumerator_m1_5652(L_10, /*hidden argument*/KeyCollection_GetEnumerator_m1_5652_MethodInfo_var);
		V_4 = L_11;
	}

IL_0050:
	try
	{ // begin try (depth: 1)
		{
			goto IL_007b;
		}

IL_0055:
		{
			int32_t L_12 = Enumerator_get_Current_m1_5653((&V_4), /*hidden argument*/Enumerator_get_Current_m1_5653_MethodInfo_var);
			V_3 = L_12;
			int32_t L_13 = V_3;
			int32_t L_14 = V_2;
			if ((((int32_t)L_13) >= ((int32_t)L_14)))
			{
				goto IL_006b;
			}
		}

IL_0064:
		{
			int32_t L_15 = V_3;
			V_2 = L_15;
			goto IL_007b;
		}

IL_006b:
		{
			int32_t L_16 = V_3;
			int32_t L_17 = ___currentPlayerId;
			if ((((int32_t)L_16) <= ((int32_t)L_17)))
			{
				goto IL_007b;
			}
		}

IL_0072:
		{
			int32_t L_18 = V_3;
			int32_t L_19 = V_1;
			if ((((int32_t)L_18) >= ((int32_t)L_19)))
			{
				goto IL_007b;
			}
		}

IL_0079:
		{
			int32_t L_20 = V_3;
			V_1 = L_20;
		}

IL_007b:
		{
			bool L_21 = Enumerator_MoveNext_m1_5654((&V_4), /*hidden argument*/Enumerator_MoveNext_m1_5654_MethodInfo_var);
			if (L_21)
			{
				goto IL_0055;
			}
		}

IL_0087:
		{
			IL2CPP_LEAVE(0x99, FINALLY_008c);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1_33 *)e.ex;
		goto FINALLY_008c;
	}

FINALLY_008c:
	{ // begin finally (depth: 1)
		Enumerator_t1_961  L_22 = V_4;
		Enumerator_t1_961  L_23 = L_22;
		Object_t * L_24 = Box(Enumerator_t1_961_il2cpp_TypeInfo_var, &L_23);
		NullCheck(L_24);
		InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1_835_il2cpp_TypeInfo_var, L_24);
		IL2CPP_END_FINALLY(140)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(140)
	{
		IL2CPP_JUMP_TBL(0x99, IL_0099)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1_33 *)
	}

IL_0099:
	{
		int32_t L_25 = V_1;
		if ((((int32_t)L_25) == ((int32_t)((int32_t)2147483647))))
		{
			goto IL_00b0;
		}
	}
	{
		Dictionary_2_t1_936 * L_26 = V_0;
		int32_t L_27 = V_1;
		NullCheck(L_26);
		PhotonPlayer_t8_102 * L_28 = (PhotonPlayer_t8_102 *)VirtFuncInvoker1< PhotonPlayer_t8_102 *, int32_t >::Invoke(25 /* !1 System.Collections.Generic.Dictionary`2<System.Int32,PhotonPlayer>::get_Item(!0) */, L_26, L_27);
		G_B17_0 = L_28;
		goto IL_00b7;
	}

IL_00b0:
	{
		Dictionary_2_t1_936 * L_29 = V_0;
		int32_t L_30 = V_2;
		NullCheck(L_29);
		PhotonPlayer_t8_102 * L_31 = (PhotonPlayer_t8_102 *)VirtFuncInvoker1< PhotonPlayer_t8_102 *, int32_t >::Invoke(25 /* !1 System.Collections.Generic.Dictionary`2<System.Int32,PhotonPlayer>::get_Item(!0) */, L_29, L_30);
		G_B17_0 = L_31;
	}

IL_00b7:
	{
		return G_B17_0;
	}
}
// System.String PhotonPlayer::ToString()
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* Int32_t1_3_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3465;
extern Il2CppCodeGenString* _stringLiteral2996;
extern Il2CppCodeGenString* _stringLiteral3466;
extern "C" String_t* PhotonPlayer_ToString_m8_747 (PhotonPlayer_t8_102 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(11);
		Int32_t1_3_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(8);
		_stringLiteral3465 = il2cpp_codegen_string_literal_from_index(3465);
		_stringLiteral2996 = il2cpp_codegen_string_literal_from_index(2996);
		_stringLiteral3466 = il2cpp_codegen_string_literal_from_index(3466);
		s_Il2CppMethodIntialized = true;
	}
	Object_t * G_B3_0 = {0};
	String_t* G_B3_1 = {0};
	Object_t * G_B2_0 = {0};
	String_t* G_B2_1 = {0};
	String_t* G_B4_0 = {0};
	Object_t * G_B4_1 = {0};
	String_t* G_B4_2 = {0};
	String_t* G_B7_0 = {0};
	String_t* G_B7_1 = {0};
	String_t* G_B6_0 = {0};
	String_t* G_B6_1 = {0};
	String_t* G_B8_0 = {0};
	String_t* G_B8_1 = {0};
	String_t* G_B8_2 = {0};
	{
		String_t* L_0 = PhotonPlayer_get_name_m8_731(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_1 = String_IsNullOrEmpty_m1_397(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0040;
		}
	}
	{
		int32_t L_2 = PhotonPlayer_get_ID_m8_730(__this, /*hidden argument*/NULL);
		int32_t L_3 = L_2;
		Object_t * L_4 = Box(Int32_t1_3_il2cpp_TypeInfo_var, &L_3);
		bool L_5 = PhotonPlayer_get_isMasterClient_m8_733(__this, /*hidden argument*/NULL);
		G_B2_0 = L_4;
		G_B2_1 = _stringLiteral3465;
		if (!L_5)
		{
			G_B3_0 = L_4;
			G_B3_1 = _stringLiteral3465;
			goto IL_0035;
		}
	}
	{
		G_B4_0 = _stringLiteral2996;
		G_B4_1 = G_B2_0;
		G_B4_2 = G_B2_1;
		goto IL_003a;
	}

IL_0035:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_6 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->___Empty_2;
		G_B4_0 = L_6;
		G_B4_1 = G_B3_0;
		G_B4_2 = G_B3_1;
	}

IL_003a:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_7 = String_Format_m1_411(NULL /*static, unused*/, G_B4_2, G_B4_1, G_B4_0, /*hidden argument*/NULL);
		return L_7;
	}

IL_0040:
	{
		String_t* L_8 = PhotonPlayer_get_name_m8_731(__this, /*hidden argument*/NULL);
		bool L_9 = PhotonPlayer_get_isMasterClient_m8_733(__this, /*hidden argument*/NULL);
		G_B6_0 = L_8;
		G_B6_1 = _stringLiteral3466;
		if (!L_9)
		{
			G_B7_0 = L_8;
			G_B7_1 = _stringLiteral3466;
			goto IL_0060;
		}
	}
	{
		G_B8_0 = _stringLiteral2996;
		G_B8_1 = G_B6_0;
		G_B8_2 = G_B6_1;
		goto IL_0065;
	}

IL_0060:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_10 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->___Empty_2;
		G_B8_0 = L_10;
		G_B8_1 = G_B7_0;
		G_B8_2 = G_B7_1;
	}

IL_0065:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_11 = String_Format_m1_411(NULL /*static, unused*/, G_B8_2, G_B8_1, G_B8_0, /*hidden argument*/NULL);
		return L_11;
	}
}
// System.String PhotonPlayer::ToStringFull()
extern TypeInfo* Int32_t1_3_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3467;
extern "C" String_t* PhotonPlayer_ToStringFull_m8_748 (PhotonPlayer_t8_102 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Int32_t1_3_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(8);
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(11);
		_stringLiteral3467 = il2cpp_codegen_string_literal_from_index(3467);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = PhotonPlayer_get_ID_m8_730(__this, /*hidden argument*/NULL);
		int32_t L_1 = L_0;
		Object_t * L_2 = Box(Int32_t1_3_il2cpp_TypeInfo_var, &L_1);
		String_t* L_3 = PhotonPlayer_get_name_m8_731(__this, /*hidden argument*/NULL);
		Hashtable_t5_1 * L_4 = PhotonPlayer_get_customProperties_m8_734(__this, /*hidden argument*/NULL);
		String_t* L_5 = Extensions_ToStringFull_m8_299(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_6 = String_Format_m1_412(NULL /*static, unused*/, _stringLiteral3467, L_2, L_3, L_5, /*hidden argument*/NULL);
		return L_6;
	}
}
// System.Void PhotonStatsGui::.ctor()
extern "C" void PhotonStatsGui__ctor_m8_749 (PhotonStatsGui_t8_116 * __this, const MethodInfo* method)
{
	{
		__this->___statsWindowOn_2 = 1;
		__this->___statsOn_3 = 1;
		Rect_t6_52  L_0 = {0};
		Rect__ctor_m6_270(&L_0, (0.0f), (100.0f), (200.0f), (50.0f), /*hidden argument*/NULL);
		__this->___statsRect_7 = L_0;
		__this->___WindowId_8 = ((int32_t)100);
		MonoBehaviour__ctor_m6_525(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void PhotonStatsGui::Start()
extern "C" void PhotonStatsGui_Start_m8_750 (PhotonStatsGui_t8_116 * __this, const MethodInfo* method)
{
	{
		Rect_t6_52 * L_0 = &(__this->___statsRect_7);
		float L_1 = Rect_get_x_m6_273(L_0, /*hidden argument*/NULL);
		if ((!(((float)L_1) <= ((float)(0.0f)))))
		{
			goto IL_0032;
		}
	}
	{
		Rect_t6_52 * L_2 = &(__this->___statsRect_7);
		int32_t L_3 = Screen_get_width_m6_121(NULL /*static, unused*/, /*hidden argument*/NULL);
		Rect_t6_52 * L_4 = &(__this->___statsRect_7);
		float L_5 = Rect_get_width_m6_277(L_4, /*hidden argument*/NULL);
		Rect_set_x_m6_274(L_2, ((float)((float)(((float)((float)L_3)))-(float)L_5)), /*hidden argument*/NULL);
	}

IL_0032:
	{
		return;
	}
}
// System.Void PhotonStatsGui::Update()
extern TypeInfo* Input_t6_83_il2cpp_TypeInfo_var;
extern "C" void PhotonStatsGui_Update_m8_751 (PhotonStatsGui_t8_116 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Input_t6_83_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(956);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t6_83_il2cpp_TypeInfo_var);
		bool L_0 = Input_GetKeyDown_m6_542(NULL /*static, unused*/, ((int32_t)9), /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_0031;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t6_83_il2cpp_TypeInfo_var);
		bool L_1 = Input_GetKey_m6_541(NULL /*static, unused*/, ((int32_t)304), /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0031;
		}
	}
	{
		bool L_2 = (__this->___statsWindowOn_2);
		__this->___statsWindowOn_2 = ((((int32_t)L_2) == ((int32_t)0))? 1 : 0);
		__this->___statsOn_3 = 1;
	}

IL_0031:
	{
		return;
	}
}
// System.Void PhotonStatsGui::OnGUI()
extern TypeInfo* PhotonNetwork_t8_114_il2cpp_TypeInfo_var;
extern TypeInfo* WindowFunction_t6_159_il2cpp_TypeInfo_var;
extern TypeInfo* GUILayoutOptionU5BU5D_t6_165_il2cpp_TypeInfo_var;
extern const MethodInfo* PhotonStatsGui_TrafficStatsWindow_m8_753_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral3468;
extern "C" void PhotonStatsGui_OnGUI_m8_752 (PhotonStatsGui_t8_116 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		PhotonNetwork_t8_114_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1058);
		WindowFunction_t6_159_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(997);
		GUILayoutOptionU5BU5D_t6_165_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(991);
		PhotonStatsGui_TrafficStatsWindow_m8_753_MethodInfo_var = il2cpp_codegen_method_info_from_index(291);
		_stringLiteral3468 = il2cpp_codegen_string_literal_from_index(3468);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(PhotonNetwork_t8_114_il2cpp_TypeInfo_var);
		NetworkingPeer_t8_98 * L_0 = ((PhotonNetwork_t8_114_StaticFields*)PhotonNetwork_t8_114_il2cpp_TypeInfo_var->static_fields)->___networkingPeer_4;
		NullCheck(L_0);
		bool L_1 = PhotonPeer_get_TrafficStatsEnabled_m5_202(L_0, /*hidden argument*/NULL);
		bool L_2 = (__this->___statsOn_3);
		if ((((int32_t)L_1) == ((int32_t)L_2)))
		{
			goto IL_0025;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(PhotonNetwork_t8_114_il2cpp_TypeInfo_var);
		NetworkingPeer_t8_98 * L_3 = ((PhotonNetwork_t8_114_StaticFields*)PhotonNetwork_t8_114_il2cpp_TypeInfo_var->static_fields)->___networkingPeer_4;
		bool L_4 = (__this->___statsOn_3);
		NullCheck(L_3);
		PhotonPeer_set_TrafficStatsEnabled_m5_203(L_3, L_4, /*hidden argument*/NULL);
	}

IL_0025:
	{
		bool L_5 = (__this->___statsWindowOn_2);
		if (L_5)
		{
			goto IL_0031;
		}
	}
	{
		return;
	}

IL_0031:
	{
		int32_t L_6 = (__this->___WindowId_8);
		Rect_t6_52  L_7 = (__this->___statsRect_7);
		IntPtr_t L_8 = { (void*)PhotonStatsGui_TrafficStatsWindow_m8_753_MethodInfo_var };
		WindowFunction_t6_159 * L_9 = (WindowFunction_t6_159 *)il2cpp_codegen_object_new (WindowFunction_t6_159_il2cpp_TypeInfo_var);
		WindowFunction__ctor_m6_1031(L_9, __this, L_8, /*hidden argument*/NULL);
		Rect_t6_52  L_10 = GUILayout_Window_m6_1141(NULL /*static, unused*/, L_6, L_7, L_9, _stringLiteral3468, ((GUILayoutOptionU5BU5D_t6_165*)SZArrayNew(GUILayoutOptionU5BU5D_t6_165_il2cpp_TypeInfo_var, 0)), /*hidden argument*/NULL);
		__this->___statsRect_7 = L_10;
		return;
	}
}
// System.Void PhotonStatsGui::TrafficStatsWindow(System.Int32)
extern TypeInfo* PhotonNetwork_t8_114_il2cpp_TypeInfo_var;
extern TypeInfo* GUILayoutOptionU5BU5D_t6_165_il2cpp_TypeInfo_var;
extern TypeInfo* Int32_t1_3_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* Int64_t1_7_il2cpp_TypeInfo_var;
extern TypeInfo* ObjectU5BU5D_t1_157_il2cpp_TypeInfo_var;
extern TypeInfo* Byte_t1_11_il2cpp_TypeInfo_var;
extern TypeInfo* GUI_t6_160_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3469;
extern Il2CppCodeGenString* _stringLiteral3470;
extern Il2CppCodeGenString* _stringLiteral3471;
extern Il2CppCodeGenString* _stringLiteral3472;
extern Il2CppCodeGenString* _stringLiteral3473;
extern Il2CppCodeGenString* _stringLiteral3474;
extern Il2CppCodeGenString* _stringLiteral3475;
extern Il2CppCodeGenString* _stringLiteral3476;
extern Il2CppCodeGenString* _stringLiteral3477;
extern Il2CppCodeGenString* _stringLiteral3478;
extern Il2CppCodeGenString* _stringLiteral3479;
extern Il2CppCodeGenString* _stringLiteral3480;
extern "C" void PhotonStatsGui_TrafficStatsWindow_m8_753 (PhotonStatsGui_t8_116 * __this, int32_t ___windowID, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		PhotonNetwork_t8_114_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1058);
		GUILayoutOptionU5BU5D_t6_165_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(991);
		Int32_t1_3_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(8);
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(11);
		Int64_t1_7_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(18);
		ObjectU5BU5D_t1_157_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(19);
		Byte_t1_11_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(22);
		GUI_t6_160_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(982);
		_stringLiteral3469 = il2cpp_codegen_string_literal_from_index(3469);
		_stringLiteral3470 = il2cpp_codegen_string_literal_from_index(3470);
		_stringLiteral3471 = il2cpp_codegen_string_literal_from_index(3471);
		_stringLiteral3472 = il2cpp_codegen_string_literal_from_index(3472);
		_stringLiteral3473 = il2cpp_codegen_string_literal_from_index(3473);
		_stringLiteral3474 = il2cpp_codegen_string_literal_from_index(3474);
		_stringLiteral3475 = il2cpp_codegen_string_literal_from_index(3475);
		_stringLiteral3476 = il2cpp_codegen_string_literal_from_index(3476);
		_stringLiteral3477 = il2cpp_codegen_string_literal_from_index(3477);
		_stringLiteral3478 = il2cpp_codegen_string_literal_from_index(3478);
		_stringLiteral3479 = il2cpp_codegen_string_literal_from_index(3479);
		_stringLiteral3480 = il2cpp_codegen_string_literal_from_index(3480);
		s_Il2CppMethodIntialized = true;
	}
	bool V_0 = false;
	TrafficStatsGameLevel_t5_15 * V_1 = {0};
	int64_t V_2 = 0;
	String_t* V_3 = {0};
	String_t* V_4 = {0};
	String_t* V_5 = {0};
	String_t* V_6 = {0};
	String_t* V_7 = {0};
	String_t* V_8 = {0};
	String_t* V_9 = {0};
	{
		V_0 = 0;
		IL2CPP_RUNTIME_CLASS_INIT(PhotonNetwork_t8_114_il2cpp_TypeInfo_var);
		NetworkingPeer_t8_98 * L_0 = ((PhotonNetwork_t8_114_StaticFields*)PhotonNetwork_t8_114_il2cpp_TypeInfo_var->static_fields)->___networkingPeer_4;
		NullCheck(L_0);
		TrafficStatsGameLevel_t5_15 * L_1 = PhotonPeer_get_TrafficStatsGameLevel_m5_208(L_0, /*hidden argument*/NULL);
		V_1 = L_1;
		NetworkingPeer_t8_98 * L_2 = ((PhotonNetwork_t8_114_StaticFields*)PhotonNetwork_t8_114_il2cpp_TypeInfo_var->static_fields)->___networkingPeer_4;
		NullCheck(L_2);
		int64_t L_3 = PhotonPeer_get_TrafficStatsElapsedMs_m5_204(L_2, /*hidden argument*/NULL);
		V_2 = ((int64_t)((int64_t)L_3/(int64_t)(((int64_t)((int64_t)((int32_t)1000))))));
		int64_t L_4 = V_2;
		if (L_4)
		{
			goto IL_0028;
		}
	}
	{
		V_2 = (((int64_t)((int64_t)1)));
	}

IL_0028:
	{
		GUILayout_BeginHorizontal_m6_1127(NULL /*static, unused*/, ((GUILayoutOptionU5BU5D_t6_165*)SZArrayNew(GUILayoutOptionU5BU5D_t6_165_il2cpp_TypeInfo_var, 0)), /*hidden argument*/NULL);
		bool L_5 = (__this->___buttonsOn_6);
		bool L_6 = GUILayout_Toggle_m6_1119(NULL /*static, unused*/, L_5, _stringLiteral3469, ((GUILayoutOptionU5BU5D_t6_165*)SZArrayNew(GUILayoutOptionU5BU5D_t6_165_il2cpp_TypeInfo_var, 0)), /*hidden argument*/NULL);
		__this->___buttonsOn_6 = L_6;
		bool L_7 = (__this->___healthStatsVisible_4);
		bool L_8 = GUILayout_Toggle_m6_1119(NULL /*static, unused*/, L_7, _stringLiteral3470, ((GUILayoutOptionU5BU5D_t6_165*)SZArrayNew(GUILayoutOptionU5BU5D_t6_165_il2cpp_TypeInfo_var, 0)), /*hidden argument*/NULL);
		__this->___healthStatsVisible_4 = L_8;
		bool L_9 = (__this->___trafficStatsOn_5);
		bool L_10 = GUILayout_Toggle_m6_1119(NULL /*static, unused*/, L_9, _stringLiteral3471, ((GUILayoutOptionU5BU5D_t6_165*)SZArrayNew(GUILayoutOptionU5BU5D_t6_165_il2cpp_TypeInfo_var, 0)), /*hidden argument*/NULL);
		__this->___trafficStatsOn_5 = L_10;
		GUILayout_EndHorizontal_m6_1129(NULL /*static, unused*/, /*hidden argument*/NULL);
		TrafficStatsGameLevel_t5_15 * L_11 = V_1;
		NullCheck(L_11);
		int32_t L_12 = TrafficStatsGameLevel_get_TotalOutgoingMessageCount_m5_427(L_11, /*hidden argument*/NULL);
		int32_t L_13 = L_12;
		Object_t * L_14 = Box(Int32_t1_3_il2cpp_TypeInfo_var, &L_13);
		TrafficStatsGameLevel_t5_15 * L_15 = V_1;
		NullCheck(L_15);
		int32_t L_16 = TrafficStatsGameLevel_get_TotalIncomingMessageCount_m5_426(L_15, /*hidden argument*/NULL);
		int32_t L_17 = L_16;
		Object_t * L_18 = Box(Int32_t1_3_il2cpp_TypeInfo_var, &L_17);
		TrafficStatsGameLevel_t5_15 * L_19 = V_1;
		NullCheck(L_19);
		int32_t L_20 = TrafficStatsGameLevel_get_TotalMessageCount_m5_425(L_19, /*hidden argument*/NULL);
		int32_t L_21 = L_20;
		Object_t * L_22 = Box(Int32_t1_3_il2cpp_TypeInfo_var, &L_21);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_23 = String_Format_m1_412(NULL /*static, unused*/, _stringLiteral3472, L_14, L_18, L_22, /*hidden argument*/NULL);
		V_3 = L_23;
		int64_t L_24 = V_2;
		int64_t L_25 = L_24;
		Object_t * L_26 = Box(Int64_t1_7_il2cpp_TypeInfo_var, &L_25);
		String_t* L_27 = String_Format_m1_410(NULL /*static, unused*/, _stringLiteral3473, L_26, /*hidden argument*/NULL);
		V_4 = L_27;
		TrafficStatsGameLevel_t5_15 * L_28 = V_1;
		NullCheck(L_28);
		int32_t L_29 = TrafficStatsGameLevel_get_TotalOutgoingMessageCount_m5_427(L_28, /*hidden argument*/NULL);
		int64_t L_30 = V_2;
		int64_t L_31 = ((int64_t)((int64_t)(((int64_t)((int64_t)L_29)))/(int64_t)L_30));
		Object_t * L_32 = Box(Int64_t1_7_il2cpp_TypeInfo_var, &L_31);
		TrafficStatsGameLevel_t5_15 * L_33 = V_1;
		NullCheck(L_33);
		int32_t L_34 = TrafficStatsGameLevel_get_TotalIncomingMessageCount_m5_426(L_33, /*hidden argument*/NULL);
		int64_t L_35 = V_2;
		int64_t L_36 = ((int64_t)((int64_t)(((int64_t)((int64_t)L_34)))/(int64_t)L_35));
		Object_t * L_37 = Box(Int64_t1_7_il2cpp_TypeInfo_var, &L_36);
		TrafficStatsGameLevel_t5_15 * L_38 = V_1;
		NullCheck(L_38);
		int32_t L_39 = TrafficStatsGameLevel_get_TotalMessageCount_m5_425(L_38, /*hidden argument*/NULL);
		int64_t L_40 = V_2;
		int64_t L_41 = ((int64_t)((int64_t)(((int64_t)((int64_t)L_39)))/(int64_t)L_40));
		Object_t * L_42 = Box(Int64_t1_7_il2cpp_TypeInfo_var, &L_41);
		String_t* L_43 = String_Format_m1_412(NULL /*static, unused*/, _stringLiteral3472, L_32, L_37, L_42, /*hidden argument*/NULL);
		V_5 = L_43;
		String_t* L_44 = V_3;
		GUILayout_Label_m6_1112(NULL /*static, unused*/, L_44, ((GUILayoutOptionU5BU5D_t6_165*)SZArrayNew(GUILayoutOptionU5BU5D_t6_165_il2cpp_TypeInfo_var, 0)), /*hidden argument*/NULL);
		String_t* L_45 = V_4;
		GUILayout_Label_m6_1112(NULL /*static, unused*/, L_45, ((GUILayoutOptionU5BU5D_t6_165*)SZArrayNew(GUILayoutOptionU5BU5D_t6_165_il2cpp_TypeInfo_var, 0)), /*hidden argument*/NULL);
		String_t* L_46 = V_5;
		GUILayout_Label_m6_1112(NULL /*static, unused*/, L_46, ((GUILayoutOptionU5BU5D_t6_165*)SZArrayNew(GUILayoutOptionU5BU5D_t6_165_il2cpp_TypeInfo_var, 0)), /*hidden argument*/NULL);
		bool L_47 = (__this->___buttonsOn_6);
		if (!L_47)
		{
			goto IL_0198;
		}
	}
	{
		GUILayout_BeginHorizontal_m6_1127(NULL /*static, unused*/, ((GUILayoutOptionU5BU5D_t6_165*)SZArrayNew(GUILayoutOptionU5BU5D_t6_165_il2cpp_TypeInfo_var, 0)), /*hidden argument*/NULL);
		bool L_48 = (__this->___statsOn_3);
		bool L_49 = GUILayout_Toggle_m6_1119(NULL /*static, unused*/, L_48, _stringLiteral3474, ((GUILayoutOptionU5BU5D_t6_165*)SZArrayNew(GUILayoutOptionU5BU5D_t6_165_il2cpp_TypeInfo_var, 0)), /*hidden argument*/NULL);
		__this->___statsOn_3 = L_49;
		bool L_50 = GUILayout_Button_m6_1115(NULL /*static, unused*/, _stringLiteral3475, ((GUILayoutOptionU5BU5D_t6_165*)SZArrayNew(GUILayoutOptionU5BU5D_t6_165_il2cpp_TypeInfo_var, 0)), /*hidden argument*/NULL);
		if (!L_50)
		{
			goto IL_0182;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(PhotonNetwork_t8_114_il2cpp_TypeInfo_var);
		NetworkingPeer_t8_98 * L_51 = ((PhotonNetwork_t8_114_StaticFields*)PhotonNetwork_t8_114_il2cpp_TypeInfo_var->static_fields)->___networkingPeer_4;
		NullCheck(L_51);
		PhotonPeer_TrafficStatsReset_m5_205(L_51, /*hidden argument*/NULL);
		NetworkingPeer_t8_98 * L_52 = ((PhotonNetwork_t8_114_StaticFields*)PhotonNetwork_t8_114_il2cpp_TypeInfo_var->static_fields)->___networkingPeer_4;
		NullCheck(L_52);
		PhotonPeer_set_TrafficStatsEnabled_m5_203(L_52, 1, /*hidden argument*/NULL);
	}

IL_0182:
	{
		bool L_53 = GUILayout_Button_m6_1115(NULL /*static, unused*/, _stringLiteral3476, ((GUILayoutOptionU5BU5D_t6_165*)SZArrayNew(GUILayoutOptionU5BU5D_t6_165_il2cpp_TypeInfo_var, 0)), /*hidden argument*/NULL);
		V_0 = L_53;
		GUILayout_EndHorizontal_m6_1129(NULL /*static, unused*/, /*hidden argument*/NULL);
	}

IL_0198:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_54 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->___Empty_2;
		V_6 = L_54;
		String_t* L_55 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->___Empty_2;
		V_7 = L_55;
		bool L_56 = (__this->___trafficStatsOn_5);
		if (!L_56)
		{
			goto IL_0201;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(PhotonNetwork_t8_114_il2cpp_TypeInfo_var);
		NetworkingPeer_t8_98 * L_57 = ((PhotonNetwork_t8_114_StaticFields*)PhotonNetwork_t8_114_il2cpp_TypeInfo_var->static_fields)->___networkingPeer_4;
		NullCheck(L_57);
		TrafficStats_t5_14 * L_58 = PhotonPeer_get_TrafficStatsIncoming_m5_206(L_57, /*hidden argument*/NULL);
		NullCheck(L_58);
		String_t* L_59 = (String_t*)VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String ExitGames.Client.Photon.TrafficStats::ToString() */, L_58);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_60 = String_Concat_m1_418(NULL /*static, unused*/, _stringLiteral3477, L_59, /*hidden argument*/NULL);
		V_6 = L_60;
		NetworkingPeer_t8_98 * L_61 = ((PhotonNetwork_t8_114_StaticFields*)PhotonNetwork_t8_114_il2cpp_TypeInfo_var->static_fields)->___networkingPeer_4;
		NullCheck(L_61);
		TrafficStats_t5_14 * L_62 = PhotonPeer_get_TrafficStatsOutgoing_m5_207(L_61, /*hidden argument*/NULL);
		NullCheck(L_62);
		String_t* L_63 = (String_t*)VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String ExitGames.Client.Photon.TrafficStats::ToString() */, L_62);
		String_t* L_64 = String_Concat_m1_418(NULL /*static, unused*/, _stringLiteral3478, L_63, /*hidden argument*/NULL);
		V_7 = L_64;
		String_t* L_65 = V_6;
		GUILayout_Label_m6_1112(NULL /*static, unused*/, L_65, ((GUILayoutOptionU5BU5D_t6_165*)SZArrayNew(GUILayoutOptionU5BU5D_t6_165_il2cpp_TypeInfo_var, 0)), /*hidden argument*/NULL);
		String_t* L_66 = V_7;
		GUILayout_Label_m6_1112(NULL /*static, unused*/, L_66, ((GUILayoutOptionU5BU5D_t6_165*)SZArrayNew(GUILayoutOptionU5BU5D_t6_165_il2cpp_TypeInfo_var, 0)), /*hidden argument*/NULL);
	}

IL_0201:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_67 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->___Empty_2;
		V_8 = L_67;
		bool L_68 = (__this->___healthStatsVisible_4);
		if (!L_68)
		{
			goto IL_02bd;
		}
	}
	{
		ObjectU5BU5D_t1_157* L_69 = ((ObjectU5BU5D_t1_157*)SZArrayNew(ObjectU5BU5D_t1_157_il2cpp_TypeInfo_var, ((int32_t)9)));
		TrafficStatsGameLevel_t5_15 * L_70 = V_1;
		NullCheck(L_70);
		int32_t L_71 = TrafficStatsGameLevel_get_LongestDeltaBetweenSending_m5_419(L_70, /*hidden argument*/NULL);
		int32_t L_72 = L_71;
		Object_t * L_73 = Box(Int32_t1_3_il2cpp_TypeInfo_var, &L_72);
		NullCheck(L_69);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_69, 0);
		ArrayElementTypeCheck (L_69, L_73);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_69, 0, sizeof(Object_t *))) = (Object_t *)L_73;
		ObjectU5BU5D_t1_157* L_74 = L_69;
		TrafficStatsGameLevel_t5_15 * L_75 = V_1;
		NullCheck(L_75);
		int32_t L_76 = TrafficStatsGameLevel_get_LongestDeltaBetweenDispatching_m5_417(L_75, /*hidden argument*/NULL);
		int32_t L_77 = L_76;
		Object_t * L_78 = Box(Int32_t1_3_il2cpp_TypeInfo_var, &L_77);
		NullCheck(L_74);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_74, 1);
		ArrayElementTypeCheck (L_74, L_78);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_74, 1, sizeof(Object_t *))) = (Object_t *)L_78;
		ObjectU5BU5D_t1_157* L_79 = L_74;
		TrafficStatsGameLevel_t5_15 * L_80 = V_1;
		NullCheck(L_80);
		int32_t L_81 = TrafficStatsGameLevel_get_LongestEventCallback_m5_413(L_80, /*hidden argument*/NULL);
		int32_t L_82 = L_81;
		Object_t * L_83 = Box(Int32_t1_3_il2cpp_TypeInfo_var, &L_82);
		NullCheck(L_79);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_79, 2);
		ArrayElementTypeCheck (L_79, L_83);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_79, 2, sizeof(Object_t *))) = (Object_t *)L_83;
		ObjectU5BU5D_t1_157* L_84 = L_79;
		TrafficStatsGameLevel_t5_15 * L_85 = V_1;
		NullCheck(L_85);
		uint8_t L_86 = TrafficStatsGameLevel_get_LongestEventCallbackCode_m5_415(L_85, /*hidden argument*/NULL);
		uint8_t L_87 = L_86;
		Object_t * L_88 = Box(Byte_t1_11_il2cpp_TypeInfo_var, &L_87);
		NullCheck(L_84);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_84, 3);
		ArrayElementTypeCheck (L_84, L_88);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_84, 3, sizeof(Object_t *))) = (Object_t *)L_88;
		ObjectU5BU5D_t1_157* L_89 = L_84;
		TrafficStatsGameLevel_t5_15 * L_90 = V_1;
		NullCheck(L_90);
		int32_t L_91 = TrafficStatsGameLevel_get_LongestOpResponseCallback_m5_409(L_90, /*hidden argument*/NULL);
		int32_t L_92 = L_91;
		Object_t * L_93 = Box(Int32_t1_3_il2cpp_TypeInfo_var, &L_92);
		NullCheck(L_89);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_89, 4);
		ArrayElementTypeCheck (L_89, L_93);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_89, 4, sizeof(Object_t *))) = (Object_t *)L_93;
		ObjectU5BU5D_t1_157* L_94 = L_89;
		TrafficStatsGameLevel_t5_15 * L_95 = V_1;
		NullCheck(L_95);
		uint8_t L_96 = TrafficStatsGameLevel_get_LongestOpResponseCallbackOpCode_m5_411(L_95, /*hidden argument*/NULL);
		uint8_t L_97 = L_96;
		Object_t * L_98 = Box(Byte_t1_11_il2cpp_TypeInfo_var, &L_97);
		NullCheck(L_94);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_94, 5);
		ArrayElementTypeCheck (L_94, L_98);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_94, 5, sizeof(Object_t *))) = (Object_t *)L_98;
		ObjectU5BU5D_t1_157* L_99 = L_94;
		IL2CPP_RUNTIME_CLASS_INIT(PhotonNetwork_t8_114_il2cpp_TypeInfo_var);
		NetworkingPeer_t8_98 * L_100 = ((PhotonNetwork_t8_114_StaticFields*)PhotonNetwork_t8_114_il2cpp_TypeInfo_var->static_fields)->___networkingPeer_4;
		NullCheck(L_100);
		int32_t L_101 = PhotonPeer_get_RoundTripTime_m5_224(L_100, /*hidden argument*/NULL);
		int32_t L_102 = L_101;
		Object_t * L_103 = Box(Int32_t1_3_il2cpp_TypeInfo_var, &L_102);
		NullCheck(L_99);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_99, 6);
		ArrayElementTypeCheck (L_99, L_103);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_99, 6, sizeof(Object_t *))) = (Object_t *)L_103;
		ObjectU5BU5D_t1_157* L_104 = L_99;
		NetworkingPeer_t8_98 * L_105 = ((PhotonNetwork_t8_114_StaticFields*)PhotonNetwork_t8_114_il2cpp_TypeInfo_var->static_fields)->___networkingPeer_4;
		NullCheck(L_105);
		int32_t L_106 = PhotonPeer_get_RoundTripTimeVariance_m5_225(L_105, /*hidden argument*/NULL);
		int32_t L_107 = L_106;
		Object_t * L_108 = Box(Int32_t1_3_il2cpp_TypeInfo_var, &L_107);
		NullCheck(L_104);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_104, 7);
		ArrayElementTypeCheck (L_104, L_108);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_104, 7, sizeof(Object_t *))) = (Object_t *)L_108;
		ObjectU5BU5D_t1_157* L_109 = L_104;
		NetworkingPeer_t8_98 * L_110 = ((PhotonNetwork_t8_114_StaticFields*)PhotonNetwork_t8_114_il2cpp_TypeInfo_var->static_fields)->___networkingPeer_4;
		NullCheck(L_110);
		int32_t L_111 = PhotonPeer_get_ResentReliableCommands_m5_217(L_110, /*hidden argument*/NULL);
		int32_t L_112 = L_111;
		Object_t * L_113 = Box(Int32_t1_3_il2cpp_TypeInfo_var, &L_112);
		NullCheck(L_109);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_109, 8);
		ArrayElementTypeCheck (L_109, L_113);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_109, 8, sizeof(Object_t *))) = (Object_t *)L_113;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_114 = String_Format_m1_413(NULL /*static, unused*/, _stringLiteral3479, L_109, /*hidden argument*/NULL);
		V_8 = L_114;
		String_t* L_115 = V_8;
		GUILayout_Label_m6_1112(NULL /*static, unused*/, L_115, ((GUILayoutOptionU5BU5D_t6_165*)SZArrayNew(GUILayoutOptionU5BU5D_t6_165_il2cpp_TypeInfo_var, 0)), /*hidden argument*/NULL);
	}

IL_02bd:
	{
		bool L_116 = V_0;
		if (!L_116)
		{
			goto IL_02f9;
		}
	}
	{
		ObjectU5BU5D_t1_157* L_117 = ((ObjectU5BU5D_t1_157*)SZArrayNew(ObjectU5BU5D_t1_157_il2cpp_TypeInfo_var, 6));
		String_t* L_118 = V_3;
		NullCheck(L_117);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_117, 0);
		ArrayElementTypeCheck (L_117, L_118);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_117, 0, sizeof(Object_t *))) = (Object_t *)L_118;
		ObjectU5BU5D_t1_157* L_119 = L_117;
		String_t* L_120 = V_4;
		NullCheck(L_119);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_119, 1);
		ArrayElementTypeCheck (L_119, L_120);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_119, 1, sizeof(Object_t *))) = (Object_t *)L_120;
		ObjectU5BU5D_t1_157* L_121 = L_119;
		String_t* L_122 = V_5;
		NullCheck(L_121);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_121, 2);
		ArrayElementTypeCheck (L_121, L_122);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_121, 2, sizeof(Object_t *))) = (Object_t *)L_122;
		ObjectU5BU5D_t1_157* L_123 = L_121;
		String_t* L_124 = V_6;
		NullCheck(L_123);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_123, 3);
		ArrayElementTypeCheck (L_123, L_124);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_123, 3, sizeof(Object_t *))) = (Object_t *)L_124;
		ObjectU5BU5D_t1_157* L_125 = L_123;
		String_t* L_126 = V_7;
		NullCheck(L_125);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_125, 4);
		ArrayElementTypeCheck (L_125, L_126);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_125, 4, sizeof(Object_t *))) = (Object_t *)L_126;
		ObjectU5BU5D_t1_157* L_127 = L_125;
		String_t* L_128 = V_8;
		NullCheck(L_127);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_127, 5);
		ArrayElementTypeCheck (L_127, L_128);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_127, 5, sizeof(Object_t *))) = (Object_t *)L_128;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_129 = String_Format_m1_413(NULL /*static, unused*/, _stringLiteral3480, L_127, /*hidden argument*/NULL);
		V_9 = L_129;
		String_t* L_130 = V_9;
		Debug_Log_m6_489(NULL /*static, unused*/, L_130, /*hidden argument*/NULL);
	}

IL_02f9:
	{
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t6_160_il2cpp_TypeInfo_var);
		bool L_131 = GUI_get_changed_m6_1080(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (!L_131)
		{
			goto IL_0313;
		}
	}
	{
		Rect_t6_52 * L_132 = &(__this->___statsRect_7);
		Rect_set_height_m6_280(L_132, (100.0f), /*hidden argument*/NULL);
	}

IL_0313:
	{
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t6_160_il2cpp_TypeInfo_var);
		GUI_DragWindow_m6_1077(NULL /*static, unused*/, /*hidden argument*/NULL);
		return;
	}
}
// System.Void PhotonStreamQueue::.ctor(System.Int32)
extern TypeInfo* List_1_t1_931_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1__ctor_m1_5617_MethodInfo_var;
extern "C" void PhotonStreamQueue__ctor_m8_754 (PhotonStreamQueue_t8_117 * __this, int32_t ___sampleRate, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		List_1_t1_931_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1049);
		List_1__ctor_m1_5617_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483829);
		s_Il2CppMethodIntialized = true;
	}
	{
		__this->___m_ObjectsPerSample_2 = (-1);
		__this->___m_LastSampleTime_3 = (-std::numeric_limits<float>::infinity());
		__this->___m_LastFrameCount_4 = (-1);
		__this->___m_NextObjectIndex_5 = (-1);
		List_1_t1_931 * L_0 = (List_1_t1_931 *)il2cpp_codegen_object_new (List_1_t1_931_il2cpp_TypeInfo_var);
		List_1__ctor_m1_5617(L_0, /*hidden argument*/List_1__ctor_m1_5617_MethodInfo_var);
		__this->___m_Objects_6 = L_0;
		Object__ctor_m1_0(__this, /*hidden argument*/NULL);
		int32_t L_1 = ___sampleRate;
		__this->___m_SampleRate_0 = L_1;
		return;
	}
}
// System.Void PhotonStreamQueue::BeginWritePackage()
extern TypeInfo* ObjectU5BU5D_t1_157_il2cpp_TypeInfo_var;
extern TypeInfo* Int32_t1_3_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3481;
extern Il2CppCodeGenString* _stringLiteral3482;
extern Il2CppCodeGenString* _stringLiteral3483;
extern Il2CppCodeGenString* _stringLiteral1158;
extern Il2CppCodeGenString* _stringLiteral3484;
extern "C" void PhotonStreamQueue_BeginWritePackage_m8_755 (PhotonStreamQueue_t8_117 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ObjectU5BU5D_t1_157_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(19);
		Int32_t1_3_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(8);
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(11);
		_stringLiteral3481 = il2cpp_codegen_string_literal_from_index(3481);
		_stringLiteral3482 = il2cpp_codegen_string_literal_from_index(3482);
		_stringLiteral3483 = il2cpp_codegen_string_literal_from_index(3483);
		_stringLiteral1158 = il2cpp_codegen_string_literal_from_index(1158);
		_stringLiteral3484 = il2cpp_codegen_string_literal_from_index(3484);
		s_Il2CppMethodIntialized = true;
	}
	{
		float L_0 = Time_get_realtimeSinceStartup_m6_656(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_1 = (__this->___m_LastSampleTime_3);
		int32_t L_2 = (__this->___m_SampleRate_0);
		if ((!(((float)L_0) < ((float)((float)((float)L_1+(float)((float)((float)(1.0f)/(float)(((float)((float)L_2)))))))))))
		{
			goto IL_0026;
		}
	}
	{
		__this->___m_IsWriting_7 = 0;
		return;
	}

IL_0026:
	{
		int32_t L_3 = (__this->___m_SampleCount_1);
		if ((!(((uint32_t)L_3) == ((uint32_t)1))))
		{
			goto IL_0048;
		}
	}
	{
		List_1_t1_931 * L_4 = (__this->___m_Objects_6);
		NullCheck(L_4);
		int32_t L_5 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(17 /* System.Int32 System.Collections.Generic.List`1<System.Object>::get_Count() */, L_4);
		__this->___m_ObjectsPerSample_2 = L_5;
		goto IL_00f4;
	}

IL_0048:
	{
		int32_t L_6 = (__this->___m_SampleCount_1);
		if ((((int32_t)L_6) <= ((int32_t)1)))
		{
			goto IL_00f4;
		}
	}
	{
		List_1_t1_931 * L_7 = (__this->___m_Objects_6);
		NullCheck(L_7);
		int32_t L_8 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(17 /* System.Int32 System.Collections.Generic.List`1<System.Object>::get_Count() */, L_7);
		int32_t L_9 = (__this->___m_SampleCount_1);
		int32_t L_10 = (__this->___m_ObjectsPerSample_2);
		if ((((int32_t)((int32_t)((int32_t)L_8/(int32_t)L_9))) == ((int32_t)L_10)))
		{
			goto IL_00f4;
		}
	}
	{
		Debug_LogWarning_m6_491(NULL /*static, unused*/, _stringLiteral3481, /*hidden argument*/NULL);
		ObjectU5BU5D_t1_157* L_11 = ((ObjectU5BU5D_t1_157*)SZArrayNew(ObjectU5BU5D_t1_157_il2cpp_TypeInfo_var, 8));
		NullCheck(L_11);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_11, 0);
		ArrayElementTypeCheck (L_11, _stringLiteral3482);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_11, 0, sizeof(Object_t *))) = (Object_t *)_stringLiteral3482;
		ObjectU5BU5D_t1_157* L_12 = L_11;
		List_1_t1_931 * L_13 = (__this->___m_Objects_6);
		NullCheck(L_13);
		int32_t L_14 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(17 /* System.Int32 System.Collections.Generic.List`1<System.Object>::get_Count() */, L_13);
		int32_t L_15 = L_14;
		Object_t * L_16 = Box(Int32_t1_3_il2cpp_TypeInfo_var, &L_15);
		NullCheck(L_12);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_12, 1);
		ArrayElementTypeCheck (L_12, L_16);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_12, 1, sizeof(Object_t *))) = (Object_t *)L_16;
		ObjectU5BU5D_t1_157* L_17 = L_12;
		NullCheck(L_17);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_17, 2);
		ArrayElementTypeCheck (L_17, _stringLiteral3483);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_17, 2, sizeof(Object_t *))) = (Object_t *)_stringLiteral3483;
		ObjectU5BU5D_t1_157* L_18 = L_17;
		int32_t L_19 = (__this->___m_SampleCount_1);
		int32_t L_20 = L_19;
		Object_t * L_21 = Box(Int32_t1_3_il2cpp_TypeInfo_var, &L_20);
		NullCheck(L_18);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_18, 3);
		ArrayElementTypeCheck (L_18, L_21);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_18, 3, sizeof(Object_t *))) = (Object_t *)L_21;
		ObjectU5BU5D_t1_157* L_22 = L_18;
		NullCheck(L_22);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_22, 4);
		ArrayElementTypeCheck (L_22, _stringLiteral1158);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_22, 4, sizeof(Object_t *))) = (Object_t *)_stringLiteral1158;
		ObjectU5BU5D_t1_157* L_23 = L_22;
		List_1_t1_931 * L_24 = (__this->___m_Objects_6);
		NullCheck(L_24);
		int32_t L_25 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(17 /* System.Int32 System.Collections.Generic.List`1<System.Object>::get_Count() */, L_24);
		int32_t L_26 = (__this->___m_SampleCount_1);
		int32_t L_27 = ((int32_t)((int32_t)L_25/(int32_t)L_26));
		Object_t * L_28 = Box(Int32_t1_3_il2cpp_TypeInfo_var, &L_27);
		NullCheck(L_23);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_23, 5);
		ArrayElementTypeCheck (L_23, L_28);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_23, 5, sizeof(Object_t *))) = (Object_t *)L_28;
		ObjectU5BU5D_t1_157* L_29 = L_23;
		NullCheck(L_29);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_29, 6);
		ArrayElementTypeCheck (L_29, _stringLiteral3484);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_29, 6, sizeof(Object_t *))) = (Object_t *)_stringLiteral3484;
		ObjectU5BU5D_t1_157* L_30 = L_29;
		int32_t L_31 = (__this->___m_ObjectsPerSample_2);
		int32_t L_32 = L_31;
		Object_t * L_33 = Box(Int32_t1_3_il2cpp_TypeInfo_var, &L_32);
		NullCheck(L_30);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_30, 7);
		ArrayElementTypeCheck (L_30, L_33);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_30, 7, sizeof(Object_t *))) = (Object_t *)L_33;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_34 = String_Concat_m1_421(NULL /*static, unused*/, L_30, /*hidden argument*/NULL);
		Debug_LogWarning_m6_491(NULL /*static, unused*/, L_34, /*hidden argument*/NULL);
	}

IL_00f4:
	{
		__this->___m_IsWriting_7 = 1;
		int32_t L_35 = (__this->___m_SampleCount_1);
		__this->___m_SampleCount_1 = ((int32_t)((int32_t)L_35+(int32_t)1));
		float L_36 = Time_get_realtimeSinceStartup_m6_656(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->___m_LastSampleTime_3 = L_36;
		return;
	}
}
// System.Void PhotonStreamQueue::Reset()
extern "C" void PhotonStreamQueue_Reset_m8_756 (PhotonStreamQueue_t8_117 * __this, const MethodInfo* method)
{
	{
		__this->___m_SampleCount_1 = 0;
		__this->___m_ObjectsPerSample_2 = (-1);
		__this->___m_LastSampleTime_3 = (-std::numeric_limits<float>::infinity());
		__this->___m_LastFrameCount_4 = (-1);
		List_1_t1_931 * L_0 = (__this->___m_Objects_6);
		NullCheck(L_0);
		VirtActionInvoker0::Invoke(20 /* System.Void System.Collections.Generic.List`1<System.Object>::Clear() */, L_0);
		return;
	}
}
// System.Void PhotonStreamQueue::SendNext(System.Object)
extern "C" void PhotonStreamQueue_SendNext_m8_757 (PhotonStreamQueue_t8_117 * __this, Object_t * ___obj, const MethodInfo* method)
{
	{
		int32_t L_0 = Time_get_frameCount_m6_655(NULL /*static, unused*/, /*hidden argument*/NULL);
		int32_t L_1 = (__this->___m_LastFrameCount_4);
		if ((((int32_t)L_0) == ((int32_t)L_1)))
		{
			goto IL_0016;
		}
	}
	{
		PhotonStreamQueue_BeginWritePackage_m8_755(__this, /*hidden argument*/NULL);
	}

IL_0016:
	{
		int32_t L_2 = Time_get_frameCount_m6_655(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->___m_LastFrameCount_4 = L_2;
		bool L_3 = (__this->___m_IsWriting_7);
		if (L_3)
		{
			goto IL_002d;
		}
	}
	{
		return;
	}

IL_002d:
	{
		List_1_t1_931 * L_4 = (__this->___m_Objects_6);
		Object_t * L_5 = ___obj;
		NullCheck(L_4);
		VirtActionInvoker1< Object_t * >::Invoke(19 /* System.Void System.Collections.Generic.List`1<System.Object>::Add(!0) */, L_4, L_5);
		return;
	}
}
// System.Boolean PhotonStreamQueue::HasQueuedObjects()
extern "C" bool PhotonStreamQueue_HasQueuedObjects_m8_758 (PhotonStreamQueue_t8_117 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (__this->___m_NextObjectIndex_5);
		return ((((int32_t)((((int32_t)L_0) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
	}
}
// System.Object PhotonStreamQueue::ReceiveNext()
extern "C" Object_t * PhotonStreamQueue_ReceiveNext_m8_759 (PhotonStreamQueue_t8_117 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		int32_t L_0 = (__this->___m_NextObjectIndex_5);
		if ((!(((uint32_t)L_0) == ((uint32_t)(-1)))))
		{
			goto IL_000e;
		}
	}
	{
		return NULL;
	}

IL_000e:
	{
		int32_t L_1 = (__this->___m_NextObjectIndex_5);
		List_1_t1_931 * L_2 = (__this->___m_Objects_6);
		NullCheck(L_2);
		int32_t L_3 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(17 /* System.Int32 System.Collections.Generic.List`1<System.Object>::get_Count() */, L_2);
		if ((((int32_t)L_1) < ((int32_t)L_3)))
		{
			goto IL_0037;
		}
	}
	{
		int32_t L_4 = (__this->___m_NextObjectIndex_5);
		int32_t L_5 = (__this->___m_ObjectsPerSample_2);
		__this->___m_NextObjectIndex_5 = ((int32_t)((int32_t)L_4-(int32_t)L_5));
	}

IL_0037:
	{
		List_1_t1_931 * L_6 = (__this->___m_Objects_6);
		int32_t L_7 = (__this->___m_NextObjectIndex_5);
		int32_t L_8 = L_7;
		V_0 = L_8;
		__this->___m_NextObjectIndex_5 = ((int32_t)((int32_t)L_8+(int32_t)1));
		int32_t L_9 = V_0;
		NullCheck(L_6);
		Object_t * L_10 = (Object_t *)VirtFuncInvoker1< Object_t *, int32_t >::Invoke(28 /* !0 System.Collections.Generic.List`1<System.Object>::get_Item(System.Int32) */, L_6, L_9);
		return L_10;
	}
}
// System.Void PhotonStreamQueue::Serialize(PhotonStream)
extern TypeInfo* Int32_t1_3_il2cpp_TypeInfo_var;
extern "C" void PhotonStreamQueue_Serialize_m8_760 (PhotonStreamQueue_t8_117 * __this, PhotonStream_t8_106 * ___stream, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Int32_t1_3_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(8);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	{
		List_1_t1_931 * L_0 = (__this->___m_Objects_6);
		NullCheck(L_0);
		int32_t L_1 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(17 /* System.Int32 System.Collections.Generic.List`1<System.Object>::get_Count() */, L_0);
		if ((((int32_t)L_1) <= ((int32_t)0)))
		{
			goto IL_002e;
		}
	}
	{
		int32_t L_2 = (__this->___m_ObjectsPerSample_2);
		if ((((int32_t)L_2) >= ((int32_t)0)))
		{
			goto IL_002e;
		}
	}
	{
		List_1_t1_931 * L_3 = (__this->___m_Objects_6);
		NullCheck(L_3);
		int32_t L_4 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(17 /* System.Int32 System.Collections.Generic.List`1<System.Object>::get_Count() */, L_3);
		__this->___m_ObjectsPerSample_2 = L_4;
	}

IL_002e:
	{
		PhotonStream_t8_106 * L_5 = ___stream;
		int32_t L_6 = (__this->___m_SampleCount_1);
		int32_t L_7 = L_6;
		Object_t * L_8 = Box(Int32_t1_3_il2cpp_TypeInfo_var, &L_7);
		NullCheck(L_5);
		PhotonStream_SendNext_m8_543(L_5, L_8, /*hidden argument*/NULL);
		PhotonStream_t8_106 * L_9 = ___stream;
		int32_t L_10 = (__this->___m_ObjectsPerSample_2);
		int32_t L_11 = L_10;
		Object_t * L_12 = Box(Int32_t1_3_il2cpp_TypeInfo_var, &L_11);
		NullCheck(L_9);
		PhotonStream_SendNext_m8_543(L_9, L_12, /*hidden argument*/NULL);
		V_0 = 0;
		goto IL_006d;
	}

IL_0057:
	{
		PhotonStream_t8_106 * L_13 = ___stream;
		List_1_t1_931 * L_14 = (__this->___m_Objects_6);
		int32_t L_15 = V_0;
		NullCheck(L_14);
		Object_t * L_16 = (Object_t *)VirtFuncInvoker1< Object_t *, int32_t >::Invoke(28 /* !0 System.Collections.Generic.List`1<System.Object>::get_Item(System.Int32) */, L_14, L_15);
		NullCheck(L_13);
		PhotonStream_SendNext_m8_543(L_13, L_16, /*hidden argument*/NULL);
		int32_t L_17 = V_0;
		V_0 = ((int32_t)((int32_t)L_17+(int32_t)1));
	}

IL_006d:
	{
		int32_t L_18 = V_0;
		List_1_t1_931 * L_19 = (__this->___m_Objects_6);
		NullCheck(L_19);
		int32_t L_20 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(17 /* System.Int32 System.Collections.Generic.List`1<System.Object>::get_Count() */, L_19);
		if ((((int32_t)L_18) < ((int32_t)L_20)))
		{
			goto IL_0057;
		}
	}
	{
		List_1_t1_931 * L_21 = (__this->___m_Objects_6);
		NullCheck(L_21);
		VirtActionInvoker0::Invoke(20 /* System.Void System.Collections.Generic.List`1<System.Object>::Clear() */, L_21);
		__this->___m_SampleCount_1 = 0;
		return;
	}
}
// System.Void PhotonStreamQueue::Deserialize(PhotonStream)
extern TypeInfo* Int32_t1_3_il2cpp_TypeInfo_var;
extern "C" void PhotonStreamQueue_Deserialize_m8_761 (PhotonStreamQueue_t8_117 * __this, PhotonStream_t8_106 * ___stream, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Int32_t1_3_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(8);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	{
		List_1_t1_931 * L_0 = (__this->___m_Objects_6);
		NullCheck(L_0);
		VirtActionInvoker0::Invoke(20 /* System.Void System.Collections.Generic.List`1<System.Object>::Clear() */, L_0);
		PhotonStream_t8_106 * L_1 = ___stream;
		NullCheck(L_1);
		Object_t * L_2 = PhotonStream_ReceiveNext_m8_541(L_1, /*hidden argument*/NULL);
		__this->___m_SampleCount_1 = ((*(int32_t*)((int32_t*)UnBox (L_2, Int32_t1_3_il2cpp_TypeInfo_var))));
		PhotonStream_t8_106 * L_3 = ___stream;
		NullCheck(L_3);
		Object_t * L_4 = PhotonStream_ReceiveNext_m8_541(L_3, /*hidden argument*/NULL);
		__this->___m_ObjectsPerSample_2 = ((*(int32_t*)((int32_t*)UnBox (L_4, Int32_t1_3_il2cpp_TypeInfo_var))));
		V_0 = 0;
		goto IL_0049;
	}

IL_0034:
	{
		List_1_t1_931 * L_5 = (__this->___m_Objects_6);
		PhotonStream_t8_106 * L_6 = ___stream;
		NullCheck(L_6);
		Object_t * L_7 = PhotonStream_ReceiveNext_m8_541(L_6, /*hidden argument*/NULL);
		NullCheck(L_5);
		VirtActionInvoker1< Object_t * >::Invoke(19 /* System.Void System.Collections.Generic.List`1<System.Object>::Add(!0) */, L_5, L_7);
		int32_t L_8 = V_0;
		V_0 = ((int32_t)((int32_t)L_8+(int32_t)1));
	}

IL_0049:
	{
		int32_t L_9 = V_0;
		int32_t L_10 = (__this->___m_SampleCount_1);
		int32_t L_11 = (__this->___m_ObjectsPerSample_2);
		if ((((int32_t)L_9) < ((int32_t)((int32_t)((int32_t)L_10*(int32_t)L_11)))))
		{
			goto IL_0034;
		}
	}
	{
		List_1_t1_931 * L_12 = (__this->___m_Objects_6);
		NullCheck(L_12);
		int32_t L_13 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(17 /* System.Int32 System.Collections.Generic.List`1<System.Object>::get_Count() */, L_12);
		if ((((int32_t)L_13) <= ((int32_t)0)))
		{
			goto IL_0079;
		}
	}
	{
		__this->___m_NextObjectIndex_5 = 0;
		goto IL_0080;
	}

IL_0079:
	{
		__this->___m_NextObjectIndex_5 = (-1);
	}

IL_0080:
	{
		return;
	}
}
// System.Void PhotonView::.ctor()
extern TypeInfo* Dictionary_2_t1_948_il2cpp_TypeInfo_var;
extern const MethodInfo* Dictionary_2__ctor_m1_5677_MethodInfo_var;
extern "C" void PhotonView__ctor_m8_762 (PhotonView_t8_3 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Dictionary_2_t1_948_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1160);
		Dictionary_2__ctor_m1_5677_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483940);
		s_Il2CppMethodIntialized = true;
	}
	{
		__this->___prefixBackup_5 = (-1);
		__this->___onSerializeTransformOption_11 = 3;
		__this->___onSerializeRigidBodyOption_12 = 2;
		Dictionary_2_t1_948 * L_0 = (Dictionary_2_t1_948 *)il2cpp_codegen_object_new (Dictionary_2_t1_948_il2cpp_TypeInfo_var);
		Dictionary_2__ctor_m1_5677(L_0, /*hidden argument*/Dictionary_2__ctor_m1_5677_MethodInfo_var);
		__this->___m_OnSerializeMethodInfos_15 = L_0;
		MonoBehaviour__ctor_m8_494(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Int32 PhotonView::get_prefix()
extern TypeInfo* PhotonNetwork_t8_114_il2cpp_TypeInfo_var;
extern "C" int32_t PhotonView_get_prefix_m8_763 (PhotonView_t8_3 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		PhotonNetwork_t8_114_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1058);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = (__this->___prefixBackup_5);
		if ((!(((uint32_t)L_0) == ((uint32_t)(-1)))))
		{
			goto IL_0026;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(PhotonNetwork_t8_114_il2cpp_TypeInfo_var);
		NetworkingPeer_t8_98 * L_1 = ((PhotonNetwork_t8_114_StaticFields*)PhotonNetwork_t8_114_il2cpp_TypeInfo_var->static_fields)->___networkingPeer_4;
		if (!L_1)
		{
			goto IL_0026;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(PhotonNetwork_t8_114_il2cpp_TypeInfo_var);
		NetworkingPeer_t8_98 * L_2 = ((PhotonNetwork_t8_114_StaticFields*)PhotonNetwork_t8_114_il2cpp_TypeInfo_var->static_fields)->___networkingPeer_4;
		NullCheck(L_2);
		int16_t L_3 = (L_2->___currentLevelPrefix_34);
		__this->___prefixBackup_5 = L_3;
	}

IL_0026:
	{
		int32_t L_4 = (__this->___prefixBackup_5);
		return L_4;
	}
}
// System.Void PhotonView::set_prefix(System.Int32)
extern "C" void PhotonView_set_prefix_m8_764 (PhotonView_t8_3 * __this, int32_t ___value, const MethodInfo* method)
{
	{
		int32_t L_0 = ___value;
		__this->___prefixBackup_5 = L_0;
		return;
	}
}
// System.Object[] PhotonView::get_instantiationData()
extern TypeInfo* PhotonNetwork_t8_114_il2cpp_TypeInfo_var;
extern "C" ObjectU5BU5D_t1_157* PhotonView_get_instantiationData_m8_765 (PhotonView_t8_3 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		PhotonNetwork_t8_114_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1058);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool L_0 = (__this->___didAwake_18);
		if (L_0)
		{
			goto IL_0021;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(PhotonNetwork_t8_114_il2cpp_TypeInfo_var);
		NetworkingPeer_t8_98 * L_1 = ((PhotonNetwork_t8_114_StaticFields*)PhotonNetwork_t8_114_il2cpp_TypeInfo_var->static_fields)->___networkingPeer_4;
		int32_t L_2 = (__this->___instantiationId_17);
		NullCheck(L_1);
		ObjectU5BU5D_t1_157* L_3 = NetworkingPeer_FetchInstantiationData_m8_452(L_1, L_2, /*hidden argument*/NULL);
		__this->___instantiationDataField_6 = L_3;
	}

IL_0021:
	{
		ObjectU5BU5D_t1_157* L_4 = (__this->___instantiationDataField_6);
		return L_4;
	}
}
// System.Void PhotonView::set_instantiationData(System.Object[])
extern "C" void PhotonView_set_instantiationData_m8_766 (PhotonView_t8_3 * __this, ObjectU5BU5D_t1_157* ___value, const MethodInfo* method)
{
	{
		ObjectU5BU5D_t1_157* L_0 = ___value;
		__this->___instantiationDataField_6 = L_0;
		return;
	}
}
// System.Int32 PhotonView::get_viewID()
extern "C" int32_t PhotonView_get_viewID_m8_767 (PhotonView_t8_3 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (__this->___viewIdField_16);
		return L_0;
	}
}
// System.Void PhotonView::set_viewID(System.Int32)
extern TypeInfo* PhotonNetwork_t8_114_il2cpp_TypeInfo_var;
extern "C" void PhotonView_set_viewID_m8_768 (PhotonView_t8_3 * __this, int32_t ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		PhotonNetwork_t8_114_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1058);
		s_Il2CppMethodIntialized = true;
	}
	bool V_0 = false;
	int32_t G_B3_0 = 0;
	{
		bool L_0 = (__this->___didAwake_18);
		if (!L_0)
		{
			goto IL_0016;
		}
	}
	{
		int32_t L_1 = (__this->___viewIdField_16);
		G_B3_0 = ((((int32_t)L_1) == ((int32_t)0))? 1 : 0);
		goto IL_0017;
	}

IL_0016:
	{
		G_B3_0 = 0;
	}

IL_0017:
	{
		V_0 = G_B3_0;
		int32_t L_2 = ___value;
		IL2CPP_RUNTIME_CLASS_INIT(PhotonNetwork_t8_114_il2cpp_TypeInfo_var);
		int32_t L_3 = ((PhotonNetwork_t8_114_StaticFields*)PhotonNetwork_t8_114_il2cpp_TypeInfo_var->static_fields)->___MAX_VIEW_IDS_5;
		__this->___ownerId_2 = ((int32_t)((int32_t)L_2/(int32_t)L_3));
		int32_t L_4 = ___value;
		__this->___viewIdField_16 = L_4;
		bool L_5 = V_0;
		if (!L_5)
		{
			goto IL_003d;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(PhotonNetwork_t8_114_il2cpp_TypeInfo_var);
		NetworkingPeer_t8_98 * L_6 = ((PhotonNetwork_t8_114_StaticFields*)PhotonNetwork_t8_114_il2cpp_TypeInfo_var->static_fields)->___networkingPeer_4;
		NullCheck(L_6);
		NetworkingPeer_RegisterPhotonView_m8_466(L_6, __this, /*hidden argument*/NULL);
	}

IL_003d:
	{
		return;
	}
}
// System.Boolean PhotonView::get_isSceneView()
extern "C" bool PhotonView_get_isSceneView_m8_769 (PhotonView_t8_3 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = PhotonView_get_CreatorActorNr_m8_773(__this, /*hidden argument*/NULL);
		return ((((int32_t)L_0) == ((int32_t)0))? 1 : 0);
	}
}
// PhotonPlayer PhotonView::get_owner()
extern "C" PhotonPlayer_t8_102 * PhotonView_get_owner_m8_770 (PhotonView_t8_3 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (__this->___ownerId_2);
		PhotonPlayer_t8_102 * L_1 = PhotonPlayer_Find_m8_742(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Int32 PhotonView::get_OwnerActorNr()
extern "C" int32_t PhotonView_get_OwnerActorNr_m8_771 (PhotonView_t8_3 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (__this->___ownerId_2);
		return L_0;
	}
}
// System.Boolean PhotonView::get_isOwnerActive()
extern TypeInfo* PhotonNetwork_t8_114_il2cpp_TypeInfo_var;
extern "C" bool PhotonView_get_isOwnerActive_m8_772 (PhotonView_t8_3 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		PhotonNetwork_t8_114_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1058);
		s_Il2CppMethodIntialized = true;
	}
	int32_t G_B3_0 = 0;
	{
		int32_t L_0 = (__this->___ownerId_2);
		if (!L_0)
		{
			goto IL_0022;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(PhotonNetwork_t8_114_il2cpp_TypeInfo_var);
		NetworkingPeer_t8_98 * L_1 = ((PhotonNetwork_t8_114_StaticFields*)PhotonNetwork_t8_114_il2cpp_TypeInfo_var->static_fields)->___networkingPeer_4;
		NullCheck(L_1);
		Dictionary_2_t1_936 * L_2 = (L_1->___mActors_25);
		int32_t L_3 = (__this->___ownerId_2);
		NullCheck(L_2);
		bool L_4 = (bool)VirtFuncInvoker1< bool, int32_t >::Invoke(28 /* System.Boolean System.Collections.Generic.Dictionary`2<System.Int32,PhotonPlayer>::ContainsKey(!0) */, L_2, L_3);
		G_B3_0 = ((int32_t)(L_4));
		goto IL_0023;
	}

IL_0022:
	{
		G_B3_0 = 0;
	}

IL_0023:
	{
		return G_B3_0;
	}
}
// System.Int32 PhotonView::get_CreatorActorNr()
extern TypeInfo* PhotonNetwork_t8_114_il2cpp_TypeInfo_var;
extern "C" int32_t PhotonView_get_CreatorActorNr_m8_773 (PhotonView_t8_3 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		PhotonNetwork_t8_114_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1058);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = (__this->___viewIdField_16);
		IL2CPP_RUNTIME_CLASS_INIT(PhotonNetwork_t8_114_il2cpp_TypeInfo_var);
		int32_t L_1 = ((PhotonNetwork_t8_114_StaticFields*)PhotonNetwork_t8_114_il2cpp_TypeInfo_var->static_fields)->___MAX_VIEW_IDS_5;
		return ((int32_t)((int32_t)L_0/(int32_t)L_1));
	}
}
// System.Boolean PhotonView::get_isMine()
extern TypeInfo* PhotonNetwork_t8_114_il2cpp_TypeInfo_var;
extern "C" bool PhotonView_get_isMine_m8_774 (PhotonView_t8_3 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		PhotonNetwork_t8_114_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1058);
		s_Il2CppMethodIntialized = true;
	}
	int32_t G_B4_0 = 0;
	int32_t G_B6_0 = 0;
	{
		int32_t L_0 = (__this->___ownerId_2);
		IL2CPP_RUNTIME_CLASS_INIT(PhotonNetwork_t8_114_il2cpp_TypeInfo_var);
		PhotonPlayer_t8_102 * L_1 = PhotonNetwork_get_player_m8_612(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_1);
		int32_t L_2 = PhotonPlayer_get_ID_m8_730(L_1, /*hidden argument*/NULL);
		if ((((int32_t)L_0) == ((int32_t)L_2)))
		{
			goto IL_002a;
		}
	}
	{
		bool L_3 = PhotonView_get_isOwnerActive_m8_772(__this, /*hidden argument*/NULL);
		if (L_3)
		{
			goto IL_0027;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(PhotonNetwork_t8_114_il2cpp_TypeInfo_var);
		bool L_4 = PhotonNetwork_get_isMasterClient_m8_648(NULL /*static, unused*/, /*hidden argument*/NULL);
		G_B4_0 = ((int32_t)(L_4));
		goto IL_0028;
	}

IL_0027:
	{
		G_B4_0 = 0;
	}

IL_0028:
	{
		G_B6_0 = G_B4_0;
		goto IL_002b;
	}

IL_002a:
	{
		G_B6_0 = 1;
	}

IL_002b:
	{
		return G_B6_0;
	}
}
// System.Void PhotonView::Awake()
extern TypeInfo* PhotonNetwork_t8_114_il2cpp_TypeInfo_var;
extern "C" void PhotonView_Awake_m8_775 (PhotonView_t8_3 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		PhotonNetwork_t8_114_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1058);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = PhotonView_get_viewID_m8_767(__this, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_002c;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(PhotonNetwork_t8_114_il2cpp_TypeInfo_var);
		NetworkingPeer_t8_98 * L_1 = ((PhotonNetwork_t8_114_StaticFields*)PhotonNetwork_t8_114_il2cpp_TypeInfo_var->static_fields)->___networkingPeer_4;
		NullCheck(L_1);
		NetworkingPeer_RegisterPhotonView_m8_466(L_1, __this, /*hidden argument*/NULL);
		NetworkingPeer_t8_98 * L_2 = ((PhotonNetwork_t8_114_StaticFields*)PhotonNetwork_t8_114_il2cpp_TypeInfo_var->static_fields)->___networkingPeer_4;
		int32_t L_3 = (__this->___instantiationId_17);
		NullCheck(L_2);
		ObjectU5BU5D_t1_157* L_4 = NetworkingPeer_FetchInstantiationData_m8_452(L_2, L_3, /*hidden argument*/NULL);
		__this->___instantiationDataField_6 = L_4;
	}

IL_002c:
	{
		__this->___didAwake_18 = 1;
		return;
	}
}
// System.Void PhotonView::RequestOwnership()
extern TypeInfo* PhotonNetwork_t8_114_il2cpp_TypeInfo_var;
extern "C" void PhotonView_RequestOwnership_m8_776 (PhotonView_t8_3 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		PhotonNetwork_t8_114_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1058);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(PhotonNetwork_t8_114_il2cpp_TypeInfo_var);
		NetworkingPeer_t8_98 * L_0 = ((PhotonNetwork_t8_114_StaticFields*)PhotonNetwork_t8_114_il2cpp_TypeInfo_var->static_fields)->___networkingPeer_4;
		int32_t L_1 = PhotonView_get_viewID_m8_767(__this, /*hidden argument*/NULL);
		int32_t L_2 = (__this->___ownerId_2);
		NullCheck(L_0);
		NetworkingPeer_RequestOwnership_m8_462(L_0, L_1, L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Void PhotonView::TransferOwnership(PhotonPlayer)
extern "C" void PhotonView_TransferOwnership_m8_777 (PhotonView_t8_3 * __this, PhotonPlayer_t8_102 * ___newOwner, const MethodInfo* method)
{
	{
		PhotonPlayer_t8_102 * L_0 = ___newOwner;
		NullCheck(L_0);
		int32_t L_1 = PhotonPlayer_get_ID_m8_730(L_0, /*hidden argument*/NULL);
		PhotonView_TransferOwnership_m8_778(__this, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void PhotonView::TransferOwnership(System.Int32)
extern TypeInfo* PhotonNetwork_t8_114_il2cpp_TypeInfo_var;
extern "C" void PhotonView_TransferOwnership_m8_778 (PhotonView_t8_3 * __this, int32_t ___newOwnerId, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		PhotonNetwork_t8_114_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1058);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(PhotonNetwork_t8_114_il2cpp_TypeInfo_var);
		NetworkingPeer_t8_98 * L_0 = ((PhotonNetwork_t8_114_StaticFields*)PhotonNetwork_t8_114_il2cpp_TypeInfo_var->static_fields)->___networkingPeer_4;
		int32_t L_1 = PhotonView_get_viewID_m8_767(__this, /*hidden argument*/NULL);
		int32_t L_2 = ___newOwnerId;
		NullCheck(L_0);
		NetworkingPeer_TransferOwnership_m8_463(L_0, L_1, L_2, /*hidden argument*/NULL);
		int32_t L_3 = ___newOwnerId;
		__this->___ownerId_2 = L_3;
		return;
	}
}
// System.Void PhotonView::OnDestroy()
extern TypeInfo* PhotonNetwork_t8_114_il2cpp_TypeInfo_var;
extern TypeInfo* PhotonHandler_t8_111_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3485;
extern Il2CppCodeGenString* _stringLiteral3486;
extern "C" void PhotonView_OnDestroy_m8_779 (PhotonView_t8_3 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		PhotonNetwork_t8_114_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1058);
		PhotonHandler_t8_111_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1120);
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(11);
		_stringLiteral3485 = il2cpp_codegen_string_literal_from_index(3485);
		_stringLiteral3486 = il2cpp_codegen_string_literal_from_index(3486);
		s_Il2CppMethodIntialized = true;
	}
	bool V_0 = false;
	bool V_1 = false;
	{
		bool L_0 = (__this->___removedFromLocalViewList_20);
		if (L_0)
		{
			goto IL_0065;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(PhotonNetwork_t8_114_il2cpp_TypeInfo_var);
		NetworkingPeer_t8_98 * L_1 = ((PhotonNetwork_t8_114_StaticFields*)PhotonNetwork_t8_114_il2cpp_TypeInfo_var->static_fields)->___networkingPeer_4;
		NullCheck(L_1);
		bool L_2 = NetworkingPeer_LocalCleanPhotonView_m8_464(L_1, __this, /*hidden argument*/NULL);
		V_0 = L_2;
		V_1 = 0;
		bool L_3 = V_0;
		if (!L_3)
		{
			goto IL_0065;
		}
	}
	{
		bool L_4 = V_1;
		if (L_4)
		{
			goto IL_0065;
		}
	}
	{
		int32_t L_5 = (__this->___instantiationId_17);
		if ((((int32_t)L_5) <= ((int32_t)0)))
		{
			goto IL_0065;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(PhotonHandler_t8_111_il2cpp_TypeInfo_var);
		bool L_6 = ((PhotonHandler_t8_111_StaticFields*)PhotonHandler_t8_111_il2cpp_TypeInfo_var->static_fields)->___AppQuits_10;
		if (L_6)
		{
			goto IL_0065;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(PhotonNetwork_t8_114_il2cpp_TypeInfo_var);
		int32_t L_7 = ((PhotonNetwork_t8_114_StaticFields*)PhotonNetwork_t8_114_il2cpp_TypeInfo_var->static_fields)->___logLevel_8;
		if ((((int32_t)L_7) < ((int32_t)1)))
		{
			goto IL_0065;
		}
	}
	{
		GameObject_t6_85 * L_8 = Component_get_gameObject_m6_583(__this, /*hidden argument*/NULL);
		NullCheck(L_8);
		String_t* L_9 = Object_get_name_m6_562(L_8, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_10 = String_Concat_m1_419(NULL /*static, unused*/, _stringLiteral3485, L_9, _stringLiteral3486, /*hidden argument*/NULL);
		Debug_Log_m6_489(NULL /*static, unused*/, L_10, /*hidden argument*/NULL);
	}

IL_0065:
	{
		return;
	}
}
// System.Void PhotonView::SerializeView(PhotonStream,PhotonMessageInfo)
extern "C" void PhotonView_SerializeView_m8_780 (PhotonView_t8_3 * __this, PhotonStream_t8_106 * ___stream, PhotonMessageInfo_t8_104 * ___info, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		Component_t6_26 * L_0 = (__this->___observed_9);
		PhotonStream_t8_106 * L_1 = ___stream;
		PhotonMessageInfo_t8_104 * L_2 = ___info;
		PhotonView_SerializeComponent_m8_783(__this, L_0, L_1, L_2, /*hidden argument*/NULL);
		List_1_t1_947 * L_3 = (__this->___ObservedComponents_14);
		if (!L_3)
		{
			goto IL_005a;
		}
	}
	{
		List_1_t1_947 * L_4 = (__this->___ObservedComponents_14);
		NullCheck(L_4);
		int32_t L_5 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(17 /* System.Int32 System.Collections.Generic.List`1<UnityEngine.Component>::get_Count() */, L_4);
		if ((((int32_t)L_5) <= ((int32_t)0)))
		{
			goto IL_005a;
		}
	}
	{
		V_0 = 0;
		goto IL_0049;
	}

IL_0031:
	{
		List_1_t1_947 * L_6 = (__this->___ObservedComponents_14);
		int32_t L_7 = V_0;
		NullCheck(L_6);
		Component_t6_26 * L_8 = (Component_t6_26 *)VirtFuncInvoker1< Component_t6_26 *, int32_t >::Invoke(28 /* !0 System.Collections.Generic.List`1<UnityEngine.Component>::get_Item(System.Int32) */, L_6, L_7);
		PhotonStream_t8_106 * L_9 = ___stream;
		PhotonMessageInfo_t8_104 * L_10 = ___info;
		PhotonView_SerializeComponent_m8_783(__this, L_8, L_9, L_10, /*hidden argument*/NULL);
		int32_t L_11 = V_0;
		V_0 = ((int32_t)((int32_t)L_11+(int32_t)1));
	}

IL_0049:
	{
		int32_t L_12 = V_0;
		List_1_t1_947 * L_13 = (__this->___ObservedComponents_14);
		NullCheck(L_13);
		int32_t L_14 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(17 /* System.Int32 System.Collections.Generic.List`1<UnityEngine.Component>::get_Count() */, L_13);
		if ((((int32_t)L_12) < ((int32_t)L_14)))
		{
			goto IL_0031;
		}
	}

IL_005a:
	{
		return;
	}
}
// System.Void PhotonView::DeserializeView(PhotonStream,PhotonMessageInfo)
extern "C" void PhotonView_DeserializeView_m8_781 (PhotonView_t8_3 * __this, PhotonStream_t8_106 * ___stream, PhotonMessageInfo_t8_104 * ___info, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		Component_t6_26 * L_0 = (__this->___observed_9);
		PhotonStream_t8_106 * L_1 = ___stream;
		PhotonMessageInfo_t8_104 * L_2 = ___info;
		PhotonView_DeserializeComponent_m8_782(__this, L_0, L_1, L_2, /*hidden argument*/NULL);
		List_1_t1_947 * L_3 = (__this->___ObservedComponents_14);
		if (!L_3)
		{
			goto IL_005a;
		}
	}
	{
		List_1_t1_947 * L_4 = (__this->___ObservedComponents_14);
		NullCheck(L_4);
		int32_t L_5 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(17 /* System.Int32 System.Collections.Generic.List`1<UnityEngine.Component>::get_Count() */, L_4);
		if ((((int32_t)L_5) <= ((int32_t)0)))
		{
			goto IL_005a;
		}
	}
	{
		V_0 = 0;
		goto IL_0049;
	}

IL_0031:
	{
		List_1_t1_947 * L_6 = (__this->___ObservedComponents_14);
		int32_t L_7 = V_0;
		NullCheck(L_6);
		Component_t6_26 * L_8 = (Component_t6_26 *)VirtFuncInvoker1< Component_t6_26 *, int32_t >::Invoke(28 /* !0 System.Collections.Generic.List`1<UnityEngine.Component>::get_Item(System.Int32) */, L_6, L_7);
		PhotonStream_t8_106 * L_9 = ___stream;
		PhotonMessageInfo_t8_104 * L_10 = ___info;
		PhotonView_DeserializeComponent_m8_782(__this, L_8, L_9, L_10, /*hidden argument*/NULL);
		int32_t L_11 = V_0;
		V_0 = ((int32_t)((int32_t)L_11+(int32_t)1));
	}

IL_0049:
	{
		int32_t L_12 = V_0;
		List_1_t1_947 * L_13 = (__this->___ObservedComponents_14);
		NullCheck(L_13);
		int32_t L_14 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(17 /* System.Int32 System.Collections.Generic.List`1<UnityEngine.Component>::get_Count() */, L_13);
		if ((((int32_t)L_12) < ((int32_t)L_14)))
		{
			goto IL_0031;
		}
	}

IL_005a:
	{
		return;
	}
}
// System.Void PhotonView::DeserializeComponent(UnityEngine.Component,PhotonStream,PhotonMessageInfo)
extern TypeInfo* MonoBehaviour_t6_80_il2cpp_TypeInfo_var;
extern TypeInfo* Transform_t6_61_il2cpp_TypeInfo_var;
extern TypeInfo* Vector3_t6_49_il2cpp_TypeInfo_var;
extern TypeInfo* Quaternion_t6_51_il2cpp_TypeInfo_var;
extern TypeInfo* Rigidbody_t6_102_il2cpp_TypeInfo_var;
extern TypeInfo* Rigidbody2D_t6_113_il2cpp_TypeInfo_var;
extern TypeInfo* Vector2_t6_48_il2cpp_TypeInfo_var;
extern TypeInfo* Single_t1_17_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3487;
extern "C" void PhotonView_DeserializeComponent_m8_782 (PhotonView_t8_3 * __this, Component_t6_26 * ___component, PhotonStream_t8_106 * ___stream, PhotonMessageInfo_t8_104 * ___info, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		MonoBehaviour_t6_80_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1022);
		Transform_t6_61_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(958);
		Vector3_t6_49_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(941);
		Quaternion_t6_51_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(943);
		Rigidbody_t6_102_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1062);
		Rigidbody2D_t6_113_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(964);
		Vector2_t6_48_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(939);
		Single_t1_17_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(43);
		_stringLiteral3487 = il2cpp_codegen_string_literal_from_index(3487);
		s_Il2CppMethodIntialized = true;
	}
	Transform_t6_61 * V_0 = {0};
	Rigidbody_t6_102 * V_1 = {0};
	Rigidbody2D_t6_113 * V_2 = {0};
	int32_t V_3 = {0};
	int32_t V_4 = {0};
	{
		Component_t6_26 * L_0 = ___component;
		bool L_1 = Object_op_Equality_m6_579(NULL /*static, unused*/, L_0, (Object_t6_5 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_000d;
		}
	}
	{
		return;
	}

IL_000d:
	{
		Component_t6_26 * L_2 = ___component;
		if (!((MonoBehaviour_t6_80 *)IsInstClass(L_2, MonoBehaviour_t6_80_il2cpp_TypeInfo_var)))
		{
			goto IL_0026;
		}
	}
	{
		Component_t6_26 * L_3 = ___component;
		PhotonStream_t8_106 * L_4 = ___stream;
		PhotonMessageInfo_t8_104 * L_5 = ___info;
		PhotonView_ExecuteComponentOnSerialize_m8_784(__this, L_3, L_4, L_5, /*hidden argument*/NULL);
		goto IL_0222;
	}

IL_0026:
	{
		Component_t6_26 * L_6 = ___component;
		if (!((Transform_t6_61 *)IsInstClass(L_6, Transform_t6_61_il2cpp_TypeInfo_var)))
		{
			goto IL_0104;
		}
	}
	{
		Component_t6_26 * L_7 = ___component;
		V_0 = ((Transform_t6_61 *)CastclassClass(L_7, Transform_t6_61_il2cpp_TypeInfo_var));
		int32_t L_8 = (__this->___onSerializeTransformOption_11);
		V_3 = L_8;
		int32_t L_9 = V_3;
		if (L_9 == 0)
		{
			goto IL_0096;
		}
		if (L_9 == 1)
		{
			goto IL_00ac;
		}
		if (L_9 == 2)
		{
			goto IL_00c2;
		}
		if (L_9 == 3)
		{
			goto IL_00d8;
		}
		if (L_9 == 4)
		{
			goto IL_005e;
		}
	}
	{
		goto IL_00ff;
	}

IL_005e:
	{
		Transform_t6_61 * L_10 = V_0;
		PhotonStream_t8_106 * L_11 = ___stream;
		NullCheck(L_11);
		Object_t * L_12 = PhotonStream_ReceiveNext_m8_541(L_11, /*hidden argument*/NULL);
		NullCheck(L_10);
		Transform_set_localPosition_m6_616(L_10, ((*(Vector3_t6_49 *)((Vector3_t6_49 *)UnBox (L_12, Vector3_t6_49_il2cpp_TypeInfo_var)))), /*hidden argument*/NULL);
		Transform_t6_61 * L_13 = V_0;
		PhotonStream_t8_106 * L_14 = ___stream;
		NullCheck(L_14);
		Object_t * L_15 = PhotonStream_ReceiveNext_m8_541(L_14, /*hidden argument*/NULL);
		NullCheck(L_13);
		Transform_set_localRotation_m6_628(L_13, ((*(Quaternion_t6_51 *)((Quaternion_t6_51 *)UnBox (L_15, Quaternion_t6_51_il2cpp_TypeInfo_var)))), /*hidden argument*/NULL);
		Transform_t6_61 * L_16 = V_0;
		PhotonStream_t8_106 * L_17 = ___stream;
		NullCheck(L_17);
		Object_t * L_18 = PhotonStream_ReceiveNext_m8_541(L_17, /*hidden argument*/NULL);
		NullCheck(L_16);
		Transform_set_localScale_m6_632(L_16, ((*(Vector3_t6_49 *)((Vector3_t6_49 *)UnBox (L_18, Vector3_t6_49_il2cpp_TypeInfo_var)))), /*hidden argument*/NULL);
		goto IL_00ff;
	}

IL_0096:
	{
		Transform_t6_61 * L_19 = V_0;
		PhotonStream_t8_106 * L_20 = ___stream;
		NullCheck(L_20);
		Object_t * L_21 = PhotonStream_ReceiveNext_m8_541(L_20, /*hidden argument*/NULL);
		NullCheck(L_19);
		Transform_set_localPosition_m6_616(L_19, ((*(Vector3_t6_49 *)((Vector3_t6_49 *)UnBox (L_21, Vector3_t6_49_il2cpp_TypeInfo_var)))), /*hidden argument*/NULL);
		goto IL_00ff;
	}

IL_00ac:
	{
		Transform_t6_61 * L_22 = V_0;
		PhotonStream_t8_106 * L_23 = ___stream;
		NullCheck(L_23);
		Object_t * L_24 = PhotonStream_ReceiveNext_m8_541(L_23, /*hidden argument*/NULL);
		NullCheck(L_22);
		Transform_set_localRotation_m6_628(L_22, ((*(Quaternion_t6_51 *)((Quaternion_t6_51 *)UnBox (L_24, Quaternion_t6_51_il2cpp_TypeInfo_var)))), /*hidden argument*/NULL);
		goto IL_00ff;
	}

IL_00c2:
	{
		Transform_t6_61 * L_25 = V_0;
		PhotonStream_t8_106 * L_26 = ___stream;
		NullCheck(L_26);
		Object_t * L_27 = PhotonStream_ReceiveNext_m8_541(L_26, /*hidden argument*/NULL);
		NullCheck(L_25);
		Transform_set_localScale_m6_632(L_25, ((*(Vector3_t6_49 *)((Vector3_t6_49 *)UnBox (L_27, Vector3_t6_49_il2cpp_TypeInfo_var)))), /*hidden argument*/NULL);
		goto IL_00ff;
	}

IL_00d8:
	{
		Transform_t6_61 * L_28 = V_0;
		PhotonStream_t8_106 * L_29 = ___stream;
		NullCheck(L_29);
		Object_t * L_30 = PhotonStream_ReceiveNext_m8_541(L_29, /*hidden argument*/NULL);
		NullCheck(L_28);
		Transform_set_localPosition_m6_616(L_28, ((*(Vector3_t6_49 *)((Vector3_t6_49 *)UnBox (L_30, Vector3_t6_49_il2cpp_TypeInfo_var)))), /*hidden argument*/NULL);
		Transform_t6_61 * L_31 = V_0;
		PhotonStream_t8_106 * L_32 = ___stream;
		NullCheck(L_32);
		Object_t * L_33 = PhotonStream_ReceiveNext_m8_541(L_32, /*hidden argument*/NULL);
		NullCheck(L_31);
		Transform_set_localRotation_m6_628(L_31, ((*(Quaternion_t6_51 *)((Quaternion_t6_51 *)UnBox (L_33, Quaternion_t6_51_il2cpp_TypeInfo_var)))), /*hidden argument*/NULL);
		goto IL_00ff;
	}

IL_00ff:
	{
		goto IL_0222;
	}

IL_0104:
	{
		Component_t6_26 * L_34 = ___component;
		if (!((Rigidbody_t6_102 *)IsInstSealed(L_34, Rigidbody_t6_102_il2cpp_TypeInfo_var)))
		{
			goto IL_018e;
		}
	}
	{
		Component_t6_26 * L_35 = ___component;
		V_1 = ((Rigidbody_t6_102 *)CastclassSealed(L_35, Rigidbody_t6_102_il2cpp_TypeInfo_var));
		int32_t L_36 = (__this->___onSerializeRigidBodyOption_12);
		V_4 = L_36;
		int32_t L_37 = V_4;
		if (L_37 == 0)
		{
			goto IL_0173;
		}
		if (L_37 == 1)
		{
			goto IL_015d;
		}
		if (L_37 == 2)
		{
			goto IL_0136;
		}
	}
	{
		goto IL_0189;
	}

IL_0136:
	{
		Rigidbody_t6_102 * L_38 = V_1;
		PhotonStream_t8_106 * L_39 = ___stream;
		NullCheck(L_39);
		Object_t * L_40 = PhotonStream_ReceiveNext_m8_541(L_39, /*hidden argument*/NULL);
		NullCheck(L_38);
		Rigidbody_set_velocity_m6_718(L_38, ((*(Vector3_t6_49 *)((Vector3_t6_49 *)UnBox (L_40, Vector3_t6_49_il2cpp_TypeInfo_var)))), /*hidden argument*/NULL);
		Rigidbody_t6_102 * L_41 = V_1;
		PhotonStream_t8_106 * L_42 = ___stream;
		NullCheck(L_42);
		Object_t * L_43 = PhotonStream_ReceiveNext_m8_541(L_42, /*hidden argument*/NULL);
		NullCheck(L_41);
		Rigidbody_set_angularVelocity_m6_722(L_41, ((*(Vector3_t6_49 *)((Vector3_t6_49 *)UnBox (L_43, Vector3_t6_49_il2cpp_TypeInfo_var)))), /*hidden argument*/NULL);
		goto IL_0189;
	}

IL_015d:
	{
		Rigidbody_t6_102 * L_44 = V_1;
		PhotonStream_t8_106 * L_45 = ___stream;
		NullCheck(L_45);
		Object_t * L_46 = PhotonStream_ReceiveNext_m8_541(L_45, /*hidden argument*/NULL);
		NullCheck(L_44);
		Rigidbody_set_angularVelocity_m6_722(L_44, ((*(Vector3_t6_49 *)((Vector3_t6_49 *)UnBox (L_46, Vector3_t6_49_il2cpp_TypeInfo_var)))), /*hidden argument*/NULL);
		goto IL_0189;
	}

IL_0173:
	{
		Rigidbody_t6_102 * L_47 = V_1;
		PhotonStream_t8_106 * L_48 = ___stream;
		NullCheck(L_48);
		Object_t * L_49 = PhotonStream_ReceiveNext_m8_541(L_48, /*hidden argument*/NULL);
		NullCheck(L_47);
		Rigidbody_set_velocity_m6_718(L_47, ((*(Vector3_t6_49 *)((Vector3_t6_49 *)UnBox (L_49, Vector3_t6_49_il2cpp_TypeInfo_var)))), /*hidden argument*/NULL);
		goto IL_0189;
	}

IL_0189:
	{
		goto IL_0222;
	}

IL_018e:
	{
		Component_t6_26 * L_50 = ___component;
		if (!((Rigidbody2D_t6_113 *)IsInstSealed(L_50, Rigidbody2D_t6_113_il2cpp_TypeInfo_var)))
		{
			goto IL_0218;
		}
	}
	{
		Component_t6_26 * L_51 = ___component;
		V_2 = ((Rigidbody2D_t6_113 *)CastclassSealed(L_51, Rigidbody2D_t6_113_il2cpp_TypeInfo_var));
		int32_t L_52 = (__this->___onSerializeRigidBodyOption_12);
		V_4 = L_52;
		int32_t L_53 = V_4;
		if (L_53 == 0)
		{
			goto IL_01fd;
		}
		if (L_53 == 1)
		{
			goto IL_01e7;
		}
		if (L_53 == 2)
		{
			goto IL_01c0;
		}
	}
	{
		goto IL_0213;
	}

IL_01c0:
	{
		Rigidbody2D_t6_113 * L_54 = V_2;
		PhotonStream_t8_106 * L_55 = ___stream;
		NullCheck(L_55);
		Object_t * L_56 = PhotonStream_ReceiveNext_m8_541(L_55, /*hidden argument*/NULL);
		NullCheck(L_54);
		Rigidbody2D_set_velocity_m6_741(L_54, ((*(Vector2_t6_48 *)((Vector2_t6_48 *)UnBox (L_56, Vector2_t6_48_il2cpp_TypeInfo_var)))), /*hidden argument*/NULL);
		Rigidbody2D_t6_113 * L_57 = V_2;
		PhotonStream_t8_106 * L_58 = ___stream;
		NullCheck(L_58);
		Object_t * L_59 = PhotonStream_ReceiveNext_m8_541(L_58, /*hidden argument*/NULL);
		NullCheck(L_57);
		Rigidbody2D_set_angularVelocity_m6_745(L_57, ((*(float*)((float*)UnBox (L_59, Single_t1_17_il2cpp_TypeInfo_var)))), /*hidden argument*/NULL);
		goto IL_0213;
	}

IL_01e7:
	{
		Rigidbody2D_t6_113 * L_60 = V_2;
		PhotonStream_t8_106 * L_61 = ___stream;
		NullCheck(L_61);
		Object_t * L_62 = PhotonStream_ReceiveNext_m8_541(L_61, /*hidden argument*/NULL);
		NullCheck(L_60);
		Rigidbody2D_set_angularVelocity_m6_745(L_60, ((*(float*)((float*)UnBox (L_62, Single_t1_17_il2cpp_TypeInfo_var)))), /*hidden argument*/NULL);
		goto IL_0213;
	}

IL_01fd:
	{
		Rigidbody2D_t6_113 * L_63 = V_2;
		PhotonStream_t8_106 * L_64 = ___stream;
		NullCheck(L_64);
		Object_t * L_65 = PhotonStream_ReceiveNext_m8_541(L_64, /*hidden argument*/NULL);
		NullCheck(L_63);
		Rigidbody2D_set_velocity_m6_741(L_63, ((*(Vector2_t6_48 *)((Vector2_t6_48 *)UnBox (L_65, Vector2_t6_48_il2cpp_TypeInfo_var)))), /*hidden argument*/NULL);
		goto IL_0213;
	}

IL_0213:
	{
		goto IL_0222;
	}

IL_0218:
	{
		Debug_LogError_m6_490(NULL /*static, unused*/, _stringLiteral3487, /*hidden argument*/NULL);
	}

IL_0222:
	{
		return;
	}
}
// System.Void PhotonView::SerializeComponent(UnityEngine.Component,PhotonStream,PhotonMessageInfo)
extern TypeInfo* MonoBehaviour_t6_80_il2cpp_TypeInfo_var;
extern TypeInfo* Transform_t6_61_il2cpp_TypeInfo_var;
extern TypeInfo* Vector3_t6_49_il2cpp_TypeInfo_var;
extern TypeInfo* Quaternion_t6_51_il2cpp_TypeInfo_var;
extern TypeInfo* Rigidbody_t6_102_il2cpp_TypeInfo_var;
extern TypeInfo* Rigidbody2D_t6_113_il2cpp_TypeInfo_var;
extern TypeInfo* Vector2_t6_48_il2cpp_TypeInfo_var;
extern TypeInfo* Single_t1_17_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3488;
extern "C" void PhotonView_SerializeComponent_m8_783 (PhotonView_t8_3 * __this, Component_t6_26 * ___component, PhotonStream_t8_106 * ___stream, PhotonMessageInfo_t8_104 * ___info, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		MonoBehaviour_t6_80_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1022);
		Transform_t6_61_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(958);
		Vector3_t6_49_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(941);
		Quaternion_t6_51_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(943);
		Rigidbody_t6_102_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1062);
		Rigidbody2D_t6_113_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(964);
		Vector2_t6_48_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(939);
		Single_t1_17_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(43);
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(11);
		_stringLiteral3488 = il2cpp_codegen_string_literal_from_index(3488);
		s_Il2CppMethodIntialized = true;
	}
	Transform_t6_61 * V_0 = {0};
	Rigidbody_t6_102 * V_1 = {0};
	Rigidbody2D_t6_113 * V_2 = {0};
	int32_t V_3 = {0};
	int32_t V_4 = {0};
	{
		Component_t6_26 * L_0 = ___component;
		bool L_1 = Object_op_Equality_m6_579(NULL /*static, unused*/, L_0, (Object_t6_5 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_000d;
		}
	}
	{
		return;
	}

IL_000d:
	{
		Component_t6_26 * L_2 = ___component;
		if (!((MonoBehaviour_t6_80 *)IsInstClass(L_2, MonoBehaviour_t6_80_il2cpp_TypeInfo_var)))
		{
			goto IL_0026;
		}
	}
	{
		Component_t6_26 * L_3 = ___component;
		PhotonStream_t8_106 * L_4 = ___stream;
		PhotonMessageInfo_t8_104 * L_5 = ___info;
		PhotonView_ExecuteComponentOnSerialize_m8_784(__this, L_3, L_4, L_5, /*hidden argument*/NULL);
		goto IL_022d;
	}

IL_0026:
	{
		Component_t6_26 * L_6 = ___component;
		if (!((Transform_t6_61 *)IsInstClass(L_6, Transform_t6_61_il2cpp_TypeInfo_var)))
		{
			goto IL_0104;
		}
	}
	{
		Component_t6_26 * L_7 = ___component;
		V_0 = ((Transform_t6_61 *)CastclassClass(L_7, Transform_t6_61_il2cpp_TypeInfo_var));
		int32_t L_8 = (__this->___onSerializeTransformOption_11);
		V_3 = L_8;
		int32_t L_9 = V_3;
		if (L_9 == 0)
		{
			goto IL_0096;
		}
		if (L_9 == 1)
		{
			goto IL_00ac;
		}
		if (L_9 == 2)
		{
			goto IL_00c2;
		}
		if (L_9 == 3)
		{
			goto IL_00d8;
		}
		if (L_9 == 4)
		{
			goto IL_005e;
		}
	}
	{
		goto IL_00ff;
	}

IL_005e:
	{
		PhotonStream_t8_106 * L_10 = ___stream;
		Transform_t6_61 * L_11 = V_0;
		NullCheck(L_11);
		Vector3_t6_49  L_12 = Transform_get_localPosition_m6_615(L_11, /*hidden argument*/NULL);
		Vector3_t6_49  L_13 = L_12;
		Object_t * L_14 = Box(Vector3_t6_49_il2cpp_TypeInfo_var, &L_13);
		NullCheck(L_10);
		PhotonStream_SendNext_m8_543(L_10, L_14, /*hidden argument*/NULL);
		PhotonStream_t8_106 * L_15 = ___stream;
		Transform_t6_61 * L_16 = V_0;
		NullCheck(L_16);
		Quaternion_t6_51  L_17 = Transform_get_localRotation_m6_627(L_16, /*hidden argument*/NULL);
		Quaternion_t6_51  L_18 = L_17;
		Object_t * L_19 = Box(Quaternion_t6_51_il2cpp_TypeInfo_var, &L_18);
		NullCheck(L_15);
		PhotonStream_SendNext_m8_543(L_15, L_19, /*hidden argument*/NULL);
		PhotonStream_t8_106 * L_20 = ___stream;
		Transform_t6_61 * L_21 = V_0;
		NullCheck(L_21);
		Vector3_t6_49  L_22 = Transform_get_localScale_m6_631(L_21, /*hidden argument*/NULL);
		Vector3_t6_49  L_23 = L_22;
		Object_t * L_24 = Box(Vector3_t6_49_il2cpp_TypeInfo_var, &L_23);
		NullCheck(L_20);
		PhotonStream_SendNext_m8_543(L_20, L_24, /*hidden argument*/NULL);
		goto IL_00ff;
	}

IL_0096:
	{
		PhotonStream_t8_106 * L_25 = ___stream;
		Transform_t6_61 * L_26 = V_0;
		NullCheck(L_26);
		Vector3_t6_49  L_27 = Transform_get_localPosition_m6_615(L_26, /*hidden argument*/NULL);
		Vector3_t6_49  L_28 = L_27;
		Object_t * L_29 = Box(Vector3_t6_49_il2cpp_TypeInfo_var, &L_28);
		NullCheck(L_25);
		PhotonStream_SendNext_m8_543(L_25, L_29, /*hidden argument*/NULL);
		goto IL_00ff;
	}

IL_00ac:
	{
		PhotonStream_t8_106 * L_30 = ___stream;
		Transform_t6_61 * L_31 = V_0;
		NullCheck(L_31);
		Quaternion_t6_51  L_32 = Transform_get_localRotation_m6_627(L_31, /*hidden argument*/NULL);
		Quaternion_t6_51  L_33 = L_32;
		Object_t * L_34 = Box(Quaternion_t6_51_il2cpp_TypeInfo_var, &L_33);
		NullCheck(L_30);
		PhotonStream_SendNext_m8_543(L_30, L_34, /*hidden argument*/NULL);
		goto IL_00ff;
	}

IL_00c2:
	{
		PhotonStream_t8_106 * L_35 = ___stream;
		Transform_t6_61 * L_36 = V_0;
		NullCheck(L_36);
		Vector3_t6_49  L_37 = Transform_get_localScale_m6_631(L_36, /*hidden argument*/NULL);
		Vector3_t6_49  L_38 = L_37;
		Object_t * L_39 = Box(Vector3_t6_49_il2cpp_TypeInfo_var, &L_38);
		NullCheck(L_35);
		PhotonStream_SendNext_m8_543(L_35, L_39, /*hidden argument*/NULL);
		goto IL_00ff;
	}

IL_00d8:
	{
		PhotonStream_t8_106 * L_40 = ___stream;
		Transform_t6_61 * L_41 = V_0;
		NullCheck(L_41);
		Vector3_t6_49  L_42 = Transform_get_localPosition_m6_615(L_41, /*hidden argument*/NULL);
		Vector3_t6_49  L_43 = L_42;
		Object_t * L_44 = Box(Vector3_t6_49_il2cpp_TypeInfo_var, &L_43);
		NullCheck(L_40);
		PhotonStream_SendNext_m8_543(L_40, L_44, /*hidden argument*/NULL);
		PhotonStream_t8_106 * L_45 = ___stream;
		Transform_t6_61 * L_46 = V_0;
		NullCheck(L_46);
		Quaternion_t6_51  L_47 = Transform_get_localRotation_m6_627(L_46, /*hidden argument*/NULL);
		Quaternion_t6_51  L_48 = L_47;
		Object_t * L_49 = Box(Quaternion_t6_51_il2cpp_TypeInfo_var, &L_48);
		NullCheck(L_45);
		PhotonStream_SendNext_m8_543(L_45, L_49, /*hidden argument*/NULL);
		goto IL_00ff;
	}

IL_00ff:
	{
		goto IL_022d;
	}

IL_0104:
	{
		Component_t6_26 * L_50 = ___component;
		if (!((Rigidbody_t6_102 *)IsInstSealed(L_50, Rigidbody_t6_102_il2cpp_TypeInfo_var)))
		{
			goto IL_018e;
		}
	}
	{
		Component_t6_26 * L_51 = ___component;
		V_1 = ((Rigidbody_t6_102 *)CastclassSealed(L_51, Rigidbody_t6_102_il2cpp_TypeInfo_var));
		int32_t L_52 = (__this->___onSerializeRigidBodyOption_12);
		V_4 = L_52;
		int32_t L_53 = V_4;
		if (L_53 == 0)
		{
			goto IL_0173;
		}
		if (L_53 == 1)
		{
			goto IL_015d;
		}
		if (L_53 == 2)
		{
			goto IL_0136;
		}
	}
	{
		goto IL_0189;
	}

IL_0136:
	{
		PhotonStream_t8_106 * L_54 = ___stream;
		Rigidbody_t6_102 * L_55 = V_1;
		NullCheck(L_55);
		Vector3_t6_49  L_56 = Rigidbody_get_velocity_m6_717(L_55, /*hidden argument*/NULL);
		Vector3_t6_49  L_57 = L_56;
		Object_t * L_58 = Box(Vector3_t6_49_il2cpp_TypeInfo_var, &L_57);
		NullCheck(L_54);
		PhotonStream_SendNext_m8_543(L_54, L_58, /*hidden argument*/NULL);
		PhotonStream_t8_106 * L_59 = ___stream;
		Rigidbody_t6_102 * L_60 = V_1;
		NullCheck(L_60);
		Vector3_t6_49  L_61 = Rigidbody_get_angularVelocity_m6_721(L_60, /*hidden argument*/NULL);
		Vector3_t6_49  L_62 = L_61;
		Object_t * L_63 = Box(Vector3_t6_49_il2cpp_TypeInfo_var, &L_62);
		NullCheck(L_59);
		PhotonStream_SendNext_m8_543(L_59, L_63, /*hidden argument*/NULL);
		goto IL_0189;
	}

IL_015d:
	{
		PhotonStream_t8_106 * L_64 = ___stream;
		Rigidbody_t6_102 * L_65 = V_1;
		NullCheck(L_65);
		Vector3_t6_49  L_66 = Rigidbody_get_angularVelocity_m6_721(L_65, /*hidden argument*/NULL);
		Vector3_t6_49  L_67 = L_66;
		Object_t * L_68 = Box(Vector3_t6_49_il2cpp_TypeInfo_var, &L_67);
		NullCheck(L_64);
		PhotonStream_SendNext_m8_543(L_64, L_68, /*hidden argument*/NULL);
		goto IL_0189;
	}

IL_0173:
	{
		PhotonStream_t8_106 * L_69 = ___stream;
		Rigidbody_t6_102 * L_70 = V_1;
		NullCheck(L_70);
		Vector3_t6_49  L_71 = Rigidbody_get_velocity_m6_717(L_70, /*hidden argument*/NULL);
		Vector3_t6_49  L_72 = L_71;
		Object_t * L_73 = Box(Vector3_t6_49_il2cpp_TypeInfo_var, &L_72);
		NullCheck(L_69);
		PhotonStream_SendNext_m8_543(L_69, L_73, /*hidden argument*/NULL);
		goto IL_0189;
	}

IL_0189:
	{
		goto IL_022d;
	}

IL_018e:
	{
		Component_t6_26 * L_74 = ___component;
		if (!((Rigidbody2D_t6_113 *)IsInstSealed(L_74, Rigidbody2D_t6_113_il2cpp_TypeInfo_var)))
		{
			goto IL_0218;
		}
	}
	{
		Component_t6_26 * L_75 = ___component;
		V_2 = ((Rigidbody2D_t6_113 *)CastclassSealed(L_75, Rigidbody2D_t6_113_il2cpp_TypeInfo_var));
		int32_t L_76 = (__this->___onSerializeRigidBodyOption_12);
		V_4 = L_76;
		int32_t L_77 = V_4;
		if (L_77 == 0)
		{
			goto IL_01fd;
		}
		if (L_77 == 1)
		{
			goto IL_01e7;
		}
		if (L_77 == 2)
		{
			goto IL_01c0;
		}
	}
	{
		goto IL_0213;
	}

IL_01c0:
	{
		PhotonStream_t8_106 * L_78 = ___stream;
		Rigidbody2D_t6_113 * L_79 = V_2;
		NullCheck(L_79);
		Vector2_t6_48  L_80 = Rigidbody2D_get_velocity_m6_740(L_79, /*hidden argument*/NULL);
		Vector2_t6_48  L_81 = L_80;
		Object_t * L_82 = Box(Vector2_t6_48_il2cpp_TypeInfo_var, &L_81);
		NullCheck(L_78);
		PhotonStream_SendNext_m8_543(L_78, L_82, /*hidden argument*/NULL);
		PhotonStream_t8_106 * L_83 = ___stream;
		Rigidbody2D_t6_113 * L_84 = V_2;
		NullCheck(L_84);
		float L_85 = Rigidbody2D_get_angularVelocity_m6_744(L_84, /*hidden argument*/NULL);
		float L_86 = L_85;
		Object_t * L_87 = Box(Single_t1_17_il2cpp_TypeInfo_var, &L_86);
		NullCheck(L_83);
		PhotonStream_SendNext_m8_543(L_83, L_87, /*hidden argument*/NULL);
		goto IL_0213;
	}

IL_01e7:
	{
		PhotonStream_t8_106 * L_88 = ___stream;
		Rigidbody2D_t6_113 * L_89 = V_2;
		NullCheck(L_89);
		float L_90 = Rigidbody2D_get_angularVelocity_m6_744(L_89, /*hidden argument*/NULL);
		float L_91 = L_90;
		Object_t * L_92 = Box(Single_t1_17_il2cpp_TypeInfo_var, &L_91);
		NullCheck(L_88);
		PhotonStream_SendNext_m8_543(L_88, L_92, /*hidden argument*/NULL);
		goto IL_0213;
	}

IL_01fd:
	{
		PhotonStream_t8_106 * L_93 = ___stream;
		Rigidbody2D_t6_113 * L_94 = V_2;
		NullCheck(L_94);
		Vector2_t6_48  L_95 = Rigidbody2D_get_velocity_m6_740(L_94, /*hidden argument*/NULL);
		Vector2_t6_48  L_96 = L_95;
		Object_t * L_97 = Box(Vector2_t6_48_il2cpp_TypeInfo_var, &L_96);
		NullCheck(L_93);
		PhotonStream_SendNext_m8_543(L_93, L_97, /*hidden argument*/NULL);
		goto IL_0213;
	}

IL_0213:
	{
		goto IL_022d;
	}

IL_0218:
	{
		Component_t6_26 * L_98 = ___component;
		NullCheck(L_98);
		Type_t * L_99 = Object_GetType_m1_5(L_98, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_100 = String_Concat_m1_416(NULL /*static, unused*/, _stringLiteral3488, L_99, /*hidden argument*/NULL);
		Debug_LogError_m6_490(NULL /*static, unused*/, L_100, /*hidden argument*/NULL);
	}

IL_022d:
	{
		return;
	}
}
// System.Void PhotonView::ExecuteComponentOnSerialize(UnityEngine.Component,PhotonStream,PhotonMessageInfo)
extern TypeInfo* MonoBehaviour_t6_80_il2cpp_TypeInfo_var;
extern TypeInfo* PhotonNetworkingMessage_t8_62_il2cpp_TypeInfo_var;
extern TypeInfo* NetworkingPeer_t8_98_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* ObjectU5BU5D_t1_157_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3489;
extern Il2CppCodeGenString* _stringLiteral3490;
extern "C" void PhotonView_ExecuteComponentOnSerialize_m8_784 (PhotonView_t8_3 * __this, Component_t6_26 * ___component, PhotonStream_t8_106 * ___stream, PhotonMessageInfo_t8_104 * ___info, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		MonoBehaviour_t6_80_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1022);
		PhotonNetworkingMessage_t8_62_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1139);
		NetworkingPeer_t8_98_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1121);
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(11);
		ObjectU5BU5D_t1_157_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(19);
		_stringLiteral3489 = il2cpp_codegen_string_literal_from_index(3489);
		_stringLiteral3490 = il2cpp_codegen_string_literal_from_index(3490);
		s_Il2CppMethodIntialized = true;
	}
	MethodInfo_t * V_0 = {0};
	bool V_1 = false;
	{
		Component_t6_26 * L_0 = ___component;
		bool L_1 = Object_op_Inequality_m6_580(NULL /*static, unused*/, L_0, (Object_t6_5 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_009a;
		}
	}
	{
		Dictionary_2_t1_948 * L_2 = (__this->___m_OnSerializeMethodInfos_15);
		Component_t6_26 * L_3 = ___component;
		NullCheck(L_2);
		bool L_4 = (bool)VirtFuncInvoker1< bool, Component_t6_26 * >::Invoke(28 /* System.Boolean System.Collections.Generic.Dictionary`2<UnityEngine.Component,System.Reflection.MethodInfo>::ContainsKey(!0) */, L_2, L_3);
		if (L_4)
		{
			goto IL_0068;
		}
	}
	{
		V_0 = (MethodInfo_t *)NULL;
		Component_t6_26 * L_5 = ___component;
		int32_t L_6 = ((int32_t)17);
		Object_t * L_7 = Box(PhotonNetworkingMessage_t8_62_il2cpp_TypeInfo_var, &L_6);
		NullCheck(L_7);
		String_t* L_8 = (String_t*)VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Enum::ToString() */, L_7);
		IL2CPP_RUNTIME_CLASS_INIT(NetworkingPeer_t8_98_il2cpp_TypeInfo_var);
		bool L_9 = NetworkingPeer_GetMethod_m8_489(NULL /*static, unused*/, ((MonoBehaviour_t6_80 *)IsInstClass(L_5, MonoBehaviour_t6_80_il2cpp_TypeInfo_var)), L_8, (&V_0), /*hidden argument*/NULL);
		V_1 = L_9;
		bool L_10 = V_1;
		if (L_10)
		{
			goto IL_005b;
		}
	}
	{
		Component_t6_26 * L_11 = ___component;
		NullCheck(L_11);
		String_t* L_12 = Object_get_name_m6_562(L_11, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_13 = String_Concat_m1_419(NULL /*static, unused*/, _stringLiteral3489, L_12, _stringLiteral3490, /*hidden argument*/NULL);
		Debug_LogError_m6_490(NULL /*static, unused*/, L_13, /*hidden argument*/NULL);
		V_0 = (MethodInfo_t *)NULL;
	}

IL_005b:
	{
		Dictionary_2_t1_948 * L_14 = (__this->___m_OnSerializeMethodInfos_15);
		Component_t6_26 * L_15 = ___component;
		MethodInfo_t * L_16 = V_0;
		NullCheck(L_14);
		VirtActionInvoker2< Component_t6_26 *, MethodInfo_t * >::Invoke(27 /* System.Void System.Collections.Generic.Dictionary`2<UnityEngine.Component,System.Reflection.MethodInfo>::Add(!0,!1) */, L_14, L_15, L_16);
	}

IL_0068:
	{
		Dictionary_2_t1_948 * L_17 = (__this->___m_OnSerializeMethodInfos_15);
		Component_t6_26 * L_18 = ___component;
		NullCheck(L_17);
		MethodInfo_t * L_19 = (MethodInfo_t *)VirtFuncInvoker1< MethodInfo_t *, Component_t6_26 * >::Invoke(25 /* !1 System.Collections.Generic.Dictionary`2<UnityEngine.Component,System.Reflection.MethodInfo>::get_Item(!0) */, L_17, L_18);
		if (!L_19)
		{
			goto IL_009a;
		}
	}
	{
		Dictionary_2_t1_948 * L_20 = (__this->___m_OnSerializeMethodInfos_15);
		Component_t6_26 * L_21 = ___component;
		NullCheck(L_20);
		MethodInfo_t * L_22 = (MethodInfo_t *)VirtFuncInvoker1< MethodInfo_t *, Component_t6_26 * >::Invoke(25 /* !1 System.Collections.Generic.Dictionary`2<UnityEngine.Component,System.Reflection.MethodInfo>::get_Item(!0) */, L_20, L_21);
		Component_t6_26 * L_23 = ___component;
		ObjectU5BU5D_t1_157* L_24 = ((ObjectU5BU5D_t1_157*)SZArrayNew(ObjectU5BU5D_t1_157_il2cpp_TypeInfo_var, 2));
		PhotonStream_t8_106 * L_25 = ___stream;
		NullCheck(L_24);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_24, 0);
		ArrayElementTypeCheck (L_24, L_25);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_24, 0, sizeof(Object_t *))) = (Object_t *)L_25;
		ObjectU5BU5D_t1_157* L_26 = L_24;
		PhotonMessageInfo_t8_104 * L_27 = ___info;
		NullCheck(L_26);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_26, 1);
		ArrayElementTypeCheck (L_26, L_27);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_26, 1, sizeof(Object_t *))) = (Object_t *)L_27;
		NullCheck(L_22);
		VirtFuncInvoker2< Object_t *, Object_t *, ObjectU5BU5D_t1_157* >::Invoke(16 /* System.Object System.Reflection.MethodBase::Invoke(System.Object,System.Object[]) */, L_22, L_23, L_26);
	}

IL_009a:
	{
		return;
	}
}
// System.Void PhotonView::RefreshRpcMonoBehaviourCache()
extern const MethodInfo* Component_GetComponents_TisMonoBehaviour_t6_80_m6_1688_MethodInfo_var;
extern "C" void PhotonView_RefreshRpcMonoBehaviourCache_m8_785 (PhotonView_t8_3 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Component_GetComponents_TisMonoBehaviour_t6_80_m6_1688_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483941);
		s_Il2CppMethodIntialized = true;
	}
	{
		MonoBehaviourU5BU5D_t6_273* L_0 = Component_GetComponents_TisMonoBehaviour_t6_80_m6_1688(__this, /*hidden argument*/Component_GetComponents_TisMonoBehaviour_t6_80_m6_1688_MethodInfo_var);
		__this->___RpcMonoBehaviours_21 = L_0;
		return;
	}
}
// System.Void PhotonView::RPC(System.String,PhotonTargets,System.Object[])
extern TypeInfo* PhotonNetwork_t8_114_il2cpp_TypeInfo_var;
extern "C" void PhotonView_RPC_m8_786 (PhotonView_t8_3 * __this, String_t* ___methodName, int32_t ___target, ObjectU5BU5D_t1_157* ___parameters, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		PhotonNetwork_t8_114_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1058);
		s_Il2CppMethodIntialized = true;
	}
	{
		String_t* L_0 = ___methodName;
		int32_t L_1 = ___target;
		ObjectU5BU5D_t1_157* L_2 = ___parameters;
		IL2CPP_RUNTIME_CLASS_INIT(PhotonNetwork_t8_114_il2cpp_TypeInfo_var);
		PhotonNetwork_RPC_m8_716(NULL /*static, unused*/, __this, L_0, L_1, 0, L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Void PhotonView::RpcSecure(System.String,PhotonTargets,System.Boolean,System.Object[])
extern TypeInfo* PhotonNetwork_t8_114_il2cpp_TypeInfo_var;
extern "C" void PhotonView_RpcSecure_m8_787 (PhotonView_t8_3 * __this, String_t* ___methodName, int32_t ___target, bool ___encrypt, ObjectU5BU5D_t1_157* ___parameters, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		PhotonNetwork_t8_114_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1058);
		s_Il2CppMethodIntialized = true;
	}
	{
		String_t* L_0 = ___methodName;
		int32_t L_1 = ___target;
		bool L_2 = ___encrypt;
		ObjectU5BU5D_t1_157* L_3 = ___parameters;
		IL2CPP_RUNTIME_CLASS_INIT(PhotonNetwork_t8_114_il2cpp_TypeInfo_var);
		PhotonNetwork_RPC_m8_716(NULL /*static, unused*/, __this, L_0, L_1, L_2, L_3, /*hidden argument*/NULL);
		return;
	}
}
// System.Void PhotonView::RPC(System.String,PhotonPlayer,System.Object[])
extern TypeInfo* PhotonNetwork_t8_114_il2cpp_TypeInfo_var;
extern "C" void PhotonView_RPC_m8_788 (PhotonView_t8_3 * __this, String_t* ___methodName, PhotonPlayer_t8_102 * ___targetPlayer, ObjectU5BU5D_t1_157* ___parameters, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		PhotonNetwork_t8_114_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1058);
		s_Il2CppMethodIntialized = true;
	}
	{
		String_t* L_0 = ___methodName;
		PhotonPlayer_t8_102 * L_1 = ___targetPlayer;
		ObjectU5BU5D_t1_157* L_2 = ___parameters;
		IL2CPP_RUNTIME_CLASS_INIT(PhotonNetwork_t8_114_il2cpp_TypeInfo_var);
		PhotonNetwork_RPC_m8_717(NULL /*static, unused*/, __this, L_0, L_1, 0, L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Void PhotonView::RpcSecure(System.String,PhotonPlayer,System.Boolean,System.Object[])
extern TypeInfo* PhotonNetwork_t8_114_il2cpp_TypeInfo_var;
extern "C" void PhotonView_RpcSecure_m8_789 (PhotonView_t8_3 * __this, String_t* ___methodName, PhotonPlayer_t8_102 * ___targetPlayer, bool ___encrypt, ObjectU5BU5D_t1_157* ___parameters, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		PhotonNetwork_t8_114_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1058);
		s_Il2CppMethodIntialized = true;
	}
	{
		String_t* L_0 = ___methodName;
		PhotonPlayer_t8_102 * L_1 = ___targetPlayer;
		bool L_2 = ___encrypt;
		ObjectU5BU5D_t1_157* L_3 = ___parameters;
		IL2CPP_RUNTIME_CLASS_INIT(PhotonNetwork_t8_114_il2cpp_TypeInfo_var);
		PhotonNetwork_RPC_m8_717(NULL /*static, unused*/, __this, L_0, L_1, L_2, L_3, /*hidden argument*/NULL);
		return;
	}
}
// PhotonView PhotonView::Get(UnityEngine.Component)
extern const MethodInfo* Component_GetComponent_TisPhotonView_t8_3_m6_1658_MethodInfo_var;
extern "C" PhotonView_t8_3 * PhotonView_Get_m8_790 (Object_t * __this /* static, unused */, Component_t6_26 * ___component, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Component_GetComponent_TisPhotonView_t8_3_m6_1658_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483836);
		s_Il2CppMethodIntialized = true;
	}
	{
		Component_t6_26 * L_0 = ___component;
		NullCheck(L_0);
		PhotonView_t8_3 * L_1 = Component_GetComponent_TisPhotonView_t8_3_m6_1658(L_0, /*hidden argument*/Component_GetComponent_TisPhotonView_t8_3_m6_1658_MethodInfo_var);
		return L_1;
	}
}
// PhotonView PhotonView::Get(UnityEngine.GameObject)
extern const MethodInfo* GameObject_GetComponent_TisPhotonView_t8_3_m6_1666_MethodInfo_var;
extern "C" PhotonView_t8_3 * PhotonView_Get_m8_791 (Object_t * __this /* static, unused */, GameObject_t6_85 * ___gameObj, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GameObject_GetComponent_TisPhotonView_t8_3_m6_1666_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483849);
		s_Il2CppMethodIntialized = true;
	}
	{
		GameObject_t6_85 * L_0 = ___gameObj;
		NullCheck(L_0);
		PhotonView_t8_3 * L_1 = GameObject_GetComponent_TisPhotonView_t8_3_m6_1666(L_0, /*hidden argument*/GameObject_GetComponent_TisPhotonView_t8_3_m6_1666_MethodInfo_var);
		return L_1;
	}
}
// PhotonView PhotonView::Find(System.Int32)
extern TypeInfo* PhotonNetwork_t8_114_il2cpp_TypeInfo_var;
extern "C" PhotonView_t8_3 * PhotonView_Find_m8_792 (Object_t * __this /* static, unused */, int32_t ___viewID, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		PhotonNetwork_t8_114_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1058);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(PhotonNetwork_t8_114_il2cpp_TypeInfo_var);
		NetworkingPeer_t8_98 * L_0 = ((PhotonNetwork_t8_114_StaticFields*)PhotonNetwork_t8_114_il2cpp_TypeInfo_var->static_fields)->___networkingPeer_4;
		int32_t L_1 = ___viewID;
		NullCheck(L_0);
		PhotonView_t8_3 * L_2 = NetworkingPeer_GetPhotonView_m8_465(L_0, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// System.String PhotonView::ToString()
extern TypeInfo* ObjectU5BU5D_t1_157_il2cpp_TypeInfo_var;
extern TypeInfo* Int32_t1_3_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3491;
extern Il2CppCodeGenString* _stringLiteral3492;
extern Il2CppCodeGenString* _stringLiteral3493;
extern "C" String_t* PhotonView_ToString_m8_793 (PhotonView_t8_3 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ObjectU5BU5D_t1_157_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(19);
		Int32_t1_3_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(8);
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(11);
		_stringLiteral3491 = il2cpp_codegen_string_literal_from_index(3491);
		_stringLiteral3492 = il2cpp_codegen_string_literal_from_index(3492);
		_stringLiteral3493 = il2cpp_codegen_string_literal_from_index(3493);
		s_Il2CppMethodIntialized = true;
	}
	int32_t G_B2_0 = 0;
	ObjectU5BU5D_t1_157* G_B2_1 = {0};
	ObjectU5BU5D_t1_157* G_B2_2 = {0};
	String_t* G_B2_3 = {0};
	int32_t G_B1_0 = 0;
	ObjectU5BU5D_t1_157* G_B1_1 = {0};
	ObjectU5BU5D_t1_157* G_B1_2 = {0};
	String_t* G_B1_3 = {0};
	String_t* G_B3_0 = {0};
	int32_t G_B3_1 = 0;
	ObjectU5BU5D_t1_157* G_B3_2 = {0};
	ObjectU5BU5D_t1_157* G_B3_3 = {0};
	String_t* G_B3_4 = {0};
	int32_t G_B5_0 = 0;
	ObjectU5BU5D_t1_157* G_B5_1 = {0};
	ObjectU5BU5D_t1_157* G_B5_2 = {0};
	String_t* G_B5_3 = {0};
	int32_t G_B4_0 = 0;
	ObjectU5BU5D_t1_157* G_B4_1 = {0};
	ObjectU5BU5D_t1_157* G_B4_2 = {0};
	String_t* G_B4_3 = {0};
	String_t* G_B6_0 = {0};
	int32_t G_B6_1 = 0;
	ObjectU5BU5D_t1_157* G_B6_2 = {0};
	ObjectU5BU5D_t1_157* G_B6_3 = {0};
	String_t* G_B6_4 = {0};
	{
		ObjectU5BU5D_t1_157* L_0 = ((ObjectU5BU5D_t1_157*)SZArrayNew(ObjectU5BU5D_t1_157_il2cpp_TypeInfo_var, 4));
		int32_t L_1 = PhotonView_get_viewID_m8_767(__this, /*hidden argument*/NULL);
		int32_t L_2 = L_1;
		Object_t * L_3 = Box(Int32_t1_3_il2cpp_TypeInfo_var, &L_2);
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, 0);
		ArrayElementTypeCheck (L_0, L_3);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_0, 0, sizeof(Object_t *))) = (Object_t *)L_3;
		ObjectU5BU5D_t1_157* L_4 = L_0;
		GameObject_t6_85 * L_5 = Component_get_gameObject_m6_583(__this, /*hidden argument*/NULL);
		bool L_6 = Object_op_Inequality_m6_580(NULL /*static, unused*/, L_5, (Object_t6_5 *)NULL, /*hidden argument*/NULL);
		G_B1_0 = 1;
		G_B1_1 = L_4;
		G_B1_2 = L_4;
		G_B1_3 = _stringLiteral3491;
		if (!L_6)
		{
			G_B2_0 = 1;
			G_B2_1 = L_4;
			G_B2_2 = L_4;
			G_B2_3 = _stringLiteral3491;
			goto IL_003c;
		}
	}
	{
		GameObject_t6_85 * L_7 = Component_get_gameObject_m6_583(__this, /*hidden argument*/NULL);
		NullCheck(L_7);
		String_t* L_8 = Object_get_name_m6_562(L_7, /*hidden argument*/NULL);
		G_B3_0 = L_8;
		G_B3_1 = G_B1_0;
		G_B3_2 = G_B1_1;
		G_B3_3 = G_B1_2;
		G_B3_4 = G_B1_3;
		goto IL_0041;
	}

IL_003c:
	{
		G_B3_0 = _stringLiteral3492;
		G_B3_1 = G_B2_0;
		G_B3_2 = G_B2_1;
		G_B3_3 = G_B2_2;
		G_B3_4 = G_B2_3;
	}

IL_0041:
	{
		NullCheck(G_B3_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(G_B3_2, G_B3_1);
		ArrayElementTypeCheck (G_B3_2, G_B3_0);
		*((Object_t **)(Object_t **)SZArrayLdElema(G_B3_2, G_B3_1, sizeof(Object_t *))) = (Object_t *)G_B3_0;
		ObjectU5BU5D_t1_157* L_9 = G_B3_3;
		bool L_10 = PhotonView_get_isSceneView_m8_769(__this, /*hidden argument*/NULL);
		G_B4_0 = 2;
		G_B4_1 = L_9;
		G_B4_2 = L_9;
		G_B4_3 = G_B3_4;
		if (!L_10)
		{
			G_B5_0 = 2;
			G_B5_1 = L_9;
			G_B5_2 = L_9;
			G_B5_3 = G_B3_4;
			goto IL_0059;
		}
	}
	{
		G_B6_0 = _stringLiteral3493;
		G_B6_1 = G_B4_0;
		G_B6_2 = G_B4_1;
		G_B6_3 = G_B4_2;
		G_B6_4 = G_B4_3;
		goto IL_005e;
	}

IL_0059:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_11 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->___Empty_2;
		G_B6_0 = L_11;
		G_B6_1 = G_B5_0;
		G_B6_2 = G_B5_1;
		G_B6_3 = G_B5_2;
		G_B6_4 = G_B5_3;
	}

IL_005e:
	{
		NullCheck(G_B6_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(G_B6_2, G_B6_1);
		ArrayElementTypeCheck (G_B6_2, G_B6_0);
		*((Object_t **)(Object_t **)SZArrayLdElema(G_B6_2, G_B6_1, sizeof(Object_t *))) = (Object_t *)G_B6_0;
		ObjectU5BU5D_t1_157* L_12 = G_B6_3;
		int32_t L_13 = PhotonView_get_prefix_m8_763(__this, /*hidden argument*/NULL);
		int32_t L_14 = L_13;
		Object_t * L_15 = Box(Int32_t1_3_il2cpp_TypeInfo_var, &L_14);
		NullCheck(L_12);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_12, 3);
		ArrayElementTypeCheck (L_12, L_15);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_12, 3, sizeof(Object_t *))) = (Object_t *)L_15;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_16 = String_Format_m1_413(NULL /*static, unused*/, G_B6_4, L_12, /*hidden argument*/NULL);
		return L_16;
	}
}
// System.Void PhotonPingManager/<PingSocket>c__Iterator1::.ctor()
extern "C" void U3CPingSocketU3Ec__Iterator1__ctor_m8_794 (U3CPingSocketU3Ec__Iterator1_t8_122 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1_0(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Object PhotonPingManager/<PingSocket>c__Iterator1::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C" Object_t * U3CPingSocketU3Ec__Iterator1_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m8_795 (U3CPingSocketU3Ec__Iterator1_t8_122 * __this, const MethodInfo* method)
{
	{
		Object_t * L_0 = (__this->___U24current_12);
		return L_0;
	}
}
// System.Object PhotonPingManager/<PingSocket>c__Iterator1::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * U3CPingSocketU3Ec__Iterator1_System_Collections_IEnumerator_get_Current_m8_796 (U3CPingSocketU3Ec__Iterator1_t8_122 * __this, const MethodInfo* method)
{
	{
		Object_t * L_0 = (__this->___U24current_12);
		return L_0;
	}
}
// System.Boolean PhotonPingManager/<PingSocket>c__Iterator1::MoveNext()
extern const Il2CppType* PingNativeDynamic_t5_41_0_0_0_var;
extern const Il2CppType* PingMono_t5_40_0_0_0_var;
extern TypeInfo* PhotonPingManager_t8_109_il2cpp_TypeInfo_var;
extern TypeInfo* PhotonHandler_t8_111_il2cpp_TypeInfo_var;
extern TypeInfo* Type_t_il2cpp_TypeInfo_var;
extern TypeInfo* PingNativeDynamic_t5_41_il2cpp_TypeInfo_var;
extern TypeInfo* PingMono_t5_40_il2cpp_TypeInfo_var;
extern TypeInfo* PhotonPing_t5_39_il2cpp_TypeInfo_var;
extern TypeInfo* Stopwatch_t3_20_il2cpp_TypeInfo_var;
extern TypeInfo* Exception_t1_33_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* Int32_t1_3_il2cpp_TypeInfo_var;
extern TypeInfo* WaitForSeconds_t6_11_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3497;
extern Il2CppCodeGenString* _stringLiteral3498;
extern "C" bool U3CPingSocketU3Ec__Iterator1_MoveNext_m8_797 (U3CPingSocketU3Ec__Iterator1_t8_122 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		PingNativeDynamic_t5_41_0_0_0_var = il2cpp_codegen_type_from_index(1161);
		PingMono_t5_40_0_0_0_var = il2cpp_codegen_type_from_index(1109);
		PhotonPingManager_t8_109_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1151);
		PhotonHandler_t8_111_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1120);
		Type_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3);
		PingNativeDynamic_t5_41_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1161);
		PingMono_t5_40_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1109);
		PhotonPing_t5_39_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1162);
		Stopwatch_t3_20_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(629);
		Exception_t1_33_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(45);
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(11);
		Int32_t1_3_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(8);
		WaitForSeconds_t6_11_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1150);
		_stringLiteral3497 = il2cpp_codegen_string_literal_from_index(3497);
		_stringLiteral3498 = il2cpp_codegen_string_literal_from_index(3498);
		s_Il2CppMethodIntialized = true;
	}
	uint32_t V_0 = 0;
	Exception_t1_33 * V_1 = {0};
	bool V_2 = false;
	Exception_t1_33 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1_33 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		int32_t L_0 = (__this->___U24PC_11);
		V_0 = L_0;
		__this->___U24PC_11 = (-1);
		uint32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_0029;
		}
		if (L_1 == 1)
		{
			goto IL_01dd;
		}
		if (L_1 == 2)
		{
			goto IL_028c;
		}
		if (L_1 == 3)
		{
			goto IL_02d0;
		}
	}
	{
		goto IL_02d7;
	}

IL_0029:
	{
		Region_t8_110 * L_2 = (__this->___region_0);
		IL2CPP_RUNTIME_CLASS_INIT(PhotonPingManager_t8_109_il2cpp_TypeInfo_var);
		int32_t L_3 = ((PhotonPingManager_t8_109_StaticFields*)PhotonPingManager_t8_109_il2cpp_TypeInfo_var->static_fields)->___Attempts_1;
		int32_t L_4 = ((PhotonPingManager_t8_109_StaticFields*)PhotonPingManager_t8_109_il2cpp_TypeInfo_var->static_fields)->___MaxMilliseconsPerPing_3;
		NullCheck(L_2);
		L_2->___Ping_2 = ((int32_t)((int32_t)L_3*(int32_t)L_4));
		PhotonPingManager_t8_109 * L_5 = (__this->___U3CU3Ef__this_14);
		PhotonPingManager_t8_109 * L_6 = L_5;
		NullCheck(L_6);
		int32_t L_7 = (L_6->___PingsRunning_4);
		NullCheck(L_6);
		L_6->___PingsRunning_4 = ((int32_t)((int32_t)L_7+(int32_t)1));
		IL2CPP_RUNTIME_CLASS_INIT(PhotonHandler_t8_111_il2cpp_TypeInfo_var);
		Type_t * L_8 = ((PhotonHandler_t8_111_StaticFields*)PhotonHandler_t8_111_il2cpp_TypeInfo_var->static_fields)->___PingImplementation_11;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_9 = Type_GetTypeFromHandle_m1_893(NULL /*static, unused*/, LoadTypeToken(PingNativeDynamic_t5_41_0_0_0_var), /*hidden argument*/NULL);
		if ((!(((Object_t*)(Type_t *)L_8) == ((Object_t*)(Type_t *)L_9))))
		{
			goto IL_0080;
		}
	}
	{
		Debug_Log_m6_489(NULL /*static, unused*/, _stringLiteral3497, /*hidden argument*/NULL);
		PingNativeDynamic_t5_41 * L_10 = (PingNativeDynamic_t5_41 *)il2cpp_codegen_object_new (PingNativeDynamic_t5_41_il2cpp_TypeInfo_var);
		PingNativeDynamic__ctor_m5_259(L_10, /*hidden argument*/NULL);
		__this->___U3CpingU3E__0_1 = L_10;
		goto IL_00b9;
	}

IL_0080:
	{
		IL2CPP_RUNTIME_CLASS_INIT(PhotonHandler_t8_111_il2cpp_TypeInfo_var);
		Type_t * L_11 = ((PhotonHandler_t8_111_StaticFields*)PhotonHandler_t8_111_il2cpp_TypeInfo_var->static_fields)->___PingImplementation_11;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_12 = Type_GetTypeFromHandle_m1_893(NULL /*static, unused*/, LoadTypeToken(PingMono_t5_40_0_0_0_var), /*hidden argument*/NULL);
		if ((!(((Object_t*)(Type_t *)L_11) == ((Object_t*)(Type_t *)L_12))))
		{
			goto IL_00a4;
		}
	}
	{
		PingMono_t5_40 * L_13 = (PingMono_t5_40 *)il2cpp_codegen_object_new (PingMono_t5_40_il2cpp_TypeInfo_var);
		PingMono__ctor_m5_258(L_13, /*hidden argument*/NULL);
		__this->___U3CpingU3E__0_1 = L_13;
		goto IL_00b9;
	}

IL_00a4:
	{
		IL2CPP_RUNTIME_CLASS_INIT(PhotonHandler_t8_111_il2cpp_TypeInfo_var);
		Type_t * L_14 = ((PhotonHandler_t8_111_StaticFields*)PhotonHandler_t8_111_il2cpp_TypeInfo_var->static_fields)->___PingImplementation_11;
		Object_t * L_15 = Activator_CreateInstance_m1_4575(NULL /*static, unused*/, L_14, /*hidden argument*/NULL);
		__this->___U3CpingU3E__0_1 = ((PhotonPing_t5_39 *)CastclassClass(L_15, PhotonPing_t5_39_il2cpp_TypeInfo_var));
	}

IL_00b9:
	{
		__this->___U3CrttSumU3E__1_2 = (0.0f);
		__this->___U3CreplyCountU3E__2_3 = 0;
		Region_t8_110 * L_16 = (__this->___region_0);
		NullCheck(L_16);
		String_t* L_17 = (L_16->___HostAndPort_1);
		__this->___U3CcleanIpOfRegionU3E__3_4 = L_17;
		String_t* L_18 = (__this->___U3CcleanIpOfRegionU3E__3_4);
		NullCheck(L_18);
		int32_t L_19 = String_LastIndexOf_m1_390(L_18, ((int32_t)58), /*hidden argument*/NULL);
		__this->___U3CindexOfColonU3E__4_5 = L_19;
		int32_t L_20 = (__this->___U3CindexOfColonU3E__4_5);
		if ((((int32_t)L_20) <= ((int32_t)1)))
		{
			goto IL_0113;
		}
	}
	{
		String_t* L_21 = (__this->___U3CcleanIpOfRegionU3E__3_4);
		int32_t L_22 = (__this->___U3CindexOfColonU3E__4_5);
		NullCheck(L_21);
		String_t* L_23 = String_Substring_m1_352(L_21, 0, L_22, /*hidden argument*/NULL);
		__this->___U3CcleanIpOfRegionU3E__3_4 = L_23;
	}

IL_0113:
	{
		String_t* L_24 = (__this->___U3CcleanIpOfRegionU3E__3_4);
		IL2CPP_RUNTIME_CLASS_INIT(PhotonPingManager_t8_109_il2cpp_TypeInfo_var);
		String_t* L_25 = PhotonPingManager_ResolveHost_m8_805(NULL /*static, unused*/, L_24, /*hidden argument*/NULL);
		__this->___U3CcleanIpOfRegionU3E__3_4 = L_25;
		__this->___U3CiU3E__5_6 = 0;
		goto IL_029a;
	}

IL_0130:
	{
		__this->___U3CovertimeU3E__6_7 = 0;
		Stopwatch_t3_20 * L_26 = (Stopwatch_t3_20 *)il2cpp_codegen_object_new (Stopwatch_t3_20_il2cpp_TypeInfo_var);
		Stopwatch__ctor_m3_106(L_26, /*hidden argument*/NULL);
		__this->___U3CswU3E__7_8 = L_26;
		Stopwatch_t3_20 * L_27 = (__this->___U3CswU3E__7_8);
		NullCheck(L_27);
		Stopwatch_Start_m3_113(L_27, /*hidden argument*/NULL);
	}

IL_014d:
	try
	{ // begin try (depth: 1)
		PhotonPing_t5_39 * L_28 = (__this->___U3CpingU3E__0_1);
		String_t* L_29 = (__this->___U3CcleanIpOfRegionU3E__3_4);
		NullCheck(L_28);
		VirtFuncInvoker1< bool, String_t* >::Invoke(5 /* System.Boolean ExitGames.Client.Photon.PhotonPing::StartPing(System.String) */, L_28, L_29);
		goto IL_019e;
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t1_33 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (Exception_t1_33_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_0164;
		throw e;
	}

CATCH_0164:
	{ // begin catch(System.Exception)
		{
			V_1 = ((Exception_t1_33 *)__exception_local);
			Exception_t1_33 * L_30 = V_1;
			__this->___U3CeU3E__8_9 = L_30;
			Exception_t1_33 * L_31 = (__this->___U3CeU3E__8_9);
			IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
			String_t* L_32 = String_Concat_m1_416(NULL /*static, unused*/, _stringLiteral3498, L_31, /*hidden argument*/NULL);
			Debug_Log_m6_489(NULL /*static, unused*/, L_32, /*hidden argument*/NULL);
			PhotonPingManager_t8_109 * L_33 = (__this->___U3CU3Ef__this_14);
			PhotonPingManager_t8_109 * L_34 = L_33;
			NullCheck(L_34);
			int32_t L_35 = (L_34->___PingsRunning_4);
			NullCheck(L_34);
			L_34->___PingsRunning_4 = ((int32_t)((int32_t)L_35-(int32_t)1));
			goto IL_02aa;
		}

IL_0199:
		{
			; // IL_0199: leave IL_019e
		}
	} // end catch (depth: 1)

IL_019e:
	{
		goto IL_01dd;
	}

IL_01a3:
	{
		Stopwatch_t3_20 * L_36 = (__this->___U3CswU3E__7_8);
		NullCheck(L_36);
		int64_t L_37 = Stopwatch_get_ElapsedMilliseconds_m3_110(L_36, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(PhotonPingManager_t8_109_il2cpp_TypeInfo_var);
		int32_t L_38 = ((PhotonPingManager_t8_109_StaticFields*)PhotonPingManager_t8_109_il2cpp_TypeInfo_var->static_fields)->___MaxMilliseconsPerPing_3;
		if ((((int64_t)L_37) < ((int64_t)(((int64_t)((int64_t)L_38))))))
		{
			goto IL_01c5;
		}
	}
	{
		__this->___U3CovertimeU3E__6_7 = 1;
		goto IL_01ed;
	}

IL_01c5:
	{
		int32_t L_39 = 0;
		Object_t * L_40 = Box(Int32_t1_3_il2cpp_TypeInfo_var, &L_39);
		__this->___U24current_12 = L_40;
		__this->___U24PC_11 = 1;
		goto IL_02d9;
	}

IL_01dd:
	{
		PhotonPing_t5_39 * L_41 = (__this->___U3CpingU3E__0_1);
		NullCheck(L_41);
		bool L_42 = (bool)VirtFuncInvoker0< bool >::Invoke(6 /* System.Boolean ExitGames.Client.Photon.PhotonPing::Done() */, L_41);
		if (!L_42)
		{
			goto IL_01a3;
		}
	}

IL_01ed:
	{
		Stopwatch_t3_20 * L_43 = (__this->___U3CswU3E__7_8);
		NullCheck(L_43);
		int64_t L_44 = Stopwatch_get_ElapsedMilliseconds_m3_110(L_43, /*hidden argument*/NULL);
		__this->___U3CrttU3E__9_10 = (((int32_t)((int32_t)L_44)));
		IL2CPP_RUNTIME_CLASS_INIT(PhotonPingManager_t8_109_il2cpp_TypeInfo_var);
		bool L_45 = ((PhotonPingManager_t8_109_StaticFields*)PhotonPingManager_t8_109_il2cpp_TypeInfo_var->static_fields)->___IgnoreInitialAttempt_2;
		if (!L_45)
		{
			goto IL_0219;
		}
	}
	{
		int32_t L_46 = (__this->___U3CiU3E__5_6);
		if (L_46)
		{
			goto IL_0219;
		}
	}
	{
		goto IL_0270;
	}

IL_0219:
	{
		PhotonPing_t5_39 * L_47 = (__this->___U3CpingU3E__0_1);
		NullCheck(L_47);
		bool L_48 = (L_47->___Successful_1);
		if (!L_48)
		{
			goto IL_0270;
		}
	}
	{
		bool L_49 = (__this->___U3CovertimeU3E__6_7);
		if (L_49)
		{
			goto IL_0270;
		}
	}
	{
		float L_50 = (__this->___U3CrttSumU3E__1_2);
		int32_t L_51 = (__this->___U3CrttU3E__9_10);
		__this->___U3CrttSumU3E__1_2 = ((float)((float)L_50+(float)(((float)((float)L_51)))));
		int32_t L_52 = (__this->___U3CreplyCountU3E__2_3);
		__this->___U3CreplyCountU3E__2_3 = ((int32_t)((int32_t)L_52+(int32_t)1));
		Region_t8_110 * L_53 = (__this->___region_0);
		float L_54 = (__this->___U3CrttSumU3E__1_2);
		int32_t L_55 = (__this->___U3CreplyCountU3E__2_3);
		NullCheck(L_53);
		L_53->___Ping_2 = (((int32_t)((int32_t)((float)((float)L_54/(float)(((float)((float)L_55))))))));
	}

IL_0270:
	{
		WaitForSeconds_t6_11 * L_56 = (WaitForSeconds_t6_11 *)il2cpp_codegen_object_new (WaitForSeconds_t6_11_il2cpp_TypeInfo_var);
		WaitForSeconds__ctor_m6_10(L_56, (0.1f), /*hidden argument*/NULL);
		__this->___U24current_12 = L_56;
		__this->___U24PC_11 = 2;
		goto IL_02d9;
	}

IL_028c:
	{
		int32_t L_57 = (__this->___U3CiU3E__5_6);
		__this->___U3CiU3E__5_6 = ((int32_t)((int32_t)L_57+(int32_t)1));
	}

IL_029a:
	{
		int32_t L_58 = (__this->___U3CiU3E__5_6);
		IL2CPP_RUNTIME_CLASS_INIT(PhotonPingManager_t8_109_il2cpp_TypeInfo_var);
		int32_t L_59 = ((PhotonPingManager_t8_109_StaticFields*)PhotonPingManager_t8_109_il2cpp_TypeInfo_var->static_fields)->___Attempts_1;
		if ((((int32_t)L_58) < ((int32_t)L_59)))
		{
			goto IL_0130;
		}
	}

IL_02aa:
	{
		PhotonPingManager_t8_109 * L_60 = (__this->___U3CU3Ef__this_14);
		PhotonPingManager_t8_109 * L_61 = L_60;
		NullCheck(L_61);
		int32_t L_62 = (L_61->___PingsRunning_4);
		NullCheck(L_61);
		L_61->___PingsRunning_4 = ((int32_t)((int32_t)L_62-(int32_t)1));
		__this->___U24current_12 = NULL;
		__this->___U24PC_11 = 3;
		goto IL_02d9;
	}

IL_02d0:
	{
		__this->___U24PC_11 = (-1);
	}

IL_02d7:
	{
		return 0;
	}

IL_02d9:
	{
		return 1;
	}
	// Dead block : IL_02db: ldloc.2
}
// System.Void PhotonPingManager/<PingSocket>c__Iterator1::Dispose()
extern "C" void U3CPingSocketU3Ec__Iterator1_Dispose_m8_798 (U3CPingSocketU3Ec__Iterator1_t8_122 * __this, const MethodInfo* method)
{
	{
		__this->___U24PC_11 = (-1);
		return;
	}
}
// System.Void PhotonPingManager/<PingSocket>c__Iterator1::Reset()
extern TypeInfo* NotSupportedException_t1_749_il2cpp_TypeInfo_var;
extern "C" void U3CPingSocketU3Ec__Iterator1_Reset_m8_799 (U3CPingSocketU3Ec__Iterator1_t8_122 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NotSupportedException_t1_749_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(6);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t1_749 * L_0 = (NotSupportedException_t1_749 *)il2cpp_codegen_object_new (NotSupportedException_t1_749_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m1_5205(L_0, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_0);
	}
}
// System.Void PhotonPingManager::.ctor()
extern "C" void PhotonPingManager__ctor_m8_800 (PhotonPingManager_t8_109 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1_0(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void PhotonPingManager::.cctor()
extern TypeInfo* PhotonPingManager_t8_109_il2cpp_TypeInfo_var;
extern "C" void PhotonPingManager__cctor_m8_801 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		PhotonPingManager_t8_109_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1151);
		s_Il2CppMethodIntialized = true;
	}
	{
		((PhotonPingManager_t8_109_StaticFields*)PhotonPingManager_t8_109_il2cpp_TypeInfo_var->static_fields)->___Attempts_1 = 5;
		((PhotonPingManager_t8_109_StaticFields*)PhotonPingManager_t8_109_il2cpp_TypeInfo_var->static_fields)->___IgnoreInitialAttempt_2 = 1;
		((PhotonPingManager_t8_109_StaticFields*)PhotonPingManager_t8_109_il2cpp_TypeInfo_var->static_fields)->___MaxMilliseconsPerPing_3 = ((int32_t)800);
		return;
	}
}
// Region PhotonPingManager::get_BestRegion()
extern TypeInfo* PhotonNetwork_t8_114_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* Enumerator_t1_944_il2cpp_TypeInfo_var;
extern TypeInfo* IDisposable_t1_835_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1_GetEnumerator_m1_5674_MethodInfo_var;
extern const MethodInfo* Enumerator_get_Current_m1_5675_MethodInfo_var;
extern const MethodInfo* Enumerator_MoveNext_m1_5676_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral3494;
extern "C" Region_t8_110 * PhotonPingManager_get_BestRegion_m8_802 (PhotonPingManager_t8_109 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		PhotonNetwork_t8_114_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1058);
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(11);
		Enumerator_t1_944_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1152);
		IDisposable_t1_835_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(142);
		List_1_GetEnumerator_m1_5674_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483931);
		Enumerator_get_Current_m1_5675_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483932);
		Enumerator_MoveNext_m1_5676_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483933);
		_stringLiteral3494 = il2cpp_codegen_string_literal_from_index(3494);
		s_Il2CppMethodIntialized = true;
	}
	Region_t8_110 * V_0 = {0};
	int32_t V_1 = 0;
	Region_t8_110 * V_2 = {0};
	Enumerator_t1_944  V_3 = {0};
	Exception_t1_33 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1_33 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		V_0 = (Region_t8_110 *)NULL;
		V_1 = ((int32_t)2147483647);
		IL2CPP_RUNTIME_CLASS_INIT(PhotonNetwork_t8_114_il2cpp_TypeInfo_var);
		NetworkingPeer_t8_98 * L_0 = ((PhotonNetwork_t8_114_StaticFields*)PhotonNetwork_t8_114_il2cpp_TypeInfo_var->static_fields)->___networkingPeer_4;
		NullCheck(L_0);
		List_1_t1_941 * L_1 = NetworkingPeer_get_AvailableRegions_m8_384(L_0, /*hidden argument*/NULL);
		NullCheck(L_1);
		Enumerator_t1_944  L_2 = List_1_GetEnumerator_m1_5674(L_1, /*hidden argument*/List_1_GetEnumerator_m1_5674_MethodInfo_var);
		V_3 = L_2;
	}

IL_0018:
	try
	{ // begin try (depth: 1)
		{
			goto IL_0055;
		}

IL_001d:
		{
			Region_t8_110 * L_3 = Enumerator_get_Current_m1_5675((&V_3), /*hidden argument*/Enumerator_get_Current_m1_5675_MethodInfo_var);
			V_2 = L_3;
			Region_t8_110 * L_4 = V_2;
			IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
			String_t* L_5 = String_Concat_m1_416(NULL /*static, unused*/, _stringLiteral3494, L_4, /*hidden argument*/NULL);
			Debug_Log_m6_489(NULL /*static, unused*/, L_5, /*hidden argument*/NULL);
			Region_t8_110 * L_6 = V_2;
			NullCheck(L_6);
			int32_t L_7 = (L_6->___Ping_2);
			if (!L_7)
			{
				goto IL_0055;
			}
		}

IL_0040:
		{
			Region_t8_110 * L_8 = V_2;
			NullCheck(L_8);
			int32_t L_9 = (L_8->___Ping_2);
			int32_t L_10 = V_1;
			if ((((int32_t)L_9) >= ((int32_t)L_10)))
			{
				goto IL_0055;
			}
		}

IL_004c:
		{
			Region_t8_110 * L_11 = V_2;
			NullCheck(L_11);
			int32_t L_12 = (L_11->___Ping_2);
			V_1 = L_12;
			Region_t8_110 * L_13 = V_2;
			V_0 = L_13;
		}

IL_0055:
		{
			bool L_14 = Enumerator_MoveNext_m1_5676((&V_3), /*hidden argument*/Enumerator_MoveNext_m1_5676_MethodInfo_var);
			if (L_14)
			{
				goto IL_001d;
			}
		}

IL_0061:
		{
			IL2CPP_LEAVE(0x72, FINALLY_0066);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1_33 *)e.ex;
		goto FINALLY_0066;
	}

FINALLY_0066:
	{ // begin finally (depth: 1)
		Enumerator_t1_944  L_15 = V_3;
		Enumerator_t1_944  L_16 = L_15;
		Object_t * L_17 = Box(Enumerator_t1_944_il2cpp_TypeInfo_var, &L_16);
		NullCheck(L_17);
		InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1_835_il2cpp_TypeInfo_var, L_17);
		IL2CPP_END_FINALLY(102)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(102)
	{
		IL2CPP_JUMP_TBL(0x72, IL_0072)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1_33 *)
	}

IL_0072:
	{
		Region_t8_110 * L_18 = V_0;
		return L_18;
	}
}
// System.Boolean PhotonPingManager::get_Done()
extern "C" bool PhotonPingManager_get_Done_m8_803 (PhotonPingManager_t8_109 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (__this->___PingsRunning_4);
		return ((((int32_t)L_0) == ((int32_t)0))? 1 : 0);
	}
}
// System.Collections.IEnumerator PhotonPingManager::PingSocket(Region)
extern TypeInfo* U3CPingSocketU3Ec__Iterator1_t8_122_il2cpp_TypeInfo_var;
extern "C" Object_t * PhotonPingManager_PingSocket_m8_804 (PhotonPingManager_t8_109 * __this, Region_t8_110 * ___region, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		U3CPingSocketU3Ec__Iterator1_t8_122_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1163);
		s_Il2CppMethodIntialized = true;
	}
	U3CPingSocketU3Ec__Iterator1_t8_122 * V_0 = {0};
	{
		U3CPingSocketU3Ec__Iterator1_t8_122 * L_0 = (U3CPingSocketU3Ec__Iterator1_t8_122 *)il2cpp_codegen_object_new (U3CPingSocketU3Ec__Iterator1_t8_122_il2cpp_TypeInfo_var);
		U3CPingSocketU3Ec__Iterator1__ctor_m8_794(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CPingSocketU3Ec__Iterator1_t8_122 * L_1 = V_0;
		Region_t8_110 * L_2 = ___region;
		NullCheck(L_1);
		L_1->___region_0 = L_2;
		U3CPingSocketU3Ec__Iterator1_t8_122 * L_3 = V_0;
		Region_t8_110 * L_4 = ___region;
		NullCheck(L_3);
		L_3->___U3CU24U3Eregion_13 = L_4;
		U3CPingSocketU3Ec__Iterator1_t8_122 * L_5 = V_0;
		NullCheck(L_5);
		L_5->___U3CU3Ef__this_14 = __this;
		U3CPingSocketU3Ec__Iterator1_t8_122 * L_6 = V_0;
		return L_6;
	}
}
// System.String PhotonPingManager::ResolveHost(System.String)
extern TypeInfo* Dns_t3_38_il2cpp_TypeInfo_var;
extern TypeInfo* Exception_t1_33_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3495;
extern Il2CppCodeGenString* _stringLiteral3496;
extern "C" String_t* PhotonPingManager_ResolveHost_m8_805 (Object_t * __this /* static, unused */, String_t* ___hostName, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Dns_t3_38_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(633);
		Exception_t1_33_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(45);
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(11);
		_stringLiteral3495 = il2cpp_codegen_string_literal_from_index(3495);
		_stringLiteral3496 = il2cpp_codegen_string_literal_from_index(3496);
		s_Il2CppMethodIntialized = true;
	}
	IPAddressU5BU5D_t3_57* V_0 = {0};
	int32_t V_1 = 0;
	IPAddress_t3_54 * V_2 = {0};
	String_t* V_3 = {0};
	Exception_t1_33 * V_4 = {0};
	String_t* V_5 = {0};
	Exception_t1_33 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1_33 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		{
			String_t* L_0 = ___hostName;
			IL2CPP_RUNTIME_CLASS_INIT(Dns_t3_38_il2cpp_TypeInfo_var);
			IPAddressU5BU5D_t3_57* L_1 = Dns_GetHostAddresses_m3_179(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
			V_0 = L_1;
			IPAddressU5BU5D_t3_57* L_2 = V_0;
			NullCheck(L_2);
			if ((!(((uint32_t)(((int32_t)((int32_t)(((Array_t *)L_2)->max_length))))) == ((uint32_t)1))))
			{
				goto IL_001f;
			}
		}

IL_0010:
		{
			IPAddressU5BU5D_t3_57* L_3 = V_0;
			NullCheck(L_3);
			IL2CPP_ARRAY_BOUNDS_CHECK(L_3, 0);
			int32_t L_4 = 0;
			NullCheck((*(IPAddress_t3_54 **)(IPAddress_t3_54 **)SZArrayLdElema(L_3, L_4, sizeof(IPAddress_t3_54 *))));
			String_t* L_5 = (String_t*)VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Net.IPAddress::ToString() */, (*(IPAddress_t3_54 **)(IPAddress_t3_54 **)SZArrayLdElema(L_3, L_4, sizeof(IPAddress_t3_54 *))));
			V_5 = L_5;
			goto IL_008e;
		}

IL_001f:
		{
			V_1 = 0;
			goto IL_0051;
		}

IL_0026:
		{
			IPAddressU5BU5D_t3_57* L_6 = V_0;
			int32_t L_7 = V_1;
			NullCheck(L_6);
			IL2CPP_ARRAY_BOUNDS_CHECK(L_6, L_7);
			int32_t L_8 = L_7;
			V_2 = (*(IPAddress_t3_54 **)(IPAddress_t3_54 **)SZArrayLdElema(L_6, L_8, sizeof(IPAddress_t3_54 *)));
			IPAddress_t3_54 * L_9 = V_2;
			if (!L_9)
			{
				goto IL_004d;
			}
		}

IL_0030:
		{
			IPAddress_t3_54 * L_10 = V_2;
			NullCheck(L_10);
			String_t* L_11 = (String_t*)VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Net.IPAddress::ToString() */, L_10);
			V_3 = L_11;
			String_t* L_12 = V_3;
			NullCheck(L_12);
			int32_t L_13 = String_IndexOf_m1_381(L_12, ((int32_t)46), /*hidden argument*/NULL);
			if ((((int32_t)L_13) < ((int32_t)0)))
			{
				goto IL_004d;
			}
		}

IL_0045:
		{
			String_t* L_14 = V_3;
			V_5 = L_14;
			goto IL_008e;
		}

IL_004d:
		{
			int32_t L_15 = V_1;
			V_1 = ((int32_t)((int32_t)L_15+(int32_t)1));
		}

IL_0051:
		{
			int32_t L_16 = V_1;
			IPAddressU5BU5D_t3_57* L_17 = V_0;
			NullCheck(L_17);
			if ((((int32_t)L_16) < ((int32_t)(((int32_t)((int32_t)(((Array_t *)L_17)->max_length)))))))
			{
				goto IL_0026;
			}
		}

IL_005a:
		{
			goto IL_0088;
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t1_33 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (Exception_t1_33_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_005f;
		throw e;
	}

CATCH_005f:
	{ // begin catch(System.Exception)
		V_4 = ((Exception_t1_33 *)__exception_local);
		Exception_t1_33 * L_18 = V_4;
		NullCheck(L_18);
		String_t* L_19 = (String_t*)VirtFuncInvoker0< String_t* >::Invoke(7 /* System.String System.Exception::get_Source() */, L_18);
		Exception_t1_33 * L_20 = V_4;
		NullCheck(L_20);
		String_t* L_21 = (String_t*)VirtFuncInvoker0< String_t* >::Invoke(6 /* System.String System.Exception::get_Message() */, L_20);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_22 = String_Concat_m1_420(NULL /*static, unused*/, _stringLiteral3495, L_19, _stringLiteral3496, L_21, /*hidden argument*/NULL);
		Debug_Log_m6_489(NULL /*static, unused*/, L_22, /*hidden argument*/NULL);
		goto IL_0088;
	} // end catch (depth: 1)

IL_0088:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_23 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->___Empty_2;
		return L_23;
	}

IL_008e:
	{
		String_t* L_24 = V_5;
		return L_24;
	}
}
// System.Void PunRPC::.ctor()
extern "C" void PunRPC__ctor_m8_806 (PunRPC_t8_123 * __this, const MethodInfo* method)
{
	{
		Attribute__ctor_m1_17(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Room::.ctor(System.String,RoomOptions)
extern TypeInfo* RoomOptions_t8_78_il2cpp_TypeInfo_var;
extern "C" void Room__ctor_m8_807 (Room_t8_100 * __this, String_t* ___roomName, RoomOptions_t8_78 * ___options, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		RoomOptions_t8_78_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1090);
		s_Il2CppMethodIntialized = true;
	}
	{
		String_t* L_0 = ___roomName;
		RoomInfo__ctor_m8_826(__this, L_0, (Hashtable_t5_1 *)NULL, /*hidden argument*/NULL);
		RoomOptions_t8_78 * L_1 = ___options;
		if (L_1)
		{
			goto IL_0015;
		}
	}
	{
		RoomOptions_t8_78 * L_2 = (RoomOptions_t8_78 *)il2cpp_codegen_object_new (RoomOptions_t8_78_il2cpp_TypeInfo_var);
		RoomOptions__ctor_m8_528(L_2, /*hidden argument*/NULL);
		___options = L_2;
	}

IL_0015:
	{
		RoomOptions_t8_78 * L_3 = ___options;
		NullCheck(L_3);
		bool L_4 = RoomOptions_get_isVisible_m8_529(L_3, /*hidden argument*/NULL);
		((RoomInfo_t8_124 *)__this)->___visibleField_3 = L_4;
		RoomOptions_t8_78 * L_5 = ___options;
		NullCheck(L_5);
		bool L_6 = RoomOptions_get_isOpen_m8_531(L_5, /*hidden argument*/NULL);
		((RoomInfo_t8_124 *)__this)->___openField_2 = L_6;
		RoomOptions_t8_78 * L_7 = ___options;
		NullCheck(L_7);
		uint8_t L_8 = (L_7->___maxPlayers_2);
		((RoomInfo_t8_124 *)__this)->___maxPlayersField_1 = L_8;
		((RoomInfo_t8_124 *)__this)->___autoCleanUpField_4 = 0;
		RoomOptions_t8_78 * L_9 = ___options;
		NullCheck(L_9);
		Hashtable_t5_1 * L_10 = (L_9->___customRoomProperties_4);
		RoomInfo_InternalCacheProperties_m8_844(__this, L_10, /*hidden argument*/NULL);
		RoomOptions_t8_78 * L_11 = ___options;
		NullCheck(L_11);
		StringU5BU5D_t1_202* L_12 = (L_11->___customRoomPropertiesForLobby_5);
		Room_set_propertiesListedInLobby_m8_818(__this, L_12, /*hidden argument*/NULL);
		return;
	}
}
// System.Int32 Room::get_playerCount()
extern TypeInfo* PhotonNetwork_t8_114_il2cpp_TypeInfo_var;
extern "C" int32_t Room_get_playerCount_m8_808 (Room_t8_100 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		PhotonNetwork_t8_114_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1058);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(PhotonNetwork_t8_114_il2cpp_TypeInfo_var);
		PhotonPlayerU5BU5D_t8_101* L_0 = PhotonNetwork_get_playerList_m8_616(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_0012;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(PhotonNetwork_t8_114_il2cpp_TypeInfo_var);
		PhotonPlayerU5BU5D_t8_101* L_1 = PhotonNetwork_get_playerList_m8_616(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_1);
		return (((int32_t)((int32_t)(((Array_t *)L_1)->max_length))));
	}

IL_0012:
	{
		return 0;
	}
}
// System.String Room::get_name()
extern "C" String_t* Room_get_name_m8_809 (Room_t8_100 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = (((RoomInfo_t8_124 *)__this)->___nameField_5);
		return L_0;
	}
}
// System.Void Room::set_name(System.String)
extern "C" void Room_set_name_m8_810 (Room_t8_100 * __this, String_t* ___value, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value;
		((RoomInfo_t8_124 *)__this)->___nameField_5 = L_0;
		return;
	}
}
// System.Int32 Room::get_maxPlayers()
extern "C" int32_t Room_get_maxPlayers_m8_811 (Room_t8_100 * __this, const MethodInfo* method)
{
	{
		uint8_t L_0 = (((RoomInfo_t8_124 *)__this)->___maxPlayersField_1);
		return L_0;
	}
}
// System.Void Room::set_maxPlayers(System.Int32)
extern TypeInfo* PhotonNetwork_t8_114_il2cpp_TypeInfo_var;
extern TypeInfo* Int32_t1_3_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* Hashtable_t5_1_il2cpp_TypeInfo_var;
extern TypeInfo* Byte_t1_11_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3499;
extern Il2CppCodeGenString* _stringLiteral3500;
extern Il2CppCodeGenString* _stringLiteral3501;
extern "C" void Room_set_maxPlayers_m8_812 (Room_t8_100 * __this, int32_t ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		PhotonNetwork_t8_114_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1058);
		Int32_t1_3_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(8);
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(11);
		Hashtable_t5_1_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(891);
		Byte_t1_11_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(22);
		_stringLiteral3499 = il2cpp_codegen_string_literal_from_index(3499);
		_stringLiteral3500 = il2cpp_codegen_string_literal_from_index(3500);
		_stringLiteral3501 = il2cpp_codegen_string_literal_from_index(3501);
		s_Il2CppMethodIntialized = true;
	}
	Hashtable_t5_1 * V_0 = {0};
	{
		IL2CPP_RUNTIME_CLASS_INIT(PhotonNetwork_t8_114_il2cpp_TypeInfo_var);
		Room_t8_100 * L_0 = PhotonNetwork_get_room_m8_611(NULL /*static, unused*/, /*hidden argument*/NULL);
		bool L_1 = (bool)VirtFuncInvoker1< bool, Object_t * >::Invoke(0 /* System.Boolean RoomInfo::Equals(System.Object) */, __this, L_0);
		if (L_1)
		{
			goto IL_001a;
		}
	}
	{
		Debug_LogWarning_m6_491(NULL /*static, unused*/, _stringLiteral3499, /*hidden argument*/NULL);
	}

IL_001a:
	{
		int32_t L_2 = ___value;
		if ((((int32_t)L_2) <= ((int32_t)((int32_t)255))))
		{
			goto IL_0046;
		}
	}
	{
		int32_t L_3 = ___value;
		int32_t L_4 = L_3;
		Object_t * L_5 = Box(Int32_t1_3_il2cpp_TypeInfo_var, &L_4);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_6 = String_Concat_m1_417(NULL /*static, unused*/, _stringLiteral3500, L_5, _stringLiteral3501, /*hidden argument*/NULL);
		Debug_LogWarning_m6_491(NULL /*static, unused*/, L_6, /*hidden argument*/NULL);
		___value = ((int32_t)255);
	}

IL_0046:
	{
		int32_t L_7 = ___value;
		uint8_t L_8 = (((RoomInfo_t8_124 *)__this)->___maxPlayersField_1);
		if ((((int32_t)L_7) == ((int32_t)L_8)))
		{
			goto IL_0087;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(PhotonNetwork_t8_114_il2cpp_TypeInfo_var);
		bool L_9 = PhotonNetwork_get_offlineMode_m8_623(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (L_9)
		{
			goto IL_0087;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(PhotonNetwork_t8_114_il2cpp_TypeInfo_var);
		NetworkingPeer_t8_98 * L_10 = ((PhotonNetwork_t8_114_StaticFields*)PhotonNetwork_t8_114_il2cpp_TypeInfo_var->static_fields)->___networkingPeer_4;
		Hashtable_t5_1 * L_11 = (Hashtable_t5_1 *)il2cpp_codegen_object_new (Hashtable_t5_1_il2cpp_TypeInfo_var);
		Hashtable__ctor_m5_0(L_11, /*hidden argument*/NULL);
		V_0 = L_11;
		Hashtable_t5_1 * L_12 = V_0;
		uint8_t L_13 = ((int32_t)255);
		Object_t * L_14 = Box(Byte_t1_11_il2cpp_TypeInfo_var, &L_13);
		int32_t L_15 = ___value;
		uint8_t L_16 = (((int32_t)((uint8_t)L_15)));
		Object_t * L_17 = Box(Byte_t1_11_il2cpp_TypeInfo_var, &L_16);
		NullCheck(L_12);
		VirtActionInvoker2< Object_t *, Object_t * >::Invoke(27 /* System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Object>::Add(!0,!1) */, L_12, L_14, L_17);
		Hashtable_t5_1 * L_18 = V_0;
		NullCheck(L_10);
		LoadbalancingPeer_OpSetPropertiesOfRoom_m8_332(L_10, L_18, (Hashtable_t5_1 *)NULL, 0, /*hidden argument*/NULL);
	}

IL_0087:
	{
		int32_t L_19 = ___value;
		((RoomInfo_t8_124 *)__this)->___maxPlayersField_1 = (((int32_t)((uint8_t)L_19)));
		return;
	}
}
// System.Boolean Room::get_open()
extern "C" bool Room_get_open_m8_813 (Room_t8_100 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (((RoomInfo_t8_124 *)__this)->___openField_2);
		return L_0;
	}
}
// System.Void Room::set_open(System.Boolean)
extern TypeInfo* PhotonNetwork_t8_114_il2cpp_TypeInfo_var;
extern TypeInfo* Hashtable_t5_1_il2cpp_TypeInfo_var;
extern TypeInfo* Byte_t1_11_il2cpp_TypeInfo_var;
extern TypeInfo* Boolean_t1_20_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3502;
extern "C" void Room_set_open_m8_814 (Room_t8_100 * __this, bool ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		PhotonNetwork_t8_114_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1058);
		Hashtable_t5_1_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(891);
		Byte_t1_11_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(22);
		Boolean_t1_20_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(49);
		_stringLiteral3502 = il2cpp_codegen_string_literal_from_index(3502);
		s_Il2CppMethodIntialized = true;
	}
	Hashtable_t5_1 * V_0 = {0};
	{
		IL2CPP_RUNTIME_CLASS_INIT(PhotonNetwork_t8_114_il2cpp_TypeInfo_var);
		Room_t8_100 * L_0 = PhotonNetwork_get_room_m8_611(NULL /*static, unused*/, /*hidden argument*/NULL);
		bool L_1 = (bool)VirtFuncInvoker1< bool, Object_t * >::Invoke(0 /* System.Boolean RoomInfo::Equals(System.Object) */, __this, L_0);
		if (L_1)
		{
			goto IL_001a;
		}
	}
	{
		Debug_LogWarning_m6_491(NULL /*static, unused*/, _stringLiteral3502, /*hidden argument*/NULL);
	}

IL_001a:
	{
		bool L_2 = ___value;
		bool L_3 = (((RoomInfo_t8_124 *)__this)->___openField_2);
		if ((((int32_t)L_2) == ((int32_t)L_3)))
		{
			goto IL_005a;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(PhotonNetwork_t8_114_il2cpp_TypeInfo_var);
		bool L_4 = PhotonNetwork_get_offlineMode_m8_623(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (L_4)
		{
			goto IL_005a;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(PhotonNetwork_t8_114_il2cpp_TypeInfo_var);
		NetworkingPeer_t8_98 * L_5 = ((PhotonNetwork_t8_114_StaticFields*)PhotonNetwork_t8_114_il2cpp_TypeInfo_var->static_fields)->___networkingPeer_4;
		Hashtable_t5_1 * L_6 = (Hashtable_t5_1 *)il2cpp_codegen_object_new (Hashtable_t5_1_il2cpp_TypeInfo_var);
		Hashtable__ctor_m5_0(L_6, /*hidden argument*/NULL);
		V_0 = L_6;
		Hashtable_t5_1 * L_7 = V_0;
		uint8_t L_8 = ((int32_t)253);
		Object_t * L_9 = Box(Byte_t1_11_il2cpp_TypeInfo_var, &L_8);
		bool L_10 = ___value;
		bool L_11 = L_10;
		Object_t * L_12 = Box(Boolean_t1_20_il2cpp_TypeInfo_var, &L_11);
		NullCheck(L_7);
		VirtActionInvoker2< Object_t *, Object_t * >::Invoke(27 /* System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Object>::Add(!0,!1) */, L_7, L_9, L_12);
		Hashtable_t5_1 * L_13 = V_0;
		NullCheck(L_5);
		LoadbalancingPeer_OpSetPropertiesOfRoom_m8_332(L_5, L_13, (Hashtable_t5_1 *)NULL, 0, /*hidden argument*/NULL);
	}

IL_005a:
	{
		bool L_14 = ___value;
		((RoomInfo_t8_124 *)__this)->___openField_2 = L_14;
		return;
	}
}
// System.Boolean Room::get_visible()
extern "C" bool Room_get_visible_m8_815 (Room_t8_100 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (((RoomInfo_t8_124 *)__this)->___visibleField_3);
		return L_0;
	}
}
// System.Void Room::set_visible(System.Boolean)
extern TypeInfo* PhotonNetwork_t8_114_il2cpp_TypeInfo_var;
extern TypeInfo* Hashtable_t5_1_il2cpp_TypeInfo_var;
extern TypeInfo* Byte_t1_11_il2cpp_TypeInfo_var;
extern TypeInfo* Boolean_t1_20_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3503;
extern "C" void Room_set_visible_m8_816 (Room_t8_100 * __this, bool ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		PhotonNetwork_t8_114_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1058);
		Hashtable_t5_1_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(891);
		Byte_t1_11_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(22);
		Boolean_t1_20_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(49);
		_stringLiteral3503 = il2cpp_codegen_string_literal_from_index(3503);
		s_Il2CppMethodIntialized = true;
	}
	Hashtable_t5_1 * V_0 = {0};
	{
		IL2CPP_RUNTIME_CLASS_INIT(PhotonNetwork_t8_114_il2cpp_TypeInfo_var);
		Room_t8_100 * L_0 = PhotonNetwork_get_room_m8_611(NULL /*static, unused*/, /*hidden argument*/NULL);
		bool L_1 = (bool)VirtFuncInvoker1< bool, Object_t * >::Invoke(0 /* System.Boolean RoomInfo::Equals(System.Object) */, __this, L_0);
		if (L_1)
		{
			goto IL_001a;
		}
	}
	{
		Debug_LogWarning_m6_491(NULL /*static, unused*/, _stringLiteral3503, /*hidden argument*/NULL);
	}

IL_001a:
	{
		bool L_2 = ___value;
		bool L_3 = (((RoomInfo_t8_124 *)__this)->___visibleField_3);
		if ((((int32_t)L_2) == ((int32_t)L_3)))
		{
			goto IL_005a;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(PhotonNetwork_t8_114_il2cpp_TypeInfo_var);
		bool L_4 = PhotonNetwork_get_offlineMode_m8_623(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (L_4)
		{
			goto IL_005a;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(PhotonNetwork_t8_114_il2cpp_TypeInfo_var);
		NetworkingPeer_t8_98 * L_5 = ((PhotonNetwork_t8_114_StaticFields*)PhotonNetwork_t8_114_il2cpp_TypeInfo_var->static_fields)->___networkingPeer_4;
		Hashtable_t5_1 * L_6 = (Hashtable_t5_1 *)il2cpp_codegen_object_new (Hashtable_t5_1_il2cpp_TypeInfo_var);
		Hashtable__ctor_m5_0(L_6, /*hidden argument*/NULL);
		V_0 = L_6;
		Hashtable_t5_1 * L_7 = V_0;
		uint8_t L_8 = ((int32_t)254);
		Object_t * L_9 = Box(Byte_t1_11_il2cpp_TypeInfo_var, &L_8);
		bool L_10 = ___value;
		bool L_11 = L_10;
		Object_t * L_12 = Box(Boolean_t1_20_il2cpp_TypeInfo_var, &L_11);
		NullCheck(L_7);
		VirtActionInvoker2< Object_t *, Object_t * >::Invoke(27 /* System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Object>::Add(!0,!1) */, L_7, L_9, L_12);
		Hashtable_t5_1 * L_13 = V_0;
		NullCheck(L_5);
		LoadbalancingPeer_OpSetPropertiesOfRoom_m8_332(L_5, L_13, (Hashtable_t5_1 *)NULL, 0, /*hidden argument*/NULL);
	}

IL_005a:
	{
		bool L_14 = ___value;
		((RoomInfo_t8_124 *)__this)->___visibleField_3 = L_14;
		return;
	}
}
// System.String[] Room::get_propertiesListedInLobby()
extern "C" StringU5BU5D_t1_202* Room_get_propertiesListedInLobby_m8_817 (Room_t8_100 * __this, const MethodInfo* method)
{
	{
		StringU5BU5D_t1_202* L_0 = (__this->___U3CpropertiesListedInLobbyU3Ek__BackingField_11);
		return L_0;
	}
}
// System.Void Room::set_propertiesListedInLobby(System.String[])
extern "C" void Room_set_propertiesListedInLobby_m8_818 (Room_t8_100 * __this, StringU5BU5D_t1_202* ___value, const MethodInfo* method)
{
	{
		StringU5BU5D_t1_202* L_0 = ___value;
		__this->___U3CpropertiesListedInLobbyU3Ek__BackingField_11 = L_0;
		return;
	}
}
// System.Boolean Room::get_autoCleanUp()
extern "C" bool Room_get_autoCleanUp_m8_819 (Room_t8_100 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (((RoomInfo_t8_124 *)__this)->___autoCleanUpField_4);
		return L_0;
	}
}
// System.Int32 Room::get_masterClientId()
extern "C" int32_t Room_get_masterClientId_m8_820 (Room_t8_100 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (((RoomInfo_t8_124 *)__this)->___masterClientIdField_6);
		return L_0;
	}
}
// System.Void Room::set_masterClientId(System.Int32)
extern "C" void Room_set_masterClientId_m8_821 (Room_t8_100 * __this, int32_t ___value, const MethodInfo* method)
{
	{
		int32_t L_0 = ___value;
		((RoomInfo_t8_124 *)__this)->___masterClientIdField_6 = L_0;
		return;
	}
}
// System.Void Room::SetCustomProperties(ExitGames.Client.Photon.Hashtable,ExitGames.Client.Photon.Hashtable,System.Boolean)
extern TypeInfo* PhotonNetwork_t8_114_il2cpp_TypeInfo_var;
extern TypeInfo* ObjectU5BU5D_t1_157_il2cpp_TypeInfo_var;
extern TypeInfo* NetworkingPeer_t8_98_il2cpp_TypeInfo_var;
extern "C" void Room_SetCustomProperties_m8_822 (Room_t8_100 * __this, Hashtable_t5_1 * ___propertiesToSet, Hashtable_t5_1 * ___expectedValues, bool ___webForward, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		PhotonNetwork_t8_114_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1058);
		ObjectU5BU5D_t1_157_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(19);
		NetworkingPeer_t8_98_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1121);
		s_Il2CppMethodIntialized = true;
	}
	Hashtable_t5_1 * V_0 = {0};
	Hashtable_t5_1 * V_1 = {0};
	bool V_2 = false;
	int32_t G_B5_0 = 0;
	{
		Hashtable_t5_1 * L_0 = ___propertiesToSet;
		if (L_0)
		{
			goto IL_0007;
		}
	}
	{
		return;
	}

IL_0007:
	{
		Hashtable_t5_1 * L_1 = ___propertiesToSet;
		Hashtable_t5_1 * L_2 = Extensions_StripToStringKeys_m8_300(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		Hashtable_t5_1 * L_3 = ___expectedValues;
		Hashtable_t5_1 * L_4 = Extensions_StripToStringKeys_m8_300(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		V_1 = L_4;
		Hashtable_t5_1 * L_5 = V_1;
		if (!L_5)
		{
			goto IL_0026;
		}
	}
	{
		Hashtable_t5_1 * L_6 = V_1;
		NullCheck(L_6);
		int32_t L_7 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(9 /* System.Int32 System.Collections.Generic.Dictionary`2<System.Object,System.Object>::get_Count() */, L_6);
		G_B5_0 = ((((int32_t)L_7) == ((int32_t)0))? 1 : 0);
		goto IL_0027;
	}

IL_0026:
	{
		G_B5_0 = 1;
	}

IL_0027:
	{
		V_2 = G_B5_0;
		IL2CPP_RUNTIME_CLASS_INIT(PhotonNetwork_t8_114_il2cpp_TypeInfo_var);
		bool L_8 = PhotonNetwork_get_offlineMode_m8_623(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (L_8)
		{
			goto IL_0040;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(PhotonNetwork_t8_114_il2cpp_TypeInfo_var);
		NetworkingPeer_t8_98 * L_9 = ((PhotonNetwork_t8_114_StaticFields*)PhotonNetwork_t8_114_il2cpp_TypeInfo_var->static_fields)->___networkingPeer_4;
		Hashtable_t5_1 * L_10 = V_0;
		Hashtable_t5_1 * L_11 = V_1;
		bool L_12 = ___webForward;
		NullCheck(L_9);
		LoadbalancingPeer_OpSetPropertiesOfRoom_m8_332(L_9, L_10, L_11, L_12, /*hidden argument*/NULL);
	}

IL_0040:
	{
		IL2CPP_RUNTIME_CLASS_INIT(PhotonNetwork_t8_114_il2cpp_TypeInfo_var);
		bool L_13 = PhotonNetwork_get_offlineMode_m8_623(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (L_13)
		{
			goto IL_0050;
		}
	}
	{
		bool L_14 = V_2;
		if (!L_14)
		{
			goto IL_0068;
		}
	}

IL_0050:
	{
		Hashtable_t5_1 * L_15 = V_0;
		RoomInfo_InternalCacheProperties_m8_844(__this, L_15, /*hidden argument*/NULL);
		ObjectU5BU5D_t1_157* L_16 = ((ObjectU5BU5D_t1_157*)SZArrayNew(ObjectU5BU5D_t1_157_il2cpp_TypeInfo_var, 1));
		Hashtable_t5_1 * L_17 = V_0;
		NullCheck(L_16);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_16, 0);
		ArrayElementTypeCheck (L_16, L_17);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_16, 0, sizeof(Object_t *))) = (Object_t *)L_17;
		IL2CPP_RUNTIME_CLASS_INIT(NetworkingPeer_t8_98_il2cpp_TypeInfo_var);
		NetworkingPeer_SendMonoMessage_m8_446(NULL /*static, unused*/, ((int32_t)20), L_16, /*hidden argument*/NULL);
	}

IL_0068:
	{
		return;
	}
}
// System.Void Room::SetPropertiesListedInLobby(System.String[])
extern TypeInfo* Hashtable_t5_1_il2cpp_TypeInfo_var;
extern TypeInfo* Byte_t1_11_il2cpp_TypeInfo_var;
extern TypeInfo* PhotonNetwork_t8_114_il2cpp_TypeInfo_var;
extern "C" void Room_SetPropertiesListedInLobby_m8_823 (Room_t8_100 * __this, StringU5BU5D_t1_202* ___propsListedInLobby, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Hashtable_t5_1_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(891);
		Byte_t1_11_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(22);
		PhotonNetwork_t8_114_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1058);
		s_Il2CppMethodIntialized = true;
	}
	Hashtable_t5_1 * V_0 = {0};
	{
		Hashtable_t5_1 * L_0 = (Hashtable_t5_1 *)il2cpp_codegen_object_new (Hashtable_t5_1_il2cpp_TypeInfo_var);
		Hashtable__ctor_m5_0(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		Hashtable_t5_1 * L_1 = V_0;
		uint8_t L_2 = ((int32_t)250);
		Object_t * L_3 = Box(Byte_t1_11_il2cpp_TypeInfo_var, &L_2);
		StringU5BU5D_t1_202* L_4 = ___propsListedInLobby;
		NullCheck(L_1);
		Hashtable_set_Item_m5_3(L_1, L_3, (Object_t *)(Object_t *)L_4, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(PhotonNetwork_t8_114_il2cpp_TypeInfo_var);
		NetworkingPeer_t8_98 * L_5 = ((PhotonNetwork_t8_114_StaticFields*)PhotonNetwork_t8_114_il2cpp_TypeInfo_var->static_fields)->___networkingPeer_4;
		Hashtable_t5_1 * L_6 = V_0;
		NullCheck(L_5);
		LoadbalancingPeer_OpSetPropertiesOfRoom_m8_332(L_5, L_6, (Hashtable_t5_1 *)NULL, 0, /*hidden argument*/NULL);
		StringU5BU5D_t1_202* L_7 = ___propsListedInLobby;
		Room_set_propertiesListedInLobby_m8_818(__this, L_7, /*hidden argument*/NULL);
		return;
	}
}
// System.String Room::ToString()
extern TypeInfo* ObjectU5BU5D_t1_157_il2cpp_TypeInfo_var;
extern TypeInfo* Byte_t1_11_il2cpp_TypeInfo_var;
extern TypeInfo* Int32_t1_3_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3504;
extern Il2CppCodeGenString* _stringLiteral3505;
extern Il2CppCodeGenString* _stringLiteral3506;
extern Il2CppCodeGenString* _stringLiteral3507;
extern Il2CppCodeGenString* _stringLiteral3508;
extern "C" String_t* Room_ToString_m8_824 (Room_t8_100 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ObjectU5BU5D_t1_157_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(19);
		Byte_t1_11_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(22);
		Int32_t1_3_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(8);
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(11);
		_stringLiteral3504 = il2cpp_codegen_string_literal_from_index(3504);
		_stringLiteral3505 = il2cpp_codegen_string_literal_from_index(3505);
		_stringLiteral3506 = il2cpp_codegen_string_literal_from_index(3506);
		_stringLiteral3507 = il2cpp_codegen_string_literal_from_index(3507);
		_stringLiteral3508 = il2cpp_codegen_string_literal_from_index(3508);
		s_Il2CppMethodIntialized = true;
	}
	int32_t G_B2_0 = 0;
	ObjectU5BU5D_t1_157* G_B2_1 = {0};
	ObjectU5BU5D_t1_157* G_B2_2 = {0};
	String_t* G_B2_3 = {0};
	int32_t G_B1_0 = 0;
	ObjectU5BU5D_t1_157* G_B1_1 = {0};
	ObjectU5BU5D_t1_157* G_B1_2 = {0};
	String_t* G_B1_3 = {0};
	String_t* G_B3_0 = {0};
	int32_t G_B3_1 = 0;
	ObjectU5BU5D_t1_157* G_B3_2 = {0};
	ObjectU5BU5D_t1_157* G_B3_3 = {0};
	String_t* G_B3_4 = {0};
	int32_t G_B5_0 = 0;
	ObjectU5BU5D_t1_157* G_B5_1 = {0};
	ObjectU5BU5D_t1_157* G_B5_2 = {0};
	String_t* G_B5_3 = {0};
	int32_t G_B4_0 = 0;
	ObjectU5BU5D_t1_157* G_B4_1 = {0};
	ObjectU5BU5D_t1_157* G_B4_2 = {0};
	String_t* G_B4_3 = {0};
	String_t* G_B6_0 = {0};
	int32_t G_B6_1 = 0;
	ObjectU5BU5D_t1_157* G_B6_2 = {0};
	ObjectU5BU5D_t1_157* G_B6_3 = {0};
	String_t* G_B6_4 = {0};
	{
		ObjectU5BU5D_t1_157* L_0 = ((ObjectU5BU5D_t1_157*)SZArrayNew(ObjectU5BU5D_t1_157_il2cpp_TypeInfo_var, 5));
		String_t* L_1 = (((RoomInfo_t8_124 *)__this)->___nameField_5);
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, 0);
		ArrayElementTypeCheck (L_0, L_1);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_0, 0, sizeof(Object_t *))) = (Object_t *)L_1;
		ObjectU5BU5D_t1_157* L_2 = L_0;
		bool L_3 = (((RoomInfo_t8_124 *)__this)->___visibleField_3);
		G_B1_0 = 1;
		G_B1_1 = L_2;
		G_B1_2 = L_2;
		G_B1_3 = _stringLiteral3504;
		if (!L_3)
		{
			G_B2_0 = 1;
			G_B2_1 = L_2;
			G_B2_2 = L_2;
			G_B2_3 = _stringLiteral3504;
			goto IL_002b;
		}
	}
	{
		G_B3_0 = _stringLiteral3505;
		G_B3_1 = G_B1_0;
		G_B3_2 = G_B1_1;
		G_B3_3 = G_B1_2;
		G_B3_4 = G_B1_3;
		goto IL_0030;
	}

IL_002b:
	{
		G_B3_0 = _stringLiteral3506;
		G_B3_1 = G_B2_0;
		G_B3_2 = G_B2_1;
		G_B3_3 = G_B2_2;
		G_B3_4 = G_B2_3;
	}

IL_0030:
	{
		NullCheck(G_B3_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(G_B3_2, G_B3_1);
		ArrayElementTypeCheck (G_B3_2, G_B3_0);
		*((Object_t **)(Object_t **)SZArrayLdElema(G_B3_2, G_B3_1, sizeof(Object_t *))) = (Object_t *)G_B3_0;
		ObjectU5BU5D_t1_157* L_4 = G_B3_3;
		bool L_5 = (((RoomInfo_t8_124 *)__this)->___openField_2);
		G_B4_0 = 2;
		G_B4_1 = L_4;
		G_B4_2 = L_4;
		G_B4_3 = G_B3_4;
		if (!L_5)
		{
			G_B5_0 = 2;
			G_B5_1 = L_4;
			G_B5_2 = L_4;
			G_B5_3 = G_B3_4;
			goto IL_0048;
		}
	}
	{
		G_B6_0 = _stringLiteral3507;
		G_B6_1 = G_B4_0;
		G_B6_2 = G_B4_1;
		G_B6_3 = G_B4_2;
		G_B6_4 = G_B4_3;
		goto IL_004d;
	}

IL_0048:
	{
		G_B6_0 = _stringLiteral3508;
		G_B6_1 = G_B5_0;
		G_B6_2 = G_B5_1;
		G_B6_3 = G_B5_2;
		G_B6_4 = G_B5_3;
	}

IL_004d:
	{
		NullCheck(G_B6_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(G_B6_2, G_B6_1);
		ArrayElementTypeCheck (G_B6_2, G_B6_0);
		*((Object_t **)(Object_t **)SZArrayLdElema(G_B6_2, G_B6_1, sizeof(Object_t *))) = (Object_t *)G_B6_0;
		ObjectU5BU5D_t1_157* L_6 = G_B6_3;
		uint8_t L_7 = (((RoomInfo_t8_124 *)__this)->___maxPlayersField_1);
		uint8_t L_8 = L_7;
		Object_t * L_9 = Box(Byte_t1_11_il2cpp_TypeInfo_var, &L_8);
		NullCheck(L_6);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_6, 3);
		ArrayElementTypeCheck (L_6, L_9);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_6, 3, sizeof(Object_t *))) = (Object_t *)L_9;
		ObjectU5BU5D_t1_157* L_10 = L_6;
		int32_t L_11 = Room_get_playerCount_m8_808(__this, /*hidden argument*/NULL);
		int32_t L_12 = L_11;
		Object_t * L_13 = Box(Int32_t1_3_il2cpp_TypeInfo_var, &L_12);
		NullCheck(L_10);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_10, 4);
		ArrayElementTypeCheck (L_10, L_13);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_10, 4, sizeof(Object_t *))) = (Object_t *)L_13;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_14 = String_Format_m1_413(NULL /*static, unused*/, G_B6_4, L_10, /*hidden argument*/NULL);
		return L_14;
	}
}
// System.String Room::ToStringFull()
extern TypeInfo* ObjectU5BU5D_t1_157_il2cpp_TypeInfo_var;
extern TypeInfo* Byte_t1_11_il2cpp_TypeInfo_var;
extern TypeInfo* Int32_t1_3_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3509;
extern Il2CppCodeGenString* _stringLiteral3505;
extern Il2CppCodeGenString* _stringLiteral3506;
extern Il2CppCodeGenString* _stringLiteral3507;
extern Il2CppCodeGenString* _stringLiteral3508;
extern "C" String_t* Room_ToStringFull_m8_825 (Room_t8_100 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ObjectU5BU5D_t1_157_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(19);
		Byte_t1_11_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(22);
		Int32_t1_3_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(8);
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(11);
		_stringLiteral3509 = il2cpp_codegen_string_literal_from_index(3509);
		_stringLiteral3505 = il2cpp_codegen_string_literal_from_index(3505);
		_stringLiteral3506 = il2cpp_codegen_string_literal_from_index(3506);
		_stringLiteral3507 = il2cpp_codegen_string_literal_from_index(3507);
		_stringLiteral3508 = il2cpp_codegen_string_literal_from_index(3508);
		s_Il2CppMethodIntialized = true;
	}
	int32_t G_B2_0 = 0;
	ObjectU5BU5D_t1_157* G_B2_1 = {0};
	ObjectU5BU5D_t1_157* G_B2_2 = {0};
	String_t* G_B2_3 = {0};
	int32_t G_B1_0 = 0;
	ObjectU5BU5D_t1_157* G_B1_1 = {0};
	ObjectU5BU5D_t1_157* G_B1_2 = {0};
	String_t* G_B1_3 = {0};
	String_t* G_B3_0 = {0};
	int32_t G_B3_1 = 0;
	ObjectU5BU5D_t1_157* G_B3_2 = {0};
	ObjectU5BU5D_t1_157* G_B3_3 = {0};
	String_t* G_B3_4 = {0};
	int32_t G_B5_0 = 0;
	ObjectU5BU5D_t1_157* G_B5_1 = {0};
	ObjectU5BU5D_t1_157* G_B5_2 = {0};
	String_t* G_B5_3 = {0};
	int32_t G_B4_0 = 0;
	ObjectU5BU5D_t1_157* G_B4_1 = {0};
	ObjectU5BU5D_t1_157* G_B4_2 = {0};
	String_t* G_B4_3 = {0};
	String_t* G_B6_0 = {0};
	int32_t G_B6_1 = 0;
	ObjectU5BU5D_t1_157* G_B6_2 = {0};
	ObjectU5BU5D_t1_157* G_B6_3 = {0};
	String_t* G_B6_4 = {0};
	{
		ObjectU5BU5D_t1_157* L_0 = ((ObjectU5BU5D_t1_157*)SZArrayNew(ObjectU5BU5D_t1_157_il2cpp_TypeInfo_var, 6));
		String_t* L_1 = (((RoomInfo_t8_124 *)__this)->___nameField_5);
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, 0);
		ArrayElementTypeCheck (L_0, L_1);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_0, 0, sizeof(Object_t *))) = (Object_t *)L_1;
		ObjectU5BU5D_t1_157* L_2 = L_0;
		bool L_3 = (((RoomInfo_t8_124 *)__this)->___visibleField_3);
		G_B1_0 = 1;
		G_B1_1 = L_2;
		G_B1_2 = L_2;
		G_B1_3 = _stringLiteral3509;
		if (!L_3)
		{
			G_B2_0 = 1;
			G_B2_1 = L_2;
			G_B2_2 = L_2;
			G_B2_3 = _stringLiteral3509;
			goto IL_002b;
		}
	}
	{
		G_B3_0 = _stringLiteral3505;
		G_B3_1 = G_B1_0;
		G_B3_2 = G_B1_1;
		G_B3_3 = G_B1_2;
		G_B3_4 = G_B1_3;
		goto IL_0030;
	}

IL_002b:
	{
		G_B3_0 = _stringLiteral3506;
		G_B3_1 = G_B2_0;
		G_B3_2 = G_B2_1;
		G_B3_3 = G_B2_2;
		G_B3_4 = G_B2_3;
	}

IL_0030:
	{
		NullCheck(G_B3_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(G_B3_2, G_B3_1);
		ArrayElementTypeCheck (G_B3_2, G_B3_0);
		*((Object_t **)(Object_t **)SZArrayLdElema(G_B3_2, G_B3_1, sizeof(Object_t *))) = (Object_t *)G_B3_0;
		ObjectU5BU5D_t1_157* L_4 = G_B3_3;
		bool L_5 = (((RoomInfo_t8_124 *)__this)->___openField_2);
		G_B4_0 = 2;
		G_B4_1 = L_4;
		G_B4_2 = L_4;
		G_B4_3 = G_B3_4;
		if (!L_5)
		{
			G_B5_0 = 2;
			G_B5_1 = L_4;
			G_B5_2 = L_4;
			G_B5_3 = G_B3_4;
			goto IL_0048;
		}
	}
	{
		G_B6_0 = _stringLiteral3507;
		G_B6_1 = G_B4_0;
		G_B6_2 = G_B4_1;
		G_B6_3 = G_B4_2;
		G_B6_4 = G_B4_3;
		goto IL_004d;
	}

IL_0048:
	{
		G_B6_0 = _stringLiteral3508;
		G_B6_1 = G_B5_0;
		G_B6_2 = G_B5_1;
		G_B6_3 = G_B5_2;
		G_B6_4 = G_B5_3;
	}

IL_004d:
	{
		NullCheck(G_B6_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(G_B6_2, G_B6_1);
		ArrayElementTypeCheck (G_B6_2, G_B6_0);
		*((Object_t **)(Object_t **)SZArrayLdElema(G_B6_2, G_B6_1, sizeof(Object_t *))) = (Object_t *)G_B6_0;
		ObjectU5BU5D_t1_157* L_6 = G_B6_3;
		uint8_t L_7 = (((RoomInfo_t8_124 *)__this)->___maxPlayersField_1);
		uint8_t L_8 = L_7;
		Object_t * L_9 = Box(Byte_t1_11_il2cpp_TypeInfo_var, &L_8);
		NullCheck(L_6);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_6, 3);
		ArrayElementTypeCheck (L_6, L_9);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_6, 3, sizeof(Object_t *))) = (Object_t *)L_9;
		ObjectU5BU5D_t1_157* L_10 = L_6;
		int32_t L_11 = Room_get_playerCount_m8_808(__this, /*hidden argument*/NULL);
		int32_t L_12 = L_11;
		Object_t * L_13 = Box(Int32_t1_3_il2cpp_TypeInfo_var, &L_12);
		NullCheck(L_10);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_10, 4);
		ArrayElementTypeCheck (L_10, L_13);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_10, 4, sizeof(Object_t *))) = (Object_t *)L_13;
		ObjectU5BU5D_t1_157* L_14 = L_10;
		Hashtable_t5_1 * L_15 = RoomInfo_get_customProperties_m8_831(__this, /*hidden argument*/NULL);
		String_t* L_16 = Extensions_ToStringFull_m8_299(NULL /*static, unused*/, L_15, /*hidden argument*/NULL);
		NullCheck(L_14);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_14, 5);
		ArrayElementTypeCheck (L_14, L_16);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_14, 5, sizeof(Object_t *))) = (Object_t *)L_16;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_17 = String_Format_m1_413(NULL /*static, unused*/, G_B6_4, L_14, /*hidden argument*/NULL);
		return L_17;
	}
}
// System.Void RoomInfo::.ctor(System.String,ExitGames.Client.Photon.Hashtable)
extern TypeInfo* Hashtable_t5_1_il2cpp_TypeInfo_var;
extern TypeInfo* PhotonNetwork_t8_114_il2cpp_TypeInfo_var;
extern "C" void RoomInfo__ctor_m8_826 (RoomInfo_t8_124 * __this, String_t* ___roomName, Hashtable_t5_1 * ___properties, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Hashtable_t5_1_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(891);
		PhotonNetwork_t8_114_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1058);
		s_Il2CppMethodIntialized = true;
	}
	{
		Hashtable_t5_1 * L_0 = (Hashtable_t5_1 *)il2cpp_codegen_object_new (Hashtable_t5_1_il2cpp_TypeInfo_var);
		Hashtable__ctor_m5_0(L_0, /*hidden argument*/NULL);
		__this->___customPropertiesField_0 = L_0;
		__this->___openField_2 = 1;
		__this->___visibleField_3 = 1;
		IL2CPP_RUNTIME_CLASS_INIT(PhotonNetwork_t8_114_il2cpp_TypeInfo_var);
		bool L_1 = PhotonNetwork_get_autoCleanUpPlayerObjects_m8_627(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->___autoCleanUpField_4 = L_1;
		Object__ctor_m1_0(__this, /*hidden argument*/NULL);
		Hashtable_t5_1 * L_2 = ___properties;
		RoomInfo_InternalCacheProperties_m8_844(__this, L_2, /*hidden argument*/NULL);
		String_t* L_3 = ___roomName;
		__this->___nameField_5 = L_3;
		return;
	}
}
// System.Boolean RoomInfo::get_removedFromList()
extern "C" bool RoomInfo_get_removedFromList_m8_827 (RoomInfo_t8_124 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (__this->___U3CremovedFromListU3Ek__BackingField_7);
		return L_0;
	}
}
// System.Void RoomInfo::set_removedFromList(System.Boolean)
extern "C" void RoomInfo_set_removedFromList_m8_828 (RoomInfo_t8_124 * __this, bool ___value, const MethodInfo* method)
{
	{
		bool L_0 = ___value;
		__this->___U3CremovedFromListU3Ek__BackingField_7 = L_0;
		return;
	}
}
// System.Boolean RoomInfo::get_serverSideMasterClient()
extern "C" bool RoomInfo_get_serverSideMasterClient_m8_829 (RoomInfo_t8_124 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (__this->___U3CserverSideMasterClientU3Ek__BackingField_8);
		return L_0;
	}
}
// System.Void RoomInfo::set_serverSideMasterClient(System.Boolean)
extern "C" void RoomInfo_set_serverSideMasterClient_m8_830 (RoomInfo_t8_124 * __this, bool ___value, const MethodInfo* method)
{
	{
		bool L_0 = ___value;
		__this->___U3CserverSideMasterClientU3Ek__BackingField_8 = L_0;
		return;
	}
}
// ExitGames.Client.Photon.Hashtable RoomInfo::get_customProperties()
extern "C" Hashtable_t5_1 * RoomInfo_get_customProperties_m8_831 (RoomInfo_t8_124 * __this, const MethodInfo* method)
{
	{
		Hashtable_t5_1 * L_0 = (__this->___customPropertiesField_0);
		return L_0;
	}
}
// System.String RoomInfo::get_name()
extern "C" String_t* RoomInfo_get_name_m8_832 (RoomInfo_t8_124 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = (__this->___nameField_5);
		return L_0;
	}
}
// System.Int32 RoomInfo::get_playerCount()
extern "C" int32_t RoomInfo_get_playerCount_m8_833 (RoomInfo_t8_124 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (__this->___U3CplayerCountU3Ek__BackingField_9);
		return L_0;
	}
}
// System.Void RoomInfo::set_playerCount(System.Int32)
extern "C" void RoomInfo_set_playerCount_m8_834 (RoomInfo_t8_124 * __this, int32_t ___value, const MethodInfo* method)
{
	{
		int32_t L_0 = ___value;
		__this->___U3CplayerCountU3Ek__BackingField_9 = L_0;
		return;
	}
}
// System.Boolean RoomInfo::get_isLocalClientInside()
extern "C" bool RoomInfo_get_isLocalClientInside_m8_835 (RoomInfo_t8_124 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (__this->___U3CisLocalClientInsideU3Ek__BackingField_10);
		return L_0;
	}
}
// System.Void RoomInfo::set_isLocalClientInside(System.Boolean)
extern "C" void RoomInfo_set_isLocalClientInside_m8_836 (RoomInfo_t8_124 * __this, bool ___value, const MethodInfo* method)
{
	{
		bool L_0 = ___value;
		__this->___U3CisLocalClientInsideU3Ek__BackingField_10 = L_0;
		return;
	}
}
// System.Byte RoomInfo::get_maxPlayers()
extern "C" uint8_t RoomInfo_get_maxPlayers_m8_837 (RoomInfo_t8_124 * __this, const MethodInfo* method)
{
	{
		uint8_t L_0 = (__this->___maxPlayersField_1);
		return L_0;
	}
}
// System.Boolean RoomInfo::get_open()
extern "C" bool RoomInfo_get_open_m8_838 (RoomInfo_t8_124 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (__this->___openField_2);
		return L_0;
	}
}
// System.Boolean RoomInfo::get_visible()
extern "C" bool RoomInfo_get_visible_m8_839 (RoomInfo_t8_124 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (__this->___visibleField_3);
		return L_0;
	}
}
// System.Boolean RoomInfo::Equals(System.Object)
extern TypeInfo* Room_t8_100_il2cpp_TypeInfo_var;
extern "C" bool RoomInfo_Equals_m8_840 (RoomInfo_t8_124 * __this, Object_t * ___p, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Room_t8_100_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1130);
		s_Il2CppMethodIntialized = true;
	}
	Room_t8_100 * V_0 = {0};
	int32_t G_B3_0 = 0;
	{
		Object_t * L_0 = ___p;
		V_0 = ((Room_t8_100 *)IsInstClass(L_0, Room_t8_100_il2cpp_TypeInfo_var));
		Room_t8_100 * L_1 = V_0;
		if (!L_1)
		{
			goto IL_0020;
		}
	}
	{
		String_t* L_2 = (__this->___nameField_5);
		Room_t8_100 * L_3 = V_0;
		NullCheck(L_3);
		String_t* L_4 = (((RoomInfo_t8_124 *)L_3)->___nameField_5);
		NullCheck(L_2);
		bool L_5 = String_Equals_m1_340(L_2, L_4, /*hidden argument*/NULL);
		G_B3_0 = ((int32_t)(L_5));
		goto IL_0021;
	}

IL_0020:
	{
		G_B3_0 = 0;
	}

IL_0021:
	{
		return G_B3_0;
	}
}
// System.Int32 RoomInfo::GetHashCode()
extern "C" int32_t RoomInfo_GetHashCode_m8_841 (RoomInfo_t8_124 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = (__this->___nameField_5);
		NullCheck(L_0);
		int32_t L_1 = String_GetHashCode_m1_433(L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.String RoomInfo::ToString()
extern TypeInfo* ObjectU5BU5D_t1_157_il2cpp_TypeInfo_var;
extern TypeInfo* Byte_t1_11_il2cpp_TypeInfo_var;
extern TypeInfo* Int32_t1_3_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3504;
extern Il2CppCodeGenString* _stringLiteral3505;
extern Il2CppCodeGenString* _stringLiteral3506;
extern Il2CppCodeGenString* _stringLiteral3507;
extern Il2CppCodeGenString* _stringLiteral3508;
extern "C" String_t* RoomInfo_ToString_m8_842 (RoomInfo_t8_124 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ObjectU5BU5D_t1_157_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(19);
		Byte_t1_11_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(22);
		Int32_t1_3_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(8);
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(11);
		_stringLiteral3504 = il2cpp_codegen_string_literal_from_index(3504);
		_stringLiteral3505 = il2cpp_codegen_string_literal_from_index(3505);
		_stringLiteral3506 = il2cpp_codegen_string_literal_from_index(3506);
		_stringLiteral3507 = il2cpp_codegen_string_literal_from_index(3507);
		_stringLiteral3508 = il2cpp_codegen_string_literal_from_index(3508);
		s_Il2CppMethodIntialized = true;
	}
	int32_t G_B2_0 = 0;
	ObjectU5BU5D_t1_157* G_B2_1 = {0};
	ObjectU5BU5D_t1_157* G_B2_2 = {0};
	String_t* G_B2_3 = {0};
	int32_t G_B1_0 = 0;
	ObjectU5BU5D_t1_157* G_B1_1 = {0};
	ObjectU5BU5D_t1_157* G_B1_2 = {0};
	String_t* G_B1_3 = {0};
	String_t* G_B3_0 = {0};
	int32_t G_B3_1 = 0;
	ObjectU5BU5D_t1_157* G_B3_2 = {0};
	ObjectU5BU5D_t1_157* G_B3_3 = {0};
	String_t* G_B3_4 = {0};
	int32_t G_B5_0 = 0;
	ObjectU5BU5D_t1_157* G_B5_1 = {0};
	ObjectU5BU5D_t1_157* G_B5_2 = {0};
	String_t* G_B5_3 = {0};
	int32_t G_B4_0 = 0;
	ObjectU5BU5D_t1_157* G_B4_1 = {0};
	ObjectU5BU5D_t1_157* G_B4_2 = {0};
	String_t* G_B4_3 = {0};
	String_t* G_B6_0 = {0};
	int32_t G_B6_1 = 0;
	ObjectU5BU5D_t1_157* G_B6_2 = {0};
	ObjectU5BU5D_t1_157* G_B6_3 = {0};
	String_t* G_B6_4 = {0};
	{
		ObjectU5BU5D_t1_157* L_0 = ((ObjectU5BU5D_t1_157*)SZArrayNew(ObjectU5BU5D_t1_157_il2cpp_TypeInfo_var, 5));
		String_t* L_1 = (__this->___nameField_5);
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, 0);
		ArrayElementTypeCheck (L_0, L_1);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_0, 0, sizeof(Object_t *))) = (Object_t *)L_1;
		ObjectU5BU5D_t1_157* L_2 = L_0;
		bool L_3 = (__this->___visibleField_3);
		G_B1_0 = 1;
		G_B1_1 = L_2;
		G_B1_2 = L_2;
		G_B1_3 = _stringLiteral3504;
		if (!L_3)
		{
			G_B2_0 = 1;
			G_B2_1 = L_2;
			G_B2_2 = L_2;
			G_B2_3 = _stringLiteral3504;
			goto IL_002b;
		}
	}
	{
		G_B3_0 = _stringLiteral3505;
		G_B3_1 = G_B1_0;
		G_B3_2 = G_B1_1;
		G_B3_3 = G_B1_2;
		G_B3_4 = G_B1_3;
		goto IL_0030;
	}

IL_002b:
	{
		G_B3_0 = _stringLiteral3506;
		G_B3_1 = G_B2_0;
		G_B3_2 = G_B2_1;
		G_B3_3 = G_B2_2;
		G_B3_4 = G_B2_3;
	}

IL_0030:
	{
		NullCheck(G_B3_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(G_B3_2, G_B3_1);
		ArrayElementTypeCheck (G_B3_2, G_B3_0);
		*((Object_t **)(Object_t **)SZArrayLdElema(G_B3_2, G_B3_1, sizeof(Object_t *))) = (Object_t *)G_B3_0;
		ObjectU5BU5D_t1_157* L_4 = G_B3_3;
		bool L_5 = (__this->___openField_2);
		G_B4_0 = 2;
		G_B4_1 = L_4;
		G_B4_2 = L_4;
		G_B4_3 = G_B3_4;
		if (!L_5)
		{
			G_B5_0 = 2;
			G_B5_1 = L_4;
			G_B5_2 = L_4;
			G_B5_3 = G_B3_4;
			goto IL_0048;
		}
	}
	{
		G_B6_0 = _stringLiteral3507;
		G_B6_1 = G_B4_0;
		G_B6_2 = G_B4_1;
		G_B6_3 = G_B4_2;
		G_B6_4 = G_B4_3;
		goto IL_004d;
	}

IL_0048:
	{
		G_B6_0 = _stringLiteral3508;
		G_B6_1 = G_B5_0;
		G_B6_2 = G_B5_1;
		G_B6_3 = G_B5_2;
		G_B6_4 = G_B5_3;
	}

IL_004d:
	{
		NullCheck(G_B6_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(G_B6_2, G_B6_1);
		ArrayElementTypeCheck (G_B6_2, G_B6_0);
		*((Object_t **)(Object_t **)SZArrayLdElema(G_B6_2, G_B6_1, sizeof(Object_t *))) = (Object_t *)G_B6_0;
		ObjectU5BU5D_t1_157* L_6 = G_B6_3;
		uint8_t L_7 = (__this->___maxPlayersField_1);
		uint8_t L_8 = L_7;
		Object_t * L_9 = Box(Byte_t1_11_il2cpp_TypeInfo_var, &L_8);
		NullCheck(L_6);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_6, 3);
		ArrayElementTypeCheck (L_6, L_9);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_6, 3, sizeof(Object_t *))) = (Object_t *)L_9;
		ObjectU5BU5D_t1_157* L_10 = L_6;
		int32_t L_11 = RoomInfo_get_playerCount_m8_833(__this, /*hidden argument*/NULL);
		int32_t L_12 = L_11;
		Object_t * L_13 = Box(Int32_t1_3_il2cpp_TypeInfo_var, &L_12);
		NullCheck(L_10);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_10, 4);
		ArrayElementTypeCheck (L_10, L_13);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_10, 4, sizeof(Object_t *))) = (Object_t *)L_13;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_14 = String_Format_m1_413(NULL /*static, unused*/, G_B6_4, L_10, /*hidden argument*/NULL);
		return L_14;
	}
}
// System.String RoomInfo::ToStringFull()
extern TypeInfo* ObjectU5BU5D_t1_157_il2cpp_TypeInfo_var;
extern TypeInfo* Byte_t1_11_il2cpp_TypeInfo_var;
extern TypeInfo* Int32_t1_3_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3509;
extern Il2CppCodeGenString* _stringLiteral3505;
extern Il2CppCodeGenString* _stringLiteral3506;
extern Il2CppCodeGenString* _stringLiteral3507;
extern Il2CppCodeGenString* _stringLiteral3508;
extern "C" String_t* RoomInfo_ToStringFull_m8_843 (RoomInfo_t8_124 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ObjectU5BU5D_t1_157_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(19);
		Byte_t1_11_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(22);
		Int32_t1_3_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(8);
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(11);
		_stringLiteral3509 = il2cpp_codegen_string_literal_from_index(3509);
		_stringLiteral3505 = il2cpp_codegen_string_literal_from_index(3505);
		_stringLiteral3506 = il2cpp_codegen_string_literal_from_index(3506);
		_stringLiteral3507 = il2cpp_codegen_string_literal_from_index(3507);
		_stringLiteral3508 = il2cpp_codegen_string_literal_from_index(3508);
		s_Il2CppMethodIntialized = true;
	}
	int32_t G_B2_0 = 0;
	ObjectU5BU5D_t1_157* G_B2_1 = {0};
	ObjectU5BU5D_t1_157* G_B2_2 = {0};
	String_t* G_B2_3 = {0};
	int32_t G_B1_0 = 0;
	ObjectU5BU5D_t1_157* G_B1_1 = {0};
	ObjectU5BU5D_t1_157* G_B1_2 = {0};
	String_t* G_B1_3 = {0};
	String_t* G_B3_0 = {0};
	int32_t G_B3_1 = 0;
	ObjectU5BU5D_t1_157* G_B3_2 = {0};
	ObjectU5BU5D_t1_157* G_B3_3 = {0};
	String_t* G_B3_4 = {0};
	int32_t G_B5_0 = 0;
	ObjectU5BU5D_t1_157* G_B5_1 = {0};
	ObjectU5BU5D_t1_157* G_B5_2 = {0};
	String_t* G_B5_3 = {0};
	int32_t G_B4_0 = 0;
	ObjectU5BU5D_t1_157* G_B4_1 = {0};
	ObjectU5BU5D_t1_157* G_B4_2 = {0};
	String_t* G_B4_3 = {0};
	String_t* G_B6_0 = {0};
	int32_t G_B6_1 = 0;
	ObjectU5BU5D_t1_157* G_B6_2 = {0};
	ObjectU5BU5D_t1_157* G_B6_3 = {0};
	String_t* G_B6_4 = {0};
	{
		ObjectU5BU5D_t1_157* L_0 = ((ObjectU5BU5D_t1_157*)SZArrayNew(ObjectU5BU5D_t1_157_il2cpp_TypeInfo_var, 6));
		String_t* L_1 = (__this->___nameField_5);
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, 0);
		ArrayElementTypeCheck (L_0, L_1);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_0, 0, sizeof(Object_t *))) = (Object_t *)L_1;
		ObjectU5BU5D_t1_157* L_2 = L_0;
		bool L_3 = (__this->___visibleField_3);
		G_B1_0 = 1;
		G_B1_1 = L_2;
		G_B1_2 = L_2;
		G_B1_3 = _stringLiteral3509;
		if (!L_3)
		{
			G_B2_0 = 1;
			G_B2_1 = L_2;
			G_B2_2 = L_2;
			G_B2_3 = _stringLiteral3509;
			goto IL_002b;
		}
	}
	{
		G_B3_0 = _stringLiteral3505;
		G_B3_1 = G_B1_0;
		G_B3_2 = G_B1_1;
		G_B3_3 = G_B1_2;
		G_B3_4 = G_B1_3;
		goto IL_0030;
	}

IL_002b:
	{
		G_B3_0 = _stringLiteral3506;
		G_B3_1 = G_B2_0;
		G_B3_2 = G_B2_1;
		G_B3_3 = G_B2_2;
		G_B3_4 = G_B2_3;
	}

IL_0030:
	{
		NullCheck(G_B3_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(G_B3_2, G_B3_1);
		ArrayElementTypeCheck (G_B3_2, G_B3_0);
		*((Object_t **)(Object_t **)SZArrayLdElema(G_B3_2, G_B3_1, sizeof(Object_t *))) = (Object_t *)G_B3_0;
		ObjectU5BU5D_t1_157* L_4 = G_B3_3;
		bool L_5 = (__this->___openField_2);
		G_B4_0 = 2;
		G_B4_1 = L_4;
		G_B4_2 = L_4;
		G_B4_3 = G_B3_4;
		if (!L_5)
		{
			G_B5_0 = 2;
			G_B5_1 = L_4;
			G_B5_2 = L_4;
			G_B5_3 = G_B3_4;
			goto IL_0048;
		}
	}
	{
		G_B6_0 = _stringLiteral3507;
		G_B6_1 = G_B4_0;
		G_B6_2 = G_B4_1;
		G_B6_3 = G_B4_2;
		G_B6_4 = G_B4_3;
		goto IL_004d;
	}

IL_0048:
	{
		G_B6_0 = _stringLiteral3508;
		G_B6_1 = G_B5_0;
		G_B6_2 = G_B5_1;
		G_B6_3 = G_B5_2;
		G_B6_4 = G_B5_3;
	}

IL_004d:
	{
		NullCheck(G_B6_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(G_B6_2, G_B6_1);
		ArrayElementTypeCheck (G_B6_2, G_B6_0);
		*((Object_t **)(Object_t **)SZArrayLdElema(G_B6_2, G_B6_1, sizeof(Object_t *))) = (Object_t *)G_B6_0;
		ObjectU5BU5D_t1_157* L_6 = G_B6_3;
		uint8_t L_7 = (__this->___maxPlayersField_1);
		uint8_t L_8 = L_7;
		Object_t * L_9 = Box(Byte_t1_11_il2cpp_TypeInfo_var, &L_8);
		NullCheck(L_6);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_6, 3);
		ArrayElementTypeCheck (L_6, L_9);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_6, 3, sizeof(Object_t *))) = (Object_t *)L_9;
		ObjectU5BU5D_t1_157* L_10 = L_6;
		int32_t L_11 = RoomInfo_get_playerCount_m8_833(__this, /*hidden argument*/NULL);
		int32_t L_12 = L_11;
		Object_t * L_13 = Box(Int32_t1_3_il2cpp_TypeInfo_var, &L_12);
		NullCheck(L_10);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_10, 4);
		ArrayElementTypeCheck (L_10, L_13);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_10, 4, sizeof(Object_t *))) = (Object_t *)L_13;
		ObjectU5BU5D_t1_157* L_14 = L_10;
		Hashtable_t5_1 * L_15 = (__this->___customPropertiesField_0);
		String_t* L_16 = Extensions_ToStringFull_m8_299(NULL /*static, unused*/, L_15, /*hidden argument*/NULL);
		NullCheck(L_14);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_14, 5);
		ArrayElementTypeCheck (L_14, L_16);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_14, 5, sizeof(Object_t *))) = (Object_t *)L_16;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_17 = String_Format_m1_413(NULL /*static, unused*/, G_B6_4, L_14, /*hidden argument*/NULL);
		return L_17;
	}
}
// System.Void RoomInfo::InternalCacheProperties(ExitGames.Client.Photon.Hashtable)
extern TypeInfo* Byte_t1_11_il2cpp_TypeInfo_var;
extern TypeInfo* Boolean_t1_20_il2cpp_TypeInfo_var;
extern TypeInfo* Int32_t1_3_il2cpp_TypeInfo_var;
extern TypeInfo* PhotonNetwork_t8_114_il2cpp_TypeInfo_var;
extern "C" void RoomInfo_InternalCacheProperties_m8_844 (RoomInfo_t8_124 * __this, Hashtable_t5_1 * ___propertiesToCache, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Byte_t1_11_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(22);
		Boolean_t1_20_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(49);
		Int32_t1_3_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(8);
		PhotonNetwork_t8_114_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1058);
		s_Il2CppMethodIntialized = true;
	}
	bool V_0 = false;
	{
		Hashtable_t5_1 * L_0 = ___propertiesToCache;
		if (!L_0)
		{
			goto IL_0022;
		}
	}
	{
		Hashtable_t5_1 * L_1 = ___propertiesToCache;
		NullCheck(L_1);
		int32_t L_2 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(9 /* System.Int32 System.Collections.Generic.Dictionary`2<System.Object,System.Object>::get_Count() */, L_1);
		if (!L_2)
		{
			goto IL_0022;
		}
	}
	{
		Hashtable_t5_1 * L_3 = (__this->___customPropertiesField_0);
		Hashtable_t5_1 * L_4 = ___propertiesToCache;
		NullCheck(L_3);
		bool L_5 = (bool)VirtFuncInvoker1< bool, Object_t * >::Invoke(0 /* System.Boolean System.Object::Equals(System.Object) */, L_3, L_4);
		if (!L_5)
		{
			goto IL_0023;
		}
	}

IL_0022:
	{
		return;
	}

IL_0023:
	{
		Hashtable_t5_1 * L_6 = ___propertiesToCache;
		uint8_t L_7 = ((int32_t)251);
		Object_t * L_8 = Box(Byte_t1_11_il2cpp_TypeInfo_var, &L_7);
		NullCheck(L_6);
		bool L_9 = (bool)VirtFuncInvoker1< bool, Object_t * >::Invoke(28 /* System.Boolean System.Collections.Generic.Dictionary`2<System.Object,System.Object>::ContainsKey(!0) */, L_6, L_8);
		if (!L_9)
		{
			goto IL_005f;
		}
	}
	{
		Hashtable_t5_1 * L_10 = ___propertiesToCache;
		uint8_t L_11 = ((int32_t)251);
		Object_t * L_12 = Box(Byte_t1_11_il2cpp_TypeInfo_var, &L_11);
		NullCheck(L_10);
		Object_t * L_13 = Hashtable_get_Item_m5_2(L_10, L_12, /*hidden argument*/NULL);
		RoomInfo_set_removedFromList_m8_828(__this, ((*(bool*)((bool*)UnBox (L_13, Boolean_t1_20_il2cpp_TypeInfo_var)))), /*hidden argument*/NULL);
		bool L_14 = RoomInfo_get_removedFromList_m8_827(__this, /*hidden argument*/NULL);
		if (!L_14)
		{
			goto IL_005f;
		}
	}
	{
		return;
	}

IL_005f:
	{
		Hashtable_t5_1 * L_15 = ___propertiesToCache;
		uint8_t L_16 = ((int32_t)255);
		Object_t * L_17 = Box(Byte_t1_11_il2cpp_TypeInfo_var, &L_16);
		NullCheck(L_15);
		bool L_18 = (bool)VirtFuncInvoker1< bool, Object_t * >::Invoke(28 /* System.Boolean System.Collections.Generic.Dictionary`2<System.Object,System.Object>::ContainsKey(!0) */, L_15, L_17);
		if (!L_18)
		{
			goto IL_008f;
		}
	}
	{
		Hashtable_t5_1 * L_19 = ___propertiesToCache;
		uint8_t L_20 = ((int32_t)255);
		Object_t * L_21 = Box(Byte_t1_11_il2cpp_TypeInfo_var, &L_20);
		NullCheck(L_19);
		Object_t * L_22 = Hashtable_get_Item_m5_2(L_19, L_21, /*hidden argument*/NULL);
		__this->___maxPlayersField_1 = ((*(uint8_t*)((uint8_t*)UnBox (L_22, Byte_t1_11_il2cpp_TypeInfo_var))));
	}

IL_008f:
	{
		Hashtable_t5_1 * L_23 = ___propertiesToCache;
		uint8_t L_24 = ((int32_t)253);
		Object_t * L_25 = Box(Byte_t1_11_il2cpp_TypeInfo_var, &L_24);
		NullCheck(L_23);
		bool L_26 = (bool)VirtFuncInvoker1< bool, Object_t * >::Invoke(28 /* System.Boolean System.Collections.Generic.Dictionary`2<System.Object,System.Object>::ContainsKey(!0) */, L_23, L_25);
		if (!L_26)
		{
			goto IL_00bf;
		}
	}
	{
		Hashtable_t5_1 * L_27 = ___propertiesToCache;
		uint8_t L_28 = ((int32_t)253);
		Object_t * L_29 = Box(Byte_t1_11_il2cpp_TypeInfo_var, &L_28);
		NullCheck(L_27);
		Object_t * L_30 = Hashtable_get_Item_m5_2(L_27, L_29, /*hidden argument*/NULL);
		__this->___openField_2 = ((*(bool*)((bool*)UnBox (L_30, Boolean_t1_20_il2cpp_TypeInfo_var))));
	}

IL_00bf:
	{
		Hashtable_t5_1 * L_31 = ___propertiesToCache;
		uint8_t L_32 = ((int32_t)254);
		Object_t * L_33 = Box(Byte_t1_11_il2cpp_TypeInfo_var, &L_32);
		NullCheck(L_31);
		bool L_34 = (bool)VirtFuncInvoker1< bool, Object_t * >::Invoke(28 /* System.Boolean System.Collections.Generic.Dictionary`2<System.Object,System.Object>::ContainsKey(!0) */, L_31, L_33);
		if (!L_34)
		{
			goto IL_00ef;
		}
	}
	{
		Hashtable_t5_1 * L_35 = ___propertiesToCache;
		uint8_t L_36 = ((int32_t)254);
		Object_t * L_37 = Box(Byte_t1_11_il2cpp_TypeInfo_var, &L_36);
		NullCheck(L_35);
		Object_t * L_38 = Hashtable_get_Item_m5_2(L_35, L_37, /*hidden argument*/NULL);
		__this->___visibleField_3 = ((*(bool*)((bool*)UnBox (L_38, Boolean_t1_20_il2cpp_TypeInfo_var))));
	}

IL_00ef:
	{
		Hashtable_t5_1 * L_39 = ___propertiesToCache;
		uint8_t L_40 = ((int32_t)252);
		Object_t * L_41 = Box(Byte_t1_11_il2cpp_TypeInfo_var, &L_40);
		NullCheck(L_39);
		bool L_42 = (bool)VirtFuncInvoker1< bool, Object_t * >::Invoke(28 /* System.Boolean System.Collections.Generic.Dictionary`2<System.Object,System.Object>::ContainsKey(!0) */, L_39, L_41);
		if (!L_42)
		{
			goto IL_011f;
		}
	}
	{
		Hashtable_t5_1 * L_43 = ___propertiesToCache;
		uint8_t L_44 = ((int32_t)252);
		Object_t * L_45 = Box(Byte_t1_11_il2cpp_TypeInfo_var, &L_44);
		NullCheck(L_43);
		Object_t * L_46 = Hashtable_get_Item_m5_2(L_43, L_45, /*hidden argument*/NULL);
		RoomInfo_set_playerCount_m8_834(__this, ((*(uint8_t*)((uint8_t*)UnBox (L_46, Byte_t1_11_il2cpp_TypeInfo_var)))), /*hidden argument*/NULL);
	}

IL_011f:
	{
		Hashtable_t5_1 * L_47 = ___propertiesToCache;
		uint8_t L_48 = ((int32_t)249);
		Object_t * L_49 = Box(Byte_t1_11_il2cpp_TypeInfo_var, &L_48);
		NullCheck(L_47);
		bool L_50 = (bool)VirtFuncInvoker1< bool, Object_t * >::Invoke(28 /* System.Boolean System.Collections.Generic.Dictionary`2<System.Object,System.Object>::ContainsKey(!0) */, L_47, L_49);
		if (!L_50)
		{
			goto IL_014f;
		}
	}
	{
		Hashtable_t5_1 * L_51 = ___propertiesToCache;
		uint8_t L_52 = ((int32_t)249);
		Object_t * L_53 = Box(Byte_t1_11_il2cpp_TypeInfo_var, &L_52);
		NullCheck(L_51);
		Object_t * L_54 = Hashtable_get_Item_m5_2(L_51, L_53, /*hidden argument*/NULL);
		__this->___autoCleanUpField_4 = ((*(bool*)((bool*)UnBox (L_54, Boolean_t1_20_il2cpp_TypeInfo_var))));
	}

IL_014f:
	{
		Hashtable_t5_1 * L_55 = ___propertiesToCache;
		uint8_t L_56 = ((int32_t)248);
		Object_t * L_57 = Box(Byte_t1_11_il2cpp_TypeInfo_var, &L_56);
		NullCheck(L_55);
		bool L_58 = (bool)VirtFuncInvoker1< bool, Object_t * >::Invoke(28 /* System.Boolean System.Collections.Generic.Dictionary`2<System.Object,System.Object>::ContainsKey(!0) */, L_55, L_57);
		if (!L_58)
		{
			goto IL_01a3;
		}
	}
	{
		RoomInfo_set_serverSideMasterClient_m8_830(__this, 1, /*hidden argument*/NULL);
		int32_t L_59 = (__this->___masterClientIdField_6);
		V_0 = ((((int32_t)((((int32_t)L_59) == ((int32_t)0))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		Hashtable_t5_1 * L_60 = ___propertiesToCache;
		uint8_t L_61 = ((int32_t)248);
		Object_t * L_62 = Box(Byte_t1_11_il2cpp_TypeInfo_var, &L_61);
		NullCheck(L_60);
		Object_t * L_63 = Hashtable_get_Item_m5_2(L_60, L_62, /*hidden argument*/NULL);
		__this->___masterClientIdField_6 = ((*(int32_t*)((int32_t*)UnBox (L_63, Int32_t1_3_il2cpp_TypeInfo_var))));
		bool L_64 = V_0;
		if (!L_64)
		{
			goto IL_01a3;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(PhotonNetwork_t8_114_il2cpp_TypeInfo_var);
		NetworkingPeer_t8_98 * L_65 = ((PhotonNetwork_t8_114_StaticFields*)PhotonNetwork_t8_114_il2cpp_TypeInfo_var->static_fields)->___networkingPeer_4;
		NullCheck(L_65);
		NetworkingPeer_UpdateMasterClient_m8_424(L_65, /*hidden argument*/NULL);
	}

IL_01a3:
	{
		Hashtable_t5_1 * L_66 = (__this->___customPropertiesField_0);
		Hashtable_t5_1 * L_67 = ___propertiesToCache;
		Extensions_MergeStringKeys_m8_298(NULL /*static, unused*/, L_66, L_67, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Region::.ctor()
extern "C" void Region__ctor_m8_845 (Region_t8_110 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1_0(__this, /*hidden argument*/NULL);
		return;
	}
}
// CloudRegionCode Region::Parse(System.String)
extern const Il2CppType* CloudRegionCode_t8_65_0_0_0_var;
extern TypeInfo* Type_t_il2cpp_TypeInfo_var;
extern TypeInfo* Enum_t1_24_il2cpp_TypeInfo_var;
extern TypeInfo* Int32_t1_3_il2cpp_TypeInfo_var;
extern "C" int32_t Region_Parse_m8_846 (Object_t * __this /* static, unused */, String_t* ___codeAsString, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CloudRegionCode_t8_65_0_0_0_var = il2cpp_codegen_type_from_index(1123);
		Type_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3);
		Enum_t1_24_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(61);
		Int32_t1_3_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(8);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = {0};
	{
		String_t* L_0 = ___codeAsString;
		NullCheck(L_0);
		String_t* L_1 = String_ToLower_m1_405(L_0, /*hidden argument*/NULL);
		___codeAsString = L_1;
		V_0 = 4;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_2 = Type_GetTypeFromHandle_m1_893(NULL /*static, unused*/, LoadTypeToken(CloudRegionCode_t8_65_0_0_0_var), /*hidden argument*/NULL);
		String_t* L_3 = ___codeAsString;
		IL2CPP_RUNTIME_CLASS_INIT(Enum_t1_24_il2cpp_TypeInfo_var);
		bool L_4 = Enum_IsDefined_m1_723(NULL /*static, unused*/, L_2, L_3, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_0035;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_5 = Type_GetTypeFromHandle_m1_893(NULL /*static, unused*/, LoadTypeToken(CloudRegionCode_t8_65_0_0_0_var), /*hidden argument*/NULL);
		String_t* L_6 = ___codeAsString;
		IL2CPP_RUNTIME_CLASS_INIT(Enum_t1_24_il2cpp_TypeInfo_var);
		Object_t * L_7 = Enum_Parse_m1_726(NULL /*static, unused*/, L_5, L_6, /*hidden argument*/NULL);
		V_0 = ((*(int32_t*)((int32_t*)UnBox (L_7, Int32_t1_3_il2cpp_TypeInfo_var))));
	}

IL_0035:
	{
		int32_t L_8 = V_0;
		return L_8;
	}
}
// CloudRegionFlag Region::ParseFlag(System.String)
extern const Il2CppType* CloudRegionFlag_t8_66_0_0_0_var;
extern TypeInfo* Type_t_il2cpp_TypeInfo_var;
extern TypeInfo* Enum_t1_24_il2cpp_TypeInfo_var;
extern TypeInfo* Int32_t1_3_il2cpp_TypeInfo_var;
extern "C" int32_t Region_ParseFlag_m8_847 (Object_t * __this /* static, unused */, String_t* ___codeAsString, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CloudRegionFlag_t8_66_0_0_0_var = il2cpp_codegen_type_from_index(1164);
		Type_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3);
		Enum_t1_24_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(61);
		Int32_t1_3_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(8);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = {0};
	{
		String_t* L_0 = ___codeAsString;
		NullCheck(L_0);
		String_t* L_1 = String_ToLower_m1_405(L_0, /*hidden argument*/NULL);
		___codeAsString = L_1;
		V_0 = 0;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_2 = Type_GetTypeFromHandle_m1_893(NULL /*static, unused*/, LoadTypeToken(CloudRegionFlag_t8_66_0_0_0_var), /*hidden argument*/NULL);
		String_t* L_3 = ___codeAsString;
		IL2CPP_RUNTIME_CLASS_INIT(Enum_t1_24_il2cpp_TypeInfo_var);
		bool L_4 = Enum_IsDefined_m1_723(NULL /*static, unused*/, L_2, L_3, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_0035;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_5 = Type_GetTypeFromHandle_m1_893(NULL /*static, unused*/, LoadTypeToken(CloudRegionFlag_t8_66_0_0_0_var), /*hidden argument*/NULL);
		String_t* L_6 = ___codeAsString;
		IL2CPP_RUNTIME_CLASS_INIT(Enum_t1_24_il2cpp_TypeInfo_var);
		Object_t * L_7 = Enum_Parse_m1_726(NULL /*static, unused*/, L_5, L_6, /*hidden argument*/NULL);
		V_0 = ((*(int32_t*)((int32_t*)UnBox (L_7, Int32_t1_3_il2cpp_TypeInfo_var))));
	}

IL_0035:
	{
		int32_t L_8 = V_0;
		return L_8;
	}
}
// System.String Region::ToString()
extern TypeInfo* CloudRegionCode_t8_65_il2cpp_TypeInfo_var;
extern TypeInfo* Int32_t1_3_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3510;
extern "C" String_t* Region_ToString_m8_848 (Region_t8_110 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CloudRegionCode_t8_65_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1123);
		Int32_t1_3_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(8);
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(11);
		_stringLiteral3510 = il2cpp_codegen_string_literal_from_index(3510);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = (__this->___Code_0);
		int32_t L_1 = L_0;
		Object_t * L_2 = Box(CloudRegionCode_t8_65_il2cpp_TypeInfo_var, &L_1);
		int32_t L_3 = (__this->___Ping_2);
		int32_t L_4 = L_3;
		Object_t * L_5 = Box(Int32_t1_3_il2cpp_TypeInfo_var, &L_4);
		String_t* L_6 = (__this->___HostAndPort_1);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_7 = String_Format_m1_412(NULL /*static, unused*/, _stringLiteral3510, L_2, L_5, L_6, /*hidden argument*/NULL);
		return L_7;
	}
}
// System.Void ServerSettings::.ctor()
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* List_1_t1_829_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1__ctor_m1_5519_MethodInfo_var;
extern "C" void ServerSettings__ctor_m8_849 (ServerSettings_t8_115 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(11);
		List_1_t1_829_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(34);
		List_1__ctor_m1_5519_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483649);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_0 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->___Empty_2;
		__this->___ServerAddress_4 = L_0;
		__this->___ServerPort_5 = ((int32_t)5055);
		String_t* L_1 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->___Empty_2;
		__this->___AppID_6 = L_1;
		__this->___EnabledRegions_8 = (-1);
		List_1_t1_829 * L_2 = (List_1_t1_829 *)il2cpp_codegen_object_new (List_1_t1_829_il2cpp_TypeInfo_var);
		List_1__ctor_m1_5519(L_2, /*hidden argument*/List_1__ctor_m1_5519_MethodInfo_var);
		__this->___RpcList_11 = L_2;
		ScriptableObject__ctor_m6_16(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void ServerSettings::UseCloudBestRegion(System.String)
extern "C" void ServerSettings_UseCloudBestRegion_m8_850 (ServerSettings_t8_115 * __this, String_t* ___cloudAppid, const MethodInfo* method)
{
	{
		__this->___HostType_2 = 4;
		String_t* L_0 = ___cloudAppid;
		__this->___AppID_6 = L_0;
		return;
	}
}
// System.Void ServerSettings::UseCloud(System.String)
extern "C" void ServerSettings_UseCloud_m8_851 (ServerSettings_t8_115 * __this, String_t* ___cloudAppid, const MethodInfo* method)
{
	{
		__this->___HostType_2 = 1;
		String_t* L_0 = ___cloudAppid;
		__this->___AppID_6 = L_0;
		return;
	}
}
// System.Void ServerSettings::UseCloud(System.String,CloudRegionCode)
extern "C" void ServerSettings_UseCloud_m8_852 (ServerSettings_t8_115 * __this, String_t* ___cloudAppid, int32_t ___code, const MethodInfo* method)
{
	{
		__this->___HostType_2 = 1;
		String_t* L_0 = ___cloudAppid;
		__this->___AppID_6 = L_0;
		int32_t L_1 = ___code;
		__this->___PreferredRegion_7 = L_1;
		return;
	}
}
// System.Void ServerSettings::UseMyServer(System.String,System.Int32,System.String)
extern Il2CppCodeGenString* _stringLiteral3511;
extern "C" void ServerSettings_UseMyServer_m8_853 (ServerSettings_t8_115 * __this, String_t* ___serverAddress, int32_t ___serverPort, String_t* ___application, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		_stringLiteral3511 = il2cpp_codegen_string_literal_from_index(3511);
		s_Il2CppMethodIntialized = true;
	}
	ServerSettings_t8_115 * G_B2_0 = {0};
	ServerSettings_t8_115 * G_B1_0 = {0};
	String_t* G_B3_0 = {0};
	ServerSettings_t8_115 * G_B3_1 = {0};
	{
		__this->___HostType_2 = 2;
		String_t* L_0 = ___application;
		G_B1_0 = __this;
		if (!L_0)
		{
			G_B2_0 = __this;
			goto IL_0014;
		}
	}
	{
		String_t* L_1 = ___application;
		G_B3_0 = L_1;
		G_B3_1 = G_B1_0;
		goto IL_0019;
	}

IL_0014:
	{
		G_B3_0 = _stringLiteral3511;
		G_B3_1 = G_B2_0;
	}

IL_0019:
	{
		NullCheck(G_B3_1);
		G_B3_1->___AppID_6 = G_B3_0;
		String_t* L_2 = ___serverAddress;
		__this->___ServerAddress_4 = L_2;
		int32_t L_3 = ___serverPort;
		__this->___ServerPort_5 = L_3;
		return;
	}
}
// System.String ServerSettings::ToString()
extern TypeInfo* ObjectU5BU5D_t1_157_il2cpp_TypeInfo_var;
extern TypeInfo* HostingOption_t8_125_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3512;
extern Il2CppCodeGenString* _stringLiteral232;
extern "C" String_t* ServerSettings_ToString_m8_854 (ServerSettings_t8_115 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ObjectU5BU5D_t1_157_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(19);
		HostingOption_t8_125_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1165);
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(11);
		_stringLiteral3512 = il2cpp_codegen_string_literal_from_index(3512);
		_stringLiteral232 = il2cpp_codegen_string_literal_from_index(232);
		s_Il2CppMethodIntialized = true;
	}
	{
		ObjectU5BU5D_t1_157* L_0 = ((ObjectU5BU5D_t1_157*)SZArrayNew(ObjectU5BU5D_t1_157_il2cpp_TypeInfo_var, 4));
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, 0);
		ArrayElementTypeCheck (L_0, _stringLiteral3512);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_0, 0, sizeof(Object_t *))) = (Object_t *)_stringLiteral3512;
		ObjectU5BU5D_t1_157* L_1 = L_0;
		int32_t L_2 = (__this->___HostType_2);
		int32_t L_3 = L_2;
		Object_t * L_4 = Box(HostingOption_t8_125_il2cpp_TypeInfo_var, &L_3);
		NullCheck(L_1);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_1, 1);
		ArrayElementTypeCheck (L_1, L_4);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_1, 1, sizeof(Object_t *))) = (Object_t *)L_4;
		ObjectU5BU5D_t1_157* L_5 = L_1;
		NullCheck(L_5);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_5, 2);
		ArrayElementTypeCheck (L_5, _stringLiteral232);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_5, 2, sizeof(Object_t *))) = (Object_t *)_stringLiteral232;
		ObjectU5BU5D_t1_157* L_6 = L_5;
		String_t* L_7 = (__this->___ServerAddress_4);
		NullCheck(L_6);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_6, 3);
		ArrayElementTypeCheck (L_6, L_7);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_6, 3, sizeof(Object_t *))) = (Object_t *)L_7;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_8 = String_Concat_m1_421(NULL /*static, unused*/, L_6, /*hidden argument*/NULL);
		return L_8;
	}
}
// System.Void PhotonAnimatorView/SynchronizedParameter::.ctor()
extern "C" void SynchronizedParameter__ctor_m8_855 (SynchronizedParameter_t8_128 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1_0(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void PhotonAnimatorView/SynchronizedLayer::.ctor()
extern "C" void SynchronizedLayer__ctor_m8_856 (SynchronizedLayer_t8_129 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1_0(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void PhotonAnimatorView/<DoesLayerSynchronizeTypeExist>c__AnonStorey3::.ctor()
extern "C" void U3CDoesLayerSynchronizeTypeExistU3Ec__AnonStorey3__ctor_m8_857 (U3CDoesLayerSynchronizeTypeExistU3Ec__AnonStorey3_t8_130 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1_0(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean PhotonAnimatorView/<DoesLayerSynchronizeTypeExist>c__AnonStorey3::<>m__0(PhotonAnimatorView/SynchronizedLayer)
extern "C" bool U3CDoesLayerSynchronizeTypeExistU3Ec__AnonStorey3_U3CU3Em__0_m8_858 (U3CDoesLayerSynchronizeTypeExistU3Ec__AnonStorey3_t8_130 * __this, SynchronizedLayer_t8_129 * ___item, const MethodInfo* method)
{
	{
		SynchronizedLayer_t8_129 * L_0 = ___item;
		NullCheck(L_0);
		int32_t L_1 = (L_0->___LayerIndex_1);
		int32_t L_2 = (__this->___layerIndex_0);
		return ((((int32_t)L_1) == ((int32_t)L_2))? 1 : 0);
	}
}
// System.Void PhotonAnimatorView/<DoesParameterSynchronizeTypeExist>c__AnonStorey4::.ctor()
extern "C" void U3CDoesParameterSynchronizeTypeExistU3Ec__AnonStorey4__ctor_m8_859 (U3CDoesParameterSynchronizeTypeExistU3Ec__AnonStorey4_t8_131 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1_0(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean PhotonAnimatorView/<DoesParameterSynchronizeTypeExist>c__AnonStorey4::<>m__1(PhotonAnimatorView/SynchronizedParameter)
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern "C" bool U3CDoesParameterSynchronizeTypeExistU3Ec__AnonStorey4_U3CU3Em__1_m8_860 (U3CDoesParameterSynchronizeTypeExistU3Ec__AnonStorey4_t8_131 * __this, SynchronizedParameter_t8_128 * ___item, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(11);
		s_Il2CppMethodIntialized = true;
	}
	{
		SynchronizedParameter_t8_128 * L_0 = ___item;
		NullCheck(L_0);
		String_t* L_1 = (L_0->___Name_2);
		String_t* L_2 = (__this->___name_0);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_3 = String_op_Equality_m1_454(NULL /*static, unused*/, L_1, L_2, /*hidden argument*/NULL);
		return L_3;
	}
}
// System.Void PhotonAnimatorView/<GetLayerSynchronizeType>c__AnonStorey5::.ctor()
extern "C" void U3CGetLayerSynchronizeTypeU3Ec__AnonStorey5__ctor_m8_861 (U3CGetLayerSynchronizeTypeU3Ec__AnonStorey5_t8_132 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1_0(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean PhotonAnimatorView/<GetLayerSynchronizeType>c__AnonStorey5::<>m__2(PhotonAnimatorView/SynchronizedLayer)
extern "C" bool U3CGetLayerSynchronizeTypeU3Ec__AnonStorey5_U3CU3Em__2_m8_862 (U3CGetLayerSynchronizeTypeU3Ec__AnonStorey5_t8_132 * __this, SynchronizedLayer_t8_129 * ___item, const MethodInfo* method)
{
	{
		SynchronizedLayer_t8_129 * L_0 = ___item;
		NullCheck(L_0);
		int32_t L_1 = (L_0->___LayerIndex_1);
		int32_t L_2 = (__this->___layerIndex_0);
		return ((((int32_t)L_1) == ((int32_t)L_2))? 1 : 0);
	}
}
// System.Void PhotonAnimatorView/<GetParameterSynchronizeType>c__AnonStorey6::.ctor()
extern "C" void U3CGetParameterSynchronizeTypeU3Ec__AnonStorey6__ctor_m8_863 (U3CGetParameterSynchronizeTypeU3Ec__AnonStorey6_t8_133 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1_0(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean PhotonAnimatorView/<GetParameterSynchronizeType>c__AnonStorey6::<>m__3(PhotonAnimatorView/SynchronizedParameter)
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern "C" bool U3CGetParameterSynchronizeTypeU3Ec__AnonStorey6_U3CU3Em__3_m8_864 (U3CGetParameterSynchronizeTypeU3Ec__AnonStorey6_t8_133 * __this, SynchronizedParameter_t8_128 * ___item, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(11);
		s_Il2CppMethodIntialized = true;
	}
	{
		SynchronizedParameter_t8_128 * L_0 = ___item;
		NullCheck(L_0);
		String_t* L_1 = (L_0->___Name_2);
		String_t* L_2 = (__this->___name_0);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_3 = String_op_Equality_m1_454(NULL /*static, unused*/, L_1, L_2, /*hidden argument*/NULL);
		return L_3;
	}
}
// System.Void PhotonAnimatorView/<SetLayerSynchronized>c__AnonStorey7::.ctor()
extern "C" void U3CSetLayerSynchronizedU3Ec__AnonStorey7__ctor_m8_865 (U3CSetLayerSynchronizedU3Ec__AnonStorey7_t8_134 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1_0(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean PhotonAnimatorView/<SetLayerSynchronized>c__AnonStorey7::<>m__4(PhotonAnimatorView/SynchronizedLayer)
extern "C" bool U3CSetLayerSynchronizedU3Ec__AnonStorey7_U3CU3Em__4_m8_866 (U3CSetLayerSynchronizedU3Ec__AnonStorey7_t8_134 * __this, SynchronizedLayer_t8_129 * ___item, const MethodInfo* method)
{
	{
		SynchronizedLayer_t8_129 * L_0 = ___item;
		NullCheck(L_0);
		int32_t L_1 = (L_0->___LayerIndex_1);
		int32_t L_2 = (__this->___layerIndex_0);
		return ((((int32_t)L_1) == ((int32_t)L_2))? 1 : 0);
	}
}
// System.Void PhotonAnimatorView/<SetParameterSynchronized>c__AnonStorey8::.ctor()
extern "C" void U3CSetParameterSynchronizedU3Ec__AnonStorey8__ctor_m8_867 (U3CSetParameterSynchronizedU3Ec__AnonStorey8_t8_135 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1_0(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean PhotonAnimatorView/<SetParameterSynchronized>c__AnonStorey8::<>m__5(PhotonAnimatorView/SynchronizedParameter)
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern "C" bool U3CSetParameterSynchronizedU3Ec__AnonStorey8_U3CU3Em__5_m8_868 (U3CSetParameterSynchronizedU3Ec__AnonStorey8_t8_135 * __this, SynchronizedParameter_t8_128 * ___item, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(11);
		s_Il2CppMethodIntialized = true;
	}
	{
		SynchronizedParameter_t8_128 * L_0 = ___item;
		NullCheck(L_0);
		String_t* L_1 = (L_0->___Name_2);
		String_t* L_2 = (__this->___name_0);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_3 = String_op_Equality_m1_454(NULL /*static, unused*/, L_1, L_2, /*hidden argument*/NULL);
		return L_3;
	}
}
// System.Void PhotonAnimatorView::.ctor()
extern TypeInfo* List_1_t1_949_il2cpp_TypeInfo_var;
extern TypeInfo* List_1_t1_950_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1__ctor_m1_5678_MethodInfo_var;
extern const MethodInfo* List_1__ctor_m1_5679_MethodInfo_var;
extern "C" void PhotonAnimatorView__ctor_m8_869 (PhotonAnimatorView_t8_27 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		List_1_t1_949_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1168);
		List_1_t1_950_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1169);
		List_1__ctor_m1_5678_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483942);
		List_1__ctor_m1_5679_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483943);
		s_Il2CppMethodIntialized = true;
	}
	{
		__this->___ShowLayerWeightsInspector_4 = 1;
		__this->___ShowParameterInspector_5 = 1;
		List_1_t1_949 * L_0 = (List_1_t1_949 *)il2cpp_codegen_object_new (List_1_t1_949_il2cpp_TypeInfo_var);
		List_1__ctor_m1_5678(L_0, /*hidden argument*/List_1__ctor_m1_5678_MethodInfo_var);
		__this->___m_SynchronizeParameters_6 = L_0;
		List_1_t1_950 * L_1 = (List_1_t1_950 *)il2cpp_codegen_object_new (List_1_t1_950_il2cpp_TypeInfo_var);
		List_1__ctor_m1_5679(L_1, /*hidden argument*/List_1__ctor_m1_5679_MethodInfo_var);
		__this->___m_SynchronizeLayers_7 = L_1;
		__this->___m_WasSynchronizeTypeChanged_10 = 1;
		MonoBehaviour__ctor_m6_525(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void PhotonAnimatorView::Awake()
extern TypeInfo* PhotonStreamQueue_t8_117_il2cpp_TypeInfo_var;
extern const MethodInfo* Component_GetComponent_TisPhotonView_t8_3_m6_1658_MethodInfo_var;
extern const MethodInfo* Component_GetComponent_TisAnimator_t6_138_m6_1656_MethodInfo_var;
extern "C" void PhotonAnimatorView_Awake_m8_870 (PhotonAnimatorView_t8_27 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		PhotonStreamQueue_t8_117_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1170);
		Component_GetComponent_TisPhotonView_t8_3_m6_1658_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483836);
		Component_GetComponent_TisAnimator_t6_138_m6_1656_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483834);
		s_Il2CppMethodIntialized = true;
	}
	{
		PhotonView_t8_3 * L_0 = Component_GetComponent_TisPhotonView_t8_3_m6_1658(__this, /*hidden argument*/Component_GetComponent_TisPhotonView_t8_3_m6_1658_MethodInfo_var);
		__this->___m_PhotonView_11 = L_0;
		PhotonStreamQueue_t8_117 * L_1 = (PhotonStreamQueue_t8_117 *)il2cpp_codegen_object_new (PhotonStreamQueue_t8_117_il2cpp_TypeInfo_var);
		PhotonStreamQueue__ctor_m8_754(L_1, ((int32_t)120), /*hidden argument*/NULL);
		__this->___m_StreamQueue_3 = L_1;
		Animator_t6_138 * L_2 = Component_GetComponent_TisAnimator_t6_138_m6_1656(__this, /*hidden argument*/Component_GetComponent_TisAnimator_t6_138_m6_1656_MethodInfo_var);
		__this->___m_Animator_2 = L_2;
		return;
	}
}
// System.Void PhotonAnimatorView::Update()
extern TypeInfo* PhotonNetwork_t8_114_il2cpp_TypeInfo_var;
extern "C" void PhotonAnimatorView_Update_m8_871 (PhotonAnimatorView_t8_27 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		PhotonNetwork_t8_114_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1058);
		s_Il2CppMethodIntialized = true;
	}
	{
		PhotonView_t8_3 * L_0 = (__this->___m_PhotonView_11);
		NullCheck(L_0);
		bool L_1 = PhotonView_get_isMine_m8_774(L_0, /*hidden argument*/NULL);
		if (L_1)
		{
			goto IL_0026;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(PhotonNetwork_t8_114_il2cpp_TypeInfo_var);
		bool L_2 = PhotonNetwork_get_connected_m8_603(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_0026;
		}
	}
	{
		Animator_t6_138 * L_3 = (__this->___m_Animator_2);
		NullCheck(L_3);
		Animator_set_applyRootMotion_m6_840(L_3, 0, /*hidden argument*/NULL);
	}

IL_0026:
	{
		IL2CPP_RUNTIME_CLASS_INIT(PhotonNetwork_t8_114_il2cpp_TypeInfo_var);
		bool L_4 = PhotonNetwork_get_inRoom_m8_649(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_0040;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(PhotonNetwork_t8_114_il2cpp_TypeInfo_var);
		Room_t8_100 * L_5 = PhotonNetwork_get_room_m8_611(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_5);
		int32_t L_6 = Room_get_playerCount_m8_808(L_5, /*hidden argument*/NULL);
		if ((((int32_t)L_6) > ((int32_t)1)))
		{
			goto IL_004c;
		}
	}

IL_0040:
	{
		PhotonStreamQueue_t8_117 * L_7 = (__this->___m_StreamQueue_3);
		NullCheck(L_7);
		PhotonStreamQueue_Reset_m8_756(L_7, /*hidden argument*/NULL);
		return;
	}

IL_004c:
	{
		PhotonView_t8_3 * L_8 = (__this->___m_PhotonView_11);
		NullCheck(L_8);
		bool L_9 = PhotonView_get_isMine_m8_774(L_8, /*hidden argument*/NULL);
		if (!L_9)
		{
			goto IL_0067;
		}
	}
	{
		PhotonAnimatorView_SerializeDataContinuously_m8_880(__this, /*hidden argument*/NULL);
		goto IL_006d;
	}

IL_0067:
	{
		PhotonAnimatorView_DeserializeDataContinuously_m8_881(__this, /*hidden argument*/NULL);
	}

IL_006d:
	{
		return;
	}
}
// System.Boolean PhotonAnimatorView::DoesLayerSynchronizeTypeExist(System.Int32)
extern TypeInfo* U3CDoesLayerSynchronizeTypeExistU3Ec__AnonStorey3_t8_130_il2cpp_TypeInfo_var;
extern TypeInfo* Predicate_1_t1_969_il2cpp_TypeInfo_var;
extern const MethodInfo* U3CDoesLayerSynchronizeTypeExistU3Ec__AnonStorey3_U3CU3Em__0_m8_858_MethodInfo_var;
extern const MethodInfo* Predicate_1__ctor_m1_5680_MethodInfo_var;
extern const MethodInfo* List_1_FindIndex_m1_5681_MethodInfo_var;
extern "C" bool PhotonAnimatorView_DoesLayerSynchronizeTypeExist_m8_872 (PhotonAnimatorView_t8_27 * __this, int32_t ___layerIndex, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		U3CDoesLayerSynchronizeTypeExistU3Ec__AnonStorey3_t8_130_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1171);
		Predicate_1_t1_969_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1172);
		U3CDoesLayerSynchronizeTypeExistU3Ec__AnonStorey3_U3CU3Em__0_m8_858_MethodInfo_var = il2cpp_codegen_method_info_from_index(296);
		Predicate_1__ctor_m1_5680_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483945);
		List_1_FindIndex_m1_5681_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483946);
		s_Il2CppMethodIntialized = true;
	}
	U3CDoesLayerSynchronizeTypeExistU3Ec__AnonStorey3_t8_130 * V_0 = {0};
	{
		U3CDoesLayerSynchronizeTypeExistU3Ec__AnonStorey3_t8_130 * L_0 = (U3CDoesLayerSynchronizeTypeExistU3Ec__AnonStorey3_t8_130 *)il2cpp_codegen_object_new (U3CDoesLayerSynchronizeTypeExistU3Ec__AnonStorey3_t8_130_il2cpp_TypeInfo_var);
		U3CDoesLayerSynchronizeTypeExistU3Ec__AnonStorey3__ctor_m8_857(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CDoesLayerSynchronizeTypeExistU3Ec__AnonStorey3_t8_130 * L_1 = V_0;
		int32_t L_2 = ___layerIndex;
		NullCheck(L_1);
		L_1->___layerIndex_0 = L_2;
		List_1_t1_950 * L_3 = (__this->___m_SynchronizeLayers_7);
		U3CDoesLayerSynchronizeTypeExistU3Ec__AnonStorey3_t8_130 * L_4 = V_0;
		IntPtr_t L_5 = { (void*)U3CDoesLayerSynchronizeTypeExistU3Ec__AnonStorey3_U3CU3Em__0_m8_858_MethodInfo_var };
		Predicate_1_t1_969 * L_6 = (Predicate_1_t1_969 *)il2cpp_codegen_object_new (Predicate_1_t1_969_il2cpp_TypeInfo_var);
		Predicate_1__ctor_m1_5680(L_6, L_4, L_5, /*hidden argument*/Predicate_1__ctor_m1_5680_MethodInfo_var);
		NullCheck(L_3);
		int32_t L_7 = List_1_FindIndex_m1_5681(L_3, L_6, /*hidden argument*/List_1_FindIndex_m1_5681_MethodInfo_var);
		return ((((int32_t)((((int32_t)L_7) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
	}
}
// System.Boolean PhotonAnimatorView::DoesParameterSynchronizeTypeExist(System.String)
extern TypeInfo* U3CDoesParameterSynchronizeTypeExistU3Ec__AnonStorey4_t8_131_il2cpp_TypeInfo_var;
extern TypeInfo* Predicate_1_t1_970_il2cpp_TypeInfo_var;
extern const MethodInfo* U3CDoesParameterSynchronizeTypeExistU3Ec__AnonStorey4_U3CU3Em__1_m8_860_MethodInfo_var;
extern const MethodInfo* Predicate_1__ctor_m1_5682_MethodInfo_var;
extern const MethodInfo* List_1_FindIndex_m1_5683_MethodInfo_var;
extern "C" bool PhotonAnimatorView_DoesParameterSynchronizeTypeExist_m8_873 (PhotonAnimatorView_t8_27 * __this, String_t* ___name, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		U3CDoesParameterSynchronizeTypeExistU3Ec__AnonStorey4_t8_131_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1173);
		Predicate_1_t1_970_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1174);
		U3CDoesParameterSynchronizeTypeExistU3Ec__AnonStorey4_U3CU3Em__1_m8_860_MethodInfo_var = il2cpp_codegen_method_info_from_index(299);
		Predicate_1__ctor_m1_5682_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483948);
		List_1_FindIndex_m1_5683_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483949);
		s_Il2CppMethodIntialized = true;
	}
	U3CDoesParameterSynchronizeTypeExistU3Ec__AnonStorey4_t8_131 * V_0 = {0};
	{
		U3CDoesParameterSynchronizeTypeExistU3Ec__AnonStorey4_t8_131 * L_0 = (U3CDoesParameterSynchronizeTypeExistU3Ec__AnonStorey4_t8_131 *)il2cpp_codegen_object_new (U3CDoesParameterSynchronizeTypeExistU3Ec__AnonStorey4_t8_131_il2cpp_TypeInfo_var);
		U3CDoesParameterSynchronizeTypeExistU3Ec__AnonStorey4__ctor_m8_859(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CDoesParameterSynchronizeTypeExistU3Ec__AnonStorey4_t8_131 * L_1 = V_0;
		String_t* L_2 = ___name;
		NullCheck(L_1);
		L_1->___name_0 = L_2;
		List_1_t1_949 * L_3 = (__this->___m_SynchronizeParameters_6);
		U3CDoesParameterSynchronizeTypeExistU3Ec__AnonStorey4_t8_131 * L_4 = V_0;
		IntPtr_t L_5 = { (void*)U3CDoesParameterSynchronizeTypeExistU3Ec__AnonStorey4_U3CU3Em__1_m8_860_MethodInfo_var };
		Predicate_1_t1_970 * L_6 = (Predicate_1_t1_970 *)il2cpp_codegen_object_new (Predicate_1_t1_970_il2cpp_TypeInfo_var);
		Predicate_1__ctor_m1_5682(L_6, L_4, L_5, /*hidden argument*/Predicate_1__ctor_m1_5682_MethodInfo_var);
		NullCheck(L_3);
		int32_t L_7 = List_1_FindIndex_m1_5683(L_3, L_6, /*hidden argument*/List_1_FindIndex_m1_5683_MethodInfo_var);
		return ((((int32_t)((((int32_t)L_7) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
	}
}
// System.Collections.Generic.List`1<PhotonAnimatorView/SynchronizedLayer> PhotonAnimatorView::GetSynchronizedLayers()
extern "C" List_1_t1_950 * PhotonAnimatorView_GetSynchronizedLayers_m8_874 (PhotonAnimatorView_t8_27 * __this, const MethodInfo* method)
{
	{
		List_1_t1_950 * L_0 = (__this->___m_SynchronizeLayers_7);
		return L_0;
	}
}
// System.Collections.Generic.List`1<PhotonAnimatorView/SynchronizedParameter> PhotonAnimatorView::GetSynchronizedParameters()
extern "C" List_1_t1_949 * PhotonAnimatorView_GetSynchronizedParameters_m8_875 (PhotonAnimatorView_t8_27 * __this, const MethodInfo* method)
{
	{
		List_1_t1_949 * L_0 = (__this->___m_SynchronizeParameters_6);
		return L_0;
	}
}
// PhotonAnimatorView/SynchronizeType PhotonAnimatorView::GetLayerSynchronizeType(System.Int32)
extern TypeInfo* U3CGetLayerSynchronizeTypeU3Ec__AnonStorey5_t8_132_il2cpp_TypeInfo_var;
extern TypeInfo* Predicate_1_t1_969_il2cpp_TypeInfo_var;
extern const MethodInfo* U3CGetLayerSynchronizeTypeU3Ec__AnonStorey5_U3CU3Em__2_m8_862_MethodInfo_var;
extern const MethodInfo* Predicate_1__ctor_m1_5680_MethodInfo_var;
extern const MethodInfo* List_1_FindIndex_m1_5681_MethodInfo_var;
extern "C" int32_t PhotonAnimatorView_GetLayerSynchronizeType_m8_876 (PhotonAnimatorView_t8_27 * __this, int32_t ___layerIndex, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		U3CGetLayerSynchronizeTypeU3Ec__AnonStorey5_t8_132_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1175);
		Predicate_1_t1_969_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1172);
		U3CGetLayerSynchronizeTypeU3Ec__AnonStorey5_U3CU3Em__2_m8_862_MethodInfo_var = il2cpp_codegen_method_info_from_index(302);
		Predicate_1__ctor_m1_5680_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483945);
		List_1_FindIndex_m1_5681_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483946);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	U3CGetLayerSynchronizeTypeU3Ec__AnonStorey5_t8_132 * V_1 = {0};
	{
		U3CGetLayerSynchronizeTypeU3Ec__AnonStorey5_t8_132 * L_0 = (U3CGetLayerSynchronizeTypeU3Ec__AnonStorey5_t8_132 *)il2cpp_codegen_object_new (U3CGetLayerSynchronizeTypeU3Ec__AnonStorey5_t8_132_il2cpp_TypeInfo_var);
		U3CGetLayerSynchronizeTypeU3Ec__AnonStorey5__ctor_m8_861(L_0, /*hidden argument*/NULL);
		V_1 = L_0;
		U3CGetLayerSynchronizeTypeU3Ec__AnonStorey5_t8_132 * L_1 = V_1;
		int32_t L_2 = ___layerIndex;
		NullCheck(L_1);
		L_1->___layerIndex_0 = L_2;
		List_1_t1_950 * L_3 = (__this->___m_SynchronizeLayers_7);
		U3CGetLayerSynchronizeTypeU3Ec__AnonStorey5_t8_132 * L_4 = V_1;
		IntPtr_t L_5 = { (void*)U3CGetLayerSynchronizeTypeU3Ec__AnonStorey5_U3CU3Em__2_m8_862_MethodInfo_var };
		Predicate_1_t1_969 * L_6 = (Predicate_1_t1_969 *)il2cpp_codegen_object_new (Predicate_1_t1_969_il2cpp_TypeInfo_var);
		Predicate_1__ctor_m1_5680(L_6, L_4, L_5, /*hidden argument*/Predicate_1__ctor_m1_5680_MethodInfo_var);
		NullCheck(L_3);
		int32_t L_7 = List_1_FindIndex_m1_5681(L_3, L_6, /*hidden argument*/List_1_FindIndex_m1_5681_MethodInfo_var);
		V_0 = L_7;
		int32_t L_8 = V_0;
		if ((!(((uint32_t)L_8) == ((uint32_t)(-1)))))
		{
			goto IL_002e;
		}
	}
	{
		return (int32_t)(0);
	}

IL_002e:
	{
		List_1_t1_950 * L_9 = (__this->___m_SynchronizeLayers_7);
		int32_t L_10 = V_0;
		NullCheck(L_9);
		SynchronizedLayer_t8_129 * L_11 = (SynchronizedLayer_t8_129 *)VirtFuncInvoker1< SynchronizedLayer_t8_129 *, int32_t >::Invoke(28 /* !0 System.Collections.Generic.List`1<PhotonAnimatorView/SynchronizedLayer>::get_Item(System.Int32) */, L_9, L_10);
		NullCheck(L_11);
		int32_t L_12 = (L_11->___SynchronizeType_0);
		return L_12;
	}
}
// PhotonAnimatorView/SynchronizeType PhotonAnimatorView::GetParameterSynchronizeType(System.String)
extern TypeInfo* U3CGetParameterSynchronizeTypeU3Ec__AnonStorey6_t8_133_il2cpp_TypeInfo_var;
extern TypeInfo* Predicate_1_t1_970_il2cpp_TypeInfo_var;
extern const MethodInfo* U3CGetParameterSynchronizeTypeU3Ec__AnonStorey6_U3CU3Em__3_m8_864_MethodInfo_var;
extern const MethodInfo* Predicate_1__ctor_m1_5682_MethodInfo_var;
extern const MethodInfo* List_1_FindIndex_m1_5683_MethodInfo_var;
extern "C" int32_t PhotonAnimatorView_GetParameterSynchronizeType_m8_877 (PhotonAnimatorView_t8_27 * __this, String_t* ___name, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		U3CGetParameterSynchronizeTypeU3Ec__AnonStorey6_t8_133_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1176);
		Predicate_1_t1_970_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1174);
		U3CGetParameterSynchronizeTypeU3Ec__AnonStorey6_U3CU3Em__3_m8_864_MethodInfo_var = il2cpp_codegen_method_info_from_index(303);
		Predicate_1__ctor_m1_5682_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483948);
		List_1_FindIndex_m1_5683_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483949);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	U3CGetParameterSynchronizeTypeU3Ec__AnonStorey6_t8_133 * V_1 = {0};
	{
		U3CGetParameterSynchronizeTypeU3Ec__AnonStorey6_t8_133 * L_0 = (U3CGetParameterSynchronizeTypeU3Ec__AnonStorey6_t8_133 *)il2cpp_codegen_object_new (U3CGetParameterSynchronizeTypeU3Ec__AnonStorey6_t8_133_il2cpp_TypeInfo_var);
		U3CGetParameterSynchronizeTypeU3Ec__AnonStorey6__ctor_m8_863(L_0, /*hidden argument*/NULL);
		V_1 = L_0;
		U3CGetParameterSynchronizeTypeU3Ec__AnonStorey6_t8_133 * L_1 = V_1;
		String_t* L_2 = ___name;
		NullCheck(L_1);
		L_1->___name_0 = L_2;
		List_1_t1_949 * L_3 = (__this->___m_SynchronizeParameters_6);
		U3CGetParameterSynchronizeTypeU3Ec__AnonStorey6_t8_133 * L_4 = V_1;
		IntPtr_t L_5 = { (void*)U3CGetParameterSynchronizeTypeU3Ec__AnonStorey6_U3CU3Em__3_m8_864_MethodInfo_var };
		Predicate_1_t1_970 * L_6 = (Predicate_1_t1_970 *)il2cpp_codegen_object_new (Predicate_1_t1_970_il2cpp_TypeInfo_var);
		Predicate_1__ctor_m1_5682(L_6, L_4, L_5, /*hidden argument*/Predicate_1__ctor_m1_5682_MethodInfo_var);
		NullCheck(L_3);
		int32_t L_7 = List_1_FindIndex_m1_5683(L_3, L_6, /*hidden argument*/List_1_FindIndex_m1_5683_MethodInfo_var);
		V_0 = L_7;
		int32_t L_8 = V_0;
		if ((!(((uint32_t)L_8) == ((uint32_t)(-1)))))
		{
			goto IL_002e;
		}
	}
	{
		return (int32_t)(0);
	}

IL_002e:
	{
		List_1_t1_949 * L_9 = (__this->___m_SynchronizeParameters_6);
		int32_t L_10 = V_0;
		NullCheck(L_9);
		SynchronizedParameter_t8_128 * L_11 = (SynchronizedParameter_t8_128 *)VirtFuncInvoker1< SynchronizedParameter_t8_128 *, int32_t >::Invoke(28 /* !0 System.Collections.Generic.List`1<PhotonAnimatorView/SynchronizedParameter>::get_Item(System.Int32) */, L_9, L_10);
		NullCheck(L_11);
		int32_t L_12 = (L_11->___SynchronizeType_1);
		return L_12;
	}
}
// System.Void PhotonAnimatorView::SetLayerSynchronized(System.Int32,PhotonAnimatorView/SynchronizeType)
extern TypeInfo* U3CSetLayerSynchronizedU3Ec__AnonStorey7_t8_134_il2cpp_TypeInfo_var;
extern TypeInfo* Predicate_1_t1_969_il2cpp_TypeInfo_var;
extern TypeInfo* SynchronizedLayer_t8_129_il2cpp_TypeInfo_var;
extern const MethodInfo* U3CSetLayerSynchronizedU3Ec__AnonStorey7_U3CU3Em__4_m8_866_MethodInfo_var;
extern const MethodInfo* Predicate_1__ctor_m1_5680_MethodInfo_var;
extern const MethodInfo* List_1_FindIndex_m1_5681_MethodInfo_var;
extern "C" void PhotonAnimatorView_SetLayerSynchronized_m8_878 (PhotonAnimatorView_t8_27 * __this, int32_t ___layerIndex, int32_t ___synchronizeType, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		U3CSetLayerSynchronizedU3Ec__AnonStorey7_t8_134_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1177);
		Predicate_1_t1_969_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1172);
		SynchronizedLayer_t8_129_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1167);
		U3CSetLayerSynchronizedU3Ec__AnonStorey7_U3CU3Em__4_m8_866_MethodInfo_var = il2cpp_codegen_method_info_from_index(304);
		Predicate_1__ctor_m1_5680_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483945);
		List_1_FindIndex_m1_5681_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483946);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	U3CSetLayerSynchronizedU3Ec__AnonStorey7_t8_134 * V_1 = {0};
	SynchronizedLayer_t8_129 * V_2 = {0};
	{
		U3CSetLayerSynchronizedU3Ec__AnonStorey7_t8_134 * L_0 = (U3CSetLayerSynchronizedU3Ec__AnonStorey7_t8_134 *)il2cpp_codegen_object_new (U3CSetLayerSynchronizedU3Ec__AnonStorey7_t8_134_il2cpp_TypeInfo_var);
		U3CSetLayerSynchronizedU3Ec__AnonStorey7__ctor_m8_865(L_0, /*hidden argument*/NULL);
		V_1 = L_0;
		U3CSetLayerSynchronizedU3Ec__AnonStorey7_t8_134 * L_1 = V_1;
		int32_t L_2 = ___layerIndex;
		NullCheck(L_1);
		L_1->___layerIndex_0 = L_2;
		bool L_3 = Application_get_isPlaying_m6_451(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_001e;
		}
	}
	{
		__this->___m_WasSynchronizeTypeChanged_10 = 1;
	}

IL_001e:
	{
		List_1_t1_950 * L_4 = (__this->___m_SynchronizeLayers_7);
		U3CSetLayerSynchronizedU3Ec__AnonStorey7_t8_134 * L_5 = V_1;
		IntPtr_t L_6 = { (void*)U3CSetLayerSynchronizedU3Ec__AnonStorey7_U3CU3Em__4_m8_866_MethodInfo_var };
		Predicate_1_t1_969 * L_7 = (Predicate_1_t1_969 *)il2cpp_codegen_object_new (Predicate_1_t1_969_il2cpp_TypeInfo_var);
		Predicate_1__ctor_m1_5680(L_7, L_5, L_6, /*hidden argument*/Predicate_1__ctor_m1_5680_MethodInfo_var);
		NullCheck(L_4);
		int32_t L_8 = List_1_FindIndex_m1_5681(L_4, L_7, /*hidden argument*/List_1_FindIndex_m1_5681_MethodInfo_var);
		V_0 = L_8;
		int32_t L_9 = V_0;
		if ((!(((uint32_t)L_9) == ((uint32_t)(-1)))))
		{
			goto IL_0067;
		}
	}
	{
		List_1_t1_950 * L_10 = (__this->___m_SynchronizeLayers_7);
		SynchronizedLayer_t8_129 * L_11 = (SynchronizedLayer_t8_129 *)il2cpp_codegen_object_new (SynchronizedLayer_t8_129_il2cpp_TypeInfo_var);
		SynchronizedLayer__ctor_m8_856(L_11, /*hidden argument*/NULL);
		V_2 = L_11;
		SynchronizedLayer_t8_129 * L_12 = V_2;
		U3CSetLayerSynchronizedU3Ec__AnonStorey7_t8_134 * L_13 = V_1;
		NullCheck(L_13);
		int32_t L_14 = (L_13->___layerIndex_0);
		NullCheck(L_12);
		L_12->___LayerIndex_1 = L_14;
		SynchronizedLayer_t8_129 * L_15 = V_2;
		int32_t L_16 = ___synchronizeType;
		NullCheck(L_15);
		L_15->___SynchronizeType_0 = L_16;
		SynchronizedLayer_t8_129 * L_17 = V_2;
		NullCheck(L_10);
		VirtActionInvoker1< SynchronizedLayer_t8_129 * >::Invoke(19 /* System.Void System.Collections.Generic.List`1<PhotonAnimatorView/SynchronizedLayer>::Add(!0) */, L_10, L_17);
		goto IL_0079;
	}

IL_0067:
	{
		List_1_t1_950 * L_18 = (__this->___m_SynchronizeLayers_7);
		int32_t L_19 = V_0;
		NullCheck(L_18);
		SynchronizedLayer_t8_129 * L_20 = (SynchronizedLayer_t8_129 *)VirtFuncInvoker1< SynchronizedLayer_t8_129 *, int32_t >::Invoke(28 /* !0 System.Collections.Generic.List`1<PhotonAnimatorView/SynchronizedLayer>::get_Item(System.Int32) */, L_18, L_19);
		int32_t L_21 = ___synchronizeType;
		NullCheck(L_20);
		L_20->___SynchronizeType_0 = L_21;
	}

IL_0079:
	{
		return;
	}
}
// System.Void PhotonAnimatorView::SetParameterSynchronized(System.String,PhotonAnimatorView/ParameterType,PhotonAnimatorView/SynchronizeType)
extern TypeInfo* U3CSetParameterSynchronizedU3Ec__AnonStorey8_t8_135_il2cpp_TypeInfo_var;
extern TypeInfo* Predicate_1_t1_970_il2cpp_TypeInfo_var;
extern TypeInfo* SynchronizedParameter_t8_128_il2cpp_TypeInfo_var;
extern const MethodInfo* U3CSetParameterSynchronizedU3Ec__AnonStorey8_U3CU3Em__5_m8_868_MethodInfo_var;
extern const MethodInfo* Predicate_1__ctor_m1_5682_MethodInfo_var;
extern const MethodInfo* List_1_FindIndex_m1_5683_MethodInfo_var;
extern "C" void PhotonAnimatorView_SetParameterSynchronized_m8_879 (PhotonAnimatorView_t8_27 * __this, String_t* ___name, int32_t ___type, int32_t ___synchronizeType, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		U3CSetParameterSynchronizedU3Ec__AnonStorey8_t8_135_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1178);
		Predicate_1_t1_970_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1174);
		SynchronizedParameter_t8_128_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1166);
		U3CSetParameterSynchronizedU3Ec__AnonStorey8_U3CU3Em__5_m8_868_MethodInfo_var = il2cpp_codegen_method_info_from_index(305);
		Predicate_1__ctor_m1_5682_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483948);
		List_1_FindIndex_m1_5683_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483949);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	U3CSetParameterSynchronizedU3Ec__AnonStorey8_t8_135 * V_1 = {0};
	SynchronizedParameter_t8_128 * V_2 = {0};
	{
		U3CSetParameterSynchronizedU3Ec__AnonStorey8_t8_135 * L_0 = (U3CSetParameterSynchronizedU3Ec__AnonStorey8_t8_135 *)il2cpp_codegen_object_new (U3CSetParameterSynchronizedU3Ec__AnonStorey8_t8_135_il2cpp_TypeInfo_var);
		U3CSetParameterSynchronizedU3Ec__AnonStorey8__ctor_m8_867(L_0, /*hidden argument*/NULL);
		V_1 = L_0;
		U3CSetParameterSynchronizedU3Ec__AnonStorey8_t8_135 * L_1 = V_1;
		String_t* L_2 = ___name;
		NullCheck(L_1);
		L_1->___name_0 = L_2;
		bool L_3 = Application_get_isPlaying_m6_451(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_001e;
		}
	}
	{
		__this->___m_WasSynchronizeTypeChanged_10 = 1;
	}

IL_001e:
	{
		List_1_t1_949 * L_4 = (__this->___m_SynchronizeParameters_6);
		U3CSetParameterSynchronizedU3Ec__AnonStorey8_t8_135 * L_5 = V_1;
		IntPtr_t L_6 = { (void*)U3CSetParameterSynchronizedU3Ec__AnonStorey8_U3CU3Em__5_m8_868_MethodInfo_var };
		Predicate_1_t1_970 * L_7 = (Predicate_1_t1_970 *)il2cpp_codegen_object_new (Predicate_1_t1_970_il2cpp_TypeInfo_var);
		Predicate_1__ctor_m1_5682(L_7, L_5, L_6, /*hidden argument*/Predicate_1__ctor_m1_5682_MethodInfo_var);
		NullCheck(L_4);
		int32_t L_8 = List_1_FindIndex_m1_5683(L_4, L_7, /*hidden argument*/List_1_FindIndex_m1_5683_MethodInfo_var);
		V_0 = L_8;
		int32_t L_9 = V_0;
		if ((!(((uint32_t)L_9) == ((uint32_t)(-1)))))
		{
			goto IL_006e;
		}
	}
	{
		List_1_t1_949 * L_10 = (__this->___m_SynchronizeParameters_6);
		SynchronizedParameter_t8_128 * L_11 = (SynchronizedParameter_t8_128 *)il2cpp_codegen_object_new (SynchronizedParameter_t8_128_il2cpp_TypeInfo_var);
		SynchronizedParameter__ctor_m8_855(L_11, /*hidden argument*/NULL);
		V_2 = L_11;
		SynchronizedParameter_t8_128 * L_12 = V_2;
		U3CSetParameterSynchronizedU3Ec__AnonStorey8_t8_135 * L_13 = V_1;
		NullCheck(L_13);
		String_t* L_14 = (L_13->___name_0);
		NullCheck(L_12);
		L_12->___Name_2 = L_14;
		SynchronizedParameter_t8_128 * L_15 = V_2;
		int32_t L_16 = ___type;
		NullCheck(L_15);
		L_15->___Type_0 = L_16;
		SynchronizedParameter_t8_128 * L_17 = V_2;
		int32_t L_18 = ___synchronizeType;
		NullCheck(L_17);
		L_17->___SynchronizeType_1 = L_18;
		SynchronizedParameter_t8_128 * L_19 = V_2;
		NullCheck(L_10);
		VirtActionInvoker1< SynchronizedParameter_t8_128 * >::Invoke(19 /* System.Void System.Collections.Generic.List`1<PhotonAnimatorView/SynchronizedParameter>::Add(!0) */, L_10, L_19);
		goto IL_0080;
	}

IL_006e:
	{
		List_1_t1_949 * L_20 = (__this->___m_SynchronizeParameters_6);
		int32_t L_21 = V_0;
		NullCheck(L_20);
		SynchronizedParameter_t8_128 * L_22 = (SynchronizedParameter_t8_128 *)VirtFuncInvoker1< SynchronizedParameter_t8_128 *, int32_t >::Invoke(28 /* !0 System.Collections.Generic.List`1<PhotonAnimatorView/SynchronizedParameter>::get_Item(System.Int32) */, L_20, L_21);
		int32_t L_23 = ___synchronizeType;
		NullCheck(L_22);
		L_22->___SynchronizeType_1 = L_23;
	}

IL_0080:
	{
		return;
	}
}
// System.Void PhotonAnimatorView::SerializeDataContinuously()
extern TypeInfo* Single_t1_17_il2cpp_TypeInfo_var;
extern TypeInfo* Boolean_t1_20_il2cpp_TypeInfo_var;
extern TypeInfo* Int32_t1_3_il2cpp_TypeInfo_var;
extern "C" void PhotonAnimatorView_SerializeDataContinuously_m8_880 (PhotonAnimatorView_t8_27 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Single_t1_17_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(43);
		Boolean_t1_20_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(49);
		Int32_t1_3_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(8);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	SynchronizedParameter_t8_128 * V_2 = {0};
	int32_t V_3 = {0};
	{
		Animator_t6_138 * L_0 = (__this->___m_Animator_2);
		bool L_1 = Object_op_Equality_m6_579(NULL /*static, unused*/, L_0, (Object_t6_5 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0012;
		}
	}
	{
		return;
	}

IL_0012:
	{
		V_0 = 0;
		goto IL_0060;
	}

IL_0019:
	{
		List_1_t1_950 * L_2 = (__this->___m_SynchronizeLayers_7);
		int32_t L_3 = V_0;
		NullCheck(L_2);
		SynchronizedLayer_t8_129 * L_4 = (SynchronizedLayer_t8_129 *)VirtFuncInvoker1< SynchronizedLayer_t8_129 *, int32_t >::Invoke(28 /* !0 System.Collections.Generic.List`1<PhotonAnimatorView/SynchronizedLayer>::get_Item(System.Int32) */, L_2, L_3);
		NullCheck(L_4);
		int32_t L_5 = (L_4->___SynchronizeType_0);
		if ((!(((uint32_t)L_5) == ((uint32_t)2))))
		{
			goto IL_005c;
		}
	}
	{
		PhotonStreamQueue_t8_117 * L_6 = (__this->___m_StreamQueue_3);
		Animator_t6_138 * L_7 = (__this->___m_Animator_2);
		List_1_t1_950 * L_8 = (__this->___m_SynchronizeLayers_7);
		int32_t L_9 = V_0;
		NullCheck(L_8);
		SynchronizedLayer_t8_129 * L_10 = (SynchronizedLayer_t8_129 *)VirtFuncInvoker1< SynchronizedLayer_t8_129 *, int32_t >::Invoke(28 /* !0 System.Collections.Generic.List`1<PhotonAnimatorView/SynchronizedLayer>::get_Item(System.Int32) */, L_8, L_9);
		NullCheck(L_10);
		int32_t L_11 = (L_10->___LayerIndex_1);
		NullCheck(L_7);
		float L_12 = Animator_GetLayerWeight_m6_842(L_7, L_11, /*hidden argument*/NULL);
		float L_13 = L_12;
		Object_t * L_14 = Box(Single_t1_17_il2cpp_TypeInfo_var, &L_13);
		NullCheck(L_6);
		PhotonStreamQueue_SendNext_m8_757(L_6, L_14, /*hidden argument*/NULL);
	}

IL_005c:
	{
		int32_t L_15 = V_0;
		V_0 = ((int32_t)((int32_t)L_15+(int32_t)1));
	}

IL_0060:
	{
		int32_t L_16 = V_0;
		List_1_t1_950 * L_17 = (__this->___m_SynchronizeLayers_7);
		NullCheck(L_17);
		int32_t L_18 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(17 /* System.Int32 System.Collections.Generic.List`1<PhotonAnimatorView/SynchronizedLayer>::get_Count() */, L_17);
		if ((((int32_t)L_16) < ((int32_t)L_18)))
		{
			goto IL_0019;
		}
	}
	{
		V_1 = 0;
		goto IL_0138;
	}

IL_0078:
	{
		List_1_t1_949 * L_19 = (__this->___m_SynchronizeParameters_6);
		int32_t L_20 = V_1;
		NullCheck(L_19);
		SynchronizedParameter_t8_128 * L_21 = (SynchronizedParameter_t8_128 *)VirtFuncInvoker1< SynchronizedParameter_t8_128 *, int32_t >::Invoke(28 /* !0 System.Collections.Generic.List`1<PhotonAnimatorView/SynchronizedParameter>::get_Item(System.Int32) */, L_19, L_20);
		V_2 = L_21;
		SynchronizedParameter_t8_128 * L_22 = V_2;
		NullCheck(L_22);
		int32_t L_23 = (L_22->___SynchronizeType_1);
		if ((!(((uint32_t)L_23) == ((uint32_t)2))))
		{
			goto IL_0134;
		}
	}
	{
		SynchronizedParameter_t8_128 * L_24 = V_2;
		NullCheck(L_24);
		int32_t L_25 = (L_24->___Type_0);
		V_3 = L_25;
		int32_t L_26 = V_3;
		if (((int32_t)((int32_t)L_26-(int32_t)1)) == 0)
		{
			goto IL_00e3;
		}
		if (((int32_t)((int32_t)L_26-(int32_t)1)) == 1)
		{
			goto IL_00b0;
		}
		if (((int32_t)((int32_t)L_26-(int32_t)1)) == 2)
		{
			goto IL_0109;
		}
		if (((int32_t)((int32_t)L_26-(int32_t)1)) == 3)
		{
			goto IL_00bd;
		}
	}

IL_00b0:
	{
		int32_t L_27 = V_3;
		if ((((int32_t)L_27) == ((int32_t)((int32_t)9))))
		{
			goto IL_012f;
		}
	}
	{
		goto IL_0134;
	}

IL_00bd:
	{
		PhotonStreamQueue_t8_117 * L_28 = (__this->___m_StreamQueue_3);
		Animator_t6_138 * L_29 = (__this->___m_Animator_2);
		SynchronizedParameter_t8_128 * L_30 = V_2;
		NullCheck(L_30);
		String_t* L_31 = (L_30->___Name_2);
		NullCheck(L_29);
		bool L_32 = Animator_GetBool_m6_835(L_29, L_31, /*hidden argument*/NULL);
		bool L_33 = L_32;
		Object_t * L_34 = Box(Boolean_t1_20_il2cpp_TypeInfo_var, &L_33);
		NullCheck(L_28);
		PhotonStreamQueue_SendNext_m8_757(L_28, L_34, /*hidden argument*/NULL);
		goto IL_0134;
	}

IL_00e3:
	{
		PhotonStreamQueue_t8_117 * L_35 = (__this->___m_StreamQueue_3);
		Animator_t6_138 * L_36 = (__this->___m_Animator_2);
		SynchronizedParameter_t8_128 * L_37 = V_2;
		NullCheck(L_37);
		String_t* L_38 = (L_37->___Name_2);
		NullCheck(L_36);
		float L_39 = Animator_GetFloat_m6_832(L_36, L_38, /*hidden argument*/NULL);
		float L_40 = L_39;
		Object_t * L_41 = Box(Single_t1_17_il2cpp_TypeInfo_var, &L_40);
		NullCheck(L_35);
		PhotonStreamQueue_SendNext_m8_757(L_35, L_41, /*hidden argument*/NULL);
		goto IL_0134;
	}

IL_0109:
	{
		PhotonStreamQueue_t8_117 * L_42 = (__this->___m_StreamQueue_3);
		Animator_t6_138 * L_43 = (__this->___m_Animator_2);
		SynchronizedParameter_t8_128 * L_44 = V_2;
		NullCheck(L_44);
		String_t* L_45 = (L_44->___Name_2);
		NullCheck(L_43);
		int32_t L_46 = Animator_GetInteger_m6_837(L_43, L_45, /*hidden argument*/NULL);
		int32_t L_47 = L_46;
		Object_t * L_48 = Box(Int32_t1_3_il2cpp_TypeInfo_var, &L_47);
		NullCheck(L_42);
		PhotonStreamQueue_SendNext_m8_757(L_42, L_48, /*hidden argument*/NULL);
		goto IL_0134;
	}

IL_012f:
	{
		goto IL_0134;
	}

IL_0134:
	{
		int32_t L_49 = V_1;
		V_1 = ((int32_t)((int32_t)L_49+(int32_t)1));
	}

IL_0138:
	{
		int32_t L_50 = V_1;
		List_1_t1_949 * L_51 = (__this->___m_SynchronizeParameters_6);
		NullCheck(L_51);
		int32_t L_52 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(17 /* System.Int32 System.Collections.Generic.List`1<PhotonAnimatorView/SynchronizedParameter>::get_Count() */, L_51);
		if ((((int32_t)L_50) < ((int32_t)L_52)))
		{
			goto IL_0078;
		}
	}
	{
		return;
	}
}
// System.Void PhotonAnimatorView::DeserializeDataContinuously()
extern TypeInfo* Single_t1_17_il2cpp_TypeInfo_var;
extern TypeInfo* Boolean_t1_20_il2cpp_TypeInfo_var;
extern TypeInfo* Int32_t1_3_il2cpp_TypeInfo_var;
extern "C" void PhotonAnimatorView_DeserializeDataContinuously_m8_881 (PhotonAnimatorView_t8_27 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Single_t1_17_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(43);
		Boolean_t1_20_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(49);
		Int32_t1_3_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(8);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	SynchronizedParameter_t8_128 * V_2 = {0};
	int32_t V_3 = {0};
	{
		PhotonStreamQueue_t8_117 * L_0 = (__this->___m_StreamQueue_3);
		NullCheck(L_0);
		bool L_1 = PhotonStreamQueue_HasQueuedObjects_m8_758(L_0, /*hidden argument*/NULL);
		if (L_1)
		{
			goto IL_0011;
		}
	}
	{
		return;
	}

IL_0011:
	{
		V_0 = 0;
		goto IL_005f;
	}

IL_0018:
	{
		List_1_t1_950 * L_2 = (__this->___m_SynchronizeLayers_7);
		int32_t L_3 = V_0;
		NullCheck(L_2);
		SynchronizedLayer_t8_129 * L_4 = (SynchronizedLayer_t8_129 *)VirtFuncInvoker1< SynchronizedLayer_t8_129 *, int32_t >::Invoke(28 /* !0 System.Collections.Generic.List`1<PhotonAnimatorView/SynchronizedLayer>::get_Item(System.Int32) */, L_2, L_3);
		NullCheck(L_4);
		int32_t L_5 = (L_4->___SynchronizeType_0);
		if ((!(((uint32_t)L_5) == ((uint32_t)2))))
		{
			goto IL_005b;
		}
	}
	{
		Animator_t6_138 * L_6 = (__this->___m_Animator_2);
		List_1_t1_950 * L_7 = (__this->___m_SynchronizeLayers_7);
		int32_t L_8 = V_0;
		NullCheck(L_7);
		SynchronizedLayer_t8_129 * L_9 = (SynchronizedLayer_t8_129 *)VirtFuncInvoker1< SynchronizedLayer_t8_129 *, int32_t >::Invoke(28 /* !0 System.Collections.Generic.List`1<PhotonAnimatorView/SynchronizedLayer>::get_Item(System.Int32) */, L_7, L_8);
		NullCheck(L_9);
		int32_t L_10 = (L_9->___LayerIndex_1);
		PhotonStreamQueue_t8_117 * L_11 = (__this->___m_StreamQueue_3);
		NullCheck(L_11);
		Object_t * L_12 = PhotonStreamQueue_ReceiveNext_m8_759(L_11, /*hidden argument*/NULL);
		NullCheck(L_6);
		Animator_SetLayerWeight_m6_843(L_6, L_10, ((*(float*)((float*)UnBox (L_12, Single_t1_17_il2cpp_TypeInfo_var)))), /*hidden argument*/NULL);
	}

IL_005b:
	{
		int32_t L_13 = V_0;
		V_0 = ((int32_t)((int32_t)L_13+(int32_t)1));
	}

IL_005f:
	{
		int32_t L_14 = V_0;
		List_1_t1_950 * L_15 = (__this->___m_SynchronizeLayers_7);
		NullCheck(L_15);
		int32_t L_16 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(17 /* System.Int32 System.Collections.Generic.List`1<PhotonAnimatorView/SynchronizedLayer>::get_Count() */, L_15);
		if ((((int32_t)L_14) < ((int32_t)L_16)))
		{
			goto IL_0018;
		}
	}
	{
		V_1 = 0;
		goto IL_0137;
	}

IL_0077:
	{
		List_1_t1_949 * L_17 = (__this->___m_SynchronizeParameters_6);
		int32_t L_18 = V_1;
		NullCheck(L_17);
		SynchronizedParameter_t8_128 * L_19 = (SynchronizedParameter_t8_128 *)VirtFuncInvoker1< SynchronizedParameter_t8_128 *, int32_t >::Invoke(28 /* !0 System.Collections.Generic.List`1<PhotonAnimatorView/SynchronizedParameter>::get_Item(System.Int32) */, L_17, L_18);
		V_2 = L_19;
		SynchronizedParameter_t8_128 * L_20 = V_2;
		NullCheck(L_20);
		int32_t L_21 = (L_20->___SynchronizeType_1);
		if ((!(((uint32_t)L_21) == ((uint32_t)2))))
		{
			goto IL_0133;
		}
	}
	{
		SynchronizedParameter_t8_128 * L_22 = V_2;
		NullCheck(L_22);
		int32_t L_23 = (L_22->___Type_0);
		V_3 = L_23;
		int32_t L_24 = V_3;
		if (((int32_t)((int32_t)L_24-(int32_t)1)) == 0)
		{
			goto IL_00e2;
		}
		if (((int32_t)((int32_t)L_24-(int32_t)1)) == 1)
		{
			goto IL_00af;
		}
		if (((int32_t)((int32_t)L_24-(int32_t)1)) == 2)
		{
			goto IL_0108;
		}
		if (((int32_t)((int32_t)L_24-(int32_t)1)) == 3)
		{
			goto IL_00bc;
		}
	}

IL_00af:
	{
		int32_t L_25 = V_3;
		if ((((int32_t)L_25) == ((int32_t)((int32_t)9))))
		{
			goto IL_012e;
		}
	}
	{
		goto IL_0133;
	}

IL_00bc:
	{
		Animator_t6_138 * L_26 = (__this->___m_Animator_2);
		SynchronizedParameter_t8_128 * L_27 = V_2;
		NullCheck(L_27);
		String_t* L_28 = (L_27->___Name_2);
		PhotonStreamQueue_t8_117 * L_29 = (__this->___m_StreamQueue_3);
		NullCheck(L_29);
		Object_t * L_30 = PhotonStreamQueue_ReceiveNext_m8_759(L_29, /*hidden argument*/NULL);
		NullCheck(L_26);
		Animator_SetBool_m6_836(L_26, L_28, ((*(bool*)((bool*)UnBox (L_30, Boolean_t1_20_il2cpp_TypeInfo_var)))), /*hidden argument*/NULL);
		goto IL_0133;
	}

IL_00e2:
	{
		Animator_t6_138 * L_31 = (__this->___m_Animator_2);
		SynchronizedParameter_t8_128 * L_32 = V_2;
		NullCheck(L_32);
		String_t* L_33 = (L_32->___Name_2);
		PhotonStreamQueue_t8_117 * L_34 = (__this->___m_StreamQueue_3);
		NullCheck(L_34);
		Object_t * L_35 = PhotonStreamQueue_ReceiveNext_m8_759(L_34, /*hidden argument*/NULL);
		NullCheck(L_31);
		Animator_SetFloat_m6_833(L_31, L_33, ((*(float*)((float*)UnBox (L_35, Single_t1_17_il2cpp_TypeInfo_var)))), /*hidden argument*/NULL);
		goto IL_0133;
	}

IL_0108:
	{
		Animator_t6_138 * L_36 = (__this->___m_Animator_2);
		SynchronizedParameter_t8_128 * L_37 = V_2;
		NullCheck(L_37);
		String_t* L_38 = (L_37->___Name_2);
		PhotonStreamQueue_t8_117 * L_39 = (__this->___m_StreamQueue_3);
		NullCheck(L_39);
		Object_t * L_40 = PhotonStreamQueue_ReceiveNext_m8_759(L_39, /*hidden argument*/NULL);
		NullCheck(L_36);
		Animator_SetInteger_m6_838(L_36, L_38, ((*(int32_t*)((int32_t*)UnBox (L_40, Int32_t1_3_il2cpp_TypeInfo_var)))), /*hidden argument*/NULL);
		goto IL_0133;
	}

IL_012e:
	{
		goto IL_0133;
	}

IL_0133:
	{
		int32_t L_41 = V_1;
		V_1 = ((int32_t)((int32_t)L_41+(int32_t)1));
	}

IL_0137:
	{
		int32_t L_42 = V_1;
		List_1_t1_949 * L_43 = (__this->___m_SynchronizeParameters_6);
		NullCheck(L_43);
		int32_t L_44 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(17 /* System.Int32 System.Collections.Generic.List`1<PhotonAnimatorView/SynchronizedParameter>::get_Count() */, L_43);
		if ((((int32_t)L_42) < ((int32_t)L_44)))
		{
			goto IL_0077;
		}
	}
	{
		return;
	}
}
// System.Void PhotonAnimatorView::SerializeDataDiscretly(PhotonStream)
extern TypeInfo* Single_t1_17_il2cpp_TypeInfo_var;
extern TypeInfo* Boolean_t1_20_il2cpp_TypeInfo_var;
extern TypeInfo* Int32_t1_3_il2cpp_TypeInfo_var;
extern "C" void PhotonAnimatorView_SerializeDataDiscretly_m8_882 (PhotonAnimatorView_t8_27 * __this, PhotonStream_t8_106 * ___stream, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Single_t1_17_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(43);
		Boolean_t1_20_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(49);
		Int32_t1_3_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(8);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	SynchronizedParameter_t8_128 * V_2 = {0};
	int32_t V_3 = {0};
	{
		V_0 = 0;
		goto IL_0049;
	}

IL_0007:
	{
		List_1_t1_950 * L_0 = (__this->___m_SynchronizeLayers_7);
		int32_t L_1 = V_0;
		NullCheck(L_0);
		SynchronizedLayer_t8_129 * L_2 = (SynchronizedLayer_t8_129 *)VirtFuncInvoker1< SynchronizedLayer_t8_129 *, int32_t >::Invoke(28 /* !0 System.Collections.Generic.List`1<PhotonAnimatorView/SynchronizedLayer>::get_Item(System.Int32) */, L_0, L_1);
		NullCheck(L_2);
		int32_t L_3 = (L_2->___SynchronizeType_0);
		if ((!(((uint32_t)L_3) == ((uint32_t)1))))
		{
			goto IL_0045;
		}
	}
	{
		PhotonStream_t8_106 * L_4 = ___stream;
		Animator_t6_138 * L_5 = (__this->___m_Animator_2);
		List_1_t1_950 * L_6 = (__this->___m_SynchronizeLayers_7);
		int32_t L_7 = V_0;
		NullCheck(L_6);
		SynchronizedLayer_t8_129 * L_8 = (SynchronizedLayer_t8_129 *)VirtFuncInvoker1< SynchronizedLayer_t8_129 *, int32_t >::Invoke(28 /* !0 System.Collections.Generic.List`1<PhotonAnimatorView/SynchronizedLayer>::get_Item(System.Int32) */, L_6, L_7);
		NullCheck(L_8);
		int32_t L_9 = (L_8->___LayerIndex_1);
		NullCheck(L_5);
		float L_10 = Animator_GetLayerWeight_m6_842(L_5, L_9, /*hidden argument*/NULL);
		float L_11 = L_10;
		Object_t * L_12 = Box(Single_t1_17_il2cpp_TypeInfo_var, &L_11);
		NullCheck(L_4);
		PhotonStream_SendNext_m8_543(L_4, L_12, /*hidden argument*/NULL);
	}

IL_0045:
	{
		int32_t L_13 = V_0;
		V_0 = ((int32_t)((int32_t)L_13+(int32_t)1));
	}

IL_0049:
	{
		int32_t L_14 = V_0;
		List_1_t1_950 * L_15 = (__this->___m_SynchronizeLayers_7);
		NullCheck(L_15);
		int32_t L_16 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(17 /* System.Int32 System.Collections.Generic.List`1<PhotonAnimatorView/SynchronizedLayer>::get_Count() */, L_15);
		if ((((int32_t)L_14) < ((int32_t)L_16)))
		{
			goto IL_0007;
		}
	}
	{
		V_1 = 0;
		goto IL_0112;
	}

IL_0061:
	{
		List_1_t1_949 * L_17 = (__this->___m_SynchronizeParameters_6);
		int32_t L_18 = V_1;
		NullCheck(L_17);
		SynchronizedParameter_t8_128 * L_19 = (SynchronizedParameter_t8_128 *)VirtFuncInvoker1< SynchronizedParameter_t8_128 *, int32_t >::Invoke(28 /* !0 System.Collections.Generic.List`1<PhotonAnimatorView/SynchronizedParameter>::get_Item(System.Int32) */, L_17, L_18);
		V_2 = L_19;
		SynchronizedParameter_t8_128 * L_20 = V_2;
		NullCheck(L_20);
		int32_t L_21 = (L_20->___SynchronizeType_1);
		if ((!(((uint32_t)L_21) == ((uint32_t)1))))
		{
			goto IL_010e;
		}
	}
	{
		SynchronizedParameter_t8_128 * L_22 = V_2;
		NullCheck(L_22);
		int32_t L_23 = (L_22->___Type_0);
		V_3 = L_23;
		int32_t L_24 = V_3;
		if (((int32_t)((int32_t)L_24-(int32_t)1)) == 0)
		{
			goto IL_00c7;
		}
		if (((int32_t)((int32_t)L_24-(int32_t)1)) == 1)
		{
			goto IL_0099;
		}
		if (((int32_t)((int32_t)L_24-(int32_t)1)) == 2)
		{
			goto IL_00e8;
		}
		if (((int32_t)((int32_t)L_24-(int32_t)1)) == 3)
		{
			goto IL_00a6;
		}
	}

IL_0099:
	{
		int32_t L_25 = V_3;
		if ((((int32_t)L_25) == ((int32_t)((int32_t)9))))
		{
			goto IL_0109;
		}
	}
	{
		goto IL_010e;
	}

IL_00a6:
	{
		PhotonStream_t8_106 * L_26 = ___stream;
		Animator_t6_138 * L_27 = (__this->___m_Animator_2);
		SynchronizedParameter_t8_128 * L_28 = V_2;
		NullCheck(L_28);
		String_t* L_29 = (L_28->___Name_2);
		NullCheck(L_27);
		bool L_30 = Animator_GetBool_m6_835(L_27, L_29, /*hidden argument*/NULL);
		bool L_31 = L_30;
		Object_t * L_32 = Box(Boolean_t1_20_il2cpp_TypeInfo_var, &L_31);
		NullCheck(L_26);
		PhotonStream_SendNext_m8_543(L_26, L_32, /*hidden argument*/NULL);
		goto IL_010e;
	}

IL_00c7:
	{
		PhotonStream_t8_106 * L_33 = ___stream;
		Animator_t6_138 * L_34 = (__this->___m_Animator_2);
		SynchronizedParameter_t8_128 * L_35 = V_2;
		NullCheck(L_35);
		String_t* L_36 = (L_35->___Name_2);
		NullCheck(L_34);
		float L_37 = Animator_GetFloat_m6_832(L_34, L_36, /*hidden argument*/NULL);
		float L_38 = L_37;
		Object_t * L_39 = Box(Single_t1_17_il2cpp_TypeInfo_var, &L_38);
		NullCheck(L_33);
		PhotonStream_SendNext_m8_543(L_33, L_39, /*hidden argument*/NULL);
		goto IL_010e;
	}

IL_00e8:
	{
		PhotonStream_t8_106 * L_40 = ___stream;
		Animator_t6_138 * L_41 = (__this->___m_Animator_2);
		SynchronizedParameter_t8_128 * L_42 = V_2;
		NullCheck(L_42);
		String_t* L_43 = (L_42->___Name_2);
		NullCheck(L_41);
		int32_t L_44 = Animator_GetInteger_m6_837(L_41, L_43, /*hidden argument*/NULL);
		int32_t L_45 = L_44;
		Object_t * L_46 = Box(Int32_t1_3_il2cpp_TypeInfo_var, &L_45);
		NullCheck(L_40);
		PhotonStream_SendNext_m8_543(L_40, L_46, /*hidden argument*/NULL);
		goto IL_010e;
	}

IL_0109:
	{
		goto IL_010e;
	}

IL_010e:
	{
		int32_t L_47 = V_1;
		V_1 = ((int32_t)((int32_t)L_47+(int32_t)1));
	}

IL_0112:
	{
		int32_t L_48 = V_1;
		List_1_t1_949 * L_49 = (__this->___m_SynchronizeParameters_6);
		NullCheck(L_49);
		int32_t L_50 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(17 /* System.Int32 System.Collections.Generic.List`1<PhotonAnimatorView/SynchronizedParameter>::get_Count() */, L_49);
		if ((((int32_t)L_48) < ((int32_t)L_50)))
		{
			goto IL_0061;
		}
	}
	{
		return;
	}
}
// System.Void PhotonAnimatorView::DeserializeDataDiscretly(PhotonStream)
extern TypeInfo* Single_t1_17_il2cpp_TypeInfo_var;
extern TypeInfo* Boolean_t1_20_il2cpp_TypeInfo_var;
extern TypeInfo* Int32_t1_3_il2cpp_TypeInfo_var;
extern "C" void PhotonAnimatorView_DeserializeDataDiscretly_m8_883 (PhotonAnimatorView_t8_27 * __this, PhotonStream_t8_106 * ___stream, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Single_t1_17_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(43);
		Boolean_t1_20_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(49);
		Int32_t1_3_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(8);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	SynchronizedParameter_t8_128 * V_2 = {0};
	int32_t V_3 = {0};
	{
		V_0 = 0;
		goto IL_0049;
	}

IL_0007:
	{
		List_1_t1_950 * L_0 = (__this->___m_SynchronizeLayers_7);
		int32_t L_1 = V_0;
		NullCheck(L_0);
		SynchronizedLayer_t8_129 * L_2 = (SynchronizedLayer_t8_129 *)VirtFuncInvoker1< SynchronizedLayer_t8_129 *, int32_t >::Invoke(28 /* !0 System.Collections.Generic.List`1<PhotonAnimatorView/SynchronizedLayer>::get_Item(System.Int32) */, L_0, L_1);
		NullCheck(L_2);
		int32_t L_3 = (L_2->___SynchronizeType_0);
		if ((!(((uint32_t)L_3) == ((uint32_t)1))))
		{
			goto IL_0045;
		}
	}
	{
		Animator_t6_138 * L_4 = (__this->___m_Animator_2);
		List_1_t1_950 * L_5 = (__this->___m_SynchronizeLayers_7);
		int32_t L_6 = V_0;
		NullCheck(L_5);
		SynchronizedLayer_t8_129 * L_7 = (SynchronizedLayer_t8_129 *)VirtFuncInvoker1< SynchronizedLayer_t8_129 *, int32_t >::Invoke(28 /* !0 System.Collections.Generic.List`1<PhotonAnimatorView/SynchronizedLayer>::get_Item(System.Int32) */, L_5, L_6);
		NullCheck(L_7);
		int32_t L_8 = (L_7->___LayerIndex_1);
		PhotonStream_t8_106 * L_9 = ___stream;
		NullCheck(L_9);
		Object_t * L_10 = PhotonStream_ReceiveNext_m8_541(L_9, /*hidden argument*/NULL);
		NullCheck(L_4);
		Animator_SetLayerWeight_m6_843(L_4, L_8, ((*(float*)((float*)UnBox (L_10, Single_t1_17_il2cpp_TypeInfo_var)))), /*hidden argument*/NULL);
	}

IL_0045:
	{
		int32_t L_11 = V_0;
		V_0 = ((int32_t)((int32_t)L_11+(int32_t)1));
	}

IL_0049:
	{
		int32_t L_12 = V_0;
		List_1_t1_950 * L_13 = (__this->___m_SynchronizeLayers_7);
		NullCheck(L_13);
		int32_t L_14 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(17 /* System.Int32 System.Collections.Generic.List`1<PhotonAnimatorView/SynchronizedLayer>::get_Count() */, L_13);
		if ((((int32_t)L_12) < ((int32_t)L_14)))
		{
			goto IL_0007;
		}
	}
	{
		V_1 = 0;
		goto IL_0145;
	}

IL_0061:
	{
		List_1_t1_949 * L_15 = (__this->___m_SynchronizeParameters_6);
		int32_t L_16 = V_1;
		NullCheck(L_15);
		SynchronizedParameter_t8_128 * L_17 = (SynchronizedParameter_t8_128 *)VirtFuncInvoker1< SynchronizedParameter_t8_128 *, int32_t >::Invoke(28 /* !0 System.Collections.Generic.List`1<PhotonAnimatorView/SynchronizedParameter>::get_Item(System.Int32) */, L_15, L_16);
		V_2 = L_17;
		SynchronizedParameter_t8_128 * L_18 = V_2;
		NullCheck(L_18);
		int32_t L_19 = (L_18->___SynchronizeType_1);
		if ((!(((uint32_t)L_19) == ((uint32_t)1))))
		{
			goto IL_0141;
		}
	}
	{
		SynchronizedParameter_t8_128 * L_20 = V_2;
		NullCheck(L_20);
		int32_t L_21 = (L_20->___Type_0);
		V_3 = L_21;
		int32_t L_22 = V_3;
		if (((int32_t)((int32_t)L_22-(int32_t)1)) == 0)
		{
			goto IL_00d8;
		}
		if (((int32_t)((int32_t)L_22-(int32_t)1)) == 1)
		{
			goto IL_0099;
		}
		if (((int32_t)((int32_t)L_22-(int32_t)1)) == 2)
		{
			goto IL_010a;
		}
		if (((int32_t)((int32_t)L_22-(int32_t)1)) == 3)
		{
			goto IL_00a6;
		}
	}

IL_0099:
	{
		int32_t L_23 = V_3;
		if ((((int32_t)L_23) == ((int32_t)((int32_t)9))))
		{
			goto IL_013c;
		}
	}
	{
		goto IL_0141;
	}

IL_00a6:
	{
		PhotonStream_t8_106 * L_24 = ___stream;
		NullCheck(L_24);
		Object_t * L_25 = PhotonStream_PeekNext_m8_542(L_24, /*hidden argument*/NULL);
		if (((Object_t *)IsInstSealed(L_25, Boolean_t1_20_il2cpp_TypeInfo_var)))
		{
			goto IL_00b7;
		}
	}
	{
		return;
	}

IL_00b7:
	{
		Animator_t6_138 * L_26 = (__this->___m_Animator_2);
		SynchronizedParameter_t8_128 * L_27 = V_2;
		NullCheck(L_27);
		String_t* L_28 = (L_27->___Name_2);
		PhotonStream_t8_106 * L_29 = ___stream;
		NullCheck(L_29);
		Object_t * L_30 = PhotonStream_ReceiveNext_m8_541(L_29, /*hidden argument*/NULL);
		NullCheck(L_26);
		Animator_SetBool_m6_836(L_26, L_28, ((*(bool*)((bool*)UnBox (L_30, Boolean_t1_20_il2cpp_TypeInfo_var)))), /*hidden argument*/NULL);
		goto IL_0141;
	}

IL_00d8:
	{
		PhotonStream_t8_106 * L_31 = ___stream;
		NullCheck(L_31);
		Object_t * L_32 = PhotonStream_PeekNext_m8_542(L_31, /*hidden argument*/NULL);
		if (((Object_t *)IsInstSealed(L_32, Single_t1_17_il2cpp_TypeInfo_var)))
		{
			goto IL_00e9;
		}
	}
	{
		return;
	}

IL_00e9:
	{
		Animator_t6_138 * L_33 = (__this->___m_Animator_2);
		SynchronizedParameter_t8_128 * L_34 = V_2;
		NullCheck(L_34);
		String_t* L_35 = (L_34->___Name_2);
		PhotonStream_t8_106 * L_36 = ___stream;
		NullCheck(L_36);
		Object_t * L_37 = PhotonStream_ReceiveNext_m8_541(L_36, /*hidden argument*/NULL);
		NullCheck(L_33);
		Animator_SetFloat_m6_833(L_33, L_35, ((*(float*)((float*)UnBox (L_37, Single_t1_17_il2cpp_TypeInfo_var)))), /*hidden argument*/NULL);
		goto IL_0141;
	}

IL_010a:
	{
		PhotonStream_t8_106 * L_38 = ___stream;
		NullCheck(L_38);
		Object_t * L_39 = PhotonStream_PeekNext_m8_542(L_38, /*hidden argument*/NULL);
		if (((Object_t *)IsInstSealed(L_39, Int32_t1_3_il2cpp_TypeInfo_var)))
		{
			goto IL_011b;
		}
	}
	{
		return;
	}

IL_011b:
	{
		Animator_t6_138 * L_40 = (__this->___m_Animator_2);
		SynchronizedParameter_t8_128 * L_41 = V_2;
		NullCheck(L_41);
		String_t* L_42 = (L_41->___Name_2);
		PhotonStream_t8_106 * L_43 = ___stream;
		NullCheck(L_43);
		Object_t * L_44 = PhotonStream_ReceiveNext_m8_541(L_43, /*hidden argument*/NULL);
		NullCheck(L_40);
		Animator_SetInteger_m6_838(L_40, L_42, ((*(int32_t*)((int32_t*)UnBox (L_44, Int32_t1_3_il2cpp_TypeInfo_var)))), /*hidden argument*/NULL);
		goto IL_0141;
	}

IL_013c:
	{
		goto IL_0141;
	}

IL_0141:
	{
		int32_t L_45 = V_1;
		V_1 = ((int32_t)((int32_t)L_45+(int32_t)1));
	}

IL_0145:
	{
		int32_t L_46 = V_1;
		List_1_t1_949 * L_47 = (__this->___m_SynchronizeParameters_6);
		NullCheck(L_47);
		int32_t L_48 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(17 /* System.Int32 System.Collections.Generic.List`1<PhotonAnimatorView/SynchronizedParameter>::get_Count() */, L_47);
		if ((((int32_t)L_46) < ((int32_t)L_48)))
		{
			goto IL_0061;
		}
	}
	{
		return;
	}
}
// System.Void PhotonAnimatorView::SerializeSynchronizationTypeState(PhotonStream)
extern TypeInfo* ByteU5BU5D_t1_71_il2cpp_TypeInfo_var;
extern "C" void PhotonAnimatorView_SerializeSynchronizationTypeState_m8_884 (PhotonAnimatorView_t8_27 * __this, PhotonStream_t8_106 * ___stream, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ByteU5BU5D_t1_71_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(42);
		s_Il2CppMethodIntialized = true;
	}
	ByteU5BU5D_t1_71* V_0 = {0};
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	{
		List_1_t1_950 * L_0 = (__this->___m_SynchronizeLayers_7);
		NullCheck(L_0);
		int32_t L_1 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(17 /* System.Int32 System.Collections.Generic.List`1<PhotonAnimatorView/SynchronizedLayer>::get_Count() */, L_0);
		List_1_t1_949 * L_2 = (__this->___m_SynchronizeParameters_6);
		NullCheck(L_2);
		int32_t L_3 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(17 /* System.Int32 System.Collections.Generic.List`1<PhotonAnimatorView/SynchronizedParameter>::get_Count() */, L_2);
		V_0 = ((ByteU5BU5D_t1_71*)SZArrayNew(ByteU5BU5D_t1_71_il2cpp_TypeInfo_var, ((int32_t)((int32_t)L_1+(int32_t)L_3))));
		V_1 = 0;
		goto IL_003d;
	}

IL_0024:
	{
		ByteU5BU5D_t1_71* L_4 = V_0;
		int32_t L_5 = V_1;
		List_1_t1_950 * L_6 = (__this->___m_SynchronizeLayers_7);
		int32_t L_7 = V_1;
		NullCheck(L_6);
		SynchronizedLayer_t8_129 * L_8 = (SynchronizedLayer_t8_129 *)VirtFuncInvoker1< SynchronizedLayer_t8_129 *, int32_t >::Invoke(28 /* !0 System.Collections.Generic.List`1<PhotonAnimatorView/SynchronizedLayer>::get_Item(System.Int32) */, L_6, L_7);
		NullCheck(L_8);
		int32_t L_9 = (L_8->___SynchronizeType_0);
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, L_5);
		*((uint8_t*)(uint8_t*)SZArrayLdElema(L_4, L_5, sizeof(uint8_t))) = (uint8_t)(((int32_t)((uint8_t)L_9)));
		int32_t L_10 = V_1;
		V_1 = ((int32_t)((int32_t)L_10+(int32_t)1));
	}

IL_003d:
	{
		int32_t L_11 = V_1;
		List_1_t1_950 * L_12 = (__this->___m_SynchronizeLayers_7);
		NullCheck(L_12);
		int32_t L_13 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(17 /* System.Int32 System.Collections.Generic.List`1<PhotonAnimatorView/SynchronizedLayer>::get_Count() */, L_12);
		if ((((int32_t)L_11) < ((int32_t)L_13)))
		{
			goto IL_0024;
		}
	}
	{
		V_2 = 0;
		goto IL_007a;
	}

IL_0055:
	{
		ByteU5BU5D_t1_71* L_14 = V_0;
		List_1_t1_950 * L_15 = (__this->___m_SynchronizeLayers_7);
		NullCheck(L_15);
		int32_t L_16 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(17 /* System.Int32 System.Collections.Generic.List`1<PhotonAnimatorView/SynchronizedLayer>::get_Count() */, L_15);
		int32_t L_17 = V_2;
		List_1_t1_949 * L_18 = (__this->___m_SynchronizeParameters_6);
		int32_t L_19 = V_2;
		NullCheck(L_18);
		SynchronizedParameter_t8_128 * L_20 = (SynchronizedParameter_t8_128 *)VirtFuncInvoker1< SynchronizedParameter_t8_128 *, int32_t >::Invoke(28 /* !0 System.Collections.Generic.List`1<PhotonAnimatorView/SynchronizedParameter>::get_Item(System.Int32) */, L_18, L_19);
		NullCheck(L_20);
		int32_t L_21 = (L_20->___SynchronizeType_1);
		NullCheck(L_14);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_14, ((int32_t)((int32_t)L_16+(int32_t)L_17)));
		*((uint8_t*)(uint8_t*)SZArrayLdElema(L_14, ((int32_t)((int32_t)L_16+(int32_t)L_17)), sizeof(uint8_t))) = (uint8_t)(((int32_t)((uint8_t)L_21)));
		int32_t L_22 = V_2;
		V_2 = ((int32_t)((int32_t)L_22+(int32_t)1));
	}

IL_007a:
	{
		int32_t L_23 = V_2;
		List_1_t1_949 * L_24 = (__this->___m_SynchronizeParameters_6);
		NullCheck(L_24);
		int32_t L_25 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(17 /* System.Int32 System.Collections.Generic.List`1<PhotonAnimatorView/SynchronizedParameter>::get_Count() */, L_24);
		if ((((int32_t)L_23) < ((int32_t)L_25)))
		{
			goto IL_0055;
		}
	}
	{
		PhotonStream_t8_106 * L_26 = ___stream;
		ByteU5BU5D_t1_71* L_27 = V_0;
		NullCheck(L_26);
		PhotonStream_SendNext_m8_543(L_26, (Object_t *)(Object_t *)L_27, /*hidden argument*/NULL);
		return;
	}
}
// System.Void PhotonAnimatorView::DeserializeSynchronizationTypeState(PhotonStream)
extern TypeInfo* ByteU5BU5D_t1_71_il2cpp_TypeInfo_var;
extern "C" void PhotonAnimatorView_DeserializeSynchronizationTypeState_m8_885 (PhotonAnimatorView_t8_27 * __this, PhotonStream_t8_106 * ___stream, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ByteU5BU5D_t1_71_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(42);
		s_Il2CppMethodIntialized = true;
	}
	ByteU5BU5D_t1_71* V_0 = {0};
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	{
		PhotonStream_t8_106 * L_0 = ___stream;
		NullCheck(L_0);
		Object_t * L_1 = PhotonStream_ReceiveNext_m8_541(L_0, /*hidden argument*/NULL);
		V_0 = ((ByteU5BU5D_t1_71*)Castclass(L_1, ByteU5BU5D_t1_71_il2cpp_TypeInfo_var));
		V_1 = 0;
		goto IL_002b;
	}

IL_0013:
	{
		List_1_t1_950 * L_2 = (__this->___m_SynchronizeLayers_7);
		int32_t L_3 = V_1;
		NullCheck(L_2);
		SynchronizedLayer_t8_129 * L_4 = (SynchronizedLayer_t8_129 *)VirtFuncInvoker1< SynchronizedLayer_t8_129 *, int32_t >::Invoke(28 /* !0 System.Collections.Generic.List`1<PhotonAnimatorView/SynchronizedLayer>::get_Item(System.Int32) */, L_2, L_3);
		ByteU5BU5D_t1_71* L_5 = V_0;
		int32_t L_6 = V_1;
		NullCheck(L_5);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_5, L_6);
		int32_t L_7 = L_6;
		NullCheck(L_4);
		L_4->___SynchronizeType_0 = (*(uint8_t*)(uint8_t*)SZArrayLdElema(L_5, L_7, sizeof(uint8_t)));
		int32_t L_8 = V_1;
		V_1 = ((int32_t)((int32_t)L_8+(int32_t)1));
	}

IL_002b:
	{
		int32_t L_9 = V_1;
		List_1_t1_950 * L_10 = (__this->___m_SynchronizeLayers_7);
		NullCheck(L_10);
		int32_t L_11 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(17 /* System.Int32 System.Collections.Generic.List`1<PhotonAnimatorView/SynchronizedLayer>::get_Count() */, L_10);
		if ((((int32_t)L_9) < ((int32_t)L_11)))
		{
			goto IL_0013;
		}
	}
	{
		V_2 = 0;
		goto IL_0067;
	}

IL_0043:
	{
		List_1_t1_949 * L_12 = (__this->___m_SynchronizeParameters_6);
		int32_t L_13 = V_2;
		NullCheck(L_12);
		SynchronizedParameter_t8_128 * L_14 = (SynchronizedParameter_t8_128 *)VirtFuncInvoker1< SynchronizedParameter_t8_128 *, int32_t >::Invoke(28 /* !0 System.Collections.Generic.List`1<PhotonAnimatorView/SynchronizedParameter>::get_Item(System.Int32) */, L_12, L_13);
		ByteU5BU5D_t1_71* L_15 = V_0;
		List_1_t1_950 * L_16 = (__this->___m_SynchronizeLayers_7);
		NullCheck(L_16);
		int32_t L_17 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(17 /* System.Int32 System.Collections.Generic.List`1<PhotonAnimatorView/SynchronizedLayer>::get_Count() */, L_16);
		int32_t L_18 = V_2;
		NullCheck(L_15);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_15, ((int32_t)((int32_t)L_17+(int32_t)L_18)));
		int32_t L_19 = ((int32_t)((int32_t)L_17+(int32_t)L_18));
		NullCheck(L_14);
		L_14->___SynchronizeType_1 = (*(uint8_t*)(uint8_t*)SZArrayLdElema(L_15, L_19, sizeof(uint8_t)));
		int32_t L_20 = V_2;
		V_2 = ((int32_t)((int32_t)L_20+(int32_t)1));
	}

IL_0067:
	{
		int32_t L_21 = V_2;
		List_1_t1_949 * L_22 = (__this->___m_SynchronizeParameters_6);
		NullCheck(L_22);
		int32_t L_23 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(17 /* System.Int32 System.Collections.Generic.List`1<PhotonAnimatorView/SynchronizedParameter>::get_Count() */, L_22);
		if ((((int32_t)L_21) < ((int32_t)L_23)))
		{
			goto IL_0043;
		}
	}
	{
		return;
	}
}
// System.Void PhotonAnimatorView::OnPhotonSerializeView(PhotonStream,PhotonMessageInfo)
extern TypeInfo* ByteU5BU5D_t1_71_il2cpp_TypeInfo_var;
extern "C" void PhotonAnimatorView_OnPhotonSerializeView_m8_886 (PhotonAnimatorView_t8_27 * __this, PhotonStream_t8_106 * ___stream, PhotonMessageInfo_t8_104 * ___info, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ByteU5BU5D_t1_71_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(42);
		s_Il2CppMethodIntialized = true;
	}
	{
		Animator_t6_138 * L_0 = (__this->___m_Animator_2);
		bool L_1 = Object_op_Equality_m6_579(NULL /*static, unused*/, L_0, (Object_t6_5 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0012;
		}
	}
	{
		return;
	}

IL_0012:
	{
		PhotonStream_t8_106 * L_2 = ___stream;
		NullCheck(L_2);
		bool L_3 = PhotonStream_get_isWriting_m8_538(L_2, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_0059;
		}
	}
	{
		bool L_4 = (__this->___m_WasSynchronizeTypeChanged_10);
		if (!L_4)
		{
			goto IL_0041;
		}
	}
	{
		PhotonStreamQueue_t8_117 * L_5 = (__this->___m_StreamQueue_3);
		NullCheck(L_5);
		PhotonStreamQueue_Reset_m8_756(L_5, /*hidden argument*/NULL);
		PhotonStream_t8_106 * L_6 = ___stream;
		PhotonAnimatorView_SerializeSynchronizationTypeState_m8_884(__this, L_6, /*hidden argument*/NULL);
		__this->___m_WasSynchronizeTypeChanged_10 = 0;
	}

IL_0041:
	{
		PhotonStreamQueue_t8_117 * L_7 = (__this->___m_StreamQueue_3);
		PhotonStream_t8_106 * L_8 = ___stream;
		NullCheck(L_7);
		PhotonStreamQueue_Serialize_m8_760(L_7, L_8, /*hidden argument*/NULL);
		PhotonStream_t8_106 * L_9 = ___stream;
		PhotonAnimatorView_SerializeDataDiscretly_m8_882(__this, L_9, /*hidden argument*/NULL);
		goto IL_0083;
	}

IL_0059:
	{
		PhotonStream_t8_106 * L_10 = ___stream;
		NullCheck(L_10);
		Object_t * L_11 = PhotonStream_PeekNext_m8_542(L_10, /*hidden argument*/NULL);
		if (!((ByteU5BU5D_t1_71*)IsInst(L_11, ByteU5BU5D_t1_71_il2cpp_TypeInfo_var)))
		{
			goto IL_0070;
		}
	}
	{
		PhotonStream_t8_106 * L_12 = ___stream;
		PhotonAnimatorView_DeserializeSynchronizationTypeState_m8_885(__this, L_12, /*hidden argument*/NULL);
	}

IL_0070:
	{
		PhotonStreamQueue_t8_117 * L_13 = (__this->___m_StreamQueue_3);
		PhotonStream_t8_106 * L_14 = ___stream;
		NullCheck(L_13);
		PhotonStreamQueue_Deserialize_m8_761(L_13, L_14, /*hidden argument*/NULL);
		PhotonStream_t8_106 * L_15 = ___stream;
		PhotonAnimatorView_DeserializeDataDiscretly_m8_883(__this, L_15, /*hidden argument*/NULL);
	}

IL_0083:
	{
		return;
	}
}
// System.Void PhotonRigidbody2DView::.ctor()
extern "C" void PhotonRigidbody2DView__ctor_m8_887 (PhotonRigidbody2DView_t8_136 * __this, const MethodInfo* method)
{
	{
		__this->___m_SynchronizeVelocity_2 = 1;
		__this->___m_SynchronizeAngularVelocity_3 = 1;
		MonoBehaviour__ctor_m6_525(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void PhotonRigidbody2DView::Awake()
extern const MethodInfo* Component_GetComponent_TisRigidbody2D_t6_113_m6_1657_MethodInfo_var;
extern "C" void PhotonRigidbody2DView_Awake_m8_888 (PhotonRigidbody2DView_t8_136 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Component_GetComponent_TisRigidbody2D_t6_113_m6_1657_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483835);
		s_Il2CppMethodIntialized = true;
	}
	{
		Rigidbody2D_t6_113 * L_0 = Component_GetComponent_TisRigidbody2D_t6_113_m6_1657(__this, /*hidden argument*/Component_GetComponent_TisRigidbody2D_t6_113_m6_1657_MethodInfo_var);
		__this->___m_Body_4 = L_0;
		return;
	}
}
// System.Void PhotonRigidbody2DView::OnPhotonSerializeView(PhotonStream,PhotonMessageInfo)
extern TypeInfo* Vector2_t6_48_il2cpp_TypeInfo_var;
extern TypeInfo* Single_t1_17_il2cpp_TypeInfo_var;
extern "C" void PhotonRigidbody2DView_OnPhotonSerializeView_m8_889 (PhotonRigidbody2DView_t8_136 * __this, PhotonStream_t8_106 * ___stream, PhotonMessageInfo_t8_104 * ___info, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Vector2_t6_48_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(939);
		Single_t1_17_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(43);
		s_Il2CppMethodIntialized = true;
	}
	{
		PhotonStream_t8_106 * L_0 = ___stream;
		NullCheck(L_0);
		bool L_1 = PhotonStream_get_isWriting_m8_538(L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0052;
		}
	}
	{
		bool L_2 = (__this->___m_SynchronizeVelocity_2);
		if (!L_2)
		{
			goto IL_002c;
		}
	}
	{
		PhotonStream_t8_106 * L_3 = ___stream;
		Rigidbody2D_t6_113 * L_4 = (__this->___m_Body_4);
		NullCheck(L_4);
		Vector2_t6_48  L_5 = Rigidbody2D_get_velocity_m6_740(L_4, /*hidden argument*/NULL);
		Vector2_t6_48  L_6 = L_5;
		Object_t * L_7 = Box(Vector2_t6_48_il2cpp_TypeInfo_var, &L_6);
		NullCheck(L_3);
		PhotonStream_SendNext_m8_543(L_3, L_7, /*hidden argument*/NULL);
	}

IL_002c:
	{
		bool L_8 = (__this->___m_SynchronizeAngularVelocity_3);
		if (!L_8)
		{
			goto IL_004d;
		}
	}
	{
		PhotonStream_t8_106 * L_9 = ___stream;
		Rigidbody2D_t6_113 * L_10 = (__this->___m_Body_4);
		NullCheck(L_10);
		float L_11 = Rigidbody2D_get_angularVelocity_m6_744(L_10, /*hidden argument*/NULL);
		float L_12 = L_11;
		Object_t * L_13 = Box(Single_t1_17_il2cpp_TypeInfo_var, &L_12);
		NullCheck(L_9);
		PhotonStream_SendNext_m8_543(L_9, L_13, /*hidden argument*/NULL);
	}

IL_004d:
	{
		goto IL_0094;
	}

IL_0052:
	{
		bool L_14 = (__this->___m_SynchronizeVelocity_2);
		if (!L_14)
		{
			goto IL_0073;
		}
	}
	{
		Rigidbody2D_t6_113 * L_15 = (__this->___m_Body_4);
		PhotonStream_t8_106 * L_16 = ___stream;
		NullCheck(L_16);
		Object_t * L_17 = PhotonStream_ReceiveNext_m8_541(L_16, /*hidden argument*/NULL);
		NullCheck(L_15);
		Rigidbody2D_set_velocity_m6_741(L_15, ((*(Vector2_t6_48 *)((Vector2_t6_48 *)UnBox (L_17, Vector2_t6_48_il2cpp_TypeInfo_var)))), /*hidden argument*/NULL);
	}

IL_0073:
	{
		bool L_18 = (__this->___m_SynchronizeAngularVelocity_3);
		if (!L_18)
		{
			goto IL_0094;
		}
	}
	{
		Rigidbody2D_t6_113 * L_19 = (__this->___m_Body_4);
		PhotonStream_t8_106 * L_20 = ___stream;
		NullCheck(L_20);
		Object_t * L_21 = PhotonStream_ReceiveNext_m8_541(L_20, /*hidden argument*/NULL);
		NullCheck(L_19);
		Rigidbody2D_set_angularVelocity_m6_745(L_19, ((*(float*)((float*)UnBox (L_21, Single_t1_17_il2cpp_TypeInfo_var)))), /*hidden argument*/NULL);
	}

IL_0094:
	{
		return;
	}
}
// System.Void PhotonRigidbodyView::.ctor()
extern "C" void PhotonRigidbodyView__ctor_m8_890 (PhotonRigidbodyView_t8_137 * __this, const MethodInfo* method)
{
	{
		__this->___m_SynchronizeVelocity_2 = 1;
		__this->___m_SynchronizeAngularVelocity_3 = 1;
		MonoBehaviour__ctor_m6_525(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void PhotonRigidbodyView::Awake()
extern const MethodInfo* Component_GetComponent_TisRigidbody_t6_102_m6_1659_MethodInfo_var;
extern "C" void PhotonRigidbodyView_Awake_m8_891 (PhotonRigidbodyView_t8_137 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Component_GetComponent_TisRigidbody_t6_102_m6_1659_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483837);
		s_Il2CppMethodIntialized = true;
	}
	{
		Rigidbody_t6_102 * L_0 = Component_GetComponent_TisRigidbody_t6_102_m6_1659(__this, /*hidden argument*/Component_GetComponent_TisRigidbody_t6_102_m6_1659_MethodInfo_var);
		__this->___m_Body_4 = L_0;
		return;
	}
}
// System.Void PhotonRigidbodyView::OnPhotonSerializeView(PhotonStream,PhotonMessageInfo)
extern TypeInfo* Vector3_t6_49_il2cpp_TypeInfo_var;
extern "C" void PhotonRigidbodyView_OnPhotonSerializeView_m8_892 (PhotonRigidbodyView_t8_137 * __this, PhotonStream_t8_106 * ___stream, PhotonMessageInfo_t8_104 * ___info, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Vector3_t6_49_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(941);
		s_Il2CppMethodIntialized = true;
	}
	{
		PhotonStream_t8_106 * L_0 = ___stream;
		NullCheck(L_0);
		bool L_1 = PhotonStream_get_isWriting_m8_538(L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0052;
		}
	}
	{
		bool L_2 = (__this->___m_SynchronizeVelocity_2);
		if (!L_2)
		{
			goto IL_002c;
		}
	}
	{
		PhotonStream_t8_106 * L_3 = ___stream;
		Rigidbody_t6_102 * L_4 = (__this->___m_Body_4);
		NullCheck(L_4);
		Vector3_t6_49  L_5 = Rigidbody_get_velocity_m6_717(L_4, /*hidden argument*/NULL);
		Vector3_t6_49  L_6 = L_5;
		Object_t * L_7 = Box(Vector3_t6_49_il2cpp_TypeInfo_var, &L_6);
		NullCheck(L_3);
		PhotonStream_SendNext_m8_543(L_3, L_7, /*hidden argument*/NULL);
	}

IL_002c:
	{
		bool L_8 = (__this->___m_SynchronizeAngularVelocity_3);
		if (!L_8)
		{
			goto IL_004d;
		}
	}
	{
		PhotonStream_t8_106 * L_9 = ___stream;
		Rigidbody_t6_102 * L_10 = (__this->___m_Body_4);
		NullCheck(L_10);
		Vector3_t6_49  L_11 = Rigidbody_get_angularVelocity_m6_721(L_10, /*hidden argument*/NULL);
		Vector3_t6_49  L_12 = L_11;
		Object_t * L_13 = Box(Vector3_t6_49_il2cpp_TypeInfo_var, &L_12);
		NullCheck(L_9);
		PhotonStream_SendNext_m8_543(L_9, L_13, /*hidden argument*/NULL);
	}

IL_004d:
	{
		goto IL_0094;
	}

IL_0052:
	{
		bool L_14 = (__this->___m_SynchronizeVelocity_2);
		if (!L_14)
		{
			goto IL_0073;
		}
	}
	{
		Rigidbody_t6_102 * L_15 = (__this->___m_Body_4);
		PhotonStream_t8_106 * L_16 = ___stream;
		NullCheck(L_16);
		Object_t * L_17 = PhotonStream_ReceiveNext_m8_541(L_16, /*hidden argument*/NULL);
		NullCheck(L_15);
		Rigidbody_set_velocity_m6_718(L_15, ((*(Vector3_t6_49 *)((Vector3_t6_49 *)UnBox (L_17, Vector3_t6_49_il2cpp_TypeInfo_var)))), /*hidden argument*/NULL);
	}

IL_0073:
	{
		bool L_18 = (__this->___m_SynchronizeAngularVelocity_3);
		if (!L_18)
		{
			goto IL_0094;
		}
	}
	{
		Rigidbody_t6_102 * L_19 = (__this->___m_Body_4);
		PhotonStream_t8_106 * L_20 = ___stream;
		NullCheck(L_20);
		Object_t * L_21 = PhotonStream_ReceiveNext_m8_541(L_20, /*hidden argument*/NULL);
		NullCheck(L_19);
		Rigidbody_set_angularVelocity_m6_722(L_19, ((*(Vector3_t6_49 *)((Vector3_t6_49 *)UnBox (L_21, Vector3_t6_49_il2cpp_TypeInfo_var)))), /*hidden argument*/NULL);
	}

IL_0094:
	{
		return;
	}
}
// System.Void PhotonTransformView::.ctor()
extern TypeInfo* PhotonTransformViewPositionModel_t8_138_il2cpp_TypeInfo_var;
extern TypeInfo* PhotonTransformViewRotationModel_t8_139_il2cpp_TypeInfo_var;
extern TypeInfo* PhotonTransformViewScaleModel_t8_140_il2cpp_TypeInfo_var;
extern "C" void PhotonTransformView__ctor_m8_893 (PhotonTransformView_t8_39 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		PhotonTransformViewPositionModel_t8_138_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1179);
		PhotonTransformViewRotationModel_t8_139_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1180);
		PhotonTransformViewScaleModel_t8_140_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1181);
		s_Il2CppMethodIntialized = true;
	}
	{
		PhotonTransformViewPositionModel_t8_138 * L_0 = (PhotonTransformViewPositionModel_t8_138 *)il2cpp_codegen_object_new (PhotonTransformViewPositionModel_t8_138_il2cpp_TypeInfo_var);
		PhotonTransformViewPositionModel__ctor_m8_911(L_0, /*hidden argument*/NULL);
		__this->___m_PositionModel_2 = L_0;
		PhotonTransformViewRotationModel_t8_139 * L_1 = (PhotonTransformViewRotationModel_t8_139 *)il2cpp_codegen_object_new (PhotonTransformViewRotationModel_t8_139_il2cpp_TypeInfo_var);
		PhotonTransformViewRotationModel__ctor_m8_915(L_1, /*hidden argument*/NULL);
		__this->___m_RotationModel_3 = L_1;
		PhotonTransformViewScaleModel_t8_140 * L_2 = (PhotonTransformViewScaleModel_t8_140 *)il2cpp_codegen_object_new (PhotonTransformViewScaleModel_t8_140_il2cpp_TypeInfo_var);
		PhotonTransformViewScaleModel__ctor_m8_919(L_2, /*hidden argument*/NULL);
		__this->___m_ScaleModel_4 = L_2;
		MonoBehaviour__ctor_m6_525(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void PhotonTransformView::Awake()
extern TypeInfo* PhotonTransformViewPositionControl_t8_141_il2cpp_TypeInfo_var;
extern TypeInfo* PhotonTransformViewRotationControl_t8_142_il2cpp_TypeInfo_var;
extern TypeInfo* PhotonTransformViewScaleControl_t8_143_il2cpp_TypeInfo_var;
extern const MethodInfo* Component_GetComponent_TisPhotonView_t8_3_m6_1658_MethodInfo_var;
extern "C" void PhotonTransformView_Awake_m8_894 (PhotonTransformView_t8_39 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		PhotonTransformViewPositionControl_t8_141_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1182);
		PhotonTransformViewRotationControl_t8_142_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1183);
		PhotonTransformViewScaleControl_t8_143_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1184);
		Component_GetComponent_TisPhotonView_t8_3_m6_1658_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483836);
		s_Il2CppMethodIntialized = true;
	}
	{
		PhotonView_t8_3 * L_0 = Component_GetComponent_TisPhotonView_t8_3_m6_1658(__this, /*hidden argument*/Component_GetComponent_TisPhotonView_t8_3_m6_1658_MethodInfo_var);
		__this->___m_PhotonView_8 = L_0;
		PhotonTransformViewPositionModel_t8_138 * L_1 = (__this->___m_PositionModel_2);
		PhotonTransformViewPositionControl_t8_141 * L_2 = (PhotonTransformViewPositionControl_t8_141 *)il2cpp_codegen_object_new (PhotonTransformViewPositionControl_t8_141_il2cpp_TypeInfo_var);
		PhotonTransformViewPositionControl__ctor_m8_902(L_2, L_1, /*hidden argument*/NULL);
		__this->___m_PositionControl_5 = L_2;
		PhotonTransformViewRotationModel_t8_139 * L_3 = (__this->___m_RotationModel_3);
		PhotonTransformViewRotationControl_t8_142 * L_4 = (PhotonTransformViewRotationControl_t8_142 *)il2cpp_codegen_object_new (PhotonTransformViewRotationControl_t8_142_il2cpp_TypeInfo_var);
		PhotonTransformViewRotationControl__ctor_m8_912(L_4, L_3, /*hidden argument*/NULL);
		__this->___m_RotationControl_6 = L_4;
		PhotonTransformViewScaleModel_t8_140 * L_5 = (__this->___m_ScaleModel_4);
		PhotonTransformViewScaleControl_t8_143 * L_6 = (PhotonTransformViewScaleControl_t8_143 *)il2cpp_codegen_object_new (PhotonTransformViewScaleControl_t8_143_il2cpp_TypeInfo_var);
		PhotonTransformViewScaleControl__ctor_m8_916(L_6, L_5, /*hidden argument*/NULL);
		__this->___m_ScaleControl_7 = L_6;
		return;
	}
}
// System.Void PhotonTransformView::Update()
extern TypeInfo* PhotonNetwork_t8_114_il2cpp_TypeInfo_var;
extern "C" void PhotonTransformView_Update_m8_895 (PhotonTransformView_t8_39 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		PhotonNetwork_t8_114_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1058);
		s_Il2CppMethodIntialized = true;
	}
	{
		PhotonView_t8_3 * L_0 = (__this->___m_PhotonView_8);
		bool L_1 = Object_op_Equality_m6_579(NULL /*static, unused*/, L_0, (Object_t6_5 *)NULL, /*hidden argument*/NULL);
		if (L_1)
		{
			goto IL_002b;
		}
	}
	{
		PhotonView_t8_3 * L_2 = (__this->___m_PhotonView_8);
		NullCheck(L_2);
		bool L_3 = PhotonView_get_isMine_m8_774(L_2, /*hidden argument*/NULL);
		if (L_3)
		{
			goto IL_002b;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(PhotonNetwork_t8_114_il2cpp_TypeInfo_var);
		bool L_4 = PhotonNetwork_get_connected_m8_603(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (L_4)
		{
			goto IL_002c;
		}
	}

IL_002b:
	{
		return;
	}

IL_002c:
	{
		PhotonTransformView_UpdatePosition_m8_896(__this, /*hidden argument*/NULL);
		PhotonTransformView_UpdateRotation_m8_897(__this, /*hidden argument*/NULL);
		PhotonTransformView_UpdateScale_m8_898(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void PhotonTransformView::UpdatePosition()
extern "C" void PhotonTransformView_UpdatePosition_m8_896 (PhotonTransformView_t8_39 * __this, const MethodInfo* method)
{
	{
		PhotonTransformViewPositionModel_t8_138 * L_0 = (__this->___m_PositionModel_2);
		NullCheck(L_0);
		bool L_1 = (L_0->___SynchronizeEnabled_0);
		if (!L_1)
		{
			goto IL_001b;
		}
	}
	{
		bool L_2 = (__this->___m_ReceivedNetworkUpdate_9);
		if (L_2)
		{
			goto IL_001c;
		}
	}

IL_001b:
	{
		return;
	}

IL_001c:
	{
		Transform_t6_61 * L_3 = Component_get_transform_m6_582(__this, /*hidden argument*/NULL);
		PhotonTransformViewPositionControl_t8_141 * L_4 = (__this->___m_PositionControl_5);
		Transform_t6_61 * L_5 = Component_get_transform_m6_582(__this, /*hidden argument*/NULL);
		NullCheck(L_5);
		Vector3_t6_49  L_6 = Transform_get_localPosition_m6_615(L_5, /*hidden argument*/NULL);
		NullCheck(L_4);
		Vector3_t6_49  L_7 = PhotonTransformViewPositionControl_UpdatePosition_m8_905(L_4, L_6, /*hidden argument*/NULL);
		NullCheck(L_3);
		Transform_set_localPosition_m6_616(L_3, L_7, /*hidden argument*/NULL);
		return;
	}
}
// System.Void PhotonTransformView::UpdateRotation()
extern "C" void PhotonTransformView_UpdateRotation_m8_897 (PhotonTransformView_t8_39 * __this, const MethodInfo* method)
{
	{
		PhotonTransformViewRotationModel_t8_139 * L_0 = (__this->___m_RotationModel_3);
		NullCheck(L_0);
		bool L_1 = (L_0->___SynchronizeEnabled_0);
		if (!L_1)
		{
			goto IL_001b;
		}
	}
	{
		bool L_2 = (__this->___m_ReceivedNetworkUpdate_9);
		if (L_2)
		{
			goto IL_001c;
		}
	}

IL_001b:
	{
		return;
	}

IL_001c:
	{
		Transform_t6_61 * L_3 = Component_get_transform_m6_582(__this, /*hidden argument*/NULL);
		PhotonTransformViewRotationControl_t8_142 * L_4 = (__this->___m_RotationControl_6);
		Transform_t6_61 * L_5 = Component_get_transform_m6_582(__this, /*hidden argument*/NULL);
		NullCheck(L_5);
		Quaternion_t6_51  L_6 = Transform_get_localRotation_m6_627(L_5, /*hidden argument*/NULL);
		NullCheck(L_4);
		Quaternion_t6_51  L_7 = PhotonTransformViewRotationControl_GetRotation_m8_913(L_4, L_6, /*hidden argument*/NULL);
		NullCheck(L_3);
		Transform_set_localRotation_m6_628(L_3, L_7, /*hidden argument*/NULL);
		return;
	}
}
// System.Void PhotonTransformView::UpdateScale()
extern "C" void PhotonTransformView_UpdateScale_m8_898 (PhotonTransformView_t8_39 * __this, const MethodInfo* method)
{
	{
		PhotonTransformViewScaleModel_t8_140 * L_0 = (__this->___m_ScaleModel_4);
		NullCheck(L_0);
		bool L_1 = (L_0->___SynchronizeEnabled_0);
		if (!L_1)
		{
			goto IL_001b;
		}
	}
	{
		bool L_2 = (__this->___m_ReceivedNetworkUpdate_9);
		if (L_2)
		{
			goto IL_001c;
		}
	}

IL_001b:
	{
		return;
	}

IL_001c:
	{
		Transform_t6_61 * L_3 = Component_get_transform_m6_582(__this, /*hidden argument*/NULL);
		PhotonTransformViewScaleControl_t8_143 * L_4 = (__this->___m_ScaleControl_7);
		Transform_t6_61 * L_5 = Component_get_transform_m6_582(__this, /*hidden argument*/NULL);
		NullCheck(L_5);
		Vector3_t6_49  L_6 = Transform_get_localScale_m6_631(L_5, /*hidden argument*/NULL);
		NullCheck(L_4);
		Vector3_t6_49  L_7 = PhotonTransformViewScaleControl_GetScale_m8_917(L_4, L_6, /*hidden argument*/NULL);
		NullCheck(L_3);
		Transform_set_localScale_m6_632(L_3, L_7, /*hidden argument*/NULL);
		return;
	}
}
// System.Void PhotonTransformView::SetSynchronizedValues(UnityEngine.Vector3,System.Single)
extern "C" void PhotonTransformView_SetSynchronizedValues_m8_899 (PhotonTransformView_t8_39 * __this, Vector3_t6_49  ___speed, float ___turnSpeed, const MethodInfo* method)
{
	{
		PhotonTransformViewPositionControl_t8_141 * L_0 = (__this->___m_PositionControl_5);
		Vector3_t6_49  L_1 = ___speed;
		float L_2 = ___turnSpeed;
		NullCheck(L_0);
		PhotonTransformViewPositionControl_SetSynchronizedValues_m8_904(L_0, L_1, L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Void PhotonTransformView::OnPhotonSerializeView(PhotonStream,PhotonMessageInfo)
extern "C" void PhotonTransformView_OnPhotonSerializeView_m8_900 (PhotonTransformView_t8_39 * __this, PhotonStream_t8_106 * ___stream, PhotonMessageInfo_t8_104 * ___info, const MethodInfo* method)
{
	{
		PhotonTransformViewPositionControl_t8_141 * L_0 = (__this->___m_PositionControl_5);
		Transform_t6_61 * L_1 = Component_get_transform_m6_582(__this, /*hidden argument*/NULL);
		NullCheck(L_1);
		Vector3_t6_49  L_2 = Transform_get_localPosition_m6_615(L_1, /*hidden argument*/NULL);
		PhotonStream_t8_106 * L_3 = ___stream;
		PhotonMessageInfo_t8_104 * L_4 = ___info;
		NullCheck(L_0);
		PhotonTransformViewPositionControl_OnPhotonSerializeView_m8_908(L_0, L_2, L_3, L_4, /*hidden argument*/NULL);
		PhotonTransformViewRotationControl_t8_142 * L_5 = (__this->___m_RotationControl_6);
		Transform_t6_61 * L_6 = Component_get_transform_m6_582(__this, /*hidden argument*/NULL);
		NullCheck(L_6);
		Quaternion_t6_51  L_7 = Transform_get_localRotation_m6_627(L_6, /*hidden argument*/NULL);
		PhotonStream_t8_106 * L_8 = ___stream;
		PhotonMessageInfo_t8_104 * L_9 = ___info;
		NullCheck(L_5);
		PhotonTransformViewRotationControl_OnPhotonSerializeView_m8_914(L_5, L_7, L_8, L_9, /*hidden argument*/NULL);
		PhotonTransformViewScaleControl_t8_143 * L_10 = (__this->___m_ScaleControl_7);
		Transform_t6_61 * L_11 = Component_get_transform_m6_582(__this, /*hidden argument*/NULL);
		NullCheck(L_11);
		Vector3_t6_49  L_12 = Transform_get_localScale_m6_631(L_11, /*hidden argument*/NULL);
		PhotonStream_t8_106 * L_13 = ___stream;
		PhotonMessageInfo_t8_104 * L_14 = ___info;
		NullCheck(L_10);
		PhotonTransformViewScaleControl_OnPhotonSerializeView_m8_918(L_10, L_12, L_13, L_14, /*hidden argument*/NULL);
		PhotonView_t8_3 * L_15 = (__this->___m_PhotonView_8);
		NullCheck(L_15);
		bool L_16 = PhotonView_get_isMine_m8_774(L_15, /*hidden argument*/NULL);
		if (L_16)
		{
			goto IL_006e;
		}
	}
	{
		PhotonTransformViewPositionModel_t8_138 * L_17 = (__this->___m_PositionModel_2);
		NullCheck(L_17);
		bool L_18 = (L_17->___DrawErrorGizmo_13);
		if (!L_18)
		{
			goto IL_006e;
		}
	}
	{
		PhotonTransformView_DoDrawEstimatedPositionError_m8_901(__this, /*hidden argument*/NULL);
	}

IL_006e:
	{
		PhotonStream_t8_106 * L_19 = ___stream;
		NullCheck(L_19);
		bool L_20 = PhotonStream_get_isReading_m8_539(L_19, /*hidden argument*/NULL);
		if (!L_20)
		{
			goto IL_0080;
		}
	}
	{
		__this->___m_ReceivedNetworkUpdate_9 = 1;
	}

IL_0080:
	{
		return;
	}
}
// System.Void PhotonTransformView::DoDrawEstimatedPositionError()
extern "C" void PhotonTransformView_DoDrawEstimatedPositionError_m8_901 (PhotonTransformView_t8_39 * __this, const MethodInfo* method)
{
	Vector3_t6_49  V_0 = {0};
	{
		PhotonTransformViewPositionControl_t8_141 * L_0 = (__this->___m_PositionControl_5);
		NullCheck(L_0);
		Vector3_t6_49  L_1 = PhotonTransformViewPositionControl_GetNetworkPosition_m8_906(L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		Vector3_t6_49  L_2 = V_0;
		Transform_t6_61 * L_3 = Component_get_transform_m6_582(__this, /*hidden argument*/NULL);
		NullCheck(L_3);
		Vector3_t6_49  L_4 = Transform_get_position_m6_611(L_3, /*hidden argument*/NULL);
		Color_t6_40  L_5 = Color_get_red_m6_234(NULL /*static, unused*/, /*hidden argument*/NULL);
		Debug_DrawLine_m6_485(NULL /*static, unused*/, L_2, L_4, L_5, (2.0f), /*hidden argument*/NULL);
		Transform_t6_61 * L_6 = Component_get_transform_m6_582(__this, /*hidden argument*/NULL);
		NullCheck(L_6);
		Vector3_t6_49  L_7 = Transform_get_position_m6_611(L_6, /*hidden argument*/NULL);
		Transform_t6_61 * L_8 = Component_get_transform_m6_582(__this, /*hidden argument*/NULL);
		NullCheck(L_8);
		Vector3_t6_49  L_9 = Transform_get_position_m6_611(L_8, /*hidden argument*/NULL);
		Vector3_t6_49  L_10 = Vector3_get_up_m6_217(NULL /*static, unused*/, /*hidden argument*/NULL);
		Vector3_t6_49  L_11 = Vector3_op_Addition_m6_221(NULL /*static, unused*/, L_9, L_10, /*hidden argument*/NULL);
		Color_t6_40  L_12 = Color_get_green_m6_235(NULL /*static, unused*/, /*hidden argument*/NULL);
		Debug_DrawLine_m6_485(NULL /*static, unused*/, L_7, L_11, L_12, (2.0f), /*hidden argument*/NULL);
		Vector3_t6_49  L_13 = V_0;
		Vector3_t6_49  L_14 = V_0;
		Vector3_t6_49  L_15 = Vector3_get_up_m6_217(NULL /*static, unused*/, /*hidden argument*/NULL);
		Vector3_t6_49  L_16 = Vector3_op_Addition_m6_221(NULL /*static, unused*/, L_14, L_15, /*hidden argument*/NULL);
		Color_t6_40  L_17 = Color_get_red_m6_234(NULL /*static, unused*/, /*hidden argument*/NULL);
		Debug_DrawLine_m6_485(NULL /*static, unused*/, L_13, L_16, L_17, (2.0f), /*hidden argument*/NULL);
		return;
	}
}
// System.Void PhotonTransformViewPositionControl::.ctor(PhotonTransformViewPositionModel)
extern TypeInfo* Queue_1_t3_190_il2cpp_TypeInfo_var;
extern const MethodInfo* Queue_1__ctor_m3_1054_MethodInfo_var;
extern "C" void PhotonTransformViewPositionControl__ctor_m8_902 (PhotonTransformViewPositionControl_t8_141 * __this, PhotonTransformViewPositionModel_t8_138 * ___model, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Queue_1_t3_190_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1185);
		Queue_1__ctor_m3_1054_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483954);
		s_Il2CppMethodIntialized = true;
	}
	{
		Vector3_t6_49  L_0 = Vector3_get_zero_m6_213(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->___m_SynchronizedSpeed_3 = L_0;
		Queue_1_t3_190 * L_1 = (Queue_1_t3_190 *)il2cpp_codegen_object_new (Queue_1_t3_190_il2cpp_TypeInfo_var);
		Queue_1__ctor_m3_1054(L_1, /*hidden argument*/Queue_1__ctor_m3_1054_MethodInfo_var);
		__this->___m_OldNetworkPositions_6 = L_1;
		__this->___m_UpdatedPositionAfterOnSerialize_7 = 1;
		Object__ctor_m1_0(__this, /*hidden argument*/NULL);
		PhotonTransformViewPositionModel_t8_138 * L_2 = ___model;
		__this->___m_Model_0 = L_2;
		return;
	}
}
// UnityEngine.Vector3 PhotonTransformViewPositionControl::GetOldestStoredNetworkPosition()
extern const MethodInfo* Queue_1_Peek_m3_1055_MethodInfo_var;
extern "C" Vector3_t6_49  PhotonTransformViewPositionControl_GetOldestStoredNetworkPosition_m8_903 (PhotonTransformViewPositionControl_t8_141 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Queue_1_Peek_m3_1055_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483955);
		s_Il2CppMethodIntialized = true;
	}
	Vector3_t6_49  V_0 = {0};
	{
		Vector3_t6_49  L_0 = (__this->___m_NetworkPosition_5);
		V_0 = L_0;
		Queue_1_t3_190 * L_1 = (__this->___m_OldNetworkPositions_6);
		NullCheck(L_1);
		int32_t L_2 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(6 /* System.Int32 System.Collections.Generic.Queue`1<UnityEngine.Vector3>::get_Count() */, L_1);
		if ((((int32_t)L_2) <= ((int32_t)0)))
		{
			goto IL_0024;
		}
	}
	{
		Queue_1_t3_190 * L_3 = (__this->___m_OldNetworkPositions_6);
		NullCheck(L_3);
		Vector3_t6_49  L_4 = Queue_1_Peek_m3_1055(L_3, /*hidden argument*/Queue_1_Peek_m3_1055_MethodInfo_var);
		V_0 = L_4;
	}

IL_0024:
	{
		Vector3_t6_49  L_5 = V_0;
		return L_5;
	}
}
// System.Void PhotonTransformViewPositionControl::SetSynchronizedValues(UnityEngine.Vector3,System.Single)
extern "C" void PhotonTransformViewPositionControl_SetSynchronizedValues_m8_904 (PhotonTransformViewPositionControl_t8_141 * __this, Vector3_t6_49  ___speed, float ___turnSpeed, const MethodInfo* method)
{
	{
		Vector3_t6_49  L_0 = ___speed;
		__this->___m_SynchronizedSpeed_3 = L_0;
		float L_1 = ___turnSpeed;
		__this->___m_SynchronizedTurnSpeed_4 = L_1;
		return;
	}
}
// UnityEngine.Vector3 PhotonTransformViewPositionControl::UpdatePosition(UnityEngine.Vector3)
extern TypeInfo* Mathf_t6_58_il2cpp_TypeInfo_var;
extern "C" Vector3_t6_49  PhotonTransformViewPositionControl_UpdatePosition_m8_905 (PhotonTransformViewPositionControl_t8_141 * __this, Vector3_t6_49  ___currentPosition, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Mathf_t6_58_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(940);
		s_Il2CppMethodIntialized = true;
	}
	Vector3_t6_49  V_0 = {0};
	int32_t V_1 = 0;
	float V_2 = 0.0f;
	int32_t V_3 = {0};
	{
		Vector3_t6_49  L_0 = PhotonTransformViewPositionControl_GetNetworkPosition_m8_906(__this, /*hidden argument*/NULL);
		Vector3_t6_49  L_1 = PhotonTransformViewPositionControl_GetExtrapolatedPositionOffset_m8_907(__this, /*hidden argument*/NULL);
		Vector3_t6_49  L_2 = Vector3_op_Addition_m6_221(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		PhotonTransformViewPositionModel_t8_138 * L_3 = (__this->___m_Model_0);
		NullCheck(L_3);
		int32_t L_4 = (L_3->___InterpolateOption_3);
		V_3 = L_4;
		int32_t L_5 = V_3;
		if (L_5 == 0)
		{
			goto IL_003d;
		}
		if (L_5 == 1)
		{
			goto IL_0057;
		}
		if (L_5 == 2)
		{
			goto IL_0076;
		}
		if (L_5 == 3)
		{
			goto IL_00b2;
		}
		if (L_5 == 4)
		{
			goto IL_00ee;
		}
	}
	{
		goto IL_010d;
	}

IL_003d:
	{
		bool L_6 = (__this->___m_UpdatedPositionAfterOnSerialize_7);
		if (L_6)
		{
			goto IL_0052;
		}
	}
	{
		Vector3_t6_49  L_7 = V_0;
		___currentPosition = L_7;
		__this->___m_UpdatedPositionAfterOnSerialize_7 = 1;
	}

IL_0052:
	{
		goto IL_010d;
	}

IL_0057:
	{
		Vector3_t6_49  L_8 = ___currentPosition;
		Vector3_t6_49  L_9 = V_0;
		float L_10 = Time_get_deltaTime_m6_654(NULL /*static, unused*/, /*hidden argument*/NULL);
		PhotonTransformViewPositionModel_t8_138 * L_11 = (__this->___m_Model_0);
		NullCheck(L_11);
		float L_12 = (L_11->___InterpolateMoveTowardsSpeed_4);
		Vector3_t6_49  L_13 = Vector3_MoveTowards_m6_195(NULL /*static, unused*/, L_8, L_9, ((float)((float)L_10*(float)L_12)), /*hidden argument*/NULL);
		___currentPosition = L_13;
		goto IL_010d;
	}

IL_0076:
	{
		Queue_1_t3_190 * L_14 = (__this->___m_OldNetworkPositions_6);
		NullCheck(L_14);
		int32_t L_15 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(6 /* System.Int32 System.Collections.Generic.Queue`1<UnityEngine.Vector3>::get_Count() */, L_14);
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t6_58_il2cpp_TypeInfo_var);
		int32_t L_16 = Mathf_Min_m6_382(NULL /*static, unused*/, 1, L_15, /*hidden argument*/NULL);
		V_1 = L_16;
		Vector3_t6_49  L_17 = (__this->___m_NetworkPosition_5);
		Vector3_t6_49  L_18 = PhotonTransformViewPositionControl_GetOldestStoredNetworkPosition_m8_903(__this, /*hidden argument*/NULL);
		float L_19 = Vector3_Distance_m6_206(NULL /*static, unused*/, L_17, L_18, /*hidden argument*/NULL);
		int32_t L_20 = V_1;
		V_2 = ((float)((float)L_19/(float)(((float)((float)L_20)))));
		Vector3_t6_49  L_21 = ___currentPosition;
		Vector3_t6_49  L_22 = V_0;
		float L_23 = Time_get_deltaTime_m6_654(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_24 = V_2;
		Vector3_t6_49  L_25 = Vector3_MoveTowards_m6_195(NULL /*static, unused*/, L_21, L_22, ((float)((float)L_23*(float)L_24)), /*hidden argument*/NULL);
		___currentPosition = L_25;
		goto IL_010d;
	}

IL_00b2:
	{
		Vector3_t6_49 * L_26 = &(__this->___m_SynchronizedSpeed_3);
		float L_27 = Vector3_get_magnitude_m6_208(L_26, /*hidden argument*/NULL);
		if ((!(((float)L_27) == ((float)(0.0f)))))
		{
			goto IL_00cf;
		}
	}
	{
		Vector3_t6_49  L_28 = V_0;
		___currentPosition = L_28;
		goto IL_00e9;
	}

IL_00cf:
	{
		Vector3_t6_49  L_29 = ___currentPosition;
		Vector3_t6_49  L_30 = V_0;
		float L_31 = Time_get_deltaTime_m6_654(NULL /*static, unused*/, /*hidden argument*/NULL);
		Vector3_t6_49 * L_32 = &(__this->___m_SynchronizedSpeed_3);
		float L_33 = Vector3_get_magnitude_m6_208(L_32, /*hidden argument*/NULL);
		Vector3_t6_49  L_34 = Vector3_MoveTowards_m6_195(NULL /*static, unused*/, L_29, L_30, ((float)((float)L_31*(float)L_33)), /*hidden argument*/NULL);
		___currentPosition = L_34;
	}

IL_00e9:
	{
		goto IL_010d;
	}

IL_00ee:
	{
		Vector3_t6_49  L_35 = ___currentPosition;
		Vector3_t6_49  L_36 = V_0;
		float L_37 = Time_get_deltaTime_m6_654(NULL /*static, unused*/, /*hidden argument*/NULL);
		PhotonTransformViewPositionModel_t8_138 * L_38 = (__this->___m_Model_0);
		NullCheck(L_38);
		float L_39 = (L_38->___InterpolateLerpSpeed_5);
		Vector3_t6_49  L_40 = Vector3_Lerp_m6_192(NULL /*static, unused*/, L_35, L_36, ((float)((float)L_37*(float)L_39)), /*hidden argument*/NULL);
		___currentPosition = L_40;
		goto IL_010d;
	}

IL_010d:
	{
		PhotonTransformViewPositionModel_t8_138 * L_41 = (__this->___m_Model_0);
		NullCheck(L_41);
		bool L_42 = (L_41->___TeleportEnabled_1);
		if (!L_42)
		{
			goto IL_0141;
		}
	}
	{
		Vector3_t6_49  L_43 = ___currentPosition;
		Vector3_t6_49  L_44 = PhotonTransformViewPositionControl_GetNetworkPosition_m8_906(__this, /*hidden argument*/NULL);
		float L_45 = Vector3_Distance_m6_206(NULL /*static, unused*/, L_43, L_44, /*hidden argument*/NULL);
		PhotonTransformViewPositionModel_t8_138 * L_46 = (__this->___m_Model_0);
		NullCheck(L_46);
		float L_47 = (L_46->___TeleportIfDistanceGreaterThan_2);
		if ((!(((float)L_45) > ((float)L_47))))
		{
			goto IL_0141;
		}
	}
	{
		Vector3_t6_49  L_48 = PhotonTransformViewPositionControl_GetNetworkPosition_m8_906(__this, /*hidden argument*/NULL);
		___currentPosition = L_48;
	}

IL_0141:
	{
		Vector3_t6_49  L_49 = ___currentPosition;
		return L_49;
	}
}
// UnityEngine.Vector3 PhotonTransformViewPositionControl::GetNetworkPosition()
extern "C" Vector3_t6_49  PhotonTransformViewPositionControl_GetNetworkPosition_m8_906 (PhotonTransformViewPositionControl_t8_141 * __this, const MethodInfo* method)
{
	{
		Vector3_t6_49  L_0 = (__this->___m_NetworkPosition_5);
		return L_0;
	}
}
// UnityEngine.Vector3 PhotonTransformViewPositionControl::GetExtrapolatedPositionOffset()
extern TypeInfo* PhotonNetwork_t8_114_il2cpp_TypeInfo_var;
extern "C" Vector3_t6_49  PhotonTransformViewPositionControl_GetExtrapolatedPositionOffset_m8_907 (PhotonTransformViewPositionControl_t8_141 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		PhotonNetwork_t8_114_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1058);
		s_Il2CppMethodIntialized = true;
	}
	float V_0 = 0.0f;
	Vector3_t6_49  V_1 = {0};
	Quaternion_t6_51  V_2 = {0};
	Vector3_t6_49  V_3 = {0};
	Vector3_t6_49  V_4 = {0};
	int32_t V_5 = {0};
	Vector3_t6_49  V_6 = {0};
	{
		IL2CPP_RUNTIME_CLASS_INIT(PhotonNetwork_t8_114_il2cpp_TypeInfo_var);
		double L_0 = PhotonNetwork_get_time_m8_646(NULL /*static, unused*/, /*hidden argument*/NULL);
		double L_1 = (__this->___m_LastSerializeTime_2);
		V_0 = (((float)((float)((double)((double)L_0-(double)L_1)))));
		PhotonTransformViewPositionModel_t8_138 * L_2 = (__this->___m_Model_0);
		NullCheck(L_2);
		bool L_3 = (L_2->___ExtrapolateIncludingRoundTripTime_11);
		if (!L_3)
		{
			goto IL_002d;
		}
	}
	{
		float L_4 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(PhotonNetwork_t8_114_il2cpp_TypeInfo_var);
		int32_t L_5 = PhotonNetwork_GetPing_m8_703(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_0 = ((float)((float)L_4+(float)((float)((float)(((float)((float)L_5)))/(float)(1000.0f)))));
	}

IL_002d:
	{
		Vector3_t6_49  L_6 = Vector3_get_zero_m6_213(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_1 = L_6;
		PhotonTransformViewPositionModel_t8_138 * L_7 = (__this->___m_Model_0);
		NullCheck(L_7);
		int32_t L_8 = (L_7->___ExtrapolateOption_9);
		V_5 = L_8;
		int32_t L_9 = V_5;
		if (((int32_t)((int32_t)L_9-(int32_t)1)) == 0)
		{
			goto IL_005a;
		}
		if (((int32_t)((int32_t)L_9-(int32_t)1)) == 1)
		{
			goto IL_00c2;
		}
		if (((int32_t)((int32_t)L_9-(int32_t)1)) == 2)
		{
			goto IL_008a;
		}
	}
	{
		goto IL_00ee;
	}

IL_005a:
	{
		float L_10 = (__this->___m_SynchronizedTurnSpeed_4);
		float L_11 = V_0;
		Quaternion_t6_51  L_12 = Quaternion_Euler_m6_260(NULL /*static, unused*/, (0.0f), ((float)((float)L_10*(float)L_11)), (0.0f), /*hidden argument*/NULL);
		V_2 = L_12;
		Quaternion_t6_51  L_13 = V_2;
		Vector3_t6_49  L_14 = (__this->___m_SynchronizedSpeed_3);
		float L_15 = V_0;
		Vector3_t6_49  L_16 = Vector3_op_Multiply_m6_224(NULL /*static, unused*/, L_14, L_15, /*hidden argument*/NULL);
		Vector3_t6_49  L_17 = Quaternion_op_Multiply_m6_268(NULL /*static, unused*/, L_13, L_16, /*hidden argument*/NULL);
		V_1 = L_17;
		goto IL_00ee;
	}

IL_008a:
	{
		Vector3_t6_49  L_18 = (__this->___m_NetworkPosition_5);
		Vector3_t6_49  L_19 = PhotonTransformViewPositionControl_GetOldestStoredNetworkPosition_m8_903(__this, /*hidden argument*/NULL);
		Vector3_t6_49  L_20 = Vector3_op_Subtraction_m6_222(NULL /*static, unused*/, L_18, L_19, /*hidden argument*/NULL);
		V_6 = L_20;
		Vector3_t6_49  L_21 = Vector3_get_normalized_m6_201((&V_6), /*hidden argument*/NULL);
		V_3 = L_21;
		Vector3_t6_49  L_22 = V_3;
		PhotonTransformViewPositionModel_t8_138 * L_23 = (__this->___m_Model_0);
		NullCheck(L_23);
		float L_24 = (L_23->___ExtrapolateSpeed_10);
		Vector3_t6_49  L_25 = Vector3_op_Multiply_m6_224(NULL /*static, unused*/, L_22, L_24, /*hidden argument*/NULL);
		float L_26 = V_0;
		Vector3_t6_49  L_27 = Vector3_op_Multiply_m6_224(NULL /*static, unused*/, L_25, L_26, /*hidden argument*/NULL);
		V_1 = L_27;
		goto IL_00ee;
	}

IL_00c2:
	{
		Vector3_t6_49  L_28 = (__this->___m_NetworkPosition_5);
		Vector3_t6_49  L_29 = PhotonTransformViewPositionControl_GetOldestStoredNetworkPosition_m8_903(__this, /*hidden argument*/NULL);
		Vector3_t6_49  L_30 = Vector3_op_Subtraction_m6_222(NULL /*static, unused*/, L_28, L_29, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(PhotonNetwork_t8_114_il2cpp_TypeInfo_var);
		int32_t L_31 = PhotonNetwork_get_sendRateOnSerialize_m8_640(NULL /*static, unused*/, /*hidden argument*/NULL);
		Vector3_t6_49  L_32 = Vector3_op_Multiply_m6_224(NULL /*static, unused*/, L_30, (((float)((float)L_31))), /*hidden argument*/NULL);
		V_4 = L_32;
		Vector3_t6_49  L_33 = V_4;
		float L_34 = V_0;
		Vector3_t6_49  L_35 = Vector3_op_Multiply_m6_224(NULL /*static, unused*/, L_33, L_34, /*hidden argument*/NULL);
		V_1 = L_35;
		goto IL_00ee;
	}

IL_00ee:
	{
		Vector3_t6_49  L_36 = V_1;
		return L_36;
	}
}
// System.Void PhotonTransformViewPositionControl::OnPhotonSerializeView(UnityEngine.Vector3,PhotonStream,PhotonMessageInfo)
extern TypeInfo* PhotonNetwork_t8_114_il2cpp_TypeInfo_var;
extern "C" void PhotonTransformViewPositionControl_OnPhotonSerializeView_m8_908 (PhotonTransformViewPositionControl_t8_141 * __this, Vector3_t6_49  ___currentPosition, PhotonStream_t8_106 * ___stream, PhotonMessageInfo_t8_104 * ___info, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		PhotonNetwork_t8_114_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1058);
		s_Il2CppMethodIntialized = true;
	}
	{
		PhotonTransformViewPositionModel_t8_138 * L_0 = (__this->___m_Model_0);
		NullCheck(L_0);
		bool L_1 = (L_0->___SynchronizeEnabled_0);
		if (L_1)
		{
			goto IL_0011;
		}
	}
	{
		return;
	}

IL_0011:
	{
		PhotonStream_t8_106 * L_2 = ___stream;
		NullCheck(L_2);
		bool L_3 = PhotonStream_get_isWriting_m8_538(L_2, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_002a;
		}
	}
	{
		Vector3_t6_49  L_4 = ___currentPosition;
		PhotonStream_t8_106 * L_5 = ___stream;
		PhotonMessageInfo_t8_104 * L_6 = ___info;
		PhotonTransformViewPositionControl_SerializeData_m8_909(__this, L_4, L_5, L_6, /*hidden argument*/NULL);
		goto IL_0032;
	}

IL_002a:
	{
		PhotonStream_t8_106 * L_7 = ___stream;
		PhotonMessageInfo_t8_104 * L_8 = ___info;
		PhotonTransformViewPositionControl_DeserializeData_m8_910(__this, L_7, L_8, /*hidden argument*/NULL);
	}

IL_0032:
	{
		IL2CPP_RUNTIME_CLASS_INIT(PhotonNetwork_t8_114_il2cpp_TypeInfo_var);
		double L_9 = PhotonNetwork_get_time_m8_646(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->___m_LastSerializeTime_2 = L_9;
		__this->___m_UpdatedPositionAfterOnSerialize_7 = 0;
		return;
	}
}
// System.Void PhotonTransformViewPositionControl::SerializeData(UnityEngine.Vector3,PhotonStream,PhotonMessageInfo)
extern TypeInfo* Vector3_t6_49_il2cpp_TypeInfo_var;
extern TypeInfo* Single_t1_17_il2cpp_TypeInfo_var;
extern "C" void PhotonTransformViewPositionControl_SerializeData_m8_909 (PhotonTransformViewPositionControl_t8_141 * __this, Vector3_t6_49  ___currentPosition, PhotonStream_t8_106 * ___stream, PhotonMessageInfo_t8_104 * ___info, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Vector3_t6_49_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(941);
		Single_t1_17_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(43);
		s_Il2CppMethodIntialized = true;
	}
	{
		PhotonStream_t8_106 * L_0 = ___stream;
		Vector3_t6_49  L_1 = ___currentPosition;
		Vector3_t6_49  L_2 = L_1;
		Object_t * L_3 = Box(Vector3_t6_49_il2cpp_TypeInfo_var, &L_2);
		NullCheck(L_0);
		PhotonStream_SendNext_m8_543(L_0, L_3, /*hidden argument*/NULL);
		Vector3_t6_49  L_4 = ___currentPosition;
		__this->___m_NetworkPosition_5 = L_4;
		PhotonTransformViewPositionModel_t8_138 * L_5 = (__this->___m_Model_0);
		NullCheck(L_5);
		int32_t L_6 = (L_5->___ExtrapolateOption_9);
		if ((((int32_t)L_6) == ((int32_t)1)))
		{
			goto IL_0035;
		}
	}
	{
		PhotonTransformViewPositionModel_t8_138 * L_7 = (__this->___m_Model_0);
		NullCheck(L_7);
		int32_t L_8 = (L_7->___InterpolateOption_3);
		if ((!(((uint32_t)L_8) == ((uint32_t)3))))
		{
			goto IL_0057;
		}
	}

IL_0035:
	{
		PhotonStream_t8_106 * L_9 = ___stream;
		Vector3_t6_49  L_10 = (__this->___m_SynchronizedSpeed_3);
		Vector3_t6_49  L_11 = L_10;
		Object_t * L_12 = Box(Vector3_t6_49_il2cpp_TypeInfo_var, &L_11);
		NullCheck(L_9);
		PhotonStream_SendNext_m8_543(L_9, L_12, /*hidden argument*/NULL);
		PhotonStream_t8_106 * L_13 = ___stream;
		float L_14 = (__this->___m_SynchronizedTurnSpeed_4);
		float L_15 = L_14;
		Object_t * L_16 = Box(Single_t1_17_il2cpp_TypeInfo_var, &L_15);
		NullCheck(L_13);
		PhotonStream_SendNext_m8_543(L_13, L_16, /*hidden argument*/NULL);
	}

IL_0057:
	{
		return;
	}
}
// System.Void PhotonTransformViewPositionControl::DeserializeData(PhotonStream,PhotonMessageInfo)
extern TypeInfo* Vector3_t6_49_il2cpp_TypeInfo_var;
extern TypeInfo* Single_t1_17_il2cpp_TypeInfo_var;
extern const MethodInfo* Queue_1_Enqueue_m3_1056_MethodInfo_var;
extern const MethodInfo* Queue_1_Dequeue_m3_1057_MethodInfo_var;
extern "C" void PhotonTransformViewPositionControl_DeserializeData_m8_910 (PhotonTransformViewPositionControl_t8_141 * __this, PhotonStream_t8_106 * ___stream, PhotonMessageInfo_t8_104 * ___info, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Vector3_t6_49_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(941);
		Single_t1_17_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(43);
		Queue_1_Enqueue_m3_1056_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483956);
		Queue_1_Dequeue_m3_1057_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483957);
		s_Il2CppMethodIntialized = true;
	}
	{
		Queue_1_t3_190 * L_0 = (__this->___m_OldNetworkPositions_6);
		Vector3_t6_49  L_1 = (__this->___m_NetworkPosition_5);
		NullCheck(L_0);
		Queue_1_Enqueue_m3_1056(L_0, L_1, /*hidden argument*/Queue_1_Enqueue_m3_1056_MethodInfo_var);
		goto IL_0022;
	}

IL_0016:
	{
		Queue_1_t3_190 * L_2 = (__this->___m_OldNetworkPositions_6);
		NullCheck(L_2);
		Queue_1_Dequeue_m3_1057(L_2, /*hidden argument*/Queue_1_Dequeue_m3_1057_MethodInfo_var);
	}

IL_0022:
	{
		Queue_1_t3_190 * L_3 = (__this->___m_OldNetworkPositions_6);
		NullCheck(L_3);
		int32_t L_4 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(6 /* System.Int32 System.Collections.Generic.Queue`1<UnityEngine.Vector3>::get_Count() */, L_3);
		PhotonTransformViewPositionModel_t8_138 * L_5 = (__this->___m_Model_0);
		NullCheck(L_5);
		int32_t L_6 = (L_5->___ExtrapolateNumberOfStoredPositions_12);
		if ((((int32_t)L_4) > ((int32_t)L_6)))
		{
			goto IL_0016;
		}
	}
	{
		PhotonStream_t8_106 * L_7 = ___stream;
		NullCheck(L_7);
		Object_t * L_8 = PhotonStream_ReceiveNext_m8_541(L_7, /*hidden argument*/NULL);
		__this->___m_NetworkPosition_5 = ((*(Vector3_t6_49 *)((Vector3_t6_49 *)UnBox (L_8, Vector3_t6_49_il2cpp_TypeInfo_var))));
		PhotonTransformViewPositionModel_t8_138 * L_9 = (__this->___m_Model_0);
		NullCheck(L_9);
		int32_t L_10 = (L_9->___ExtrapolateOption_9);
		if ((((int32_t)L_10) == ((int32_t)1)))
		{
			goto IL_0070;
		}
	}
	{
		PhotonTransformViewPositionModel_t8_138 * L_11 = (__this->___m_Model_0);
		NullCheck(L_11);
		int32_t L_12 = (L_11->___InterpolateOption_3);
		if ((!(((uint32_t)L_12) == ((uint32_t)3))))
		{
			goto IL_0092;
		}
	}

IL_0070:
	{
		PhotonStream_t8_106 * L_13 = ___stream;
		NullCheck(L_13);
		Object_t * L_14 = PhotonStream_ReceiveNext_m8_541(L_13, /*hidden argument*/NULL);
		__this->___m_SynchronizedSpeed_3 = ((*(Vector3_t6_49 *)((Vector3_t6_49 *)UnBox (L_14, Vector3_t6_49_il2cpp_TypeInfo_var))));
		PhotonStream_t8_106 * L_15 = ___stream;
		NullCheck(L_15);
		Object_t * L_16 = PhotonStream_ReceiveNext_m8_541(L_15, /*hidden argument*/NULL);
		__this->___m_SynchronizedTurnSpeed_4 = ((*(float*)((float*)UnBox (L_16, Single_t1_17_il2cpp_TypeInfo_var))));
	}

IL_0092:
	{
		return;
	}
}
// System.Void PhotonTransformViewPositionModel::.ctor()
extern TypeInfo* KeyframeU5BU5D_t6_259_il2cpp_TypeInfo_var;
extern TypeInfo* AnimationCurve_t6_132_il2cpp_TypeInfo_var;
extern "C" void PhotonTransformViewPositionModel__ctor_m8_911 (PhotonTransformViewPositionModel_t8_138 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		KeyframeU5BU5D_t6_259_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1186);
		AnimationCurve_t6_132_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1188);
		s_Il2CppMethodIntialized = true;
	}
	{
		__this->___TeleportEnabled_1 = 1;
		__this->___TeleportIfDistanceGreaterThan_2 = (3.0f);
		__this->___InterpolateOption_3 = 2;
		__this->___InterpolateMoveTowardsSpeed_4 = (1.0f);
		__this->___InterpolateLerpSpeed_5 = (1.0f);
		__this->___InterpolateMoveTowardsAcceleration_6 = (2.0f);
		__this->___InterpolateMoveTowardsDeceleration_7 = (2.0f);
		KeyframeU5BU5D_t6_259* L_0 = ((KeyframeU5BU5D_t6_259*)SZArrayNew(KeyframeU5BU5D_t6_259_il2cpp_TypeInfo_var, 4));
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, 0);
		Keyframe_t6_131  L_1 = {0};
		Keyframe__ctor_m6_792(&L_1, (-1.0f), (0.0f), (0.0f), (std::numeric_limits<float>::infinity()), /*hidden argument*/NULL);
		(*(Keyframe_t6_131 *)((Keyframe_t6_131 *)(Keyframe_t6_131 *)SZArrayLdElema(L_0, 0, sizeof(Keyframe_t6_131 )))) = L_1;
		KeyframeU5BU5D_t6_259* L_2 = L_0;
		NullCheck(L_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_2, 1);
		Keyframe_t6_131  L_3 = {0};
		Keyframe__ctor_m6_792(&L_3, (0.0f), (1.0f), (0.0f), (0.0f), /*hidden argument*/NULL);
		(*(Keyframe_t6_131 *)((Keyframe_t6_131 *)(Keyframe_t6_131 *)SZArrayLdElema(L_2, 1, sizeof(Keyframe_t6_131 )))) = L_3;
		KeyframeU5BU5D_t6_259* L_4 = L_2;
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, 2);
		Keyframe_t6_131  L_5 = {0};
		Keyframe__ctor_m6_792(&L_5, (1.0f), (1.0f), (0.0f), (1.0f), /*hidden argument*/NULL);
		(*(Keyframe_t6_131 *)((Keyframe_t6_131 *)(Keyframe_t6_131 *)SZArrayLdElema(L_4, 2, sizeof(Keyframe_t6_131 )))) = L_5;
		KeyframeU5BU5D_t6_259* L_6 = L_4;
		NullCheck(L_6);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_6, 3);
		Keyframe_t6_131  L_7 = {0};
		Keyframe__ctor_m6_792(&L_7, (4.0f), (4.0f), (1.0f), (0.0f), /*hidden argument*/NULL);
		(*(Keyframe_t6_131 *)((Keyframe_t6_131 *)(Keyframe_t6_131 *)SZArrayLdElema(L_6, 3, sizeof(Keyframe_t6_131 )))) = L_7;
		AnimationCurve_t6_132 * L_8 = (AnimationCurve_t6_132 *)il2cpp_codegen_object_new (AnimationCurve_t6_132_il2cpp_TypeInfo_var);
		AnimationCurve__ctor_m6_793(L_8, L_6, /*hidden argument*/NULL);
		__this->___InterpolateSpeedCurve_8 = L_8;
		__this->___ExtrapolateSpeed_10 = (1.0f);
		__this->___ExtrapolateIncludingRoundTripTime_11 = 1;
		__this->___ExtrapolateNumberOfStoredPositions_12 = 1;
		__this->___DrawErrorGizmo_13 = 1;
		Object__ctor_m1_0(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void PhotonTransformViewRotationControl::.ctor(PhotonTransformViewRotationModel)
extern "C" void PhotonTransformViewRotationControl__ctor_m8_912 (PhotonTransformViewRotationControl_t8_142 * __this, PhotonTransformViewRotationModel_t8_139 * ___model, const MethodInfo* method)
{
	{
		Object__ctor_m1_0(__this, /*hidden argument*/NULL);
		PhotonTransformViewRotationModel_t8_139 * L_0 = ___model;
		__this->___m_Model_0 = L_0;
		return;
	}
}
// UnityEngine.Quaternion PhotonTransformViewRotationControl::GetRotation(UnityEngine.Quaternion)
extern "C" Quaternion_t6_51  PhotonTransformViewRotationControl_GetRotation_m8_913 (PhotonTransformViewRotationControl_t8_142 * __this, Quaternion_t6_51  ___currentRotation, const MethodInfo* method)
{
	int32_t V_0 = {0};
	{
		PhotonTransformViewRotationModel_t8_139 * L_0 = (__this->___m_Model_0);
		NullCheck(L_0);
		int32_t L_1 = (L_0->___InterpolateOption_1);
		V_0 = L_1;
		int32_t L_2 = V_0;
		if (L_2 == 0)
		{
			goto IL_0023;
		}
		if (L_2 == 1)
		{
			goto IL_002a;
		}
		if (L_2 == 2)
		{
			goto IL_0048;
		}
	}
	{
		goto IL_0023;
	}

IL_0023:
	{
		Quaternion_t6_51  L_3 = (__this->___m_NetworkRotation_1);
		return L_3;
	}

IL_002a:
	{
		Quaternion_t6_51  L_4 = ___currentRotation;
		Quaternion_t6_51  L_5 = (__this->___m_NetworkRotation_1);
		PhotonTransformViewRotationModel_t8_139 * L_6 = (__this->___m_Model_0);
		NullCheck(L_6);
		float L_7 = (L_6->___InterpolateRotateTowardsSpeed_2);
		float L_8 = Time_get_deltaTime_m6_654(NULL /*static, unused*/, /*hidden argument*/NULL);
		Quaternion_t6_51  L_9 = Quaternion_RotateTowards_m6_254(NULL /*static, unused*/, L_4, L_5, ((float)((float)L_7*(float)L_8)), /*hidden argument*/NULL);
		return L_9;
	}

IL_0048:
	{
		Quaternion_t6_51  L_10 = ___currentRotation;
		Quaternion_t6_51  L_11 = (__this->___m_NetworkRotation_1);
		PhotonTransformViewRotationModel_t8_139 * L_12 = (__this->___m_Model_0);
		NullCheck(L_12);
		float L_13 = (L_12->___InterpolateLerpSpeed_3);
		float L_14 = Time_get_deltaTime_m6_654(NULL /*static, unused*/, /*hidden argument*/NULL);
		Quaternion_t6_51  L_15 = Quaternion_Lerp_m6_252(NULL /*static, unused*/, L_10, L_11, ((float)((float)L_13*(float)L_14)), /*hidden argument*/NULL);
		return L_15;
	}
}
// System.Void PhotonTransformViewRotationControl::OnPhotonSerializeView(UnityEngine.Quaternion,PhotonStream,PhotonMessageInfo)
extern TypeInfo* Quaternion_t6_51_il2cpp_TypeInfo_var;
extern "C" void PhotonTransformViewRotationControl_OnPhotonSerializeView_m8_914 (PhotonTransformViewRotationControl_t8_142 * __this, Quaternion_t6_51  ___currentRotation, PhotonStream_t8_106 * ___stream, PhotonMessageInfo_t8_104 * ___info, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Quaternion_t6_51_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(943);
		s_Il2CppMethodIntialized = true;
	}
	{
		PhotonTransformViewRotationModel_t8_139 * L_0 = (__this->___m_Model_0);
		NullCheck(L_0);
		bool L_1 = (L_0->___SynchronizeEnabled_0);
		if (L_1)
		{
			goto IL_0011;
		}
	}
	{
		return;
	}

IL_0011:
	{
		PhotonStream_t8_106 * L_2 = ___stream;
		NullCheck(L_2);
		bool L_3 = PhotonStream_get_isWriting_m8_538(L_2, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_0034;
		}
	}
	{
		PhotonStream_t8_106 * L_4 = ___stream;
		Quaternion_t6_51  L_5 = ___currentRotation;
		Quaternion_t6_51  L_6 = L_5;
		Object_t * L_7 = Box(Quaternion_t6_51_il2cpp_TypeInfo_var, &L_6);
		NullCheck(L_4);
		PhotonStream_SendNext_m8_543(L_4, L_7, /*hidden argument*/NULL);
		Quaternion_t6_51  L_8 = ___currentRotation;
		__this->___m_NetworkRotation_1 = L_8;
		goto IL_0045;
	}

IL_0034:
	{
		PhotonStream_t8_106 * L_9 = ___stream;
		NullCheck(L_9);
		Object_t * L_10 = PhotonStream_ReceiveNext_m8_541(L_9, /*hidden argument*/NULL);
		__this->___m_NetworkRotation_1 = ((*(Quaternion_t6_51 *)((Quaternion_t6_51 *)UnBox (L_10, Quaternion_t6_51_il2cpp_TypeInfo_var))));
	}

IL_0045:
	{
		return;
	}
}
// System.Void PhotonTransformViewRotationModel::.ctor()
extern "C" void PhotonTransformViewRotationModel__ctor_m8_915 (PhotonTransformViewRotationModel_t8_139 * __this, const MethodInfo* method)
{
	{
		__this->___InterpolateOption_1 = 1;
		__this->___InterpolateRotateTowardsSpeed_2 = (180.0f);
		__this->___InterpolateLerpSpeed_3 = (5.0f);
		Object__ctor_m1_0(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void PhotonTransformViewScaleControl::.ctor(PhotonTransformViewScaleModel)
extern "C" void PhotonTransformViewScaleControl__ctor_m8_916 (PhotonTransformViewScaleControl_t8_143 * __this, PhotonTransformViewScaleModel_t8_140 * ___model, const MethodInfo* method)
{
	{
		Vector3_t6_49  L_0 = Vector3_get_one_m6_214(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->___m_NetworkScale_1 = L_0;
		Object__ctor_m1_0(__this, /*hidden argument*/NULL);
		PhotonTransformViewScaleModel_t8_140 * L_1 = ___model;
		__this->___m_Model_0 = L_1;
		return;
	}
}
// UnityEngine.Vector3 PhotonTransformViewScaleControl::GetScale(UnityEngine.Vector3)
extern "C" Vector3_t6_49  PhotonTransformViewScaleControl_GetScale_m8_917 (PhotonTransformViewScaleControl_t8_143 * __this, Vector3_t6_49  ___currentScale, const MethodInfo* method)
{
	int32_t V_0 = {0};
	{
		PhotonTransformViewScaleModel_t8_140 * L_0 = (__this->___m_Model_0);
		NullCheck(L_0);
		int32_t L_1 = (L_0->___InterpolateOption_1);
		V_0 = L_1;
		int32_t L_2 = V_0;
		if (L_2 == 0)
		{
			goto IL_0023;
		}
		if (L_2 == 1)
		{
			goto IL_002a;
		}
		if (L_2 == 2)
		{
			goto IL_0048;
		}
	}
	{
		goto IL_0023;
	}

IL_0023:
	{
		Vector3_t6_49  L_3 = (__this->___m_NetworkScale_1);
		return L_3;
	}

IL_002a:
	{
		Vector3_t6_49  L_4 = ___currentScale;
		Vector3_t6_49  L_5 = (__this->___m_NetworkScale_1);
		PhotonTransformViewScaleModel_t8_140 * L_6 = (__this->___m_Model_0);
		NullCheck(L_6);
		float L_7 = (L_6->___InterpolateMoveTowardsSpeed_2);
		float L_8 = Time_get_deltaTime_m6_654(NULL /*static, unused*/, /*hidden argument*/NULL);
		Vector3_t6_49  L_9 = Vector3_MoveTowards_m6_195(NULL /*static, unused*/, L_4, L_5, ((float)((float)L_7*(float)L_8)), /*hidden argument*/NULL);
		return L_9;
	}

IL_0048:
	{
		Vector3_t6_49  L_10 = ___currentScale;
		Vector3_t6_49  L_11 = (__this->___m_NetworkScale_1);
		PhotonTransformViewScaleModel_t8_140 * L_12 = (__this->___m_Model_0);
		NullCheck(L_12);
		float L_13 = (L_12->___InterpolateLerpSpeed_3);
		float L_14 = Time_get_deltaTime_m6_654(NULL /*static, unused*/, /*hidden argument*/NULL);
		Vector3_t6_49  L_15 = Vector3_Lerp_m6_192(NULL /*static, unused*/, L_10, L_11, ((float)((float)L_13*(float)L_14)), /*hidden argument*/NULL);
		return L_15;
	}
}
// System.Void PhotonTransformViewScaleControl::OnPhotonSerializeView(UnityEngine.Vector3,PhotonStream,PhotonMessageInfo)
extern TypeInfo* Vector3_t6_49_il2cpp_TypeInfo_var;
extern "C" void PhotonTransformViewScaleControl_OnPhotonSerializeView_m8_918 (PhotonTransformViewScaleControl_t8_143 * __this, Vector3_t6_49  ___currentScale, PhotonStream_t8_106 * ___stream, PhotonMessageInfo_t8_104 * ___info, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Vector3_t6_49_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(941);
		s_Il2CppMethodIntialized = true;
	}
	{
		PhotonTransformViewScaleModel_t8_140 * L_0 = (__this->___m_Model_0);
		NullCheck(L_0);
		bool L_1 = (L_0->___SynchronizeEnabled_0);
		if (L_1)
		{
			goto IL_0011;
		}
	}
	{
		return;
	}

IL_0011:
	{
		PhotonStream_t8_106 * L_2 = ___stream;
		NullCheck(L_2);
		bool L_3 = PhotonStream_get_isWriting_m8_538(L_2, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_0034;
		}
	}
	{
		PhotonStream_t8_106 * L_4 = ___stream;
		Vector3_t6_49  L_5 = ___currentScale;
		Vector3_t6_49  L_6 = L_5;
		Object_t * L_7 = Box(Vector3_t6_49_il2cpp_TypeInfo_var, &L_6);
		NullCheck(L_4);
		PhotonStream_SendNext_m8_543(L_4, L_7, /*hidden argument*/NULL);
		Vector3_t6_49  L_8 = ___currentScale;
		__this->___m_NetworkScale_1 = L_8;
		goto IL_0045;
	}

IL_0034:
	{
		PhotonStream_t8_106 * L_9 = ___stream;
		NullCheck(L_9);
		Object_t * L_10 = PhotonStream_ReceiveNext_m8_541(L_9, /*hidden argument*/NULL);
		__this->___m_NetworkScale_1 = ((*(Vector3_t6_49 *)((Vector3_t6_49 *)UnBox (L_10, Vector3_t6_49_il2cpp_TypeInfo_var))));
	}

IL_0045:
	{
		return;
	}
}
// System.Void PhotonTransformViewScaleModel::.ctor()
extern "C" void PhotonTransformViewScaleModel__ctor_m8_919 (PhotonTransformViewScaleModel_t8_140 * __this, const MethodInfo* method)
{
	{
		__this->___InterpolateMoveTowardsSpeed_2 = (1.0f);
		Object__ctor_m1_0(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void ConnectAndJoinRandom::.ctor()
extern "C" void ConnectAndJoinRandom__ctor_m8_920 (ConnectAndJoinRandom_t8_148 * __this, const MethodInfo* method)
{
	{
		__this->___AutoConnect_2 = 1;
		__this->___Version_3 = 1;
		__this->___ConnectInUpdate_4 = 1;
		MonoBehaviour__ctor_m8_494(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void ConnectAndJoinRandom::Start()
extern TypeInfo* PhotonNetwork_t8_114_il2cpp_TypeInfo_var;
extern "C" void ConnectAndJoinRandom_Start_m8_921 (ConnectAndJoinRandom_t8_148 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		PhotonNetwork_t8_114_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1058);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(PhotonNetwork_t8_114_il2cpp_TypeInfo_var);
		PhotonNetwork_set_autoJoinLobby_m8_630(NULL /*static, unused*/, 0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void ConnectAndJoinRandom::Update()
extern TypeInfo* PhotonNetwork_t8_114_il2cpp_TypeInfo_var;
extern TypeInfo* Byte_t1_11_il2cpp_TypeInfo_var;
extern TypeInfo* Int32_t1_3_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3513;
extern Il2CppCodeGenString* _stringLiteral49;
extern "C" void ConnectAndJoinRandom_Update_m8_922 (ConnectAndJoinRandom_t8_148 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		PhotonNetwork_t8_114_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1058);
		Byte_t1_11_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(22);
		Int32_t1_3_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(8);
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(11);
		_stringLiteral3513 = il2cpp_codegen_string_literal_from_index(3513);
		_stringLiteral49 = il2cpp_codegen_string_literal_from_index(49);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool L_0 = (__this->___ConnectInUpdate_4);
		if (!L_0)
		{
			goto IL_0056;
		}
	}
	{
		bool L_1 = (__this->___AutoConnect_2);
		if (!L_1)
		{
			goto IL_0056;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(PhotonNetwork_t8_114_il2cpp_TypeInfo_var);
		bool L_2 = PhotonNetwork_get_connected_m8_603(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (L_2)
		{
			goto IL_0056;
		}
	}
	{
		Debug_Log_m6_489(NULL /*static, unused*/, _stringLiteral3513, /*hidden argument*/NULL);
		__this->___ConnectInUpdate_4 = 0;
		uint8_t L_3 = (__this->___Version_3);
		uint8_t L_4 = L_3;
		Object_t * L_5 = Box(Byte_t1_11_il2cpp_TypeInfo_var, &L_4);
		int32_t L_6 = Application_get_loadedLevel_m6_446(NULL /*static, unused*/, /*hidden argument*/NULL);
		int32_t L_7 = L_6;
		Object_t * L_8 = Box(Int32_t1_3_il2cpp_TypeInfo_var, &L_7);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_9 = String_Concat_m1_417(NULL /*static, unused*/, L_5, _stringLiteral49, L_8, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(PhotonNetwork_t8_114_il2cpp_TypeInfo_var);
		PhotonNetwork_ConnectUsingSettings_m8_666(NULL /*static, unused*/, L_9, /*hidden argument*/NULL);
	}

IL_0056:
	{
		return;
	}
}
// System.Void ConnectAndJoinRandom::OnConnectedToMaster()
extern TypeInfo* PhotonNetwork_t8_114_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3514;
extern "C" void ConnectAndJoinRandom_OnConnectedToMaster_m8_923 (ConnectAndJoinRandom_t8_148 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		PhotonNetwork_t8_114_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1058);
		_stringLiteral3514 = il2cpp_codegen_string_literal_from_index(3514);
		s_Il2CppMethodIntialized = true;
	}
	{
		Debug_Log_m6_489(NULL /*static, unused*/, _stringLiteral3514, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(PhotonNetwork_t8_114_il2cpp_TypeInfo_var);
		PhotonNetwork_JoinRandomRoom_m8_683(NULL /*static, unused*/, /*hidden argument*/NULL);
		return;
	}
}
// System.Void ConnectAndJoinRandom::OnJoinedLobby()
extern TypeInfo* PhotonNetwork_t8_114_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3515;
extern "C" void ConnectAndJoinRandom_OnJoinedLobby_m8_924 (ConnectAndJoinRandom_t8_148 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		PhotonNetwork_t8_114_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1058);
		_stringLiteral3515 = il2cpp_codegen_string_literal_from_index(3515);
		s_Il2CppMethodIntialized = true;
	}
	{
		Debug_Log_m6_489(NULL /*static, unused*/, _stringLiteral3515, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(PhotonNetwork_t8_114_il2cpp_TypeInfo_var);
		PhotonNetwork_JoinRandomRoom_m8_683(NULL /*static, unused*/, /*hidden argument*/NULL);
		return;
	}
}
// System.Void ConnectAndJoinRandom::OnPhotonRandomJoinFailed()
extern TypeInfo* RoomOptions_t8_78_il2cpp_TypeInfo_var;
extern TypeInfo* PhotonNetwork_t8_114_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3516;
extern "C" void ConnectAndJoinRandom_OnPhotonRandomJoinFailed_m8_925 (ConnectAndJoinRandom_t8_148 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		RoomOptions_t8_78_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1090);
		PhotonNetwork_t8_114_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1058);
		_stringLiteral3516 = il2cpp_codegen_string_literal_from_index(3516);
		s_Il2CppMethodIntialized = true;
	}
	RoomOptions_t8_78 * V_0 = {0};
	{
		Debug_Log_m6_489(NULL /*static, unused*/, _stringLiteral3516, /*hidden argument*/NULL);
		RoomOptions_t8_78 * L_0 = (RoomOptions_t8_78 *)il2cpp_codegen_object_new (RoomOptions_t8_78_il2cpp_TypeInfo_var);
		RoomOptions__ctor_m8_528(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		RoomOptions_t8_78 * L_1 = V_0;
		NullCheck(L_1);
		L_1->___maxPlayers_2 = 4;
		RoomOptions_t8_78 * L_2 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(PhotonNetwork_t8_114_il2cpp_TypeInfo_var);
		PhotonNetwork_CreateRoom_m8_679(NULL /*static, unused*/, (String_t*)NULL, L_2, (TypedLobby_t8_79 *)NULL, /*hidden argument*/NULL);
		return;
	}
}
// System.Void ConnectAndJoinRandom::OnFailedToConnectToPhoton(DisconnectCause)
extern TypeInfo* DisconnectCause_t8_70_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3517;
extern "C" void ConnectAndJoinRandom_OnFailedToConnectToPhoton_m8_926 (ConnectAndJoinRandom_t8_148 * __this, int32_t ___cause, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DisconnectCause_t8_70_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1134);
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(11);
		_stringLiteral3517 = il2cpp_codegen_string_literal_from_index(3517);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = ___cause;
		int32_t L_1 = L_0;
		Object_t * L_2 = Box(DisconnectCause_t8_70_il2cpp_TypeInfo_var, &L_1);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_3 = String_Concat_m1_416(NULL /*static, unused*/, _stringLiteral3517, L_2, /*hidden argument*/NULL);
		Debug_LogError_m6_490(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		return;
	}
}
// System.Void ConnectAndJoinRandom::OnJoinedRoom()
extern Il2CppCodeGenString* _stringLiteral3518;
extern "C" void ConnectAndJoinRandom_OnJoinedRoom_m8_927 (ConnectAndJoinRandom_t8_148 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		_stringLiteral3518 = il2cpp_codegen_string_literal_from_index(3518);
		s_Il2CppMethodIntialized = true;
	}
	{
		Debug_Log_m6_489(NULL /*static, unused*/, _stringLiteral3518, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HighlightOwnedGameObj::.ctor()
extern "C" void HighlightOwnedGameObj__ctor_m8_928 (HighlightOwnedGameObj_t8_149 * __this, const MethodInfo* method)
{
	{
		__this->___Offset_3 = (0.5f);
		MonoBehaviour__ctor_m8_494(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HighlightOwnedGameObj::Update()
extern const MethodInfo* Object_Instantiate_TisGameObject_t6_85_m6_1689_MethodInfo_var;
extern "C" void HighlightOwnedGameObj_Update_m8_929 (HighlightOwnedGameObj_t8_149 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Object_Instantiate_TisGameObject_t6_85_m6_1689_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483958);
		s_Il2CppMethodIntialized = true;
	}
	GameObject_t6_85 * V_0 = {0};
	Vector3_t6_49  V_1 = {0};
	{
		PhotonView_t8_3 * L_0 = MonoBehaviour_get_photonView_m8_495(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		bool L_1 = PhotonView_get_isMine_m8_774(L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_00a1;
		}
	}
	{
		Transform_t6_61 * L_2 = (__this->___markerTransform_4);
		bool L_3 = Object_op_Equality_m6_579(NULL /*static, unused*/, L_2, (Object_t6_5 *)NULL, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_004f;
		}
	}
	{
		GameObject_t6_85 * L_4 = (__this->___PointerPrefab_2);
		GameObject_t6_85 * L_5 = Object_Instantiate_TisGameObject_t6_85_m6_1689(NULL /*static, unused*/, L_4, /*hidden argument*/Object_Instantiate_TisGameObject_t6_85_m6_1689_MethodInfo_var);
		V_0 = L_5;
		GameObject_t6_85 * L_6 = V_0;
		NullCheck(L_6);
		Transform_t6_61 * L_7 = GameObject_get_transform_m6_595(L_6, /*hidden argument*/NULL);
		GameObject_t6_85 * L_8 = Component_get_gameObject_m6_583(__this, /*hidden argument*/NULL);
		NullCheck(L_8);
		Transform_t6_61 * L_9 = GameObject_get_transform_m6_595(L_8, /*hidden argument*/NULL);
		NullCheck(L_7);
		Transform_set_parent_m6_636(L_7, L_9, /*hidden argument*/NULL);
		GameObject_t6_85 * L_10 = V_0;
		NullCheck(L_10);
		Transform_t6_61 * L_11 = GameObject_get_transform_m6_595(L_10, /*hidden argument*/NULL);
		__this->___markerTransform_4 = L_11;
	}

IL_004f:
	{
		GameObject_t6_85 * L_12 = Component_get_gameObject_m6_583(__this, /*hidden argument*/NULL);
		NullCheck(L_12);
		Transform_t6_61 * L_13 = GameObject_get_transform_m6_595(L_12, /*hidden argument*/NULL);
		NullCheck(L_13);
		Vector3_t6_49  L_14 = Transform_get_position_m6_611(L_13, /*hidden argument*/NULL);
		V_1 = L_14;
		Transform_t6_61 * L_15 = (__this->___markerTransform_4);
		float L_16 = ((&V_1)->___x_1);
		float L_17 = ((&V_1)->___y_2);
		float L_18 = (__this->___Offset_3);
		float L_19 = ((&V_1)->___z_3);
		Vector3_t6_49  L_20 = {0};
		Vector3__ctor_m6_191(&L_20, L_16, ((float)((float)L_17+(float)L_18)), L_19, /*hidden argument*/NULL);
		NullCheck(L_15);
		Transform_set_position_m6_612(L_15, L_20, /*hidden argument*/NULL);
		Transform_t6_61 * L_21 = (__this->___markerTransform_4);
		Quaternion_t6_51  L_22 = Quaternion_get_identity_m6_244(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_21);
		Transform_set_rotation_m6_624(L_21, L_22, /*hidden argument*/NULL);
		goto IL_00c9;
	}

IL_00a1:
	{
		Transform_t6_61 * L_23 = (__this->___markerTransform_4);
		bool L_24 = Object_op_Inequality_m6_580(NULL /*static, unused*/, L_23, (Object_t6_5 *)NULL, /*hidden argument*/NULL);
		if (!L_24)
		{
			goto IL_00c9;
		}
	}
	{
		Transform_t6_61 * L_25 = (__this->___markerTransform_4);
		NullCheck(L_25);
		GameObject_t6_85 * L_26 = Component_get_gameObject_m6_583(L_25, /*hidden argument*/NULL);
		Object_Destroy_m6_558(NULL /*static, unused*/, L_26, /*hidden argument*/NULL);
		__this->___markerTransform_4 = (Transform_t6_61 *)NULL;
	}

IL_00c9:
	{
		return;
	}
}
// System.Void InRoomChat::.ctor()
extern TypeInfo* List_1_t1_829_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1__ctor_m1_5519_MethodInfo_var;
extern "C" void InRoomChat__ctor_m8_930 (InRoomChat_t8_150 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		List_1_t1_829_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(34);
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(11);
		List_1__ctor_m1_5519_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483649);
		s_Il2CppMethodIntialized = true;
	}
	{
		Rect_t6_52  L_0 = {0};
		Rect__ctor_m6_270(&L_0, (0.0f), (0.0f), (250.0f), (300.0f), /*hidden argument*/NULL);
		__this->___GuiRect_2 = L_0;
		__this->___IsVisible_3 = 1;
		List_1_t1_829 * L_1 = (List_1_t1_829 *)il2cpp_codegen_object_new (List_1_t1_829_il2cpp_TypeInfo_var);
		List_1__ctor_m1_5519(L_1, /*hidden argument*/List_1__ctor_m1_5519_MethodInfo_var);
		__this->___messages_5 = L_1;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_2 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->___Empty_2;
		__this->___inputLine_6 = L_2;
		Vector2_t6_48  L_3 = Vector2_get_zero_m6_182(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->___scrollPos_7 = L_3;
		MonoBehaviour__ctor_m8_494(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void InRoomChat::.cctor()
extern TypeInfo* InRoomChat_t8_150_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3085;
extern "C" void InRoomChat__cctor_m8_931 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		InRoomChat_t8_150_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1094);
		_stringLiteral3085 = il2cpp_codegen_string_literal_from_index(3085);
		s_Il2CppMethodIntialized = true;
	}
	{
		((InRoomChat_t8_150_StaticFields*)InRoomChat_t8_150_il2cpp_TypeInfo_var->static_fields)->___ChatRPC_8 = _stringLiteral3085;
		return;
	}
}
// System.Void InRoomChat::Start()
extern "C" void InRoomChat_Start_m8_932 (InRoomChat_t8_150 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (__this->___AlignBottom_4);
		if (!L_0)
		{
			goto IL_0028;
		}
	}
	{
		Rect_t6_52 * L_1 = &(__this->___GuiRect_2);
		int32_t L_2 = Screen_get_height_m6_122(NULL /*static, unused*/, /*hidden argument*/NULL);
		Rect_t6_52 * L_3 = &(__this->___GuiRect_2);
		float L_4 = Rect_get_height_m6_279(L_3, /*hidden argument*/NULL);
		Rect_set_y_m6_276(L_1, ((float)((float)(((float)((float)L_2)))-(float)L_4)), /*hidden argument*/NULL);
	}

IL_0028:
	{
		return;
	}
}
// System.Void InRoomChat::OnGUI()
extern TypeInfo* PhotonNetwork_t8_114_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* ObjectU5BU5D_t1_157_il2cpp_TypeInfo_var;
extern TypeInfo* GUI_t6_160_il2cpp_TypeInfo_var;
extern TypeInfo* GUILayoutOptionU5BU5D_t6_165_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3085;
extern Il2CppCodeGenString* _stringLiteral3008;
extern Il2CppCodeGenString* _stringLiteral3013;
extern "C" void InRoomChat_OnGUI_m8_933 (InRoomChat_t8_150 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		PhotonNetwork_t8_114_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1058);
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(11);
		ObjectU5BU5D_t1_157_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(19);
		GUI_t6_160_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(982);
		GUILayoutOptionU5BU5D_t6_165_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(991);
		_stringLiteral3085 = il2cpp_codegen_string_literal_from_index(3085);
		_stringLiteral3008 = il2cpp_codegen_string_literal_from_index(3008);
		_stringLiteral3013 = il2cpp_codegen_string_literal_from_index(3013);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	{
		bool L_0 = (__this->___IsVisible_3);
		if (!L_0)
		{
			goto IL_0017;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(PhotonNetwork_t8_114_il2cpp_TypeInfo_var);
		int32_t L_1 = PhotonNetwork_get_connectionStateDetailed_m8_607(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((((int32_t)L_1) == ((int32_t)((int32_t)9))))
		{
			goto IL_0018;
		}
	}

IL_0017:
	{
		return;
	}

IL_0018:
	{
		Event_t6_154 * L_2 = Event_get_current_m6_989(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_2);
		int32_t L_3 = Event_get_type_m6_1003(L_2, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_3) == ((uint32_t)4))))
		{
			goto IL_009d;
		}
	}
	{
		Event_t6_154 * L_4 = Event_get_current_m6_989(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_4);
		int32_t L_5 = Event_get_keyCode_m6_1024(L_4, /*hidden argument*/NULL);
		if ((((int32_t)L_5) == ((int32_t)((int32_t)271))))
		{
			goto IL_004d;
		}
	}
	{
		Event_t6_154 * L_6 = Event_get_current_m6_989(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_6);
		int32_t L_7 = Event_get_keyCode_m6_1024(L_6, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_7) == ((uint32_t)((int32_t)13)))))
		{
			goto IL_009d;
		}
	}

IL_004d:
	{
		String_t* L_8 = (__this->___inputLine_6);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_9 = String_IsNullOrEmpty_m1_397(NULL /*static, unused*/, L_8, /*hidden argument*/NULL);
		if (L_9)
		{
			goto IL_0093;
		}
	}
	{
		PhotonView_t8_3 * L_10 = MonoBehaviour_get_photonView_m8_495(__this, /*hidden argument*/NULL);
		ObjectU5BU5D_t1_157* L_11 = ((ObjectU5BU5D_t1_157*)SZArrayNew(ObjectU5BU5D_t1_157_il2cpp_TypeInfo_var, 1));
		String_t* L_12 = (__this->___inputLine_6);
		NullCheck(L_11);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_11, 0);
		ArrayElementTypeCheck (L_11, L_12);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_11, 0, sizeof(Object_t *))) = (Object_t *)L_12;
		NullCheck(L_10);
		PhotonView_RPC_m8_786(L_10, _stringLiteral3085, 0, L_11, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_13 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->___Empty_2;
		__this->___inputLine_6 = L_13;
		String_t* L_14 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->___Empty_2;
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t6_160_il2cpp_TypeInfo_var);
		GUI_FocusControl_m6_1090(NULL /*static, unused*/, L_14, /*hidden argument*/NULL);
		return;
	}

IL_0093:
	{
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t6_160_il2cpp_TypeInfo_var);
		GUI_FocusControl_m6_1090(NULL /*static, unused*/, _stringLiteral3008, /*hidden argument*/NULL);
	}

IL_009d:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_15 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->___Empty_2;
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t6_160_il2cpp_TypeInfo_var);
		GUI_SetNextControlName_m6_1088(NULL /*static, unused*/, L_15, /*hidden argument*/NULL);
		Rect_t6_52  L_16 = (__this->___GuiRect_2);
		GUILayout_BeginArea_m6_1133(NULL /*static, unused*/, L_16, /*hidden argument*/NULL);
		Vector2_t6_48  L_17 = (__this->___scrollPos_7);
		Vector2_t6_48  L_18 = GUILayout_BeginScrollView_m6_1137(NULL /*static, unused*/, L_17, ((GUILayoutOptionU5BU5D_t6_165*)SZArrayNew(GUILayoutOptionU5BU5D_t6_165_il2cpp_TypeInfo_var, 0)), /*hidden argument*/NULL);
		__this->___scrollPos_7 = L_18;
		GUILayout_FlexibleSpace_m6_1126(NULL /*static, unused*/, /*hidden argument*/NULL);
		List_1_t1_829 * L_19 = (__this->___messages_5);
		NullCheck(L_19);
		int32_t L_20 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(17 /* System.Int32 System.Collections.Generic.List`1<System.String>::get_Count() */, L_19);
		V_0 = ((int32_t)((int32_t)L_20-(int32_t)1));
		goto IL_00fc;
	}

IL_00e1:
	{
		List_1_t1_829 * L_21 = (__this->___messages_5);
		int32_t L_22 = V_0;
		NullCheck(L_21);
		String_t* L_23 = (String_t*)VirtFuncInvoker1< String_t*, int32_t >::Invoke(28 /* !0 System.Collections.Generic.List`1<System.String>::get_Item(System.Int32) */, L_21, L_22);
		GUILayout_Label_m6_1112(NULL /*static, unused*/, L_23, ((GUILayoutOptionU5BU5D_t6_165*)SZArrayNew(GUILayoutOptionU5BU5D_t6_165_il2cpp_TypeInfo_var, 0)), /*hidden argument*/NULL);
		int32_t L_24 = V_0;
		V_0 = ((int32_t)((int32_t)L_24-(int32_t)1));
	}

IL_00fc:
	{
		int32_t L_25 = V_0;
		if ((((int32_t)L_25) >= ((int32_t)0)))
		{
			goto IL_00e1;
		}
	}
	{
		GUILayout_EndScrollView_m6_1139(NULL /*static, unused*/, /*hidden argument*/NULL);
		GUILayout_BeginHorizontal_m6_1127(NULL /*static, unused*/, ((GUILayoutOptionU5BU5D_t6_165*)SZArrayNew(GUILayoutOptionU5BU5D_t6_165_il2cpp_TypeInfo_var, 0)), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t6_160_il2cpp_TypeInfo_var);
		GUI_SetNextControlName_m6_1088(NULL /*static, unused*/, _stringLiteral3008, /*hidden argument*/NULL);
		String_t* L_26 = (__this->___inputLine_6);
		String_t* L_27 = GUILayout_TextField_m6_1117(NULL /*static, unused*/, L_26, ((GUILayoutOptionU5BU5D_t6_165*)SZArrayNew(GUILayoutOptionU5BU5D_t6_165_il2cpp_TypeInfo_var, 0)), /*hidden argument*/NULL);
		__this->___inputLine_6 = L_27;
		GUILayoutOptionU5BU5D_t6_165* L_28 = ((GUILayoutOptionU5BU5D_t6_165*)SZArrayNew(GUILayoutOptionU5BU5D_t6_165_il2cpp_TypeInfo_var, 1));
		GUILayoutOption_t6_176 * L_29 = GUILayout_ExpandWidth_m6_1146(NULL /*static, unused*/, 0, /*hidden argument*/NULL);
		NullCheck(L_28);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_28, 0);
		ArrayElementTypeCheck (L_28, L_29);
		*((GUILayoutOption_t6_176 **)(GUILayoutOption_t6_176 **)SZArrayLdElema(L_28, 0, sizeof(GUILayoutOption_t6_176 *))) = (GUILayoutOption_t6_176 *)L_29;
		bool L_30 = GUILayout_Button_m6_1115(NULL /*static, unused*/, _stringLiteral3013, L_28, /*hidden argument*/NULL);
		if (!L_30)
		{
			goto IL_0187;
		}
	}
	{
		PhotonView_t8_3 * L_31 = MonoBehaviour_get_photonView_m8_495(__this, /*hidden argument*/NULL);
		ObjectU5BU5D_t1_157* L_32 = ((ObjectU5BU5D_t1_157*)SZArrayNew(ObjectU5BU5D_t1_157_il2cpp_TypeInfo_var, 1));
		String_t* L_33 = (__this->___inputLine_6);
		NullCheck(L_32);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_32, 0);
		ArrayElementTypeCheck (L_32, L_33);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_32, 0, sizeof(Object_t *))) = (Object_t *)L_33;
		NullCheck(L_31);
		PhotonView_RPC_m8_786(L_31, _stringLiteral3085, 0, L_32, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_34 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->___Empty_2;
		__this->___inputLine_6 = L_34;
		String_t* L_35 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->___Empty_2;
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t6_160_il2cpp_TypeInfo_var);
		GUI_FocusControl_m6_1090(NULL /*static, unused*/, L_35, /*hidden argument*/NULL);
	}

IL_0187:
	{
		GUILayout_EndHorizontal_m6_1129(NULL /*static, unused*/, /*hidden argument*/NULL);
		GUILayout_EndArea_m6_1136(NULL /*static, unused*/, /*hidden argument*/NULL);
		return;
	}
}
// System.Void InRoomChat::Chat(System.String,PhotonMessageInfo)
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* Int32_t1_3_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3519;
extern Il2CppCodeGenString* _stringLiteral3520;
extern Il2CppCodeGenString* _stringLiteral225;
extern "C" void InRoomChat_Chat_m8_934 (InRoomChat_t8_150 * __this, String_t* ___newLine, PhotonMessageInfo_t8_104 * ___mi, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(11);
		Int32_t1_3_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(8);
		_stringLiteral3519 = il2cpp_codegen_string_literal_from_index(3519);
		_stringLiteral3520 = il2cpp_codegen_string_literal_from_index(3520);
		_stringLiteral225 = il2cpp_codegen_string_literal_from_index(225);
		s_Il2CppMethodIntialized = true;
	}
	String_t* V_0 = {0};
	{
		V_0 = _stringLiteral3519;
		PhotonMessageInfo_t8_104 * L_0 = ___mi;
		if (!L_0)
		{
			goto IL_0058;
		}
	}
	{
		PhotonMessageInfo_t8_104 * L_1 = ___mi;
		NullCheck(L_1);
		PhotonPlayer_t8_102 * L_2 = (L_1->___sender_1);
		if (!L_2)
		{
			goto IL_0058;
		}
	}
	{
		PhotonMessageInfo_t8_104 * L_3 = ___mi;
		NullCheck(L_3);
		PhotonPlayer_t8_102 * L_4 = (L_3->___sender_1);
		NullCheck(L_4);
		String_t* L_5 = PhotonPlayer_get_name_m8_731(L_4, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_6 = String_IsNullOrEmpty_m1_397(NULL /*static, unused*/, L_5, /*hidden argument*/NULL);
		if (L_6)
		{
			goto IL_003d;
		}
	}
	{
		PhotonMessageInfo_t8_104 * L_7 = ___mi;
		NullCheck(L_7);
		PhotonPlayer_t8_102 * L_8 = (L_7->___sender_1);
		NullCheck(L_8);
		String_t* L_9 = PhotonPlayer_get_name_m8_731(L_8, /*hidden argument*/NULL);
		V_0 = L_9;
		goto IL_0058;
	}

IL_003d:
	{
		PhotonMessageInfo_t8_104 * L_10 = ___mi;
		NullCheck(L_10);
		PhotonPlayer_t8_102 * L_11 = (L_10->___sender_1);
		NullCheck(L_11);
		int32_t L_12 = PhotonPlayer_get_ID_m8_730(L_11, /*hidden argument*/NULL);
		int32_t L_13 = L_12;
		Object_t * L_14 = Box(Int32_t1_3_il2cpp_TypeInfo_var, &L_13);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_15 = String_Concat_m1_416(NULL /*static, unused*/, _stringLiteral3520, L_14, /*hidden argument*/NULL);
		V_0 = L_15;
	}

IL_0058:
	{
		List_1_t1_829 * L_16 = (__this->___messages_5);
		String_t* L_17 = V_0;
		String_t* L_18 = ___newLine;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_19 = String_Concat_m1_419(NULL /*static, unused*/, L_17, _stringLiteral225, L_18, /*hidden argument*/NULL);
		NullCheck(L_16);
		VirtActionInvoker1< String_t* >::Invoke(19 /* System.Void System.Collections.Generic.List`1<System.String>::Add(!0) */, L_16, L_19);
		return;
	}
}
// System.Void InRoomChat::AddLine(System.String)
extern "C" void InRoomChat_AddLine_m8_935 (InRoomChat_t8_150 * __this, String_t* ___newLine, const MethodInfo* method)
{
	{
		List_1_t1_829 * L_0 = (__this->___messages_5);
		String_t* L_1 = ___newLine;
		NullCheck(L_0);
		VirtActionInvoker1< String_t* >::Invoke(19 /* System.Void System.Collections.Generic.List`1<System.String>::Add(!0) */, L_0, L_1);
		return;
	}
}
// System.Void InRoomRoundTimer::.ctor()
extern "C" void InRoomRoundTimer__ctor_m8_936 (InRoomRoundTimer_t8_151 * __this, const MethodInfo* method)
{
	{
		__this->___SecondsPerTurn_3 = 5;
		Rect_t6_52  L_0 = {0};
		Rect__ctor_m6_270(&L_0, (0.0f), (80.0f), (150.0f), (300.0f), /*hidden argument*/NULL);
		__this->___TextPos_5 = L_0;
		MonoBehaviour__ctor_m6_525(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void InRoomRoundTimer::StartRoundNow()
extern TypeInfo* PhotonNetwork_t8_114_il2cpp_TypeInfo_var;
extern TypeInfo* Hashtable_t5_1_il2cpp_TypeInfo_var;
extern TypeInfo* Double_t1_18_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3521;
extern "C" void InRoomRoundTimer_StartRoundNow_m8_937 (InRoomRoundTimer_t8_151 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		PhotonNetwork_t8_114_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1058);
		Hashtable_t5_1_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(891);
		Double_t1_18_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(44);
		_stringLiteral3521 = il2cpp_codegen_string_literal_from_index(3521);
		s_Il2CppMethodIntialized = true;
	}
	Hashtable_t5_1 * V_0 = {0};
	{
		IL2CPP_RUNTIME_CLASS_INIT(PhotonNetwork_t8_114_il2cpp_TypeInfo_var);
		double L_0 = PhotonNetwork_get_time_m8_646(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((!(((double)L_0) < ((double)(9.9999997473787516E-05)))))
		{
			goto IL_001b;
		}
	}
	{
		__this->___startRoundWhenTimeIsSynced_6 = 1;
		return;
	}

IL_001b:
	{
		__this->___startRoundWhenTimeIsSynced_6 = 0;
		Hashtable_t5_1 * L_1 = (Hashtable_t5_1 *)il2cpp_codegen_object_new (Hashtable_t5_1_il2cpp_TypeInfo_var);
		Hashtable__ctor_m5_0(L_1, /*hidden argument*/NULL);
		V_0 = L_1;
		Hashtable_t5_1 * L_2 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(PhotonNetwork_t8_114_il2cpp_TypeInfo_var);
		double L_3 = PhotonNetwork_get_time_m8_646(NULL /*static, unused*/, /*hidden argument*/NULL);
		double L_4 = L_3;
		Object_t * L_5 = Box(Double_t1_18_il2cpp_TypeInfo_var, &L_4);
		NullCheck(L_2);
		Hashtable_set_Item_m5_3(L_2, _stringLiteral3521, L_5, /*hidden argument*/NULL);
		Room_t8_100 * L_6 = PhotonNetwork_get_room_m8_611(NULL /*static, unused*/, /*hidden argument*/NULL);
		Hashtable_t5_1 * L_7 = V_0;
		NullCheck(L_6);
		Room_SetCustomProperties_m8_822(L_6, L_7, (Hashtable_t5_1 *)NULL, 0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void InRoomRoundTimer::OnJoinedRoom()
extern TypeInfo* PhotonNetwork_t8_114_il2cpp_TypeInfo_var;
extern TypeInfo* Boolean_t1_20_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3522;
extern Il2CppCodeGenString* _stringLiteral3521;
extern "C" void InRoomRoundTimer_OnJoinedRoom_m8_938 (InRoomRoundTimer_t8_151 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		PhotonNetwork_t8_114_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1058);
		Boolean_t1_20_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(49);
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(11);
		_stringLiteral3522 = il2cpp_codegen_string_literal_from_index(3522);
		_stringLiteral3521 = il2cpp_codegen_string_literal_from_index(3521);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(PhotonNetwork_t8_114_il2cpp_TypeInfo_var);
		bool L_0 = PhotonNetwork_get_isMasterClient_m8_648(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_0015;
		}
	}
	{
		InRoomRoundTimer_StartRoundNow_m8_937(__this, /*hidden argument*/NULL);
		goto IL_003d;
	}

IL_0015:
	{
		IL2CPP_RUNTIME_CLASS_INIT(PhotonNetwork_t8_114_il2cpp_TypeInfo_var);
		Room_t8_100 * L_1 = PhotonNetwork_get_room_m8_611(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_1);
		Hashtable_t5_1 * L_2 = RoomInfo_get_customProperties_m8_831(L_1, /*hidden argument*/NULL);
		NullCheck(L_2);
		bool L_3 = (bool)VirtFuncInvoker1< bool, Object_t * >::Invoke(28 /* System.Boolean System.Collections.Generic.Dictionary`2<System.Object,System.Object>::ContainsKey(!0) */, L_2, _stringLiteral3521);
		bool L_4 = L_3;
		Object_t * L_5 = Box(Boolean_t1_20_il2cpp_TypeInfo_var, &L_4);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_6 = String_Concat_m1_416(NULL /*static, unused*/, _stringLiteral3522, L_5, /*hidden argument*/NULL);
		Debug_Log_m6_489(NULL /*static, unused*/, L_6, /*hidden argument*/NULL);
	}

IL_003d:
	{
		return;
	}
}
// System.Void InRoomRoundTimer::OnPhotonCustomRoomPropertiesChanged(ExitGames.Client.Photon.Hashtable)
extern TypeInfo* Double_t1_18_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3521;
extern "C" void InRoomRoundTimer_OnPhotonCustomRoomPropertiesChanged_m8_939 (InRoomRoundTimer_t8_151 * __this, Hashtable_t5_1 * ___propertiesThatChanged, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Double_t1_18_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(44);
		_stringLiteral3521 = il2cpp_codegen_string_literal_from_index(3521);
		s_Il2CppMethodIntialized = true;
	}
	{
		Hashtable_t5_1 * L_0 = ___propertiesThatChanged;
		NullCheck(L_0);
		bool L_1 = (bool)VirtFuncInvoker1< bool, Object_t * >::Invoke(28 /* System.Boolean System.Collections.Generic.Dictionary`2<System.Object,System.Object>::ContainsKey(!0) */, L_0, _stringLiteral3521);
		if (!L_1)
		{
			goto IL_0026;
		}
	}
	{
		Hashtable_t5_1 * L_2 = ___propertiesThatChanged;
		NullCheck(L_2);
		Object_t * L_3 = Hashtable_get_Item_m5_2(L_2, _stringLiteral3521, /*hidden argument*/NULL);
		__this->___StartTime_4 = ((*(double*)((double*)UnBox (L_3, Double_t1_18_il2cpp_TypeInfo_var))));
	}

IL_0026:
	{
		return;
	}
}
// System.Void InRoomRoundTimer::OnMasterClientSwitched(PhotonPlayer)
extern TypeInfo* PhotonNetwork_t8_114_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3521;
extern Il2CppCodeGenString* _stringLiteral3523;
extern "C" void InRoomRoundTimer_OnMasterClientSwitched_m8_940 (InRoomRoundTimer_t8_151 * __this, PhotonPlayer_t8_102 * ___newMasterClient, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		PhotonNetwork_t8_114_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1058);
		_stringLiteral3521 = il2cpp_codegen_string_literal_from_index(3521);
		_stringLiteral3523 = il2cpp_codegen_string_literal_from_index(3523);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(PhotonNetwork_t8_114_il2cpp_TypeInfo_var);
		Room_t8_100 * L_0 = PhotonNetwork_get_room_m8_611(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_0);
		Hashtable_t5_1 * L_1 = RoomInfo_get_customProperties_m8_831(L_0, /*hidden argument*/NULL);
		NullCheck(L_1);
		bool L_2 = (bool)VirtFuncInvoker1< bool, Object_t * >::Invoke(28 /* System.Boolean System.Collections.Generic.Dictionary`2<System.Object,System.Object>::ContainsKey(!0) */, L_1, _stringLiteral3521);
		if (L_2)
		{
			goto IL_0029;
		}
	}
	{
		Debug_Log_m6_489(NULL /*static, unused*/, _stringLiteral3523, /*hidden argument*/NULL);
		InRoomRoundTimer_StartRoundNow_m8_937(__this, /*hidden argument*/NULL);
	}

IL_0029:
	{
		return;
	}
}
// System.Void InRoomRoundTimer::Update()
extern "C" void InRoomRoundTimer_Update_m8_941 (InRoomRoundTimer_t8_151 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (__this->___startRoundWhenTimeIsSynced_6);
		if (!L_0)
		{
			goto IL_0011;
		}
	}
	{
		InRoomRoundTimer_StartRoundNow_m8_937(__this, /*hidden argument*/NULL);
	}

IL_0011:
	{
		return;
	}
}
// System.Void InRoomRoundTimer::OnGUI()
extern TypeInfo* PhotonNetwork_t8_114_il2cpp_TypeInfo_var;
extern TypeInfo* Double_t1_18_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* GUILayoutOptionU5BU5D_t6_165_il2cpp_TypeInfo_var;
extern TypeInfo* Int32_t1_3_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3524;
extern Il2CppCodeGenString* _stringLiteral3525;
extern Il2CppCodeGenString* _stringLiteral3526;
extern Il2CppCodeGenString* _stringLiteral3527;
extern "C" void InRoomRoundTimer_OnGUI_m8_942 (InRoomRoundTimer_t8_151 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		PhotonNetwork_t8_114_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1058);
		Double_t1_18_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(44);
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(11);
		GUILayoutOptionU5BU5D_t6_165_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(991);
		Int32_t1_3_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(8);
		_stringLiteral3524 = il2cpp_codegen_string_literal_from_index(3524);
		_stringLiteral3525 = il2cpp_codegen_string_literal_from_index(3525);
		_stringLiteral3526 = il2cpp_codegen_string_literal_from_index(3526);
		_stringLiteral3527 = il2cpp_codegen_string_literal_from_index(3527);
		s_Il2CppMethodIntialized = true;
	}
	double V_0 = 0.0;
	double V_1 = 0.0;
	int32_t V_2 = 0;
	{
		IL2CPP_RUNTIME_CLASS_INIT(PhotonNetwork_t8_114_il2cpp_TypeInfo_var);
		double L_0 = PhotonNetwork_get_time_m8_646(NULL /*static, unused*/, /*hidden argument*/NULL);
		double L_1 = (__this->___StartTime_4);
		V_0 = ((double)((double)L_0-(double)L_1));
		int32_t L_2 = (__this->___SecondsPerTurn_3);
		double L_3 = V_0;
		int32_t L_4 = (__this->___SecondsPerTurn_3);
		V_1 = ((double)((double)(((double)((double)L_2)))-(double)(fmod(L_3, (((double)((double)L_4)))))));
		double L_5 = V_0;
		int32_t L_6 = (__this->___SecondsPerTurn_3);
		V_2 = (((int32_t)((int32_t)((double)((double)L_5/(double)(((double)((double)L_6))))))));
		Rect_t6_52  L_7 = (__this->___TextPos_5);
		GUILayout_BeginArea_m6_1133(NULL /*static, unused*/, L_7, /*hidden argument*/NULL);
		double L_8 = V_0;
		double L_9 = L_8;
		Object_t * L_10 = Box(Double_t1_18_il2cpp_TypeInfo_var, &L_9);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_11 = String_Format_m1_410(NULL /*static, unused*/, _stringLiteral3524, L_10, /*hidden argument*/NULL);
		GUILayout_Label_m6_1112(NULL /*static, unused*/, L_11, ((GUILayoutOptionU5BU5D_t6_165*)SZArrayNew(GUILayoutOptionU5BU5D_t6_165_il2cpp_TypeInfo_var, 0)), /*hidden argument*/NULL);
		double L_12 = V_1;
		double L_13 = L_12;
		Object_t * L_14 = Box(Double_t1_18_il2cpp_TypeInfo_var, &L_13);
		String_t* L_15 = String_Format_m1_410(NULL /*static, unused*/, _stringLiteral3525, L_14, /*hidden argument*/NULL);
		GUILayout_Label_m6_1112(NULL /*static, unused*/, L_15, ((GUILayoutOptionU5BU5D_t6_165*)SZArrayNew(GUILayoutOptionU5BU5D_t6_165_il2cpp_TypeInfo_var, 0)), /*hidden argument*/NULL);
		int32_t L_16 = V_2;
		int32_t L_17 = L_16;
		Object_t * L_18 = Box(Int32_t1_3_il2cpp_TypeInfo_var, &L_17);
		String_t* L_19 = String_Format_m1_410(NULL /*static, unused*/, _stringLiteral3526, L_18, /*hidden argument*/NULL);
		GUILayout_Label_m6_1112(NULL /*static, unused*/, L_19, ((GUILayoutOptionU5BU5D_t6_165*)SZArrayNew(GUILayoutOptionU5BU5D_t6_165_il2cpp_TypeInfo_var, 0)), /*hidden argument*/NULL);
		bool L_20 = GUILayout_Button_m6_1115(NULL /*static, unused*/, _stringLiteral3527, ((GUILayoutOptionU5BU5D_t6_165*)SZArrayNew(GUILayoutOptionU5BU5D_t6_165_il2cpp_TypeInfo_var, 0)), /*hidden argument*/NULL);
		if (!L_20)
		{
			goto IL_00a1;
		}
	}
	{
		InRoomRoundTimer_StartRoundNow_m8_937(__this, /*hidden argument*/NULL);
	}

IL_00a1:
	{
		GUILayout_EndArea_m6_1136(NULL /*static, unused*/, /*hidden argument*/NULL);
		return;
	}
}
// System.Void InputToEvent::.ctor()
extern "C" void InputToEvent__ctor_m8_943 (InputToEvent_t8_152 * __this, const MethodInfo* method)
{
	{
		Vector2_t6_48  L_0 = Vector2_get_zero_m6_182(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->___pressedPosition_5 = L_0;
		Vector2_t6_48  L_1 = Vector2_get_zero_m6_182(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->___currentPos_6 = L_1;
		MonoBehaviour__ctor_m6_525(__this, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.GameObject InputToEvent::get_goPointedAt()
extern TypeInfo* InputToEvent_t8_152_il2cpp_TypeInfo_var;
extern "C" GameObject_t6_85 * InputToEvent_get_goPointedAt_m8_944 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		InputToEvent_t8_152_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1063);
		s_Il2CppMethodIntialized = true;
	}
	{
		GameObject_t6_85 * L_0 = ((InputToEvent_t8_152_StaticFields*)InputToEvent_t8_152_il2cpp_TypeInfo_var->static_fields)->___U3CgoPointedAtU3Ek__BackingField_9;
		return L_0;
	}
}
// System.Void InputToEvent::set_goPointedAt(UnityEngine.GameObject)
extern TypeInfo* InputToEvent_t8_152_il2cpp_TypeInfo_var;
extern "C" void InputToEvent_set_goPointedAt_m8_945 (Object_t * __this /* static, unused */, GameObject_t6_85 * ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		InputToEvent_t8_152_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1063);
		s_Il2CppMethodIntialized = true;
	}
	{
		GameObject_t6_85 * L_0 = ___value;
		((InputToEvent_t8_152_StaticFields*)InputToEvent_t8_152_il2cpp_TypeInfo_var->static_fields)->___U3CgoPointedAtU3Ek__BackingField_9 = L_0;
		return;
	}
}
// UnityEngine.Vector2 InputToEvent::get_DragVector()
extern "C" Vector2_t6_48  InputToEvent_get_DragVector_m8_946 (InputToEvent_t8_152 * __this, const MethodInfo* method)
{
	Vector2_t6_48  G_B3_0 = {0};
	{
		bool L_0 = (__this->___Dragging_7);
		if (!L_0)
		{
			goto IL_0021;
		}
	}
	{
		Vector2_t6_48  L_1 = (__this->___currentPos_6);
		Vector2_t6_48  L_2 = (__this->___pressedPosition_5);
		Vector2_t6_48  L_3 = Vector2_op_Subtraction_m6_185(NULL /*static, unused*/, L_1, L_2, /*hidden argument*/NULL);
		G_B3_0 = L_3;
		goto IL_0026;
	}

IL_0021:
	{
		Vector2_t6_48  L_4 = Vector2_get_zero_m6_182(NULL /*static, unused*/, /*hidden argument*/NULL);
		G_B3_0 = L_4;
	}

IL_0026:
	{
		return G_B3_0;
	}
}
// System.Void InputToEvent::Start()
extern const MethodInfo* Component_GetComponent_TisCamera_t6_75_m6_1665_MethodInfo_var;
extern "C" void InputToEvent_Start_m8_947 (InputToEvent_t8_152 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Component_GetComponent_TisCamera_t6_75_m6_1665_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483848);
		s_Il2CppMethodIntialized = true;
	}
	{
		Camera_t6_75 * L_0 = Component_GetComponent_TisCamera_t6_75_m6_1665(__this, /*hidden argument*/Component_GetComponent_TisCamera_t6_75_m6_1665_MethodInfo_var);
		__this->___m_Camera_8 = L_0;
		return;
	}
}
// System.Void InputToEvent::Update()
extern TypeInfo* Input_t6_83_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3528;
extern "C" void InputToEvent_Update_m8_948 (InputToEvent_t8_152 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Input_t6_83_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(956);
		_stringLiteral3528 = il2cpp_codegen_string_literal_from_index(3528);
		s_Il2CppMethodIntialized = true;
	}
	Touch_t6_82  V_0 = {0};
	{
		bool L_0 = (__this->___DetectPointedAtGameObject_4);
		if (!L_0)
		{
			goto IL_0020;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t6_83_il2cpp_TypeInfo_var);
		Vector3_t6_49  L_1 = Input_get_mousePosition_m6_546(NULL /*static, unused*/, /*hidden argument*/NULL);
		Vector2_t6_48  L_2 = Vector2_op_Implicit_m6_189(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		GameObject_t6_85 * L_3 = InputToEvent_RaycastObject_m8_951(__this, L_2, /*hidden argument*/NULL);
		InputToEvent_set_goPointedAt_m8_945(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
	}

IL_0020:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t6_83_il2cpp_TypeInfo_var);
		int32_t L_4 = Input_get_touchCount_m6_549(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((((int32_t)L_4) <= ((int32_t)0)))
		{
			goto IL_0078;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t6_83_il2cpp_TypeInfo_var);
		Touch_t6_82  L_5 = Input_GetTouch_m6_548(NULL /*static, unused*/, 0, /*hidden argument*/NULL);
		V_0 = L_5;
		Vector2_t6_48  L_6 = Touch_get_position_m6_532((&V_0), /*hidden argument*/NULL);
		__this->___currentPos_6 = L_6;
		int32_t L_7 = Touch_get_phase_m6_533((&V_0), /*hidden argument*/NULL);
		if (L_7)
		{
			goto IL_005d;
		}
	}
	{
		Vector2_t6_48  L_8 = Touch_get_position_m6_532((&V_0), /*hidden argument*/NULL);
		InputToEvent_Press_m8_949(__this, L_8, /*hidden argument*/NULL);
		goto IL_0077;
	}

IL_005d:
	{
		int32_t L_9 = Touch_get_phase_m6_533((&V_0), /*hidden argument*/NULL);
		if ((!(((uint32_t)L_9) == ((uint32_t)3))))
		{
			goto IL_0077;
		}
	}
	{
		Vector2_t6_48  L_10 = Touch_get_position_m6_532((&V_0), /*hidden argument*/NULL);
		InputToEvent_Release_m8_950(__this, L_10, /*hidden argument*/NULL);
	}

IL_0077:
	{
		return;
	}

IL_0078:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t6_83_il2cpp_TypeInfo_var);
		Vector3_t6_49  L_11 = Input_get_mousePosition_m6_546(NULL /*static, unused*/, /*hidden argument*/NULL);
		Vector2_t6_48  L_12 = Vector2_op_Implicit_m6_189(NULL /*static, unused*/, L_11, /*hidden argument*/NULL);
		__this->___currentPos_6 = L_12;
		bool L_13 = Input_GetMouseButtonDown_m6_544(NULL /*static, unused*/, 0, /*hidden argument*/NULL);
		if (!L_13)
		{
			goto IL_00a3;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t6_83_il2cpp_TypeInfo_var);
		Vector3_t6_49  L_14 = Input_get_mousePosition_m6_546(NULL /*static, unused*/, /*hidden argument*/NULL);
		Vector2_t6_48  L_15 = Vector2_op_Implicit_m6_189(NULL /*static, unused*/, L_14, /*hidden argument*/NULL);
		InputToEvent_Press_m8_949(__this, L_15, /*hidden argument*/NULL);
	}

IL_00a3:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t6_83_il2cpp_TypeInfo_var);
		bool L_16 = Input_GetMouseButtonUp_m6_545(NULL /*static, unused*/, 0, /*hidden argument*/NULL);
		if (!L_16)
		{
			goto IL_00be;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t6_83_il2cpp_TypeInfo_var);
		Vector3_t6_49  L_17 = Input_get_mousePosition_m6_546(NULL /*static, unused*/, /*hidden argument*/NULL);
		Vector2_t6_48  L_18 = Vector2_op_Implicit_m6_189(NULL /*static, unused*/, L_17, /*hidden argument*/NULL);
		InputToEvent_Release_m8_950(__this, L_18, /*hidden argument*/NULL);
	}

IL_00be:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t6_83_il2cpp_TypeInfo_var);
		bool L_19 = Input_GetMouseButtonDown_m6_544(NULL /*static, unused*/, 1, /*hidden argument*/NULL);
		if (!L_19)
		{
			goto IL_010d;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t6_83_il2cpp_TypeInfo_var);
		Vector3_t6_49  L_20 = Input_get_mousePosition_m6_546(NULL /*static, unused*/, /*hidden argument*/NULL);
		Vector2_t6_48  L_21 = Vector2_op_Implicit_m6_189(NULL /*static, unused*/, L_20, /*hidden argument*/NULL);
		__this->___pressedPosition_5 = L_21;
		Vector2_t6_48  L_22 = (__this->___pressedPosition_5);
		GameObject_t6_85 * L_23 = InputToEvent_RaycastObject_m8_951(__this, L_22, /*hidden argument*/NULL);
		__this->___lastGo_2 = L_23;
		GameObject_t6_85 * L_24 = (__this->___lastGo_2);
		bool L_25 = Object_op_Inequality_m6_580(NULL /*static, unused*/, L_24, (Object_t6_5 *)NULL, /*hidden argument*/NULL);
		if (!L_25)
		{
			goto IL_010d;
		}
	}
	{
		GameObject_t6_85 * L_26 = (__this->___lastGo_2);
		NullCheck(L_26);
		GameObject_SendMessage_m6_602(L_26, _stringLiteral3528, 1, /*hidden argument*/NULL);
	}

IL_010d:
	{
		return;
	}
}
// System.Void InputToEvent::Press(UnityEngine.Vector2)
extern Il2CppCodeGenString* _stringLiteral3529;
extern "C" void InputToEvent_Press_m8_949 (InputToEvent_t8_152 * __this, Vector2_t6_48  ___screenPos, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		_stringLiteral3529 = il2cpp_codegen_string_literal_from_index(3529);
		s_Il2CppMethodIntialized = true;
	}
	{
		Vector2_t6_48  L_0 = ___screenPos;
		__this->___pressedPosition_5 = L_0;
		__this->___Dragging_7 = 1;
		Vector2_t6_48  L_1 = ___screenPos;
		GameObject_t6_85 * L_2 = InputToEvent_RaycastObject_m8_951(__this, L_1, /*hidden argument*/NULL);
		__this->___lastGo_2 = L_2;
		GameObject_t6_85 * L_3 = (__this->___lastGo_2);
		bool L_4 = Object_op_Inequality_m6_580(NULL /*static, unused*/, L_3, (Object_t6_5 *)NULL, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_003d;
		}
	}
	{
		GameObject_t6_85 * L_5 = (__this->___lastGo_2);
		NullCheck(L_5);
		GameObject_SendMessage_m6_602(L_5, _stringLiteral3529, 1, /*hidden argument*/NULL);
	}

IL_003d:
	{
		return;
	}
}
// System.Void InputToEvent::Release(UnityEngine.Vector2)
extern Il2CppCodeGenString* _stringLiteral3530;
extern Il2CppCodeGenString* _stringLiteral3531;
extern "C" void InputToEvent_Release_m8_950 (InputToEvent_t8_152 * __this, Vector2_t6_48  ___screenPos, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		_stringLiteral3530 = il2cpp_codegen_string_literal_from_index(3530);
		_stringLiteral3531 = il2cpp_codegen_string_literal_from_index(3531);
		s_Il2CppMethodIntialized = true;
	}
	GameObject_t6_85 * V_0 = {0};
	{
		GameObject_t6_85 * L_0 = (__this->___lastGo_2);
		bool L_1 = Object_op_Inequality_m6_580(NULL /*static, unused*/, L_0, (Object_t6_5 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0053;
		}
	}
	{
		Vector2_t6_48  L_2 = ___screenPos;
		GameObject_t6_85 * L_3 = InputToEvent_RaycastObject_m8_951(__this, L_2, /*hidden argument*/NULL);
		V_0 = L_3;
		GameObject_t6_85 * L_4 = V_0;
		GameObject_t6_85 * L_5 = (__this->___lastGo_2);
		bool L_6 = Object_op_Equality_m6_579(NULL /*static, unused*/, L_4, L_5, /*hidden argument*/NULL);
		if (!L_6)
		{
			goto IL_003b;
		}
	}
	{
		GameObject_t6_85 * L_7 = (__this->___lastGo_2);
		NullCheck(L_7);
		GameObject_SendMessage_m6_602(L_7, _stringLiteral3530, 1, /*hidden argument*/NULL);
	}

IL_003b:
	{
		GameObject_t6_85 * L_8 = (__this->___lastGo_2);
		NullCheck(L_8);
		GameObject_SendMessage_m6_602(L_8, _stringLiteral3531, 1, /*hidden argument*/NULL);
		__this->___lastGo_2 = (GameObject_t6_85 *)NULL;
	}

IL_0053:
	{
		Vector2_t6_48  L_9 = Vector2_get_zero_m6_182(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->___pressedPosition_5 = L_9;
		__this->___Dragging_7 = 0;
		return;
	}
}
// UnityEngine.GameObject InputToEvent::RaycastObject(UnityEngine.Vector2)
extern TypeInfo* InputToEvent_t8_152_il2cpp_TypeInfo_var;
extern "C" GameObject_t6_85 * InputToEvent_RaycastObject_m8_951 (InputToEvent_t8_152 * __this, Vector2_t6_48  ___screenPos, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		InputToEvent_t8_152_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1063);
		s_Il2CppMethodIntialized = true;
	}
	RaycastHit_t6_108  V_0 = {0};
	{
		Camera_t6_75 * L_0 = (__this->___m_Camera_8);
		Vector2_t6_48  L_1 = ___screenPos;
		Vector3_t6_49  L_2 = Vector2_op_Implicit_m6_190(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		NullCheck(L_0);
		Ray_t6_56  L_3 = Camera_ScreenPointToRay_m6_473(L_0, L_2, /*hidden argument*/NULL);
		bool L_4 = Physics_Raycast_m6_713(NULL /*static, unused*/, L_3, (&V_0), (200.0f), /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_003b;
		}
	}
	{
		Vector3_t6_49  L_5 = RaycastHit_get_point_m6_730((&V_0), /*hidden argument*/NULL);
		((InputToEvent_t8_152_StaticFields*)InputToEvent_t8_152_il2cpp_TypeInfo_var->static_fields)->___inputHitPos_3 = L_5;
		Collider_t6_100 * L_6 = RaycastHit_get_collider_m6_731((&V_0), /*hidden argument*/NULL);
		NullCheck(L_6);
		GameObject_t6_85 * L_7 = Component_get_gameObject_m6_583(L_6, /*hidden argument*/NULL);
		return L_7;
	}

IL_003b:
	{
		return (GameObject_t6_85 *)NULL;
	}
}
// System.Void ManualPhotonViewAllocator::.ctor()
extern "C" void ManualPhotonViewAllocator__ctor_m8_952 (ManualPhotonViewAllocator_t8_153 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m6_525(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void ManualPhotonViewAllocator::AllocateManualPhotonView()
extern TypeInfo* PhotonNetwork_t8_114_il2cpp_TypeInfo_var;
extern TypeInfo* ObjectU5BU5D_t1_157_il2cpp_TypeInfo_var;
extern TypeInfo* Int32_t1_3_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3532;
extern Il2CppCodeGenString* _stringLiteral3533;
extern "C" void ManualPhotonViewAllocator_AllocateManualPhotonView_m8_953 (ManualPhotonViewAllocator_t8_153 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		PhotonNetwork_t8_114_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1058);
		ObjectU5BU5D_t1_157_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(19);
		Int32_t1_3_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(8);
		_stringLiteral3532 = il2cpp_codegen_string_literal_from_index(3532);
		_stringLiteral3533 = il2cpp_codegen_string_literal_from_index(3533);
		s_Il2CppMethodIntialized = true;
	}
	PhotonView_t8_3 * V_0 = {0};
	int32_t V_1 = 0;
	{
		GameObject_t6_85 * L_0 = Component_get_gameObject_m6_583(__this, /*hidden argument*/NULL);
		PhotonView_t8_3 * L_1 = Extensions_GetPhotonView_m8_292(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		PhotonView_t8_3 * L_2 = V_0;
		bool L_3 = Object_op_Equality_m6_579(NULL /*static, unused*/, L_2, (Object_t6_5 *)NULL, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_0023;
		}
	}
	{
		Debug_LogError_m6_490(NULL /*static, unused*/, _stringLiteral3532, /*hidden argument*/NULL);
		return;
	}

IL_0023:
	{
		IL2CPP_RUNTIME_CLASS_INIT(PhotonNetwork_t8_114_il2cpp_TypeInfo_var);
		int32_t L_4 = PhotonNetwork_AllocateViewID_m8_695(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_1 = L_4;
		PhotonView_t8_3 * L_5 = V_0;
		ObjectU5BU5D_t1_157* L_6 = ((ObjectU5BU5D_t1_157*)SZArrayNew(ObjectU5BU5D_t1_157_il2cpp_TypeInfo_var, 1));
		int32_t L_7 = V_1;
		int32_t L_8 = L_7;
		Object_t * L_9 = Box(Int32_t1_3_il2cpp_TypeInfo_var, &L_8);
		NullCheck(L_6);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_6, 0);
		ArrayElementTypeCheck (L_6, L_9);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_6, 0, sizeof(Object_t *))) = (Object_t *)L_9;
		NullCheck(L_5);
		PhotonView_RPC_m8_786(L_5, _stringLiteral3533, 3, L_6, /*hidden argument*/NULL);
		return;
	}
}
// System.Void ManualPhotonViewAllocator::InstantiateRpc(System.Int32)
extern TypeInfo* InputToEvent_t8_152_il2cpp_TypeInfo_var;
extern TypeInfo* GameObject_t6_85_il2cpp_TypeInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisOnClickDestroy_t8_157_m6_1690_MethodInfo_var;
extern "C" void ManualPhotonViewAllocator_InstantiateRpc_m8_954 (ManualPhotonViewAllocator_t8_153 * __this, int32_t ___viewID, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		InputToEvent_t8_152_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1063);
		GameObject_t6_85_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1082);
		GameObject_GetComponent_TisOnClickDestroy_t8_157_m6_1690_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483959);
		s_Il2CppMethodIntialized = true;
	}
	GameObject_t6_85 * V_0 = {0};
	OnClickDestroy_t8_157 * V_1 = {0};
	{
		GameObject_t6_85 * L_0 = (__this->___Prefab_2);
		Vector3_t6_49  L_1 = ((InputToEvent_t8_152_StaticFields*)InputToEvent_t8_152_il2cpp_TypeInfo_var->static_fields)->___inputHitPos_3;
		Vector3_t6_49  L_2 = {0};
		Vector3__ctor_m6_191(&L_2, (0.0f), (5.0f), (0.0f), /*hidden argument*/NULL);
		Vector3_t6_49  L_3 = Vector3_op_Addition_m6_221(NULL /*static, unused*/, L_1, L_2, /*hidden argument*/NULL);
		Quaternion_t6_51  L_4 = Quaternion_get_identity_m6_244(NULL /*static, unused*/, /*hidden argument*/NULL);
		Object_t6_5 * L_5 = Object_Instantiate_m6_575(NULL /*static, unused*/, L_0, L_3, L_4, /*hidden argument*/NULL);
		V_0 = ((GameObject_t6_85 *)IsInstSealed(L_5, GameObject_t6_85_il2cpp_TypeInfo_var));
		GameObject_t6_85 * L_6 = V_0;
		PhotonView_t8_3 * L_7 = Extensions_GetPhotonView_m8_292(NULL /*static, unused*/, L_6, /*hidden argument*/NULL);
		int32_t L_8 = ___viewID;
		NullCheck(L_7);
		PhotonView_set_viewID_m8_768(L_7, L_8, /*hidden argument*/NULL);
		GameObject_t6_85 * L_9 = V_0;
		NullCheck(L_9);
		OnClickDestroy_t8_157 * L_10 = GameObject_GetComponent_TisOnClickDestroy_t8_157_m6_1690(L_9, /*hidden argument*/GameObject_GetComponent_TisOnClickDestroy_t8_157_m6_1690_MethodInfo_var);
		V_1 = L_10;
		OnClickDestroy_t8_157 * L_11 = V_1;
		NullCheck(L_11);
		L_11->___DestroyByRpc_2 = 1;
		return;
	}
}
// System.Void MoveByKeys::.ctor()
extern "C" void MoveByKeys__ctor_m8_955 (MoveByKeys_t8_154 * __this, const MethodInfo* method)
{
	{
		__this->___Speed_2 = (10.0f);
		__this->___JumpForce_3 = (200.0f);
		__this->___JumpTimeout_4 = (0.5f);
		MonoBehaviour__ctor_m8_494(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void MoveByKeys::Start()
extern const MethodInfo* Component_GetComponent_TisSpriteRenderer_t6_69_m6_1691_MethodInfo_var;
extern const MethodInfo* Component_GetComponent_TisRigidbody2D_t6_113_m6_1657_MethodInfo_var;
extern const MethodInfo* Component_GetComponent_TisRigidbody_t6_102_m6_1659_MethodInfo_var;
extern "C" void MoveByKeys_Start_m8_956 (MoveByKeys_t8_154 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Component_GetComponent_TisSpriteRenderer_t6_69_m6_1691_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483960);
		Component_GetComponent_TisRigidbody2D_t6_113_m6_1657_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483835);
		Component_GetComponent_TisRigidbody_t6_102_m6_1659_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483837);
		s_Il2CppMethodIntialized = true;
	}
	{
		SpriteRenderer_t6_69 * L_0 = Component_GetComponent_TisSpriteRenderer_t6_69_m6_1691(__this, /*hidden argument*/Component_GetComponent_TisSpriteRenderer_t6_69_m6_1691_MethodInfo_var);
		bool L_1 = Object_op_Inequality_m6_580(NULL /*static, unused*/, L_0, (Object_t6_5 *)NULL, /*hidden argument*/NULL);
		__this->___isSprite_5 = L_1;
		Rigidbody2D_t6_113 * L_2 = Component_GetComponent_TisRigidbody2D_t6_113_m6_1657(__this, /*hidden argument*/Component_GetComponent_TisRigidbody2D_t6_113_m6_1657_MethodInfo_var);
		__this->___body2d_8 = L_2;
		Rigidbody_t6_102 * L_3 = Component_GetComponent_TisRigidbody_t6_102_m6_1659(__this, /*hidden argument*/Component_GetComponent_TisRigidbody_t6_102_m6_1659_MethodInfo_var);
		__this->___body_7 = L_3;
		return;
	}
}
// System.Void MoveByKeys::FixedUpdate()
extern TypeInfo* Input_t6_83_il2cpp_TypeInfo_var;
extern "C" void MoveByKeys_FixedUpdate_m8_957 (MoveByKeys_t8_154 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Input_t6_83_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(956);
		s_Il2CppMethodIntialized = true;
	}
	Vector2_t6_48  V_0 = {0};
	{
		PhotonView_t8_3 * L_0 = MonoBehaviour_get_photonView_m8_495(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		bool L_1 = PhotonView_get_isMine_m8_774(L_0, /*hidden argument*/NULL);
		if (L_1)
		{
			goto IL_0011;
		}
	}
	{
		return;
	}

IL_0011:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t6_83_il2cpp_TypeInfo_var);
		bool L_2 = Input_GetKey_m6_541(NULL /*static, unused*/, ((int32_t)97), /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_0049;
		}
	}
	{
		Transform_t6_61 * L_3 = Component_get_transform_m6_582(__this, /*hidden argument*/NULL);
		Transform_t6_61 * L_4 = L_3;
		NullCheck(L_4);
		Vector3_t6_49  L_5 = Transform_get_position_m6_611(L_4, /*hidden argument*/NULL);
		Vector3_t6_49  L_6 = Vector3_get_left_m6_219(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_7 = (__this->___Speed_2);
		float L_8 = Time_get_deltaTime_m6_654(NULL /*static, unused*/, /*hidden argument*/NULL);
		Vector3_t6_49  L_9 = Vector3_op_Multiply_m6_224(NULL /*static, unused*/, L_6, ((float)((float)L_7*(float)L_8)), /*hidden argument*/NULL);
		Vector3_t6_49  L_10 = Vector3_op_Addition_m6_221(NULL /*static, unused*/, L_5, L_9, /*hidden argument*/NULL);
		NullCheck(L_4);
		Transform_set_position_m6_612(L_4, L_10, /*hidden argument*/NULL);
	}

IL_0049:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t6_83_il2cpp_TypeInfo_var);
		bool L_11 = Input_GetKey_m6_541(NULL /*static, unused*/, ((int32_t)100), /*hidden argument*/NULL);
		if (!L_11)
		{
			goto IL_0081;
		}
	}
	{
		Transform_t6_61 * L_12 = Component_get_transform_m6_582(__this, /*hidden argument*/NULL);
		Transform_t6_61 * L_13 = L_12;
		NullCheck(L_13);
		Vector3_t6_49  L_14 = Transform_get_position_m6_611(L_13, /*hidden argument*/NULL);
		Vector3_t6_49  L_15 = Vector3_get_right_m6_220(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_16 = (__this->___Speed_2);
		float L_17 = Time_get_deltaTime_m6_654(NULL /*static, unused*/, /*hidden argument*/NULL);
		Vector3_t6_49  L_18 = Vector3_op_Multiply_m6_224(NULL /*static, unused*/, L_15, ((float)((float)L_16*(float)L_17)), /*hidden argument*/NULL);
		Vector3_t6_49  L_19 = Vector3_op_Addition_m6_221(NULL /*static, unused*/, L_14, L_18, /*hidden argument*/NULL);
		NullCheck(L_13);
		Transform_set_position_m6_612(L_13, L_19, /*hidden argument*/NULL);
	}

IL_0081:
	{
		float L_20 = (__this->___jumpingTime_6);
		if ((!(((float)L_20) <= ((float)(0.0f)))))
		{
			goto IL_0125;
		}
	}
	{
		Rigidbody_t6_102 * L_21 = (__this->___body_7);
		bool L_22 = Object_op_Inequality_m6_580(NULL /*static, unused*/, L_21, (Object_t6_5 *)NULL, /*hidden argument*/NULL);
		if (L_22)
		{
			goto IL_00b3;
		}
	}
	{
		Rigidbody2D_t6_113 * L_23 = (__this->___body2d_8);
		bool L_24 = Object_op_Inequality_m6_580(NULL /*static, unused*/, L_23, (Object_t6_5 *)NULL, /*hidden argument*/NULL);
		if (!L_24)
		{
			goto IL_0120;
		}
	}

IL_00b3:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t6_83_il2cpp_TypeInfo_var);
		bool L_25 = Input_GetKey_m6_541(NULL /*static, unused*/, ((int32_t)32), /*hidden argument*/NULL);
		if (!L_25)
		{
			goto IL_0120;
		}
	}
	{
		float L_26 = (__this->___JumpTimeout_4);
		__this->___jumpingTime_6 = L_26;
		Vector2_t6_48  L_27 = Vector2_get_up_m6_183(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_28 = (__this->___JumpForce_3);
		Vector2_t6_48  L_29 = Vector2_op_Multiply_m6_187(NULL /*static, unused*/, L_27, L_28, /*hidden argument*/NULL);
		V_0 = L_29;
		Rigidbody2D_t6_113 * L_30 = (__this->___body2d_8);
		bool L_31 = Object_op_Inequality_m6_580(NULL /*static, unused*/, L_30, (Object_t6_5 *)NULL, /*hidden argument*/NULL);
		if (!L_31)
		{
			goto IL_00fe;
		}
	}
	{
		Rigidbody2D_t6_113 * L_32 = (__this->___body2d_8);
		Vector2_t6_48  L_33 = V_0;
		NullCheck(L_32);
		Rigidbody2D_AddForce_m6_747(L_32, L_33, /*hidden argument*/NULL);
		goto IL_0120;
	}

IL_00fe:
	{
		Rigidbody_t6_102 * L_34 = (__this->___body_7);
		bool L_35 = Object_op_Inequality_m6_580(NULL /*static, unused*/, L_34, (Object_t6_5 *)NULL, /*hidden argument*/NULL);
		if (!L_35)
		{
			goto IL_0120;
		}
	}
	{
		Rigidbody_t6_102 * L_36 = (__this->___body_7);
		Vector2_t6_48  L_37 = V_0;
		Vector3_t6_49  L_38 = Vector2_op_Implicit_m6_190(NULL /*static, unused*/, L_37, /*hidden argument*/NULL);
		NullCheck(L_36);
		Rigidbody_AddForce_m6_726(L_36, L_38, /*hidden argument*/NULL);
	}

IL_0120:
	{
		goto IL_0137;
	}

IL_0125:
	{
		float L_39 = (__this->___jumpingTime_6);
		float L_40 = Time_get_deltaTime_m6_654(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->___jumpingTime_6 = ((float)((float)L_39-(float)L_40));
	}

IL_0137:
	{
		bool L_41 = (__this->___isSprite_5);
		if (L_41)
		{
			goto IL_01b2;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t6_83_il2cpp_TypeInfo_var);
		bool L_42 = Input_GetKey_m6_541(NULL /*static, unused*/, ((int32_t)119), /*hidden argument*/NULL);
		if (!L_42)
		{
			goto IL_017a;
		}
	}
	{
		Transform_t6_61 * L_43 = Component_get_transform_m6_582(__this, /*hidden argument*/NULL);
		Transform_t6_61 * L_44 = L_43;
		NullCheck(L_44);
		Vector3_t6_49  L_45 = Transform_get_position_m6_611(L_44, /*hidden argument*/NULL);
		Vector3_t6_49  L_46 = Vector3_get_forward_m6_215(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_47 = (__this->___Speed_2);
		float L_48 = Time_get_deltaTime_m6_654(NULL /*static, unused*/, /*hidden argument*/NULL);
		Vector3_t6_49  L_49 = Vector3_op_Multiply_m6_224(NULL /*static, unused*/, L_46, ((float)((float)L_47*(float)L_48)), /*hidden argument*/NULL);
		Vector3_t6_49  L_50 = Vector3_op_Addition_m6_221(NULL /*static, unused*/, L_45, L_49, /*hidden argument*/NULL);
		NullCheck(L_44);
		Transform_set_position_m6_612(L_44, L_50, /*hidden argument*/NULL);
	}

IL_017a:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t6_83_il2cpp_TypeInfo_var);
		bool L_51 = Input_GetKey_m6_541(NULL /*static, unused*/, ((int32_t)115), /*hidden argument*/NULL);
		if (!L_51)
		{
			goto IL_01b2;
		}
	}
	{
		Transform_t6_61 * L_52 = Component_get_transform_m6_582(__this, /*hidden argument*/NULL);
		Transform_t6_61 * L_53 = L_52;
		NullCheck(L_53);
		Vector3_t6_49  L_54 = Transform_get_position_m6_611(L_53, /*hidden argument*/NULL);
		Vector3_t6_49  L_55 = Vector3_get_forward_m6_215(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_56 = (__this->___Speed_2);
		float L_57 = Time_get_deltaTime_m6_654(NULL /*static, unused*/, /*hidden argument*/NULL);
		Vector3_t6_49  L_58 = Vector3_op_Multiply_m6_224(NULL /*static, unused*/, L_55, ((float)((float)L_56*(float)L_57)), /*hidden argument*/NULL);
		Vector3_t6_49  L_59 = Vector3_op_Subtraction_m6_222(NULL /*static, unused*/, L_54, L_58, /*hidden argument*/NULL);
		NullCheck(L_53);
		Transform_set_position_m6_612(L_53, L_59, /*hidden argument*/NULL);
	}

IL_01b2:
	{
		return;
	}
}
// System.Void OnAwakeUsePhotonView::.ctor()
extern "C" void OnAwakeUsePhotonView__ctor_m8_958 (OnAwakeUsePhotonView_t8_155 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m8_494(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void OnAwakeUsePhotonView::Awake()
extern TypeInfo* ObjectU5BU5D_t1_157_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3534;
extern "C" void OnAwakeUsePhotonView_Awake_m8_959 (OnAwakeUsePhotonView_t8_155 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ObjectU5BU5D_t1_157_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(19);
		_stringLiteral3534 = il2cpp_codegen_string_literal_from_index(3534);
		s_Il2CppMethodIntialized = true;
	}
	{
		PhotonView_t8_3 * L_0 = MonoBehaviour_get_photonView_m8_495(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		bool L_1 = PhotonView_get_isMine_m8_774(L_0, /*hidden argument*/NULL);
		if (L_1)
		{
			goto IL_0011;
		}
	}
	{
		return;
	}

IL_0011:
	{
		PhotonView_t8_3 * L_2 = MonoBehaviour_get_photonView_m8_495(__this, /*hidden argument*/NULL);
		NullCheck(L_2);
		PhotonView_RPC_m8_786(L_2, _stringLiteral3534, 0, ((ObjectU5BU5D_t1_157*)SZArrayNew(ObjectU5BU5D_t1_157_il2cpp_TypeInfo_var, 0)), /*hidden argument*/NULL);
		return;
	}
}
// System.Void OnAwakeUsePhotonView::Start()
extern TypeInfo* ObjectU5BU5D_t1_157_il2cpp_TypeInfo_var;
extern TypeInfo* Byte_t1_11_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3534;
extern "C" void OnAwakeUsePhotonView_Start_m8_960 (OnAwakeUsePhotonView_t8_155 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ObjectU5BU5D_t1_157_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(19);
		Byte_t1_11_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(22);
		_stringLiteral3534 = il2cpp_codegen_string_literal_from_index(3534);
		s_Il2CppMethodIntialized = true;
	}
	{
		PhotonView_t8_3 * L_0 = MonoBehaviour_get_photonView_m8_495(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		bool L_1 = PhotonView_get_isMine_m8_774(L_0, /*hidden argument*/NULL);
		if (L_1)
		{
			goto IL_0011;
		}
	}
	{
		return;
	}

IL_0011:
	{
		PhotonView_t8_3 * L_2 = MonoBehaviour_get_photonView_m8_495(__this, /*hidden argument*/NULL);
		ObjectU5BU5D_t1_157* L_3 = ((ObjectU5BU5D_t1_157*)SZArrayNew(ObjectU5BU5D_t1_157_il2cpp_TypeInfo_var, 1));
		uint8_t L_4 = 1;
		Object_t * L_5 = Box(Byte_t1_11_il2cpp_TypeInfo_var, &L_4);
		NullCheck(L_3);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_3, 0);
		ArrayElementTypeCheck (L_3, L_5);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_3, 0, sizeof(Object_t *))) = (Object_t *)L_5;
		NullCheck(L_2);
		PhotonView_RPC_m8_786(L_2, _stringLiteral3534, 0, L_3, /*hidden argument*/NULL);
		return;
	}
}
// System.Void OnAwakeUsePhotonView::OnAwakeRPC()
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3535;
extern "C" void OnAwakeUsePhotonView_OnAwakeRPC_m8_961 (OnAwakeUsePhotonView_t8_155 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(11);
		_stringLiteral3535 = il2cpp_codegen_string_literal_from_index(3535);
		s_Il2CppMethodIntialized = true;
	}
	{
		PhotonView_t8_3 * L_0 = MonoBehaviour_get_photonView_m8_495(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_1 = String_Concat_m1_416(NULL /*static, unused*/, _stringLiteral3535, L_0, /*hidden argument*/NULL);
		Debug_Log_m6_489(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void OnAwakeUsePhotonView::OnAwakeRPC(System.Byte)
extern TypeInfo* ObjectU5BU5D_t1_157_il2cpp_TypeInfo_var;
extern TypeInfo* Byte_t1_11_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3536;
extern Il2CppCodeGenString* _stringLiteral3537;
extern "C" void OnAwakeUsePhotonView_OnAwakeRPC_m8_962 (OnAwakeUsePhotonView_t8_155 * __this, uint8_t ___myParameter, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ObjectU5BU5D_t1_157_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(19);
		Byte_t1_11_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(22);
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(11);
		_stringLiteral3536 = il2cpp_codegen_string_literal_from_index(3536);
		_stringLiteral3537 = il2cpp_codegen_string_literal_from_index(3537);
		s_Il2CppMethodIntialized = true;
	}
	{
		ObjectU5BU5D_t1_157* L_0 = ((ObjectU5BU5D_t1_157*)SZArrayNew(ObjectU5BU5D_t1_157_il2cpp_TypeInfo_var, 4));
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, 0);
		ArrayElementTypeCheck (L_0, _stringLiteral3536);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_0, 0, sizeof(Object_t *))) = (Object_t *)_stringLiteral3536;
		ObjectU5BU5D_t1_157* L_1 = L_0;
		uint8_t L_2 = ___myParameter;
		uint8_t L_3 = L_2;
		Object_t * L_4 = Box(Byte_t1_11_il2cpp_TypeInfo_var, &L_3);
		NullCheck(L_1);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_1, 1);
		ArrayElementTypeCheck (L_1, L_4);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_1, 1, sizeof(Object_t *))) = (Object_t *)L_4;
		ObjectU5BU5D_t1_157* L_5 = L_1;
		NullCheck(L_5);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_5, 2);
		ArrayElementTypeCheck (L_5, _stringLiteral3537);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_5, 2, sizeof(Object_t *))) = (Object_t *)_stringLiteral3537;
		ObjectU5BU5D_t1_157* L_6 = L_5;
		PhotonView_t8_3 * L_7 = MonoBehaviour_get_photonView_m8_495(__this, /*hidden argument*/NULL);
		NullCheck(L_6);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_6, 3);
		ArrayElementTypeCheck (L_6, L_7);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_6, 3, sizeof(Object_t *))) = (Object_t *)L_7;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_8 = String_Concat_m1_421(NULL /*static, unused*/, L_6, /*hidden argument*/NULL);
		Debug_Log_m6_489(NULL /*static, unused*/, L_8, /*hidden argument*/NULL);
		return;
	}
}
// System.Void OnClickDestroy/<DestroyRpc>c__Iterator2::.ctor()
extern "C" void U3CDestroyRpcU3Ec__Iterator2__ctor_m8_963 (U3CDestroyRpcU3Ec__Iterator2_t8_156 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1_0(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Object OnClickDestroy/<DestroyRpc>c__Iterator2::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C" Object_t * U3CDestroyRpcU3Ec__Iterator2_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m8_964 (U3CDestroyRpcU3Ec__Iterator2_t8_156 * __this, const MethodInfo* method)
{
	{
		Object_t * L_0 = (__this->___U24current_1);
		return L_0;
	}
}
// System.Object OnClickDestroy/<DestroyRpc>c__Iterator2::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * U3CDestroyRpcU3Ec__Iterator2_System_Collections_IEnumerator_get_Current_m8_965 (U3CDestroyRpcU3Ec__Iterator2_t8_156 * __this, const MethodInfo* method)
{
	{
		Object_t * L_0 = (__this->___U24current_1);
		return L_0;
	}
}
// System.Boolean OnClickDestroy/<DestroyRpc>c__Iterator2::MoveNext()
extern TypeInfo* Int32_t1_3_il2cpp_TypeInfo_var;
extern TypeInfo* PhotonNetwork_t8_114_il2cpp_TypeInfo_var;
extern "C" bool U3CDestroyRpcU3Ec__Iterator2_MoveNext_m8_966 (U3CDestroyRpcU3Ec__Iterator2_t8_156 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Int32_t1_3_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(8);
		PhotonNetwork_t8_114_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1058);
		s_Il2CppMethodIntialized = true;
	}
	uint32_t V_0 = 0;
	bool V_1 = false;
	{
		int32_t L_0 = (__this->___U24PC_0);
		V_0 = L_0;
		__this->___U24PC_0 = (-1);
		uint32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_0021;
		}
		if (L_1 == 1)
		{
			goto IL_0049;
		}
	}
	{
		goto IL_0065;
	}

IL_0021:
	{
		OnClickDestroy_t8_157 * L_2 = (__this->___U3CU3Ef__this_2);
		NullCheck(L_2);
		GameObject_t6_85 * L_3 = Component_get_gameObject_m6_583(L_2, /*hidden argument*/NULL);
		Object_Destroy_m6_558(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		int32_t L_4 = 0;
		Object_t * L_5 = Box(Int32_t1_3_il2cpp_TypeInfo_var, &L_4);
		__this->___U24current_1 = L_5;
		__this->___U24PC_0 = 1;
		goto IL_0067;
	}

IL_0049:
	{
		OnClickDestroy_t8_157 * L_6 = (__this->___U3CU3Ef__this_2);
		NullCheck(L_6);
		PhotonView_t8_3 * L_7 = MonoBehaviour_get_photonView_m8_495(L_6, /*hidden argument*/NULL);
		NullCheck(L_7);
		int32_t L_8 = PhotonView_get_viewID_m8_767(L_7, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(PhotonNetwork_t8_114_il2cpp_TypeInfo_var);
		PhotonNetwork_UnAllocateViewID_m8_699(NULL /*static, unused*/, L_8, /*hidden argument*/NULL);
		__this->___U24PC_0 = (-1);
	}

IL_0065:
	{
		return 0;
	}

IL_0067:
	{
		return 1;
	}
	// Dead block : IL_0069: ldloc.1
}
// System.Void OnClickDestroy/<DestroyRpc>c__Iterator2::Dispose()
extern "C" void U3CDestroyRpcU3Ec__Iterator2_Dispose_m8_967 (U3CDestroyRpcU3Ec__Iterator2_t8_156 * __this, const MethodInfo* method)
{
	{
		__this->___U24PC_0 = (-1);
		return;
	}
}
// System.Void OnClickDestroy/<DestroyRpc>c__Iterator2::Reset()
extern TypeInfo* NotSupportedException_t1_749_il2cpp_TypeInfo_var;
extern "C" void U3CDestroyRpcU3Ec__Iterator2_Reset_m8_968 (U3CDestroyRpcU3Ec__Iterator2_t8_156 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NotSupportedException_t1_749_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(6);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t1_749 * L_0 = (NotSupportedException_t1_749 *)il2cpp_codegen_object_new (NotSupportedException_t1_749_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m1_5205(L_0, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_0);
	}
}
// System.Void OnClickDestroy::.ctor()
extern "C" void OnClickDestroy__ctor_m8_969 (OnClickDestroy_t8_157 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m8_494(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void OnClickDestroy::OnClick()
extern TypeInfo* PhotonNetwork_t8_114_il2cpp_TypeInfo_var;
extern TypeInfo* ObjectU5BU5D_t1_157_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3538;
extern "C" void OnClickDestroy_OnClick_m8_970 (OnClickDestroy_t8_157 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		PhotonNetwork_t8_114_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1058);
		ObjectU5BU5D_t1_157_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(19);
		_stringLiteral3538 = il2cpp_codegen_string_literal_from_index(3538);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool L_0 = (__this->___DestroyByRpc_2);
		if (L_0)
		{
			goto IL_001b;
		}
	}
	{
		GameObject_t6_85 * L_1 = Component_get_gameObject_m6_583(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(PhotonNetwork_t8_114_il2cpp_TypeInfo_var);
		PhotonNetwork_Destroy_m8_709(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		goto IL_0032;
	}

IL_001b:
	{
		PhotonView_t8_3 * L_2 = MonoBehaviour_get_photonView_m8_495(__this, /*hidden argument*/NULL);
		NullCheck(L_2);
		PhotonView_RPC_m8_786(L_2, _stringLiteral3538, 3, ((ObjectU5BU5D_t1_157*)SZArrayNew(ObjectU5BU5D_t1_157_il2cpp_TypeInfo_var, 0)), /*hidden argument*/NULL);
	}

IL_0032:
	{
		return;
	}
}
// System.Collections.IEnumerator OnClickDestroy::DestroyRpc()
extern TypeInfo* U3CDestroyRpcU3Ec__Iterator2_t8_156_il2cpp_TypeInfo_var;
extern "C" Object_t * OnClickDestroy_DestroyRpc_m8_971 (OnClickDestroy_t8_157 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		U3CDestroyRpcU3Ec__Iterator2_t8_156_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1191);
		s_Il2CppMethodIntialized = true;
	}
	U3CDestroyRpcU3Ec__Iterator2_t8_156 * V_0 = {0};
	{
		U3CDestroyRpcU3Ec__Iterator2_t8_156 * L_0 = (U3CDestroyRpcU3Ec__Iterator2_t8_156 *)il2cpp_codegen_object_new (U3CDestroyRpcU3Ec__Iterator2_t8_156_il2cpp_TypeInfo_var);
		U3CDestroyRpcU3Ec__Iterator2__ctor_m8_963(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CDestroyRpcU3Ec__Iterator2_t8_156 * L_1 = V_0;
		NullCheck(L_1);
		L_1->___U3CU3Ef__this_2 = __this;
		U3CDestroyRpcU3Ec__Iterator2_t8_156 * L_2 = V_0;
		return L_2;
	}
}
// System.Void OnClickInstantiate::.ctor()
extern TypeInfo* StringU5BU5D_t1_202_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3539;
extern Il2CppCodeGenString* _stringLiteral3540;
extern "C" void OnClickInstantiate__ctor_m8_972 (OnClickInstantiate_t8_158 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		StringU5BU5D_t1_202_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(32);
		_stringLiteral3539 = il2cpp_codegen_string_literal_from_index(3539);
		_stringLiteral3540 = il2cpp_codegen_string_literal_from_index(3540);
		s_Il2CppMethodIntialized = true;
	}
	{
		StringU5BU5D_t1_202* L_0 = ((StringU5BU5D_t1_202*)SZArrayNew(StringU5BU5D_t1_202_il2cpp_TypeInfo_var, 2));
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, 0);
		ArrayElementTypeCheck (L_0, _stringLiteral3539);
		*((String_t**)(String_t**)SZArrayLdElema(L_0, 0, sizeof(String_t*))) = (String_t*)_stringLiteral3539;
		StringU5BU5D_t1_202* L_1 = L_0;
		NullCheck(L_1);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_1, 1);
		ArrayElementTypeCheck (L_1, _stringLiteral3540);
		*((String_t**)(String_t**)SZArrayLdElema(L_1, 1, sizeof(String_t*))) = (String_t*)_stringLiteral3540;
		__this->___InstantiateTypeNames_4 = L_1;
		MonoBehaviour__ctor_m6_525(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void OnClickInstantiate::OnClick()
extern TypeInfo* PhotonNetwork_t8_114_il2cpp_TypeInfo_var;
extern TypeInfo* InputToEvent_t8_152_il2cpp_TypeInfo_var;
extern "C" void OnClickInstantiate_OnClick_m8_973 (OnClickInstantiate_t8_158 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		PhotonNetwork_t8_114_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1058);
		InputToEvent_t8_152_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1063);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	{
		IL2CPP_RUNTIME_CLASS_INIT(PhotonNetwork_t8_114_il2cpp_TypeInfo_var);
		int32_t L_0 = PhotonNetwork_get_connectionStateDetailed_m8_607(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((((int32_t)L_0) == ((int32_t)((int32_t)9))))
		{
			goto IL_000d;
		}
	}
	{
		return;
	}

IL_000d:
	{
		int32_t L_1 = (__this->___InstantiateType_3);
		V_0 = L_1;
		int32_t L_2 = V_0;
		if (!L_2)
		{
			goto IL_0026;
		}
	}
	{
		int32_t L_3 = V_0;
		if ((((int32_t)L_3) == ((int32_t)1)))
		{
			goto IL_0060;
		}
	}
	{
		goto IL_009b;
	}

IL_0026:
	{
		GameObject_t6_85 * L_4 = (__this->___Prefab_2);
		NullCheck(L_4);
		String_t* L_5 = Object_get_name_m6_562(L_4, /*hidden argument*/NULL);
		Vector3_t6_49  L_6 = ((InputToEvent_t8_152_StaticFields*)InputToEvent_t8_152_il2cpp_TypeInfo_var->static_fields)->___inputHitPos_3;
		Vector3_t6_49  L_7 = {0};
		Vector3__ctor_m6_191(&L_7, (0.0f), (5.0f), (0.0f), /*hidden argument*/NULL);
		Vector3_t6_49  L_8 = Vector3_op_Addition_m6_221(NULL /*static, unused*/, L_6, L_7, /*hidden argument*/NULL);
		Quaternion_t6_51  L_9 = Quaternion_get_identity_m6_244(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(PhotonNetwork_t8_114_il2cpp_TypeInfo_var);
		PhotonNetwork_Instantiate_m8_700(NULL /*static, unused*/, L_5, L_8, L_9, 0, /*hidden argument*/NULL);
		goto IL_009b;
	}

IL_0060:
	{
		GameObject_t6_85 * L_10 = (__this->___Prefab_2);
		NullCheck(L_10);
		String_t* L_11 = Object_get_name_m6_562(L_10, /*hidden argument*/NULL);
		Vector3_t6_49  L_12 = ((InputToEvent_t8_152_StaticFields*)InputToEvent_t8_152_il2cpp_TypeInfo_var->static_fields)->___inputHitPos_3;
		Vector3_t6_49  L_13 = {0};
		Vector3__ctor_m6_191(&L_13, (0.0f), (5.0f), (0.0f), /*hidden argument*/NULL);
		Vector3_t6_49  L_14 = Vector3_op_Addition_m6_221(NULL /*static, unused*/, L_12, L_13, /*hidden argument*/NULL);
		Quaternion_t6_51  L_15 = Quaternion_get_identity_m6_244(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(PhotonNetwork_t8_114_il2cpp_TypeInfo_var);
		PhotonNetwork_InstantiateSceneObject_m8_702(NULL /*static, unused*/, L_11, L_14, L_15, 0, (ObjectU5BU5D_t1_157*)(ObjectU5BU5D_t1_157*)NULL, /*hidden argument*/NULL);
		goto IL_009b;
	}

IL_009b:
	{
		return;
	}
}
// System.Void OnClickInstantiate::OnGUI()
extern TypeInfo* GUILayoutOptionU5BU5D_t6_165_il2cpp_TypeInfo_var;
extern "C" void OnClickInstantiate_OnGUI_m8_974 (OnClickInstantiate_t8_158 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GUILayoutOptionU5BU5D_t6_165_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(991);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool L_0 = (__this->___showGui_5);
		if (!L_0)
		{
			goto IL_0052;
		}
	}
	{
		int32_t L_1 = Screen_get_width_m6_121(NULL /*static, unused*/, /*hidden argument*/NULL);
		Rect_t6_52  L_2 = {0};
		Rect__ctor_m6_270(&L_2, (((float)((float)((int32_t)((int32_t)L_1-(int32_t)((int32_t)180)))))), (0.0f), (180.0f), (50.0f), /*hidden argument*/NULL);
		GUILayout_BeginArea_m6_1133(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		int32_t L_3 = (__this->___InstantiateType_3);
		StringU5BU5D_t1_202* L_4 = (__this->___InstantiateTypeNames_4);
		int32_t L_5 = GUILayout_Toolbar_m6_1121(NULL /*static, unused*/, L_3, L_4, ((GUILayoutOptionU5BU5D_t6_165*)SZArrayNew(GUILayoutOptionU5BU5D_t6_165_il2cpp_TypeInfo_var, 0)), /*hidden argument*/NULL);
		__this->___InstantiateType_3 = L_5;
		GUILayout_EndArea_m6_1136(NULL /*static, unused*/, /*hidden argument*/NULL);
	}

IL_0052:
	{
		return;
	}
}
// System.Void OnClickLoadSomething::.ctor()
extern "C" void OnClickLoadSomething__ctor_m8_975 (OnClickLoadSomething_t8_160 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m6_525(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void OnClickLoadSomething::OnClick()
extern "C" void OnClickLoadSomething_OnClick_m8_976 (OnClickLoadSomething_t8_160 * __this, const MethodInfo* method)
{
	uint8_t V_0 = {0};
	{
		uint8_t L_0 = (__this->___ResourceTypeToLoad_2);
		V_0 = L_0;
		uint8_t L_1 = V_0;
		if ((((int32_t)L_1) == ((int32_t)0)))
		{
			goto IL_001a;
		}
	}
	{
		uint8_t L_2 = V_0;
		if ((((int32_t)L_2) == ((int32_t)1)))
		{
			goto IL_002a;
		}
	}
	{
		goto IL_003a;
	}

IL_001a:
	{
		String_t* L_3 = (__this->___ResourceToLoad_3);
		Application_LoadLevel_m6_449(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		goto IL_003a;
	}

IL_002a:
	{
		String_t* L_4 = (__this->___ResourceToLoad_3);
		Application_OpenURL_m6_454(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
		goto IL_003a;
	}

IL_003a:
	{
		return;
	}
}
// System.Void OnJoinedInstantiate::.ctor()
extern "C" void OnJoinedInstantiate__ctor_m8_977 (OnJoinedInstantiate_t8_161 * __this, const MethodInfo* method)
{
	{
		__this->___PositionOffset_3 = (2.0f);
		MonoBehaviour__ctor_m6_525(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void OnJoinedInstantiate::OnJoinedRoom()
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* PhotonNetwork_t8_114_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3541;
extern "C" void OnJoinedInstantiate_OnJoinedRoom_m8_978 (OnJoinedInstantiate_t8_161 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(11);
		PhotonNetwork_t8_114_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1058);
		_stringLiteral3541 = il2cpp_codegen_string_literal_from_index(3541);
		s_Il2CppMethodIntialized = true;
	}
	GameObject_t6_85 * V_0 = {0};
	GameObjectU5BU5D_t6_258* V_1 = {0};
	int32_t V_2 = 0;
	Vector3_t6_49  V_3 = {0};
	Vector3_t6_49  V_4 = {0};
	Vector3_t6_49  V_5 = {0};
	{
		GameObjectU5BU5D_t6_258* L_0 = (__this->___PrefabsToInstantiate_4);
		if (!L_0)
		{
			goto IL_00a7;
		}
	}
	{
		GameObjectU5BU5D_t6_258* L_1 = (__this->___PrefabsToInstantiate_4);
		V_1 = L_1;
		V_2 = 0;
		goto IL_009e;
	}

IL_0019:
	{
		GameObjectU5BU5D_t6_258* L_2 = V_1;
		int32_t L_3 = V_2;
		NullCheck(L_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_2, L_3);
		int32_t L_4 = L_3;
		V_0 = (*(GameObject_t6_85 **)(GameObject_t6_85 **)SZArrayLdElema(L_2, L_4, sizeof(GameObject_t6_85 *)));
		GameObject_t6_85 * L_5 = V_0;
		NullCheck(L_5);
		String_t* L_6 = Object_get_name_m6_562(L_5, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_7 = String_Concat_m1_418(NULL /*static, unused*/, _stringLiteral3541, L_6, /*hidden argument*/NULL);
		Debug_Log_m6_489(NULL /*static, unused*/, L_7, /*hidden argument*/NULL);
		Vector3_t6_49  L_8 = Vector3_get_up_m6_217(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_3 = L_8;
		Transform_t6_61 * L_9 = (__this->___SpawnPosition_2);
		bool L_10 = Object_op_Inequality_m6_580(NULL /*static, unused*/, L_9, (Object_t6_5 *)NULL, /*hidden argument*/NULL);
		if (!L_10)
		{
			goto IL_0055;
		}
	}
	{
		Transform_t6_61 * L_11 = (__this->___SpawnPosition_2);
		NullCheck(L_11);
		Vector3_t6_49  L_12 = Transform_get_position_m6_611(L_11, /*hidden argument*/NULL);
		V_3 = L_12;
	}

IL_0055:
	{
		Vector3_t6_49  L_13 = Random_get_insideUnitSphere_m6_660(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_4 = L_13;
		(&V_4)->___y_2 = (0.0f);
		Vector3_t6_49  L_14 = Vector3_get_normalized_m6_201((&V_4), /*hidden argument*/NULL);
		V_4 = L_14;
		Vector3_t6_49  L_15 = V_3;
		float L_16 = (__this->___PositionOffset_3);
		Vector3_t6_49  L_17 = V_4;
		Vector3_t6_49  L_18 = Vector3_op_Multiply_m6_225(NULL /*static, unused*/, L_16, L_17, /*hidden argument*/NULL);
		Vector3_t6_49  L_19 = Vector3_op_Addition_m6_221(NULL /*static, unused*/, L_15, L_18, /*hidden argument*/NULL);
		V_5 = L_19;
		GameObject_t6_85 * L_20 = V_0;
		NullCheck(L_20);
		String_t* L_21 = Object_get_name_m6_562(L_20, /*hidden argument*/NULL);
		Vector3_t6_49  L_22 = V_5;
		Quaternion_t6_51  L_23 = Quaternion_get_identity_m6_244(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(PhotonNetwork_t8_114_il2cpp_TypeInfo_var);
		PhotonNetwork_Instantiate_m8_700(NULL /*static, unused*/, L_21, L_22, L_23, 0, /*hidden argument*/NULL);
		int32_t L_24 = V_2;
		V_2 = ((int32_t)((int32_t)L_24+(int32_t)1));
	}

IL_009e:
	{
		int32_t L_25 = V_2;
		GameObjectU5BU5D_t6_258* L_26 = V_1;
		NullCheck(L_26);
		if ((((int32_t)L_25) < ((int32_t)(((int32_t)((int32_t)(((Array_t *)L_26)->max_length)))))))
		{
			goto IL_0019;
		}
	}

IL_00a7:
	{
		return;
	}
}
// System.Void OnStartDelete::.ctor()
extern "C" void OnStartDelete__ctor_m8_979 (OnStartDelete_t8_162 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m6_525(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void OnStartDelete::Start()
extern "C" void OnStartDelete_Start_m8_980 (OnStartDelete_t8_162 * __this, const MethodInfo* method)
{
	{
		GameObject_t6_85 * L_0 = Component_get_gameObject_m6_583(__this, /*hidden argument*/NULL);
		Object_DestroyObject_m6_567(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void PickupItem::.ctor()
extern "C" void PickupItem__ctor_m8_981 (PickupItem_t8_163 * __this, const MethodInfo* method)
{
	{
		__this->___SecondsBeforeRespawn_2 = (2.0f);
		MonoBehaviour__ctor_m8_494(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void PickupItem::.cctor()
extern TypeInfo* HashSet_1_t2_17_il2cpp_TypeInfo_var;
extern TypeInfo* PickupItem_t8_163_il2cpp_TypeInfo_var;
extern const MethodInfo* HashSet_1__ctor_m2_59_MethodInfo_var;
extern "C" void PickupItem__cctor_m8_982 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		HashSet_1_t2_17_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1192);
		PickupItem_t8_163_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1079);
		HashSet_1__ctor_m2_59_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483961);
		s_Il2CppMethodIntialized = true;
	}
	{
		HashSet_1_t2_17 * L_0 = (HashSet_1_t2_17 *)il2cpp_codegen_object_new (HashSet_1_t2_17_il2cpp_TypeInfo_var);
		HashSet_1__ctor_m2_59(L_0, /*hidden argument*/HashSet_1__ctor_m2_59_MethodInfo_var);
		((PickupItem_t8_163_StaticFields*)PickupItem_t8_163_il2cpp_TypeInfo_var->static_fields)->___DisabledPickupItems_8 = L_0;
		return;
	}
}
// System.Int32 PickupItem::get_ViewID()
extern "C" int32_t PickupItem_get_ViewID_m8_983 (PickupItem_t8_163 * __this, const MethodInfo* method)
{
	{
		PhotonView_t8_3 * L_0 = MonoBehaviour_get_photonView_m8_495(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		int32_t L_1 = PhotonView_get_viewID_m8_767(L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Void PickupItem::OnTriggerEnter(UnityEngine.Collider)
extern const MethodInfo* Component_GetComponent_TisPhotonView_t8_3_m6_1658_MethodInfo_var;
extern "C" void PickupItem_OnTriggerEnter_m8_984 (PickupItem_t8_163 * __this, Collider_t6_100 * ___other, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Component_GetComponent_TisPhotonView_t8_3_m6_1658_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483836);
		s_Il2CppMethodIntialized = true;
	}
	PhotonView_t8_3 * V_0 = {0};
	{
		Collider_t6_100 * L_0 = ___other;
		NullCheck(L_0);
		PhotonView_t8_3 * L_1 = Component_GetComponent_TisPhotonView_t8_3_m6_1658(L_0, /*hidden argument*/Component_GetComponent_TisPhotonView_t8_3_m6_1658_MethodInfo_var);
		V_0 = L_1;
		bool L_2 = (__this->___PickupOnTrigger_3);
		if (!L_2)
		{
			goto IL_002f;
		}
	}
	{
		PhotonView_t8_3 * L_3 = V_0;
		bool L_4 = Object_op_Inequality_m6_580(NULL /*static, unused*/, L_3, (Object_t6_5 *)NULL, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_002f;
		}
	}
	{
		PhotonView_t8_3 * L_5 = V_0;
		NullCheck(L_5);
		bool L_6 = PhotonView_get_isMine_m8_774(L_5, /*hidden argument*/NULL);
		if (!L_6)
		{
			goto IL_002f;
		}
	}
	{
		PickupItem_Pickup_m8_986(__this, /*hidden argument*/NULL);
	}

IL_002f:
	{
		return;
	}
}
// System.Void PickupItem::OnPhotonSerializeView(PhotonStream,PhotonMessageInfo)
extern TypeInfo* Vector3_t6_49_il2cpp_TypeInfo_var;
extern "C" void PickupItem_OnPhotonSerializeView_m8_985 (PickupItem_t8_163 * __this, PhotonStream_t8_106 * ___stream, PhotonMessageInfo_t8_104 * ___info, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Vector3_t6_49_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(941);
		s_Il2CppMethodIntialized = true;
	}
	Vector3_t6_49  V_0 = {0};
	{
		PhotonStream_t8_106 * L_0 = ___stream;
		NullCheck(L_0);
		bool L_1 = PhotonStream_get_isWriting_m8_538(L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_003b;
		}
	}
	{
		float L_2 = (__this->___SecondsBeforeRespawn_2);
		if ((!(((float)L_2) <= ((float)(0.0f)))))
		{
			goto IL_003b;
		}
	}
	{
		PhotonStream_t8_106 * L_3 = ___stream;
		GameObject_t6_85 * L_4 = Component_get_gameObject_m6_583(__this, /*hidden argument*/NULL);
		NullCheck(L_4);
		Transform_t6_61 * L_5 = GameObject_get_transform_m6_595(L_4, /*hidden argument*/NULL);
		NullCheck(L_5);
		Vector3_t6_49  L_6 = Transform_get_position_m6_611(L_5, /*hidden argument*/NULL);
		Vector3_t6_49  L_7 = L_6;
		Object_t * L_8 = Box(Vector3_t6_49_il2cpp_TypeInfo_var, &L_7);
		NullCheck(L_3);
		PhotonStream_SendNext_m8_543(L_3, L_8, /*hidden argument*/NULL);
		goto IL_0058;
	}

IL_003b:
	{
		PhotonStream_t8_106 * L_9 = ___stream;
		NullCheck(L_9);
		Object_t * L_10 = PhotonStream_ReceiveNext_m8_541(L_9, /*hidden argument*/NULL);
		V_0 = ((*(Vector3_t6_49 *)((Vector3_t6_49 *)UnBox (L_10, Vector3_t6_49_il2cpp_TypeInfo_var))));
		GameObject_t6_85 * L_11 = Component_get_gameObject_m6_583(__this, /*hidden argument*/NULL);
		NullCheck(L_11);
		Transform_t6_61 * L_12 = GameObject_get_transform_m6_595(L_11, /*hidden argument*/NULL);
		Vector3_t6_49  L_13 = V_0;
		NullCheck(L_12);
		Transform_set_position_m6_612(L_12, L_13, /*hidden argument*/NULL);
	}

IL_0058:
	{
		return;
	}
}
// System.Void PickupItem::Pickup()
extern TypeInfo* ObjectU5BU5D_t1_157_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3542;
extern "C" void PickupItem_Pickup_m8_986 (PickupItem_t8_163 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ObjectU5BU5D_t1_157_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(19);
		_stringLiteral3542 = il2cpp_codegen_string_literal_from_index(3542);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool L_0 = (__this->___SentPickup_6);
		if (!L_0)
		{
			goto IL_000c;
		}
	}
	{
		return;
	}

IL_000c:
	{
		__this->___SentPickup_6 = 1;
		PhotonView_t8_3 * L_1 = MonoBehaviour_get_photonView_m8_495(__this, /*hidden argument*/NULL);
		NullCheck(L_1);
		PhotonView_RPC_m8_786(L_1, _stringLiteral3542, 5, ((ObjectU5BU5D_t1_157*)SZArrayNew(ObjectU5BU5D_t1_157_il2cpp_TypeInfo_var, 0)), /*hidden argument*/NULL);
		return;
	}
}
// System.Void PickupItem::Drop()
extern TypeInfo* ObjectU5BU5D_t1_157_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3543;
extern "C" void PickupItem_Drop_m8_987 (PickupItem_t8_163 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ObjectU5BU5D_t1_157_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(19);
		_stringLiteral3543 = il2cpp_codegen_string_literal_from_index(3543);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool L_0 = (__this->___PickupIsMine_4);
		if (!L_0)
		{
			goto IL_0022;
		}
	}
	{
		PhotonView_t8_3 * L_1 = MonoBehaviour_get_photonView_m8_495(__this, /*hidden argument*/NULL);
		NullCheck(L_1);
		PhotonView_RPC_m8_786(L_1, _stringLiteral3543, 5, ((ObjectU5BU5D_t1_157*)SZArrayNew(ObjectU5BU5D_t1_157_il2cpp_TypeInfo_var, 0)), /*hidden argument*/NULL);
	}

IL_0022:
	{
		return;
	}
}
// System.Void PickupItem::Drop(UnityEngine.Vector3)
extern TypeInfo* ObjectU5BU5D_t1_157_il2cpp_TypeInfo_var;
extern TypeInfo* Vector3_t6_49_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3543;
extern "C" void PickupItem_Drop_m8_988 (PickupItem_t8_163 * __this, Vector3_t6_49  ___newPosition, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ObjectU5BU5D_t1_157_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(19);
		Vector3_t6_49_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(941);
		_stringLiteral3543 = il2cpp_codegen_string_literal_from_index(3543);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool L_0 = (__this->___PickupIsMine_4);
		if (!L_0)
		{
			goto IL_002b;
		}
	}
	{
		PhotonView_t8_3 * L_1 = MonoBehaviour_get_photonView_m8_495(__this, /*hidden argument*/NULL);
		ObjectU5BU5D_t1_157* L_2 = ((ObjectU5BU5D_t1_157*)SZArrayNew(ObjectU5BU5D_t1_157_il2cpp_TypeInfo_var, 1));
		Vector3_t6_49  L_3 = ___newPosition;
		Vector3_t6_49  L_4 = L_3;
		Object_t * L_5 = Box(Vector3_t6_49_il2cpp_TypeInfo_var, &L_4);
		NullCheck(L_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_2, 0);
		ArrayElementTypeCheck (L_2, L_5);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_2, 0, sizeof(Object_t *))) = (Object_t *)L_5;
		NullCheck(L_1);
		PhotonView_RPC_m8_786(L_1, _stringLiteral3543, 5, L_2, /*hidden argument*/NULL);
	}

IL_002b:
	{
		return;
	}
}
// System.Void PickupItem::PunPickup(PhotonMessageInfo)
extern TypeInfo* ObjectU5BU5D_t1_157_il2cpp_TypeInfo_var;
extern TypeInfo* Single_t1_17_il2cpp_TypeInfo_var;
extern TypeInfo* Double_t1_18_il2cpp_TypeInfo_var;
extern TypeInfo* PhotonNetwork_t8_114_il2cpp_TypeInfo_var;
extern TypeInfo* Boolean_t1_20_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3544;
extern Il2CppCodeGenString* _stringLiteral3545;
extern Il2CppCodeGenString* _stringLiteral3546;
extern Il2CppCodeGenString* _stringLiteral3547;
extern Il2CppCodeGenString* _stringLiteral3548;
extern "C" void PickupItem_PunPickup_m8_989 (PickupItem_t8_163 * __this, PhotonMessageInfo_t8_104 * ___msgInfo, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ObjectU5BU5D_t1_157_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(19);
		Single_t1_17_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(43);
		Double_t1_18_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(44);
		PhotonNetwork_t8_114_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1058);
		Boolean_t1_20_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(49);
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(11);
		_stringLiteral3544 = il2cpp_codegen_string_literal_from_index(3544);
		_stringLiteral3545 = il2cpp_codegen_string_literal_from_index(3545);
		_stringLiteral3546 = il2cpp_codegen_string_literal_from_index(3546);
		_stringLiteral3547 = il2cpp_codegen_string_literal_from_index(3547);
		_stringLiteral3548 = il2cpp_codegen_string_literal_from_index(3548);
		s_Il2CppMethodIntialized = true;
	}
	double V_0 = 0.0;
	double V_1 = 0.0;
	{
		PhotonMessageInfo_t8_104 * L_0 = ___msgInfo;
		NullCheck(L_0);
		PhotonPlayer_t8_102 * L_1 = (L_0->___sender_1);
		NullCheck(L_1);
		bool L_2 = (L_1->___isLocal_2);
		if (!L_2)
		{
			goto IL_0017;
		}
	}
	{
		__this->___SentPickup_6 = 0;
	}

IL_0017:
	{
		GameObject_t6_85 * L_3 = Component_get_gameObject_m6_583(__this, /*hidden argument*/NULL);
		bool L_4 = GameObjectExtensions_GetActive_m8_303(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		if (L_4)
		{
			goto IL_0092;
		}
	}
	{
		ObjectU5BU5D_t1_157* L_5 = ((ObjectU5BU5D_t1_157*)SZArrayNew(ObjectU5BU5D_t1_157_il2cpp_TypeInfo_var, 8));
		NullCheck(L_5);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_5, 0);
		ArrayElementTypeCheck (L_5, _stringLiteral3544);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_5, 0, sizeof(Object_t *))) = (Object_t *)_stringLiteral3544;
		ObjectU5BU5D_t1_157* L_6 = L_5;
		GameObject_t6_85 * L_7 = Component_get_gameObject_m6_583(__this, /*hidden argument*/NULL);
		NullCheck(L_6);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_6, 1);
		ArrayElementTypeCheck (L_6, L_7);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_6, 1, sizeof(Object_t *))) = (Object_t *)L_7;
		ObjectU5BU5D_t1_157* L_8 = L_6;
		NullCheck(L_8);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_8, 2);
		ArrayElementTypeCheck (L_8, _stringLiteral3545);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_8, 2, sizeof(Object_t *))) = (Object_t *)_stringLiteral3545;
		ObjectU5BU5D_t1_157* L_9 = L_8;
		float L_10 = (__this->___SecondsBeforeRespawn_2);
		float L_11 = L_10;
		Object_t * L_12 = Box(Single_t1_17_il2cpp_TypeInfo_var, &L_11);
		NullCheck(L_9);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_9, 3);
		ArrayElementTypeCheck (L_9, L_12);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_9, 3, sizeof(Object_t *))) = (Object_t *)L_12;
		ObjectU5BU5D_t1_157* L_13 = L_9;
		NullCheck(L_13);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_13, 4);
		ArrayElementTypeCheck (L_13, _stringLiteral3546);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_13, 4, sizeof(Object_t *))) = (Object_t *)_stringLiteral3546;
		ObjectU5BU5D_t1_157* L_14 = L_13;
		double L_15 = (__this->___TimeOfRespawn_7);
		double L_16 = L_15;
		Object_t * L_17 = Box(Double_t1_18_il2cpp_TypeInfo_var, &L_16);
		NullCheck(L_14);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_14, 5);
		ArrayElementTypeCheck (L_14, L_17);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_14, 5, sizeof(Object_t *))) = (Object_t *)L_17;
		ObjectU5BU5D_t1_157* L_18 = L_14;
		NullCheck(L_18);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_18, 6);
		ArrayElementTypeCheck (L_18, _stringLiteral3547);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_18, 6, sizeof(Object_t *))) = (Object_t *)_stringLiteral3547;
		ObjectU5BU5D_t1_157* L_19 = L_18;
		double L_20 = (__this->___TimeOfRespawn_7);
		IL2CPP_RUNTIME_CLASS_INIT(PhotonNetwork_t8_114_il2cpp_TypeInfo_var);
		double L_21 = PhotonNetwork_get_time_m8_646(NULL /*static, unused*/, /*hidden argument*/NULL);
		bool L_22 = ((((double)L_20) > ((double)L_21))? 1 : 0);
		Object_t * L_23 = Box(Boolean_t1_20_il2cpp_TypeInfo_var, &L_22);
		NullCheck(L_19);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_19, 7);
		ArrayElementTypeCheck (L_19, L_23);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_19, 7, sizeof(Object_t *))) = (Object_t *)L_23;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_24 = String_Concat_m1_421(NULL /*static, unused*/, L_19, /*hidden argument*/NULL);
		Debug_Log_m6_489(NULL /*static, unused*/, L_24, /*hidden argument*/NULL);
		return;
	}

IL_0092:
	{
		PhotonMessageInfo_t8_104 * L_25 = ___msgInfo;
		NullCheck(L_25);
		PhotonPlayer_t8_102 * L_26 = (L_25->___sender_1);
		NullCheck(L_26);
		bool L_27 = (L_26->___isLocal_2);
		__this->___PickupIsMine_4 = L_27;
		MonoBehaviour_t6_80 * L_28 = (__this->___OnPickedUpCall_5);
		bool L_29 = Object_op_Inequality_m6_580(NULL /*static, unused*/, L_28, (Object_t6_5 *)NULL, /*hidden argument*/NULL);
		if (!L_29)
		{
			goto IL_00c5;
		}
	}
	{
		MonoBehaviour_t6_80 * L_30 = (__this->___OnPickedUpCall_5);
		NullCheck(L_30);
		Component_SendMessage_m6_587(L_30, _stringLiteral3548, __this, /*hidden argument*/NULL);
	}

IL_00c5:
	{
		float L_31 = (__this->___SecondsBeforeRespawn_2);
		if ((!(((float)L_31) <= ((float)(0.0f)))))
		{
			goto IL_00e5;
		}
	}
	{
		PickupItem_PickedUp_m8_990(__this, (0.0f), /*hidden argument*/NULL);
		goto IL_0113;
	}

IL_00e5:
	{
		IL2CPP_RUNTIME_CLASS_INIT(PhotonNetwork_t8_114_il2cpp_TypeInfo_var);
		double L_32 = PhotonNetwork_get_time_m8_646(NULL /*static, unused*/, /*hidden argument*/NULL);
		PhotonMessageInfo_t8_104 * L_33 = ___msgInfo;
		NullCheck(L_33);
		double L_34 = PhotonMessageInfo_get_timestamp_m8_526(L_33, /*hidden argument*/NULL);
		V_0 = ((double)((double)L_32-(double)L_34));
		float L_35 = (__this->___SecondsBeforeRespawn_2);
		double L_36 = V_0;
		V_1 = ((double)((double)(((double)((double)L_35)))-(double)L_36));
		double L_37 = V_1;
		if ((!(((double)L_37) > ((double)(0.0)))))
		{
			goto IL_0113;
		}
	}
	{
		double L_38 = V_1;
		PickupItem_PickedUp_m8_990(__this, (((float)((float)L_38))), /*hidden argument*/NULL);
	}

IL_0113:
	{
		return;
	}
}
// System.Void PickupItem::PickedUp(System.Single)
extern TypeInfo* PickupItem_t8_163_il2cpp_TypeInfo_var;
extern TypeInfo* PhotonNetwork_t8_114_il2cpp_TypeInfo_var;
extern const MethodInfo* HashSet_1_Add_m2_60_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral3543;
extern "C" void PickupItem_PickedUp_m8_990 (PickupItem_t8_163 * __this, float ___timeUntilRespawn, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		PickupItem_t8_163_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1079);
		PhotonNetwork_t8_114_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1058);
		HashSet_1_Add_m2_60_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483962);
		_stringLiteral3543 = il2cpp_codegen_string_literal_from_index(3543);
		s_Il2CppMethodIntialized = true;
	}
	{
		GameObject_t6_85 * L_0 = Component_get_gameObject_m6_583(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		GameObject_SetActive_m6_596(L_0, 0, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(PickupItem_t8_163_il2cpp_TypeInfo_var);
		HashSet_1_t2_17 * L_1 = ((PickupItem_t8_163_StaticFields*)PickupItem_t8_163_il2cpp_TypeInfo_var->static_fields)->___DisabledPickupItems_8;
		NullCheck(L_1);
		HashSet_1_Add_m2_60(L_1, __this, /*hidden argument*/HashSet_1_Add_m2_60_MethodInfo_var);
		__this->___TimeOfRespawn_7 = (0.0);
		float L_2 = ___timeUntilRespawn;
		if ((!(((float)L_2) > ((float)(0.0f)))))
		{
			goto IL_004c;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(PhotonNetwork_t8_114_il2cpp_TypeInfo_var);
		double L_3 = PhotonNetwork_get_time_m8_646(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_4 = ___timeUntilRespawn;
		__this->___TimeOfRespawn_7 = ((double)((double)L_3+(double)(((double)((double)L_4)))));
		float L_5 = ___timeUntilRespawn;
		MonoBehaviour_Invoke_m6_527(__this, _stringLiteral3543, L_5, /*hidden argument*/NULL);
	}

IL_004c:
	{
		return;
	}
}
// System.Void PickupItem::PunRespawn(UnityEngine.Vector3)
extern Il2CppCodeGenString* _stringLiteral3549;
extern "C" void PickupItem_PunRespawn_m8_991 (PickupItem_t8_163 * __this, Vector3_t6_49  ___pos, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		_stringLiteral3549 = il2cpp_codegen_string_literal_from_index(3549);
		s_Il2CppMethodIntialized = true;
	}
	{
		Debug_Log_m6_489(NULL /*static, unused*/, _stringLiteral3549, /*hidden argument*/NULL);
		PickupItem_PunRespawn_m8_992(__this, /*hidden argument*/NULL);
		GameObject_t6_85 * L_0 = Component_get_gameObject_m6_583(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		Transform_t6_61 * L_1 = GameObject_get_transform_m6_595(L_0, /*hidden argument*/NULL);
		Vector3_t6_49  L_2 = ___pos;
		NullCheck(L_1);
		Transform_set_position_m6_612(L_1, L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Void PickupItem::PunRespawn()
extern TypeInfo* PickupItem_t8_163_il2cpp_TypeInfo_var;
extern "C" void PickupItem_PunRespawn_m8_992 (PickupItem_t8_163 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		PickupItem_t8_163_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1079);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(PickupItem_t8_163_il2cpp_TypeInfo_var);
		HashSet_1_t2_17 * L_0 = ((PickupItem_t8_163_StaticFields*)PickupItem_t8_163_il2cpp_TypeInfo_var->static_fields)->___DisabledPickupItems_8;
		NullCheck(L_0);
		VirtFuncInvoker1< bool, PickupItem_t8_163 * >::Invoke(10 /* System.Boolean System.Collections.Generic.HashSet`1<PickupItem>::Remove(!0) */, L_0, __this);
		__this->___TimeOfRespawn_7 = (0.0);
		__this->___PickupIsMine_4 = 0;
		GameObject_t6_85 * L_1 = Component_get_gameObject_m6_583(__this, /*hidden argument*/NULL);
		bool L_2 = Object_op_Inequality_m6_580(NULL /*static, unused*/, L_1, (Object_t6_5 *)NULL, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_003f;
		}
	}
	{
		GameObject_t6_85 * L_3 = Component_get_gameObject_m6_583(__this, /*hidden argument*/NULL);
		NullCheck(L_3);
		GameObject_SetActive_m6_596(L_3, 1, /*hidden argument*/NULL);
	}

IL_003f:
	{
		return;
	}
}
// System.Void PickupItemSimple::.ctor()
extern "C" void PickupItemSimple__ctor_m8_993 (PickupItemSimple_t8_164 * __this, const MethodInfo* method)
{
	{
		__this->___SecondsBeforeRespawn_2 = (2.0f);
		MonoBehaviour__ctor_m8_494(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void PickupItemSimple::OnTriggerEnter(UnityEngine.Collider)
extern const MethodInfo* Component_GetComponent_TisPhotonView_t8_3_m6_1658_MethodInfo_var;
extern "C" void PickupItemSimple_OnTriggerEnter_m8_994 (PickupItemSimple_t8_164 * __this, Collider_t6_100 * ___other, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Component_GetComponent_TisPhotonView_t8_3_m6_1658_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483836);
		s_Il2CppMethodIntialized = true;
	}
	PhotonView_t8_3 * V_0 = {0};
	{
		Collider_t6_100 * L_0 = ___other;
		NullCheck(L_0);
		PhotonView_t8_3 * L_1 = Component_GetComponent_TisPhotonView_t8_3_m6_1658(L_0, /*hidden argument*/Component_GetComponent_TisPhotonView_t8_3_m6_1658_MethodInfo_var);
		V_0 = L_1;
		bool L_2 = (__this->___PickupOnCollide_3);
		if (!L_2)
		{
			goto IL_002f;
		}
	}
	{
		PhotonView_t8_3 * L_3 = V_0;
		bool L_4 = Object_op_Inequality_m6_580(NULL /*static, unused*/, L_3, (Object_t6_5 *)NULL, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_002f;
		}
	}
	{
		PhotonView_t8_3 * L_5 = V_0;
		NullCheck(L_5);
		bool L_6 = PhotonView_get_isMine_m8_774(L_5, /*hidden argument*/NULL);
		if (!L_6)
		{
			goto IL_002f;
		}
	}
	{
		PickupItemSimple_Pickup_m8_995(__this, /*hidden argument*/NULL);
	}

IL_002f:
	{
		return;
	}
}
// System.Void PickupItemSimple::Pickup()
extern TypeInfo* ObjectU5BU5D_t1_157_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3550;
extern "C" void PickupItemSimple_Pickup_m8_995 (PickupItemSimple_t8_164 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ObjectU5BU5D_t1_157_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(19);
		_stringLiteral3550 = il2cpp_codegen_string_literal_from_index(3550);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool L_0 = (__this->___SentPickup_4);
		if (!L_0)
		{
			goto IL_000c;
		}
	}
	{
		return;
	}

IL_000c:
	{
		__this->___SentPickup_4 = 1;
		PhotonView_t8_3 * L_1 = MonoBehaviour_get_photonView_m8_495(__this, /*hidden argument*/NULL);
		NullCheck(L_1);
		PhotonView_RPC_m8_786(L_1, _stringLiteral3550, 5, ((ObjectU5BU5D_t1_157*)SZArrayNew(ObjectU5BU5D_t1_157_il2cpp_TypeInfo_var, 0)), /*hidden argument*/NULL);
		return;
	}
}
// System.Void PickupItemSimple::PunPickupSimple(PhotonMessageInfo)
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* PhotonNetwork_t8_114_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3544;
extern Il2CppCodeGenString* _stringLiteral3551;
extern "C" void PickupItemSimple_PunPickupSimple_m8_996 (PickupItemSimple_t8_164 * __this, PhotonMessageInfo_t8_104 * ___msgInfo, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(11);
		PhotonNetwork_t8_114_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1058);
		_stringLiteral3544 = il2cpp_codegen_string_literal_from_index(3544);
		_stringLiteral3551 = il2cpp_codegen_string_literal_from_index(3551);
		s_Il2CppMethodIntialized = true;
	}
	double V_0 = 0.0;
	float V_1 = 0.0f;
	{
		bool L_0 = (__this->___SentPickup_4);
		if (!L_0)
		{
			goto IL_0030;
		}
	}
	{
		PhotonMessageInfo_t8_104 * L_1 = ___msgInfo;
		NullCheck(L_1);
		PhotonPlayer_t8_102 * L_2 = (L_1->___sender_1);
		NullCheck(L_2);
		bool L_3 = (L_2->___isLocal_2);
		if (!L_3)
		{
			goto IL_0030;
		}
	}
	{
		GameObject_t6_85 * L_4 = Component_get_gameObject_m6_583(__this, /*hidden argument*/NULL);
		bool L_5 = GameObjectExtensions_GetActive_m8_303(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_0030;
		}
	}
	{
		goto IL_0030;
	}

IL_0030:
	{
		__this->___SentPickup_4 = 0;
		GameObject_t6_85 * L_6 = Component_get_gameObject_m6_583(__this, /*hidden argument*/NULL);
		bool L_7 = GameObjectExtensions_GetActive_m8_303(NULL /*static, unused*/, L_6, /*hidden argument*/NULL);
		if (L_7)
		{
			goto IL_005d;
		}
	}
	{
		GameObject_t6_85 * L_8 = Component_get_gameObject_m6_583(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_9 = String_Concat_m1_416(NULL /*static, unused*/, _stringLiteral3544, L_8, /*hidden argument*/NULL);
		Debug_Log_m6_489(NULL /*static, unused*/, L_9, /*hidden argument*/NULL);
		return;
	}

IL_005d:
	{
		IL2CPP_RUNTIME_CLASS_INIT(PhotonNetwork_t8_114_il2cpp_TypeInfo_var);
		double L_10 = PhotonNetwork_get_time_m8_646(NULL /*static, unused*/, /*hidden argument*/NULL);
		PhotonMessageInfo_t8_104 * L_11 = ___msgInfo;
		NullCheck(L_11);
		double L_12 = PhotonMessageInfo_get_timestamp_m8_526(L_11, /*hidden argument*/NULL);
		V_0 = ((double)((double)L_10-(double)L_12));
		float L_13 = (__this->___SecondsBeforeRespawn_2);
		double L_14 = V_0;
		V_1 = ((float)((float)L_13-(float)(((float)((float)L_14)))));
		float L_15 = V_1;
		if ((!(((float)L_15) > ((float)(0.0f)))))
		{
			goto IL_0097;
		}
	}
	{
		GameObject_t6_85 * L_16 = Component_get_gameObject_m6_583(__this, /*hidden argument*/NULL);
		NullCheck(L_16);
		GameObject_SetActive_m6_596(L_16, 0, /*hidden argument*/NULL);
		float L_17 = V_1;
		MonoBehaviour_Invoke_m6_527(__this, _stringLiteral3551, L_17, /*hidden argument*/NULL);
	}

IL_0097:
	{
		return;
	}
}
// System.Void PickupItemSimple::RespawnAfter()
extern "C" void PickupItemSimple_RespawnAfter_m8_997 (PickupItemSimple_t8_164 * __this, const MethodInfo* method)
{
	{
		GameObject_t6_85 * L_0 = Component_get_gameObject_m6_583(__this, /*hidden argument*/NULL);
		bool L_1 = Object_op_Inequality_m6_580(NULL /*static, unused*/, L_0, (Object_t6_5 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_001d;
		}
	}
	{
		GameObject_t6_85 * L_2 = Component_get_gameObject_m6_583(__this, /*hidden argument*/NULL);
		NullCheck(L_2);
		GameObject_SetActive_m6_596(L_2, 1, /*hidden argument*/NULL);
	}

IL_001d:
	{
		return;
	}
}
// System.Void PickupItemSyncer::.ctor()
extern "C" void PickupItemSyncer__ctor_m8_998 (PickupItemSyncer_t8_165 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m8_494(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void PickupItemSyncer::OnPhotonPlayerConnected(PhotonPlayer)
extern TypeInfo* PhotonNetwork_t8_114_il2cpp_TypeInfo_var;
extern "C" void PickupItemSyncer_OnPhotonPlayerConnected_m8_999 (PickupItemSyncer_t8_165 * __this, PhotonPlayer_t8_102 * ___newPlayer, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		PhotonNetwork_t8_114_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1058);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(PhotonNetwork_t8_114_il2cpp_TypeInfo_var);
		bool L_0 = PhotonNetwork_get_isMasterClient_m8_648(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_0011;
		}
	}
	{
		PhotonPlayer_t8_102 * L_1 = ___newPlayer;
		PickupItemSyncer_SendPickedUpItems_m8_1003(__this, L_1, /*hidden argument*/NULL);
	}

IL_0011:
	{
		return;
	}
}
// System.Void PickupItemSyncer::OnJoinedRoom()
extern TypeInfo* ObjectU5BU5D_t1_157_il2cpp_TypeInfo_var;
extern TypeInfo* PhotonNetwork_t8_114_il2cpp_TypeInfo_var;
extern TypeInfo* Boolean_t1_20_il2cpp_TypeInfo_var;
extern TypeInfo* Int32_t1_3_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3552;
extern Il2CppCodeGenString* _stringLiteral3553;
extern Il2CppCodeGenString* _stringLiteral3554;
extern "C" void PickupItemSyncer_OnJoinedRoom_m8_1000 (PickupItemSyncer_t8_165 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ObjectU5BU5D_t1_157_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(19);
		PhotonNetwork_t8_114_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1058);
		Boolean_t1_20_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(49);
		Int32_t1_3_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(8);
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(11);
		_stringLiteral3552 = il2cpp_codegen_string_literal_from_index(3552);
		_stringLiteral3553 = il2cpp_codegen_string_literal_from_index(3553);
		_stringLiteral3554 = il2cpp_codegen_string_literal_from_index(3554);
		s_Il2CppMethodIntialized = true;
	}
	{
		ObjectU5BU5D_t1_157* L_0 = ((ObjectU5BU5D_t1_157*)SZArrayNew(ObjectU5BU5D_t1_157_il2cpp_TypeInfo_var, 4));
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, 0);
		ArrayElementTypeCheck (L_0, _stringLiteral3552);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_0, 0, sizeof(Object_t *))) = (Object_t *)_stringLiteral3552;
		ObjectU5BU5D_t1_157* L_1 = L_0;
		IL2CPP_RUNTIME_CLASS_INIT(PhotonNetwork_t8_114_il2cpp_TypeInfo_var);
		bool L_2 = PhotonNetwork_get_isMasterClient_m8_648(NULL /*static, unused*/, /*hidden argument*/NULL);
		bool L_3 = L_2;
		Object_t * L_4 = Box(Boolean_t1_20_il2cpp_TypeInfo_var, &L_3);
		NullCheck(L_1);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_1, 1);
		ArrayElementTypeCheck (L_1, L_4);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_1, 1, sizeof(Object_t *))) = (Object_t *)L_4;
		ObjectU5BU5D_t1_157* L_5 = L_1;
		NullCheck(L_5);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_5, 2);
		ArrayElementTypeCheck (L_5, _stringLiteral3553);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_5, 2, sizeof(Object_t *))) = (Object_t *)_stringLiteral3553;
		ObjectU5BU5D_t1_157* L_6 = L_5;
		PhotonPlayer_t8_102 * L_7 = PhotonNetwork_get_player_m8_612(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_7);
		int32_t L_8 = PhotonPlayer_get_ID_m8_730(L_7, /*hidden argument*/NULL);
		int32_t L_9 = L_8;
		Object_t * L_10 = Box(Int32_t1_3_il2cpp_TypeInfo_var, &L_9);
		NullCheck(L_6);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_6, 3);
		ArrayElementTypeCheck (L_6, L_10);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_6, 3, sizeof(Object_t *))) = (Object_t *)L_10;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_11 = String_Concat_m1_421(NULL /*static, unused*/, L_6, /*hidden argument*/NULL);
		Debug_Log_m6_489(NULL /*static, unused*/, L_11, /*hidden argument*/NULL);
		bool L_12 = PhotonNetwork_get_isMasterClient_m8_648(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->___IsWaitingForPickupInit_3 = ((((int32_t)L_12) == ((int32_t)0))? 1 : 0);
		PhotonPlayerU5BU5D_t8_101* L_13 = PhotonNetwork_get_playerList_m8_616(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_13);
		if ((((int32_t)(((int32_t)((int32_t)(((Array_t *)L_13)->max_length))))) < ((int32_t)2)))
		{
			goto IL_006a;
		}
	}
	{
		MonoBehaviour_Invoke_m6_527(__this, _stringLiteral3554, (2.0f), /*hidden argument*/NULL);
	}

IL_006a:
	{
		return;
	}
}
// System.Void PickupItemSyncer::AskForPickupItemSpawnTimes()
extern TypeInfo* PhotonNetwork_t8_114_il2cpp_TypeInfo_var;
extern TypeInfo* ObjectU5BU5D_t1_157_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3555;
extern Il2CppCodeGenString* _stringLiteral3556;
extern Il2CppCodeGenString* _stringLiteral3557;
extern "C" void PickupItemSyncer_AskForPickupItemSpawnTimes_m8_1001 (PickupItemSyncer_t8_165 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		PhotonNetwork_t8_114_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1058);
		ObjectU5BU5D_t1_157_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(19);
		_stringLiteral3555 = il2cpp_codegen_string_literal_from_index(3555);
		_stringLiteral3556 = il2cpp_codegen_string_literal_from_index(3556);
		_stringLiteral3557 = il2cpp_codegen_string_literal_from_index(3557);
		s_Il2CppMethodIntialized = true;
	}
	PhotonPlayer_t8_102 * V_0 = {0};
	{
		bool L_0 = (__this->___IsWaitingForPickupInit_3);
		if (!L_0)
		{
			goto IL_0099;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(PhotonNetwork_t8_114_il2cpp_TypeInfo_var);
		PhotonPlayerU5BU5D_t8_101* L_1 = PhotonNetwork_get_playerList_m8_616(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_1);
		if ((((int32_t)(((int32_t)((int32_t)(((Array_t *)L_1)->max_length))))) >= ((int32_t)2)))
		{
			goto IL_002a;
		}
	}
	{
		Debug_Log_m6_489(NULL /*static, unused*/, _stringLiteral3555, /*hidden argument*/NULL);
		__this->___IsWaitingForPickupInit_3 = 0;
		return;
	}

IL_002a:
	{
		IL2CPP_RUNTIME_CLASS_INIT(PhotonNetwork_t8_114_il2cpp_TypeInfo_var);
		PhotonPlayer_t8_102 * L_2 = PhotonNetwork_get_masterClient_m8_613(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_2);
		PhotonPlayer_t8_102 * L_3 = PhotonPlayer_GetNext_m8_744(L_2, /*hidden argument*/NULL);
		V_0 = L_3;
		PhotonPlayer_t8_102 * L_4 = V_0;
		if (!L_4)
		{
			goto IL_004b;
		}
	}
	{
		PhotonPlayer_t8_102 * L_5 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(PhotonNetwork_t8_114_il2cpp_TypeInfo_var);
		PhotonPlayer_t8_102 * L_6 = PhotonNetwork_get_player_m8_612(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_5);
		bool L_7 = (bool)VirtFuncInvoker1< bool, Object_t * >::Invoke(0 /* System.Boolean PhotonPlayer::Equals(System.Object) */, L_5, L_6);
		if (!L_7)
		{
			goto IL_0056;
		}
	}

IL_004b:
	{
		IL2CPP_RUNTIME_CLASS_INIT(PhotonNetwork_t8_114_il2cpp_TypeInfo_var);
		PhotonPlayer_t8_102 * L_8 = PhotonNetwork_get_player_m8_612(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_8);
		PhotonPlayer_t8_102 * L_9 = PhotonPlayer_GetNext_m8_744(L_8, /*hidden argument*/NULL);
		V_0 = L_9;
	}

IL_0056:
	{
		PhotonPlayer_t8_102 * L_10 = V_0;
		if (!L_10)
		{
			goto IL_0088;
		}
	}
	{
		PhotonPlayer_t8_102 * L_11 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(PhotonNetwork_t8_114_il2cpp_TypeInfo_var);
		PhotonPlayer_t8_102 * L_12 = PhotonNetwork_get_player_m8_612(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_11);
		bool L_13 = (bool)VirtFuncInvoker1< bool, Object_t * >::Invoke(0 /* System.Boolean PhotonPlayer::Equals(System.Object) */, L_11, L_12);
		if (L_13)
		{
			goto IL_0088;
		}
	}
	{
		PhotonView_t8_3 * L_14 = MonoBehaviour_get_photonView_m8_495(__this, /*hidden argument*/NULL);
		PhotonPlayer_t8_102 * L_15 = V_0;
		NullCheck(L_14);
		PhotonView_RPC_m8_788(L_14, _stringLiteral3556, L_15, ((ObjectU5BU5D_t1_157*)SZArrayNew(ObjectU5BU5D_t1_157_il2cpp_TypeInfo_var, 0)), /*hidden argument*/NULL);
		goto IL_0099;
	}

IL_0088:
	{
		Debug_Log_m6_489(NULL /*static, unused*/, _stringLiteral3557, /*hidden argument*/NULL);
		__this->___IsWaitingForPickupInit_3 = 0;
	}

IL_0099:
	{
		return;
	}
}
// System.Void PickupItemSyncer::RequestForPickupTimes(PhotonMessageInfo)
extern Il2CppCodeGenString* _stringLiteral3558;
extern "C" void PickupItemSyncer_RequestForPickupTimes_m8_1002 (PickupItemSyncer_t8_165 * __this, PhotonMessageInfo_t8_104 * ___msgInfo, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		_stringLiteral3558 = il2cpp_codegen_string_literal_from_index(3558);
		s_Il2CppMethodIntialized = true;
	}
	{
		PhotonMessageInfo_t8_104 * L_0 = ___msgInfo;
		NullCheck(L_0);
		PhotonPlayer_t8_102 * L_1 = (L_0->___sender_1);
		if (L_1)
		{
			goto IL_0016;
		}
	}
	{
		Debug_LogError_m6_490(NULL /*static, unused*/, _stringLiteral3558, /*hidden argument*/NULL);
		return;
	}

IL_0016:
	{
		PhotonMessageInfo_t8_104 * L_2 = ___msgInfo;
		NullCheck(L_2);
		PhotonPlayer_t8_102 * L_3 = (L_2->___sender_1);
		PickupItemSyncer_SendPickedUpItems_m8_1003(__this, L_3, /*hidden argument*/NULL);
		return;
	}
}
// System.Void PickupItemSyncer::SendPickedUpItems(PhotonPlayer)
extern TypeInfo* PhotonNetwork_t8_114_il2cpp_TypeInfo_var;
extern TypeInfo* PickupItem_t8_163_il2cpp_TypeInfo_var;
extern TypeInfo* PickupItemU5BU5D_t8_181_il2cpp_TypeInfo_var;
extern TypeInfo* List_1_t1_971_il2cpp_TypeInfo_var;
extern TypeInfo* ObjectU5BU5D_t1_157_il2cpp_TypeInfo_var;
extern TypeInfo* Int32_t1_3_il2cpp_TypeInfo_var;
extern TypeInfo* Double_t1_18_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern const MethodInfo* HashSet_1_CopyTo_m2_61_MethodInfo_var;
extern const MethodInfo* List_1__ctor_m1_5684_MethodInfo_var;
extern const MethodInfo* List_1_ToArray_m1_5685_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral3559;
extern Il2CppCodeGenString* _stringLiteral3560;
extern Il2CppCodeGenString* _stringLiteral3561;
extern Il2CppCodeGenString* _stringLiteral3562;
extern Il2CppCodeGenString* _stringLiteral93;
extern Il2CppCodeGenString* _stringLiteral3563;
extern Il2CppCodeGenString* _stringLiteral2576;
extern Il2CppCodeGenString* _stringLiteral3564;
extern "C" void PickupItemSyncer_SendPickedUpItems_m8_1003 (PickupItemSyncer_t8_165 * __this, PhotonPlayer_t8_102 * ___targtePlayer, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		PhotonNetwork_t8_114_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1058);
		PickupItem_t8_163_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1079);
		PickupItemU5BU5D_t8_181_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1193);
		List_1_t1_971_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1194);
		ObjectU5BU5D_t1_157_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(19);
		Int32_t1_3_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(8);
		Double_t1_18_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(44);
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(11);
		HashSet_1_CopyTo_m2_61_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483963);
		List_1__ctor_m1_5684_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483964);
		List_1_ToArray_m1_5685_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483965);
		_stringLiteral3559 = il2cpp_codegen_string_literal_from_index(3559);
		_stringLiteral3560 = il2cpp_codegen_string_literal_from_index(3560);
		_stringLiteral3561 = il2cpp_codegen_string_literal_from_index(3561);
		_stringLiteral3562 = il2cpp_codegen_string_literal_from_index(3562);
		_stringLiteral93 = il2cpp_codegen_string_literal_from_index(93);
		_stringLiteral3563 = il2cpp_codegen_string_literal_from_index(3563);
		_stringLiteral2576 = il2cpp_codegen_string_literal_from_index(2576);
		_stringLiteral3564 = il2cpp_codegen_string_literal_from_index(3564);
		s_Il2CppMethodIntialized = true;
	}
	double V_0 = 0.0;
	double V_1 = 0.0;
	PickupItemU5BU5D_t8_181* V_2 = {0};
	List_1_t1_971 * V_3 = {0};
	int32_t V_4 = 0;
	PickupItem_t8_163 * V_5 = {0};
	double V_6 = 0.0;
	{
		PhotonPlayer_t8_102 * L_0 = ___targtePlayer;
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		Debug_LogWarning_m6_491(NULL /*static, unused*/, _stringLiteral3559, /*hidden argument*/NULL);
		return;
	}

IL_0011:
	{
		IL2CPP_RUNTIME_CLASS_INIT(PhotonNetwork_t8_114_il2cpp_TypeInfo_var);
		double L_1 = PhotonNetwork_get_time_m8_646(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_0 = L_1;
		double L_2 = V_0;
		V_1 = ((double)((double)L_2+(double)(0.20000000298023224)));
		IL2CPP_RUNTIME_CLASS_INIT(PickupItem_t8_163_il2cpp_TypeInfo_var);
		HashSet_1_t2_17 * L_3 = ((PickupItem_t8_163_StaticFields*)PickupItem_t8_163_il2cpp_TypeInfo_var->static_fields)->___DisabledPickupItems_8;
		NullCheck(L_3);
		int32_t L_4 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(4 /* System.Int32 System.Collections.Generic.HashSet`1<PickupItem>::get_Count() */, L_3);
		V_2 = ((PickupItemU5BU5D_t8_181*)SZArrayNew(PickupItemU5BU5D_t8_181_il2cpp_TypeInfo_var, L_4));
		HashSet_1_t2_17 * L_5 = ((PickupItem_t8_163_StaticFields*)PickupItem_t8_163_il2cpp_TypeInfo_var->static_fields)->___DisabledPickupItems_8;
		PickupItemU5BU5D_t8_181* L_6 = V_2;
		NullCheck(L_5);
		HashSet_1_CopyTo_m2_61(L_5, L_6, /*hidden argument*/HashSet_1_CopyTo_m2_61_MethodInfo_var);
		PickupItemU5BU5D_t8_181* L_7 = V_2;
		NullCheck(L_7);
		List_1_t1_971 * L_8 = (List_1_t1_971 *)il2cpp_codegen_object_new (List_1_t1_971_il2cpp_TypeInfo_var);
		List_1__ctor_m1_5684(L_8, ((int32_t)((int32_t)(((int32_t)((int32_t)(((Array_t *)L_7)->max_length))))*(int32_t)2)), /*hidden argument*/List_1__ctor_m1_5684_MethodInfo_var);
		V_3 = L_8;
		V_4 = 0;
		goto IL_0124;
	}

IL_0051:
	{
		PickupItemU5BU5D_t8_181* L_9 = V_2;
		int32_t L_10 = V_4;
		NullCheck(L_9);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_9, L_10);
		int32_t L_11 = L_10;
		V_5 = (*(PickupItem_t8_163 **)(PickupItem_t8_163 **)SZArrayLdElema(L_9, L_11, sizeof(PickupItem_t8_163 *)));
		PickupItem_t8_163 * L_12 = V_5;
		NullCheck(L_12);
		float L_13 = (L_12->___SecondsBeforeRespawn_2);
		if ((!(((float)L_13) <= ((float)(0.0f)))))
		{
			goto IL_0086;
		}
	}
	{
		List_1_t1_971 * L_14 = V_3;
		PickupItem_t8_163 * L_15 = V_5;
		NullCheck(L_15);
		int32_t L_16 = PickupItem_get_ViewID_m8_983(L_15, /*hidden argument*/NULL);
		NullCheck(L_14);
		VirtActionInvoker1< float >::Invoke(19 /* System.Void System.Collections.Generic.List`1<System.Single>::Add(!0) */, L_14, (((float)((float)L_16))));
		List_1_t1_971 * L_17 = V_3;
		NullCheck(L_17);
		VirtActionInvoker1< float >::Invoke(19 /* System.Void System.Collections.Generic.List`1<System.Single>::Add(!0) */, L_17, (0.0f));
		goto IL_011e;
	}

IL_0086:
	{
		PickupItem_t8_163 * L_18 = V_5;
		NullCheck(L_18);
		double L_19 = (L_18->___TimeOfRespawn_7);
		IL2CPP_RUNTIME_CLASS_INIT(PhotonNetwork_t8_114_il2cpp_TypeInfo_var);
		double L_20 = PhotonNetwork_get_time_m8_646(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_6 = ((double)((double)L_19-(double)L_20));
		PickupItem_t8_163 * L_21 = V_5;
		NullCheck(L_21);
		double L_22 = (L_21->___TimeOfRespawn_7);
		double L_23 = V_1;
		if ((!(((double)L_22) > ((double)L_23))))
		{
			goto IL_011e;
		}
	}
	{
		ObjectU5BU5D_t1_157* L_24 = ((ObjectU5BU5D_t1_157*)SZArrayNew(ObjectU5BU5D_t1_157_il2cpp_TypeInfo_var, 8));
		PickupItem_t8_163 * L_25 = V_5;
		NullCheck(L_25);
		int32_t L_26 = PickupItem_get_ViewID_m8_983(L_25, /*hidden argument*/NULL);
		int32_t L_27 = L_26;
		Object_t * L_28 = Box(Int32_t1_3_il2cpp_TypeInfo_var, &L_27);
		NullCheck(L_24);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_24, 0);
		ArrayElementTypeCheck (L_24, L_28);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_24, 0, sizeof(Object_t *))) = (Object_t *)L_28;
		ObjectU5BU5D_t1_157* L_29 = L_24;
		NullCheck(L_29);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_29, 1);
		ArrayElementTypeCheck (L_29, _stringLiteral3560);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_29, 1, sizeof(Object_t *))) = (Object_t *)_stringLiteral3560;
		ObjectU5BU5D_t1_157* L_30 = L_29;
		PickupItem_t8_163 * L_31 = V_5;
		NullCheck(L_31);
		double L_32 = (L_31->___TimeOfRespawn_7);
		double L_33 = L_32;
		Object_t * L_34 = Box(Double_t1_18_il2cpp_TypeInfo_var, &L_33);
		NullCheck(L_30);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_30, 2);
		ArrayElementTypeCheck (L_30, L_34);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_30, 2, sizeof(Object_t *))) = (Object_t *)L_34;
		ObjectU5BU5D_t1_157* L_35 = L_30;
		NullCheck(L_35);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_35, 3);
		ArrayElementTypeCheck (L_35, _stringLiteral3561);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_35, 3, sizeof(Object_t *))) = (Object_t *)_stringLiteral3561;
		ObjectU5BU5D_t1_157* L_36 = L_35;
		double L_37 = V_6;
		double L_38 = L_37;
		Object_t * L_39 = Box(Double_t1_18_il2cpp_TypeInfo_var, &L_38);
		NullCheck(L_36);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_36, 4);
		ArrayElementTypeCheck (L_36, L_39);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_36, 4, sizeof(Object_t *))) = (Object_t *)L_39;
		ObjectU5BU5D_t1_157* L_40 = L_36;
		NullCheck(L_40);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_40, 5);
		ArrayElementTypeCheck (L_40, _stringLiteral3562);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_40, 5, sizeof(Object_t *))) = (Object_t *)_stringLiteral3562;
		ObjectU5BU5D_t1_157* L_41 = L_40;
		IL2CPP_RUNTIME_CLASS_INIT(PhotonNetwork_t8_114_il2cpp_TypeInfo_var);
		double L_42 = PhotonNetwork_get_time_m8_646(NULL /*static, unused*/, /*hidden argument*/NULL);
		double L_43 = L_42;
		Object_t * L_44 = Box(Double_t1_18_il2cpp_TypeInfo_var, &L_43);
		NullCheck(L_41);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_41, 6);
		ArrayElementTypeCheck (L_41, L_44);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_41, 6, sizeof(Object_t *))) = (Object_t *)L_44;
		ObjectU5BU5D_t1_157* L_45 = L_41;
		NullCheck(L_45);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_45, 7);
		ArrayElementTypeCheck (L_45, _stringLiteral93);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_45, 7, sizeof(Object_t *))) = (Object_t *)_stringLiteral93;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_46 = String_Concat_m1_421(NULL /*static, unused*/, L_45, /*hidden argument*/NULL);
		Debug_Log_m6_489(NULL /*static, unused*/, L_46, /*hidden argument*/NULL);
		List_1_t1_971 * L_47 = V_3;
		PickupItem_t8_163 * L_48 = V_5;
		NullCheck(L_48);
		int32_t L_49 = PickupItem_get_ViewID_m8_983(L_48, /*hidden argument*/NULL);
		NullCheck(L_47);
		VirtActionInvoker1< float >::Invoke(19 /* System.Void System.Collections.Generic.List`1<System.Single>::Add(!0) */, L_47, (((float)((float)L_49))));
		List_1_t1_971 * L_50 = V_3;
		double L_51 = V_6;
		NullCheck(L_50);
		VirtActionInvoker1< float >::Invoke(19 /* System.Void System.Collections.Generic.List`1<System.Single>::Add(!0) */, L_50, (((float)((float)L_51))));
	}

IL_011e:
	{
		int32_t L_52 = V_4;
		V_4 = ((int32_t)((int32_t)L_52+(int32_t)1));
	}

IL_0124:
	{
		int32_t L_53 = V_4;
		PickupItemU5BU5D_t8_181* L_54 = V_2;
		NullCheck(L_54);
		if ((((int32_t)L_53) < ((int32_t)(((int32_t)((int32_t)(((Array_t *)L_54)->max_length)))))))
		{
			goto IL_0051;
		}
	}
	{
		ObjectU5BU5D_t1_157* L_55 = ((ObjectU5BU5D_t1_157*)SZArrayNew(ObjectU5BU5D_t1_157_il2cpp_TypeInfo_var, 4));
		NullCheck(L_55);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_55, 0);
		ArrayElementTypeCheck (L_55, _stringLiteral3563);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_55, 0, sizeof(Object_t *))) = (Object_t *)_stringLiteral3563;
		ObjectU5BU5D_t1_157* L_56 = L_55;
		List_1_t1_971 * L_57 = V_3;
		NullCheck(L_57);
		int32_t L_58 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(17 /* System.Int32 System.Collections.Generic.List`1<System.Single>::get_Count() */, L_57);
		int32_t L_59 = L_58;
		Object_t * L_60 = Box(Int32_t1_3_il2cpp_TypeInfo_var, &L_59);
		NullCheck(L_56);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_56, 1);
		ArrayElementTypeCheck (L_56, L_60);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_56, 1, sizeof(Object_t *))) = (Object_t *)L_60;
		ObjectU5BU5D_t1_157* L_61 = L_56;
		NullCheck(L_61);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_61, 2);
		ArrayElementTypeCheck (L_61, _stringLiteral2576);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_61, 2, sizeof(Object_t *))) = (Object_t *)_stringLiteral2576;
		ObjectU5BU5D_t1_157* L_62 = L_61;
		double L_63 = V_0;
		double L_64 = L_63;
		Object_t * L_65 = Box(Double_t1_18_il2cpp_TypeInfo_var, &L_64);
		NullCheck(L_62);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_62, 3);
		ArrayElementTypeCheck (L_62, L_65);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_62, 3, sizeof(Object_t *))) = (Object_t *)L_65;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_66 = String_Concat_m1_421(NULL /*static, unused*/, L_62, /*hidden argument*/NULL);
		Debug_Log_m6_489(NULL /*static, unused*/, L_66, /*hidden argument*/NULL);
		PhotonView_t8_3 * L_67 = MonoBehaviour_get_photonView_m8_495(__this, /*hidden argument*/NULL);
		PhotonPlayer_t8_102 * L_68 = ___targtePlayer;
		ObjectU5BU5D_t1_157* L_69 = ((ObjectU5BU5D_t1_157*)SZArrayNew(ObjectU5BU5D_t1_157_il2cpp_TypeInfo_var, 2));
		IL2CPP_RUNTIME_CLASS_INIT(PhotonNetwork_t8_114_il2cpp_TypeInfo_var);
		double L_70 = PhotonNetwork_get_time_m8_646(NULL /*static, unused*/, /*hidden argument*/NULL);
		double L_71 = L_70;
		Object_t * L_72 = Box(Double_t1_18_il2cpp_TypeInfo_var, &L_71);
		NullCheck(L_69);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_69, 0);
		ArrayElementTypeCheck (L_69, L_72);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_69, 0, sizeof(Object_t *))) = (Object_t *)L_72;
		ObjectU5BU5D_t1_157* L_73 = L_69;
		List_1_t1_971 * L_74 = V_3;
		NullCheck(L_74);
		SingleU5BU5D_t1_863* L_75 = List_1_ToArray_m1_5685(L_74, /*hidden argument*/List_1_ToArray_m1_5685_MethodInfo_var);
		NullCheck(L_73);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_73, 1);
		ArrayElementTypeCheck (L_73, L_75);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_73, 1, sizeof(Object_t *))) = (Object_t *)L_75;
		NullCheck(L_67);
		PhotonView_RPC_m8_788(L_67, _stringLiteral3564, L_68, L_73, /*hidden argument*/NULL);
		return;
	}
}
// System.Void PickupItemSyncer::PickupItemInit(System.Double,System.Single[])
extern TypeInfo* ObjectU5BU5D_t1_157_il2cpp_TypeInfo_var;
extern TypeInfo* Int32_t1_3_il2cpp_TypeInfo_var;
extern TypeInfo* Double_t1_18_il2cpp_TypeInfo_var;
extern TypeInfo* Single_t1_17_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* PhotonNetwork_t8_114_il2cpp_TypeInfo_var;
extern const MethodInfo* Component_GetComponent_TisPickupItem_t8_163_m6_1673_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral3560;
extern Il2CppCodeGenString* _stringLiteral3565;
extern Il2CppCodeGenString* _stringLiteral3545;
extern "C" void PickupItemSyncer_PickupItemInit_m8_1004 (PickupItemSyncer_t8_165 * __this, double ___timeBase, SingleU5BU5D_t1_863* ___inactivePickupsAndTimes, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ObjectU5BU5D_t1_157_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(19);
		Int32_t1_3_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(8);
		Double_t1_18_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(44);
		Single_t1_17_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(43);
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(11);
		PhotonNetwork_t8_114_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1058);
		Component_GetComponent_TisPickupItem_t8_163_m6_1673_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483866);
		_stringLiteral3560 = il2cpp_codegen_string_literal_from_index(3560);
		_stringLiteral3565 = il2cpp_codegen_string_literal_from_index(3565);
		_stringLiteral3545 = il2cpp_codegen_string_literal_from_index(3545);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	float V_3 = 0.0f;
	PhotonView_t8_3 * V_4 = {0};
	PickupItem_t8_163 * V_5 = {0};
	double V_6 = 0.0;
	double V_7 = 0.0;
	{
		__this->___IsWaitingForPickupInit_3 = 0;
		V_0 = 0;
		goto IL_00d7;
	}

IL_000e:
	{
		int32_t L_0 = V_0;
		V_1 = ((int32_t)((int32_t)L_0*(int32_t)2));
		SingleU5BU5D_t1_863* L_1 = ___inactivePickupsAndTimes;
		int32_t L_2 = V_1;
		NullCheck(L_1);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_1, L_2);
		int32_t L_3 = L_2;
		V_2 = (((int32_t)((int32_t)(*(float*)(float*)SZArrayLdElema(L_1, L_3, sizeof(float))))));
		SingleU5BU5D_t1_863* L_4 = ___inactivePickupsAndTimes;
		int32_t L_5 = V_1;
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, ((int32_t)((int32_t)L_5+(int32_t)1)));
		int32_t L_6 = ((int32_t)((int32_t)L_5+(int32_t)1));
		V_3 = (*(float*)(float*)SZArrayLdElema(L_4, L_6, sizeof(float)));
		int32_t L_7 = V_2;
		PhotonView_t8_3 * L_8 = PhotonView_Find_m8_792(NULL /*static, unused*/, L_7, /*hidden argument*/NULL);
		V_4 = L_8;
		PhotonView_t8_3 * L_9 = V_4;
		NullCheck(L_9);
		PickupItem_t8_163 * L_10 = Component_GetComponent_TisPickupItem_t8_163_m6_1673(L_9, /*hidden argument*/Component_GetComponent_TisPickupItem_t8_163_m6_1673_MethodInfo_var);
		V_5 = L_10;
		float L_11 = V_3;
		if ((!(((float)L_11) <= ((float)(0.0f)))))
		{
			goto IL_004a;
		}
	}
	{
		PickupItem_t8_163 * L_12 = V_5;
		NullCheck(L_12);
		PickupItem_PickedUp_m8_990(L_12, (0.0f), /*hidden argument*/NULL);
		goto IL_00d3;
	}

IL_004a:
	{
		float L_13 = V_3;
		double L_14 = ___timeBase;
		V_6 = ((double)((double)(((double)((double)L_13)))+(double)L_14));
		ObjectU5BU5D_t1_157* L_15 = ((ObjectU5BU5D_t1_157*)SZArrayNew(ObjectU5BU5D_t1_157_il2cpp_TypeInfo_var, 7));
		PhotonView_t8_3 * L_16 = V_4;
		NullCheck(L_16);
		int32_t L_17 = PhotonView_get_viewID_m8_767(L_16, /*hidden argument*/NULL);
		int32_t L_18 = L_17;
		Object_t * L_19 = Box(Int32_t1_3_il2cpp_TypeInfo_var, &L_18);
		NullCheck(L_15);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_15, 0);
		ArrayElementTypeCheck (L_15, L_19);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_15, 0, sizeof(Object_t *))) = (Object_t *)L_19;
		ObjectU5BU5D_t1_157* L_20 = L_15;
		NullCheck(L_20);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_20, 1);
		ArrayElementTypeCheck (L_20, _stringLiteral3560);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_20, 1, sizeof(Object_t *))) = (Object_t *)_stringLiteral3560;
		ObjectU5BU5D_t1_157* L_21 = L_20;
		double L_22 = V_6;
		double L_23 = L_22;
		Object_t * L_24 = Box(Double_t1_18_il2cpp_TypeInfo_var, &L_23);
		NullCheck(L_21);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_21, 2);
		ArrayElementTypeCheck (L_21, L_24);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_21, 2, sizeof(Object_t *))) = (Object_t *)L_24;
		ObjectU5BU5D_t1_157* L_25 = L_21;
		NullCheck(L_25);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_25, 3);
		ArrayElementTypeCheck (L_25, _stringLiteral3565);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_25, 3, sizeof(Object_t *))) = (Object_t *)_stringLiteral3565;
		ObjectU5BU5D_t1_157* L_26 = L_25;
		float L_27 = V_3;
		float L_28 = L_27;
		Object_t * L_29 = Box(Single_t1_17_il2cpp_TypeInfo_var, &L_28);
		NullCheck(L_26);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_26, 4);
		ArrayElementTypeCheck (L_26, L_29);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_26, 4, sizeof(Object_t *))) = (Object_t *)L_29;
		ObjectU5BU5D_t1_157* L_30 = L_26;
		NullCheck(L_30);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_30, 5);
		ArrayElementTypeCheck (L_30, _stringLiteral3545);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_30, 5, sizeof(Object_t *))) = (Object_t *)_stringLiteral3545;
		ObjectU5BU5D_t1_157* L_31 = L_30;
		PickupItem_t8_163 * L_32 = V_5;
		NullCheck(L_32);
		float L_33 = (L_32->___SecondsBeforeRespawn_2);
		float L_34 = L_33;
		Object_t * L_35 = Box(Single_t1_17_il2cpp_TypeInfo_var, &L_34);
		NullCheck(L_31);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_31, 6);
		ArrayElementTypeCheck (L_31, L_35);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_31, 6, sizeof(Object_t *))) = (Object_t *)L_35;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_36 = String_Concat_m1_421(NULL /*static, unused*/, L_31, /*hidden argument*/NULL);
		Debug_Log_m6_489(NULL /*static, unused*/, L_36, /*hidden argument*/NULL);
		double L_37 = V_6;
		IL2CPP_RUNTIME_CLASS_INIT(PhotonNetwork_t8_114_il2cpp_TypeInfo_var);
		double L_38 = PhotonNetwork_get_time_m8_646(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_7 = ((double)((double)L_37-(double)L_38));
		float L_39 = V_3;
		if ((!(((float)L_39) <= ((float)(0.0f)))))
		{
			goto IL_00c9;
		}
	}
	{
		V_7 = (0.0);
	}

IL_00c9:
	{
		PickupItem_t8_163 * L_40 = V_5;
		double L_41 = V_7;
		NullCheck(L_40);
		PickupItem_PickedUp_m8_990(L_40, (((float)((float)L_41))), /*hidden argument*/NULL);
	}

IL_00d3:
	{
		int32_t L_42 = V_0;
		V_0 = ((int32_t)((int32_t)L_42+(int32_t)1));
	}

IL_00d7:
	{
		int32_t L_43 = V_0;
		SingleU5BU5D_t1_863* L_44 = ___inactivePickupsAndTimes;
		NullCheck(L_44);
		if ((((int32_t)L_43) < ((int32_t)((int32_t)((int32_t)(((int32_t)((int32_t)(((Array_t *)L_44)->max_length))))/(int32_t)2)))))
		{
			goto IL_000e;
		}
	}
	{
		return;
	}
}
// System.Void PointedAtGameObjectInfo::.ctor()
extern "C" void PointedAtGameObjectInfo__ctor_m8_1005 (PointedAtGameObjectInfo_t8_166 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m6_525(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void PointedAtGameObjectInfo::OnGUI()
extern TypeInfo* Input_t6_83_il2cpp_TypeInfo_var;
extern TypeInfo* Int32_t1_3_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* GUI_t6_160_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3566;
extern Il2CppCodeGenString* _stringLiteral3567;
extern Il2CppCodeGenString* _stringLiteral3568;
extern Il2CppCodeGenString* _stringLiteral3569;
extern "C" void PointedAtGameObjectInfo_OnGUI_m8_1006 (PointedAtGameObjectInfo_t8_166 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Input_t6_83_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(956);
		Int32_t1_3_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(8);
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(11);
		GUI_t6_160_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(982);
		_stringLiteral3566 = il2cpp_codegen_string_literal_from_index(3566);
		_stringLiteral3567 = il2cpp_codegen_string_literal_from_index(3567);
		_stringLiteral3568 = il2cpp_codegen_string_literal_from_index(3568);
		_stringLiteral3569 = il2cpp_codegen_string_literal_from_index(3569);
		s_Il2CppMethodIntialized = true;
	}
	PhotonView_t8_3 * V_0 = {0};
	Vector3_t6_49  V_1 = {0};
	Vector3_t6_49  V_2 = {0};
	Object_t * G_B4_0 = {0};
	String_t* G_B4_1 = {0};
	Rect_t6_52  G_B4_2 = {0};
	Object_t * G_B3_0 = {0};
	String_t* G_B3_1 = {0};
	Rect_t6_52  G_B3_2 = {0};
	String_t* G_B5_0 = {0};
	Object_t * G_B5_1 = {0};
	String_t* G_B5_2 = {0};
	Rect_t6_52  G_B5_3 = {0};
	String_t* G_B7_0 = {0};
	Object_t * G_B7_1 = {0};
	String_t* G_B7_2 = {0};
	Rect_t6_52  G_B7_3 = {0};
	String_t* G_B6_0 = {0};
	Object_t * G_B6_1 = {0};
	String_t* G_B6_2 = {0};
	Rect_t6_52  G_B6_3 = {0};
	String_t* G_B8_0 = {0};
	String_t* G_B8_1 = {0};
	Object_t * G_B8_2 = {0};
	String_t* G_B8_3 = {0};
	Rect_t6_52  G_B8_4 = {0};
	{
		GameObject_t6_85 * L_0 = InputToEvent_get_goPointedAt_m8_944(NULL /*static, unused*/, /*hidden argument*/NULL);
		bool L_1 = Object_op_Inequality_m6_580(NULL /*static, unused*/, L_0, (Object_t6_5 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_00c1;
		}
	}
	{
		GameObject_t6_85 * L_2 = InputToEvent_get_goPointedAt_m8_944(NULL /*static, unused*/, /*hidden argument*/NULL);
		PhotonView_t8_3 * L_3 = Extensions_GetPhotonView_m8_292(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		V_0 = L_3;
		PhotonView_t8_3 * L_4 = V_0;
		bool L_5 = Object_op_Inequality_m6_580(NULL /*static, unused*/, L_4, (Object_t6_5 *)NULL, /*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_00c1;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t6_83_il2cpp_TypeInfo_var);
		Vector3_t6_49  L_6 = Input_get_mousePosition_m6_546(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_1 = L_6;
		float L_7 = ((&V_1)->___x_1);
		int32_t L_8 = Screen_get_height_m6_122(NULL /*static, unused*/, /*hidden argument*/NULL);
		Vector3_t6_49  L_9 = Input_get_mousePosition_m6_546(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_2 = L_9;
		float L_10 = ((&V_2)->___y_2);
		Rect_t6_52  L_11 = {0};
		Rect__ctor_m6_270(&L_11, ((float)((float)L_7+(float)(5.0f))), ((float)((float)((float)((float)(((float)((float)L_8)))-(float)L_10))-(float)(15.0f))), (300.0f), (30.0f), /*hidden argument*/NULL);
		PhotonView_t8_3 * L_12 = V_0;
		NullCheck(L_12);
		int32_t L_13 = PhotonView_get_viewID_m8_767(L_12, /*hidden argument*/NULL);
		int32_t L_14 = L_13;
		Object_t * L_15 = Box(Int32_t1_3_il2cpp_TypeInfo_var, &L_14);
		PhotonView_t8_3 * L_16 = V_0;
		NullCheck(L_16);
		bool L_17 = PhotonView_get_isSceneView_m8_769(L_16, /*hidden argument*/NULL);
		G_B3_0 = L_15;
		G_B3_1 = _stringLiteral3566;
		G_B3_2 = L_11;
		if (!L_17)
		{
			G_B4_0 = L_15;
			G_B4_1 = _stringLiteral3566;
			G_B4_2 = L_11;
			goto IL_0088;
		}
	}
	{
		G_B5_0 = _stringLiteral3567;
		G_B5_1 = G_B3_0;
		G_B5_2 = G_B3_1;
		G_B5_3 = G_B3_2;
		goto IL_008d;
	}

IL_0088:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_18 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->___Empty_2;
		G_B5_0 = L_18;
		G_B5_1 = G_B4_0;
		G_B5_2 = G_B4_1;
		G_B5_3 = G_B4_2;
	}

IL_008d:
	{
		PhotonView_t8_3 * L_19 = V_0;
		NullCheck(L_19);
		bool L_20 = PhotonView_get_isMine_m8_774(L_19, /*hidden argument*/NULL);
		G_B6_0 = G_B5_0;
		G_B6_1 = G_B5_1;
		G_B6_2 = G_B5_2;
		G_B6_3 = G_B5_3;
		if (!L_20)
		{
			G_B7_0 = G_B5_0;
			G_B7_1 = G_B5_1;
			G_B7_2 = G_B5_2;
			G_B7_3 = G_B5_3;
			goto IL_00a2;
		}
	}
	{
		G_B8_0 = _stringLiteral3568;
		G_B8_1 = G_B6_0;
		G_B8_2 = G_B6_1;
		G_B8_3 = G_B6_2;
		G_B8_4 = G_B6_3;
		goto IL_00b7;
	}

IL_00a2:
	{
		PhotonView_t8_3 * L_21 = V_0;
		NullCheck(L_21);
		int32_t L_22 = (L_21->___ownerId_2);
		int32_t L_23 = L_22;
		Object_t * L_24 = Box(Int32_t1_3_il2cpp_TypeInfo_var, &L_23);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_25 = String_Concat_m1_416(NULL /*static, unused*/, _stringLiteral3569, L_24, /*hidden argument*/NULL);
		G_B8_0 = L_25;
		G_B8_1 = G_B7_0;
		G_B8_2 = G_B7_1;
		G_B8_3 = G_B7_2;
		G_B8_4 = G_B7_3;
	}

IL_00b7:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_26 = String_Format_m1_412(NULL /*static, unused*/, G_B8_3, G_B8_2, G_B8_1, G_B8_0, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t6_160_il2cpp_TypeInfo_var);
		GUI_Label_m6_1044(NULL /*static, unused*/, G_B8_4, L_26, /*hidden argument*/NULL);
	}

IL_00c1:
	{
		return;
	}
}
// System.Void PunPlayerScores::.ctor()
extern "C" void PunPlayerScores__ctor_m8_1007 (PunPlayerScores_t8_167 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m6_525(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void ScoreExtensions::SetScore(PhotonPlayer,System.Int32)
extern TypeInfo* Hashtable_t5_1_il2cpp_TypeInfo_var;
extern TypeInfo* Int32_t1_3_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3570;
extern "C" void ScoreExtensions_SetScore_m8_1008 (Object_t * __this /* static, unused */, PhotonPlayer_t8_102 * ___player, int32_t ___newScore, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Hashtable_t5_1_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(891);
		Int32_t1_3_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(8);
		_stringLiteral3570 = il2cpp_codegen_string_literal_from_index(3570);
		s_Il2CppMethodIntialized = true;
	}
	Hashtable_t5_1 * V_0 = {0};
	{
		Hashtable_t5_1 * L_0 = (Hashtable_t5_1 *)il2cpp_codegen_object_new (Hashtable_t5_1_il2cpp_TypeInfo_var);
		Hashtable__ctor_m5_0(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		Hashtable_t5_1 * L_1 = V_0;
		int32_t L_2 = ___newScore;
		int32_t L_3 = L_2;
		Object_t * L_4 = Box(Int32_t1_3_il2cpp_TypeInfo_var, &L_3);
		NullCheck(L_1);
		Hashtable_set_Item_m5_3(L_1, _stringLiteral3570, L_4, /*hidden argument*/NULL);
		PhotonPlayer_t8_102 * L_5 = ___player;
		Hashtable_t5_1 * L_6 = V_0;
		NullCheck(L_5);
		PhotonPlayer_SetCustomProperties_m8_741(L_5, L_6, (Hashtable_t5_1 *)NULL, 0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void ScoreExtensions::AddScore(PhotonPlayer,System.Int32)
extern TypeInfo* Hashtable_t5_1_il2cpp_TypeInfo_var;
extern TypeInfo* Int32_t1_3_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3570;
extern "C" void ScoreExtensions_AddScore_m8_1009 (Object_t * __this /* static, unused */, PhotonPlayer_t8_102 * ___player, int32_t ___scoreToAddToCurrent, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Hashtable_t5_1_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(891);
		Int32_t1_3_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(8);
		_stringLiteral3570 = il2cpp_codegen_string_literal_from_index(3570);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	Hashtable_t5_1 * V_1 = {0};
	{
		PhotonPlayer_t8_102 * L_0 = ___player;
		int32_t L_1 = ScoreExtensions_GetScore_m8_1010(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		int32_t L_2 = V_0;
		int32_t L_3 = ___scoreToAddToCurrent;
		V_0 = ((int32_t)((int32_t)L_2+(int32_t)L_3));
		Hashtable_t5_1 * L_4 = (Hashtable_t5_1 *)il2cpp_codegen_object_new (Hashtable_t5_1_il2cpp_TypeInfo_var);
		Hashtable__ctor_m5_0(L_4, /*hidden argument*/NULL);
		V_1 = L_4;
		Hashtable_t5_1 * L_5 = V_1;
		int32_t L_6 = V_0;
		int32_t L_7 = L_6;
		Object_t * L_8 = Box(Int32_t1_3_il2cpp_TypeInfo_var, &L_7);
		NullCheck(L_5);
		Hashtable_set_Item_m5_3(L_5, _stringLiteral3570, L_8, /*hidden argument*/NULL);
		PhotonPlayer_t8_102 * L_9 = ___player;
		Hashtable_t5_1 * L_10 = V_1;
		NullCheck(L_9);
		PhotonPlayer_SetCustomProperties_m8_741(L_9, L_10, (Hashtable_t5_1 *)NULL, 0, /*hidden argument*/NULL);
		return;
	}
}
// System.Int32 ScoreExtensions::GetScore(PhotonPlayer)
extern TypeInfo* Int32_t1_3_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3570;
extern "C" int32_t ScoreExtensions_GetScore_m8_1010 (Object_t * __this /* static, unused */, PhotonPlayer_t8_102 * ___player, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Int32_t1_3_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(8);
		_stringLiteral3570 = il2cpp_codegen_string_literal_from_index(3570);
		s_Il2CppMethodIntialized = true;
	}
	Object_t * V_0 = {0};
	{
		PhotonPlayer_t8_102 * L_0 = ___player;
		NullCheck(L_0);
		Hashtable_t5_1 * L_1 = PhotonPlayer_get_customProperties_m8_734(L_0, /*hidden argument*/NULL);
		NullCheck(L_1);
		bool L_2 = (bool)VirtFuncInvoker2< bool, Object_t *, Object_t ** >::Invoke(32 /* System.Boolean System.Collections.Generic.Dictionary`2<System.Object,System.Object>::TryGetValue(!0,!1&) */, L_1, _stringLiteral3570, (&V_0));
		if (!L_2)
		{
			goto IL_001e;
		}
	}
	{
		Object_t * L_3 = V_0;
		return ((*(int32_t*)((int32_t*)UnBox (L_3, Int32_t1_3_il2cpp_TypeInfo_var))));
	}

IL_001e:
	{
		return 0;
	}
}
// System.Void PunTeams::.ctor()
extern "C" void PunTeams__ctor_m8_1011 (PunTeams_t8_170 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m6_525(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void PunTeams::Start()
extern const Il2CppType* Team_t8_169_0_0_0_var;
extern TypeInfo* Dictionary_2_t1_951_il2cpp_TypeInfo_var;
extern TypeInfo* PunTeams_t8_170_il2cpp_TypeInfo_var;
extern TypeInfo* Type_t_il2cpp_TypeInfo_var;
extern TypeInfo* Enum_t1_24_il2cpp_TypeInfo_var;
extern TypeInfo* IEnumerator_t1_130_il2cpp_TypeInfo_var;
extern TypeInfo* Byte_t1_11_il2cpp_TypeInfo_var;
extern TypeInfo* List_1_t1_955_il2cpp_TypeInfo_var;
extern TypeInfo* IDisposable_t1_835_il2cpp_TypeInfo_var;
extern const MethodInfo* Dictionary_2__ctor_m1_5686_MethodInfo_var;
extern const MethodInfo* List_1__ctor_m1_5649_MethodInfo_var;
extern "C" void PunTeams_Start_m8_1012 (PunTeams_t8_170 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Team_t8_169_0_0_0_var = il2cpp_codegen_type_from_index(1080);
		Dictionary_2_t1_951_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1195);
		PunTeams_t8_170_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1084);
		Type_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3);
		Enum_t1_24_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(61);
		IEnumerator_t1_130_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(159);
		Byte_t1_11_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(22);
		List_1_t1_955_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1081);
		IDisposable_t1_835_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(142);
		Dictionary_2__ctor_m1_5686_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483966);
		List_1__ctor_m1_5649_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483905);
		s_Il2CppMethodIntialized = true;
	}
	Array_t * V_0 = {0};
	Object_t * V_1 = {0};
	Object_t * V_2 = {0};
	Object_t * V_3 = {0};
	Exception_t1_33 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1_33 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		Dictionary_2_t1_951 * L_0 = (Dictionary_2_t1_951 *)il2cpp_codegen_object_new (Dictionary_2_t1_951_il2cpp_TypeInfo_var);
		Dictionary_2__ctor_m1_5686(L_0, /*hidden argument*/Dictionary_2__ctor_m1_5686_MethodInfo_var);
		((PunTeams_t8_170_StaticFields*)PunTeams_t8_170_il2cpp_TypeInfo_var->static_fields)->___PlayersPerTeam_3 = L_0;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_1 = Type_GetTypeFromHandle_m1_893(NULL /*static, unused*/, LoadTypeToken(Team_t8_169_0_0_0_var), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Enum_t1_24_il2cpp_TypeInfo_var);
		Array_t * L_2 = Enum_GetValues_m1_720(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		Array_t * L_3 = V_0;
		NullCheck(L_3);
		Object_t * L_4 = (Object_t *)VirtFuncInvoker0< Object_t * >::Invoke(4 /* System.Collections.IEnumerator System.Array::GetEnumerator() */, L_3);
		V_2 = L_4;
	}

IL_0021:
	try
	{ // begin try (depth: 1)
		{
			goto IL_0042;
		}

IL_0026:
		{
			Object_t * L_5 = V_2;
			NullCheck(L_5);
			Object_t * L_6 = (Object_t *)InterfaceFuncInvoker0< Object_t * >::Invoke(0 /* System.Object System.Collections.IEnumerator::get_Current() */, IEnumerator_t1_130_il2cpp_TypeInfo_var, L_5);
			V_1 = L_6;
			Dictionary_2_t1_951 * L_7 = ((PunTeams_t8_170_StaticFields*)PunTeams_t8_170_il2cpp_TypeInfo_var->static_fields)->___PlayersPerTeam_3;
			Object_t * L_8 = V_1;
			List_1_t1_955 * L_9 = (List_1_t1_955 *)il2cpp_codegen_object_new (List_1_t1_955_il2cpp_TypeInfo_var);
			List_1__ctor_m1_5649(L_9, /*hidden argument*/List_1__ctor_m1_5649_MethodInfo_var);
			NullCheck(L_7);
			VirtActionInvoker2< uint8_t, List_1_t1_955 * >::Invoke(26 /* System.Void System.Collections.Generic.Dictionary`2<PunTeams/Team,System.Collections.Generic.List`1<PhotonPlayer>>::set_Item(!0,!1) */, L_7, ((*(uint8_t*)((uint8_t*)UnBox (L_8, Byte_t1_11_il2cpp_TypeInfo_var)))), L_9);
		}

IL_0042:
		{
			Object_t * L_10 = V_2;
			NullCheck(L_10);
			bool L_11 = (bool)InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t1_130_il2cpp_TypeInfo_var, L_10);
			if (L_11)
			{
				goto IL_0026;
			}
		}

IL_004d:
		{
			IL2CPP_LEAVE(0x64, FINALLY_0052);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1_33 *)e.ex;
		goto FINALLY_0052;
	}

FINALLY_0052:
	{ // begin finally (depth: 1)
		{
			Object_t * L_12 = V_2;
			V_3 = ((Object_t *)IsInst(L_12, IDisposable_t1_835_il2cpp_TypeInfo_var));
			Object_t * L_13 = V_3;
			if (L_13)
			{
				goto IL_005d;
			}
		}

IL_005c:
		{
			IL2CPP_END_FINALLY(82)
		}

IL_005d:
		{
			Object_t * L_14 = V_3;
			NullCheck(L_14);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1_835_il2cpp_TypeInfo_var, L_14);
			IL2CPP_END_FINALLY(82)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(82)
	{
		IL2CPP_JUMP_TBL(0x64, IL_0064)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1_33 *)
	}

IL_0064:
	{
		return;
	}
}
// System.Void PunTeams::OnJoinedRoom()
extern "C" void PunTeams_OnJoinedRoom_m8_1013 (PunTeams_t8_170 * __this, const MethodInfo* method)
{
	{
		PunTeams_UpdateTeams_m8_1015(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void PunTeams::OnPhotonPlayerPropertiesChanged(System.Object[])
extern "C" void PunTeams_OnPhotonPlayerPropertiesChanged_m8_1014 (PunTeams_t8_170 * __this, ObjectU5BU5D_t1_157* ___playerAndUpdatedProps, const MethodInfo* method)
{
	{
		PunTeams_UpdateTeams_m8_1015(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void PunTeams::UpdateTeams()
extern const Il2CppType* Team_t8_169_0_0_0_var;
extern TypeInfo* Type_t_il2cpp_TypeInfo_var;
extern TypeInfo* Enum_t1_24_il2cpp_TypeInfo_var;
extern TypeInfo* IEnumerator_t1_130_il2cpp_TypeInfo_var;
extern TypeInfo* PunTeams_t8_170_il2cpp_TypeInfo_var;
extern TypeInfo* Byte_t1_11_il2cpp_TypeInfo_var;
extern TypeInfo* IDisposable_t1_835_il2cpp_TypeInfo_var;
extern TypeInfo* PhotonNetwork_t8_114_il2cpp_TypeInfo_var;
extern "C" void PunTeams_UpdateTeams_m8_1015 (PunTeams_t8_170 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Team_t8_169_0_0_0_var = il2cpp_codegen_type_from_index(1080);
		Type_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3);
		Enum_t1_24_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(61);
		IEnumerator_t1_130_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(159);
		PunTeams_t8_170_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1084);
		Byte_t1_11_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(22);
		IDisposable_t1_835_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(142);
		PhotonNetwork_t8_114_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1058);
		s_Il2CppMethodIntialized = true;
	}
	Array_t * V_0 = {0};
	Object_t * V_1 = {0};
	Object_t * V_2 = {0};
	int32_t V_3 = 0;
	PhotonPlayer_t8_102 * V_4 = {0};
	uint8_t V_5 = {0};
	Object_t * V_6 = {0};
	Exception_t1_33 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1_33 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m1_893(NULL /*static, unused*/, LoadTypeToken(Team_t8_169_0_0_0_var), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Enum_t1_24_il2cpp_TypeInfo_var);
		Array_t * L_1 = Enum_GetValues_m1_720(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		Array_t * L_2 = V_0;
		NullCheck(L_2);
		Object_t * L_3 = (Object_t *)VirtFuncInvoker0< Object_t * >::Invoke(4 /* System.Collections.IEnumerator System.Array::GetEnumerator() */, L_2);
		V_2 = L_3;
	}

IL_0017:
	try
	{ // begin try (depth: 1)
		{
			goto IL_0038;
		}

IL_001c:
		{
			Object_t * L_4 = V_2;
			NullCheck(L_4);
			Object_t * L_5 = (Object_t *)InterfaceFuncInvoker0< Object_t * >::Invoke(0 /* System.Object System.Collections.IEnumerator::get_Current() */, IEnumerator_t1_130_il2cpp_TypeInfo_var, L_4);
			V_1 = L_5;
			Dictionary_2_t1_951 * L_6 = ((PunTeams_t8_170_StaticFields*)PunTeams_t8_170_il2cpp_TypeInfo_var->static_fields)->___PlayersPerTeam_3;
			Object_t * L_7 = V_1;
			NullCheck(L_6);
			List_1_t1_955 * L_8 = (List_1_t1_955 *)VirtFuncInvoker1< List_1_t1_955 *, uint8_t >::Invoke(25 /* !1 System.Collections.Generic.Dictionary`2<PunTeams/Team,System.Collections.Generic.List`1<PhotonPlayer>>::get_Item(!0) */, L_6, ((*(uint8_t*)((uint8_t*)UnBox (L_7, Byte_t1_11_il2cpp_TypeInfo_var)))));
			NullCheck(L_8);
			VirtActionInvoker0::Invoke(20 /* System.Void System.Collections.Generic.List`1<PhotonPlayer>::Clear() */, L_8);
		}

IL_0038:
		{
			Object_t * L_9 = V_2;
			NullCheck(L_9);
			bool L_10 = (bool)InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t1_130_il2cpp_TypeInfo_var, L_9);
			if (L_10)
			{
				goto IL_001c;
			}
		}

IL_0043:
		{
			IL2CPP_LEAVE(0x5D, FINALLY_0048);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1_33 *)e.ex;
		goto FINALLY_0048;
	}

FINALLY_0048:
	{ // begin finally (depth: 1)
		{
			Object_t * L_11 = V_2;
			V_6 = ((Object_t *)IsInst(L_11, IDisposable_t1_835_il2cpp_TypeInfo_var));
			Object_t * L_12 = V_6;
			if (L_12)
			{
				goto IL_0055;
			}
		}

IL_0054:
		{
			IL2CPP_END_FINALLY(72)
		}

IL_0055:
		{
			Object_t * L_13 = V_6;
			NullCheck(L_13);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1_835_il2cpp_TypeInfo_var, L_13);
			IL2CPP_END_FINALLY(72)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(72)
	{
		IL2CPP_JUMP_TBL(0x5D, IL_005d)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1_33 *)
	}

IL_005d:
	{
		V_3 = 0;
		goto IL_008d;
	}

IL_0064:
	{
		IL2CPP_RUNTIME_CLASS_INIT(PhotonNetwork_t8_114_il2cpp_TypeInfo_var);
		PhotonPlayerU5BU5D_t8_101* L_14 = PhotonNetwork_get_playerList_m8_616(NULL /*static, unused*/, /*hidden argument*/NULL);
		int32_t L_15 = V_3;
		NullCheck(L_14);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_14, L_15);
		int32_t L_16 = L_15;
		V_4 = (*(PhotonPlayer_t8_102 **)(PhotonPlayer_t8_102 **)SZArrayLdElema(L_14, L_16, sizeof(PhotonPlayer_t8_102 *)));
		PhotonPlayer_t8_102 * L_17 = V_4;
		uint8_t L_18 = TeamExtensions_GetTeam_m8_1016(NULL /*static, unused*/, L_17, /*hidden argument*/NULL);
		V_5 = L_18;
		Dictionary_2_t1_951 * L_19 = ((PunTeams_t8_170_StaticFields*)PunTeams_t8_170_il2cpp_TypeInfo_var->static_fields)->___PlayersPerTeam_3;
		uint8_t L_20 = V_5;
		NullCheck(L_19);
		List_1_t1_955 * L_21 = (List_1_t1_955 *)VirtFuncInvoker1< List_1_t1_955 *, uint8_t >::Invoke(25 /* !1 System.Collections.Generic.Dictionary`2<PunTeams/Team,System.Collections.Generic.List`1<PhotonPlayer>>::get_Item(!0) */, L_19, L_20);
		PhotonPlayer_t8_102 * L_22 = V_4;
		NullCheck(L_21);
		VirtActionInvoker1< PhotonPlayer_t8_102 * >::Invoke(19 /* System.Void System.Collections.Generic.List`1<PhotonPlayer>::Add(!0) */, L_21, L_22);
		int32_t L_23 = V_3;
		V_3 = ((int32_t)((int32_t)L_23+(int32_t)1));
	}

IL_008d:
	{
		int32_t L_24 = V_3;
		IL2CPP_RUNTIME_CLASS_INIT(PhotonNetwork_t8_114_il2cpp_TypeInfo_var);
		PhotonPlayerU5BU5D_t8_101* L_25 = PhotonNetwork_get_playerList_m8_616(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_25);
		if ((((int32_t)L_24) < ((int32_t)(((int32_t)((int32_t)(((Array_t *)L_25)->max_length)))))))
		{
			goto IL_0064;
		}
	}
	{
		return;
	}
}
// PunTeams/Team TeamExtensions::GetTeam(PhotonPlayer)
extern TypeInfo* Byte_t1_11_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3571;
extern "C" uint8_t TeamExtensions_GetTeam_m8_1016 (Object_t * __this /* static, unused */, PhotonPlayer_t8_102 * ___player, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Byte_t1_11_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(22);
		_stringLiteral3571 = il2cpp_codegen_string_literal_from_index(3571);
		s_Il2CppMethodIntialized = true;
	}
	Object_t * V_0 = {0};
	{
		PhotonPlayer_t8_102 * L_0 = ___player;
		NullCheck(L_0);
		Hashtable_t5_1 * L_1 = PhotonPlayer_get_customProperties_m8_734(L_0, /*hidden argument*/NULL);
		NullCheck(L_1);
		bool L_2 = (bool)VirtFuncInvoker2< bool, Object_t *, Object_t ** >::Invoke(32 /* System.Boolean System.Collections.Generic.Dictionary`2<System.Object,System.Object>::TryGetValue(!0,!1&) */, L_1, _stringLiteral3571, (&V_0));
		if (!L_2)
		{
			goto IL_001e;
		}
	}
	{
		Object_t * L_3 = V_0;
		return (uint8_t)(((*(uint8_t*)((uint8_t*)UnBox (L_3, Byte_t1_11_il2cpp_TypeInfo_var)))));
	}

IL_001e:
	{
		return (uint8_t)(0);
	}
}
// System.Void TeamExtensions::SetTeam(PhotonPlayer,PunTeams/Team)
extern TypeInfo* PhotonNetwork_t8_114_il2cpp_TypeInfo_var;
extern TypeInfo* PeerState_t8_69_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* Hashtable_t5_1_il2cpp_TypeInfo_var;
extern TypeInfo* Byte_t1_11_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3572;
extern Il2CppCodeGenString* _stringLiteral3573;
extern Il2CppCodeGenString* _stringLiteral3571;
extern "C" void TeamExtensions_SetTeam_m8_1017 (Object_t * __this /* static, unused */, PhotonPlayer_t8_102 * ___player, uint8_t ___team, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		PhotonNetwork_t8_114_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1058);
		PeerState_t8_69_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1061);
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(11);
		Hashtable_t5_1_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(891);
		Byte_t1_11_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(22);
		_stringLiteral3572 = il2cpp_codegen_string_literal_from_index(3572);
		_stringLiteral3573 = il2cpp_codegen_string_literal_from_index(3573);
		_stringLiteral3571 = il2cpp_codegen_string_literal_from_index(3571);
		s_Il2CppMethodIntialized = true;
	}
	uint8_t V_0 = {0};
	Hashtable_t5_1 * V_1 = {0};
	{
		IL2CPP_RUNTIME_CLASS_INIT(PhotonNetwork_t8_114_il2cpp_TypeInfo_var);
		bool L_0 = PhotonNetwork_get_connectedAndReady_m8_605(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (L_0)
		{
			goto IL_0028;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(PhotonNetwork_t8_114_il2cpp_TypeInfo_var);
		int32_t L_1 = PhotonNetwork_get_connectionStateDetailed_m8_607(NULL /*static, unused*/, /*hidden argument*/NULL);
		int32_t L_2 = L_1;
		Object_t * L_3 = Box(PeerState_t8_69_il2cpp_TypeInfo_var, &L_2);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_4 = String_Concat_m1_417(NULL /*static, unused*/, _stringLiteral3572, L_3, _stringLiteral3573, /*hidden argument*/NULL);
		Debug_LogWarning_m6_491(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
	}

IL_0028:
	{
		IL2CPP_RUNTIME_CLASS_INIT(PhotonNetwork_t8_114_il2cpp_TypeInfo_var);
		PhotonPlayer_t8_102 * L_5 = PhotonNetwork_get_player_m8_612(NULL /*static, unused*/, /*hidden argument*/NULL);
		uint8_t L_6 = TeamExtensions_GetTeam_m8_1016(NULL /*static, unused*/, L_5, /*hidden argument*/NULL);
		V_0 = L_6;
		uint8_t L_7 = V_0;
		uint8_t L_8 = ___team;
		if ((((int32_t)L_7) == ((int32_t)L_8)))
		{
			goto IL_005e;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(PhotonNetwork_t8_114_il2cpp_TypeInfo_var);
		PhotonPlayer_t8_102 * L_9 = PhotonNetwork_get_player_m8_612(NULL /*static, unused*/, /*hidden argument*/NULL);
		Hashtable_t5_1 * L_10 = (Hashtable_t5_1 *)il2cpp_codegen_object_new (Hashtable_t5_1_il2cpp_TypeInfo_var);
		Hashtable__ctor_m5_0(L_10, /*hidden argument*/NULL);
		V_1 = L_10;
		Hashtable_t5_1 * L_11 = V_1;
		uint8_t L_12 = ___team;
		uint8_t L_13 = L_12;
		Object_t * L_14 = Box(Byte_t1_11_il2cpp_TypeInfo_var, &L_13);
		NullCheck(L_11);
		VirtActionInvoker2< Object_t *, Object_t * >::Invoke(27 /* System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Object>::Add(!0,!1) */, L_11, _stringLiteral3571, L_14);
		Hashtable_t5_1 * L_15 = V_1;
		NullCheck(L_9);
		PhotonPlayer_SetCustomProperties_m8_741(L_9, L_15, (Hashtable_t5_1 *)NULL, 0, /*hidden argument*/NULL);
	}

IL_005e:
	{
		return;
	}
}
// System.Void QuitOnEscapeOrBack::.ctor()
extern "C" void QuitOnEscapeOrBack__ctor_m8_1018 (QuitOnEscapeOrBack_t8_172 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m6_525(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void QuitOnEscapeOrBack::Update()
extern TypeInfo* Input_t6_83_il2cpp_TypeInfo_var;
extern "C" void QuitOnEscapeOrBack_Update_m8_1019 (QuitOnEscapeOrBack_t8_172 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Input_t6_83_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(956);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t6_83_il2cpp_TypeInfo_var);
		bool L_0 = Input_GetKeyDown_m6_542(NULL /*static, unused*/, ((int32_t)27), /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_0011;
		}
	}
	{
		Application_Quit_m6_445(NULL /*static, unused*/, /*hidden argument*/NULL);
	}

IL_0011:
	{
		return;
	}
}
// System.Void ServerTime::.ctor()
extern "C" void ServerTime__ctor_m8_1020 (ServerTime_t8_173 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m6_525(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void ServerTime::OnGUI()
extern TypeInfo* PhotonNetwork_t8_114_il2cpp_TypeInfo_var;
extern TypeInfo* Int32_t1_3_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* GUILayoutOptionU5BU5D_t6_165_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3574;
extern Il2CppCodeGenString* _stringLiteral3575;
extern "C" void ServerTime_OnGUI_m8_1021 (ServerTime_t8_173 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		PhotonNetwork_t8_114_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1058);
		Int32_t1_3_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(8);
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(11);
		GUILayoutOptionU5BU5D_t6_165_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(991);
		_stringLiteral3574 = il2cpp_codegen_string_literal_from_index(3574);
		_stringLiteral3575 = il2cpp_codegen_string_literal_from_index(3575);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = Screen_get_width_m6_121(NULL /*static, unused*/, /*hidden argument*/NULL);
		Rect_t6_52  L_1 = {0};
		Rect__ctor_m6_270(&L_1, (((float)((float)((int32_t)((int32_t)((int32_t)((int32_t)L_0/(int32_t)2))-(int32_t)((int32_t)100)))))), (0.0f), (200.0f), (30.0f), /*hidden argument*/NULL);
		GUILayout_BeginArea_m6_1133(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(PhotonNetwork_t8_114_il2cpp_TypeInfo_var);
		int32_t L_2 = PhotonNetwork_get_ServerTimestamp_m8_647(NULL /*static, unused*/, /*hidden argument*/NULL);
		int32_t L_3 = Environment_get_TickCount_m1_5041(NULL /*static, unused*/, /*hidden argument*/NULL);
		int32_t L_4 = ((int32_t)((int32_t)L_2-(int32_t)L_3));
		Object_t * L_5 = Box(Int32_t1_3_il2cpp_TypeInfo_var, &L_4);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_6 = String_Format_m1_410(NULL /*static, unused*/, _stringLiteral3574, L_5, /*hidden argument*/NULL);
		GUILayout_Label_m6_1112(NULL /*static, unused*/, L_6, ((GUILayoutOptionU5BU5D_t6_165*)SZArrayNew(GUILayoutOptionU5BU5D_t6_165_il2cpp_TypeInfo_var, 0)), /*hidden argument*/NULL);
		bool L_7 = GUILayout_Button_m6_1115(NULL /*static, unused*/, _stringLiteral3575, ((GUILayoutOptionU5BU5D_t6_165*)SZArrayNew(GUILayoutOptionU5BU5D_t6_165_il2cpp_TypeInfo_var, 0)), /*hidden argument*/NULL);
		if (!L_7)
		{
			goto IL_0063;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(PhotonNetwork_t8_114_il2cpp_TypeInfo_var);
		PhotonNetwork_FetchServerTimestamp_m8_704(NULL /*static, unused*/, /*hidden argument*/NULL);
	}

IL_0063:
	{
		GUILayout_EndArea_m6_1136(NULL /*static, unused*/, /*hidden argument*/NULL);
		return;
	}
}
// System.Void ShowInfoOfPlayer::.ctor()
extern "C" void ShowInfoOfPlayer__ctor_m8_1022 (ShowInfoOfPlayer_t8_174 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m8_494(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void ShowInfoOfPlayer::Start()
extern const Il2CppType* Font_t6_148_0_0_0_var;
extern TypeInfo* Type_t_il2cpp_TypeInfo_var;
extern TypeInfo* Font_t6_148_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* GameObject_t6_85_il2cpp_TypeInfo_var;
extern const MethodInfo* GameObject_AddComponent_TisMeshRenderer_t6_28_m6_1692_MethodInfo_var;
extern const MethodInfo* GameObject_AddComponent_TisTextMesh_t6_145_m6_1693_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral3576;
extern Il2CppCodeGenString* _stringLiteral3577;
extern "C" void ShowInfoOfPlayer_Start_m8_1023 (ShowInfoOfPlayer_t8_174 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Font_t6_148_0_0_0_var = il2cpp_codegen_type_from_index(969);
		Type_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3);
		Font_t6_148_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(969);
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(11);
		GameObject_t6_85_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1082);
		GameObject_AddComponent_TisMeshRenderer_t6_28_m6_1692_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483967);
		GameObject_AddComponent_TisTextMesh_t6_145_m6_1693_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483968);
		_stringLiteral3576 = il2cpp_codegen_string_literal_from_index(3576);
		_stringLiteral3577 = il2cpp_codegen_string_literal_from_index(3577);
		s_Il2CppMethodIntialized = true;
	}
	MeshRenderer_t6_28 * V_0 = {0};
	{
		Font_t6_148 * L_0 = (__this->___font_5);
		bool L_1 = Object_op_Equality_m6_579(NULL /*static, unused*/, L_0, (Object_t6_5 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0042;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_2 = Type_GetTypeFromHandle_m1_893(NULL /*static, unused*/, LoadTypeToken(Font_t6_148_0_0_0_var), /*hidden argument*/NULL);
		ObjectU5BU5D_t6_252* L_3 = Resources_FindObjectsOfTypeAll_m6_408(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		NullCheck(L_3);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_3, 0);
		int32_t L_4 = 0;
		__this->___font_5 = ((Font_t6_148 *)CastclassSealed((*(Object_t6_5 **)(Object_t6_5 **)SZArrayLdElema(L_3, L_4, sizeof(Object_t6_5 *))), Font_t6_148_il2cpp_TypeInfo_var));
		Font_t6_148 * L_5 = (__this->___font_5);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_6 = String_Concat_m1_416(NULL /*static, unused*/, _stringLiteral3576, L_5, /*hidden argument*/NULL);
		Debug_LogWarning_m6_491(NULL /*static, unused*/, L_6, /*hidden argument*/NULL);
	}

IL_0042:
	{
		TextMesh_t6_145 * L_7 = (__this->___tm_3);
		bool L_8 = Object_op_Equality_m6_579(NULL /*static, unused*/, L_7, (Object_t6_5 *)NULL, /*hidden argument*/NULL);
		if (!L_8)
		{
			goto IL_00ff;
		}
	}
	{
		GameObject_t6_85 * L_9 = (GameObject_t6_85 *)il2cpp_codegen_object_new (GameObject_t6_85_il2cpp_TypeInfo_var);
		GameObject__ctor_m6_589(L_9, _stringLiteral3577, /*hidden argument*/NULL);
		__this->___textGo_2 = L_9;
		GameObject_t6_85 * L_10 = (__this->___textGo_2);
		NullCheck(L_10);
		Transform_t6_61 * L_11 = GameObject_get_transform_m6_595(L_10, /*hidden argument*/NULL);
		GameObject_t6_85 * L_12 = Component_get_gameObject_m6_583(__this, /*hidden argument*/NULL);
		NullCheck(L_12);
		Transform_t6_61 * L_13 = GameObject_get_transform_m6_595(L_12, /*hidden argument*/NULL);
		NullCheck(L_11);
		Transform_set_parent_m6_636(L_11, L_13, /*hidden argument*/NULL);
		GameObject_t6_85 * L_14 = (__this->___textGo_2);
		NullCheck(L_14);
		Transform_t6_61 * L_15 = GameObject_get_transform_m6_595(L_14, /*hidden argument*/NULL);
		Vector3_t6_49  L_16 = Vector3_get_zero_m6_213(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_15);
		Transform_set_localPosition_m6_616(L_15, L_16, /*hidden argument*/NULL);
		GameObject_t6_85 * L_17 = (__this->___textGo_2);
		NullCheck(L_17);
		MeshRenderer_t6_28 * L_18 = GameObject_AddComponent_TisMeshRenderer_t6_28_m6_1692(L_17, /*hidden argument*/GameObject_AddComponent_TisMeshRenderer_t6_28_m6_1692_MethodInfo_var);
		V_0 = L_18;
		MeshRenderer_t6_28 * L_19 = V_0;
		Font_t6_148 * L_20 = (__this->___font_5);
		NullCheck(L_20);
		Material_t6_67 * L_21 = Font_get_material_m6_910(L_20, /*hidden argument*/NULL);
		NullCheck(L_19);
		Renderer_set_material_m6_120(L_19, L_21, /*hidden argument*/NULL);
		GameObject_t6_85 * L_22 = (__this->___textGo_2);
		NullCheck(L_22);
		TextMesh_t6_145 * L_23 = GameObject_AddComponent_TisTextMesh_t6_145_m6_1693(L_22, /*hidden argument*/GameObject_AddComponent_TisTextMesh_t6_145_m6_1693_MethodInfo_var);
		__this->___tm_3 = L_23;
		TextMesh_t6_145 * L_24 = (__this->___tm_3);
		Font_t6_148 * L_25 = (__this->___font_5);
		NullCheck(L_24);
		TextMesh_set_font_m6_859(L_24, L_25, /*hidden argument*/NULL);
		TextMesh_t6_145 * L_26 = (__this->___tm_3);
		NullCheck(L_26);
		TextMesh_set_anchor_m6_860(L_26, 4, /*hidden argument*/NULL);
		float L_27 = (__this->___CharacterSize_4);
		if ((!(((float)L_27) > ((float)(0.0f)))))
		{
			goto IL_00ff;
		}
	}
	{
		TextMesh_t6_145 * L_28 = (__this->___tm_3);
		float L_29 = (__this->___CharacterSize_4);
		NullCheck(L_28);
		TextMesh_set_characterSize_m6_861(L_28, L_29, /*hidden argument*/NULL);
	}

IL_00ff:
	{
		return;
	}
}
// System.Void ShowInfoOfPlayer::Update()
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* Int32_t1_3_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3578;
extern Il2CppCodeGenString* _stringLiteral3579;
extern Il2CppCodeGenString* _stringLiteral3580;
extern "C" void ShowInfoOfPlayer_Update_m8_1024 (ShowInfoOfPlayer_t8_174 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(11);
		Int32_t1_3_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(8);
		_stringLiteral3578 = il2cpp_codegen_string_literal_from_index(3578);
		_stringLiteral3579 = il2cpp_codegen_string_literal_from_index(3579);
		_stringLiteral3580 = il2cpp_codegen_string_literal_from_index(3580);
		s_Il2CppMethodIntialized = true;
	}
	bool V_0 = false;
	PhotonPlayer_t8_102 * V_1 = {0};
	int32_t G_B3_0 = 0;
	TextMesh_t6_145 * G_B10_0 = {0};
	TextMesh_t6_145 * G_B9_0 = {0};
	String_t* G_B11_0 = {0};
	TextMesh_t6_145 * G_B11_1 = {0};
	{
		bool L_0 = (__this->___DisableOnOwnObjects_6);
		if (!L_0)
		{
			goto IL_0018;
		}
	}
	{
		PhotonView_t8_3 * L_1 = MonoBehaviour_get_photonView_m8_495(__this, /*hidden argument*/NULL);
		NullCheck(L_1);
		bool L_2 = PhotonView_get_isMine_m8_774(L_1, /*hidden argument*/NULL);
		G_B3_0 = ((int32_t)(L_2));
		goto IL_0019;
	}

IL_0018:
	{
		G_B3_0 = 1;
	}

IL_0019:
	{
		V_0 = G_B3_0;
		GameObject_t6_85 * L_3 = (__this->___textGo_2);
		bool L_4 = Object_op_Inequality_m6_580(NULL /*static, unused*/, L_3, (Object_t6_5 *)NULL, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_0037;
		}
	}
	{
		GameObject_t6_85 * L_5 = (__this->___textGo_2);
		bool L_6 = V_0;
		NullCheck(L_5);
		GameObject_SetActive_m6_596(L_5, L_6, /*hidden argument*/NULL);
	}

IL_0037:
	{
		bool L_7 = V_0;
		if (L_7)
		{
			goto IL_003e;
		}
	}
	{
		return;
	}

IL_003e:
	{
		PhotonView_t8_3 * L_8 = MonoBehaviour_get_photonView_m8_495(__this, /*hidden argument*/NULL);
		NullCheck(L_8);
		PhotonPlayer_t8_102 * L_9 = PhotonView_get_owner_m8_770(L_8, /*hidden argument*/NULL);
		V_1 = L_9;
		PhotonPlayer_t8_102 * L_10 = V_1;
		if (!L_10)
		{
			goto IL_0090;
		}
	}
	{
		TextMesh_t6_145 * L_11 = (__this->___tm_3);
		PhotonPlayer_t8_102 * L_12 = V_1;
		NullCheck(L_12);
		String_t* L_13 = PhotonPlayer_get_name_m8_731(L_12, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_14 = String_IsNullOrEmpty_m1_397(NULL /*static, unused*/, L_13, /*hidden argument*/NULL);
		G_B9_0 = L_11;
		if (!L_14)
		{
			G_B10_0 = L_11;
			goto IL_0080;
		}
	}
	{
		PhotonPlayer_t8_102 * L_15 = V_1;
		NullCheck(L_15);
		int32_t L_16 = PhotonPlayer_get_ID_m8_730(L_15, /*hidden argument*/NULL);
		int32_t L_17 = L_16;
		Object_t * L_18 = Box(Int32_t1_3_il2cpp_TypeInfo_var, &L_17);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_19 = String_Concat_m1_416(NULL /*static, unused*/, _stringLiteral3578, L_18, /*hidden argument*/NULL);
		G_B11_0 = L_19;
		G_B11_1 = G_B9_0;
		goto IL_0086;
	}

IL_0080:
	{
		PhotonPlayer_t8_102 * L_20 = V_1;
		NullCheck(L_20);
		String_t* L_21 = PhotonPlayer_get_name_m8_731(L_20, /*hidden argument*/NULL);
		G_B11_0 = L_21;
		G_B11_1 = G_B10_0;
	}

IL_0086:
	{
		NullCheck(G_B11_1);
		TextMesh_set_text_m6_858(G_B11_1, G_B11_0, /*hidden argument*/NULL);
		goto IL_00c5;
	}

IL_0090:
	{
		PhotonView_t8_3 * L_22 = MonoBehaviour_get_photonView_m8_495(__this, /*hidden argument*/NULL);
		NullCheck(L_22);
		bool L_23 = PhotonView_get_isSceneView_m8_769(L_22, /*hidden argument*/NULL);
		if (!L_23)
		{
			goto IL_00b5;
		}
	}
	{
		TextMesh_t6_145 * L_24 = (__this->___tm_3);
		NullCheck(L_24);
		TextMesh_set_text_m6_858(L_24, _stringLiteral3579, /*hidden argument*/NULL);
		goto IL_00c5;
	}

IL_00b5:
	{
		TextMesh_t6_145 * L_25 = (__this->___tm_3);
		NullCheck(L_25);
		TextMesh_set_text_m6_858(L_25, _stringLiteral3580, /*hidden argument*/NULL);
	}

IL_00c5:
	{
		return;
	}
}
// System.Void ShowStatusWhenConnecting::.ctor()
extern "C" void ShowStatusWhenConnecting__ctor_m8_1025 (ShowStatusWhenConnecting_t8_175 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m6_525(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void ShowStatusWhenConnecting::OnGUI()
extern TypeInfo* GUI_t6_160_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* GUILayoutOptionU5BU5D_t6_165_il2cpp_TypeInfo_var;
extern TypeInfo* PhotonNetwork_t8_114_il2cpp_TypeInfo_var;
extern TypeInfo* PeerState_t8_69_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3581;
extern Il2CppCodeGenString* _stringLiteral3582;
extern "C" void ShowStatusWhenConnecting_OnGUI_m8_1026 (ShowStatusWhenConnecting_t8_175 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GUI_t6_160_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(982);
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(11);
		GUILayoutOptionU5BU5D_t6_165_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(991);
		PhotonNetwork_t8_114_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1058);
		PeerState_t8_69_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1061);
		_stringLiteral3581 = il2cpp_codegen_string_literal_from_index(3581);
		_stringLiteral3582 = il2cpp_codegen_string_literal_from_index(3582);
		s_Il2CppMethodIntialized = true;
	}
	float V_0 = 0.0f;
	float V_1 = 0.0f;
	Rect_t6_52  V_2 = {0};
	{
		GUISkin_t6_161 * L_0 = (__this->___Skin_2);
		bool L_1 = Object_op_Inequality_m6_580(NULL /*static, unused*/, L_0, (Object_t6_5 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_001c;
		}
	}
	{
		GUISkin_t6_161 * L_2 = (__this->___Skin_2);
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t6_160_il2cpp_TypeInfo_var);
		GUI_set_skin_m6_1040(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
	}

IL_001c:
	{
		V_0 = (400.0f);
		V_1 = (100.0f);
		int32_t L_3 = Screen_get_width_m6_121(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_4 = V_0;
		int32_t L_5 = Screen_get_height_m6_122(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_6 = V_1;
		float L_7 = V_0;
		float L_8 = V_1;
		Rect__ctor_m6_270((&V_2), ((float)((float)((float)((float)(((float)((float)L_3)))-(float)L_4))/(float)(2.0f))), ((float)((float)((float)((float)(((float)((float)L_5)))-(float)L_6))/(float)(2.0f))), L_7, L_8, /*hidden argument*/NULL);
		Rect_t6_52  L_9 = V_2;
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t6_160_il2cpp_TypeInfo_var);
		GUISkin_t6_161 * L_10 = GUI_get_skin_m6_1041(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_10);
		GUIStyle_t6_166 * L_11 = GUISkin_get_box_m6_1219(L_10, /*hidden argument*/NULL);
		GUILayout_BeginArea_m6_1134(NULL /*static, unused*/, L_9, L_11, /*hidden argument*/NULL);
		String_t* L_12 = ShowStatusWhenConnecting_GetConnectingDots_m8_1027(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_13 = String_Concat_m1_418(NULL /*static, unused*/, _stringLiteral3581, L_12, /*hidden argument*/NULL);
		GUISkin_t6_161 * L_14 = GUI_get_skin_m6_1041(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_14);
		GUIStyleU5BU5D_t6_179* L_15 = GUISkin_get_customStyles_m6_1259(L_14, /*hidden argument*/NULL);
		NullCheck(L_15);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_15, 0);
		int32_t L_16 = 0;
		GUILayout_Label_m6_1113(NULL /*static, unused*/, L_13, (*(GUIStyle_t6_166 **)(GUIStyle_t6_166 **)SZArrayLdElema(L_15, L_16, sizeof(GUIStyle_t6_166 *))), ((GUILayoutOptionU5BU5D_t6_165*)SZArrayNew(GUILayoutOptionU5BU5D_t6_165_il2cpp_TypeInfo_var, 0)), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(PhotonNetwork_t8_114_il2cpp_TypeInfo_var);
		int32_t L_17 = PhotonNetwork_get_connectionStateDetailed_m8_607(NULL /*static, unused*/, /*hidden argument*/NULL);
		int32_t L_18 = L_17;
		Object_t * L_19 = Box(PeerState_t8_69_il2cpp_TypeInfo_var, &L_18);
		String_t* L_20 = String_Concat_m1_416(NULL /*static, unused*/, _stringLiteral3582, L_19, /*hidden argument*/NULL);
		GUILayout_Label_m6_1112(NULL /*static, unused*/, L_20, ((GUILayoutOptionU5BU5D_t6_165*)SZArrayNew(GUILayoutOptionU5BU5D_t6_165_il2cpp_TypeInfo_var, 0)), /*hidden argument*/NULL);
		GUILayout_EndArea_m6_1136(NULL /*static, unused*/, /*hidden argument*/NULL);
		int32_t L_21 = PhotonNetwork_get_connectionStateDetailed_m8_607(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_21) == ((uint32_t)((int32_t)9)))))
		{
			goto IL_00bb;
		}
	}
	{
		Behaviour_set_enabled_m6_458(__this, 0, /*hidden argument*/NULL);
	}

IL_00bb:
	{
		return;
	}
}
// System.String ShowStatusWhenConnecting::GetConnectingDots()
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* Mathf_t6_58_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3583;
extern "C" String_t* ShowStatusWhenConnecting_GetConnectingDots_m8_1027 (ShowStatusWhenConnecting_t8_175 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(11);
		Mathf_t6_58_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(940);
		_stringLiteral3583 = il2cpp_codegen_string_literal_from_index(3583);
		s_Il2CppMethodIntialized = true;
	}
	String_t* V_0 = {0};
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_0 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->___Empty_2;
		V_0 = L_0;
		float L_1 = Time_get_timeSinceLevelLoad_m6_653(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t6_58_il2cpp_TypeInfo_var);
		int32_t L_2 = Mathf_FloorToInt_m6_387(NULL /*static, unused*/, (fmodf(((float)((float)L_1*(float)(3.0f))), (4.0f))), /*hidden argument*/NULL);
		V_1 = L_2;
		V_2 = 0;
		goto IL_0034;
	}

IL_0024:
	{
		String_t* L_3 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_4 = String_Concat_m1_418(NULL /*static, unused*/, L_3, _stringLiteral3583, /*hidden argument*/NULL);
		V_0 = L_4;
		int32_t L_5 = V_2;
		V_2 = ((int32_t)((int32_t)L_5+(int32_t)1));
	}

IL_0034:
	{
		int32_t L_6 = V_2;
		int32_t L_7 = V_1;
		if ((((int32_t)L_6) < ((int32_t)L_7)))
		{
			goto IL_0024;
		}
	}
	{
		String_t* L_8 = V_0;
		return L_8;
	}
}
// System.Void SmoothSyncMovement::.ctor()
extern "C" void SmoothSyncMovement__ctor_m8_1028 (SmoothSyncMovement_t8_176 * __this, const MethodInfo* method)
{
	{
		__this->___SmoothingDelay_2 = (5.0f);
		Vector3_t6_49  L_0 = Vector3_get_zero_m6_213(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->___correctPlayerPos_3 = L_0;
		Quaternion_t6_51  L_1 = Quaternion_get_identity_m6_244(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->___correctPlayerRot_4 = L_1;
		MonoBehaviour__ctor_m8_494(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void SmoothSyncMovement::Awake()
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3584;
extern "C" void SmoothSyncMovement_Awake_m8_1029 (SmoothSyncMovement_t8_176 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(11);
		_stringLiteral3584 = il2cpp_codegen_string_literal_from_index(3584);
		s_Il2CppMethodIntialized = true;
	}
	{
		PhotonView_t8_3 * L_0 = MonoBehaviour_get_photonView_m8_495(__this, /*hidden argument*/NULL);
		bool L_1 = Object_op_Equality_m6_579(NULL /*static, unused*/, L_0, (Object_t6_5 *)NULL, /*hidden argument*/NULL);
		if (L_1)
		{
			goto IL_0027;
		}
	}
	{
		PhotonView_t8_3 * L_2 = MonoBehaviour_get_photonView_m8_495(__this, /*hidden argument*/NULL);
		NullCheck(L_2);
		Component_t6_26 * L_3 = (L_2->___observed_9);
		bool L_4 = Object_op_Inequality_m6_580(NULL /*static, unused*/, L_3, __this, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_0037;
		}
	}

IL_0027:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_5 = String_Concat_m1_416(NULL /*static, unused*/, __this, _stringLiteral3584, /*hidden argument*/NULL);
		Debug_LogWarning_m6_491(NULL /*static, unused*/, L_5, /*hidden argument*/NULL);
	}

IL_0037:
	{
		return;
	}
}
// System.Void SmoothSyncMovement::OnPhotonSerializeView(PhotonStream,PhotonMessageInfo)
extern TypeInfo* Vector3_t6_49_il2cpp_TypeInfo_var;
extern TypeInfo* Quaternion_t6_51_il2cpp_TypeInfo_var;
extern "C" void SmoothSyncMovement_OnPhotonSerializeView_m8_1030 (SmoothSyncMovement_t8_176 * __this, PhotonStream_t8_106 * ___stream, PhotonMessageInfo_t8_104 * ___info, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Vector3_t6_49_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(941);
		Quaternion_t6_51_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(943);
		s_Il2CppMethodIntialized = true;
	}
	{
		PhotonStream_t8_106 * L_0 = ___stream;
		NullCheck(L_0);
		bool L_1 = PhotonStream_get_isWriting_m8_538(L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_003c;
		}
	}
	{
		PhotonStream_t8_106 * L_2 = ___stream;
		Transform_t6_61 * L_3 = Component_get_transform_m6_582(__this, /*hidden argument*/NULL);
		NullCheck(L_3);
		Vector3_t6_49  L_4 = Transform_get_position_m6_611(L_3, /*hidden argument*/NULL);
		Vector3_t6_49  L_5 = L_4;
		Object_t * L_6 = Box(Vector3_t6_49_il2cpp_TypeInfo_var, &L_5);
		NullCheck(L_2);
		PhotonStream_SendNext_m8_543(L_2, L_6, /*hidden argument*/NULL);
		PhotonStream_t8_106 * L_7 = ___stream;
		Transform_t6_61 * L_8 = Component_get_transform_m6_582(__this, /*hidden argument*/NULL);
		NullCheck(L_8);
		Quaternion_t6_51  L_9 = Transform_get_rotation_m6_623(L_8, /*hidden argument*/NULL);
		Quaternion_t6_51  L_10 = L_9;
		Object_t * L_11 = Box(Quaternion_t6_51_il2cpp_TypeInfo_var, &L_10);
		NullCheck(L_7);
		PhotonStream_SendNext_m8_543(L_7, L_11, /*hidden argument*/NULL);
		goto IL_005e;
	}

IL_003c:
	{
		PhotonStream_t8_106 * L_12 = ___stream;
		NullCheck(L_12);
		Object_t * L_13 = PhotonStream_ReceiveNext_m8_541(L_12, /*hidden argument*/NULL);
		__this->___correctPlayerPos_3 = ((*(Vector3_t6_49 *)((Vector3_t6_49 *)UnBox (L_13, Vector3_t6_49_il2cpp_TypeInfo_var))));
		PhotonStream_t8_106 * L_14 = ___stream;
		NullCheck(L_14);
		Object_t * L_15 = PhotonStream_ReceiveNext_m8_541(L_14, /*hidden argument*/NULL);
		__this->___correctPlayerRot_4 = ((*(Quaternion_t6_51 *)((Quaternion_t6_51 *)UnBox (L_15, Quaternion_t6_51_il2cpp_TypeInfo_var))));
	}

IL_005e:
	{
		return;
	}
}
// System.Void SmoothSyncMovement::Update()
extern "C" void SmoothSyncMovement_Update_m8_1031 (SmoothSyncMovement_t8_176 * __this, const MethodInfo* method)
{
	{
		PhotonView_t8_3 * L_0 = MonoBehaviour_get_photonView_m8_495(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		bool L_1 = PhotonView_get_isMine_m8_774(L_0, /*hidden argument*/NULL);
		if (L_1)
		{
			goto IL_006a;
		}
	}
	{
		Transform_t6_61 * L_2 = Component_get_transform_m6_582(__this, /*hidden argument*/NULL);
		Transform_t6_61 * L_3 = Component_get_transform_m6_582(__this, /*hidden argument*/NULL);
		NullCheck(L_3);
		Vector3_t6_49  L_4 = Transform_get_position_m6_611(L_3, /*hidden argument*/NULL);
		Vector3_t6_49  L_5 = (__this->___correctPlayerPos_3);
		float L_6 = Time_get_deltaTime_m6_654(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_7 = (__this->___SmoothingDelay_2);
		Vector3_t6_49  L_8 = Vector3_Lerp_m6_192(NULL /*static, unused*/, L_4, L_5, ((float)((float)L_6*(float)L_7)), /*hidden argument*/NULL);
		NullCheck(L_2);
		Transform_set_position_m6_612(L_2, L_8, /*hidden argument*/NULL);
		Transform_t6_61 * L_9 = Component_get_transform_m6_582(__this, /*hidden argument*/NULL);
		Transform_t6_61 * L_10 = Component_get_transform_m6_582(__this, /*hidden argument*/NULL);
		NullCheck(L_10);
		Quaternion_t6_51  L_11 = Transform_get_rotation_m6_623(L_10, /*hidden argument*/NULL);
		Quaternion_t6_51  L_12 = (__this->___correctPlayerRot_4);
		float L_13 = Time_get_deltaTime_m6_654(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_14 = (__this->___SmoothingDelay_2);
		Quaternion_t6_51  L_15 = Quaternion_Lerp_m6_252(NULL /*static, unused*/, L_11, L_12, ((float)((float)L_13*(float)L_14)), /*hidden argument*/NULL);
		NullCheck(L_9);
		Transform_set_rotation_m6_624(L_9, L_15, /*hidden argument*/NULL);
	}

IL_006a:
	{
		return;
	}
}
// System.Void SupportLogger::.ctor()
extern "C" void SupportLogger__ctor_m8_1032 (SupportLogger_t8_177 * __this, const MethodInfo* method)
{
	{
		__this->___LogTrafficStats_2 = 1;
		MonoBehaviour__ctor_m6_525(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void SupportLogger::Start()
extern TypeInfo* GameObject_t6_85_il2cpp_TypeInfo_var;
extern const MethodInfo* GameObject_AddComponent_TisSupportLogging_t8_178_m6_1694_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral3585;
extern "C" void SupportLogger_Start_m8_1033 (SupportLogger_t8_177 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GameObject_t6_85_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1082);
		GameObject_AddComponent_TisSupportLogging_t8_178_m6_1694_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483969);
		_stringLiteral3585 = il2cpp_codegen_string_literal_from_index(3585);
		s_Il2CppMethodIntialized = true;
	}
	GameObject_t6_85 * V_0 = {0};
	SupportLogging_t8_178 * V_1 = {0};
	{
		GameObject_t6_85 * L_0 = GameObject_Find_m6_606(NULL /*static, unused*/, _stringLiteral3585, /*hidden argument*/NULL);
		V_0 = L_0;
		GameObject_t6_85 * L_1 = V_0;
		bool L_2 = Object_op_Equality_m6_579(NULL /*static, unused*/, L_1, (Object_t6_5 *)NULL, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_003b;
		}
	}
	{
		GameObject_t6_85 * L_3 = (GameObject_t6_85 *)il2cpp_codegen_object_new (GameObject_t6_85_il2cpp_TypeInfo_var);
		GameObject__ctor_m6_589(L_3, _stringLiteral3585, /*hidden argument*/NULL);
		V_0 = L_3;
		GameObject_t6_85 * L_4 = V_0;
		Object_DontDestroyOnLoad_m6_564(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
		GameObject_t6_85 * L_5 = V_0;
		NullCheck(L_5);
		SupportLogging_t8_178 * L_6 = GameObject_AddComponent_TisSupportLogging_t8_178_m6_1694(L_5, /*hidden argument*/GameObject_AddComponent_TisSupportLogging_t8_178_m6_1694_MethodInfo_var);
		V_1 = L_6;
		SupportLogging_t8_178 * L_7 = V_1;
		bool L_8 = (__this->___LogTrafficStats_2);
		NullCheck(L_7);
		L_7->___LogTrafficStats_2 = L_8;
	}

IL_003b:
	{
		return;
	}
}
// System.Void SupportLogging::.ctor()
extern "C" void SupportLogging__ctor_m8_1034 (SupportLogging_t8_178 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m6_525(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void SupportLogging::Start()
extern Il2CppCodeGenString* _stringLiteral3586;
extern "C" void SupportLogging_Start_m8_1035 (SupportLogging_t8_178 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		_stringLiteral3586 = il2cpp_codegen_string_literal_from_index(3586);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool L_0 = (__this->___LogTrafficStats_2);
		if (!L_0)
		{
			goto IL_0020;
		}
	}
	{
		MonoBehaviour_InvokeRepeating_m6_528(__this, _stringLiteral3586, (10.0f), (10.0f), /*hidden argument*/NULL);
	}

IL_0020:
	{
		return;
	}
}
// System.Void SupportLogging::OnApplicationPause(System.Boolean)
extern TypeInfo* ObjectU5BU5D_t1_157_il2cpp_TypeInfo_var;
extern TypeInfo* Boolean_t1_20_il2cpp_TypeInfo_var;
extern TypeInfo* PhotonNetwork_t8_114_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3587;
extern Il2CppCodeGenString* _stringLiteral3588;
extern "C" void SupportLogging_OnApplicationPause_m8_1036 (SupportLogging_t8_178 * __this, bool ___pause, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ObjectU5BU5D_t1_157_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(19);
		Boolean_t1_20_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(49);
		PhotonNetwork_t8_114_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1058);
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(11);
		_stringLiteral3587 = il2cpp_codegen_string_literal_from_index(3587);
		_stringLiteral3588 = il2cpp_codegen_string_literal_from_index(3588);
		s_Il2CppMethodIntialized = true;
	}
	{
		ObjectU5BU5D_t1_157* L_0 = ((ObjectU5BU5D_t1_157*)SZArrayNew(ObjectU5BU5D_t1_157_il2cpp_TypeInfo_var, 4));
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, 0);
		ArrayElementTypeCheck (L_0, _stringLiteral3587);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_0, 0, sizeof(Object_t *))) = (Object_t *)_stringLiteral3587;
		ObjectU5BU5D_t1_157* L_1 = L_0;
		bool L_2 = ___pause;
		bool L_3 = L_2;
		Object_t * L_4 = Box(Boolean_t1_20_il2cpp_TypeInfo_var, &L_3);
		NullCheck(L_1);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_1, 1);
		ArrayElementTypeCheck (L_1, L_4);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_1, 1, sizeof(Object_t *))) = (Object_t *)L_4;
		ObjectU5BU5D_t1_157* L_5 = L_1;
		NullCheck(L_5);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_5, 2);
		ArrayElementTypeCheck (L_5, _stringLiteral3588);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_5, 2, sizeof(Object_t *))) = (Object_t *)_stringLiteral3588;
		ObjectU5BU5D_t1_157* L_6 = L_5;
		IL2CPP_RUNTIME_CLASS_INIT(PhotonNetwork_t8_114_il2cpp_TypeInfo_var);
		bool L_7 = PhotonNetwork_get_connected_m8_603(NULL /*static, unused*/, /*hidden argument*/NULL);
		bool L_8 = L_7;
		Object_t * L_9 = Box(Boolean_t1_20_il2cpp_TypeInfo_var, &L_8);
		NullCheck(L_6);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_6, 3);
		ArrayElementTypeCheck (L_6, L_9);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_6, 3, sizeof(Object_t *))) = (Object_t *)L_9;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_10 = String_Concat_m1_421(NULL /*static, unused*/, L_6, /*hidden argument*/NULL);
		Debug_Log_m6_489(NULL /*static, unused*/, L_10, /*hidden argument*/NULL);
		return;
	}
}
// System.Void SupportLogging::OnApplicationQuit()
extern "C" void SupportLogging_OnApplicationQuit_m8_1037 (SupportLogging_t8_178 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour_CancelInvoke_m6_529(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void SupportLogging::LogStats()
extern TypeInfo* PhotonNetwork_t8_114_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3589;
extern "C" void SupportLogging_LogStats_m8_1038 (SupportLogging_t8_178 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		PhotonNetwork_t8_114_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1058);
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(11);
		_stringLiteral3589 = il2cpp_codegen_string_literal_from_index(3589);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool L_0 = (__this->___LogTrafficStats_2);
		if (!L_0)
		{
			goto IL_001f;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(PhotonNetwork_t8_114_il2cpp_TypeInfo_var);
		String_t* L_1 = PhotonNetwork_NetworkStatisticsToString_m8_673(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_2 = String_Concat_m1_418(NULL /*static, unused*/, _stringLiteral3589, L_1, /*hidden argument*/NULL);
		Debug_Log_m6_489(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
	}

IL_001f:
	{
		return;
	}
}
// System.Void SupportLogging::LogBasics()
extern TypeInfo* StringBuilder_t1_145_il2cpp_TypeInfo_var;
extern TypeInfo* PhotonNetwork_t8_114_il2cpp_TypeInfo_var;
extern TypeInfo* CloudRegionCode_t8_65_il2cpp_TypeInfo_var;
extern TypeInfo* HostingOption_t8_125_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3590;
extern Il2CppCodeGenString* _stringLiteral3237;
extern Il2CppCodeGenString* _stringLiteral3591;
extern Il2CppCodeGenString* _stringLiteral3592;
extern Il2CppCodeGenString* _stringLiteral3593;
extern "C" void SupportLogging_LogBasics_m8_1039 (SupportLogging_t8_178 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		StringBuilder_t1_145_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(36);
		PhotonNetwork_t8_114_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1058);
		CloudRegionCode_t8_65_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1123);
		HostingOption_t8_125_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1165);
		_stringLiteral3590 = il2cpp_codegen_string_literal_from_index(3590);
		_stringLiteral3237 = il2cpp_codegen_string_literal_from_index(3237);
		_stringLiteral3591 = il2cpp_codegen_string_literal_from_index(3591);
		_stringLiteral3592 = il2cpp_codegen_string_literal_from_index(3592);
		_stringLiteral3593 = il2cpp_codegen_string_literal_from_index(3593);
		s_Il2CppMethodIntialized = true;
	}
	StringBuilder_t1_145 * V_0 = {0};
	{
		StringBuilder_t1_145 * L_0 = (StringBuilder_t1_145 *)il2cpp_codegen_object_new (StringBuilder_t1_145_il2cpp_TypeInfo_var);
		StringBuilder__ctor_m1_4329(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		StringBuilder_t1_145 * L_1 = V_0;
		NullCheck(L_1);
		StringBuilder_AppendFormat_m1_4357(L_1, _stringLiteral3590, _stringLiteral3237, /*hidden argument*/NULL);
		StringBuilder_t1_145 * L_2 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(PhotonNetwork_t8_114_il2cpp_TypeInfo_var);
		NetworkingPeer_t8_98 * L_3 = ((PhotonNetwork_t8_114_StaticFields*)PhotonNetwork_t8_114_il2cpp_TypeInfo_var->static_fields)->___networkingPeer_4;
		NullCheck(L_3);
		String_t* L_4 = (L_3->___mAppId_9);
		NullCheck(L_4);
		String_t* L_5 = String_Substring_m1_352(L_4, 0, 8, /*hidden argument*/NULL);
		NetworkingPeer_t8_98 * L_6 = ((PhotonNetwork_t8_114_StaticFields*)PhotonNetwork_t8_114_il2cpp_TypeInfo_var->static_fields)->___networkingPeer_4;
		NullCheck(L_6);
		String_t* L_7 = NetworkingPeer_get_mAppVersionPun_m8_369(L_6, /*hidden argument*/NULL);
		NullCheck(L_2);
		StringBuilder_AppendFormat_m1_4358(L_2, _stringLiteral3591, L_5, L_7, /*hidden argument*/NULL);
		StringBuilder_t1_145 * L_8 = V_0;
		String_t* L_9 = PhotonNetwork_get_ServerAddress_m8_602(NULL /*static, unused*/, /*hidden argument*/NULL);
		NetworkingPeer_t8_98 * L_10 = ((PhotonNetwork_t8_114_StaticFields*)PhotonNetwork_t8_114_il2cpp_TypeInfo_var->static_fields)->___networkingPeer_4;
		NullCheck(L_10);
		int32_t L_11 = NetworkingPeer_get_CloudRegion_m8_386(L_10, /*hidden argument*/NULL);
		int32_t L_12 = L_11;
		Object_t * L_13 = Box(CloudRegionCode_t8_65_il2cpp_TypeInfo_var, &L_12);
		NullCheck(L_8);
		StringBuilder_AppendFormat_m1_4358(L_8, _stringLiteral3592, L_9, L_13, /*hidden argument*/NULL);
		StringBuilder_t1_145 * L_14 = V_0;
		ServerSettings_t8_115 * L_15 = ((PhotonNetwork_t8_114_StaticFields*)PhotonNetwork_t8_114_il2cpp_TypeInfo_var->static_fields)->___PhotonServerSettings_6;
		NullCheck(L_15);
		int32_t L_16 = (L_15->___HostType_2);
		int32_t L_17 = L_16;
		Object_t * L_18 = Box(HostingOption_t8_125_il2cpp_TypeInfo_var, &L_17);
		NullCheck(L_14);
		StringBuilder_AppendFormat_m1_4357(L_14, _stringLiteral3593, L_18, /*hidden argument*/NULL);
		StringBuilder_t1_145 * L_19 = V_0;
		NullCheck(L_19);
		String_t* L_20 = StringBuilder_ToString_m1_4341(L_19, /*hidden argument*/NULL);
		Debug_Log_m6_489(NULL /*static, unused*/, L_20, /*hidden argument*/NULL);
		return;
	}
}
// System.Void SupportLogging::OnConnectedToPhoton()
extern TypeInfo* PhotonNetwork_t8_114_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3594;
extern "C" void SupportLogging_OnConnectedToPhoton_m8_1040 (SupportLogging_t8_178 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		PhotonNetwork_t8_114_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1058);
		_stringLiteral3594 = il2cpp_codegen_string_literal_from_index(3594);
		s_Il2CppMethodIntialized = true;
	}
	{
		Debug_Log_m6_489(NULL /*static, unused*/, _stringLiteral3594, /*hidden argument*/NULL);
		SupportLogging_LogBasics_m8_1039(__this, /*hidden argument*/NULL);
		bool L_0 = (__this->___LogTrafficStats_2);
		if (!L_0)
		{
			goto IL_0021;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(PhotonNetwork_t8_114_il2cpp_TypeInfo_var);
		PhotonNetwork_set_NetworkStatisticsEnabled_m8_656(NULL /*static, unused*/, 1, /*hidden argument*/NULL);
	}

IL_0021:
	{
		return;
	}
}
// System.Void SupportLogging::OnFailedToConnectToPhoton(DisconnectCause)
extern TypeInfo* DisconnectCause_t8_70_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3595;
extern Il2CppCodeGenString* _stringLiteral2583;
extern "C" void SupportLogging_OnFailedToConnectToPhoton_m8_1041 (SupportLogging_t8_178 * __this, int32_t ___cause, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DisconnectCause_t8_70_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1134);
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(11);
		_stringLiteral3595 = il2cpp_codegen_string_literal_from_index(3595);
		_stringLiteral2583 = il2cpp_codegen_string_literal_from_index(2583);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = ___cause;
		int32_t L_1 = L_0;
		Object_t * L_2 = Box(DisconnectCause_t8_70_il2cpp_TypeInfo_var, &L_1);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_3 = String_Concat_m1_417(NULL /*static, unused*/, _stringLiteral3595, L_2, _stringLiteral2583, /*hidden argument*/NULL);
		Debug_Log_m6_489(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		SupportLogging_LogBasics_m8_1039(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void SupportLogging::OnJoinedLobby()
extern TypeInfo* PhotonNetwork_t8_114_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3596;
extern Il2CppCodeGenString* _stringLiteral2583;
extern "C" void SupportLogging_OnJoinedLobby_m8_1042 (SupportLogging_t8_178 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		PhotonNetwork_t8_114_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1058);
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(11);
		_stringLiteral3596 = il2cpp_codegen_string_literal_from_index(3596);
		_stringLiteral2583 = il2cpp_codegen_string_literal_from_index(2583);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(PhotonNetwork_t8_114_il2cpp_TypeInfo_var);
		TypedLobby_t8_79 * L_0 = PhotonNetwork_get_lobby_m8_636(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_1 = String_Concat_m1_417(NULL /*static, unused*/, _stringLiteral3596, L_0, _stringLiteral2583, /*hidden argument*/NULL);
		Debug_Log_m6_489(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void SupportLogging::OnJoinedRoom()
extern TypeInfo* ObjectU5BU5D_t1_157_il2cpp_TypeInfo_var;
extern TypeInfo* PhotonNetwork_t8_114_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3597;
extern Il2CppCodeGenString* _stringLiteral3598;
extern Il2CppCodeGenString* _stringLiteral3599;
extern "C" void SupportLogging_OnJoinedRoom_m8_1043 (SupportLogging_t8_178 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ObjectU5BU5D_t1_157_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(19);
		PhotonNetwork_t8_114_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1058);
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(11);
		_stringLiteral3597 = il2cpp_codegen_string_literal_from_index(3597);
		_stringLiteral3598 = il2cpp_codegen_string_literal_from_index(3598);
		_stringLiteral3599 = il2cpp_codegen_string_literal_from_index(3599);
		s_Il2CppMethodIntialized = true;
	}
	{
		ObjectU5BU5D_t1_157* L_0 = ((ObjectU5BU5D_t1_157*)SZArrayNew(ObjectU5BU5D_t1_157_il2cpp_TypeInfo_var, 6));
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, 0);
		ArrayElementTypeCheck (L_0, _stringLiteral3597);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_0, 0, sizeof(Object_t *))) = (Object_t *)_stringLiteral3597;
		ObjectU5BU5D_t1_157* L_1 = L_0;
		IL2CPP_RUNTIME_CLASS_INIT(PhotonNetwork_t8_114_il2cpp_TypeInfo_var);
		Room_t8_100 * L_2 = PhotonNetwork_get_room_m8_611(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_1);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_1, 1);
		ArrayElementTypeCheck (L_1, L_2);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_1, 1, sizeof(Object_t *))) = (Object_t *)L_2;
		ObjectU5BU5D_t1_157* L_3 = L_1;
		NullCheck(L_3);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_3, 2);
		ArrayElementTypeCheck (L_3, _stringLiteral3598);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_3, 2, sizeof(Object_t *))) = (Object_t *)_stringLiteral3598;
		ObjectU5BU5D_t1_157* L_4 = L_3;
		TypedLobby_t8_79 * L_5 = PhotonNetwork_get_lobby_m8_636(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, 3);
		ArrayElementTypeCheck (L_4, L_5);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_4, 3, sizeof(Object_t *))) = (Object_t *)L_5;
		ObjectU5BU5D_t1_157* L_6 = L_4;
		NullCheck(L_6);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_6, 4);
		ArrayElementTypeCheck (L_6, _stringLiteral3599);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_6, 4, sizeof(Object_t *))) = (Object_t *)_stringLiteral3599;
		ObjectU5BU5D_t1_157* L_7 = L_6;
		String_t* L_8 = PhotonNetwork_get_ServerAddress_m8_602(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_7);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_7, 5);
		ArrayElementTypeCheck (L_7, L_8);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_7, 5, sizeof(Object_t *))) = (Object_t *)L_8;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_9 = String_Concat_m1_421(NULL /*static, unused*/, L_7, /*hidden argument*/NULL);
		Debug_Log_m6_489(NULL /*static, unused*/, L_9, /*hidden argument*/NULL);
		return;
	}
}
// System.Void SupportLogging::OnCreatedRoom()
extern TypeInfo* ObjectU5BU5D_t1_157_il2cpp_TypeInfo_var;
extern TypeInfo* PhotonNetwork_t8_114_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3600;
extern Il2CppCodeGenString* _stringLiteral3598;
extern Il2CppCodeGenString* _stringLiteral3599;
extern "C" void SupportLogging_OnCreatedRoom_m8_1044 (SupportLogging_t8_178 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ObjectU5BU5D_t1_157_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(19);
		PhotonNetwork_t8_114_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1058);
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(11);
		_stringLiteral3600 = il2cpp_codegen_string_literal_from_index(3600);
		_stringLiteral3598 = il2cpp_codegen_string_literal_from_index(3598);
		_stringLiteral3599 = il2cpp_codegen_string_literal_from_index(3599);
		s_Il2CppMethodIntialized = true;
	}
	{
		ObjectU5BU5D_t1_157* L_0 = ((ObjectU5BU5D_t1_157*)SZArrayNew(ObjectU5BU5D_t1_157_il2cpp_TypeInfo_var, 6));
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, 0);
		ArrayElementTypeCheck (L_0, _stringLiteral3600);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_0, 0, sizeof(Object_t *))) = (Object_t *)_stringLiteral3600;
		ObjectU5BU5D_t1_157* L_1 = L_0;
		IL2CPP_RUNTIME_CLASS_INIT(PhotonNetwork_t8_114_il2cpp_TypeInfo_var);
		Room_t8_100 * L_2 = PhotonNetwork_get_room_m8_611(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_1);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_1, 1);
		ArrayElementTypeCheck (L_1, L_2);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_1, 1, sizeof(Object_t *))) = (Object_t *)L_2;
		ObjectU5BU5D_t1_157* L_3 = L_1;
		NullCheck(L_3);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_3, 2);
		ArrayElementTypeCheck (L_3, _stringLiteral3598);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_3, 2, sizeof(Object_t *))) = (Object_t *)_stringLiteral3598;
		ObjectU5BU5D_t1_157* L_4 = L_3;
		TypedLobby_t8_79 * L_5 = PhotonNetwork_get_lobby_m8_636(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, 3);
		ArrayElementTypeCheck (L_4, L_5);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_4, 3, sizeof(Object_t *))) = (Object_t *)L_5;
		ObjectU5BU5D_t1_157* L_6 = L_4;
		NullCheck(L_6);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_6, 4);
		ArrayElementTypeCheck (L_6, _stringLiteral3599);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_6, 4, sizeof(Object_t *))) = (Object_t *)_stringLiteral3599;
		ObjectU5BU5D_t1_157* L_7 = L_6;
		String_t* L_8 = PhotonNetwork_get_ServerAddress_m8_602(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_7);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_7, 5);
		ArrayElementTypeCheck (L_7, L_8);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_7, 5, sizeof(Object_t *))) = (Object_t *)L_8;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_9 = String_Concat_m1_421(NULL /*static, unused*/, L_7, /*hidden argument*/NULL);
		Debug_Log_m6_489(NULL /*static, unused*/, L_9, /*hidden argument*/NULL);
		return;
	}
}
// System.Void SupportLogging::OnLeftRoom()
extern Il2CppCodeGenString* _stringLiteral3601;
extern "C" void SupportLogging_OnLeftRoom_m8_1045 (SupportLogging_t8_178 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		_stringLiteral3601 = il2cpp_codegen_string_literal_from_index(3601);
		s_Il2CppMethodIntialized = true;
	}
	{
		Debug_Log_m6_489(NULL /*static, unused*/, _stringLiteral3601, /*hidden argument*/NULL);
		return;
	}
}
// System.Void SupportLogging::OnDisconnectedFromPhoton()
extern Il2CppCodeGenString* _stringLiteral3602;
extern "C" void SupportLogging_OnDisconnectedFromPhoton_m8_1046 (SupportLogging_t8_178 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		_stringLiteral3602 = il2cpp_codegen_string_literal_from_index(3602);
		s_Il2CppMethodIntialized = true;
	}
	{
		Debug_Log_m6_489(NULL /*static, unused*/, _stringLiteral3602, /*hidden argument*/NULL);
		return;
	}
}
// System.Void ExitGames.Client.DemoParticle.TimeKeeper::.ctor(System.Int32)
extern "C" void TimeKeeper__ctor_m8_1047 (TimeKeeper_t8_179 * __this, int32_t ___interval, const MethodInfo* method)
{
	{
		int32_t L_0 = Environment_get_TickCount_m1_5041(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->___lastExecutionTime_0 = L_0;
		Object__ctor_m1_0(__this, /*hidden argument*/NULL);
		TimeKeeper_set_IsEnabled_m8_1051(__this, 1, /*hidden argument*/NULL);
		int32_t L_1 = ___interval;
		TimeKeeper_set_Interval_m8_1049(__this, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Int32 ExitGames.Client.DemoParticle.TimeKeeper::get_Interval()
extern "C" int32_t TimeKeeper_get_Interval_m8_1048 (TimeKeeper_t8_179 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (__this->___U3CIntervalU3Ek__BackingField_2);
		return L_0;
	}
}
// System.Void ExitGames.Client.DemoParticle.TimeKeeper::set_Interval(System.Int32)
extern "C" void TimeKeeper_set_Interval_m8_1049 (TimeKeeper_t8_179 * __this, int32_t ___value, const MethodInfo* method)
{
	{
		int32_t L_0 = ___value;
		__this->___U3CIntervalU3Ek__BackingField_2 = L_0;
		return;
	}
}
// System.Boolean ExitGames.Client.DemoParticle.TimeKeeper::get_IsEnabled()
extern "C" bool TimeKeeper_get_IsEnabled_m8_1050 (TimeKeeper_t8_179 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (__this->___U3CIsEnabledU3Ek__BackingField_3);
		return L_0;
	}
}
// System.Void ExitGames.Client.DemoParticle.TimeKeeper::set_IsEnabled(System.Boolean)
extern "C" void TimeKeeper_set_IsEnabled_m8_1051 (TimeKeeper_t8_179 * __this, bool ___value, const MethodInfo* method)
{
	{
		bool L_0 = ___value;
		__this->___U3CIsEnabledU3Ek__BackingField_3 = L_0;
		return;
	}
}
// System.Boolean ExitGames.Client.DemoParticle.TimeKeeper::get_ShouldExecute()
extern "C" bool TimeKeeper_get_ShouldExecute_m8_1052 (TimeKeeper_t8_179 * __this, const MethodInfo* method)
{
	int32_t G_B4_0 = 0;
	int32_t G_B6_0 = 0;
	{
		bool L_0 = TimeKeeper_get_IsEnabled_m8_1050(__this, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_002f;
		}
	}
	{
		bool L_1 = (__this->___shouldExecute_1);
		if (L_1)
		{
			goto IL_002c;
		}
	}
	{
		int32_t L_2 = Environment_get_TickCount_m1_5041(NULL /*static, unused*/, /*hidden argument*/NULL);
		int32_t L_3 = (__this->___lastExecutionTime_0);
		int32_t L_4 = TimeKeeper_get_Interval_m8_1048(__this, /*hidden argument*/NULL);
		G_B4_0 = ((((int32_t)((int32_t)((int32_t)L_2-(int32_t)L_3))) > ((int32_t)L_4))? 1 : 0);
		goto IL_002d;
	}

IL_002c:
	{
		G_B4_0 = 1;
	}

IL_002d:
	{
		G_B6_0 = G_B4_0;
		goto IL_0030;
	}

IL_002f:
	{
		G_B6_0 = 0;
	}

IL_0030:
	{
		return G_B6_0;
	}
}
// System.Void ExitGames.Client.DemoParticle.TimeKeeper::set_ShouldExecute(System.Boolean)
extern "C" void TimeKeeper_set_ShouldExecute_m8_1053 (TimeKeeper_t8_179 * __this, bool ___value, const MethodInfo* method)
{
	{
		bool L_0 = ___value;
		__this->___shouldExecute_1 = L_0;
		return;
	}
}
// System.Void ExitGames.Client.DemoParticle.TimeKeeper::Reset()
extern "C" void TimeKeeper_Reset_m8_1054 (TimeKeeper_t8_179 * __this, const MethodInfo* method)
{
	{
		__this->___shouldExecute_1 = 0;
		int32_t L_0 = Environment_get_TickCount_m1_5041(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->___lastExecutionTime_0 = L_0;
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
