﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.ObjectModel.Collection`1<System.Reflection.CustomAttributeTypedArgument>
struct Collection_1_t1_1072;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t1_130;
// System.Object
struct Object_t;
// System.Reflection.CustomAttributeTypedArgument[]
struct CustomAttributeTypedArgumentU5BU5D_t1_838;
// System.Collections.Generic.IEnumerator`1<System.Reflection.CustomAttributeTypedArgument>
struct IEnumerator_1_t1_1412;
// System.Collections.Generic.IList`1<System.Reflection.CustomAttributeTypedArgument>
struct IList_1_t1_329;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Reflection_CustomAttributeTypedArgument.h"

// System.Void System.Collections.ObjectModel.Collection`1<System.Reflection.CustomAttributeTypedArgument>::.ctor()
extern "C" void Collection_1__ctor_m1_6568_gshared (Collection_1_t1_1072 * __this, const MethodInfo* method);
#define Collection_1__ctor_m1_6568(__this, method) (( void (*) (Collection_1_t1_1072 *, const MethodInfo*))Collection_1__ctor_m1_6568_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<System.Reflection.CustomAttributeTypedArgument>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C" bool Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m1_6569_gshared (Collection_1_t1_1072 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m1_6569(__this, method) (( bool (*) (Collection_1_t1_1072 *, const MethodInfo*))Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m1_6569_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Reflection.CustomAttributeTypedArgument>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C" void Collection_1_System_Collections_ICollection_CopyTo_m1_6570_gshared (Collection_1_t1_1072 * __this, Array_t * ___array, int32_t ___index, const MethodInfo* method);
#define Collection_1_System_Collections_ICollection_CopyTo_m1_6570(__this, ___array, ___index, method) (( void (*) (Collection_1_t1_1072 *, Array_t *, int32_t, const MethodInfo*))Collection_1_System_Collections_ICollection_CopyTo_m1_6570_gshared)(__this, ___array, ___index, method)
// System.Collections.IEnumerator System.Collections.ObjectModel.Collection`1<System.Reflection.CustomAttributeTypedArgument>::System.Collections.IEnumerable.GetEnumerator()
extern "C" Object_t * Collection_1_System_Collections_IEnumerable_GetEnumerator_m1_6571_gshared (Collection_1_t1_1072 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_IEnumerable_GetEnumerator_m1_6571(__this, method) (( Object_t * (*) (Collection_1_t1_1072 *, const MethodInfo*))Collection_1_System_Collections_IEnumerable_GetEnumerator_m1_6571_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<System.Reflection.CustomAttributeTypedArgument>::System.Collections.IList.Add(System.Object)
extern "C" int32_t Collection_1_System_Collections_IList_Add_m1_6572_gshared (Collection_1_t1_1072 * __this, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Add_m1_6572(__this, ___value, method) (( int32_t (*) (Collection_1_t1_1072 *, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_Add_m1_6572_gshared)(__this, ___value, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<System.Reflection.CustomAttributeTypedArgument>::System.Collections.IList.Contains(System.Object)
extern "C" bool Collection_1_System_Collections_IList_Contains_m1_6573_gshared (Collection_1_t1_1072 * __this, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Contains_m1_6573(__this, ___value, method) (( bool (*) (Collection_1_t1_1072 *, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_Contains_m1_6573_gshared)(__this, ___value, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<System.Reflection.CustomAttributeTypedArgument>::System.Collections.IList.IndexOf(System.Object)
extern "C" int32_t Collection_1_System_Collections_IList_IndexOf_m1_6574_gshared (Collection_1_t1_1072 * __this, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_IndexOf_m1_6574(__this, ___value, method) (( int32_t (*) (Collection_1_t1_1072 *, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_IndexOf_m1_6574_gshared)(__this, ___value, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Reflection.CustomAttributeTypedArgument>::System.Collections.IList.Insert(System.Int32,System.Object)
extern "C" void Collection_1_System_Collections_IList_Insert_m1_6575_gshared (Collection_1_t1_1072 * __this, int32_t ___index, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Insert_m1_6575(__this, ___index, ___value, method) (( void (*) (Collection_1_t1_1072 *, int32_t, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_Insert_m1_6575_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Reflection.CustomAttributeTypedArgument>::System.Collections.IList.Remove(System.Object)
extern "C" void Collection_1_System_Collections_IList_Remove_m1_6576_gshared (Collection_1_t1_1072 * __this, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Remove_m1_6576(__this, ___value, method) (( void (*) (Collection_1_t1_1072 *, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_Remove_m1_6576_gshared)(__this, ___value, method)
// System.Object System.Collections.ObjectModel.Collection`1<System.Reflection.CustomAttributeTypedArgument>::System.Collections.ICollection.get_SyncRoot()
extern "C" Object_t * Collection_1_System_Collections_ICollection_get_SyncRoot_m1_6577_gshared (Collection_1_t1_1072 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_ICollection_get_SyncRoot_m1_6577(__this, method) (( Object_t * (*) (Collection_1_t1_1072 *, const MethodInfo*))Collection_1_System_Collections_ICollection_get_SyncRoot_m1_6577_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.Collection`1<System.Reflection.CustomAttributeTypedArgument>::System.Collections.IList.get_Item(System.Int32)
extern "C" Object_t * Collection_1_System_Collections_IList_get_Item_m1_6578_gshared (Collection_1_t1_1072 * __this, int32_t ___index, const MethodInfo* method);
#define Collection_1_System_Collections_IList_get_Item_m1_6578(__this, ___index, method) (( Object_t * (*) (Collection_1_t1_1072 *, int32_t, const MethodInfo*))Collection_1_System_Collections_IList_get_Item_m1_6578_gshared)(__this, ___index, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Reflection.CustomAttributeTypedArgument>::System.Collections.IList.set_Item(System.Int32,System.Object)
extern "C" void Collection_1_System_Collections_IList_set_Item_m1_6579_gshared (Collection_1_t1_1072 * __this, int32_t ___index, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_set_Item_m1_6579(__this, ___index, ___value, method) (( void (*) (Collection_1_t1_1072 *, int32_t, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_set_Item_m1_6579_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Reflection.CustomAttributeTypedArgument>::Add(T)
extern "C" void Collection_1_Add_m1_6580_gshared (Collection_1_t1_1072 * __this, CustomAttributeTypedArgument_t1_332  ___item, const MethodInfo* method);
#define Collection_1_Add_m1_6580(__this, ___item, method) (( void (*) (Collection_1_t1_1072 *, CustomAttributeTypedArgument_t1_332 , const MethodInfo*))Collection_1_Add_m1_6580_gshared)(__this, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Reflection.CustomAttributeTypedArgument>::Clear()
extern "C" void Collection_1_Clear_m1_6581_gshared (Collection_1_t1_1072 * __this, const MethodInfo* method);
#define Collection_1_Clear_m1_6581(__this, method) (( void (*) (Collection_1_t1_1072 *, const MethodInfo*))Collection_1_Clear_m1_6581_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Reflection.CustomAttributeTypedArgument>::ClearItems()
extern "C" void Collection_1_ClearItems_m1_6582_gshared (Collection_1_t1_1072 * __this, const MethodInfo* method);
#define Collection_1_ClearItems_m1_6582(__this, method) (( void (*) (Collection_1_t1_1072 *, const MethodInfo*))Collection_1_ClearItems_m1_6582_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<System.Reflection.CustomAttributeTypedArgument>::Contains(T)
extern "C" bool Collection_1_Contains_m1_6583_gshared (Collection_1_t1_1072 * __this, CustomAttributeTypedArgument_t1_332  ___item, const MethodInfo* method);
#define Collection_1_Contains_m1_6583(__this, ___item, method) (( bool (*) (Collection_1_t1_1072 *, CustomAttributeTypedArgument_t1_332 , const MethodInfo*))Collection_1_Contains_m1_6583_gshared)(__this, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Reflection.CustomAttributeTypedArgument>::CopyTo(T[],System.Int32)
extern "C" void Collection_1_CopyTo_m1_6584_gshared (Collection_1_t1_1072 * __this, CustomAttributeTypedArgumentU5BU5D_t1_838* ___array, int32_t ___index, const MethodInfo* method);
#define Collection_1_CopyTo_m1_6584(__this, ___array, ___index, method) (( void (*) (Collection_1_t1_1072 *, CustomAttributeTypedArgumentU5BU5D_t1_838*, int32_t, const MethodInfo*))Collection_1_CopyTo_m1_6584_gshared)(__this, ___array, ___index, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.ObjectModel.Collection`1<System.Reflection.CustomAttributeTypedArgument>::GetEnumerator()
extern "C" Object_t* Collection_1_GetEnumerator_m1_6585_gshared (Collection_1_t1_1072 * __this, const MethodInfo* method);
#define Collection_1_GetEnumerator_m1_6585(__this, method) (( Object_t* (*) (Collection_1_t1_1072 *, const MethodInfo*))Collection_1_GetEnumerator_m1_6585_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<System.Reflection.CustomAttributeTypedArgument>::IndexOf(T)
extern "C" int32_t Collection_1_IndexOf_m1_6586_gshared (Collection_1_t1_1072 * __this, CustomAttributeTypedArgument_t1_332  ___item, const MethodInfo* method);
#define Collection_1_IndexOf_m1_6586(__this, ___item, method) (( int32_t (*) (Collection_1_t1_1072 *, CustomAttributeTypedArgument_t1_332 , const MethodInfo*))Collection_1_IndexOf_m1_6586_gshared)(__this, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Reflection.CustomAttributeTypedArgument>::Insert(System.Int32,T)
extern "C" void Collection_1_Insert_m1_6587_gshared (Collection_1_t1_1072 * __this, int32_t ___index, CustomAttributeTypedArgument_t1_332  ___item, const MethodInfo* method);
#define Collection_1_Insert_m1_6587(__this, ___index, ___item, method) (( void (*) (Collection_1_t1_1072 *, int32_t, CustomAttributeTypedArgument_t1_332 , const MethodInfo*))Collection_1_Insert_m1_6587_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Reflection.CustomAttributeTypedArgument>::InsertItem(System.Int32,T)
extern "C" void Collection_1_InsertItem_m1_6588_gshared (Collection_1_t1_1072 * __this, int32_t ___index, CustomAttributeTypedArgument_t1_332  ___item, const MethodInfo* method);
#define Collection_1_InsertItem_m1_6588(__this, ___index, ___item, method) (( void (*) (Collection_1_t1_1072 *, int32_t, CustomAttributeTypedArgument_t1_332 , const MethodInfo*))Collection_1_InsertItem_m1_6588_gshared)(__this, ___index, ___item, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<System.Reflection.CustomAttributeTypedArgument>::Remove(T)
extern "C" bool Collection_1_Remove_m1_6589_gshared (Collection_1_t1_1072 * __this, CustomAttributeTypedArgument_t1_332  ___item, const MethodInfo* method);
#define Collection_1_Remove_m1_6589(__this, ___item, method) (( bool (*) (Collection_1_t1_1072 *, CustomAttributeTypedArgument_t1_332 , const MethodInfo*))Collection_1_Remove_m1_6589_gshared)(__this, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Reflection.CustomAttributeTypedArgument>::RemoveAt(System.Int32)
extern "C" void Collection_1_RemoveAt_m1_6590_gshared (Collection_1_t1_1072 * __this, int32_t ___index, const MethodInfo* method);
#define Collection_1_RemoveAt_m1_6590(__this, ___index, method) (( void (*) (Collection_1_t1_1072 *, int32_t, const MethodInfo*))Collection_1_RemoveAt_m1_6590_gshared)(__this, ___index, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Reflection.CustomAttributeTypedArgument>::RemoveItem(System.Int32)
extern "C" void Collection_1_RemoveItem_m1_6591_gshared (Collection_1_t1_1072 * __this, int32_t ___index, const MethodInfo* method);
#define Collection_1_RemoveItem_m1_6591(__this, ___index, method) (( void (*) (Collection_1_t1_1072 *, int32_t, const MethodInfo*))Collection_1_RemoveItem_m1_6591_gshared)(__this, ___index, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<System.Reflection.CustomAttributeTypedArgument>::get_Count()
extern "C" int32_t Collection_1_get_Count_m1_6592_gshared (Collection_1_t1_1072 * __this, const MethodInfo* method);
#define Collection_1_get_Count_m1_6592(__this, method) (( int32_t (*) (Collection_1_t1_1072 *, const MethodInfo*))Collection_1_get_Count_m1_6592_gshared)(__this, method)
// T System.Collections.ObjectModel.Collection`1<System.Reflection.CustomAttributeTypedArgument>::get_Item(System.Int32)
extern "C" CustomAttributeTypedArgument_t1_332  Collection_1_get_Item_m1_6593_gshared (Collection_1_t1_1072 * __this, int32_t ___index, const MethodInfo* method);
#define Collection_1_get_Item_m1_6593(__this, ___index, method) (( CustomAttributeTypedArgument_t1_332  (*) (Collection_1_t1_1072 *, int32_t, const MethodInfo*))Collection_1_get_Item_m1_6593_gshared)(__this, ___index, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Reflection.CustomAttributeTypedArgument>::set_Item(System.Int32,T)
extern "C" void Collection_1_set_Item_m1_6594_gshared (Collection_1_t1_1072 * __this, int32_t ___index, CustomAttributeTypedArgument_t1_332  ___value, const MethodInfo* method);
#define Collection_1_set_Item_m1_6594(__this, ___index, ___value, method) (( void (*) (Collection_1_t1_1072 *, int32_t, CustomAttributeTypedArgument_t1_332 , const MethodInfo*))Collection_1_set_Item_m1_6594_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Reflection.CustomAttributeTypedArgument>::SetItem(System.Int32,T)
extern "C" void Collection_1_SetItem_m1_6595_gshared (Collection_1_t1_1072 * __this, int32_t ___index, CustomAttributeTypedArgument_t1_332  ___item, const MethodInfo* method);
#define Collection_1_SetItem_m1_6595(__this, ___index, ___item, method) (( void (*) (Collection_1_t1_1072 *, int32_t, CustomAttributeTypedArgument_t1_332 , const MethodInfo*))Collection_1_SetItem_m1_6595_gshared)(__this, ___index, ___item, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<System.Reflection.CustomAttributeTypedArgument>::IsValidItem(System.Object)
extern "C" bool Collection_1_IsValidItem_m1_6596_gshared (Object_t * __this /* static, unused */, Object_t * ___item, const MethodInfo* method);
#define Collection_1_IsValidItem_m1_6596(__this /* static, unused */, ___item, method) (( bool (*) (Object_t * /* static, unused */, Object_t *, const MethodInfo*))Collection_1_IsValidItem_m1_6596_gshared)(__this /* static, unused */, ___item, method)
// T System.Collections.ObjectModel.Collection`1<System.Reflection.CustomAttributeTypedArgument>::ConvertItem(System.Object)
extern "C" CustomAttributeTypedArgument_t1_332  Collection_1_ConvertItem_m1_6597_gshared (Object_t * __this /* static, unused */, Object_t * ___item, const MethodInfo* method);
#define Collection_1_ConvertItem_m1_6597(__this /* static, unused */, ___item, method) (( CustomAttributeTypedArgument_t1_332  (*) (Object_t * /* static, unused */, Object_t *, const MethodInfo*))Collection_1_ConvertItem_m1_6597_gshared)(__this /* static, unused */, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Reflection.CustomAttributeTypedArgument>::CheckWritable(System.Collections.Generic.IList`1<T>)
extern "C" void Collection_1_CheckWritable_m1_6598_gshared (Object_t * __this /* static, unused */, Object_t* ___list, const MethodInfo* method);
#define Collection_1_CheckWritable_m1_6598(__this /* static, unused */, ___list, method) (( void (*) (Object_t * /* static, unused */, Object_t*, const MethodInfo*))Collection_1_CheckWritable_m1_6598_gshared)(__this /* static, unused */, ___list, method)
