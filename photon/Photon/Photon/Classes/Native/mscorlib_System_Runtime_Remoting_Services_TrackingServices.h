﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Collections.ArrayList
struct ArrayList_t1_113;

#include "mscorlib_System_Object.h"

// System.Runtime.Remoting.Services.TrackingServices
struct  TrackingServices_t1_479  : public Object_t
{
};
struct TrackingServices_t1_479_StaticFields{
	// System.Collections.ArrayList System.Runtime.Remoting.Services.TrackingServices::_handlers
	ArrayList_t1_113 * ____handlers_0;
};
