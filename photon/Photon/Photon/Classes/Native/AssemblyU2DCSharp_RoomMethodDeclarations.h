﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Room
struct Room_t8_100;
// System.String
struct String_t;
// RoomOptions
struct RoomOptions_t8_78;
// System.String[]
struct StringU5BU5D_t1_202;
// ExitGames.Client.Photon.Hashtable
struct Hashtable_t5_1;

#include "codegen/il2cpp-codegen.h"

// System.Void Room::.ctor(System.String,RoomOptions)
extern "C" void Room__ctor_m8_807 (Room_t8_100 * __this, String_t* ___roomName, RoomOptions_t8_78 * ___options, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Room::get_playerCount()
extern "C" int32_t Room_get_playerCount_m8_808 (Room_t8_100 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Room::get_name()
extern "C" String_t* Room_get_name_m8_809 (Room_t8_100 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Room::set_name(System.String)
extern "C" void Room_set_name_m8_810 (Room_t8_100 * __this, String_t* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Room::get_maxPlayers()
extern "C" int32_t Room_get_maxPlayers_m8_811 (Room_t8_100 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Room::set_maxPlayers(System.Int32)
extern "C" void Room_set_maxPlayers_m8_812 (Room_t8_100 * __this, int32_t ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Room::get_open()
extern "C" bool Room_get_open_m8_813 (Room_t8_100 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Room::set_open(System.Boolean)
extern "C" void Room_set_open_m8_814 (Room_t8_100 * __this, bool ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Room::get_visible()
extern "C" bool Room_get_visible_m8_815 (Room_t8_100 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Room::set_visible(System.Boolean)
extern "C" void Room_set_visible_m8_816 (Room_t8_100 * __this, bool ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String[] Room::get_propertiesListedInLobby()
extern "C" StringU5BU5D_t1_202* Room_get_propertiesListedInLobby_m8_817 (Room_t8_100 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Room::set_propertiesListedInLobby(System.String[])
extern "C" void Room_set_propertiesListedInLobby_m8_818 (Room_t8_100 * __this, StringU5BU5D_t1_202* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Room::get_autoCleanUp()
extern "C" bool Room_get_autoCleanUp_m8_819 (Room_t8_100 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Room::get_masterClientId()
extern "C" int32_t Room_get_masterClientId_m8_820 (Room_t8_100 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Room::set_masterClientId(System.Int32)
extern "C" void Room_set_masterClientId_m8_821 (Room_t8_100 * __this, int32_t ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Room::SetCustomProperties(ExitGames.Client.Photon.Hashtable,ExitGames.Client.Photon.Hashtable,System.Boolean)
extern "C" void Room_SetCustomProperties_m8_822 (Room_t8_100 * __this, Hashtable_t5_1 * ___propertiesToSet, Hashtable_t5_1 * ___expectedValues, bool ___webForward, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Room::SetPropertiesListedInLobby(System.String[])
extern "C" void Room_SetPropertiesListedInLobby_m8_823 (Room_t8_100 * __this, StringU5BU5D_t1_202* ___propsListedInLobby, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Room::ToString()
extern "C" String_t* Room_ToString_m8_824 (Room_t8_100 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Room::ToStringFull()
extern "C" String_t* Room_ToStringFull_m8_825 (Room_t8_100 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
