﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2<System.Int32,System.Object>
struct Dictionary_2_t1_919;
// System.Collections.Generic.IEqualityComparer`1<System.Int32>
struct IEqualityComparer_1_t1_1157;
// System.Runtime.Serialization.SerializationInfo
struct SerializationInfo_t1_177;
// System.Collections.ICollection
struct ICollection_t1_811;
// System.Object
struct Object_t;
// System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>[]
struct KeyValuePair_2U5BU5D_t1_1423;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t1_130;
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>>
struct IEnumerator_1_t1_1424;
// System.Collections.IDictionaryEnumerator
struct IDictionaryEnumerator_t1_458;
// System.Collections.Generic.Dictionary`2/KeyCollection<System.Int32,System.Object>
struct KeyCollection_t1_1161;
// System.Collections.Generic.Dictionary`2/ValueCollection<System.Int32,System.Object>
struct ValueCollection_t1_1165;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Runtime_Serialization_StreamingContext.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_7.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Enumerator__7.h"
#include "mscorlib_System_Collections_DictionaryEntry.h"

// System.Void System.Collections.Generic.Dictionary`2<System.Int32,System.Object>::.ctor()
extern "C" void Dictionary_2__ctor_m1_5613_gshared (Dictionary_2_t1_919 * __this, const MethodInfo* method);
#define Dictionary_2__ctor_m1_5613(__this, method) (( void (*) (Dictionary_2_t1_919 *, const MethodInfo*))Dictionary_2__ctor_m1_5613_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,System.Object>::.ctor(System.Collections.Generic.IEqualityComparer`1<TKey>)
extern "C" void Dictionary_2__ctor_m1_7366_gshared (Dictionary_2_t1_919 * __this, Object_t* ___comparer, const MethodInfo* method);
#define Dictionary_2__ctor_m1_7366(__this, ___comparer, method) (( void (*) (Dictionary_2_t1_919 *, Object_t*, const MethodInfo*))Dictionary_2__ctor_m1_7366_gshared)(__this, ___comparer, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,System.Object>::.ctor(System.Int32)
extern "C" void Dictionary_2__ctor_m1_7367_gshared (Dictionary_2_t1_919 * __this, int32_t ___capacity, const MethodInfo* method);
#define Dictionary_2__ctor_m1_7367(__this, ___capacity, method) (( void (*) (Dictionary_2_t1_919 *, int32_t, const MethodInfo*))Dictionary_2__ctor_m1_7367_gshared)(__this, ___capacity, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,System.Object>::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C" void Dictionary_2__ctor_m1_7369_gshared (Dictionary_2_t1_919 * __this, SerializationInfo_t1_177 * ___info, StreamingContext_t1_514  ___context, const MethodInfo* method);
#define Dictionary_2__ctor_m1_7369(__this, ___info, ___context, method) (( void (*) (Dictionary_2_t1_919 *, SerializationInfo_t1_177 *, StreamingContext_t1_514 , const MethodInfo*))Dictionary_2__ctor_m1_7369_gshared)(__this, ___info, ___context, method)
// System.Collections.ICollection System.Collections.Generic.Dictionary`2<System.Int32,System.Object>::System.Collections.IDictionary.get_Keys()
extern "C" Object_t * Dictionary_2_System_Collections_IDictionary_get_Keys_m1_7371_gshared (Dictionary_2_t1_919 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_get_Keys_m1_7371(__this, method) (( Object_t * (*) (Dictionary_2_t1_919 *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_get_Keys_m1_7371_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2<System.Int32,System.Object>::System.Collections.IDictionary.get_Item(System.Object)
extern "C" Object_t * Dictionary_2_System_Collections_IDictionary_get_Item_m1_7373_gshared (Dictionary_2_t1_919 * __this, Object_t * ___key, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_get_Item_m1_7373(__this, ___key, method) (( Object_t * (*) (Dictionary_2_t1_919 *, Object_t *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_get_Item_m1_7373_gshared)(__this, ___key, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,System.Object>::System.Collections.IDictionary.set_Item(System.Object,System.Object)
extern "C" void Dictionary_2_System_Collections_IDictionary_set_Item_m1_7375_gshared (Dictionary_2_t1_919 * __this, Object_t * ___key, Object_t * ___value, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_set_Item_m1_7375(__this, ___key, ___value, method) (( void (*) (Dictionary_2_t1_919 *, Object_t *, Object_t *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_set_Item_m1_7375_gshared)(__this, ___key, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,System.Object>::System.Collections.IDictionary.Add(System.Object,System.Object)
extern "C" void Dictionary_2_System_Collections_IDictionary_Add_m1_7377_gshared (Dictionary_2_t1_919 * __this, Object_t * ___key, Object_t * ___value, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_Add_m1_7377(__this, ___key, ___value, method) (( void (*) (Dictionary_2_t1_919 *, Object_t *, Object_t *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Add_m1_7377_gshared)(__this, ___key, ___value, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Int32,System.Object>::System.Collections.IDictionary.Contains(System.Object)
extern "C" bool Dictionary_2_System_Collections_IDictionary_Contains_m1_7379_gshared (Dictionary_2_t1_919 * __this, Object_t * ___key, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_Contains_m1_7379(__this, ___key, method) (( bool (*) (Dictionary_2_t1_919 *, Object_t *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Contains_m1_7379_gshared)(__this, ___key, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,System.Object>::System.Collections.IDictionary.Remove(System.Object)
extern "C" void Dictionary_2_System_Collections_IDictionary_Remove_m1_7381_gshared (Dictionary_2_t1_919 * __this, Object_t * ___key, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_Remove_m1_7381(__this, ___key, method) (( void (*) (Dictionary_2_t1_919 *, Object_t *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Remove_m1_7381_gshared)(__this, ___key, method)
// System.Object System.Collections.Generic.Dictionary`2<System.Int32,System.Object>::System.Collections.ICollection.get_SyncRoot()
extern "C" Object_t * Dictionary_2_System_Collections_ICollection_get_SyncRoot_m1_7383_gshared (Dictionary_2_t1_919 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_ICollection_get_SyncRoot_m1_7383(__this, method) (( Object_t * (*) (Dictionary_2_t1_919 *, const MethodInfo*))Dictionary_2_System_Collections_ICollection_get_SyncRoot_m1_7383_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Int32,System.Object>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.get_IsReadOnly()
extern "C" bool Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m1_7385_gshared (Dictionary_2_t1_919 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m1_7385(__this, method) (( bool (*) (Dictionary_2_t1_919 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m1_7385_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,System.Object>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Add(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C" void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m1_7387_gshared (Dictionary_2_t1_919 * __this, KeyValuePair_2_t1_1159  ___keyValuePair, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m1_7387(__this, ___keyValuePair, method) (( void (*) (Dictionary_2_t1_919 *, KeyValuePair_2_t1_1159 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m1_7387_gshared)(__this, ___keyValuePair, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Int32,System.Object>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Contains(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C" bool Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m1_7389_gshared (Dictionary_2_t1_919 * __this, KeyValuePair_2_t1_1159  ___keyValuePair, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m1_7389(__this, ___keyValuePair, method) (( bool (*) (Dictionary_2_t1_919 *, KeyValuePair_2_t1_1159 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m1_7389_gshared)(__this, ___keyValuePair, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,System.Object>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.CopyTo(System.Collections.Generic.KeyValuePair`2<TKey,TValue>[],System.Int32)
extern "C" void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m1_7391_gshared (Dictionary_2_t1_919 * __this, KeyValuePair_2U5BU5D_t1_1423* ___array, int32_t ___index, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m1_7391(__this, ___array, ___index, method) (( void (*) (Dictionary_2_t1_919 *, KeyValuePair_2U5BU5D_t1_1423*, int32_t, const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m1_7391_gshared)(__this, ___array, ___index, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Int32,System.Object>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Remove(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C" bool Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m1_7393_gshared (Dictionary_2_t1_919 * __this, KeyValuePair_2_t1_1159  ___keyValuePair, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m1_7393(__this, ___keyValuePair, method) (( bool (*) (Dictionary_2_t1_919 *, KeyValuePair_2_t1_1159 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m1_7393_gshared)(__this, ___keyValuePair, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,System.Object>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C" void Dictionary_2_System_Collections_ICollection_CopyTo_m1_7395_gshared (Dictionary_2_t1_919 * __this, Array_t * ___array, int32_t ___index, const MethodInfo* method);
#define Dictionary_2_System_Collections_ICollection_CopyTo_m1_7395(__this, ___array, ___index, method) (( void (*) (Dictionary_2_t1_919 *, Array_t *, int32_t, const MethodInfo*))Dictionary_2_System_Collections_ICollection_CopyTo_m1_7395_gshared)(__this, ___array, ___index, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2<System.Int32,System.Object>::System.Collections.IEnumerable.GetEnumerator()
extern "C" Object_t * Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m1_7397_gshared (Dictionary_2_t1_919 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m1_7397(__this, method) (( Object_t * (*) (Dictionary_2_t1_919 *, const MethodInfo*))Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m1_7397_gshared)(__this, method)
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<TKey,TValue>> System.Collections.Generic.Dictionary`2<System.Int32,System.Object>::System.Collections.Generic.IEnumerable<System.Collections.Generic.KeyValuePair<TKey,TValue>>.GetEnumerator()
extern "C" Object_t* Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m1_7399_gshared (Dictionary_2_t1_919 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m1_7399(__this, method) (( Object_t* (*) (Dictionary_2_t1_919 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m1_7399_gshared)(__this, method)
// System.Collections.IDictionaryEnumerator System.Collections.Generic.Dictionary`2<System.Int32,System.Object>::System.Collections.IDictionary.GetEnumerator()
extern "C" Object_t * Dictionary_2_System_Collections_IDictionary_GetEnumerator_m1_7401_gshared (Dictionary_2_t1_919 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_GetEnumerator_m1_7401(__this, method) (( Object_t * (*) (Dictionary_2_t1_919 *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_GetEnumerator_m1_7401_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2<System.Int32,System.Object>::get_Count()
extern "C" int32_t Dictionary_2_get_Count_m1_7403_gshared (Dictionary_2_t1_919 * __this, const MethodInfo* method);
#define Dictionary_2_get_Count_m1_7403(__this, method) (( int32_t (*) (Dictionary_2_t1_919 *, const MethodInfo*))Dictionary_2_get_Count_m1_7403_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2<System.Int32,System.Object>::get_Item(TKey)
extern "C" Object_t * Dictionary_2_get_Item_m1_7405_gshared (Dictionary_2_t1_919 * __this, int32_t ___key, const MethodInfo* method);
#define Dictionary_2_get_Item_m1_7405(__this, ___key, method) (( Object_t * (*) (Dictionary_2_t1_919 *, int32_t, const MethodInfo*))Dictionary_2_get_Item_m1_7405_gshared)(__this, ___key, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,System.Object>::set_Item(TKey,TValue)
extern "C" void Dictionary_2_set_Item_m1_7407_gshared (Dictionary_2_t1_919 * __this, int32_t ___key, Object_t * ___value, const MethodInfo* method);
#define Dictionary_2_set_Item_m1_7407(__this, ___key, ___value, method) (( void (*) (Dictionary_2_t1_919 *, int32_t, Object_t *, const MethodInfo*))Dictionary_2_set_Item_m1_7407_gshared)(__this, ___key, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,System.Object>::Init(System.Int32,System.Collections.Generic.IEqualityComparer`1<TKey>)
extern "C" void Dictionary_2_Init_m1_7409_gshared (Dictionary_2_t1_919 * __this, int32_t ___capacity, Object_t* ___hcp, const MethodInfo* method);
#define Dictionary_2_Init_m1_7409(__this, ___capacity, ___hcp, method) (( void (*) (Dictionary_2_t1_919 *, int32_t, Object_t*, const MethodInfo*))Dictionary_2_Init_m1_7409_gshared)(__this, ___capacity, ___hcp, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,System.Object>::InitArrays(System.Int32)
extern "C" void Dictionary_2_InitArrays_m1_7411_gshared (Dictionary_2_t1_919 * __this, int32_t ___size, const MethodInfo* method);
#define Dictionary_2_InitArrays_m1_7411(__this, ___size, method) (( void (*) (Dictionary_2_t1_919 *, int32_t, const MethodInfo*))Dictionary_2_InitArrays_m1_7411_gshared)(__this, ___size, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,System.Object>::CopyToCheck(System.Array,System.Int32)
extern "C" void Dictionary_2_CopyToCheck_m1_7413_gshared (Dictionary_2_t1_919 * __this, Array_t * ___array, int32_t ___index, const MethodInfo* method);
#define Dictionary_2_CopyToCheck_m1_7413(__this, ___array, ___index, method) (( void (*) (Dictionary_2_t1_919 *, Array_t *, int32_t, const MethodInfo*))Dictionary_2_CopyToCheck_m1_7413_gshared)(__this, ___array, ___index, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2<System.Int32,System.Object>::make_pair(TKey,TValue)
extern "C" KeyValuePair_2_t1_1159  Dictionary_2_make_pair_m1_7415_gshared (Object_t * __this /* static, unused */, int32_t ___key, Object_t * ___value, const MethodInfo* method);
#define Dictionary_2_make_pair_m1_7415(__this /* static, unused */, ___key, ___value, method) (( KeyValuePair_2_t1_1159  (*) (Object_t * /* static, unused */, int32_t, Object_t *, const MethodInfo*))Dictionary_2_make_pair_m1_7415_gshared)(__this /* static, unused */, ___key, ___value, method)
// TKey System.Collections.Generic.Dictionary`2<System.Int32,System.Object>::pick_key(TKey,TValue)
extern "C" int32_t Dictionary_2_pick_key_m1_7417_gshared (Object_t * __this /* static, unused */, int32_t ___key, Object_t * ___value, const MethodInfo* method);
#define Dictionary_2_pick_key_m1_7417(__this /* static, unused */, ___key, ___value, method) (( int32_t (*) (Object_t * /* static, unused */, int32_t, Object_t *, const MethodInfo*))Dictionary_2_pick_key_m1_7417_gshared)(__this /* static, unused */, ___key, ___value, method)
// TValue System.Collections.Generic.Dictionary`2<System.Int32,System.Object>::pick_value(TKey,TValue)
extern "C" Object_t * Dictionary_2_pick_value_m1_7419_gshared (Object_t * __this /* static, unused */, int32_t ___key, Object_t * ___value, const MethodInfo* method);
#define Dictionary_2_pick_value_m1_7419(__this /* static, unused */, ___key, ___value, method) (( Object_t * (*) (Object_t * /* static, unused */, int32_t, Object_t *, const MethodInfo*))Dictionary_2_pick_value_m1_7419_gshared)(__this /* static, unused */, ___key, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,System.Object>::CopyTo(System.Collections.Generic.KeyValuePair`2<TKey,TValue>[],System.Int32)
extern "C" void Dictionary_2_CopyTo_m1_7421_gshared (Dictionary_2_t1_919 * __this, KeyValuePair_2U5BU5D_t1_1423* ___array, int32_t ___index, const MethodInfo* method);
#define Dictionary_2_CopyTo_m1_7421(__this, ___array, ___index, method) (( void (*) (Dictionary_2_t1_919 *, KeyValuePair_2U5BU5D_t1_1423*, int32_t, const MethodInfo*))Dictionary_2_CopyTo_m1_7421_gshared)(__this, ___array, ___index, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,System.Object>::Resize()
extern "C" void Dictionary_2_Resize_m1_7423_gshared (Dictionary_2_t1_919 * __this, const MethodInfo* method);
#define Dictionary_2_Resize_m1_7423(__this, method) (( void (*) (Dictionary_2_t1_919 *, const MethodInfo*))Dictionary_2_Resize_m1_7423_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,System.Object>::Add(TKey,TValue)
extern "C" void Dictionary_2_Add_m1_7425_gshared (Dictionary_2_t1_919 * __this, int32_t ___key, Object_t * ___value, const MethodInfo* method);
#define Dictionary_2_Add_m1_7425(__this, ___key, ___value, method) (( void (*) (Dictionary_2_t1_919 *, int32_t, Object_t *, const MethodInfo*))Dictionary_2_Add_m1_7425_gshared)(__this, ___key, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,System.Object>::Clear()
extern "C" void Dictionary_2_Clear_m1_7427_gshared (Dictionary_2_t1_919 * __this, const MethodInfo* method);
#define Dictionary_2_Clear_m1_7427(__this, method) (( void (*) (Dictionary_2_t1_919 *, const MethodInfo*))Dictionary_2_Clear_m1_7427_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Int32,System.Object>::ContainsKey(TKey)
extern "C" bool Dictionary_2_ContainsKey_m1_7429_gshared (Dictionary_2_t1_919 * __this, int32_t ___key, const MethodInfo* method);
#define Dictionary_2_ContainsKey_m1_7429(__this, ___key, method) (( bool (*) (Dictionary_2_t1_919 *, int32_t, const MethodInfo*))Dictionary_2_ContainsKey_m1_7429_gshared)(__this, ___key, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Int32,System.Object>::ContainsValue(TValue)
extern "C" bool Dictionary_2_ContainsValue_m1_7431_gshared (Dictionary_2_t1_919 * __this, Object_t * ___value, const MethodInfo* method);
#define Dictionary_2_ContainsValue_m1_7431(__this, ___value, method) (( bool (*) (Dictionary_2_t1_919 *, Object_t *, const MethodInfo*))Dictionary_2_ContainsValue_m1_7431_gshared)(__this, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,System.Object>::GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C" void Dictionary_2_GetObjectData_m1_7433_gshared (Dictionary_2_t1_919 * __this, SerializationInfo_t1_177 * ___info, StreamingContext_t1_514  ___context, const MethodInfo* method);
#define Dictionary_2_GetObjectData_m1_7433(__this, ___info, ___context, method) (( void (*) (Dictionary_2_t1_919 *, SerializationInfo_t1_177 *, StreamingContext_t1_514 , const MethodInfo*))Dictionary_2_GetObjectData_m1_7433_gshared)(__this, ___info, ___context, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,System.Object>::OnDeserialization(System.Object)
extern "C" void Dictionary_2_OnDeserialization_m1_7435_gshared (Dictionary_2_t1_919 * __this, Object_t * ___sender, const MethodInfo* method);
#define Dictionary_2_OnDeserialization_m1_7435(__this, ___sender, method) (( void (*) (Dictionary_2_t1_919 *, Object_t *, const MethodInfo*))Dictionary_2_OnDeserialization_m1_7435_gshared)(__this, ___sender, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Int32,System.Object>::Remove(TKey)
extern "C" bool Dictionary_2_Remove_m1_7437_gshared (Dictionary_2_t1_919 * __this, int32_t ___key, const MethodInfo* method);
#define Dictionary_2_Remove_m1_7437(__this, ___key, method) (( bool (*) (Dictionary_2_t1_919 *, int32_t, const MethodInfo*))Dictionary_2_Remove_m1_7437_gshared)(__this, ___key, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Int32,System.Object>::TryGetValue(TKey,TValue&)
extern "C" bool Dictionary_2_TryGetValue_m1_7439_gshared (Dictionary_2_t1_919 * __this, int32_t ___key, Object_t ** ___value, const MethodInfo* method);
#define Dictionary_2_TryGetValue_m1_7439(__this, ___key, ___value, method) (( bool (*) (Dictionary_2_t1_919 *, int32_t, Object_t **, const MethodInfo*))Dictionary_2_TryGetValue_m1_7439_gshared)(__this, ___key, ___value, method)
// System.Collections.Generic.Dictionary`2/KeyCollection<TKey,TValue> System.Collections.Generic.Dictionary`2<System.Int32,System.Object>::get_Keys()
extern "C" KeyCollection_t1_1161 * Dictionary_2_get_Keys_m1_7440_gshared (Dictionary_2_t1_919 * __this, const MethodInfo* method);
#define Dictionary_2_get_Keys_m1_7440(__this, method) (( KeyCollection_t1_1161 * (*) (Dictionary_2_t1_919 *, const MethodInfo*))Dictionary_2_get_Keys_m1_7440_gshared)(__this, method)
// System.Collections.Generic.Dictionary`2/ValueCollection<TKey,TValue> System.Collections.Generic.Dictionary`2<System.Int32,System.Object>::get_Values()
extern "C" ValueCollection_t1_1165 * Dictionary_2_get_Values_m1_7442_gshared (Dictionary_2_t1_919 * __this, const MethodInfo* method);
#define Dictionary_2_get_Values_m1_7442(__this, method) (( ValueCollection_t1_1165 * (*) (Dictionary_2_t1_919 *, const MethodInfo*))Dictionary_2_get_Values_m1_7442_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2<System.Int32,System.Object>::ToTKey(System.Object)
extern "C" int32_t Dictionary_2_ToTKey_m1_7444_gshared (Dictionary_2_t1_919 * __this, Object_t * ___key, const MethodInfo* method);
#define Dictionary_2_ToTKey_m1_7444(__this, ___key, method) (( int32_t (*) (Dictionary_2_t1_919 *, Object_t *, const MethodInfo*))Dictionary_2_ToTKey_m1_7444_gshared)(__this, ___key, method)
// TValue System.Collections.Generic.Dictionary`2<System.Int32,System.Object>::ToTValue(System.Object)
extern "C" Object_t * Dictionary_2_ToTValue_m1_7446_gshared (Dictionary_2_t1_919 * __this, Object_t * ___value, const MethodInfo* method);
#define Dictionary_2_ToTValue_m1_7446(__this, ___value, method) (( Object_t * (*) (Dictionary_2_t1_919 *, Object_t *, const MethodInfo*))Dictionary_2_ToTValue_m1_7446_gshared)(__this, ___value, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Int32,System.Object>::ContainsKeyValuePair(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C" bool Dictionary_2_ContainsKeyValuePair_m1_7448_gshared (Dictionary_2_t1_919 * __this, KeyValuePair_2_t1_1159  ___pair, const MethodInfo* method);
#define Dictionary_2_ContainsKeyValuePair_m1_7448(__this, ___pair, method) (( bool (*) (Dictionary_2_t1_919 *, KeyValuePair_2_t1_1159 , const MethodInfo*))Dictionary_2_ContainsKeyValuePair_m1_7448_gshared)(__this, ___pair, method)
// System.Collections.Generic.Dictionary`2/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2<System.Int32,System.Object>::GetEnumerator()
extern "C" Enumerator_t1_1163  Dictionary_2_GetEnumerator_m1_7450_gshared (Dictionary_2_t1_919 * __this, const MethodInfo* method);
#define Dictionary_2_GetEnumerator_m1_7450(__this, method) (( Enumerator_t1_1163  (*) (Dictionary_2_t1_919 *, const MethodInfo*))Dictionary_2_GetEnumerator_m1_7450_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2<System.Int32,System.Object>::<CopyTo>m__0(TKey,TValue)
extern "C" DictionaryEntry_t1_167  Dictionary_2_U3CCopyToU3Em__0_m1_7452_gshared (Object_t * __this /* static, unused */, int32_t ___key, Object_t * ___value, const MethodInfo* method);
#define Dictionary_2_U3CCopyToU3Em__0_m1_7452(__this /* static, unused */, ___key, ___value, method) (( DictionaryEntry_t1_167  (*) (Object_t * /* static, unused */, int32_t, Object_t *, const MethodInfo*))Dictionary_2_U3CCopyToU3Em__0_m1_7452_gshared)(__this /* static, unused */, ___key, ___value, method)
