﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.Events.UnityEvent`1<System.Object>
struct UnityEvent_1_t6_285;

#include "codegen/il2cpp-codegen.h"

// System.Void UnityEngine.Events.UnityEvent`1<System.Object>::.ctor()
extern "C" void UnityEvent_1__ctor_m6_1705_gshared (UnityEvent_1_t6_285 * __this, const MethodInfo* method);
#define UnityEvent_1__ctor_m6_1705(__this, method) (( void (*) (UnityEvent_1_t6_285 *, const MethodInfo*))UnityEvent_1__ctor_m6_1705_gshared)(__this, method)
