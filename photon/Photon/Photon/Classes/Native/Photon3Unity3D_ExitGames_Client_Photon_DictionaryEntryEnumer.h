﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Collections.IDictionaryEnumerator
struct IDictionaryEnumerator_t1_458;

#include "mscorlib_System_Object.h"

// ExitGames.Client.Photon.DictionaryEntryEnumerator
struct  DictionaryEntryEnumerator_t5_2  : public Object_t
{
	// System.Collections.IDictionaryEnumerator ExitGames.Client.Photon.DictionaryEntryEnumerator::enumerator
	Object_t * ___enumerator_0;
};
