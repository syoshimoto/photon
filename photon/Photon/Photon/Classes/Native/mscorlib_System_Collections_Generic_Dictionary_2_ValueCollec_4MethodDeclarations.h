﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_ValueCollec_15MethodDeclarations.h"

// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<System.Int32,PhotonView>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
#define ValueCollection__ctor_m1_10285(__this, ___dictionary, method) (( void (*) (ValueCollection_t1_959 *, Dictionary_2_t1_937 *, const MethodInfo*))ValueCollection__ctor_m1_7502_gshared)(__this, ___dictionary, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<System.Int32,PhotonView>::System.Collections.Generic.ICollection<TValue>.Add(TValue)
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m1_10286(__this, ___item, method) (( void (*) (ValueCollection_t1_959 *, PhotonView_t8_3 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m1_7503_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<System.Int32,PhotonView>::System.Collections.Generic.ICollection<TValue>.Clear()
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m1_10287(__this, method) (( void (*) (ValueCollection_t1_959 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m1_7504_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<System.Int32,PhotonView>::System.Collections.Generic.ICollection<TValue>.Contains(TValue)
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m1_10288(__this, ___item, method) (( bool (*) (ValueCollection_t1_959 *, PhotonView_t8_3 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m1_7505_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<System.Int32,PhotonView>::System.Collections.Generic.ICollection<TValue>.Remove(TValue)
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m1_10289(__this, ___item, method) (( bool (*) (ValueCollection_t1_959 *, PhotonView_t8_3 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m1_7506_gshared)(__this, ___item, method)
// System.Collections.Generic.IEnumerator`1<TValue> System.Collections.Generic.Dictionary`2/ValueCollection<System.Int32,PhotonView>::System.Collections.Generic.IEnumerable<TValue>.GetEnumerator()
#define ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m1_10290(__this, method) (( Object_t* (*) (ValueCollection_t1_959 *, const MethodInfo*))ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m1_7507_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<System.Int32,PhotonView>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define ValueCollection_System_Collections_ICollection_CopyTo_m1_10291(__this, ___array, ___index, method) (( void (*) (ValueCollection_t1_959 *, Array_t *, int32_t, const MethodInfo*))ValueCollection_System_Collections_ICollection_CopyTo_m1_7508_gshared)(__this, ___array, ___index, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2/ValueCollection<System.Int32,PhotonView>::System.Collections.IEnumerable.GetEnumerator()
#define ValueCollection_System_Collections_IEnumerable_GetEnumerator_m1_10292(__this, method) (( Object_t * (*) (ValueCollection_t1_959 *, const MethodInfo*))ValueCollection_System_Collections_IEnumerable_GetEnumerator_m1_7509_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<System.Int32,PhotonView>::System.Collections.Generic.ICollection<TValue>.get_IsReadOnly()
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m1_10293(__this, method) (( bool (*) (ValueCollection_t1_959 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m1_7510_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ValueCollection<System.Int32,PhotonView>::System.Collections.ICollection.get_SyncRoot()
#define ValueCollection_System_Collections_ICollection_get_SyncRoot_m1_10294(__this, method) (( Object_t * (*) (ValueCollection_t1_959 *, const MethodInfo*))ValueCollection_System_Collections_ICollection_get_SyncRoot_m1_7511_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<System.Int32,PhotonView>::CopyTo(TValue[],System.Int32)
#define ValueCollection_CopyTo_m1_10295(__this, ___array, ___index, method) (( void (*) (ValueCollection_t1_959 *, PhotonViewU5BU5D_t8_180*, int32_t, const MethodInfo*))ValueCollection_CopyTo_m1_7512_gshared)(__this, ___array, ___index, method)
// System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2/ValueCollection<System.Int32,PhotonView>::GetEnumerator()
#define ValueCollection_GetEnumerator_m1_5644(__this, method) (( Enumerator_t1_958  (*) (ValueCollection_t1_959 *, const MethodInfo*))ValueCollection_GetEnumerator_m1_7513_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2/ValueCollection<System.Int32,PhotonView>::get_Count()
#define ValueCollection_get_Count_m1_10296(__this, method) (( int32_t (*) (ValueCollection_t1_959 *, const MethodInfo*))ValueCollection_get_Count_m1_7514_gshared)(__this, method)
