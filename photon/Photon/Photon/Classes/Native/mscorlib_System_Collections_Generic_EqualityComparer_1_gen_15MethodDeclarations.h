﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.EqualityComparer`1<System.Single>
struct EqualityComparer_1_t1_1397;
// System.Object
struct Object_t;

#include "codegen/il2cpp-codegen.h"

// System.Void System.Collections.Generic.EqualityComparer`1<System.Single>::.ctor()
extern "C" void EqualityComparer_1__ctor_m1_11148_gshared (EqualityComparer_1_t1_1397 * __this, const MethodInfo* method);
#define EqualityComparer_1__ctor_m1_11148(__this, method) (( void (*) (EqualityComparer_1_t1_1397 *, const MethodInfo*))EqualityComparer_1__ctor_m1_11148_gshared)(__this, method)
// System.Void System.Collections.Generic.EqualityComparer`1<System.Single>::.cctor()
extern "C" void EqualityComparer_1__cctor_m1_11149_gshared (Object_t * __this /* static, unused */, const MethodInfo* method);
#define EqualityComparer_1__cctor_m1_11149(__this /* static, unused */, method) (( void (*) (Object_t * /* static, unused */, const MethodInfo*))EqualityComparer_1__cctor_m1_11149_gshared)(__this /* static, unused */, method)
// System.Int32 System.Collections.Generic.EqualityComparer`1<System.Single>::System.Collections.IEqualityComparer.GetHashCode(System.Object)
extern "C" int32_t EqualityComparer_1_System_Collections_IEqualityComparer_GetHashCode_m1_11150_gshared (EqualityComparer_1_t1_1397 * __this, Object_t * ___obj, const MethodInfo* method);
#define EqualityComparer_1_System_Collections_IEqualityComparer_GetHashCode_m1_11150(__this, ___obj, method) (( int32_t (*) (EqualityComparer_1_t1_1397 *, Object_t *, const MethodInfo*))EqualityComparer_1_System_Collections_IEqualityComparer_GetHashCode_m1_11150_gshared)(__this, ___obj, method)
// System.Boolean System.Collections.Generic.EqualityComparer`1<System.Single>::System.Collections.IEqualityComparer.Equals(System.Object,System.Object)
extern "C" bool EqualityComparer_1_System_Collections_IEqualityComparer_Equals_m1_11151_gshared (EqualityComparer_1_t1_1397 * __this, Object_t * ___x, Object_t * ___y, const MethodInfo* method);
#define EqualityComparer_1_System_Collections_IEqualityComparer_Equals_m1_11151(__this, ___x, ___y, method) (( bool (*) (EqualityComparer_1_t1_1397 *, Object_t *, Object_t *, const MethodInfo*))EqualityComparer_1_System_Collections_IEqualityComparer_Equals_m1_11151_gshared)(__this, ___x, ___y, method)
// System.Collections.Generic.EqualityComparer`1<T> System.Collections.Generic.EqualityComparer`1<System.Single>::get_Default()
extern "C" EqualityComparer_1_t1_1397 * EqualityComparer_1_get_Default_m1_11152_gshared (Object_t * __this /* static, unused */, const MethodInfo* method);
#define EqualityComparer_1_get_Default_m1_11152(__this /* static, unused */, method) (( EqualityComparer_1_t1_1397 * (*) (Object_t * /* static, unused */, const MethodInfo*))EqualityComparer_1_get_Default_m1_11152_gshared)(__this /* static, unused */, method)
