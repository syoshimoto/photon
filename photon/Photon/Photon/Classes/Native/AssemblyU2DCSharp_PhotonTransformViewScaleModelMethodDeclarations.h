﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PhotonTransformViewScaleModel
struct PhotonTransformViewScaleModel_t8_140;

#include "codegen/il2cpp-codegen.h"

// System.Void PhotonTransformViewScaleModel::.ctor()
extern "C" void PhotonTransformViewScaleModel__ctor_m8_919 (PhotonTransformViewScaleModel_t8_140 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
