﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PhotonTransformViewRotationControl
struct PhotonTransformViewRotationControl_t8_142;
// PhotonTransformViewRotationModel
struct PhotonTransformViewRotationModel_t8_139;
// PhotonStream
struct PhotonStream_t8_106;
// PhotonMessageInfo
struct PhotonMessageInfo_t8_104;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Quaternion.h"

// System.Void PhotonTransformViewRotationControl::.ctor(PhotonTransformViewRotationModel)
extern "C" void PhotonTransformViewRotationControl__ctor_m8_912 (PhotonTransformViewRotationControl_t8_142 * __this, PhotonTransformViewRotationModel_t8_139 * ___model, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Quaternion PhotonTransformViewRotationControl::GetRotation(UnityEngine.Quaternion)
extern "C" Quaternion_t6_51  PhotonTransformViewRotationControl_GetRotation_m8_913 (PhotonTransformViewRotationControl_t8_142 * __this, Quaternion_t6_51  ___currentRotation, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PhotonTransformViewRotationControl::OnPhotonSerializeView(UnityEngine.Quaternion,PhotonStream,PhotonMessageInfo)
extern "C" void PhotonTransformViewRotationControl_OnPhotonSerializeView_m8_914 (PhotonTransformViewRotationControl_t8_142 * __this, Quaternion_t6_51  ___currentRotation, PhotonStream_t8_106 * ___stream, PhotonMessageInfo_t8_104 * ___info, const MethodInfo* method) IL2CPP_METHOD_ATTR;
