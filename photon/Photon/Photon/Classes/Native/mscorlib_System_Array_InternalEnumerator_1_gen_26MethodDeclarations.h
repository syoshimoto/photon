﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array_InternalEnumerator_1_genMethodDeclarations.h"

// System.Void System.Array/InternalEnumerator`1<Mono.Globalization.Unicode.Level2Map>::.ctor(System.Array)
#define InternalEnumerator_1__ctor_m1_6284(__this, ___array, method) (( void (*) (InternalEnumerator_1_t1_1037 *, Array_t *, const MethodInfo*))InternalEnumerator_1__ctor_m1_5687_gshared)(__this, ___array, method)
// System.Void System.Array/InternalEnumerator`1<Mono.Globalization.Unicode.Level2Map>::System.Collections.IEnumerator.Reset()
#define InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1_6285(__this, method) (( void (*) (InternalEnumerator_1_t1_1037 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1_5688_gshared)(__this, method)
// System.Object System.Array/InternalEnumerator`1<Mono.Globalization.Unicode.Level2Map>::System.Collections.IEnumerator.get_Current()
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1_6286(__this, method) (( Object_t * (*) (InternalEnumerator_1_t1_1037 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1_5689_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<Mono.Globalization.Unicode.Level2Map>::Dispose()
#define InternalEnumerator_1_Dispose_m1_6287(__this, method) (( void (*) (InternalEnumerator_1_t1_1037 *, const MethodInfo*))InternalEnumerator_1_Dispose_m1_5690_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<Mono.Globalization.Unicode.Level2Map>::MoveNext()
#define InternalEnumerator_1_MoveNext_m1_6288(__this, method) (( bool (*) (InternalEnumerator_1_t1_1037 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m1_5691_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<Mono.Globalization.Unicode.Level2Map>::get_Current()
#define InternalEnumerator_1_get_Current_m1_6289(__this, method) (( Level2Map_t1_73 * (*) (InternalEnumerator_1_t1_1037 *, const MethodInfo*))InternalEnumerator_1_get_Current_m1_5692_gshared)(__this, method)
