﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2<PunTeams/Team,System.Object>
struct Dictionary_2_t1_1316;
// System.Object
struct Object_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_KeyCollecti_28.h"
#include "AssemblyU2DCSharp_PunTeams_Team.h"

// System.Void System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<PunTeams/Team,System.Object>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C" void Enumerator__ctor_m1_9606_gshared (Enumerator_t1_1314 * __this, Dictionary_2_t1_1316 * ___host, const MethodInfo* method);
#define Enumerator__ctor_m1_9606(__this, ___host, method) (( void (*) (Enumerator_t1_1314 *, Dictionary_2_t1_1316 *, const MethodInfo*))Enumerator__ctor_m1_9606_gshared)(__this, ___host, method)
// System.Object System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<PunTeams/Team,System.Object>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * Enumerator_System_Collections_IEnumerator_get_Current_m1_9608_gshared (Enumerator_t1_1314 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m1_9608(__this, method) (( Object_t * (*) (Enumerator_t1_1314 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m1_9608_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<PunTeams/Team,System.Object>::System.Collections.IEnumerator.Reset()
extern "C" void Enumerator_System_Collections_IEnumerator_Reset_m1_9610_gshared (Enumerator_t1_1314 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_Reset_m1_9610(__this, method) (( void (*) (Enumerator_t1_1314 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m1_9610_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<PunTeams/Team,System.Object>::Dispose()
extern "C" void Enumerator_Dispose_m1_9612_gshared (Enumerator_t1_1314 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m1_9612(__this, method) (( void (*) (Enumerator_t1_1314 *, const MethodInfo*))Enumerator_Dispose_m1_9612_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<PunTeams/Team,System.Object>::MoveNext()
extern "C" bool Enumerator_MoveNext_m1_9613_gshared (Enumerator_t1_1314 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m1_9613(__this, method) (( bool (*) (Enumerator_t1_1314 *, const MethodInfo*))Enumerator_MoveNext_m1_9613_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<PunTeams/Team,System.Object>::get_Current()
extern "C" uint8_t Enumerator_get_Current_m1_9614_gshared (Enumerator_t1_1314 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m1_9614(__this, method) (( uint8_t (*) (Enumerator_t1_1314 *, const MethodInfo*))Enumerator_get_Current_m1_9614_gshared)(__this, method)
