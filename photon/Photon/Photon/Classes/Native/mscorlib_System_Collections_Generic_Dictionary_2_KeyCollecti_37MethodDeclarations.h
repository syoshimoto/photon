﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_KeyCollecti_0MethodDeclarations.h"

// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<UnityEngine.Component,System.Reflection.MethodInfo>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
#define KeyCollection__ctor_m1_11007(__this, ___dictionary, method) (( void (*) (KeyCollection_t1_1391 *, Dictionary_2_t1_948 *, const MethodInfo*))KeyCollection__ctor_m1_6357_gshared)(__this, ___dictionary, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<UnityEngine.Component,System.Reflection.MethodInfo>::System.Collections.Generic.ICollection<TKey>.Add(TKey)
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m1_11008(__this, ___item, method) (( void (*) (KeyCollection_t1_1391 *, Component_t6_26 *, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m1_6358_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<UnityEngine.Component,System.Reflection.MethodInfo>::System.Collections.Generic.ICollection<TKey>.Clear()
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m1_11009(__this, method) (( void (*) (KeyCollection_t1_1391 *, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m1_6359_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<UnityEngine.Component,System.Reflection.MethodInfo>::System.Collections.Generic.ICollection<TKey>.Contains(TKey)
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m1_11010(__this, ___item, method) (( bool (*) (KeyCollection_t1_1391 *, Component_t6_26 *, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m1_6360_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<UnityEngine.Component,System.Reflection.MethodInfo>::System.Collections.Generic.ICollection<TKey>.Remove(TKey)
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m1_11011(__this, ___item, method) (( bool (*) (KeyCollection_t1_1391 *, Component_t6_26 *, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m1_6361_gshared)(__this, ___item, method)
// System.Collections.Generic.IEnumerator`1<TKey> System.Collections.Generic.Dictionary`2/KeyCollection<UnityEngine.Component,System.Reflection.MethodInfo>::System.Collections.Generic.IEnumerable<TKey>.GetEnumerator()
#define KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m1_11012(__this, method) (( Object_t* (*) (KeyCollection_t1_1391 *, const MethodInfo*))KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m1_6362_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<UnityEngine.Component,System.Reflection.MethodInfo>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define KeyCollection_System_Collections_ICollection_CopyTo_m1_11013(__this, ___array, ___index, method) (( void (*) (KeyCollection_t1_1391 *, Array_t *, int32_t, const MethodInfo*))KeyCollection_System_Collections_ICollection_CopyTo_m1_6363_gshared)(__this, ___array, ___index, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2/KeyCollection<UnityEngine.Component,System.Reflection.MethodInfo>::System.Collections.IEnumerable.GetEnumerator()
#define KeyCollection_System_Collections_IEnumerable_GetEnumerator_m1_11014(__this, method) (( Object_t * (*) (KeyCollection_t1_1391 *, const MethodInfo*))KeyCollection_System_Collections_IEnumerable_GetEnumerator_m1_6364_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<UnityEngine.Component,System.Reflection.MethodInfo>::System.Collections.Generic.ICollection<TKey>.get_IsReadOnly()
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m1_11015(__this, method) (( bool (*) (KeyCollection_t1_1391 *, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m1_6365_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/KeyCollection<UnityEngine.Component,System.Reflection.MethodInfo>::System.Collections.ICollection.get_SyncRoot()
#define KeyCollection_System_Collections_ICollection_get_SyncRoot_m1_11016(__this, method) (( Object_t * (*) (KeyCollection_t1_1391 *, const MethodInfo*))KeyCollection_System_Collections_ICollection_get_SyncRoot_m1_6366_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<UnityEngine.Component,System.Reflection.MethodInfo>::CopyTo(TKey[],System.Int32)
#define KeyCollection_CopyTo_m1_11017(__this, ___array, ___index, method) (( void (*) (KeyCollection_t1_1391 *, ComponentU5BU5D_t6_274*, int32_t, const MethodInfo*))KeyCollection_CopyTo_m1_6367_gshared)(__this, ___array, ___index, method)
// System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2/KeyCollection<UnityEngine.Component,System.Reflection.MethodInfo>::GetEnumerator()
#define KeyCollection_GetEnumerator_m1_11018(__this, method) (( Enumerator_t1_1493  (*) (KeyCollection_t1_1391 *, const MethodInfo*))KeyCollection_GetEnumerator_m1_5551_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2/KeyCollection<UnityEngine.Component,System.Reflection.MethodInfo>::get_Count()
#define KeyCollection_get_Count_m1_11019(__this, method) (( int32_t (*) (KeyCollection_t1_1391 *, const MethodInfo*))KeyCollection_get_Count_m1_6368_gshared)(__this, method)
