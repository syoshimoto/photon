﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_4MethodDeclarations.h"

// System.Void System.Collections.Generic.KeyValuePair`2<System.String,UnityEngine.GameObject>::.ctor(TKey,TValue)
#define KeyValuePair_2__ctor_m1_10448(__this, ___key, ___value, method) (( void (*) (KeyValuePair_2_t1_1358 *, String_t*, GameObject_t6_85 *, const MethodInfo*))KeyValuePair_2__ctor_m1_6351_gshared)(__this, ___key, ___value, method)
// TKey System.Collections.Generic.KeyValuePair`2<System.String,UnityEngine.GameObject>::get_Key()
#define KeyValuePair_2_get_Key_m1_10449(__this, method) (( String_t* (*) (KeyValuePair_2_t1_1358 *, const MethodInfo*))KeyValuePair_2_get_Key_m1_6352_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.String,UnityEngine.GameObject>::set_Key(TKey)
#define KeyValuePair_2_set_Key_m1_10450(__this, ___value, method) (( void (*) (KeyValuePair_2_t1_1358 *, String_t*, const MethodInfo*))KeyValuePair_2_set_Key_m1_6353_gshared)(__this, ___value, method)
// TValue System.Collections.Generic.KeyValuePair`2<System.String,UnityEngine.GameObject>::get_Value()
#define KeyValuePair_2_get_Value_m1_10451(__this, method) (( GameObject_t6_85 * (*) (KeyValuePair_2_t1_1358 *, const MethodInfo*))KeyValuePair_2_get_Value_m1_6354_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.String,UnityEngine.GameObject>::set_Value(TValue)
#define KeyValuePair_2_set_Value_m1_10452(__this, ___value, method) (( void (*) (KeyValuePair_2_t1_1358 *, GameObject_t6_85 *, const MethodInfo*))KeyValuePair_2_set_Value_m1_6355_gshared)(__this, ___value, method)
// System.String System.Collections.Generic.KeyValuePair`2<System.String,UnityEngine.GameObject>::ToString()
#define KeyValuePair_2_ToString_m1_10453(__this, method) (( String_t* (*) (KeyValuePair_2_t1_1358 *, const MethodInfo*))KeyValuePair_2_ToString_m1_6356_gshared)(__this, method)
