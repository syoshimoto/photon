﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "mscorlib_System_Enum.h"
#include "AssemblyU2DCSharp_ExitGames_Client_Photon_JoinMode.h"

// ExitGames.Client.Photon.JoinMode
struct  JoinMode_t8_88 
{
	// System.Byte ExitGames.Client.Photon.JoinMode::value__
	uint8_t ___value___1;
};
