﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "System_System_Collections_Generic_LinkedList_1_gen_0MethodDeclarations.h"

// System.Void System.Collections.Generic.LinkedList`1<ExitGames.Client.Photon.SimulationItem>::.ctor()
#define LinkedList_1__ctor_m3_1033(__this, method) (( void (*) (LinkedList_1_t3_183 *, const MethodInfo*))LinkedList_1__ctor_m3_1058_gshared)(__this, method)
// System.Void System.Collections.Generic.LinkedList`1<ExitGames.Client.Photon.SimulationItem>::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
#define LinkedList_1__ctor_m3_1160(__this, ___info, ___context, method) (( void (*) (LinkedList_1_t3_183 *, SerializationInfo_t1_177 *, StreamingContext_t1_514 , const MethodInfo*))LinkedList_1__ctor_m3_1059_gshared)(__this, ___info, ___context, method)
// System.Void System.Collections.Generic.LinkedList`1<ExitGames.Client.Photon.SimulationItem>::System.Collections.Generic.ICollection<T>.Add(T)
#define LinkedList_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m3_1161(__this, ___value, method) (( void (*) (LinkedList_1_t3_183 *, SimulationItem_t5_28 *, const MethodInfo*))LinkedList_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m3_1060_gshared)(__this, ___value, method)
// System.Void System.Collections.Generic.LinkedList`1<ExitGames.Client.Photon.SimulationItem>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define LinkedList_1_System_Collections_ICollection_CopyTo_m3_1162(__this, ___array, ___index, method) (( void (*) (LinkedList_1_t3_183 *, Array_t *, int32_t, const MethodInfo*))LinkedList_1_System_Collections_ICollection_CopyTo_m3_1061_gshared)(__this, ___array, ___index, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.LinkedList`1<ExitGames.Client.Photon.SimulationItem>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
#define LinkedList_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m3_1163(__this, method) (( Object_t* (*) (LinkedList_1_t3_183 *, const MethodInfo*))LinkedList_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m3_1062_gshared)(__this, method)
// System.Collections.IEnumerator System.Collections.Generic.LinkedList`1<ExitGames.Client.Photon.SimulationItem>::System.Collections.IEnumerable.GetEnumerator()
#define LinkedList_1_System_Collections_IEnumerable_GetEnumerator_m3_1164(__this, method) (( Object_t * (*) (LinkedList_1_t3_183 *, const MethodInfo*))LinkedList_1_System_Collections_IEnumerable_GetEnumerator_m3_1063_gshared)(__this, method)
// System.Boolean System.Collections.Generic.LinkedList`1<ExitGames.Client.Photon.SimulationItem>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
#define LinkedList_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m3_1165(__this, method) (( bool (*) (LinkedList_1_t3_183 *, const MethodInfo*))LinkedList_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m3_1064_gshared)(__this, method)
// System.Object System.Collections.Generic.LinkedList`1<ExitGames.Client.Photon.SimulationItem>::System.Collections.ICollection.get_SyncRoot()
#define LinkedList_1_System_Collections_ICollection_get_SyncRoot_m3_1166(__this, method) (( Object_t * (*) (LinkedList_1_t3_183 *, const MethodInfo*))LinkedList_1_System_Collections_ICollection_get_SyncRoot_m3_1065_gshared)(__this, method)
// System.Void System.Collections.Generic.LinkedList`1<ExitGames.Client.Photon.SimulationItem>::VerifyReferencedNode(System.Collections.Generic.LinkedListNode`1<T>)
#define LinkedList_1_VerifyReferencedNode_m3_1167(__this, ___node, method) (( void (*) (LinkedList_1_t3_183 *, LinkedListNode_1_t3_187 *, const MethodInfo*))LinkedList_1_VerifyReferencedNode_m3_1066_gshared)(__this, ___node, method)
// System.Collections.Generic.LinkedListNode`1<T> System.Collections.Generic.LinkedList`1<ExitGames.Client.Photon.SimulationItem>::AddBefore(System.Collections.Generic.LinkedListNode`1<T>,T)
#define LinkedList_1_AddBefore_m3_1030(__this, ___node, ___value, method) (( LinkedListNode_1_t3_187 * (*) (LinkedList_1_t3_183 *, LinkedListNode_1_t3_187 *, SimulationItem_t5_28 *, const MethodInfo*))LinkedList_1_AddBefore_m3_1067_gshared)(__this, ___node, ___value, method)
// System.Collections.Generic.LinkedListNode`1<T> System.Collections.Generic.LinkedList`1<ExitGames.Client.Photon.SimulationItem>::AddLast(T)
#define LinkedList_1_AddLast_m3_1026(__this, ___value, method) (( LinkedListNode_1_t3_187 * (*) (LinkedList_1_t3_183 *, SimulationItem_t5_28 *, const MethodInfo*))LinkedList_1_AddLast_m3_1068_gshared)(__this, ___value, method)
// System.Void System.Collections.Generic.LinkedList`1<ExitGames.Client.Photon.SimulationItem>::Clear()
#define LinkedList_1_Clear_m3_1168(__this, method) (( void (*) (LinkedList_1_t3_183 *, const MethodInfo*))LinkedList_1_Clear_m3_1069_gshared)(__this, method)
// System.Boolean System.Collections.Generic.LinkedList`1<ExitGames.Client.Photon.SimulationItem>::Contains(T)
#define LinkedList_1_Contains_m3_1169(__this, ___value, method) (( bool (*) (LinkedList_1_t3_183 *, SimulationItem_t5_28 *, const MethodInfo*))LinkedList_1_Contains_m3_1070_gshared)(__this, ___value, method)
// System.Void System.Collections.Generic.LinkedList`1<ExitGames.Client.Photon.SimulationItem>::CopyTo(T[],System.Int32)
#define LinkedList_1_CopyTo_m3_1170(__this, ___array, ___index, method) (( void (*) (LinkedList_1_t3_183 *, SimulationItemU5BU5D_t5_73*, int32_t, const MethodInfo*))LinkedList_1_CopyTo_m3_1071_gshared)(__this, ___array, ___index, method)
// System.Collections.Generic.LinkedListNode`1<T> System.Collections.Generic.LinkedList`1<ExitGames.Client.Photon.SimulationItem>::Find(T)
#define LinkedList_1_Find_m3_1171(__this, ___value, method) (( LinkedListNode_1_t3_187 * (*) (LinkedList_1_t3_183 *, SimulationItem_t5_28 *, const MethodInfo*))LinkedList_1_Find_m3_1072_gshared)(__this, ___value, method)
// System.Collections.Generic.LinkedList`1/Enumerator<T> System.Collections.Generic.LinkedList`1<ExitGames.Client.Photon.SimulationItem>::GetEnumerator()
#define LinkedList_1_GetEnumerator_m3_1043(__this, method) (( Enumerator_t3_188  (*) (LinkedList_1_t3_183 *, const MethodInfo*))LinkedList_1_GetEnumerator_m3_1073_gshared)(__this, method)
// System.Void System.Collections.Generic.LinkedList`1<ExitGames.Client.Photon.SimulationItem>::GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
#define LinkedList_1_GetObjectData_m3_1172(__this, ___info, ___context, method) (( void (*) (LinkedList_1_t3_183 *, SerializationInfo_t1_177 *, StreamingContext_t1_514 , const MethodInfo*))LinkedList_1_GetObjectData_m3_1074_gshared)(__this, ___info, ___context, method)
// System.Void System.Collections.Generic.LinkedList`1<ExitGames.Client.Photon.SimulationItem>::OnDeserialization(System.Object)
#define LinkedList_1_OnDeserialization_m3_1173(__this, ___sender, method) (( void (*) (LinkedList_1_t3_183 *, Object_t *, const MethodInfo*))LinkedList_1_OnDeserialization_m3_1075_gshared)(__this, ___sender, method)
// System.Boolean System.Collections.Generic.LinkedList`1<ExitGames.Client.Photon.SimulationItem>::Remove(T)
#define LinkedList_1_Remove_m3_1174(__this, ___value, method) (( bool (*) (LinkedList_1_t3_183 *, SimulationItem_t5_28 *, const MethodInfo*))LinkedList_1_Remove_m3_1076_gshared)(__this, ___value, method)
// System.Void System.Collections.Generic.LinkedList`1<ExitGames.Client.Photon.SimulationItem>::Remove(System.Collections.Generic.LinkedListNode`1<T>)
#define LinkedList_1_Remove_m3_1175(__this, ___node, method) (( void (*) (LinkedList_1_t3_183 *, LinkedListNode_1_t3_187 *, const MethodInfo*))LinkedList_1_Remove_m3_1077_gshared)(__this, ___node, method)
// System.Void System.Collections.Generic.LinkedList`1<ExitGames.Client.Photon.SimulationItem>::RemoveFirst()
#define LinkedList_1_RemoveFirst_m3_1031(__this, method) (( void (*) (LinkedList_1_t3_183 *, const MethodInfo*))LinkedList_1_RemoveFirst_m3_1078_gshared)(__this, method)
// System.Void System.Collections.Generic.LinkedList`1<ExitGames.Client.Photon.SimulationItem>::RemoveLast()
#define LinkedList_1_RemoveLast_m3_1176(__this, method) (( void (*) (LinkedList_1_t3_183 *, const MethodInfo*))LinkedList_1_RemoveLast_m3_1079_gshared)(__this, method)
// System.Int32 System.Collections.Generic.LinkedList`1<ExitGames.Client.Photon.SimulationItem>::get_Count()
#define LinkedList_1_get_Count_m3_1177(__this, method) (( int32_t (*) (LinkedList_1_t3_183 *, const MethodInfo*))LinkedList_1_get_Count_m3_1080_gshared)(__this, method)
// System.Collections.Generic.LinkedListNode`1<T> System.Collections.Generic.LinkedList`1<ExitGames.Client.Photon.SimulationItem>::get_First()
#define LinkedList_1_get_First_m3_1027(__this, method) (( LinkedListNode_1_t3_187 * (*) (LinkedList_1_t3_183 *, const MethodInfo*))LinkedList_1_get_First_m3_1081_gshared)(__this, method)
