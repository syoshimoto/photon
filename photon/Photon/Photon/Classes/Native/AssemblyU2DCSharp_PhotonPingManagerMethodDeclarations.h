﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PhotonPingManager
struct PhotonPingManager_t8_109;
// Region
struct Region_t8_110;
// System.Collections.IEnumerator
struct IEnumerator_t1_130;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"

// System.Void PhotonPingManager::.ctor()
extern "C" void PhotonPingManager__ctor_m8_800 (PhotonPingManager_t8_109 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PhotonPingManager::.cctor()
extern "C" void PhotonPingManager__cctor_m8_801 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Region PhotonPingManager::get_BestRegion()
extern "C" Region_t8_110 * PhotonPingManager_get_BestRegion_m8_802 (PhotonPingManager_t8_109 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PhotonPingManager::get_Done()
extern "C" bool PhotonPingManager_get_Done_m8_803 (PhotonPingManager_t8_109 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator PhotonPingManager::PingSocket(Region)
extern "C" Object_t * PhotonPingManager_PingSocket_m8_804 (PhotonPingManager_t8_109 * __this, Region_t8_110 * ___region, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PhotonPingManager::ResolveHost(System.String)
extern "C" String_t* PhotonPingManager_ResolveHost_m8_805 (Object_t * __this /* static, unused */, String_t* ___hostName, const MethodInfo* method) IL2CPP_METHOD_ATTR;
