﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2<System.Byte,System.Object>
struct Dictionary_2_t1_888;
// System.Object
struct Object_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Enumerator_.h"
#include "mscorlib_System_Collections_DictionaryEntry.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen.h"

// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Byte,System.Object>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C" void Enumerator__ctor_m1_7661_gshared (Enumerator_t1_903 * __this, Dictionary_2_t1_888 * ___dictionary, const MethodInfo* method);
#define Enumerator__ctor_m1_7661(__this, ___dictionary, method) (( void (*) (Enumerator_t1_903 *, Dictionary_2_t1_888 *, const MethodInfo*))Enumerator__ctor_m1_7661_gshared)(__this, ___dictionary, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Byte,System.Object>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * Enumerator_System_Collections_IEnumerator_get_Current_m1_7662_gshared (Enumerator_t1_903 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m1_7662(__this, method) (( Object_t * (*) (Enumerator_t1_903 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m1_7662_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Byte,System.Object>::System.Collections.IEnumerator.Reset()
extern "C" void Enumerator_System_Collections_IEnumerator_Reset_m1_7663_gshared (Enumerator_t1_903 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_Reset_m1_7663(__this, method) (( void (*) (Enumerator_t1_903 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m1_7663_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<System.Byte,System.Object>::System.Collections.IDictionaryEnumerator.get_Entry()
extern "C" DictionaryEntry_t1_167  Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m1_7664_gshared (Enumerator_t1_903 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m1_7664(__this, method) (( DictionaryEntry_t1_167  (*) (Enumerator_t1_903 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m1_7664_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Byte,System.Object>::System.Collections.IDictionaryEnumerator.get_Key()
extern "C" Object_t * Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m1_7665_gshared (Enumerator_t1_903 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m1_7665(__this, method) (( Object_t * (*) (Enumerator_t1_903 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m1_7665_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Byte,System.Object>::System.Collections.IDictionaryEnumerator.get_Value()
extern "C" Object_t * Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m1_7666_gshared (Enumerator_t1_903 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m1_7666(__this, method) (( Object_t * (*) (Enumerator_t1_903 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m1_7666_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<System.Byte,System.Object>::MoveNext()
extern "C" bool Enumerator_MoveNext_m1_5578_gshared (Enumerator_t1_903 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m1_5578(__this, method) (( bool (*) (Enumerator_t1_903 *, const MethodInfo*))Enumerator_MoveNext_m1_5578_gshared)(__this, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<System.Byte,System.Object>::get_Current()
extern "C" KeyValuePair_2_t1_902  Enumerator_get_Current_m1_5575_gshared (Enumerator_t1_903 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m1_5575(__this, method) (( KeyValuePair_2_t1_902  (*) (Enumerator_t1_903 *, const MethodInfo*))Enumerator_get_Current_m1_5575_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/Enumerator<System.Byte,System.Object>::get_CurrentKey()
extern "C" uint8_t Enumerator_get_CurrentKey_m1_7667_gshared (Enumerator_t1_903 * __this, const MethodInfo* method);
#define Enumerator_get_CurrentKey_m1_7667(__this, method) (( uint8_t (*) (Enumerator_t1_903 *, const MethodInfo*))Enumerator_get_CurrentKey_m1_7667_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/Enumerator<System.Byte,System.Object>::get_CurrentValue()
extern "C" Object_t * Enumerator_get_CurrentValue_m1_7668_gshared (Enumerator_t1_903 * __this, const MethodInfo* method);
#define Enumerator_get_CurrentValue_m1_7668(__this, method) (( Object_t * (*) (Enumerator_t1_903 *, const MethodInfo*))Enumerator_get_CurrentValue_m1_7668_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Byte,System.Object>::Reset()
extern "C" void Enumerator_Reset_m1_7669_gshared (Enumerator_t1_903 * __this, const MethodInfo* method);
#define Enumerator_Reset_m1_7669(__this, method) (( void (*) (Enumerator_t1_903 *, const MethodInfo*))Enumerator_Reset_m1_7669_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Byte,System.Object>::VerifyState()
extern "C" void Enumerator_VerifyState_m1_7670_gshared (Enumerator_t1_903 * __this, const MethodInfo* method);
#define Enumerator_VerifyState_m1_7670(__this, method) (( void (*) (Enumerator_t1_903 *, const MethodInfo*))Enumerator_VerifyState_m1_7670_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Byte,System.Object>::VerifyCurrent()
extern "C" void Enumerator_VerifyCurrent_m1_7671_gshared (Enumerator_t1_903 * __this, const MethodInfo* method);
#define Enumerator_VerifyCurrent_m1_7671(__this, method) (( void (*) (Enumerator_t1_903 *, const MethodInfo*))Enumerator_VerifyCurrent_m1_7671_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Byte,System.Object>::Dispose()
extern "C" void Enumerator_Dispose_m1_5579_gshared (Enumerator_t1_903 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m1_5579(__this, method) (( void (*) (Enumerator_t1_903 *, const MethodInfo*))Enumerator_Dispose_m1_5579_gshared)(__this, method)
