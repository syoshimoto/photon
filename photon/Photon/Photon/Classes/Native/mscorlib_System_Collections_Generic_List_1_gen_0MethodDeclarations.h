﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_List_1_gen_13MethodDeclarations.h"

// System.Void System.Collections.Generic.List`1<System.Security.Policy.StrongName>::.ctor()
#define List_1__ctor_m1_6895(__this, method) (( void (*) (List_1_t1_870 *, const MethodInfo*))List_1__ctor_m1_5617_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<System.Security.Policy.StrongName>::.ctor(System.Collections.Generic.IEnumerable`1<T>)
#define List_1__ctor_m1_6896(__this, ___collection, method) (( void (*) (List_1_t1_870 *, Object_t*, const MethodInfo*))List_1__ctor_m1_5673_gshared)(__this, ___collection, method)
// System.Void System.Collections.Generic.List`1<System.Security.Policy.StrongName>::.ctor(System.Int32)
#define List_1__ctor_m1_5530(__this, ___capacity, method) (( void (*) (List_1_t1_870 *, int32_t, const MethodInfo*))List_1__ctor_m1_5706_gshared)(__this, ___capacity, method)
// System.Void System.Collections.Generic.List`1<System.Security.Policy.StrongName>::.cctor()
#define List_1__cctor_m1_6897(__this /* static, unused */, method) (( void (*) (Object_t * /* static, unused */, const MethodInfo*))List_1__cctor_m1_5708_gshared)(__this /* static, unused */, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.List`1<System.Security.Policy.StrongName>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
#define List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m1_6898(__this, method) (( Object_t* (*) (List_1_t1_870 *, const MethodInfo*))List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m1_5710_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<System.Security.Policy.StrongName>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define List_1_System_Collections_ICollection_CopyTo_m1_6899(__this, ___array, ___arrayIndex, method) (( void (*) (List_1_t1_870 *, Array_t *, int32_t, const MethodInfo*))List_1_System_Collections_ICollection_CopyTo_m1_5712_gshared)(__this, ___array, ___arrayIndex, method)
// System.Collections.IEnumerator System.Collections.Generic.List`1<System.Security.Policy.StrongName>::System.Collections.IEnumerable.GetEnumerator()
#define List_1_System_Collections_IEnumerable_GetEnumerator_m1_6900(__this, method) (( Object_t * (*) (List_1_t1_870 *, const MethodInfo*))List_1_System_Collections_IEnumerable_GetEnumerator_m1_5714_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<System.Security.Policy.StrongName>::System.Collections.IList.Add(System.Object)
#define List_1_System_Collections_IList_Add_m1_6901(__this, ___item, method) (( int32_t (*) (List_1_t1_870 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Add_m1_5716_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.List`1<System.Security.Policy.StrongName>::System.Collections.IList.Contains(System.Object)
#define List_1_System_Collections_IList_Contains_m1_6902(__this, ___item, method) (( bool (*) (List_1_t1_870 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Contains_m1_5718_gshared)(__this, ___item, method)
// System.Int32 System.Collections.Generic.List`1<System.Security.Policy.StrongName>::System.Collections.IList.IndexOf(System.Object)
#define List_1_System_Collections_IList_IndexOf_m1_6903(__this, ___item, method) (( int32_t (*) (List_1_t1_870 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_IndexOf_m1_5720_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<System.Security.Policy.StrongName>::System.Collections.IList.Insert(System.Int32,System.Object)
#define List_1_System_Collections_IList_Insert_m1_6904(__this, ___index, ___item, method) (( void (*) (List_1_t1_870 *, int32_t, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Insert_m1_5722_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.Generic.List`1<System.Security.Policy.StrongName>::System.Collections.IList.Remove(System.Object)
#define List_1_System_Collections_IList_Remove_m1_6905(__this, ___item, method) (( void (*) (List_1_t1_870 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Remove_m1_5724_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.List`1<System.Security.Policy.StrongName>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
#define List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m1_6906(__this, method) (( bool (*) (List_1_t1_870 *, const MethodInfo*))List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m1_5726_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<System.Security.Policy.StrongName>::System.Collections.ICollection.get_SyncRoot()
#define List_1_System_Collections_ICollection_get_SyncRoot_m1_6907(__this, method) (( Object_t * (*) (List_1_t1_870 *, const MethodInfo*))List_1_System_Collections_ICollection_get_SyncRoot_m1_5728_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<System.Security.Policy.StrongName>::System.Collections.IList.get_Item(System.Int32)
#define List_1_System_Collections_IList_get_Item_m1_6908(__this, ___index, method) (( Object_t * (*) (List_1_t1_870 *, int32_t, const MethodInfo*))List_1_System_Collections_IList_get_Item_m1_5730_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<System.Security.Policy.StrongName>::System.Collections.IList.set_Item(System.Int32,System.Object)
#define List_1_System_Collections_IList_set_Item_m1_6909(__this, ___index, ___value, method) (( void (*) (List_1_t1_870 *, int32_t, Object_t *, const MethodInfo*))List_1_System_Collections_IList_set_Item_m1_5732_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.Generic.List`1<System.Security.Policy.StrongName>::Add(T)
#define List_1_Add_m1_6910(__this, ___item, method) (( void (*) (List_1_t1_870 *, StrongName_t1_623 *, const MethodInfo*))List_1_Add_m1_5734_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<System.Security.Policy.StrongName>::GrowIfNeeded(System.Int32)
#define List_1_GrowIfNeeded_m1_6911(__this, ___newCount, method) (( void (*) (List_1_t1_870 *, int32_t, const MethodInfo*))List_1_GrowIfNeeded_m1_5736_gshared)(__this, ___newCount, method)
// System.Void System.Collections.Generic.List`1<System.Security.Policy.StrongName>::AddCollection(System.Collections.Generic.ICollection`1<T>)
#define List_1_AddCollection_m1_6912(__this, ___collection, method) (( void (*) (List_1_t1_870 *, Object_t*, const MethodInfo*))List_1_AddCollection_m1_5738_gshared)(__this, ___collection, method)
// System.Void System.Collections.Generic.List`1<System.Security.Policy.StrongName>::AddEnumerable(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_AddEnumerable_m1_6913(__this, ___enumerable, method) (( void (*) (List_1_t1_870 *, Object_t*, const MethodInfo*))List_1_AddEnumerable_m1_5740_gshared)(__this, ___enumerable, method)
// System.Void System.Collections.Generic.List`1<System.Security.Policy.StrongName>::AddRange(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_AddRange_m1_6914(__this, ___collection, method) (( void (*) (List_1_t1_870 *, Object_t*, const MethodInfo*))List_1_AddRange_m1_5619_gshared)(__this, ___collection, method)
// System.Void System.Collections.Generic.List`1<System.Security.Policy.StrongName>::Clear()
#define List_1_Clear_m1_6915(__this, method) (( void (*) (List_1_t1_870 *, const MethodInfo*))List_1_Clear_m1_5742_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<System.Security.Policy.StrongName>::Contains(T)
#define List_1_Contains_m1_6916(__this, ___item, method) (( bool (*) (List_1_t1_870 *, StrongName_t1_623 *, const MethodInfo*))List_1_Contains_m1_5744_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<System.Security.Policy.StrongName>::CopyTo(T[],System.Int32)
#define List_1_CopyTo_m1_6917(__this, ___array, ___arrayIndex, method) (( void (*) (List_1_t1_870 *, StrongNameU5BU5D_t1_1101*, int32_t, const MethodInfo*))List_1_CopyTo_m1_5746_gshared)(__this, ___array, ___arrayIndex, method)
// System.Void System.Collections.Generic.List`1<System.Security.Policy.StrongName>::CheckMatch(System.Predicate`1<T>)
#define List_1_CheckMatch_m1_6918(__this /* static, unused */, ___match, method) (( void (*) (Object_t * /* static, unused */, Predicate_1_t1_1102 *, const MethodInfo*))List_1_CheckMatch_m1_5748_gshared)(__this /* static, unused */, ___match, method)
// System.Int32 System.Collections.Generic.List`1<System.Security.Policy.StrongName>::FindIndex(System.Predicate`1<T>)
#define List_1_FindIndex_m1_6919(__this, ___match, method) (( int32_t (*) (List_1_t1_870 *, Predicate_1_t1_1102 *, const MethodInfo*))List_1_FindIndex_m1_5750_gshared)(__this, ___match, method)
// System.Int32 System.Collections.Generic.List`1<System.Security.Policy.StrongName>::GetIndex(System.Int32,System.Int32,System.Predicate`1<T>)
#define List_1_GetIndex_m1_6920(__this, ___startIndex, ___count, ___match, method) (( int32_t (*) (List_1_t1_870 *, int32_t, int32_t, Predicate_1_t1_1102 *, const MethodInfo*))List_1_GetIndex_m1_5752_gshared)(__this, ___startIndex, ___count, ___match, method)
// System.Collections.Generic.List`1/Enumerator<T> System.Collections.Generic.List`1<System.Security.Policy.StrongName>::GetEnumerator()
#define List_1_GetEnumerator_m1_6921(__this, method) (( Enumerator_t1_1103  (*) (List_1_t1_870 *, const MethodInfo*))List_1_GetEnumerator_m1_5754_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<System.Security.Policy.StrongName>::IndexOf(T)
#define List_1_IndexOf_m1_6922(__this, ___item, method) (( int32_t (*) (List_1_t1_870 *, StrongName_t1_623 *, const MethodInfo*))List_1_IndexOf_m1_5756_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<System.Security.Policy.StrongName>::Shift(System.Int32,System.Int32)
#define List_1_Shift_m1_6923(__this, ___start, ___delta, method) (( void (*) (List_1_t1_870 *, int32_t, int32_t, const MethodInfo*))List_1_Shift_m1_5758_gshared)(__this, ___start, ___delta, method)
// System.Void System.Collections.Generic.List`1<System.Security.Policy.StrongName>::CheckIndex(System.Int32)
#define List_1_CheckIndex_m1_6924(__this, ___index, method) (( void (*) (List_1_t1_870 *, int32_t, const MethodInfo*))List_1_CheckIndex_m1_5760_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<System.Security.Policy.StrongName>::Insert(System.Int32,T)
#define List_1_Insert_m1_6925(__this, ___index, ___item, method) (( void (*) (List_1_t1_870 *, int32_t, StrongName_t1_623 *, const MethodInfo*))List_1_Insert_m1_5762_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.Generic.List`1<System.Security.Policy.StrongName>::CheckCollection(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_CheckCollection_m1_6926(__this, ___collection, method) (( void (*) (List_1_t1_870 *, Object_t*, const MethodInfo*))List_1_CheckCollection_m1_5764_gshared)(__this, ___collection, method)
// System.Boolean System.Collections.Generic.List`1<System.Security.Policy.StrongName>::Remove(T)
#define List_1_Remove_m1_6927(__this, ___item, method) (( bool (*) (List_1_t1_870 *, StrongName_t1_623 *, const MethodInfo*))List_1_Remove_m1_5766_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<System.Security.Policy.StrongName>::RemoveAt(System.Int32)
#define List_1_RemoveAt_m1_6928(__this, ___index, method) (( void (*) (List_1_t1_870 *, int32_t, const MethodInfo*))List_1_RemoveAt_m1_5768_gshared)(__this, ___index, method)
// T[] System.Collections.Generic.List`1<System.Security.Policy.StrongName>::ToArray()
#define List_1_ToArray_m1_6929(__this, method) (( StrongNameU5BU5D_t1_1101* (*) (List_1_t1_870 *, const MethodInfo*))List_1_ToArray_m1_5672_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<System.Security.Policy.StrongName>::get_Capacity()
#define List_1_get_Capacity_m1_6930(__this, method) (( int32_t (*) (List_1_t1_870 *, const MethodInfo*))List_1_get_Capacity_m1_5770_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<System.Security.Policy.StrongName>::set_Capacity(System.Int32)
#define List_1_set_Capacity_m1_6931(__this, ___value, method) (( void (*) (List_1_t1_870 *, int32_t, const MethodInfo*))List_1_set_Capacity_m1_5772_gshared)(__this, ___value, method)
// System.Int32 System.Collections.Generic.List`1<System.Security.Policy.StrongName>::get_Count()
#define List_1_get_Count_m1_6932(__this, method) (( int32_t (*) (List_1_t1_870 *, const MethodInfo*))List_1_get_Count_m1_5774_gshared)(__this, method)
// T System.Collections.Generic.List`1<System.Security.Policy.StrongName>::get_Item(System.Int32)
#define List_1_get_Item_m1_6933(__this, ___index, method) (( StrongName_t1_623 * (*) (List_1_t1_870 *, int32_t, const MethodInfo*))List_1_get_Item_m1_5776_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<System.Security.Policy.StrongName>::set_Item(System.Int32,T)
#define List_1_set_Item_m1_6934(__this, ___index, ___value, method) (( void (*) (List_1_t1_870 *, int32_t, StrongName_t1_623 *, const MethodInfo*))List_1_set_Item_m1_5778_gshared)(__this, ___index, ___value, method)
