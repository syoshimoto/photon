﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.List`1<System.Object>
struct List_1_t1_931;
// System.Collections.Generic.IEnumerable`1<System.Object>
struct IEnumerable_1_t1_1401;
// System.Collections.Generic.IEnumerator`1<System.Object>
struct IEnumerator_1_t1_1402;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t1_130;
// System.Object
struct Object_t;
// System.Collections.Generic.ICollection`1<System.Object>
struct ICollection_1_t1_1403;
// System.Object[]
struct ObjectU5BU5D_t1_157;
// System.Predicate`1<System.Object>
struct Predicate_1_t1_979;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_6.h"

// System.Void System.Collections.Generic.List`1<System.Object>::.ctor()
extern "C" void List_1__ctor_m1_5617_gshared (List_1_t1_931 * __this, const MethodInfo* method);
#define List_1__ctor_m1_5617(__this, method) (( void (*) (List_1_t1_931 *, const MethodInfo*))List_1__ctor_m1_5617_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<System.Object>::.ctor(System.Collections.Generic.IEnumerable`1<T>)
extern "C" void List_1__ctor_m1_5673_gshared (List_1_t1_931 * __this, Object_t* ___collection, const MethodInfo* method);
#define List_1__ctor_m1_5673(__this, ___collection, method) (( void (*) (List_1_t1_931 *, Object_t*, const MethodInfo*))List_1__ctor_m1_5673_gshared)(__this, ___collection, method)
// System.Void System.Collections.Generic.List`1<System.Object>::.ctor(System.Int32)
extern "C" void List_1__ctor_m1_5706_gshared (List_1_t1_931 * __this, int32_t ___capacity, const MethodInfo* method);
#define List_1__ctor_m1_5706(__this, ___capacity, method) (( void (*) (List_1_t1_931 *, int32_t, const MethodInfo*))List_1__ctor_m1_5706_gshared)(__this, ___capacity, method)
// System.Void System.Collections.Generic.List`1<System.Object>::.cctor()
extern "C" void List_1__cctor_m1_5708_gshared (Object_t * __this /* static, unused */, const MethodInfo* method);
#define List_1__cctor_m1_5708(__this /* static, unused */, method) (( void (*) (Object_t * /* static, unused */, const MethodInfo*))List_1__cctor_m1_5708_gshared)(__this /* static, unused */, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.List`1<System.Object>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
extern "C" Object_t* List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m1_5710_gshared (List_1_t1_931 * __this, const MethodInfo* method);
#define List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m1_5710(__this, method) (( Object_t* (*) (List_1_t1_931 *, const MethodInfo*))List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m1_5710_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<System.Object>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C" void List_1_System_Collections_ICollection_CopyTo_m1_5712_gshared (List_1_t1_931 * __this, Array_t * ___array, int32_t ___arrayIndex, const MethodInfo* method);
#define List_1_System_Collections_ICollection_CopyTo_m1_5712(__this, ___array, ___arrayIndex, method) (( void (*) (List_1_t1_931 *, Array_t *, int32_t, const MethodInfo*))List_1_System_Collections_ICollection_CopyTo_m1_5712_gshared)(__this, ___array, ___arrayIndex, method)
// System.Collections.IEnumerator System.Collections.Generic.List`1<System.Object>::System.Collections.IEnumerable.GetEnumerator()
extern "C" Object_t * List_1_System_Collections_IEnumerable_GetEnumerator_m1_5714_gshared (List_1_t1_931 * __this, const MethodInfo* method);
#define List_1_System_Collections_IEnumerable_GetEnumerator_m1_5714(__this, method) (( Object_t * (*) (List_1_t1_931 *, const MethodInfo*))List_1_System_Collections_IEnumerable_GetEnumerator_m1_5714_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<System.Object>::System.Collections.IList.Add(System.Object)
extern "C" int32_t List_1_System_Collections_IList_Add_m1_5716_gshared (List_1_t1_931 * __this, Object_t * ___item, const MethodInfo* method);
#define List_1_System_Collections_IList_Add_m1_5716(__this, ___item, method) (( int32_t (*) (List_1_t1_931 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Add_m1_5716_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.List`1<System.Object>::System.Collections.IList.Contains(System.Object)
extern "C" bool List_1_System_Collections_IList_Contains_m1_5718_gshared (List_1_t1_931 * __this, Object_t * ___item, const MethodInfo* method);
#define List_1_System_Collections_IList_Contains_m1_5718(__this, ___item, method) (( bool (*) (List_1_t1_931 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Contains_m1_5718_gshared)(__this, ___item, method)
// System.Int32 System.Collections.Generic.List`1<System.Object>::System.Collections.IList.IndexOf(System.Object)
extern "C" int32_t List_1_System_Collections_IList_IndexOf_m1_5720_gshared (List_1_t1_931 * __this, Object_t * ___item, const MethodInfo* method);
#define List_1_System_Collections_IList_IndexOf_m1_5720(__this, ___item, method) (( int32_t (*) (List_1_t1_931 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_IndexOf_m1_5720_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<System.Object>::System.Collections.IList.Insert(System.Int32,System.Object)
extern "C" void List_1_System_Collections_IList_Insert_m1_5722_gshared (List_1_t1_931 * __this, int32_t ___index, Object_t * ___item, const MethodInfo* method);
#define List_1_System_Collections_IList_Insert_m1_5722(__this, ___index, ___item, method) (( void (*) (List_1_t1_931 *, int32_t, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Insert_m1_5722_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.Generic.List`1<System.Object>::System.Collections.IList.Remove(System.Object)
extern "C" void List_1_System_Collections_IList_Remove_m1_5724_gshared (List_1_t1_931 * __this, Object_t * ___item, const MethodInfo* method);
#define List_1_System_Collections_IList_Remove_m1_5724(__this, ___item, method) (( void (*) (List_1_t1_931 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Remove_m1_5724_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.List`1<System.Object>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C" bool List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m1_5726_gshared (List_1_t1_931 * __this, const MethodInfo* method);
#define List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m1_5726(__this, method) (( bool (*) (List_1_t1_931 *, const MethodInfo*))List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m1_5726_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<System.Object>::System.Collections.ICollection.get_SyncRoot()
extern "C" Object_t * List_1_System_Collections_ICollection_get_SyncRoot_m1_5728_gshared (List_1_t1_931 * __this, const MethodInfo* method);
#define List_1_System_Collections_ICollection_get_SyncRoot_m1_5728(__this, method) (( Object_t * (*) (List_1_t1_931 *, const MethodInfo*))List_1_System_Collections_ICollection_get_SyncRoot_m1_5728_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<System.Object>::System.Collections.IList.get_Item(System.Int32)
extern "C" Object_t * List_1_System_Collections_IList_get_Item_m1_5730_gshared (List_1_t1_931 * __this, int32_t ___index, const MethodInfo* method);
#define List_1_System_Collections_IList_get_Item_m1_5730(__this, ___index, method) (( Object_t * (*) (List_1_t1_931 *, int32_t, const MethodInfo*))List_1_System_Collections_IList_get_Item_m1_5730_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<System.Object>::System.Collections.IList.set_Item(System.Int32,System.Object)
extern "C" void List_1_System_Collections_IList_set_Item_m1_5732_gshared (List_1_t1_931 * __this, int32_t ___index, Object_t * ___value, const MethodInfo* method);
#define List_1_System_Collections_IList_set_Item_m1_5732(__this, ___index, ___value, method) (( void (*) (List_1_t1_931 *, int32_t, Object_t *, const MethodInfo*))List_1_System_Collections_IList_set_Item_m1_5732_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.Generic.List`1<System.Object>::Add(T)
extern "C" void List_1_Add_m1_5734_gshared (List_1_t1_931 * __this, Object_t * ___item, const MethodInfo* method);
#define List_1_Add_m1_5734(__this, ___item, method) (( void (*) (List_1_t1_931 *, Object_t *, const MethodInfo*))List_1_Add_m1_5734_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<System.Object>::GrowIfNeeded(System.Int32)
extern "C" void List_1_GrowIfNeeded_m1_5736_gshared (List_1_t1_931 * __this, int32_t ___newCount, const MethodInfo* method);
#define List_1_GrowIfNeeded_m1_5736(__this, ___newCount, method) (( void (*) (List_1_t1_931 *, int32_t, const MethodInfo*))List_1_GrowIfNeeded_m1_5736_gshared)(__this, ___newCount, method)
// System.Void System.Collections.Generic.List`1<System.Object>::AddCollection(System.Collections.Generic.ICollection`1<T>)
extern "C" void List_1_AddCollection_m1_5738_gshared (List_1_t1_931 * __this, Object_t* ___collection, const MethodInfo* method);
#define List_1_AddCollection_m1_5738(__this, ___collection, method) (( void (*) (List_1_t1_931 *, Object_t*, const MethodInfo*))List_1_AddCollection_m1_5738_gshared)(__this, ___collection, method)
// System.Void System.Collections.Generic.List`1<System.Object>::AddEnumerable(System.Collections.Generic.IEnumerable`1<T>)
extern "C" void List_1_AddEnumerable_m1_5740_gshared (List_1_t1_931 * __this, Object_t* ___enumerable, const MethodInfo* method);
#define List_1_AddEnumerable_m1_5740(__this, ___enumerable, method) (( void (*) (List_1_t1_931 *, Object_t*, const MethodInfo*))List_1_AddEnumerable_m1_5740_gshared)(__this, ___enumerable, method)
// System.Void System.Collections.Generic.List`1<System.Object>::AddRange(System.Collections.Generic.IEnumerable`1<T>)
extern "C" void List_1_AddRange_m1_5619_gshared (List_1_t1_931 * __this, Object_t* ___collection, const MethodInfo* method);
#define List_1_AddRange_m1_5619(__this, ___collection, method) (( void (*) (List_1_t1_931 *, Object_t*, const MethodInfo*))List_1_AddRange_m1_5619_gshared)(__this, ___collection, method)
// System.Void System.Collections.Generic.List`1<System.Object>::Clear()
extern "C" void List_1_Clear_m1_5742_gshared (List_1_t1_931 * __this, const MethodInfo* method);
#define List_1_Clear_m1_5742(__this, method) (( void (*) (List_1_t1_931 *, const MethodInfo*))List_1_Clear_m1_5742_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<System.Object>::Contains(T)
extern "C" bool List_1_Contains_m1_5744_gshared (List_1_t1_931 * __this, Object_t * ___item, const MethodInfo* method);
#define List_1_Contains_m1_5744(__this, ___item, method) (( bool (*) (List_1_t1_931 *, Object_t *, const MethodInfo*))List_1_Contains_m1_5744_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<System.Object>::CopyTo(T[],System.Int32)
extern "C" void List_1_CopyTo_m1_5746_gshared (List_1_t1_931 * __this, ObjectU5BU5D_t1_157* ___array, int32_t ___arrayIndex, const MethodInfo* method);
#define List_1_CopyTo_m1_5746(__this, ___array, ___arrayIndex, method) (( void (*) (List_1_t1_931 *, ObjectU5BU5D_t1_157*, int32_t, const MethodInfo*))List_1_CopyTo_m1_5746_gshared)(__this, ___array, ___arrayIndex, method)
// System.Void System.Collections.Generic.List`1<System.Object>::CheckMatch(System.Predicate`1<T>)
extern "C" void List_1_CheckMatch_m1_5748_gshared (Object_t * __this /* static, unused */, Predicate_1_t1_979 * ___match, const MethodInfo* method);
#define List_1_CheckMatch_m1_5748(__this /* static, unused */, ___match, method) (( void (*) (Object_t * /* static, unused */, Predicate_1_t1_979 *, const MethodInfo*))List_1_CheckMatch_m1_5748_gshared)(__this /* static, unused */, ___match, method)
// System.Int32 System.Collections.Generic.List`1<System.Object>::FindIndex(System.Predicate`1<T>)
extern "C" int32_t List_1_FindIndex_m1_5750_gshared (List_1_t1_931 * __this, Predicate_1_t1_979 * ___match, const MethodInfo* method);
#define List_1_FindIndex_m1_5750(__this, ___match, method) (( int32_t (*) (List_1_t1_931 *, Predicate_1_t1_979 *, const MethodInfo*))List_1_FindIndex_m1_5750_gshared)(__this, ___match, method)
// System.Int32 System.Collections.Generic.List`1<System.Object>::GetIndex(System.Int32,System.Int32,System.Predicate`1<T>)
extern "C" int32_t List_1_GetIndex_m1_5752_gshared (List_1_t1_931 * __this, int32_t ___startIndex, int32_t ___count, Predicate_1_t1_979 * ___match, const MethodInfo* method);
#define List_1_GetIndex_m1_5752(__this, ___startIndex, ___count, ___match, method) (( int32_t (*) (List_1_t1_931 *, int32_t, int32_t, Predicate_1_t1_979 *, const MethodInfo*))List_1_GetIndex_m1_5752_gshared)(__this, ___startIndex, ___count, ___match, method)
// System.Collections.Generic.List`1/Enumerator<T> System.Collections.Generic.List`1<System.Object>::GetEnumerator()
extern "C" Enumerator_t1_975  List_1_GetEnumerator_m1_5754_gshared (List_1_t1_931 * __this, const MethodInfo* method);
#define List_1_GetEnumerator_m1_5754(__this, method) (( Enumerator_t1_975  (*) (List_1_t1_931 *, const MethodInfo*))List_1_GetEnumerator_m1_5754_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<System.Object>::IndexOf(T)
extern "C" int32_t List_1_IndexOf_m1_5756_gshared (List_1_t1_931 * __this, Object_t * ___item, const MethodInfo* method);
#define List_1_IndexOf_m1_5756(__this, ___item, method) (( int32_t (*) (List_1_t1_931 *, Object_t *, const MethodInfo*))List_1_IndexOf_m1_5756_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<System.Object>::Shift(System.Int32,System.Int32)
extern "C" void List_1_Shift_m1_5758_gshared (List_1_t1_931 * __this, int32_t ___start, int32_t ___delta, const MethodInfo* method);
#define List_1_Shift_m1_5758(__this, ___start, ___delta, method) (( void (*) (List_1_t1_931 *, int32_t, int32_t, const MethodInfo*))List_1_Shift_m1_5758_gshared)(__this, ___start, ___delta, method)
// System.Void System.Collections.Generic.List`1<System.Object>::CheckIndex(System.Int32)
extern "C" void List_1_CheckIndex_m1_5760_gshared (List_1_t1_931 * __this, int32_t ___index, const MethodInfo* method);
#define List_1_CheckIndex_m1_5760(__this, ___index, method) (( void (*) (List_1_t1_931 *, int32_t, const MethodInfo*))List_1_CheckIndex_m1_5760_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<System.Object>::Insert(System.Int32,T)
extern "C" void List_1_Insert_m1_5762_gshared (List_1_t1_931 * __this, int32_t ___index, Object_t * ___item, const MethodInfo* method);
#define List_1_Insert_m1_5762(__this, ___index, ___item, method) (( void (*) (List_1_t1_931 *, int32_t, Object_t *, const MethodInfo*))List_1_Insert_m1_5762_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.Generic.List`1<System.Object>::CheckCollection(System.Collections.Generic.IEnumerable`1<T>)
extern "C" void List_1_CheckCollection_m1_5764_gshared (List_1_t1_931 * __this, Object_t* ___collection, const MethodInfo* method);
#define List_1_CheckCollection_m1_5764(__this, ___collection, method) (( void (*) (List_1_t1_931 *, Object_t*, const MethodInfo*))List_1_CheckCollection_m1_5764_gshared)(__this, ___collection, method)
// System.Boolean System.Collections.Generic.List`1<System.Object>::Remove(T)
extern "C" bool List_1_Remove_m1_5766_gshared (List_1_t1_931 * __this, Object_t * ___item, const MethodInfo* method);
#define List_1_Remove_m1_5766(__this, ___item, method) (( bool (*) (List_1_t1_931 *, Object_t *, const MethodInfo*))List_1_Remove_m1_5766_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<System.Object>::RemoveAt(System.Int32)
extern "C" void List_1_RemoveAt_m1_5768_gshared (List_1_t1_931 * __this, int32_t ___index, const MethodInfo* method);
#define List_1_RemoveAt_m1_5768(__this, ___index, method) (( void (*) (List_1_t1_931 *, int32_t, const MethodInfo*))List_1_RemoveAt_m1_5768_gshared)(__this, ___index, method)
// T[] System.Collections.Generic.List`1<System.Object>::ToArray()
extern "C" ObjectU5BU5D_t1_157* List_1_ToArray_m1_5672_gshared (List_1_t1_931 * __this, const MethodInfo* method);
#define List_1_ToArray_m1_5672(__this, method) (( ObjectU5BU5D_t1_157* (*) (List_1_t1_931 *, const MethodInfo*))List_1_ToArray_m1_5672_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<System.Object>::get_Capacity()
extern "C" int32_t List_1_get_Capacity_m1_5770_gshared (List_1_t1_931 * __this, const MethodInfo* method);
#define List_1_get_Capacity_m1_5770(__this, method) (( int32_t (*) (List_1_t1_931 *, const MethodInfo*))List_1_get_Capacity_m1_5770_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<System.Object>::set_Capacity(System.Int32)
extern "C" void List_1_set_Capacity_m1_5772_gshared (List_1_t1_931 * __this, int32_t ___value, const MethodInfo* method);
#define List_1_set_Capacity_m1_5772(__this, ___value, method) (( void (*) (List_1_t1_931 *, int32_t, const MethodInfo*))List_1_set_Capacity_m1_5772_gshared)(__this, ___value, method)
// System.Int32 System.Collections.Generic.List`1<System.Object>::get_Count()
extern "C" int32_t List_1_get_Count_m1_5774_gshared (List_1_t1_931 * __this, const MethodInfo* method);
#define List_1_get_Count_m1_5774(__this, method) (( int32_t (*) (List_1_t1_931 *, const MethodInfo*))List_1_get_Count_m1_5774_gshared)(__this, method)
// T System.Collections.Generic.List`1<System.Object>::get_Item(System.Int32)
extern "C" Object_t * List_1_get_Item_m1_5776_gshared (List_1_t1_931 * __this, int32_t ___index, const MethodInfo* method);
#define List_1_get_Item_m1_5776(__this, ___index, method) (( Object_t * (*) (List_1_t1_931 *, int32_t, const MethodInfo*))List_1_get_Item_m1_5776_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<System.Object>::set_Item(System.Int32,T)
extern "C" void List_1_set_Item_m1_5778_gshared (List_1_t1_931 * __this, int32_t ___index, Object_t * ___value, const MethodInfo* method);
#define List_1_set_Item_m1_5778(__this, ___index, ___value, method) (( void (*) (List_1_t1_931 *, int32_t, Object_t *, const MethodInfo*))List_1_set_Item_m1_5778_gshared)(__this, ___index, ___value, method)
