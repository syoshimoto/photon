﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// ExitGames.Client.Photon.EnetPeer
struct EnetPeer_t5_19;

#include "mscorlib_System_Object.h"

// ExitGames.Client.Photon.EnetPeer/<>c__DisplayClassb
struct  U3CU3Ec__DisplayClassb_t5_18  : public Object_t
{
	// ExitGames.Client.Photon.EnetPeer ExitGames.Client.Photon.EnetPeer/<>c__DisplayClassb::<>4__this
	EnetPeer_t5_19 * ___U3CU3E4__this_0;
	// System.Int32 ExitGames.Client.Photon.EnetPeer/<>c__DisplayClassb::length
	int32_t ___length_1;
};
