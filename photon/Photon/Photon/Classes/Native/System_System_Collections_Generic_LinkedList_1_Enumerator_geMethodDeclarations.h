﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "System_System_Collections_Generic_LinkedList_1_Enumerator_ge_0MethodDeclarations.h"

// System.Void System.Collections.Generic.LinkedList`1/Enumerator<ExitGames.Client.Photon.SimulationItem>::.ctor(System.Collections.Generic.LinkedList`1<T>)
#define Enumerator__ctor_m3_1182(__this, ___parent, method) (( void (*) (Enumerator_t3_188 *, LinkedList_1_t3_183 *, const MethodInfo*))Enumerator__ctor_m3_1088_gshared)(__this, ___parent, method)
// System.Object System.Collections.Generic.LinkedList`1/Enumerator<ExitGames.Client.Photon.SimulationItem>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m3_1183(__this, method) (( Object_t * (*) (Enumerator_t3_188 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m3_1089_gshared)(__this, method)
// System.Void System.Collections.Generic.LinkedList`1/Enumerator<ExitGames.Client.Photon.SimulationItem>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m3_1184(__this, method) (( void (*) (Enumerator_t3_188 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m3_1090_gshared)(__this, method)
// T System.Collections.Generic.LinkedList`1/Enumerator<ExitGames.Client.Photon.SimulationItem>::get_Current()
#define Enumerator_get_Current_m3_1044(__this, method) (( SimulationItem_t5_28 * (*) (Enumerator_t3_188 *, const MethodInfo*))Enumerator_get_Current_m3_1091_gshared)(__this, method)
// System.Boolean System.Collections.Generic.LinkedList`1/Enumerator<ExitGames.Client.Photon.SimulationItem>::MoveNext()
#define Enumerator_MoveNext_m3_1045(__this, method) (( bool (*) (Enumerator_t3_188 *, const MethodInfo*))Enumerator_MoveNext_m3_1092_gshared)(__this, method)
// System.Void System.Collections.Generic.LinkedList`1/Enumerator<ExitGames.Client.Photon.SimulationItem>::Dispose()
#define Enumerator_Dispose_m3_1046(__this, method) (( void (*) (Enumerator_t3_188 *, const MethodInfo*))Enumerator_Dispose_m3_1093_gshared)(__this, method)
