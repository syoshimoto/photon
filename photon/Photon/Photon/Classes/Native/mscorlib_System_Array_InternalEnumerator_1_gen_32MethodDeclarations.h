﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Array
struct Array_t;
// System.Object
struct Object_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen_32.h"
#include "mscorlib_System_Collections_SortedList_Slot.h"

// System.Void System.Array/InternalEnumerator`1<System.Collections.SortedList/Slot>::.ctor(System.Array)
extern "C" void InternalEnumerator_1__ctor_m1_6439_gshared (InternalEnumerator_1_t1_1055 * __this, Array_t * ___array, const MethodInfo* method);
#define InternalEnumerator_1__ctor_m1_6439(__this, ___array, method) (( void (*) (InternalEnumerator_1_t1_1055 *, Array_t *, const MethodInfo*))InternalEnumerator_1__ctor_m1_6439_gshared)(__this, ___array, method)
// System.Void System.Array/InternalEnumerator`1<System.Collections.SortedList/Slot>::System.Collections.IEnumerator.Reset()
extern "C" void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1_6440_gshared (InternalEnumerator_1_t1_1055 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1_6440(__this, method) (( void (*) (InternalEnumerator_1_t1_1055 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1_6440_gshared)(__this, method)
// System.Object System.Array/InternalEnumerator`1<System.Collections.SortedList/Slot>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1_6441_gshared (InternalEnumerator_1_t1_1055 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1_6441(__this, method) (( Object_t * (*) (InternalEnumerator_1_t1_1055 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1_6441_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<System.Collections.SortedList/Slot>::Dispose()
extern "C" void InternalEnumerator_1_Dispose_m1_6442_gshared (InternalEnumerator_1_t1_1055 * __this, const MethodInfo* method);
#define InternalEnumerator_1_Dispose_m1_6442(__this, method) (( void (*) (InternalEnumerator_1_t1_1055 *, const MethodInfo*))InternalEnumerator_1_Dispose_m1_6442_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<System.Collections.SortedList/Slot>::MoveNext()
extern "C" bool InternalEnumerator_1_MoveNext_m1_6443_gshared (InternalEnumerator_1_t1_1055 * __this, const MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m1_6443(__this, method) (( bool (*) (InternalEnumerator_1_t1_1055 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m1_6443_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<System.Collections.SortedList/Slot>::get_Current()
extern "C" Slot_t1_183  InternalEnumerator_1_get_Current_m1_6444_gshared (InternalEnumerator_1_t1_1055 * __this, const MethodInfo* method);
#define InternalEnumerator_1_get_Current_m1_6444(__this, method) (( Slot_t1_183  (*) (InternalEnumerator_1_t1_1055 *, const MethodInfo*))InternalEnumerator_1_get_Current_m1_6444_gshared)(__this, method)
