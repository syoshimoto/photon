﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Collections.Generic.Dictionary`2<System.Byte,System.Object>
struct Dictionary_2_t1_888;

#include "Photon3Unity3D_ExitGames_Client_Photon_PhotonPeer.h"

// ExitGames.Client.Photon.LoadbalancingPeer
struct  LoadbalancingPeer_t8_81  : public PhotonPeer_t5_38
{
	// System.Collections.Generic.Dictionary`2<System.Byte,System.Object> ExitGames.Client.Photon.LoadbalancingPeer::opParameters
	Dictionary_2_t1_888 * ___opParameters_5;
};
