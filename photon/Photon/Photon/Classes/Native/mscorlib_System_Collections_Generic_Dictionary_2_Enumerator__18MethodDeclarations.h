﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Enumerator__19MethodDeclarations.h"

// System.Void System.Collections.Generic.Dictionary`2/Enumerator<PunTeams/Team,System.Collections.Generic.List`1<PhotonPlayer>>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
#define Enumerator__ctor_m1_9765(__this, ___dictionary, method) (( void (*) (Enumerator_t1_1312 *, Dictionary_2_t1_951 *, const MethodInfo*))Enumerator__ctor_m1_9624_gshared)(__this, ___dictionary, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<PunTeams/Team,System.Collections.Generic.List`1<PhotonPlayer>>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m1_9766(__this, method) (( Object_t * (*) (Enumerator_t1_1312 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m1_9625_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<PunTeams/Team,System.Collections.Generic.List`1<PhotonPlayer>>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m1_9767(__this, method) (( void (*) (Enumerator_t1_1312 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m1_9626_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<PunTeams/Team,System.Collections.Generic.List`1<PhotonPlayer>>::System.Collections.IDictionaryEnumerator.get_Entry()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m1_9768(__this, method) (( DictionaryEntry_t1_167  (*) (Enumerator_t1_1312 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m1_9627_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<PunTeams/Team,System.Collections.Generic.List`1<PhotonPlayer>>::System.Collections.IDictionaryEnumerator.get_Key()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m1_9769(__this, method) (( Object_t * (*) (Enumerator_t1_1312 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m1_9628_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<PunTeams/Team,System.Collections.Generic.List`1<PhotonPlayer>>::System.Collections.IDictionaryEnumerator.get_Value()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m1_9770(__this, method) (( Object_t * (*) (Enumerator_t1_1312 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m1_9629_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<PunTeams/Team,System.Collections.Generic.List`1<PhotonPlayer>>::MoveNext()
#define Enumerator_MoveNext_m1_9771(__this, method) (( bool (*) (Enumerator_t1_1312 *, const MethodInfo*))Enumerator_MoveNext_m1_9630_gshared)(__this, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<PunTeams/Team,System.Collections.Generic.List`1<PhotonPlayer>>::get_Current()
#define Enumerator_get_Current_m1_9772(__this, method) (( KeyValuePair_2_t1_1331  (*) (Enumerator_t1_1312 *, const MethodInfo*))Enumerator_get_Current_m1_9631_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/Enumerator<PunTeams/Team,System.Collections.Generic.List`1<PhotonPlayer>>::get_CurrentKey()
#define Enumerator_get_CurrentKey_m1_9773(__this, method) (( uint8_t (*) (Enumerator_t1_1312 *, const MethodInfo*))Enumerator_get_CurrentKey_m1_9632_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/Enumerator<PunTeams/Team,System.Collections.Generic.List`1<PhotonPlayer>>::get_CurrentValue()
#define Enumerator_get_CurrentValue_m1_9774(__this, method) (( List_1_t1_955 * (*) (Enumerator_t1_1312 *, const MethodInfo*))Enumerator_get_CurrentValue_m1_9633_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<PunTeams/Team,System.Collections.Generic.List`1<PhotonPlayer>>::Reset()
#define Enumerator_Reset_m1_9775(__this, method) (( void (*) (Enumerator_t1_1312 *, const MethodInfo*))Enumerator_Reset_m1_9634_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<PunTeams/Team,System.Collections.Generic.List`1<PhotonPlayer>>::VerifyState()
#define Enumerator_VerifyState_m1_9776(__this, method) (( void (*) (Enumerator_t1_1312 *, const MethodInfo*))Enumerator_VerifyState_m1_9635_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<PunTeams/Team,System.Collections.Generic.List`1<PhotonPlayer>>::VerifyCurrent()
#define Enumerator_VerifyCurrent_m1_9777(__this, method) (( void (*) (Enumerator_t1_1312 *, const MethodInfo*))Enumerator_VerifyCurrent_m1_9636_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<PunTeams/Team,System.Collections.Generic.List`1<PhotonPlayer>>::Dispose()
#define Enumerator_Dispose_m1_9778(__this, method) (( void (*) (Enumerator_t1_1312 *, const MethodInfo*))Enumerator_Dispose_m1_9637_gshared)(__this, method)
