﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// RoomInfo
struct RoomInfo_t8_124;
// System.String
struct String_t;
// ExitGames.Client.Photon.Hashtable
struct Hashtable_t5_1;
// System.Object
struct Object_t;

#include "codegen/il2cpp-codegen.h"

// System.Void RoomInfo::.ctor(System.String,ExitGames.Client.Photon.Hashtable)
extern "C" void RoomInfo__ctor_m8_826 (RoomInfo_t8_124 * __this, String_t* ___roomName, Hashtable_t5_1 * ___properties, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean RoomInfo::get_removedFromList()
extern "C" bool RoomInfo_get_removedFromList_m8_827 (RoomInfo_t8_124 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void RoomInfo::set_removedFromList(System.Boolean)
extern "C" void RoomInfo_set_removedFromList_m8_828 (RoomInfo_t8_124 * __this, bool ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean RoomInfo::get_serverSideMasterClient()
extern "C" bool RoomInfo_get_serverSideMasterClient_m8_829 (RoomInfo_t8_124 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void RoomInfo::set_serverSideMasterClient(System.Boolean)
extern "C" void RoomInfo_set_serverSideMasterClient_m8_830 (RoomInfo_t8_124 * __this, bool ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ExitGames.Client.Photon.Hashtable RoomInfo::get_customProperties()
extern "C" Hashtable_t5_1 * RoomInfo_get_customProperties_m8_831 (RoomInfo_t8_124 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String RoomInfo::get_name()
extern "C" String_t* RoomInfo_get_name_m8_832 (RoomInfo_t8_124 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 RoomInfo::get_playerCount()
extern "C" int32_t RoomInfo_get_playerCount_m8_833 (RoomInfo_t8_124 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void RoomInfo::set_playerCount(System.Int32)
extern "C" void RoomInfo_set_playerCount_m8_834 (RoomInfo_t8_124 * __this, int32_t ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean RoomInfo::get_isLocalClientInside()
extern "C" bool RoomInfo_get_isLocalClientInside_m8_835 (RoomInfo_t8_124 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void RoomInfo::set_isLocalClientInside(System.Boolean)
extern "C" void RoomInfo_set_isLocalClientInside_m8_836 (RoomInfo_t8_124 * __this, bool ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte RoomInfo::get_maxPlayers()
extern "C" uint8_t RoomInfo_get_maxPlayers_m8_837 (RoomInfo_t8_124 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean RoomInfo::get_open()
extern "C" bool RoomInfo_get_open_m8_838 (RoomInfo_t8_124 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean RoomInfo::get_visible()
extern "C" bool RoomInfo_get_visible_m8_839 (RoomInfo_t8_124 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean RoomInfo::Equals(System.Object)
extern "C" bool RoomInfo_Equals_m8_840 (RoomInfo_t8_124 * __this, Object_t * ___p, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 RoomInfo::GetHashCode()
extern "C" int32_t RoomInfo_GetHashCode_m8_841 (RoomInfo_t8_124 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String RoomInfo::ToString()
extern "C" String_t* RoomInfo_ToString_m8_842 (RoomInfo_t8_124 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String RoomInfo::ToStringFull()
extern "C" String_t* RoomInfo_ToStringFull_m8_843 (RoomInfo_t8_124 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void RoomInfo::InternalCacheProperties(ExitGames.Client.Photon.Hashtable)
extern "C" void RoomInfo_InternalCacheProperties_m8_844 (RoomInfo_t8_124 * __this, Hashtable_t5_1 * ___propertiesToCache, const MethodInfo* method) IL2CPP_METHOD_ATTR;
