﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PhotonPlayer
struct PhotonPlayer_t8_102;
// System.String
struct String_t;
// ExitGames.Client.Photon.Hashtable
struct Hashtable_t5_1;
// System.Object
struct Object_t;

#include "codegen/il2cpp-codegen.h"

// System.Void PhotonPlayer::.ctor(System.Boolean,System.Int32,System.String)
extern "C" void PhotonPlayer__ctor_m8_728 (PhotonPlayer_t8_102 * __this, bool ___isLocal, int32_t ___actorID, String_t* ___name, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PhotonPlayer::.ctor(System.Boolean,System.Int32,ExitGames.Client.Photon.Hashtable)
extern "C" void PhotonPlayer__ctor_m8_729 (PhotonPlayer_t8_102 * __this, bool ___isLocal, int32_t ___actorID, Hashtable_t5_1 * ___properties, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 PhotonPlayer::get_ID()
extern "C" int32_t PhotonPlayer_get_ID_m8_730 (PhotonPlayer_t8_102 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PhotonPlayer::get_name()
extern "C" String_t* PhotonPlayer_get_name_m8_731 (PhotonPlayer_t8_102 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PhotonPlayer::set_name(System.String)
extern "C" void PhotonPlayer_set_name_m8_732 (PhotonPlayer_t8_102 * __this, String_t* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PhotonPlayer::get_isMasterClient()
extern "C" bool PhotonPlayer_get_isMasterClient_m8_733 (PhotonPlayer_t8_102 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ExitGames.Client.Photon.Hashtable PhotonPlayer::get_customProperties()
extern "C" Hashtable_t5_1 * PhotonPlayer_get_customProperties_m8_734 (PhotonPlayer_t8_102 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PhotonPlayer::set_customProperties(ExitGames.Client.Photon.Hashtable)
extern "C" void PhotonPlayer_set_customProperties_m8_735 (PhotonPlayer_t8_102 * __this, Hashtable_t5_1 * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ExitGames.Client.Photon.Hashtable PhotonPlayer::get_allProperties()
extern "C" Hashtable_t5_1 * PhotonPlayer_get_allProperties_m8_736 (PhotonPlayer_t8_102 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PhotonPlayer::Equals(System.Object)
extern "C" bool PhotonPlayer_Equals_m8_737 (PhotonPlayer_t8_102 * __this, Object_t * ___p, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 PhotonPlayer::GetHashCode()
extern "C" int32_t PhotonPlayer_GetHashCode_m8_738 (PhotonPlayer_t8_102 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PhotonPlayer::InternalChangeLocalID(System.Int32)
extern "C" void PhotonPlayer_InternalChangeLocalID_m8_739 (PhotonPlayer_t8_102 * __this, int32_t ___newID, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PhotonPlayer::InternalCacheProperties(ExitGames.Client.Photon.Hashtable)
extern "C" void PhotonPlayer_InternalCacheProperties_m8_740 (PhotonPlayer_t8_102 * __this, Hashtable_t5_1 * ___properties, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PhotonPlayer::SetCustomProperties(ExitGames.Client.Photon.Hashtable,ExitGames.Client.Photon.Hashtable,System.Boolean)
extern "C" void PhotonPlayer_SetCustomProperties_m8_741 (PhotonPlayer_t8_102 * __this, Hashtable_t5_1 * ___propertiesToSet, Hashtable_t5_1 * ___expectedValues, bool ___webForward, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// PhotonPlayer PhotonPlayer::Find(System.Int32)
extern "C" PhotonPlayer_t8_102 * PhotonPlayer_Find_m8_742 (Object_t * __this /* static, unused */, int32_t ___ID, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// PhotonPlayer PhotonPlayer::Get(System.Int32)
extern "C" PhotonPlayer_t8_102 * PhotonPlayer_Get_m8_743 (PhotonPlayer_t8_102 * __this, int32_t ___id, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// PhotonPlayer PhotonPlayer::GetNext()
extern "C" PhotonPlayer_t8_102 * PhotonPlayer_GetNext_m8_744 (PhotonPlayer_t8_102 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// PhotonPlayer PhotonPlayer::GetNextFor(PhotonPlayer)
extern "C" PhotonPlayer_t8_102 * PhotonPlayer_GetNextFor_m8_745 (PhotonPlayer_t8_102 * __this, PhotonPlayer_t8_102 * ___currentPlayer, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// PhotonPlayer PhotonPlayer::GetNextFor(System.Int32)
extern "C" PhotonPlayer_t8_102 * PhotonPlayer_GetNextFor_m8_746 (PhotonPlayer_t8_102 * __this, int32_t ___currentPlayerId, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PhotonPlayer::ToString()
extern "C" String_t* PhotonPlayer_ToString_m8_747 (PhotonPlayer_t8_102 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PhotonPlayer::ToStringFull()
extern "C" String_t* PhotonPlayer_ToStringFull_m8_748 (PhotonPlayer_t8_102 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
