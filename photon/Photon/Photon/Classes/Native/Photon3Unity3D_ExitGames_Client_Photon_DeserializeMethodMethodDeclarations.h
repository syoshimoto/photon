﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ExitGames.Client.Photon.DeserializeMethod
struct DeserializeMethod_t5_48;
// System.Object
struct Object_t;
// System.Byte[]
struct ByteU5BU5D_t1_71;
// System.IAsyncResult
struct IAsyncResult_t1_27;
// System.AsyncCallback
struct AsyncCallback_t1_28;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_IntPtr.h"
#include "mscorlib_ArrayTypes.h"
#include "mscorlib_System_Byte.h"

// System.Void ExitGames.Client.Photon.DeserializeMethod::.ctor(System.Object,System.IntPtr)
extern "C" void DeserializeMethod__ctor_m5_276 (DeserializeMethod_t5_48 * __this, Object_t * ___object, IntPtr_t ___method, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object ExitGames.Client.Photon.DeserializeMethod::Invoke(System.Byte[])
extern "C" Object_t * DeserializeMethod_Invoke_m5_277 (DeserializeMethod_t5_48 * __this, ByteU5BU5D_t1_71* ___serializedCustomObject, const MethodInfo* method) IL2CPP_METHOD_ATTR;
extern "C" Object_t * pinvoke_delegate_wrapper_DeserializeMethod_t5_48(Il2CppObject* delegate, ByteU5BU5D_t1_71* ___serializedCustomObject);
// System.IAsyncResult ExitGames.Client.Photon.DeserializeMethod::BeginInvoke(System.Byte[],System.AsyncCallback,System.Object)
extern "C" Object_t * DeserializeMethod_BeginInvoke_m5_278 (DeserializeMethod_t5_48 * __this, ByteU5BU5D_t1_71* ___serializedCustomObject, AsyncCallback_t1_28 * ___callback, Object_t * ___object, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object ExitGames.Client.Photon.DeserializeMethod::EndInvoke(System.IAsyncResult)
extern "C" Object_t * DeserializeMethod_EndInvoke_m5_279 (DeserializeMethod_t5_48 * __this, Object_t * ___result, const MethodInfo* method) IL2CPP_METHOD_ATTR;
