﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.List`1<UnityEngine.UIVertex>
struct List_1_t1_913;
// System.Object
struct Object_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_13.h"
#include "UnityEngine_UnityEngine_UIVertex.h"

// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.UIVertex>::.ctor(System.Collections.Generic.List`1<T>)
extern "C" void Enumerator__ctor_m1_8429_gshared (Enumerator_t1_1228 * __this, List_1_t1_913 * ___l, const MethodInfo* method);
#define Enumerator__ctor_m1_8429(__this, ___l, method) (( void (*) (Enumerator_t1_1228 *, List_1_t1_913 *, const MethodInfo*))Enumerator__ctor_m1_8429_gshared)(__this, ___l, method)
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.UIVertex>::System.Collections.IEnumerator.Reset()
extern "C" void Enumerator_System_Collections_IEnumerator_Reset_m1_8430_gshared (Enumerator_t1_1228 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_Reset_m1_8430(__this, method) (( void (*) (Enumerator_t1_1228 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m1_8430_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1/Enumerator<UnityEngine.UIVertex>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * Enumerator_System_Collections_IEnumerator_get_Current_m1_8431_gshared (Enumerator_t1_1228 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m1_8431(__this, method) (( Object_t * (*) (Enumerator_t1_1228 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m1_8431_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.UIVertex>::Dispose()
extern "C" void Enumerator_Dispose_m1_8432_gshared (Enumerator_t1_1228 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m1_8432(__this, method) (( void (*) (Enumerator_t1_1228 *, const MethodInfo*))Enumerator_Dispose_m1_8432_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.UIVertex>::VerifyState()
extern "C" void Enumerator_VerifyState_m1_8433_gshared (Enumerator_t1_1228 * __this, const MethodInfo* method);
#define Enumerator_VerifyState_m1_8433(__this, method) (( void (*) (Enumerator_t1_1228 *, const MethodInfo*))Enumerator_VerifyState_m1_8433_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<UnityEngine.UIVertex>::MoveNext()
extern "C" bool Enumerator_MoveNext_m1_8434_gshared (Enumerator_t1_1228 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m1_8434(__this, method) (( bool (*) (Enumerator_t1_1228 *, const MethodInfo*))Enumerator_MoveNext_m1_8434_gshared)(__this, method)
// T System.Collections.Generic.List`1/Enumerator<UnityEngine.UIVertex>::get_Current()
extern "C" UIVertex_t6_153  Enumerator_get_Current_m1_8435_gshared (Enumerator_t1_1228 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m1_8435(__this, method) (( UIVertex_t6_153  (*) (Enumerator_t1_1228 *, const MethodInfo*))Enumerator_get_Current_m1_8435_gshared)(__this, method)
