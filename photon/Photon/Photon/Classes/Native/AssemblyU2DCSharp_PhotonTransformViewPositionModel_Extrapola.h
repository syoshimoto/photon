﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "mscorlib_System_Enum.h"
#include "AssemblyU2DCSharp_PhotonTransformViewPositionModel_Extrapola.h"

// PhotonTransformViewPositionModel/ExtrapolateOptions
struct  ExtrapolateOptions_t8_145 
{
	// System.Int32 PhotonTransformViewPositionModel/ExtrapolateOptions::value__
	int32_t ___value___1;
};
