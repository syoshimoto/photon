﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_ValueCollec_11MethodDeclarations.h"

// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.String,UnityEngine.GUIStyle>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
#define Enumerator__ctor_m1_8838(__this, ___host, method) (( void (*) (Enumerator_t1_929 *, Dictionary_2_t1_918 *, const MethodInfo*))Enumerator__ctor_m1_6403_gshared)(__this, ___host, method)
// System.Object System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.String,UnityEngine.GUIStyle>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m1_8839(__this, method) (( Object_t * (*) (Enumerator_t1_929 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m1_6404_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.String,UnityEngine.GUIStyle>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m1_8840(__this, method) (( void (*) (Enumerator_t1_929 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m1_6405_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.String,UnityEngine.GUIStyle>::Dispose()
#define Enumerator_Dispose_m1_8841(__this, method) (( void (*) (Enumerator_t1_929 *, const MethodInfo*))Enumerator_Dispose_m1_6406_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.String,UnityEngine.GUIStyle>::MoveNext()
#define Enumerator_MoveNext_m1_8842(__this, method) (( bool (*) (Enumerator_t1_929 *, const MethodInfo*))Enumerator_MoveNext_m1_6407_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.String,UnityEngine.GUIStyle>::get_Current()
#define Enumerator_get_Current_m1_8843(__this, method) (( GUIStyle_t6_166 * (*) (Enumerator_t1_929 *, const MethodInfo*))Enumerator_get_Current_m1_6408_gshared)(__this, method)
