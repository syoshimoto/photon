﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_7MethodDeclarations.h"

// System.Void System.Collections.Generic.KeyValuePair`2<System.Int32,PhotonView>::.ctor(TKey,TValue)
#define KeyValuePair_2__ctor_m1_10268(__this, ___key, ___value, method) (( void (*) (KeyValuePair_2_t1_964 *, int32_t, PhotonView_t8_3 *, const MethodInfo*))KeyValuePair_2__ctor_m1_7459_gshared)(__this, ___key, ___value, method)
// TKey System.Collections.Generic.KeyValuePair`2<System.Int32,PhotonView>::get_Key()
#define KeyValuePair_2_get_Key_m1_5666(__this, method) (( int32_t (*) (KeyValuePair_2_t1_964 *, const MethodInfo*))KeyValuePair_2_get_Key_m1_7460_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.Int32,PhotonView>::set_Key(TKey)
#define KeyValuePair_2_set_Key_m1_10269(__this, ___value, method) (( void (*) (KeyValuePair_2_t1_964 *, int32_t, const MethodInfo*))KeyValuePair_2_set_Key_m1_7461_gshared)(__this, ___value, method)
// TValue System.Collections.Generic.KeyValuePair`2<System.Int32,PhotonView>::get_Value()
#define KeyValuePair_2_get_Value_m1_5662(__this, method) (( PhotonView_t8_3 * (*) (KeyValuePair_2_t1_964 *, const MethodInfo*))KeyValuePair_2_get_Value_m1_7462_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.Int32,PhotonView>::set_Value(TValue)
#define KeyValuePair_2_set_Value_m1_10270(__this, ___value, method) (( void (*) (KeyValuePair_2_t1_964 *, PhotonView_t8_3 *, const MethodInfo*))KeyValuePair_2_set_Value_m1_7463_gshared)(__this, ___value, method)
// System.String System.Collections.Generic.KeyValuePair`2<System.Int32,PhotonView>::ToString()
#define KeyValuePair_2_ToString_m1_10271(__this, method) (( String_t* (*) (KeyValuePair_2_t1_964 *, const MethodInfo*))KeyValuePair_2_ToString_m1_7464_gshared)(__this, method)
