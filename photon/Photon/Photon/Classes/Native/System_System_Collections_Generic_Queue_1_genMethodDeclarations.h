﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "System_System_Collections_Generic_Queue_1_gen_5MethodDeclarations.h"

// System.Void System.Collections.Generic.Queue`1<ExitGames.Client.Photon.NCommand>::.ctor()
#define Queue_1__ctor_m3_1034(__this, method) (( void (*) (Queue_1_t3_181 *, const MethodInfo*))Queue_1__ctor_m3_1094_gshared)(__this, method)
// System.Void System.Collections.Generic.Queue`1<ExitGames.Client.Photon.NCommand>::.ctor(System.Int32)
#define Queue_1__ctor_m3_1020(__this, ___count, method) (( void (*) (Queue_1_t3_181 *, int32_t, const MethodInfo*))Queue_1__ctor_m3_1095_gshared)(__this, ___count, method)
// System.Void System.Collections.Generic.Queue`1<ExitGames.Client.Photon.NCommand>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define Queue_1_System_Collections_ICollection_CopyTo_m3_1129(__this, ___array, ___idx, method) (( void (*) (Queue_1_t3_181 *, Array_t *, int32_t, const MethodInfo*))Queue_1_System_Collections_ICollection_CopyTo_m3_1096_gshared)(__this, ___array, ___idx, method)
// System.Object System.Collections.Generic.Queue`1<ExitGames.Client.Photon.NCommand>::System.Collections.ICollection.get_SyncRoot()
#define Queue_1_System_Collections_ICollection_get_SyncRoot_m3_1130(__this, method) (( Object_t * (*) (Queue_1_t3_181 *, const MethodInfo*))Queue_1_System_Collections_ICollection_get_SyncRoot_m3_1097_gshared)(__this, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.Queue`1<ExitGames.Client.Photon.NCommand>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
#define Queue_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m3_1131(__this, method) (( Object_t* (*) (Queue_1_t3_181 *, const MethodInfo*))Queue_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m3_1098_gshared)(__this, method)
// System.Collections.IEnumerator System.Collections.Generic.Queue`1<ExitGames.Client.Photon.NCommand>::System.Collections.IEnumerable.GetEnumerator()
#define Queue_1_System_Collections_IEnumerable_GetEnumerator_m3_1132(__this, method) (( Object_t * (*) (Queue_1_t3_181 *, const MethodInfo*))Queue_1_System_Collections_IEnumerable_GetEnumerator_m3_1099_gshared)(__this, method)
// System.Void System.Collections.Generic.Queue`1<ExitGames.Client.Photon.NCommand>::Clear()
#define Queue_1_Clear_m3_1021(__this, method) (( void (*) (Queue_1_t3_181 *, const MethodInfo*))Queue_1_Clear_m3_1100_gshared)(__this, method)
// System.Void System.Collections.Generic.Queue`1<ExitGames.Client.Photon.NCommand>::CopyTo(T[],System.Int32)
#define Queue_1_CopyTo_m3_1133(__this, ___array, ___idx, method) (( void (*) (Queue_1_t3_181 *, NCommandU5BU5D_t5_67*, int32_t, const MethodInfo*))Queue_1_CopyTo_m3_1101_gshared)(__this, ___array, ___idx, method)
// T System.Collections.Generic.Queue`1<ExitGames.Client.Photon.NCommand>::Dequeue()
#define Queue_1_Dequeue_m3_1041(__this, method) (( NCommand_t5_13 * (*) (Queue_1_t3_181 *, const MethodInfo*))Queue_1_Dequeue_m3_1102_gshared)(__this, method)
// T System.Collections.Generic.Queue`1<ExitGames.Client.Photon.NCommand>::Peek()
#define Queue_1_Peek_m3_1042(__this, method) (( NCommand_t5_13 * (*) (Queue_1_t3_181 *, const MethodInfo*))Queue_1_Peek_m3_1103_gshared)(__this, method)
// System.Void System.Collections.Generic.Queue`1<ExitGames.Client.Photon.NCommand>::Enqueue(T)
#define Queue_1_Enqueue_m3_1040(__this, ___item, method) (( void (*) (Queue_1_t3_181 *, NCommand_t5_13 *, const MethodInfo*))Queue_1_Enqueue_m3_1104_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.Queue`1<ExitGames.Client.Photon.NCommand>::SetCapacity(System.Int32)
#define Queue_1_SetCapacity_m3_1134(__this, ___new_size, method) (( void (*) (Queue_1_t3_181 *, int32_t, const MethodInfo*))Queue_1_SetCapacity_m3_1105_gshared)(__this, ___new_size, method)
// System.Int32 System.Collections.Generic.Queue`1<ExitGames.Client.Photon.NCommand>::get_Count()
#define Queue_1_get_Count_m3_1135(__this, method) (( int32_t (*) (Queue_1_t3_181 *, const MethodInfo*))Queue_1_get_Count_m3_1106_gshared)(__this, method)
// System.Collections.Generic.Queue`1/Enumerator<T> System.Collections.Generic.Queue`1<ExitGames.Client.Photon.NCommand>::GetEnumerator()
#define Queue_1_GetEnumerator_m3_1136(__this, method) (( Enumerator_t3_198  (*) (Queue_1_t3_181 *, const MethodInfo*))Queue_1_GetEnumerator_m3_1107_gshared)(__this, method)
