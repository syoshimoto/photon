﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif



#include "mscorlib_System_Array.h"

#pragma once
// ExitGames.Client.Photon.NCommand[]
// ExitGames.Client.Photon.NCommand[]
struct NCommandU5BU5D_t5_67  : public Array_t { };
// ExitGames.Client.Photon.PeerBase/MyAction[]
// ExitGames.Client.Photon.PeerBase/MyAction[]
struct MyActionU5BU5D_t5_68  : public Array_t { };
// ExitGames.Client.Photon.SimulationItem[]
// ExitGames.Client.Photon.SimulationItem[]
struct SimulationItemU5BU5D_t5_73  : public Array_t { };
// ExitGames.Client.Photon.CmdLogItem[]
// ExitGames.Client.Photon.CmdLogItem[]
struct CmdLogItemU5BU5D_t5_69  : public Array_t { };
// ExitGames.Client.Photon.EnetChannel[]
// ExitGames.Client.Photon.EnetChannel[]
struct EnetChannelU5BU5D_t5_22  : public Array_t { };
// ExitGames.Client.Photon.CustomType[]
// ExitGames.Client.Photon.CustomType[]
struct CustomTypeU5BU5D_t5_70  : public Array_t { };
// ExitGames.Client.Photon.ConnectionProtocol[]
// ExitGames.Client.Photon.ConnectionProtocol[]
struct ConnectionProtocolU5BU5D_t5_71  : public Array_t { };
// ExitGames.Client.Photon.Hashtable[]
// ExitGames.Client.Photon.Hashtable[]
struct HashtableU5BU5D_t5_72  : public Array_t { };
