﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ConnectAndJoinRandom
struct ConnectAndJoinRandom_t8_148;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_DisconnectCause.h"

// System.Void ConnectAndJoinRandom::.ctor()
extern "C" void ConnectAndJoinRandom__ctor_m8_920 (ConnectAndJoinRandom_t8_148 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ConnectAndJoinRandom::Start()
extern "C" void ConnectAndJoinRandom_Start_m8_921 (ConnectAndJoinRandom_t8_148 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ConnectAndJoinRandom::Update()
extern "C" void ConnectAndJoinRandom_Update_m8_922 (ConnectAndJoinRandom_t8_148 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ConnectAndJoinRandom::OnConnectedToMaster()
extern "C" void ConnectAndJoinRandom_OnConnectedToMaster_m8_923 (ConnectAndJoinRandom_t8_148 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ConnectAndJoinRandom::OnJoinedLobby()
extern "C" void ConnectAndJoinRandom_OnJoinedLobby_m8_924 (ConnectAndJoinRandom_t8_148 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ConnectAndJoinRandom::OnPhotonRandomJoinFailed()
extern "C" void ConnectAndJoinRandom_OnPhotonRandomJoinFailed_m8_925 (ConnectAndJoinRandom_t8_148 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ConnectAndJoinRandom::OnFailedToConnectToPhoton(DisconnectCause)
extern "C" void ConnectAndJoinRandom_OnFailedToConnectToPhoton_m8_926 (ConnectAndJoinRandom_t8_148 * __this, int32_t ___cause, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ConnectAndJoinRandom::OnJoinedRoom()
extern "C" void ConnectAndJoinRandom_OnJoinedRoom_m8_927 (ConnectAndJoinRandom_t8_148 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
