﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "UnityEngine_UnityEngine_MonoBehaviour.h"

// SupportLogging
struct  SupportLogging_t8_178  : public MonoBehaviour_t6_80
{
	// System.Boolean SupportLogging::LogTrafficStats
	bool ___LogTrafficStats_2;
};
