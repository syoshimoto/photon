﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_14MethodDeclarations.h"

// System.Void System.Collections.Generic.KeyValuePair`2<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>::.ctor(TKey,TValue)
#define KeyValuePair_2__ctor_m1_9114(__this, ___key, ___value, method) (( void (*) (KeyValuePair_2_t1_1280 *, Event_t6_154 *, int32_t, const MethodInfo*))KeyValuePair_2__ctor_m1_9015_gshared)(__this, ___key, ___value, method)
// TKey System.Collections.Generic.KeyValuePair`2<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>::get_Key()
#define KeyValuePair_2_get_Key_m1_9115(__this, method) (( Event_t6_154 * (*) (KeyValuePair_2_t1_1280 *, const MethodInfo*))KeyValuePair_2_get_Key_m1_9016_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>::set_Key(TKey)
#define KeyValuePair_2_set_Key_m1_9116(__this, ___value, method) (( void (*) (KeyValuePair_2_t1_1280 *, Event_t6_154 *, const MethodInfo*))KeyValuePair_2_set_Key_m1_9017_gshared)(__this, ___value, method)
// TValue System.Collections.Generic.KeyValuePair`2<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>::get_Value()
#define KeyValuePair_2_get_Value_m1_9117(__this, method) (( int32_t (*) (KeyValuePair_2_t1_1280 *, const MethodInfo*))KeyValuePair_2_get_Value_m1_9018_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>::set_Value(TValue)
#define KeyValuePair_2_set_Value_m1_9118(__this, ___value, method) (( void (*) (KeyValuePair_2_t1_1280 *, int32_t, const MethodInfo*))KeyValuePair_2_set_Value_m1_9019_gshared)(__this, ___value, method)
// System.String System.Collections.Generic.KeyValuePair`2<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>::ToString()
#define KeyValuePair_2_ToString_m1_9119(__this, method) (( String_t* (*) (KeyValuePair_2_t1_1280 *, const MethodInfo*))KeyValuePair_2_ToString_m1_9020_gshared)(__this, method)
