﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// MaterialPerOwner
struct MaterialPerOwner_t8_10;

#include "codegen/il2cpp-codegen.h"

// System.Void MaterialPerOwner::.ctor()
extern "C" void MaterialPerOwner__ctor_m8_23 (MaterialPerOwner_t8_10 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MaterialPerOwner::Start()
extern "C" void MaterialPerOwner_Start_m8_24 (MaterialPerOwner_t8_10 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MaterialPerOwner::Update()
extern "C" void MaterialPerOwner_Update_m8_25 (MaterialPerOwner_t8_10 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
