﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Object
struct Object_t;
// System.Byte[]
struct ByteU5BU5D_t1_71;
// System.IAsyncResult
struct IAsyncResult_t1_27;
// System.AsyncCallback
struct AsyncCallback_t1_28;

#include "mscorlib_System_MulticastDelegate.h"

// ExitGames.Client.Photon.DeserializeMethod
struct  DeserializeMethod_t5_48  : public MulticastDelegate_t1_21
{
};
