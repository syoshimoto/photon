﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2/KeyCollection<ExitGames.Client.Photon.ConnectionProtocol,System.Int32>
struct KeyCollection_t1_1297;
// System.Collections.Generic.Dictionary`2<ExitGames.Client.Photon.ConnectionProtocol,System.Int32>
struct Dictionary_2_t1_933;
// System.Collections.Generic.IEnumerator`1<ExitGames.Client.Photon.ConnectionProtocol>
struct IEnumerator_1_t1_1462;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t1_130;
// System.Object
struct Object_t;
// ExitGames.Client.Photon.ConnectionProtocol[]
struct ConnectionProtocolU5BU5D_t5_71;

#include "codegen/il2cpp-codegen.h"
#include "Photon3Unity3D_ExitGames_Client_Photon_ConnectionProtocol.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_KeyCollecti_27.h"

// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<ExitGames.Client.Photon.ConnectionProtocol,System.Int32>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C" void KeyCollection__ctor_m1_9419_gshared (KeyCollection_t1_1297 * __this, Dictionary_2_t1_933 * ___dictionary, const MethodInfo* method);
#define KeyCollection__ctor_m1_9419(__this, ___dictionary, method) (( void (*) (KeyCollection_t1_1297 *, Dictionary_2_t1_933 *, const MethodInfo*))KeyCollection__ctor_m1_9419_gshared)(__this, ___dictionary, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<ExitGames.Client.Photon.ConnectionProtocol,System.Int32>::System.Collections.Generic.ICollection<TKey>.Add(TKey)
extern "C" void KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m1_9420_gshared (KeyCollection_t1_1297 * __this, uint8_t ___item, const MethodInfo* method);
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m1_9420(__this, ___item, method) (( void (*) (KeyCollection_t1_1297 *, uint8_t, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m1_9420_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<ExitGames.Client.Photon.ConnectionProtocol,System.Int32>::System.Collections.Generic.ICollection<TKey>.Clear()
extern "C" void KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m1_9421_gshared (KeyCollection_t1_1297 * __this, const MethodInfo* method);
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m1_9421(__this, method) (( void (*) (KeyCollection_t1_1297 *, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m1_9421_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<ExitGames.Client.Photon.ConnectionProtocol,System.Int32>::System.Collections.Generic.ICollection<TKey>.Contains(TKey)
extern "C" bool KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m1_9422_gshared (KeyCollection_t1_1297 * __this, uint8_t ___item, const MethodInfo* method);
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m1_9422(__this, ___item, method) (( bool (*) (KeyCollection_t1_1297 *, uint8_t, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m1_9422_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<ExitGames.Client.Photon.ConnectionProtocol,System.Int32>::System.Collections.Generic.ICollection<TKey>.Remove(TKey)
extern "C" bool KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m1_9423_gshared (KeyCollection_t1_1297 * __this, uint8_t ___item, const MethodInfo* method);
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m1_9423(__this, ___item, method) (( bool (*) (KeyCollection_t1_1297 *, uint8_t, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m1_9423_gshared)(__this, ___item, method)
// System.Collections.Generic.IEnumerator`1<TKey> System.Collections.Generic.Dictionary`2/KeyCollection<ExitGames.Client.Photon.ConnectionProtocol,System.Int32>::System.Collections.Generic.IEnumerable<TKey>.GetEnumerator()
extern "C" Object_t* KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m1_9424_gshared (KeyCollection_t1_1297 * __this, const MethodInfo* method);
#define KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m1_9424(__this, method) (( Object_t* (*) (KeyCollection_t1_1297 *, const MethodInfo*))KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m1_9424_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<ExitGames.Client.Photon.ConnectionProtocol,System.Int32>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C" void KeyCollection_System_Collections_ICollection_CopyTo_m1_9425_gshared (KeyCollection_t1_1297 * __this, Array_t * ___array, int32_t ___index, const MethodInfo* method);
#define KeyCollection_System_Collections_ICollection_CopyTo_m1_9425(__this, ___array, ___index, method) (( void (*) (KeyCollection_t1_1297 *, Array_t *, int32_t, const MethodInfo*))KeyCollection_System_Collections_ICollection_CopyTo_m1_9425_gshared)(__this, ___array, ___index, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2/KeyCollection<ExitGames.Client.Photon.ConnectionProtocol,System.Int32>::System.Collections.IEnumerable.GetEnumerator()
extern "C" Object_t * KeyCollection_System_Collections_IEnumerable_GetEnumerator_m1_9426_gshared (KeyCollection_t1_1297 * __this, const MethodInfo* method);
#define KeyCollection_System_Collections_IEnumerable_GetEnumerator_m1_9426(__this, method) (( Object_t * (*) (KeyCollection_t1_1297 *, const MethodInfo*))KeyCollection_System_Collections_IEnumerable_GetEnumerator_m1_9426_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<ExitGames.Client.Photon.ConnectionProtocol,System.Int32>::System.Collections.Generic.ICollection<TKey>.get_IsReadOnly()
extern "C" bool KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m1_9427_gshared (KeyCollection_t1_1297 * __this, const MethodInfo* method);
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m1_9427(__this, method) (( bool (*) (KeyCollection_t1_1297 *, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m1_9427_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/KeyCollection<ExitGames.Client.Photon.ConnectionProtocol,System.Int32>::System.Collections.ICollection.get_SyncRoot()
extern "C" Object_t * KeyCollection_System_Collections_ICollection_get_SyncRoot_m1_9428_gshared (KeyCollection_t1_1297 * __this, const MethodInfo* method);
#define KeyCollection_System_Collections_ICollection_get_SyncRoot_m1_9428(__this, method) (( Object_t * (*) (KeyCollection_t1_1297 *, const MethodInfo*))KeyCollection_System_Collections_ICollection_get_SyncRoot_m1_9428_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<ExitGames.Client.Photon.ConnectionProtocol,System.Int32>::CopyTo(TKey[],System.Int32)
extern "C" void KeyCollection_CopyTo_m1_9429_gshared (KeyCollection_t1_1297 * __this, ConnectionProtocolU5BU5D_t5_71* ___array, int32_t ___index, const MethodInfo* method);
#define KeyCollection_CopyTo_m1_9429(__this, ___array, ___index, method) (( void (*) (KeyCollection_t1_1297 *, ConnectionProtocolU5BU5D_t5_71*, int32_t, const MethodInfo*))KeyCollection_CopyTo_m1_9429_gshared)(__this, ___array, ___index, method)
// System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2/KeyCollection<ExitGames.Client.Photon.ConnectionProtocol,System.Int32>::GetEnumerator()
extern "C" Enumerator_t1_1298  KeyCollection_GetEnumerator_m1_9430_gshared (KeyCollection_t1_1297 * __this, const MethodInfo* method);
#define KeyCollection_GetEnumerator_m1_9430(__this, method) (( Enumerator_t1_1298  (*) (KeyCollection_t1_1297 *, const MethodInfo*))KeyCollection_GetEnumerator_m1_9430_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2/KeyCollection<ExitGames.Client.Photon.ConnectionProtocol,System.Int32>::get_Count()
extern "C" int32_t KeyCollection_get_Count_m1_9431_gshared (KeyCollection_t1_1297 * __this, const MethodInfo* method);
#define KeyCollection_get_Count_m1_9431(__this, method) (( int32_t (*) (KeyCollection_t1_1297 *, const MethodInfo*))KeyCollection_get_Count_m1_9431_gshared)(__this, method)
