﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.List`1<System.Single>
struct List_1_t1_971;
// System.Collections.Generic.IEnumerable`1<System.Single>
struct IEnumerable_1_t1_1496;
// System.Collections.Generic.IEnumerator`1<System.Single>
struct IEnumerator_1_t1_1497;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t1_130;
// System.Object
struct Object_t;
// System.Collections.Generic.ICollection`1<System.Single>
struct ICollection_1_t1_1498;
// System.Single[]
struct SingleU5BU5D_t1_863;
// System.Predicate`1<System.Single>
struct Predicate_1_t1_1400;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_25.h"

// System.Void System.Collections.Generic.List`1<System.Single>::.ctor()
extern "C" void List_1__ctor_m1_11102_gshared (List_1_t1_971 * __this, const MethodInfo* method);
#define List_1__ctor_m1_11102(__this, method) (( void (*) (List_1_t1_971 *, const MethodInfo*))List_1__ctor_m1_11102_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<System.Single>::.ctor(System.Collections.Generic.IEnumerable`1<T>)
extern "C" void List_1__ctor_m1_11103_gshared (List_1_t1_971 * __this, Object_t* ___collection, const MethodInfo* method);
#define List_1__ctor_m1_11103(__this, ___collection, method) (( void (*) (List_1_t1_971 *, Object_t*, const MethodInfo*))List_1__ctor_m1_11103_gshared)(__this, ___collection, method)
// System.Void System.Collections.Generic.List`1<System.Single>::.ctor(System.Int32)
extern "C" void List_1__ctor_m1_5684_gshared (List_1_t1_971 * __this, int32_t ___capacity, const MethodInfo* method);
#define List_1__ctor_m1_5684(__this, ___capacity, method) (( void (*) (List_1_t1_971 *, int32_t, const MethodInfo*))List_1__ctor_m1_5684_gshared)(__this, ___capacity, method)
// System.Void System.Collections.Generic.List`1<System.Single>::.cctor()
extern "C" void List_1__cctor_m1_11104_gshared (Object_t * __this /* static, unused */, const MethodInfo* method);
#define List_1__cctor_m1_11104(__this /* static, unused */, method) (( void (*) (Object_t * /* static, unused */, const MethodInfo*))List_1__cctor_m1_11104_gshared)(__this /* static, unused */, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.List`1<System.Single>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
extern "C" Object_t* List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m1_11105_gshared (List_1_t1_971 * __this, const MethodInfo* method);
#define List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m1_11105(__this, method) (( Object_t* (*) (List_1_t1_971 *, const MethodInfo*))List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m1_11105_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<System.Single>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C" void List_1_System_Collections_ICollection_CopyTo_m1_11106_gshared (List_1_t1_971 * __this, Array_t * ___array, int32_t ___arrayIndex, const MethodInfo* method);
#define List_1_System_Collections_ICollection_CopyTo_m1_11106(__this, ___array, ___arrayIndex, method) (( void (*) (List_1_t1_971 *, Array_t *, int32_t, const MethodInfo*))List_1_System_Collections_ICollection_CopyTo_m1_11106_gshared)(__this, ___array, ___arrayIndex, method)
// System.Collections.IEnumerator System.Collections.Generic.List`1<System.Single>::System.Collections.IEnumerable.GetEnumerator()
extern "C" Object_t * List_1_System_Collections_IEnumerable_GetEnumerator_m1_11107_gshared (List_1_t1_971 * __this, const MethodInfo* method);
#define List_1_System_Collections_IEnumerable_GetEnumerator_m1_11107(__this, method) (( Object_t * (*) (List_1_t1_971 *, const MethodInfo*))List_1_System_Collections_IEnumerable_GetEnumerator_m1_11107_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<System.Single>::System.Collections.IList.Add(System.Object)
extern "C" int32_t List_1_System_Collections_IList_Add_m1_11108_gshared (List_1_t1_971 * __this, Object_t * ___item, const MethodInfo* method);
#define List_1_System_Collections_IList_Add_m1_11108(__this, ___item, method) (( int32_t (*) (List_1_t1_971 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Add_m1_11108_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.List`1<System.Single>::System.Collections.IList.Contains(System.Object)
extern "C" bool List_1_System_Collections_IList_Contains_m1_11109_gshared (List_1_t1_971 * __this, Object_t * ___item, const MethodInfo* method);
#define List_1_System_Collections_IList_Contains_m1_11109(__this, ___item, method) (( bool (*) (List_1_t1_971 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Contains_m1_11109_gshared)(__this, ___item, method)
// System.Int32 System.Collections.Generic.List`1<System.Single>::System.Collections.IList.IndexOf(System.Object)
extern "C" int32_t List_1_System_Collections_IList_IndexOf_m1_11110_gshared (List_1_t1_971 * __this, Object_t * ___item, const MethodInfo* method);
#define List_1_System_Collections_IList_IndexOf_m1_11110(__this, ___item, method) (( int32_t (*) (List_1_t1_971 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_IndexOf_m1_11110_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<System.Single>::System.Collections.IList.Insert(System.Int32,System.Object)
extern "C" void List_1_System_Collections_IList_Insert_m1_11111_gshared (List_1_t1_971 * __this, int32_t ___index, Object_t * ___item, const MethodInfo* method);
#define List_1_System_Collections_IList_Insert_m1_11111(__this, ___index, ___item, method) (( void (*) (List_1_t1_971 *, int32_t, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Insert_m1_11111_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.Generic.List`1<System.Single>::System.Collections.IList.Remove(System.Object)
extern "C" void List_1_System_Collections_IList_Remove_m1_11112_gshared (List_1_t1_971 * __this, Object_t * ___item, const MethodInfo* method);
#define List_1_System_Collections_IList_Remove_m1_11112(__this, ___item, method) (( void (*) (List_1_t1_971 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Remove_m1_11112_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.List`1<System.Single>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C" bool List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m1_11113_gshared (List_1_t1_971 * __this, const MethodInfo* method);
#define List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m1_11113(__this, method) (( bool (*) (List_1_t1_971 *, const MethodInfo*))List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m1_11113_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<System.Single>::System.Collections.ICollection.get_SyncRoot()
extern "C" Object_t * List_1_System_Collections_ICollection_get_SyncRoot_m1_11114_gshared (List_1_t1_971 * __this, const MethodInfo* method);
#define List_1_System_Collections_ICollection_get_SyncRoot_m1_11114(__this, method) (( Object_t * (*) (List_1_t1_971 *, const MethodInfo*))List_1_System_Collections_ICollection_get_SyncRoot_m1_11114_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<System.Single>::System.Collections.IList.get_Item(System.Int32)
extern "C" Object_t * List_1_System_Collections_IList_get_Item_m1_11115_gshared (List_1_t1_971 * __this, int32_t ___index, const MethodInfo* method);
#define List_1_System_Collections_IList_get_Item_m1_11115(__this, ___index, method) (( Object_t * (*) (List_1_t1_971 *, int32_t, const MethodInfo*))List_1_System_Collections_IList_get_Item_m1_11115_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<System.Single>::System.Collections.IList.set_Item(System.Int32,System.Object)
extern "C" void List_1_System_Collections_IList_set_Item_m1_11116_gshared (List_1_t1_971 * __this, int32_t ___index, Object_t * ___value, const MethodInfo* method);
#define List_1_System_Collections_IList_set_Item_m1_11116(__this, ___index, ___value, method) (( void (*) (List_1_t1_971 *, int32_t, Object_t *, const MethodInfo*))List_1_System_Collections_IList_set_Item_m1_11116_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.Generic.List`1<System.Single>::Add(T)
extern "C" void List_1_Add_m1_11117_gshared (List_1_t1_971 * __this, float ___item, const MethodInfo* method);
#define List_1_Add_m1_11117(__this, ___item, method) (( void (*) (List_1_t1_971 *, float, const MethodInfo*))List_1_Add_m1_11117_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<System.Single>::GrowIfNeeded(System.Int32)
extern "C" void List_1_GrowIfNeeded_m1_11118_gshared (List_1_t1_971 * __this, int32_t ___newCount, const MethodInfo* method);
#define List_1_GrowIfNeeded_m1_11118(__this, ___newCount, method) (( void (*) (List_1_t1_971 *, int32_t, const MethodInfo*))List_1_GrowIfNeeded_m1_11118_gshared)(__this, ___newCount, method)
// System.Void System.Collections.Generic.List`1<System.Single>::AddCollection(System.Collections.Generic.ICollection`1<T>)
extern "C" void List_1_AddCollection_m1_11119_gshared (List_1_t1_971 * __this, Object_t* ___collection, const MethodInfo* method);
#define List_1_AddCollection_m1_11119(__this, ___collection, method) (( void (*) (List_1_t1_971 *, Object_t*, const MethodInfo*))List_1_AddCollection_m1_11119_gshared)(__this, ___collection, method)
// System.Void System.Collections.Generic.List`1<System.Single>::AddEnumerable(System.Collections.Generic.IEnumerable`1<T>)
extern "C" void List_1_AddEnumerable_m1_11120_gshared (List_1_t1_971 * __this, Object_t* ___enumerable, const MethodInfo* method);
#define List_1_AddEnumerable_m1_11120(__this, ___enumerable, method) (( void (*) (List_1_t1_971 *, Object_t*, const MethodInfo*))List_1_AddEnumerable_m1_11120_gshared)(__this, ___enumerable, method)
// System.Void System.Collections.Generic.List`1<System.Single>::AddRange(System.Collections.Generic.IEnumerable`1<T>)
extern "C" void List_1_AddRange_m1_11121_gshared (List_1_t1_971 * __this, Object_t* ___collection, const MethodInfo* method);
#define List_1_AddRange_m1_11121(__this, ___collection, method) (( void (*) (List_1_t1_971 *, Object_t*, const MethodInfo*))List_1_AddRange_m1_11121_gshared)(__this, ___collection, method)
// System.Void System.Collections.Generic.List`1<System.Single>::Clear()
extern "C" void List_1_Clear_m1_11122_gshared (List_1_t1_971 * __this, const MethodInfo* method);
#define List_1_Clear_m1_11122(__this, method) (( void (*) (List_1_t1_971 *, const MethodInfo*))List_1_Clear_m1_11122_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<System.Single>::Contains(T)
extern "C" bool List_1_Contains_m1_11123_gshared (List_1_t1_971 * __this, float ___item, const MethodInfo* method);
#define List_1_Contains_m1_11123(__this, ___item, method) (( bool (*) (List_1_t1_971 *, float, const MethodInfo*))List_1_Contains_m1_11123_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<System.Single>::CopyTo(T[],System.Int32)
extern "C" void List_1_CopyTo_m1_11124_gshared (List_1_t1_971 * __this, SingleU5BU5D_t1_863* ___array, int32_t ___arrayIndex, const MethodInfo* method);
#define List_1_CopyTo_m1_11124(__this, ___array, ___arrayIndex, method) (( void (*) (List_1_t1_971 *, SingleU5BU5D_t1_863*, int32_t, const MethodInfo*))List_1_CopyTo_m1_11124_gshared)(__this, ___array, ___arrayIndex, method)
// System.Void System.Collections.Generic.List`1<System.Single>::CheckMatch(System.Predicate`1<T>)
extern "C" void List_1_CheckMatch_m1_11125_gshared (Object_t * __this /* static, unused */, Predicate_1_t1_1400 * ___match, const MethodInfo* method);
#define List_1_CheckMatch_m1_11125(__this /* static, unused */, ___match, method) (( void (*) (Object_t * /* static, unused */, Predicate_1_t1_1400 *, const MethodInfo*))List_1_CheckMatch_m1_11125_gshared)(__this /* static, unused */, ___match, method)
// System.Int32 System.Collections.Generic.List`1<System.Single>::FindIndex(System.Predicate`1<T>)
extern "C" int32_t List_1_FindIndex_m1_11126_gshared (List_1_t1_971 * __this, Predicate_1_t1_1400 * ___match, const MethodInfo* method);
#define List_1_FindIndex_m1_11126(__this, ___match, method) (( int32_t (*) (List_1_t1_971 *, Predicate_1_t1_1400 *, const MethodInfo*))List_1_FindIndex_m1_11126_gshared)(__this, ___match, method)
// System.Int32 System.Collections.Generic.List`1<System.Single>::GetIndex(System.Int32,System.Int32,System.Predicate`1<T>)
extern "C" int32_t List_1_GetIndex_m1_11127_gshared (List_1_t1_971 * __this, int32_t ___startIndex, int32_t ___count, Predicate_1_t1_1400 * ___match, const MethodInfo* method);
#define List_1_GetIndex_m1_11127(__this, ___startIndex, ___count, ___match, method) (( int32_t (*) (List_1_t1_971 *, int32_t, int32_t, Predicate_1_t1_1400 *, const MethodInfo*))List_1_GetIndex_m1_11127_gshared)(__this, ___startIndex, ___count, ___match, method)
// System.Collections.Generic.List`1/Enumerator<T> System.Collections.Generic.List`1<System.Single>::GetEnumerator()
extern "C" Enumerator_t1_1396  List_1_GetEnumerator_m1_11128_gshared (List_1_t1_971 * __this, const MethodInfo* method);
#define List_1_GetEnumerator_m1_11128(__this, method) (( Enumerator_t1_1396  (*) (List_1_t1_971 *, const MethodInfo*))List_1_GetEnumerator_m1_11128_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<System.Single>::IndexOf(T)
extern "C" int32_t List_1_IndexOf_m1_11129_gshared (List_1_t1_971 * __this, float ___item, const MethodInfo* method);
#define List_1_IndexOf_m1_11129(__this, ___item, method) (( int32_t (*) (List_1_t1_971 *, float, const MethodInfo*))List_1_IndexOf_m1_11129_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<System.Single>::Shift(System.Int32,System.Int32)
extern "C" void List_1_Shift_m1_11130_gshared (List_1_t1_971 * __this, int32_t ___start, int32_t ___delta, const MethodInfo* method);
#define List_1_Shift_m1_11130(__this, ___start, ___delta, method) (( void (*) (List_1_t1_971 *, int32_t, int32_t, const MethodInfo*))List_1_Shift_m1_11130_gshared)(__this, ___start, ___delta, method)
// System.Void System.Collections.Generic.List`1<System.Single>::CheckIndex(System.Int32)
extern "C" void List_1_CheckIndex_m1_11131_gshared (List_1_t1_971 * __this, int32_t ___index, const MethodInfo* method);
#define List_1_CheckIndex_m1_11131(__this, ___index, method) (( void (*) (List_1_t1_971 *, int32_t, const MethodInfo*))List_1_CheckIndex_m1_11131_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<System.Single>::Insert(System.Int32,T)
extern "C" void List_1_Insert_m1_11132_gshared (List_1_t1_971 * __this, int32_t ___index, float ___item, const MethodInfo* method);
#define List_1_Insert_m1_11132(__this, ___index, ___item, method) (( void (*) (List_1_t1_971 *, int32_t, float, const MethodInfo*))List_1_Insert_m1_11132_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.Generic.List`1<System.Single>::CheckCollection(System.Collections.Generic.IEnumerable`1<T>)
extern "C" void List_1_CheckCollection_m1_11133_gshared (List_1_t1_971 * __this, Object_t* ___collection, const MethodInfo* method);
#define List_1_CheckCollection_m1_11133(__this, ___collection, method) (( void (*) (List_1_t1_971 *, Object_t*, const MethodInfo*))List_1_CheckCollection_m1_11133_gshared)(__this, ___collection, method)
// System.Boolean System.Collections.Generic.List`1<System.Single>::Remove(T)
extern "C" bool List_1_Remove_m1_11134_gshared (List_1_t1_971 * __this, float ___item, const MethodInfo* method);
#define List_1_Remove_m1_11134(__this, ___item, method) (( bool (*) (List_1_t1_971 *, float, const MethodInfo*))List_1_Remove_m1_11134_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<System.Single>::RemoveAt(System.Int32)
extern "C" void List_1_RemoveAt_m1_11135_gshared (List_1_t1_971 * __this, int32_t ___index, const MethodInfo* method);
#define List_1_RemoveAt_m1_11135(__this, ___index, method) (( void (*) (List_1_t1_971 *, int32_t, const MethodInfo*))List_1_RemoveAt_m1_11135_gshared)(__this, ___index, method)
// T[] System.Collections.Generic.List`1<System.Single>::ToArray()
extern "C" SingleU5BU5D_t1_863* List_1_ToArray_m1_5685_gshared (List_1_t1_971 * __this, const MethodInfo* method);
#define List_1_ToArray_m1_5685(__this, method) (( SingleU5BU5D_t1_863* (*) (List_1_t1_971 *, const MethodInfo*))List_1_ToArray_m1_5685_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<System.Single>::get_Capacity()
extern "C" int32_t List_1_get_Capacity_m1_11136_gshared (List_1_t1_971 * __this, const MethodInfo* method);
#define List_1_get_Capacity_m1_11136(__this, method) (( int32_t (*) (List_1_t1_971 *, const MethodInfo*))List_1_get_Capacity_m1_11136_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<System.Single>::set_Capacity(System.Int32)
extern "C" void List_1_set_Capacity_m1_11137_gshared (List_1_t1_971 * __this, int32_t ___value, const MethodInfo* method);
#define List_1_set_Capacity_m1_11137(__this, ___value, method) (( void (*) (List_1_t1_971 *, int32_t, const MethodInfo*))List_1_set_Capacity_m1_11137_gshared)(__this, ___value, method)
// System.Int32 System.Collections.Generic.List`1<System.Single>::get_Count()
extern "C" int32_t List_1_get_Count_m1_11138_gshared (List_1_t1_971 * __this, const MethodInfo* method);
#define List_1_get_Count_m1_11138(__this, method) (( int32_t (*) (List_1_t1_971 *, const MethodInfo*))List_1_get_Count_m1_11138_gshared)(__this, method)
// T System.Collections.Generic.List`1<System.Single>::get_Item(System.Int32)
extern "C" float List_1_get_Item_m1_11139_gshared (List_1_t1_971 * __this, int32_t ___index, const MethodInfo* method);
#define List_1_get_Item_m1_11139(__this, ___index, method) (( float (*) (List_1_t1_971 *, int32_t, const MethodInfo*))List_1_get_Item_m1_11139_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<System.Single>::set_Item(System.Int32,T)
extern "C" void List_1_set_Item_m1_11140_gshared (List_1_t1_971 * __this, int32_t ___index, float ___value, const MethodInfo* method);
#define List_1_set_Item_m1_11140(__this, ___index, ___value, method) (( void (*) (List_1_t1_971 *, int32_t, float, const MethodInfo*))List_1_set_Item_m1_11140_gshared)(__this, ___index, ___value, method)
