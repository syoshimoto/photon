﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.Collider
struct Collider_t6_100;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Bounds.h"

// UnityEngine.Bounds UnityEngine.Collider::get_bounds()
extern "C" Bounds_t6_54  Collider_get_bounds_m6_728 (Collider_t6_100 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Collider::INTERNAL_get_bounds(UnityEngine.Bounds&)
extern "C" void Collider_INTERNAL_get_bounds_m6_729 (Collider_t6_100 * __this, Bounds_t6_54 * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
