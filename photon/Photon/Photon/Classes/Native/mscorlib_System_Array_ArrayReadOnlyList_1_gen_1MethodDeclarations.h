﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Array/ArrayReadOnlyList`1<System.Reflection.CustomAttributeNamedArgument>
struct ArrayReadOnlyList_1_t1_1086;
// System.Reflection.CustomAttributeNamedArgument[]
struct CustomAttributeNamedArgumentU5BU5D_t1_839;
// System.Collections.IEnumerator
struct IEnumerator_t1_130;
// System.Collections.Generic.IEnumerator`1<System.Reflection.CustomAttributeNamedArgument>
struct IEnumerator_1_t1_1414;
// System.Exception
struct Exception_t1_33;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Reflection_CustomAttributeNamedArgument.h"

// System.Void System.Array/ArrayReadOnlyList`1<System.Reflection.CustomAttributeNamedArgument>::.ctor(T[])
extern "C" void ArrayReadOnlyList_1__ctor_m1_6799_gshared (ArrayReadOnlyList_1_t1_1086 * __this, CustomAttributeNamedArgumentU5BU5D_t1_839* ___array, const MethodInfo* method);
#define ArrayReadOnlyList_1__ctor_m1_6799(__this, ___array, method) (( void (*) (ArrayReadOnlyList_1_t1_1086 *, CustomAttributeNamedArgumentU5BU5D_t1_839*, const MethodInfo*))ArrayReadOnlyList_1__ctor_m1_6799_gshared)(__this, ___array, method)
// System.Collections.IEnumerator System.Array/ArrayReadOnlyList`1<System.Reflection.CustomAttributeNamedArgument>::System.Collections.IEnumerable.GetEnumerator()
extern "C" Object_t * ArrayReadOnlyList_1_System_Collections_IEnumerable_GetEnumerator_m1_6800_gshared (ArrayReadOnlyList_1_t1_1086 * __this, const MethodInfo* method);
#define ArrayReadOnlyList_1_System_Collections_IEnumerable_GetEnumerator_m1_6800(__this, method) (( Object_t * (*) (ArrayReadOnlyList_1_t1_1086 *, const MethodInfo*))ArrayReadOnlyList_1_System_Collections_IEnumerable_GetEnumerator_m1_6800_gshared)(__this, method)
// T System.Array/ArrayReadOnlyList`1<System.Reflection.CustomAttributeNamedArgument>::get_Item(System.Int32)
extern "C" CustomAttributeNamedArgument_t1_331  ArrayReadOnlyList_1_get_Item_m1_6801_gshared (ArrayReadOnlyList_1_t1_1086 * __this, int32_t ___index, const MethodInfo* method);
#define ArrayReadOnlyList_1_get_Item_m1_6801(__this, ___index, method) (( CustomAttributeNamedArgument_t1_331  (*) (ArrayReadOnlyList_1_t1_1086 *, int32_t, const MethodInfo*))ArrayReadOnlyList_1_get_Item_m1_6801_gshared)(__this, ___index, method)
// System.Void System.Array/ArrayReadOnlyList`1<System.Reflection.CustomAttributeNamedArgument>::set_Item(System.Int32,T)
extern "C" void ArrayReadOnlyList_1_set_Item_m1_6802_gshared (ArrayReadOnlyList_1_t1_1086 * __this, int32_t ___index, CustomAttributeNamedArgument_t1_331  ___value, const MethodInfo* method);
#define ArrayReadOnlyList_1_set_Item_m1_6802(__this, ___index, ___value, method) (( void (*) (ArrayReadOnlyList_1_t1_1086 *, int32_t, CustomAttributeNamedArgument_t1_331 , const MethodInfo*))ArrayReadOnlyList_1_set_Item_m1_6802_gshared)(__this, ___index, ___value, method)
// System.Int32 System.Array/ArrayReadOnlyList`1<System.Reflection.CustomAttributeNamedArgument>::get_Count()
extern "C" int32_t ArrayReadOnlyList_1_get_Count_m1_6803_gshared (ArrayReadOnlyList_1_t1_1086 * __this, const MethodInfo* method);
#define ArrayReadOnlyList_1_get_Count_m1_6803(__this, method) (( int32_t (*) (ArrayReadOnlyList_1_t1_1086 *, const MethodInfo*))ArrayReadOnlyList_1_get_Count_m1_6803_gshared)(__this, method)
// System.Boolean System.Array/ArrayReadOnlyList`1<System.Reflection.CustomAttributeNamedArgument>::get_IsReadOnly()
extern "C" bool ArrayReadOnlyList_1_get_IsReadOnly_m1_6804_gshared (ArrayReadOnlyList_1_t1_1086 * __this, const MethodInfo* method);
#define ArrayReadOnlyList_1_get_IsReadOnly_m1_6804(__this, method) (( bool (*) (ArrayReadOnlyList_1_t1_1086 *, const MethodInfo*))ArrayReadOnlyList_1_get_IsReadOnly_m1_6804_gshared)(__this, method)
// System.Void System.Array/ArrayReadOnlyList`1<System.Reflection.CustomAttributeNamedArgument>::Add(T)
extern "C" void ArrayReadOnlyList_1_Add_m1_6805_gshared (ArrayReadOnlyList_1_t1_1086 * __this, CustomAttributeNamedArgument_t1_331  ___item, const MethodInfo* method);
#define ArrayReadOnlyList_1_Add_m1_6805(__this, ___item, method) (( void (*) (ArrayReadOnlyList_1_t1_1086 *, CustomAttributeNamedArgument_t1_331 , const MethodInfo*))ArrayReadOnlyList_1_Add_m1_6805_gshared)(__this, ___item, method)
// System.Void System.Array/ArrayReadOnlyList`1<System.Reflection.CustomAttributeNamedArgument>::Clear()
extern "C" void ArrayReadOnlyList_1_Clear_m1_6806_gshared (ArrayReadOnlyList_1_t1_1086 * __this, const MethodInfo* method);
#define ArrayReadOnlyList_1_Clear_m1_6806(__this, method) (( void (*) (ArrayReadOnlyList_1_t1_1086 *, const MethodInfo*))ArrayReadOnlyList_1_Clear_m1_6806_gshared)(__this, method)
// System.Boolean System.Array/ArrayReadOnlyList`1<System.Reflection.CustomAttributeNamedArgument>::Contains(T)
extern "C" bool ArrayReadOnlyList_1_Contains_m1_6807_gshared (ArrayReadOnlyList_1_t1_1086 * __this, CustomAttributeNamedArgument_t1_331  ___item, const MethodInfo* method);
#define ArrayReadOnlyList_1_Contains_m1_6807(__this, ___item, method) (( bool (*) (ArrayReadOnlyList_1_t1_1086 *, CustomAttributeNamedArgument_t1_331 , const MethodInfo*))ArrayReadOnlyList_1_Contains_m1_6807_gshared)(__this, ___item, method)
// System.Void System.Array/ArrayReadOnlyList`1<System.Reflection.CustomAttributeNamedArgument>::CopyTo(T[],System.Int32)
extern "C" void ArrayReadOnlyList_1_CopyTo_m1_6808_gshared (ArrayReadOnlyList_1_t1_1086 * __this, CustomAttributeNamedArgumentU5BU5D_t1_839* ___array, int32_t ___index, const MethodInfo* method);
#define ArrayReadOnlyList_1_CopyTo_m1_6808(__this, ___array, ___index, method) (( void (*) (ArrayReadOnlyList_1_t1_1086 *, CustomAttributeNamedArgumentU5BU5D_t1_839*, int32_t, const MethodInfo*))ArrayReadOnlyList_1_CopyTo_m1_6808_gshared)(__this, ___array, ___index, method)
// System.Collections.Generic.IEnumerator`1<T> System.Array/ArrayReadOnlyList`1<System.Reflection.CustomAttributeNamedArgument>::GetEnumerator()
extern "C" Object_t* ArrayReadOnlyList_1_GetEnumerator_m1_6809_gshared (ArrayReadOnlyList_1_t1_1086 * __this, const MethodInfo* method);
#define ArrayReadOnlyList_1_GetEnumerator_m1_6809(__this, method) (( Object_t* (*) (ArrayReadOnlyList_1_t1_1086 *, const MethodInfo*))ArrayReadOnlyList_1_GetEnumerator_m1_6809_gshared)(__this, method)
// System.Int32 System.Array/ArrayReadOnlyList`1<System.Reflection.CustomAttributeNamedArgument>::IndexOf(T)
extern "C" int32_t ArrayReadOnlyList_1_IndexOf_m1_6810_gshared (ArrayReadOnlyList_1_t1_1086 * __this, CustomAttributeNamedArgument_t1_331  ___item, const MethodInfo* method);
#define ArrayReadOnlyList_1_IndexOf_m1_6810(__this, ___item, method) (( int32_t (*) (ArrayReadOnlyList_1_t1_1086 *, CustomAttributeNamedArgument_t1_331 , const MethodInfo*))ArrayReadOnlyList_1_IndexOf_m1_6810_gshared)(__this, ___item, method)
// System.Void System.Array/ArrayReadOnlyList`1<System.Reflection.CustomAttributeNamedArgument>::Insert(System.Int32,T)
extern "C" void ArrayReadOnlyList_1_Insert_m1_6811_gshared (ArrayReadOnlyList_1_t1_1086 * __this, int32_t ___index, CustomAttributeNamedArgument_t1_331  ___item, const MethodInfo* method);
#define ArrayReadOnlyList_1_Insert_m1_6811(__this, ___index, ___item, method) (( void (*) (ArrayReadOnlyList_1_t1_1086 *, int32_t, CustomAttributeNamedArgument_t1_331 , const MethodInfo*))ArrayReadOnlyList_1_Insert_m1_6811_gshared)(__this, ___index, ___item, method)
// System.Boolean System.Array/ArrayReadOnlyList`1<System.Reflection.CustomAttributeNamedArgument>::Remove(T)
extern "C" bool ArrayReadOnlyList_1_Remove_m1_6812_gshared (ArrayReadOnlyList_1_t1_1086 * __this, CustomAttributeNamedArgument_t1_331  ___item, const MethodInfo* method);
#define ArrayReadOnlyList_1_Remove_m1_6812(__this, ___item, method) (( bool (*) (ArrayReadOnlyList_1_t1_1086 *, CustomAttributeNamedArgument_t1_331 , const MethodInfo*))ArrayReadOnlyList_1_Remove_m1_6812_gshared)(__this, ___item, method)
// System.Void System.Array/ArrayReadOnlyList`1<System.Reflection.CustomAttributeNamedArgument>::RemoveAt(System.Int32)
extern "C" void ArrayReadOnlyList_1_RemoveAt_m1_6813_gshared (ArrayReadOnlyList_1_t1_1086 * __this, int32_t ___index, const MethodInfo* method);
#define ArrayReadOnlyList_1_RemoveAt_m1_6813(__this, ___index, method) (( void (*) (ArrayReadOnlyList_1_t1_1086 *, int32_t, const MethodInfo*))ArrayReadOnlyList_1_RemoveAt_m1_6813_gshared)(__this, ___index, method)
// System.Exception System.Array/ArrayReadOnlyList`1<System.Reflection.CustomAttributeNamedArgument>::ReadOnlyError()
extern "C" Exception_t1_33 * ArrayReadOnlyList_1_ReadOnlyError_m1_6814_gshared (Object_t * __this /* static, unused */, const MethodInfo* method);
#define ArrayReadOnlyList_1_ReadOnlyError_m1_6814(__this /* static, unused */, method) (( Exception_t1_33 * (*) (Object_t * /* static, unused */, const MethodInfo*))ArrayReadOnlyList_1_ReadOnlyError_m1_6814_gshared)(__this /* static, unused */, method)
