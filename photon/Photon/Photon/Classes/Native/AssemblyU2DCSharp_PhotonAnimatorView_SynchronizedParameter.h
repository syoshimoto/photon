﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "mscorlib_System_Object.h"
#include "AssemblyU2DCSharp_PhotonAnimatorView_ParameterType.h"
#include "AssemblyU2DCSharp_PhotonAnimatorView_SynchronizeType.h"

// PhotonAnimatorView/SynchronizedParameter
struct  SynchronizedParameter_t8_128  : public Object_t
{
	// PhotonAnimatorView/ParameterType PhotonAnimatorView/SynchronizedParameter::Type
	int32_t ___Type_0;
	// PhotonAnimatorView/SynchronizeType PhotonAnimatorView/SynchronizedParameter::SynchronizeType
	int32_t ___SynchronizeType_1;
	// System.String PhotonAnimatorView/SynchronizedParameter::Name
	String_t* ___Name_2;
};
