﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.IO.MemoryStream
struct MemoryStream_t1_244;
// System.Object
struct Object_t;
// System.IAsyncResult
struct IAsyncResult_t1_27;
// System.AsyncCallback
struct AsyncCallback_t1_28;

#include "mscorlib_System_MulticastDelegate.h"

// ExitGames.Client.Photon.SerializeStreamMethod
struct  SerializeStreamMethod_t5_47  : public MulticastDelegate_t1_21
{
};
