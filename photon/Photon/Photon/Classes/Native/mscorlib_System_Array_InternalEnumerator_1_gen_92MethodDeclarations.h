﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Array
struct Array_t;
// System.Object
struct Object_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen_92.h"
#include "UnityEngine_UnityEngine_CharacterInfo.h"

// System.Void System.Array/InternalEnumerator`1<UnityEngine.CharacterInfo>::.ctor(System.Array)
extern "C" void InternalEnumerator_1__ctor_m1_8374_gshared (InternalEnumerator_1_t1_1226 * __this, Array_t * ___array, const MethodInfo* method);
#define InternalEnumerator_1__ctor_m1_8374(__this, ___array, method) (( void (*) (InternalEnumerator_1_t1_1226 *, Array_t *, const MethodInfo*))InternalEnumerator_1__ctor_m1_8374_gshared)(__this, ___array, method)
// System.Void System.Array/InternalEnumerator`1<UnityEngine.CharacterInfo>::System.Collections.IEnumerator.Reset()
extern "C" void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1_8375_gshared (InternalEnumerator_1_t1_1226 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1_8375(__this, method) (( void (*) (InternalEnumerator_1_t1_1226 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1_8375_gshared)(__this, method)
// System.Object System.Array/InternalEnumerator`1<UnityEngine.CharacterInfo>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1_8376_gshared (InternalEnumerator_1_t1_1226 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1_8376(__this, method) (( Object_t * (*) (InternalEnumerator_1_t1_1226 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1_8376_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<UnityEngine.CharacterInfo>::Dispose()
extern "C" void InternalEnumerator_1_Dispose_m1_8377_gshared (InternalEnumerator_1_t1_1226 * __this, const MethodInfo* method);
#define InternalEnumerator_1_Dispose_m1_8377(__this, method) (( void (*) (InternalEnumerator_1_t1_1226 *, const MethodInfo*))InternalEnumerator_1_Dispose_m1_8377_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.CharacterInfo>::MoveNext()
extern "C" bool InternalEnumerator_1_MoveNext_m1_8378_gshared (InternalEnumerator_1_t1_1226 * __this, const MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m1_8378(__this, method) (( bool (*) (InternalEnumerator_1_t1_1226 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m1_8378_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<UnityEngine.CharacterInfo>::get_Current()
extern "C" CharacterInfo_t6_146  InternalEnumerator_1_get_Current_m1_8379_gshared (InternalEnumerator_1_t1_1226 * __this, const MethodInfo* method);
#define InternalEnumerator_1_get_Current_m1_8379(__this, method) (( CharacterInfo_t6_146  (*) (InternalEnumerator_1_t1_1226 *, const MethodInfo*))InternalEnumerator_1_get_Current_m1_8379_gshared)(__this, method)
