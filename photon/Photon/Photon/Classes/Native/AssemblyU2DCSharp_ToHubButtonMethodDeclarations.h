﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ToHubButton
struct ToHubButton_t8_24;

#include "codegen/il2cpp-codegen.h"

// System.Void ToHubButton::.ctor()
extern "C" void ToHubButton__ctor_m8_86 (ToHubButton_t8_24 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ToHubButton ToHubButton::get_Instance()
extern "C" ToHubButton_t8_24 * ToHubButton_get_Instance_m8_87 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ToHubButton::Awake()
extern "C" void ToHubButton_Awake_m8_88 (ToHubButton_t8_24 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ToHubButton::Start()
extern "C" void ToHubButton_Start_m8_89 (ToHubButton_t8_24 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ToHubButton::OnGUI()
extern "C" void ToHubButton_OnGUI_m8_90 (ToHubButton_t8_24 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
