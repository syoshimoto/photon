﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Net.Sockets.Socket
struct Socket_t3_28;

#include "Photon3Unity3D_ExitGames_Client_Photon_PhotonPing.h"

// ExitGames.Client.Photon.PingMono
struct  PingMono_t5_40  : public PhotonPing_t5_39
{
	// System.Net.Sockets.Socket ExitGames.Client.Photon.PingMono::sock
	Socket_t3_28 * ___sock_6;
};
