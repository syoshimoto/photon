﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_7MethodDeclarations.h"

// System.Void System.Collections.Generic.KeyValuePair`2<System.Int32,ExitGames.Client.Photon.NCommand>::.ctor(TKey,TValue)
#define KeyValuePair_2__ctor_m1_7544(__this, ___key, ___value, method) (( void (*) (KeyValuePair_2_t1_1170 *, int32_t, NCommand_t5_13 *, const MethodInfo*))KeyValuePair_2__ctor_m1_7459_gshared)(__this, ___key, ___value, method)
// TKey System.Collections.Generic.KeyValuePair`2<System.Int32,ExitGames.Client.Photon.NCommand>::get_Key()
#define KeyValuePair_2_get_Key_m1_7545(__this, method) (( int32_t (*) (KeyValuePair_2_t1_1170 *, const MethodInfo*))KeyValuePair_2_get_Key_m1_7460_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.Int32,ExitGames.Client.Photon.NCommand>::set_Key(TKey)
#define KeyValuePair_2_set_Key_m1_7546(__this, ___value, method) (( void (*) (KeyValuePair_2_t1_1170 *, int32_t, const MethodInfo*))KeyValuePair_2_set_Key_m1_7461_gshared)(__this, ___value, method)
// TValue System.Collections.Generic.KeyValuePair`2<System.Int32,ExitGames.Client.Photon.NCommand>::get_Value()
#define KeyValuePair_2_get_Value_m1_7547(__this, method) (( NCommand_t5_13 * (*) (KeyValuePair_2_t1_1170 *, const MethodInfo*))KeyValuePair_2_get_Value_m1_7462_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.Int32,ExitGames.Client.Photon.NCommand>::set_Value(TValue)
#define KeyValuePair_2_set_Value_m1_7548(__this, ___value, method) (( void (*) (KeyValuePair_2_t1_1170 *, NCommand_t5_13 *, const MethodInfo*))KeyValuePair_2_set_Value_m1_7463_gshared)(__this, ___value, method)
// System.String System.Collections.Generic.KeyValuePair`2<System.Int32,ExitGames.Client.Photon.NCommand>::ToString()
#define KeyValuePair_2_ToString_m1_7549(__this, method) (( String_t* (*) (KeyValuePair_2_t1_1170 *, const MethodInfo*))KeyValuePair_2_ToString_m1_7464_gshared)(__this, method)
