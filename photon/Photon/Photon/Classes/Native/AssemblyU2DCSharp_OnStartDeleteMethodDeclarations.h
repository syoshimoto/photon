﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// OnStartDelete
struct OnStartDelete_t8_162;

#include "codegen/il2cpp-codegen.h"

// System.Void OnStartDelete::.ctor()
extern "C" void OnStartDelete__ctor_m8_979 (OnStartDelete_t8_162 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OnStartDelete::Start()
extern "C" void OnStartDelete_Start_m8_980 (OnStartDelete_t8_162 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
