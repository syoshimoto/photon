﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_ValueCollec_29MethodDeclarations.h"

// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<PunTeams/Team,System.Collections.Generic.List`1<PhotonPlayer>>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
#define ValueCollection__ctor_m1_9883(__this, ___dictionary, method) (( void (*) (ValueCollection_t1_1334 *, Dictionary_2_t1_951 *, const MethodInfo*))ValueCollection__ctor_m1_9719_gshared)(__this, ___dictionary, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<PunTeams/Team,System.Collections.Generic.List`1<PhotonPlayer>>::System.Collections.Generic.ICollection<TValue>.Add(TValue)
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m1_9884(__this, ___item, method) (( void (*) (ValueCollection_t1_1334 *, List_1_t1_955 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m1_9720_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<PunTeams/Team,System.Collections.Generic.List`1<PhotonPlayer>>::System.Collections.Generic.ICollection<TValue>.Clear()
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m1_9885(__this, method) (( void (*) (ValueCollection_t1_1334 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m1_9721_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<PunTeams/Team,System.Collections.Generic.List`1<PhotonPlayer>>::System.Collections.Generic.ICollection<TValue>.Contains(TValue)
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m1_9886(__this, ___item, method) (( bool (*) (ValueCollection_t1_1334 *, List_1_t1_955 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m1_9722_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<PunTeams/Team,System.Collections.Generic.List`1<PhotonPlayer>>::System.Collections.Generic.ICollection<TValue>.Remove(TValue)
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m1_9887(__this, ___item, method) (( bool (*) (ValueCollection_t1_1334 *, List_1_t1_955 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m1_9723_gshared)(__this, ___item, method)
// System.Collections.Generic.IEnumerator`1<TValue> System.Collections.Generic.Dictionary`2/ValueCollection<PunTeams/Team,System.Collections.Generic.List`1<PhotonPlayer>>::System.Collections.Generic.IEnumerable<TValue>.GetEnumerator()
#define ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m1_9888(__this, method) (( Object_t* (*) (ValueCollection_t1_1334 *, const MethodInfo*))ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m1_9724_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<PunTeams/Team,System.Collections.Generic.List`1<PhotonPlayer>>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define ValueCollection_System_Collections_ICollection_CopyTo_m1_9889(__this, ___array, ___index, method) (( void (*) (ValueCollection_t1_1334 *, Array_t *, int32_t, const MethodInfo*))ValueCollection_System_Collections_ICollection_CopyTo_m1_9725_gshared)(__this, ___array, ___index, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2/ValueCollection<PunTeams/Team,System.Collections.Generic.List`1<PhotonPlayer>>::System.Collections.IEnumerable.GetEnumerator()
#define ValueCollection_System_Collections_IEnumerable_GetEnumerator_m1_9890(__this, method) (( Object_t * (*) (ValueCollection_t1_1334 *, const MethodInfo*))ValueCollection_System_Collections_IEnumerable_GetEnumerator_m1_9726_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<PunTeams/Team,System.Collections.Generic.List`1<PhotonPlayer>>::System.Collections.Generic.ICollection<TValue>.get_IsReadOnly()
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m1_9891(__this, method) (( bool (*) (ValueCollection_t1_1334 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m1_9727_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ValueCollection<PunTeams/Team,System.Collections.Generic.List`1<PhotonPlayer>>::System.Collections.ICollection.get_SyncRoot()
#define ValueCollection_System_Collections_ICollection_get_SyncRoot_m1_9892(__this, method) (( Object_t * (*) (ValueCollection_t1_1334 *, const MethodInfo*))ValueCollection_System_Collections_ICollection_get_SyncRoot_m1_9728_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<PunTeams/Team,System.Collections.Generic.List`1<PhotonPlayer>>::CopyTo(TValue[],System.Int32)
#define ValueCollection_CopyTo_m1_9893(__this, ___array, ___index, method) (( void (*) (ValueCollection_t1_1334 *, List_1U5BU5D_t1_1332*, int32_t, const MethodInfo*))ValueCollection_CopyTo_m1_9729_gshared)(__this, ___array, ___index, method)
// System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2/ValueCollection<PunTeams/Team,System.Collections.Generic.List`1<PhotonPlayer>>::GetEnumerator()
#define ValueCollection_GetEnumerator_m1_9894(__this, method) (( Enumerator_t1_1467  (*) (ValueCollection_t1_1334 *, const MethodInfo*))ValueCollection_GetEnumerator_m1_9730_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2/ValueCollection<PunTeams/Team,System.Collections.Generic.List`1<PhotonPlayer>>::get_Count()
#define ValueCollection_get_Count_m1_9895(__this, method) (( int32_t (*) (ValueCollection_t1_1334 *, const MethodInfo*))ValueCollection_get_Count_m1_9731_gshared)(__this, method)
