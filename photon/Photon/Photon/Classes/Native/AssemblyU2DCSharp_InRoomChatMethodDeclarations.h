﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// InRoomChat
struct InRoomChat_t8_150;
// System.String
struct String_t;
// PhotonMessageInfo
struct PhotonMessageInfo_t8_104;

#include "codegen/il2cpp-codegen.h"

// System.Void InRoomChat::.ctor()
extern "C" void InRoomChat__ctor_m8_930 (InRoomChat_t8_150 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void InRoomChat::.cctor()
extern "C" void InRoomChat__cctor_m8_931 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void InRoomChat::Start()
extern "C" void InRoomChat_Start_m8_932 (InRoomChat_t8_150 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void InRoomChat::OnGUI()
extern "C" void InRoomChat_OnGUI_m8_933 (InRoomChat_t8_150 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void InRoomChat::Chat(System.String,PhotonMessageInfo)
extern "C" void InRoomChat_Chat_m8_934 (InRoomChat_t8_150 * __this, String_t* ___newLine, PhotonMessageInfo_t8_104 * ___mi, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void InRoomChat::AddLine(System.String)
extern "C" void InRoomChat_AddLine_m8_935 (InRoomChat_t8_150 * __this, String_t* ___newLine, const MethodInfo* method) IL2CPP_METHOD_ATTR;
