﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.GameObject[]
struct GameObjectU5BU5D_t6_258;

#include "UnityEngine_UnityEngine_MonoBehaviour.h"

// MessageOverlay
struct  MessageOverlay_t8_28  : public MonoBehaviour_t6_80
{
	// UnityEngine.GameObject[] MessageOverlay::Objects
	GameObjectU5BU5D_t6_258* ___Objects_2;
};
