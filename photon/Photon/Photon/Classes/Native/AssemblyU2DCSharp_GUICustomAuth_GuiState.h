﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "mscorlib_System_Enum.h"
#include "AssemblyU2DCSharp_GUICustomAuth_GuiState.h"

// GUICustomAuth/GuiState
struct  GuiState_t8_16 
{
	// System.Int32 GUICustomAuth/GuiState::value__
	int32_t ___value___1;
};
