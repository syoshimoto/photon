﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "mscorlib_System_ValueType.h"
#include "UnityEngine_UnityEngine_Vector3.h"
#include "UnityEngine_UnityEngine_Quaternion.h"

// CubeInter/State
struct  State_t8_41 
{
	// System.Double CubeInter/State::timestamp
	double ___timestamp_0;
	// UnityEngine.Vector3 CubeInter/State::pos
	Vector3_t6_49  ___pos_1;
	// UnityEngine.Quaternion CubeInter/State::rot
	Quaternion_t6_51  ___rot_2;
};
