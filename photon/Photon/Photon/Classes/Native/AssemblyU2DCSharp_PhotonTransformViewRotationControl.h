﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// PhotonTransformViewRotationModel
struct PhotonTransformViewRotationModel_t8_139;

#include "mscorlib_System_Object.h"
#include "UnityEngine_UnityEngine_Quaternion.h"

// PhotonTransformViewRotationControl
struct  PhotonTransformViewRotationControl_t8_142  : public Object_t
{
	// PhotonTransformViewRotationModel PhotonTransformViewRotationControl::m_Model
	PhotonTransformViewRotationModel_t8_139 * ___m_Model_0;
	// UnityEngine.Quaternion PhotonTransformViewRotationControl::m_NetworkRotation
	Quaternion_t6_51  ___m_NetworkRotation_1;
};
