﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// DemoRPGMovement
struct DemoRPGMovement_t8_36;

#include "codegen/il2cpp-codegen.h"

// System.Void DemoRPGMovement::.ctor()
extern "C" void DemoRPGMovement__ctor_m8_140 (DemoRPGMovement_t8_36 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DemoRPGMovement::OnJoinedRoom()
extern "C" void DemoRPGMovement_OnJoinedRoom_m8_141 (DemoRPGMovement_t8_36 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DemoRPGMovement::CreatePlayerObject()
extern "C" void DemoRPGMovement_CreatePlayerObject_m8_142 (DemoRPGMovement_t8_36 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
