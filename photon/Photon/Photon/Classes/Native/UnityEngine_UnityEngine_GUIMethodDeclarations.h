﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.GUISkin
struct GUISkin_t6_161;
// System.String
struct String_t;
// UnityEngine.GUIStyle
struct GUIStyle_t6_166;
// UnityEngine.GUIContent
struct GUIContent_t6_163;
// UnityEngine.Texture
struct Texture_t6_32;
// UnityEngine.TextEditor
struct TextEditor_t6_238;
// UnityEngine.GUIContent[]
struct GUIContentU5BU5D_t6_264;
// UnityEngine.Rect[]
struct RectU5BU5D_t6_265;
// UnityEngine.GUI/WindowFunction
struct WindowFunction_t6_159;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_DateTime.h"
#include "UnityEngine_UnityEngine_Rect.h"
#include "UnityEngine_UnityEngine_FocusType.h"
#include "UnityEngine_UnityEngine_Vector2.h"
#include "UnityEngine_UnityEngine_Color.h"
#include "mscorlib_System_IntPtr.h"

// System.Void UnityEngine.GUI::.cctor()
extern "C" void GUI__cctor_m6_1035 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.DateTime UnityEngine.GUI::get_nextScrollStepTime()
extern "C" DateTime_t1_127  GUI_get_nextScrollStepTime_m6_1036 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.GUI::set_nextScrollStepTime(System.DateTime)
extern "C" void GUI_set_nextScrollStepTime_m6_1037 (Object_t * __this /* static, unused */, DateTime_t1_127  ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.GUI::get_scrollTroughSide()
extern "C" int32_t GUI_get_scrollTroughSide_m6_1038 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.GUI::set_scrollTroughSide(System.Int32)
extern "C" void GUI_set_scrollTroughSide_m6_1039 (Object_t * __this /* static, unused */, int32_t ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.GUI::set_skin(UnityEngine.GUISkin)
extern "C" void GUI_set_skin_m6_1040 (Object_t * __this /* static, unused */, GUISkin_t6_161 * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.GUISkin UnityEngine.GUI::get_skin()
extern "C" GUISkin_t6_161 * GUI_get_skin_m6_1041 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.GUI::DoSetSkin(UnityEngine.GUISkin)
extern "C" void GUI_DoSetSkin_m6_1042 (Object_t * __this /* static, unused */, GUISkin_t6_161 * ___newSkin, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.GUI::set_tooltip(System.String)
extern "C" void GUI_set_tooltip_m6_1043 (Object_t * __this /* static, unused */, String_t* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.GUI::Label(UnityEngine.Rect,System.String)
extern "C" void GUI_Label_m6_1044 (Object_t * __this /* static, unused */, Rect_t6_52  ___position, String_t* ___text, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.GUI::Label(UnityEngine.Rect,System.String,UnityEngine.GUIStyle)
extern "C" void GUI_Label_m6_1045 (Object_t * __this /* static, unused */, Rect_t6_52  ___position, String_t* ___text, GUIStyle_t6_166 * ___style, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.GUI::Label(UnityEngine.Rect,UnityEngine.GUIContent,UnityEngine.GUIStyle)
extern "C" void GUI_Label_m6_1046 (Object_t * __this /* static, unused */, Rect_t6_52  ___position, GUIContent_t6_163 * ___content, GUIStyle_t6_166 * ___style, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.GUI::Box(UnityEngine.Rect,System.String)
extern "C" void GUI_Box_m6_1047 (Object_t * __this /* static, unused */, Rect_t6_52  ___position, String_t* ___text, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.GUI::Box(UnityEngine.Rect,UnityEngine.GUIContent,UnityEngine.GUIStyle)
extern "C" void GUI_Box_m6_1048 (Object_t * __this /* static, unused */, Rect_t6_52  ___position, GUIContent_t6_163 * ___content, GUIStyle_t6_166 * ___style, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.GUI::Button(UnityEngine.Rect,UnityEngine.Texture,UnityEngine.GUIStyle)
extern "C" bool GUI_Button_m6_1049 (Object_t * __this /* static, unused */, Rect_t6_52  ___position, Texture_t6_32 * ___image, GUIStyle_t6_166 * ___style, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.GUI::Button(UnityEngine.Rect,UnityEngine.GUIContent,UnityEngine.GUIStyle)
extern "C" bool GUI_Button_m6_1050 (Object_t * __this /* static, unused */, Rect_t6_52  ___position, GUIContent_t6_163 * ___content, GUIStyle_t6_166 * ___style, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.GUI::DoRepeatButton(UnityEngine.Rect,UnityEngine.GUIContent,UnityEngine.GUIStyle,UnityEngine.FocusType)
extern "C" bool GUI_DoRepeatButton_m6_1051 (Object_t * __this /* static, unused */, Rect_t6_52  ___position, GUIContent_t6_163 * ___content, GUIStyle_t6_166 * ___style, int32_t ___focusType, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String UnityEngine.GUI::PasswordFieldGetStrToShow(System.String,System.Char)
extern "C" String_t* GUI_PasswordFieldGetStrToShow_m6_1052 (Object_t * __this /* static, unused */, String_t* ___password, uint16_t ___maskChar, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.GUI::DoTextField(UnityEngine.Rect,System.Int32,UnityEngine.GUIContent,System.Boolean,System.Int32,UnityEngine.GUIStyle)
extern "C" void GUI_DoTextField_m6_1053 (Object_t * __this /* static, unused */, Rect_t6_52  ___position, int32_t ___id, GUIContent_t6_163 * ___content, bool ___multiline, int32_t ___maxLength, GUIStyle_t6_166 * ___style, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.GUI::DoTextField(UnityEngine.Rect,System.Int32,UnityEngine.GUIContent,System.Boolean,System.Int32,UnityEngine.GUIStyle,System.String)
extern "C" void GUI_DoTextField_m6_1054 (Object_t * __this /* static, unused */, Rect_t6_52  ___position, int32_t ___id, GUIContent_t6_163 * ___content, bool ___multiline, int32_t ___maxLength, GUIStyle_t6_166 * ___style, String_t* ___secureText, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.GUI::DoTextField(UnityEngine.Rect,System.Int32,UnityEngine.GUIContent,System.Boolean,System.Int32,UnityEngine.GUIStyle,System.String,System.Char)
extern "C" void GUI_DoTextField_m6_1055 (Object_t * __this /* static, unused */, Rect_t6_52  ___position, int32_t ___id, GUIContent_t6_163 * ___content, bool ___multiline, int32_t ___maxLength, GUIStyle_t6_166 * ___style, String_t* ___secureText, uint16_t ___maskChar, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.GUI::HandleTextFieldEventForTouchscreen(UnityEngine.Rect,System.Int32,UnityEngine.GUIContent,System.Boolean,System.Int32,UnityEngine.GUIStyle,System.String,System.Char,UnityEngine.TextEditor)
extern "C" void GUI_HandleTextFieldEventForTouchscreen_m6_1056 (Object_t * __this /* static, unused */, Rect_t6_52  ___position, int32_t ___id, GUIContent_t6_163 * ___content, bool ___multiline, int32_t ___maxLength, GUIStyle_t6_166 * ___style, String_t* ___secureText, uint16_t ___maskChar, TextEditor_t6_238 * ___editor, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.GUI::HandleTextFieldEventForDesktop(UnityEngine.Rect,System.Int32,UnityEngine.GUIContent,System.Boolean,System.Int32,UnityEngine.GUIStyle,UnityEngine.TextEditor)
extern "C" void GUI_HandleTextFieldEventForDesktop_m6_1057 (Object_t * __this /* static, unused */, Rect_t6_52  ___position, int32_t ___id, GUIContent_t6_163 * ___content, bool ___multiline, int32_t ___maxLength, GUIStyle_t6_166 * ___style, TextEditor_t6_238 * ___editor, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.GUI::Toggle(UnityEngine.Rect,System.Boolean,UnityEngine.GUIContent,UnityEngine.GUIStyle)
extern "C" bool GUI_Toggle_m6_1058 (Object_t * __this /* static, unused */, Rect_t6_52  ___position, bool ___value, GUIContent_t6_163 * ___content, GUIStyle_t6_166 * ___style, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.GUI::Toolbar(UnityEngine.Rect,System.Int32,UnityEngine.GUIContent[],UnityEngine.GUIStyle)
extern "C" int32_t GUI_Toolbar_m6_1059 (Object_t * __this /* static, unused */, Rect_t6_52  ___position, int32_t ___selected, GUIContentU5BU5D_t6_264* ___contents, GUIStyle_t6_166 * ___style, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.GUI::FindStyles(UnityEngine.GUIStyle&,UnityEngine.GUIStyle&,UnityEngine.GUIStyle&,UnityEngine.GUIStyle&,System.String,System.String,System.String)
extern "C" void GUI_FindStyles_m6_1060 (Object_t * __this /* static, unused */, GUIStyle_t6_166 ** ___style, GUIStyle_t6_166 ** ___firstStyle, GUIStyle_t6_166 ** ___midStyle, GUIStyle_t6_166 ** ___lastStyle, String_t* ___first, String_t* ___mid, String_t* ___last, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.GUI::CalcTotalHorizSpacing(System.Int32,UnityEngine.GUIStyle,UnityEngine.GUIStyle,UnityEngine.GUIStyle,UnityEngine.GUIStyle)
extern "C" int32_t GUI_CalcTotalHorizSpacing_m6_1061 (Object_t * __this /* static, unused */, int32_t ___xCount, GUIStyle_t6_166 * ___style, GUIStyle_t6_166 * ___firstStyle, GUIStyle_t6_166 * ___midStyle, GUIStyle_t6_166 * ___lastStyle, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.GUI::DoButtonGrid(UnityEngine.Rect,System.Int32,UnityEngine.GUIContent[],System.Int32,UnityEngine.GUIStyle,UnityEngine.GUIStyle,UnityEngine.GUIStyle,UnityEngine.GUIStyle)
extern "C" int32_t GUI_DoButtonGrid_m6_1062 (Object_t * __this /* static, unused */, Rect_t6_52  ___position, int32_t ___selected, GUIContentU5BU5D_t6_264* ___contents, int32_t ___xCount, GUIStyle_t6_166 * ___style, GUIStyle_t6_166 * ___firstStyle, GUIStyle_t6_166 * ___midStyle, GUIStyle_t6_166 * ___lastStyle, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Rect[] UnityEngine.GUI::CalcMouseRects(UnityEngine.Rect,System.Int32,System.Int32,System.Single,System.Single,UnityEngine.GUIStyle,UnityEngine.GUIStyle,UnityEngine.GUIStyle,UnityEngine.GUIStyle,System.Boolean)
extern "C" RectU5BU5D_t6_265* GUI_CalcMouseRects_m6_1063 (Object_t * __this /* static, unused */, Rect_t6_52  ___position, int32_t ___count, int32_t ___xCount, float ___elemWidth, float ___elemHeight, GUIStyle_t6_166 * ___style, GUIStyle_t6_166 * ___firstStyle, GUIStyle_t6_166 * ___midStyle, GUIStyle_t6_166 * ___lastStyle, bool ___addBorders, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.GUI::GetButtonGridMouseSelection(UnityEngine.Rect[],UnityEngine.Vector2,System.Boolean)
extern "C" int32_t GUI_GetButtonGridMouseSelection_m6_1064 (Object_t * __this /* static, unused */, RectU5BU5D_t6_265* ___buttonRects, Vector2_t6_48  ___mousePos, bool ___findNearest, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.GUI::HorizontalSlider(UnityEngine.Rect,System.Single,System.Single,System.Single,UnityEngine.GUIStyle,UnityEngine.GUIStyle)
extern "C" float GUI_HorizontalSlider_m6_1065 (Object_t * __this /* static, unused */, Rect_t6_52  ___position, float ___value, float ___leftValue, float ___rightValue, GUIStyle_t6_166 * ___slider, GUIStyle_t6_166 * ___thumb, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.GUI::Slider(UnityEngine.Rect,System.Single,System.Single,System.Single,System.Single,UnityEngine.GUIStyle,UnityEngine.GUIStyle,System.Boolean,System.Int32)
extern "C" float GUI_Slider_m6_1066 (Object_t * __this /* static, unused */, Rect_t6_52  ___position, float ___value, float ___size, float ___start, float ___end, GUIStyle_t6_166 * ___slider, GUIStyle_t6_166 * ___thumb, bool ___horiz, int32_t ___id, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.GUI::HorizontalScrollbar(UnityEngine.Rect,System.Single,System.Single,System.Single,System.Single,UnityEngine.GUIStyle)
extern "C" float GUI_HorizontalScrollbar_m6_1067 (Object_t * __this /* static, unused */, Rect_t6_52  ___position, float ___value, float ___size, float ___leftValue, float ___rightValue, GUIStyle_t6_166 * ___style, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.GUI::ScrollerRepeatButton(System.Int32,UnityEngine.Rect,UnityEngine.GUIStyle)
extern "C" bool GUI_ScrollerRepeatButton_m6_1068 (Object_t * __this /* static, unused */, int32_t ___scrollerID, Rect_t6_52  ___rect, GUIStyle_t6_166 * ___style, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.GUI::VerticalScrollbar(UnityEngine.Rect,System.Single,System.Single,System.Single,System.Single,UnityEngine.GUIStyle)
extern "C" float GUI_VerticalScrollbar_m6_1069 (Object_t * __this /* static, unused */, Rect_t6_52  ___position, float ___value, float ___size, float ___topValue, float ___bottomValue, GUIStyle_t6_166 * ___style, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.GUI::Scroller(UnityEngine.Rect,System.Single,System.Single,System.Single,System.Single,UnityEngine.GUIStyle,UnityEngine.GUIStyle,UnityEngine.GUIStyle,UnityEngine.GUIStyle,System.Boolean)
extern "C" float GUI_Scroller_m6_1070 (Object_t * __this /* static, unused */, Rect_t6_52  ___position, float ___value, float ___size, float ___leftValue, float ___rightValue, GUIStyle_t6_166 * ___slider, GUIStyle_t6_166 * ___thumb, GUIStyle_t6_166 * ___leftButton, GUIStyle_t6_166 * ___rightButton, bool ___horiz, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.GUI::BeginGroup(UnityEngine.Rect,UnityEngine.GUIContent,UnityEngine.GUIStyle)
extern "C" void GUI_BeginGroup_m6_1071 (Object_t * __this /* static, unused */, Rect_t6_52  ___position, GUIContent_t6_163 * ___content, GUIStyle_t6_166 * ___style, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.GUI::EndGroup()
extern "C" void GUI_EndGroup_m6_1072 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2 UnityEngine.GUI::BeginScrollView(UnityEngine.Rect,UnityEngine.Vector2,UnityEngine.Rect,System.Boolean,System.Boolean,UnityEngine.GUIStyle,UnityEngine.GUIStyle,UnityEngine.GUIStyle)
extern "C" Vector2_t6_48  GUI_BeginScrollView_m6_1073 (Object_t * __this /* static, unused */, Rect_t6_52  ___position, Vector2_t6_48  ___scrollPosition, Rect_t6_52  ___viewRect, bool ___alwaysShowHorizontal, bool ___alwaysShowVertical, GUIStyle_t6_166 * ___horizontalScrollbar, GUIStyle_t6_166 * ___verticalScrollbar, GUIStyle_t6_166 * ___background, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.GUI::EndScrollView(System.Boolean)
extern "C" void GUI_EndScrollView_m6_1074 (Object_t * __this /* static, unused */, bool ___handleScrollWheel, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Rect UnityEngine.GUI::Window(System.Int32,UnityEngine.Rect,UnityEngine.GUI/WindowFunction,UnityEngine.GUIContent,UnityEngine.GUIStyle)
extern "C" Rect_t6_52  GUI_Window_m6_1075 (Object_t * __this /* static, unused */, int32_t ___id, Rect_t6_52  ___clientRect, WindowFunction_t6_159 * ___func, GUIContent_t6_163 * ___title, GUIStyle_t6_166 * ___style, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.GUI::CallWindowDelegate(UnityEngine.GUI/WindowFunction,System.Int32,UnityEngine.GUISkin,System.Int32,System.Single,System.Single,UnityEngine.GUIStyle)
extern "C" void GUI_CallWindowDelegate_m6_1076 (Object_t * __this /* static, unused */, WindowFunction_t6_159 * ___func, int32_t ___id, GUISkin_t6_161 * ____skin, int32_t ___forceRect, float ___width, float ___height, GUIStyle_t6_166 * ___style, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.GUI::DragWindow()
extern "C" void GUI_DragWindow_m6_1077 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.GUI::set_color(UnityEngine.Color)
extern "C" void GUI_set_color_m6_1078 (Object_t * __this /* static, unused */, Color_t6_40  ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.GUI::INTERNAL_set_color(UnityEngine.Color&)
extern "C" void GUI_INTERNAL_set_color_m6_1079 (Object_t * __this /* static, unused */, Color_t6_40 * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.GUI::get_changed()
extern "C" bool GUI_get_changed_m6_1080 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.GUI::set_changed(System.Boolean)
extern "C" void GUI_set_changed_m6_1081 (Object_t * __this /* static, unused */, bool ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.GUI::get_enabled()
extern "C" bool GUI_get_enabled_m6_1082 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.GUI::Internal_SetTooltip(System.String)
extern "C" void GUI_Internal_SetTooltip_m6_1083 (Object_t * __this /* static, unused */, String_t* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.GUI::DoLabel(UnityEngine.Rect,UnityEngine.GUIContent,System.IntPtr)
extern "C" void GUI_DoLabel_m6_1084 (Object_t * __this /* static, unused */, Rect_t6_52  ___position, GUIContent_t6_163 * ___content, IntPtr_t ___style, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.GUI::INTERNAL_CALL_DoLabel(UnityEngine.Rect&,UnityEngine.GUIContent,System.IntPtr)
extern "C" void GUI_INTERNAL_CALL_DoLabel_m6_1085 (Object_t * __this /* static, unused */, Rect_t6_52 * ___position, GUIContent_t6_163 * ___content, IntPtr_t ___style, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.GUI::DoButton(UnityEngine.Rect,UnityEngine.GUIContent,System.IntPtr)
extern "C" bool GUI_DoButton_m6_1086 (Object_t * __this /* static, unused */, Rect_t6_52  ___position, GUIContent_t6_163 * ___content, IntPtr_t ___style, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.GUI::INTERNAL_CALL_DoButton(UnityEngine.Rect&,UnityEngine.GUIContent,System.IntPtr)
extern "C" bool GUI_INTERNAL_CALL_DoButton_m6_1087 (Object_t * __this /* static, unused */, Rect_t6_52 * ___position, GUIContent_t6_163 * ___content, IntPtr_t ___style, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.GUI::SetNextControlName(System.String)
extern "C" void GUI_SetNextControlName_m6_1088 (Object_t * __this /* static, unused */, String_t* ___name, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String UnityEngine.GUI::GetNameOfFocusedControl()
extern "C" String_t* GUI_GetNameOfFocusedControl_m6_1089 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.GUI::FocusControl(System.String)
extern "C" void GUI_FocusControl_m6_1090 (Object_t * __this /* static, unused */, String_t* ___name, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.GUI::DoToggle(UnityEngine.Rect,System.Int32,System.Boolean,UnityEngine.GUIContent,System.IntPtr)
extern "C" bool GUI_DoToggle_m6_1091 (Object_t * __this /* static, unused */, Rect_t6_52  ___position, int32_t ___id, bool ___value, GUIContent_t6_163 * ___content, IntPtr_t ___style, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.GUI::INTERNAL_CALL_DoToggle(UnityEngine.Rect&,System.Int32,System.Boolean,UnityEngine.GUIContent,System.IntPtr)
extern "C" bool GUI_INTERNAL_CALL_DoToggle_m6_1092 (Object_t * __this /* static, unused */, Rect_t6_52 * ___position, int32_t ___id, bool ___value, GUIContent_t6_163 * ___content, IntPtr_t ___style, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.GUI::get_usePageScrollbars()
extern "C" bool GUI_get_usePageScrollbars_m6_1093 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.GUI::InternalRepaintEditorWindow()
extern "C" void GUI_InternalRepaintEditorWindow_m6_1094 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Rect UnityEngine.GUI::DoWindow(System.Int32,UnityEngine.Rect,UnityEngine.GUI/WindowFunction,UnityEngine.GUIContent,UnityEngine.GUIStyle,UnityEngine.GUISkin,System.Boolean)
extern "C" Rect_t6_52  GUI_DoWindow_m6_1095 (Object_t * __this /* static, unused */, int32_t ___id, Rect_t6_52  ___clientRect, WindowFunction_t6_159 * ___func, GUIContent_t6_163 * ___title, GUIStyle_t6_166 * ___style, GUISkin_t6_161 * ___skin, bool ___forceRectOnLayout, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Rect UnityEngine.GUI::INTERNAL_CALL_DoWindow(System.Int32,UnityEngine.Rect&,UnityEngine.GUI/WindowFunction,UnityEngine.GUIContent,UnityEngine.GUIStyle,UnityEngine.GUISkin,System.Boolean)
extern "C" Rect_t6_52  GUI_INTERNAL_CALL_DoWindow_m6_1096 (Object_t * __this /* static, unused */, int32_t ___id, Rect_t6_52 * ___clientRect, WindowFunction_t6_159 * ___func, GUIContent_t6_163 * ___title, GUIStyle_t6_166 * ___style, GUISkin_t6_161 * ___skin, bool ___forceRectOnLayout, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.GUI::DragWindow(UnityEngine.Rect)
extern "C" void GUI_DragWindow_m6_1097 (Object_t * __this /* static, unused */, Rect_t6_52  ___position, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.GUI::INTERNAL_CALL_DragWindow(UnityEngine.Rect&)
extern "C" void GUI_INTERNAL_CALL_DragWindow_m6_1098 (Object_t * __this /* static, unused */, Rect_t6_52 * ___position, const MethodInfo* method) IL2CPP_METHOD_ATTR;
