﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.List`1<System.Reflection.CustomAttributeNamedArgument>
struct List_1_t1_1081;
// System.Collections.Generic.IEnumerable`1<System.Reflection.CustomAttributeNamedArgument>
struct IEnumerable_1_t1_1415;
// System.Collections.Generic.IEnumerator`1<System.Reflection.CustomAttributeNamedArgument>
struct IEnumerator_1_t1_1414;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t1_130;
// System.Object
struct Object_t;
// System.Collections.Generic.ICollection`1<System.Reflection.CustomAttributeNamedArgument>
struct ICollection_1_t1_843;
// System.Reflection.CustomAttributeNamedArgument[]
struct CustomAttributeNamedArgumentU5BU5D_t1_839;
// System.Predicate`1<System.Reflection.CustomAttributeNamedArgument>
struct Predicate_1_t1_1085;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Reflection_CustomAttributeNamedArgument.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_9.h"

// System.Void System.Collections.Generic.List`1<System.Reflection.CustomAttributeNamedArgument>::.ctor()
extern "C" void List_1__ctor_m1_6739_gshared (List_1_t1_1081 * __this, const MethodInfo* method);
#define List_1__ctor_m1_6739(__this, method) (( void (*) (List_1_t1_1081 *, const MethodInfo*))List_1__ctor_m1_6739_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<System.Reflection.CustomAttributeNamedArgument>::.ctor(System.Collections.Generic.IEnumerable`1<T>)
extern "C" void List_1__ctor_m1_6740_gshared (List_1_t1_1081 * __this, Object_t* ___collection, const MethodInfo* method);
#define List_1__ctor_m1_6740(__this, ___collection, method) (( void (*) (List_1_t1_1081 *, Object_t*, const MethodInfo*))List_1__ctor_m1_6740_gshared)(__this, ___collection, method)
// System.Void System.Collections.Generic.List`1<System.Reflection.CustomAttributeNamedArgument>::.ctor(System.Int32)
extern "C" void List_1__ctor_m1_6741_gshared (List_1_t1_1081 * __this, int32_t ___capacity, const MethodInfo* method);
#define List_1__ctor_m1_6741(__this, ___capacity, method) (( void (*) (List_1_t1_1081 *, int32_t, const MethodInfo*))List_1__ctor_m1_6741_gshared)(__this, ___capacity, method)
// System.Void System.Collections.Generic.List`1<System.Reflection.CustomAttributeNamedArgument>::.cctor()
extern "C" void List_1__cctor_m1_6742_gshared (Object_t * __this /* static, unused */, const MethodInfo* method);
#define List_1__cctor_m1_6742(__this /* static, unused */, method) (( void (*) (Object_t * /* static, unused */, const MethodInfo*))List_1__cctor_m1_6742_gshared)(__this /* static, unused */, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.List`1<System.Reflection.CustomAttributeNamedArgument>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
extern "C" Object_t* List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m1_6743_gshared (List_1_t1_1081 * __this, const MethodInfo* method);
#define List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m1_6743(__this, method) (( Object_t* (*) (List_1_t1_1081 *, const MethodInfo*))List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m1_6743_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<System.Reflection.CustomAttributeNamedArgument>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C" void List_1_System_Collections_ICollection_CopyTo_m1_6744_gshared (List_1_t1_1081 * __this, Array_t * ___array, int32_t ___arrayIndex, const MethodInfo* method);
#define List_1_System_Collections_ICollection_CopyTo_m1_6744(__this, ___array, ___arrayIndex, method) (( void (*) (List_1_t1_1081 *, Array_t *, int32_t, const MethodInfo*))List_1_System_Collections_ICollection_CopyTo_m1_6744_gshared)(__this, ___array, ___arrayIndex, method)
// System.Collections.IEnumerator System.Collections.Generic.List`1<System.Reflection.CustomAttributeNamedArgument>::System.Collections.IEnumerable.GetEnumerator()
extern "C" Object_t * List_1_System_Collections_IEnumerable_GetEnumerator_m1_6745_gshared (List_1_t1_1081 * __this, const MethodInfo* method);
#define List_1_System_Collections_IEnumerable_GetEnumerator_m1_6745(__this, method) (( Object_t * (*) (List_1_t1_1081 *, const MethodInfo*))List_1_System_Collections_IEnumerable_GetEnumerator_m1_6745_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<System.Reflection.CustomAttributeNamedArgument>::System.Collections.IList.Add(System.Object)
extern "C" int32_t List_1_System_Collections_IList_Add_m1_6746_gshared (List_1_t1_1081 * __this, Object_t * ___item, const MethodInfo* method);
#define List_1_System_Collections_IList_Add_m1_6746(__this, ___item, method) (( int32_t (*) (List_1_t1_1081 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Add_m1_6746_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.List`1<System.Reflection.CustomAttributeNamedArgument>::System.Collections.IList.Contains(System.Object)
extern "C" bool List_1_System_Collections_IList_Contains_m1_6747_gshared (List_1_t1_1081 * __this, Object_t * ___item, const MethodInfo* method);
#define List_1_System_Collections_IList_Contains_m1_6747(__this, ___item, method) (( bool (*) (List_1_t1_1081 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Contains_m1_6747_gshared)(__this, ___item, method)
// System.Int32 System.Collections.Generic.List`1<System.Reflection.CustomAttributeNamedArgument>::System.Collections.IList.IndexOf(System.Object)
extern "C" int32_t List_1_System_Collections_IList_IndexOf_m1_6748_gshared (List_1_t1_1081 * __this, Object_t * ___item, const MethodInfo* method);
#define List_1_System_Collections_IList_IndexOf_m1_6748(__this, ___item, method) (( int32_t (*) (List_1_t1_1081 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_IndexOf_m1_6748_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<System.Reflection.CustomAttributeNamedArgument>::System.Collections.IList.Insert(System.Int32,System.Object)
extern "C" void List_1_System_Collections_IList_Insert_m1_6749_gshared (List_1_t1_1081 * __this, int32_t ___index, Object_t * ___item, const MethodInfo* method);
#define List_1_System_Collections_IList_Insert_m1_6749(__this, ___index, ___item, method) (( void (*) (List_1_t1_1081 *, int32_t, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Insert_m1_6749_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.Generic.List`1<System.Reflection.CustomAttributeNamedArgument>::System.Collections.IList.Remove(System.Object)
extern "C" void List_1_System_Collections_IList_Remove_m1_6750_gshared (List_1_t1_1081 * __this, Object_t * ___item, const MethodInfo* method);
#define List_1_System_Collections_IList_Remove_m1_6750(__this, ___item, method) (( void (*) (List_1_t1_1081 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Remove_m1_6750_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.List`1<System.Reflection.CustomAttributeNamedArgument>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C" bool List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m1_6751_gshared (List_1_t1_1081 * __this, const MethodInfo* method);
#define List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m1_6751(__this, method) (( bool (*) (List_1_t1_1081 *, const MethodInfo*))List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m1_6751_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<System.Reflection.CustomAttributeNamedArgument>::System.Collections.ICollection.get_SyncRoot()
extern "C" Object_t * List_1_System_Collections_ICollection_get_SyncRoot_m1_6752_gshared (List_1_t1_1081 * __this, const MethodInfo* method);
#define List_1_System_Collections_ICollection_get_SyncRoot_m1_6752(__this, method) (( Object_t * (*) (List_1_t1_1081 *, const MethodInfo*))List_1_System_Collections_ICollection_get_SyncRoot_m1_6752_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<System.Reflection.CustomAttributeNamedArgument>::System.Collections.IList.get_Item(System.Int32)
extern "C" Object_t * List_1_System_Collections_IList_get_Item_m1_6753_gshared (List_1_t1_1081 * __this, int32_t ___index, const MethodInfo* method);
#define List_1_System_Collections_IList_get_Item_m1_6753(__this, ___index, method) (( Object_t * (*) (List_1_t1_1081 *, int32_t, const MethodInfo*))List_1_System_Collections_IList_get_Item_m1_6753_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<System.Reflection.CustomAttributeNamedArgument>::System.Collections.IList.set_Item(System.Int32,System.Object)
extern "C" void List_1_System_Collections_IList_set_Item_m1_6754_gshared (List_1_t1_1081 * __this, int32_t ___index, Object_t * ___value, const MethodInfo* method);
#define List_1_System_Collections_IList_set_Item_m1_6754(__this, ___index, ___value, method) (( void (*) (List_1_t1_1081 *, int32_t, Object_t *, const MethodInfo*))List_1_System_Collections_IList_set_Item_m1_6754_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.Generic.List`1<System.Reflection.CustomAttributeNamedArgument>::Add(T)
extern "C" void List_1_Add_m1_6755_gshared (List_1_t1_1081 * __this, CustomAttributeNamedArgument_t1_331  ___item, const MethodInfo* method);
#define List_1_Add_m1_6755(__this, ___item, method) (( void (*) (List_1_t1_1081 *, CustomAttributeNamedArgument_t1_331 , const MethodInfo*))List_1_Add_m1_6755_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<System.Reflection.CustomAttributeNamedArgument>::GrowIfNeeded(System.Int32)
extern "C" void List_1_GrowIfNeeded_m1_6756_gshared (List_1_t1_1081 * __this, int32_t ___newCount, const MethodInfo* method);
#define List_1_GrowIfNeeded_m1_6756(__this, ___newCount, method) (( void (*) (List_1_t1_1081 *, int32_t, const MethodInfo*))List_1_GrowIfNeeded_m1_6756_gshared)(__this, ___newCount, method)
// System.Void System.Collections.Generic.List`1<System.Reflection.CustomAttributeNamedArgument>::AddCollection(System.Collections.Generic.ICollection`1<T>)
extern "C" void List_1_AddCollection_m1_6757_gshared (List_1_t1_1081 * __this, Object_t* ___collection, const MethodInfo* method);
#define List_1_AddCollection_m1_6757(__this, ___collection, method) (( void (*) (List_1_t1_1081 *, Object_t*, const MethodInfo*))List_1_AddCollection_m1_6757_gshared)(__this, ___collection, method)
// System.Void System.Collections.Generic.List`1<System.Reflection.CustomAttributeNamedArgument>::AddEnumerable(System.Collections.Generic.IEnumerable`1<T>)
extern "C" void List_1_AddEnumerable_m1_6758_gshared (List_1_t1_1081 * __this, Object_t* ___enumerable, const MethodInfo* method);
#define List_1_AddEnumerable_m1_6758(__this, ___enumerable, method) (( void (*) (List_1_t1_1081 *, Object_t*, const MethodInfo*))List_1_AddEnumerable_m1_6758_gshared)(__this, ___enumerable, method)
// System.Void System.Collections.Generic.List`1<System.Reflection.CustomAttributeNamedArgument>::AddRange(System.Collections.Generic.IEnumerable`1<T>)
extern "C" void List_1_AddRange_m1_6759_gshared (List_1_t1_1081 * __this, Object_t* ___collection, const MethodInfo* method);
#define List_1_AddRange_m1_6759(__this, ___collection, method) (( void (*) (List_1_t1_1081 *, Object_t*, const MethodInfo*))List_1_AddRange_m1_6759_gshared)(__this, ___collection, method)
// System.Void System.Collections.Generic.List`1<System.Reflection.CustomAttributeNamedArgument>::Clear()
extern "C" void List_1_Clear_m1_6760_gshared (List_1_t1_1081 * __this, const MethodInfo* method);
#define List_1_Clear_m1_6760(__this, method) (( void (*) (List_1_t1_1081 *, const MethodInfo*))List_1_Clear_m1_6760_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<System.Reflection.CustomAttributeNamedArgument>::Contains(T)
extern "C" bool List_1_Contains_m1_6761_gshared (List_1_t1_1081 * __this, CustomAttributeNamedArgument_t1_331  ___item, const MethodInfo* method);
#define List_1_Contains_m1_6761(__this, ___item, method) (( bool (*) (List_1_t1_1081 *, CustomAttributeNamedArgument_t1_331 , const MethodInfo*))List_1_Contains_m1_6761_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<System.Reflection.CustomAttributeNamedArgument>::CopyTo(T[],System.Int32)
extern "C" void List_1_CopyTo_m1_6762_gshared (List_1_t1_1081 * __this, CustomAttributeNamedArgumentU5BU5D_t1_839* ___array, int32_t ___arrayIndex, const MethodInfo* method);
#define List_1_CopyTo_m1_6762(__this, ___array, ___arrayIndex, method) (( void (*) (List_1_t1_1081 *, CustomAttributeNamedArgumentU5BU5D_t1_839*, int32_t, const MethodInfo*))List_1_CopyTo_m1_6762_gshared)(__this, ___array, ___arrayIndex, method)
// System.Void System.Collections.Generic.List`1<System.Reflection.CustomAttributeNamedArgument>::CheckMatch(System.Predicate`1<T>)
extern "C" void List_1_CheckMatch_m1_6763_gshared (Object_t * __this /* static, unused */, Predicate_1_t1_1085 * ___match, const MethodInfo* method);
#define List_1_CheckMatch_m1_6763(__this /* static, unused */, ___match, method) (( void (*) (Object_t * /* static, unused */, Predicate_1_t1_1085 *, const MethodInfo*))List_1_CheckMatch_m1_6763_gshared)(__this /* static, unused */, ___match, method)
// System.Int32 System.Collections.Generic.List`1<System.Reflection.CustomAttributeNamedArgument>::FindIndex(System.Predicate`1<T>)
extern "C" int32_t List_1_FindIndex_m1_6764_gshared (List_1_t1_1081 * __this, Predicate_1_t1_1085 * ___match, const MethodInfo* method);
#define List_1_FindIndex_m1_6764(__this, ___match, method) (( int32_t (*) (List_1_t1_1081 *, Predicate_1_t1_1085 *, const MethodInfo*))List_1_FindIndex_m1_6764_gshared)(__this, ___match, method)
// System.Int32 System.Collections.Generic.List`1<System.Reflection.CustomAttributeNamedArgument>::GetIndex(System.Int32,System.Int32,System.Predicate`1<T>)
extern "C" int32_t List_1_GetIndex_m1_6765_gshared (List_1_t1_1081 * __this, int32_t ___startIndex, int32_t ___count, Predicate_1_t1_1085 * ___match, const MethodInfo* method);
#define List_1_GetIndex_m1_6765(__this, ___startIndex, ___count, ___match, method) (( int32_t (*) (List_1_t1_1081 *, int32_t, int32_t, Predicate_1_t1_1085 *, const MethodInfo*))List_1_GetIndex_m1_6765_gshared)(__this, ___startIndex, ___count, ___match, method)
// System.Collections.Generic.List`1/Enumerator<T> System.Collections.Generic.List`1<System.Reflection.CustomAttributeNamedArgument>::GetEnumerator()
extern "C" Enumerator_t1_1082  List_1_GetEnumerator_m1_6766_gshared (List_1_t1_1081 * __this, const MethodInfo* method);
#define List_1_GetEnumerator_m1_6766(__this, method) (( Enumerator_t1_1082  (*) (List_1_t1_1081 *, const MethodInfo*))List_1_GetEnumerator_m1_6766_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<System.Reflection.CustomAttributeNamedArgument>::IndexOf(T)
extern "C" int32_t List_1_IndexOf_m1_6767_gshared (List_1_t1_1081 * __this, CustomAttributeNamedArgument_t1_331  ___item, const MethodInfo* method);
#define List_1_IndexOf_m1_6767(__this, ___item, method) (( int32_t (*) (List_1_t1_1081 *, CustomAttributeNamedArgument_t1_331 , const MethodInfo*))List_1_IndexOf_m1_6767_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<System.Reflection.CustomAttributeNamedArgument>::Shift(System.Int32,System.Int32)
extern "C" void List_1_Shift_m1_6768_gshared (List_1_t1_1081 * __this, int32_t ___start, int32_t ___delta, const MethodInfo* method);
#define List_1_Shift_m1_6768(__this, ___start, ___delta, method) (( void (*) (List_1_t1_1081 *, int32_t, int32_t, const MethodInfo*))List_1_Shift_m1_6768_gshared)(__this, ___start, ___delta, method)
// System.Void System.Collections.Generic.List`1<System.Reflection.CustomAttributeNamedArgument>::CheckIndex(System.Int32)
extern "C" void List_1_CheckIndex_m1_6769_gshared (List_1_t1_1081 * __this, int32_t ___index, const MethodInfo* method);
#define List_1_CheckIndex_m1_6769(__this, ___index, method) (( void (*) (List_1_t1_1081 *, int32_t, const MethodInfo*))List_1_CheckIndex_m1_6769_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<System.Reflection.CustomAttributeNamedArgument>::Insert(System.Int32,T)
extern "C" void List_1_Insert_m1_6770_gshared (List_1_t1_1081 * __this, int32_t ___index, CustomAttributeNamedArgument_t1_331  ___item, const MethodInfo* method);
#define List_1_Insert_m1_6770(__this, ___index, ___item, method) (( void (*) (List_1_t1_1081 *, int32_t, CustomAttributeNamedArgument_t1_331 , const MethodInfo*))List_1_Insert_m1_6770_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.Generic.List`1<System.Reflection.CustomAttributeNamedArgument>::CheckCollection(System.Collections.Generic.IEnumerable`1<T>)
extern "C" void List_1_CheckCollection_m1_6771_gshared (List_1_t1_1081 * __this, Object_t* ___collection, const MethodInfo* method);
#define List_1_CheckCollection_m1_6771(__this, ___collection, method) (( void (*) (List_1_t1_1081 *, Object_t*, const MethodInfo*))List_1_CheckCollection_m1_6771_gshared)(__this, ___collection, method)
// System.Boolean System.Collections.Generic.List`1<System.Reflection.CustomAttributeNamedArgument>::Remove(T)
extern "C" bool List_1_Remove_m1_6772_gshared (List_1_t1_1081 * __this, CustomAttributeNamedArgument_t1_331  ___item, const MethodInfo* method);
#define List_1_Remove_m1_6772(__this, ___item, method) (( bool (*) (List_1_t1_1081 *, CustomAttributeNamedArgument_t1_331 , const MethodInfo*))List_1_Remove_m1_6772_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<System.Reflection.CustomAttributeNamedArgument>::RemoveAt(System.Int32)
extern "C" void List_1_RemoveAt_m1_6773_gshared (List_1_t1_1081 * __this, int32_t ___index, const MethodInfo* method);
#define List_1_RemoveAt_m1_6773(__this, ___index, method) (( void (*) (List_1_t1_1081 *, int32_t, const MethodInfo*))List_1_RemoveAt_m1_6773_gshared)(__this, ___index, method)
// T[] System.Collections.Generic.List`1<System.Reflection.CustomAttributeNamedArgument>::ToArray()
extern "C" CustomAttributeNamedArgumentU5BU5D_t1_839* List_1_ToArray_m1_6774_gshared (List_1_t1_1081 * __this, const MethodInfo* method);
#define List_1_ToArray_m1_6774(__this, method) (( CustomAttributeNamedArgumentU5BU5D_t1_839* (*) (List_1_t1_1081 *, const MethodInfo*))List_1_ToArray_m1_6774_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<System.Reflection.CustomAttributeNamedArgument>::get_Capacity()
extern "C" int32_t List_1_get_Capacity_m1_6775_gshared (List_1_t1_1081 * __this, const MethodInfo* method);
#define List_1_get_Capacity_m1_6775(__this, method) (( int32_t (*) (List_1_t1_1081 *, const MethodInfo*))List_1_get_Capacity_m1_6775_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<System.Reflection.CustomAttributeNamedArgument>::set_Capacity(System.Int32)
extern "C" void List_1_set_Capacity_m1_6776_gshared (List_1_t1_1081 * __this, int32_t ___value, const MethodInfo* method);
#define List_1_set_Capacity_m1_6776(__this, ___value, method) (( void (*) (List_1_t1_1081 *, int32_t, const MethodInfo*))List_1_set_Capacity_m1_6776_gshared)(__this, ___value, method)
// System.Int32 System.Collections.Generic.List`1<System.Reflection.CustomAttributeNamedArgument>::get_Count()
extern "C" int32_t List_1_get_Count_m1_6777_gshared (List_1_t1_1081 * __this, const MethodInfo* method);
#define List_1_get_Count_m1_6777(__this, method) (( int32_t (*) (List_1_t1_1081 *, const MethodInfo*))List_1_get_Count_m1_6777_gshared)(__this, method)
// T System.Collections.Generic.List`1<System.Reflection.CustomAttributeNamedArgument>::get_Item(System.Int32)
extern "C" CustomAttributeNamedArgument_t1_331  List_1_get_Item_m1_6778_gshared (List_1_t1_1081 * __this, int32_t ___index, const MethodInfo* method);
#define List_1_get_Item_m1_6778(__this, ___index, method) (( CustomAttributeNamedArgument_t1_331  (*) (List_1_t1_1081 *, int32_t, const MethodInfo*))List_1_get_Item_m1_6778_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<System.Reflection.CustomAttributeNamedArgument>::set_Item(System.Int32,T)
extern "C" void List_1_set_Item_m1_6779_gshared (List_1_t1_1081 * __this, int32_t ___index, CustomAttributeNamedArgument_t1_331  ___value, const MethodInfo* method);
#define List_1_set_Item_m1_6779(__this, ___index, ___value, method) (( void (*) (List_1_t1_1081 *, int32_t, CustomAttributeNamedArgument_t1_331 , const MethodInfo*))List_1_set_Item_m1_6779_gshared)(__this, ___index, ___value, method)
