﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Enumerator__2MethodDeclarations.h"

// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Int32>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
#define Enumerator__ctor_m1_6264(__this, ___dictionary, method) (( void (*) (Enumerator_t1_1035 *, Dictionary_2_t1_77 *, const MethodInfo*))Enumerator__ctor_m1_6155_gshared)(__this, ___dictionary, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Int32>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m1_6265(__this, method) (( Object_t * (*) (Enumerator_t1_1035 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m1_6156_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Int32>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m1_6266(__this, method) (( void (*) (Enumerator_t1_1035 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m1_6157_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Int32>::System.Collections.IDictionaryEnumerator.get_Entry()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m1_6267(__this, method) (( DictionaryEntry_t1_167  (*) (Enumerator_t1_1035 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m1_6158_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Int32>::System.Collections.IDictionaryEnumerator.get_Key()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m1_6268(__this, method) (( Object_t * (*) (Enumerator_t1_1035 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m1_6159_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Int32>::System.Collections.IDictionaryEnumerator.get_Value()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m1_6269(__this, method) (( Object_t * (*) (Enumerator_t1_1035 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m1_6160_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Int32>::MoveNext()
#define Enumerator_MoveNext_m1_6270(__this, method) (( bool (*) (Enumerator_t1_1035 *, const MethodInfo*))Enumerator_MoveNext_m1_6161_gshared)(__this, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Int32>::get_Current()
#define Enumerator_get_Current_m1_6271(__this, method) (( KeyValuePair_2_t1_1032  (*) (Enumerator_t1_1035 *, const MethodInfo*))Enumerator_get_Current_m1_6162_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Int32>::get_CurrentKey()
#define Enumerator_get_CurrentKey_m1_6272(__this, method) (( String_t* (*) (Enumerator_t1_1035 *, const MethodInfo*))Enumerator_get_CurrentKey_m1_6163_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Int32>::get_CurrentValue()
#define Enumerator_get_CurrentValue_m1_6273(__this, method) (( int32_t (*) (Enumerator_t1_1035 *, const MethodInfo*))Enumerator_get_CurrentValue_m1_6164_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Int32>::Reset()
#define Enumerator_Reset_m1_6274(__this, method) (( void (*) (Enumerator_t1_1035 *, const MethodInfo*))Enumerator_Reset_m1_6165_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Int32>::VerifyState()
#define Enumerator_VerifyState_m1_6275(__this, method) (( void (*) (Enumerator_t1_1035 *, const MethodInfo*))Enumerator_VerifyState_m1_6166_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Int32>::VerifyCurrent()
#define Enumerator_VerifyCurrent_m1_6276(__this, method) (( void (*) (Enumerator_t1_1035 *, const MethodInfo*))Enumerator_VerifyCurrent_m1_6167_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Int32>::Dispose()
#define Enumerator_Dispose_m1_6277(__this, method) (( void (*) (Enumerator_t1_1035 *, const MethodInfo*))Enumerator_Dispose_m1_6168_gshared)(__this, method)
