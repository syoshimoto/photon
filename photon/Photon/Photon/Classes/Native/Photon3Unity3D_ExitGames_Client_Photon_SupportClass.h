﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// ExitGames.Client.Photon.SupportClass/IntegerMillisecondsDelegate
struct IntegerMillisecondsDelegate_t5_54;

#include "mscorlib_System_Object.h"

// ExitGames.Client.Photon.SupportClass
struct  SupportClass_t5_57  : public Object_t
{
};
struct SupportClass_t5_57_StaticFields{
	// ExitGames.Client.Photon.SupportClass/IntegerMillisecondsDelegate ExitGames.Client.Photon.SupportClass::IntegerMilliseconds
	IntegerMillisecondsDelegate_t5_54 * ___IntegerMilliseconds_0;
	// ExitGames.Client.Photon.SupportClass/IntegerMillisecondsDelegate ExitGames.Client.Photon.SupportClass::CS$<>9__CachedAnonymousMethodDelegate4
	IntegerMillisecondsDelegate_t5_54 * ___CSU24U3CU3E9__CachedAnonymousMethodDelegate4_1;
};
