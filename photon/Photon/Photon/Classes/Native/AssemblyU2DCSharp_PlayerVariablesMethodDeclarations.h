﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayerVariables
struct PlayerVariables_t8_60;
// System.String
struct String_t;
// UnityEngine.Material
struct Material_t6_67;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Color.h"

// System.Void PlayerVariables::.ctor()
extern "C" void PlayerVariables__ctor_m8_276 (PlayerVariables_t8_60 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayerVariables::.cctor()
extern "C" void PlayerVariables__cctor_m8_277 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Color PlayerVariables::GetColor(System.Int32)
extern "C" Color_t6_40  PlayerVariables_GetColor_m8_278 (Object_t * __this /* static, unused */, int32_t ___playerId, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PlayerVariables::GetColorName(System.Int32)
extern "C" String_t* PlayerVariables_GetColorName_m8_279 (Object_t * __this /* static, unused */, int32_t ___playerId, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Material PlayerVariables::GetMaterial(UnityEngine.Material,System.Int32)
extern "C" Material_t6_67 * PlayerVariables_GetMaterial_m8_280 (Object_t * __this /* static, unused */, Material_t6_67 * ___original, int32_t ___playerId, const MethodInfo* method) IL2CPP_METHOD_ATTR;
