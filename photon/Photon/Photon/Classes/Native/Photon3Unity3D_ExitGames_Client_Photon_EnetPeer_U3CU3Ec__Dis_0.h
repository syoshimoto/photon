﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// ExitGames.Client.Photon.EnetPeer/<>c__DisplayClassb
struct U3CU3Ec__DisplayClassb_t5_18;
// System.Byte[]
struct ByteU5BU5D_t1_71;

#include "mscorlib_System_Object.h"

// ExitGames.Client.Photon.EnetPeer/<>c__DisplayClassd
struct  U3CU3Ec__DisplayClassd_t5_20  : public Object_t
{
	// ExitGames.Client.Photon.EnetPeer/<>c__DisplayClassb ExitGames.Client.Photon.EnetPeer/<>c__DisplayClassd::CS$<>8__localsc
	U3CU3Ec__DisplayClassb_t5_18 * ___CSU24U3CU3E8__localsc_0;
	// System.Byte[] ExitGames.Client.Photon.EnetPeer/<>c__DisplayClassd::dataCopy
	ByteU5BU5D_t1_71* ___dataCopy_1;
};
