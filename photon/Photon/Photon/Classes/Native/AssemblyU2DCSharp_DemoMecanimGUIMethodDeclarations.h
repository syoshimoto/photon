﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// DemoMecanimGUI
struct DemoMecanimGUI_t8_25;

#include "codegen/il2cpp-codegen.h"

// System.Void DemoMecanimGUI::.ctor()
extern "C" void DemoMecanimGUI__ctor_m8_91 (DemoMecanimGUI_t8_25 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DemoMecanimGUI::Awake()
extern "C" void DemoMecanimGUI_Awake_m8_92 (DemoMecanimGUI_t8_25 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DemoMecanimGUI::Update()
extern "C" void DemoMecanimGUI_Update_m8_93 (DemoMecanimGUI_t8_25 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DemoMecanimGUI::FindRemoteAnimator()
extern "C" void DemoMecanimGUI_FindRemoteAnimator_m8_94 (DemoMecanimGUI_t8_25 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DemoMecanimGUI::OnGUI()
extern "C" void DemoMecanimGUI_OnGUI_m8_95 (DemoMecanimGUI_t8_25 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DemoMecanimGUI::OnJoinedRoom()
extern "C" void DemoMecanimGUI_OnJoinedRoom_m8_96 (DemoMecanimGUI_t8_25 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DemoMecanimGUI::CreatePlayerObject()
extern "C" void DemoMecanimGUI_CreatePlayerObject_m8_97 (DemoMecanimGUI_t8_25 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
