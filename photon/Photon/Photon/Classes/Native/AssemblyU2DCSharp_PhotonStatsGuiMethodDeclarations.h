﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PhotonStatsGui
struct PhotonStatsGui_t8_116;

#include "codegen/il2cpp-codegen.h"

// System.Void PhotonStatsGui::.ctor()
extern "C" void PhotonStatsGui__ctor_m8_749 (PhotonStatsGui_t8_116 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PhotonStatsGui::Start()
extern "C" void PhotonStatsGui_Start_m8_750 (PhotonStatsGui_t8_116 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PhotonStatsGui::Update()
extern "C" void PhotonStatsGui_Update_m8_751 (PhotonStatsGui_t8_116 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PhotonStatsGui::OnGUI()
extern "C" void PhotonStatsGui_OnGUI_m8_752 (PhotonStatsGui_t8_116 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PhotonStatsGui::TrafficStatsWindow(System.Int32)
extern "C" void PhotonStatsGui_TrafficStatsWindow_m8_753 (PhotonStatsGui_t8_116 * __this, int32_t ___windowID, const MethodInfo* method) IL2CPP_METHOD_ATTR;
