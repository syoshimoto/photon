﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Collections.Generic.EqualityComparer`1<PunTeams/Team>
struct EqualityComparer_1_t1_1329;

#include "mscorlib_System_Object.h"

// System.Collections.Generic.EqualityComparer`1<PunTeams/Team>
struct  EqualityComparer_1_t1_1329  : public Object_t
{
};
struct EqualityComparer_1_t1_1329_StaticFields{
	// System.Collections.Generic.EqualityComparer`1<T> System.Collections.Generic.EqualityComparer`1::_default
	EqualityComparer_1_t1_1329 * ____default_0;
};
