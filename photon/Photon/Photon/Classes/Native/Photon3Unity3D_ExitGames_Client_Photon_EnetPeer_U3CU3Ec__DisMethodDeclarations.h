﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ExitGames.Client.Photon.EnetPeer/<>c__DisplayClassb
struct U3CU3Ec__DisplayClassb_t5_18;

#include "codegen/il2cpp-codegen.h"

// System.Void ExitGames.Client.Photon.EnetPeer/<>c__DisplayClassb::.ctor()
extern "C" void U3CU3Ec__DisplayClassb__ctor_m5_103 (U3CU3Ec__DisplayClassb_t5_18 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
