﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_List_1_gen_13MethodDeclarations.h"

// System.Void System.Collections.Generic.List`1<UnityEngine.Rigidbody2D>::.ctor()
#define List_1__ctor_m1_5598(__this, method) (( void (*) (List_1_t1_911 *, const MethodInfo*))List_1__ctor_m1_5617_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Rigidbody2D>::.ctor(System.Collections.Generic.IEnumerable`1<T>)
#define List_1__ctor_m1_8311(__this, ___collection, method) (( void (*) (List_1_t1_911 *, Object_t*, const MethodInfo*))List_1__ctor_m1_5673_gshared)(__this, ___collection, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Rigidbody2D>::.ctor(System.Int32)
#define List_1__ctor_m1_8312(__this, ___capacity, method) (( void (*) (List_1_t1_911 *, int32_t, const MethodInfo*))List_1__ctor_m1_5706_gshared)(__this, ___capacity, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Rigidbody2D>::.cctor()
#define List_1__cctor_m1_8313(__this /* static, unused */, method) (( void (*) (Object_t * /* static, unused */, const MethodInfo*))List_1__cctor_m1_5708_gshared)(__this /* static, unused */, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.List`1<UnityEngine.Rigidbody2D>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
#define List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m1_8314(__this, method) (( Object_t* (*) (List_1_t1_911 *, const MethodInfo*))List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m1_5710_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Rigidbody2D>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define List_1_System_Collections_ICollection_CopyTo_m1_8315(__this, ___array, ___arrayIndex, method) (( void (*) (List_1_t1_911 *, Array_t *, int32_t, const MethodInfo*))List_1_System_Collections_ICollection_CopyTo_m1_5712_gshared)(__this, ___array, ___arrayIndex, method)
// System.Collections.IEnumerator System.Collections.Generic.List`1<UnityEngine.Rigidbody2D>::System.Collections.IEnumerable.GetEnumerator()
#define List_1_System_Collections_IEnumerable_GetEnumerator_m1_8316(__this, method) (( Object_t * (*) (List_1_t1_911 *, const MethodInfo*))List_1_System_Collections_IEnumerable_GetEnumerator_m1_5714_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.Rigidbody2D>::System.Collections.IList.Add(System.Object)
#define List_1_System_Collections_IList_Add_m1_8317(__this, ___item, method) (( int32_t (*) (List_1_t1_911 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Add_m1_5716_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.Rigidbody2D>::System.Collections.IList.Contains(System.Object)
#define List_1_System_Collections_IList_Contains_m1_8318(__this, ___item, method) (( bool (*) (List_1_t1_911 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Contains_m1_5718_gshared)(__this, ___item, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.Rigidbody2D>::System.Collections.IList.IndexOf(System.Object)
#define List_1_System_Collections_IList_IndexOf_m1_8319(__this, ___item, method) (( int32_t (*) (List_1_t1_911 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_IndexOf_m1_5720_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Rigidbody2D>::System.Collections.IList.Insert(System.Int32,System.Object)
#define List_1_System_Collections_IList_Insert_m1_8320(__this, ___index, ___item, method) (( void (*) (List_1_t1_911 *, int32_t, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Insert_m1_5722_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Rigidbody2D>::System.Collections.IList.Remove(System.Object)
#define List_1_System_Collections_IList_Remove_m1_8321(__this, ___item, method) (( void (*) (List_1_t1_911 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Remove_m1_5724_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.Rigidbody2D>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
#define List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m1_8322(__this, method) (( bool (*) (List_1_t1_911 *, const MethodInfo*))List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m1_5726_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<UnityEngine.Rigidbody2D>::System.Collections.ICollection.get_SyncRoot()
#define List_1_System_Collections_ICollection_get_SyncRoot_m1_8323(__this, method) (( Object_t * (*) (List_1_t1_911 *, const MethodInfo*))List_1_System_Collections_ICollection_get_SyncRoot_m1_5728_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<UnityEngine.Rigidbody2D>::System.Collections.IList.get_Item(System.Int32)
#define List_1_System_Collections_IList_get_Item_m1_8324(__this, ___index, method) (( Object_t * (*) (List_1_t1_911 *, int32_t, const MethodInfo*))List_1_System_Collections_IList_get_Item_m1_5730_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Rigidbody2D>::System.Collections.IList.set_Item(System.Int32,System.Object)
#define List_1_System_Collections_IList_set_Item_m1_8325(__this, ___index, ___value, method) (( void (*) (List_1_t1_911 *, int32_t, Object_t *, const MethodInfo*))List_1_System_Collections_IList_set_Item_m1_5732_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Rigidbody2D>::Add(T)
#define List_1_Add_m1_8326(__this, ___item, method) (( void (*) (List_1_t1_911 *, Rigidbody2D_t6_113 *, const MethodInfo*))List_1_Add_m1_5734_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Rigidbody2D>::GrowIfNeeded(System.Int32)
#define List_1_GrowIfNeeded_m1_8327(__this, ___newCount, method) (( void (*) (List_1_t1_911 *, int32_t, const MethodInfo*))List_1_GrowIfNeeded_m1_5736_gshared)(__this, ___newCount, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Rigidbody2D>::AddCollection(System.Collections.Generic.ICollection`1<T>)
#define List_1_AddCollection_m1_8328(__this, ___collection, method) (( void (*) (List_1_t1_911 *, Object_t*, const MethodInfo*))List_1_AddCollection_m1_5738_gshared)(__this, ___collection, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Rigidbody2D>::AddEnumerable(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_AddEnumerable_m1_8329(__this, ___enumerable, method) (( void (*) (List_1_t1_911 *, Object_t*, const MethodInfo*))List_1_AddEnumerable_m1_5740_gshared)(__this, ___enumerable, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Rigidbody2D>::AddRange(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_AddRange_m1_8330(__this, ___collection, method) (( void (*) (List_1_t1_911 *, Object_t*, const MethodInfo*))List_1_AddRange_m1_5619_gshared)(__this, ___collection, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Rigidbody2D>::Clear()
#define List_1_Clear_m1_8331(__this, method) (( void (*) (List_1_t1_911 *, const MethodInfo*))List_1_Clear_m1_5742_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.Rigidbody2D>::Contains(T)
#define List_1_Contains_m1_8332(__this, ___item, method) (( bool (*) (List_1_t1_911 *, Rigidbody2D_t6_113 *, const MethodInfo*))List_1_Contains_m1_5744_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Rigidbody2D>::CopyTo(T[],System.Int32)
#define List_1_CopyTo_m1_8333(__this, ___array, ___arrayIndex, method) (( void (*) (List_1_t1_911 *, Rigidbody2DU5BU5D_t6_278*, int32_t, const MethodInfo*))List_1_CopyTo_m1_5746_gshared)(__this, ___array, ___arrayIndex, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Rigidbody2D>::CheckMatch(System.Predicate`1<T>)
#define List_1_CheckMatch_m1_8334(__this /* static, unused */, ___match, method) (( void (*) (Object_t * /* static, unused */, Predicate_1_t1_1222 *, const MethodInfo*))List_1_CheckMatch_m1_5748_gshared)(__this /* static, unused */, ___match, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.Rigidbody2D>::FindIndex(System.Predicate`1<T>)
#define List_1_FindIndex_m1_8335(__this, ___match, method) (( int32_t (*) (List_1_t1_911 *, Predicate_1_t1_1222 *, const MethodInfo*))List_1_FindIndex_m1_5750_gshared)(__this, ___match, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.Rigidbody2D>::GetIndex(System.Int32,System.Int32,System.Predicate`1<T>)
#define List_1_GetIndex_m1_8336(__this, ___startIndex, ___count, ___match, method) (( int32_t (*) (List_1_t1_911 *, int32_t, int32_t, Predicate_1_t1_1222 *, const MethodInfo*))List_1_GetIndex_m1_5752_gshared)(__this, ___startIndex, ___count, ___match, method)
// System.Collections.Generic.List`1/Enumerator<T> System.Collections.Generic.List`1<UnityEngine.Rigidbody2D>::GetEnumerator()
#define List_1_GetEnumerator_m1_8337(__this, method) (( Enumerator_t1_1223  (*) (List_1_t1_911 *, const MethodInfo*))List_1_GetEnumerator_m1_5754_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.Rigidbody2D>::IndexOf(T)
#define List_1_IndexOf_m1_8338(__this, ___item, method) (( int32_t (*) (List_1_t1_911 *, Rigidbody2D_t6_113 *, const MethodInfo*))List_1_IndexOf_m1_5756_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Rigidbody2D>::Shift(System.Int32,System.Int32)
#define List_1_Shift_m1_8339(__this, ___start, ___delta, method) (( void (*) (List_1_t1_911 *, int32_t, int32_t, const MethodInfo*))List_1_Shift_m1_5758_gshared)(__this, ___start, ___delta, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Rigidbody2D>::CheckIndex(System.Int32)
#define List_1_CheckIndex_m1_8340(__this, ___index, method) (( void (*) (List_1_t1_911 *, int32_t, const MethodInfo*))List_1_CheckIndex_m1_5760_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Rigidbody2D>::Insert(System.Int32,T)
#define List_1_Insert_m1_8341(__this, ___index, ___item, method) (( void (*) (List_1_t1_911 *, int32_t, Rigidbody2D_t6_113 *, const MethodInfo*))List_1_Insert_m1_5762_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Rigidbody2D>::CheckCollection(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_CheckCollection_m1_8342(__this, ___collection, method) (( void (*) (List_1_t1_911 *, Object_t*, const MethodInfo*))List_1_CheckCollection_m1_5764_gshared)(__this, ___collection, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.Rigidbody2D>::Remove(T)
#define List_1_Remove_m1_8343(__this, ___item, method) (( bool (*) (List_1_t1_911 *, Rigidbody2D_t6_113 *, const MethodInfo*))List_1_Remove_m1_5766_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Rigidbody2D>::RemoveAt(System.Int32)
#define List_1_RemoveAt_m1_8344(__this, ___index, method) (( void (*) (List_1_t1_911 *, int32_t, const MethodInfo*))List_1_RemoveAt_m1_5768_gshared)(__this, ___index, method)
// T[] System.Collections.Generic.List`1<UnityEngine.Rigidbody2D>::ToArray()
#define List_1_ToArray_m1_8345(__this, method) (( Rigidbody2DU5BU5D_t6_278* (*) (List_1_t1_911 *, const MethodInfo*))List_1_ToArray_m1_5672_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.Rigidbody2D>::get_Capacity()
#define List_1_get_Capacity_m1_8346(__this, method) (( int32_t (*) (List_1_t1_911 *, const MethodInfo*))List_1_get_Capacity_m1_5770_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Rigidbody2D>::set_Capacity(System.Int32)
#define List_1_set_Capacity_m1_8347(__this, ___value, method) (( void (*) (List_1_t1_911 *, int32_t, const MethodInfo*))List_1_set_Capacity_m1_5772_gshared)(__this, ___value, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.Rigidbody2D>::get_Count()
#define List_1_get_Count_m1_8348(__this, method) (( int32_t (*) (List_1_t1_911 *, const MethodInfo*))List_1_get_Count_m1_5774_gshared)(__this, method)
// T System.Collections.Generic.List`1<UnityEngine.Rigidbody2D>::get_Item(System.Int32)
#define List_1_get_Item_m1_8349(__this, ___index, method) (( Rigidbody2D_t6_113 * (*) (List_1_t1_911 *, int32_t, const MethodInfo*))List_1_get_Item_m1_5776_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Rigidbody2D>::set_Item(System.Int32,T)
#define List_1_set_Item_m1_8350(__this, ___index, ___value, method) (( void (*) (List_1_t1_911 *, int32_t, Rigidbody2D_t6_113 *, const MethodInfo*))List_1_set_Item_m1_5778_gshared)(__this, ___index, ___value, method)
