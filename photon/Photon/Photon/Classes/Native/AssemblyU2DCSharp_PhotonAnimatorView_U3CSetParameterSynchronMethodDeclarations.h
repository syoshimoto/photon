﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PhotonAnimatorView/<SetParameterSynchronized>c__AnonStorey8
struct U3CSetParameterSynchronizedU3Ec__AnonStorey8_t8_135;
// PhotonAnimatorView/SynchronizedParameter
struct SynchronizedParameter_t8_128;

#include "codegen/il2cpp-codegen.h"

// System.Void PhotonAnimatorView/<SetParameterSynchronized>c__AnonStorey8::.ctor()
extern "C" void U3CSetParameterSynchronizedU3Ec__AnonStorey8__ctor_m8_867 (U3CSetParameterSynchronizedU3Ec__AnonStorey8_t8_135 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PhotonAnimatorView/<SetParameterSynchronized>c__AnonStorey8::<>m__5(PhotonAnimatorView/SynchronizedParameter)
extern "C" bool U3CSetParameterSynchronizedU3Ec__AnonStorey8_U3CU3Em__5_m8_868 (U3CSetParameterSynchronizedU3Ec__AnonStorey8_t8_135 * __this, SynchronizedParameter_t8_128 * ___item, const MethodInfo* method) IL2CPP_METHOD_ATTR;
