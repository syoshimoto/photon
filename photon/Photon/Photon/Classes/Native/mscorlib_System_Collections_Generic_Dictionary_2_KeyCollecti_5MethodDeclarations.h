﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_KeyCollecti_29MethodDeclarations.h"

// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<PunTeams/Team,System.Collections.Generic.List`1<PhotonPlayer>>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
#define KeyCollection__ctor_m1_9871(__this, ___dictionary, method) (( void (*) (KeyCollection_t1_957 *, Dictionary_2_t1_951 *, const MethodInfo*))KeyCollection__ctor_m1_9702_gshared)(__this, ___dictionary, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<PunTeams/Team,System.Collections.Generic.List`1<PhotonPlayer>>::System.Collections.Generic.ICollection<TKey>.Add(TKey)
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m1_9872(__this, ___item, method) (( void (*) (KeyCollection_t1_957 *, uint8_t, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m1_9703_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<PunTeams/Team,System.Collections.Generic.List`1<PhotonPlayer>>::System.Collections.Generic.ICollection<TKey>.Clear()
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m1_9873(__this, method) (( void (*) (KeyCollection_t1_957 *, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m1_9704_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<PunTeams/Team,System.Collections.Generic.List`1<PhotonPlayer>>::System.Collections.Generic.ICollection<TKey>.Contains(TKey)
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m1_9874(__this, ___item, method) (( bool (*) (KeyCollection_t1_957 *, uint8_t, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m1_9705_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<PunTeams/Team,System.Collections.Generic.List`1<PhotonPlayer>>::System.Collections.Generic.ICollection<TKey>.Remove(TKey)
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m1_9875(__this, ___item, method) (( bool (*) (KeyCollection_t1_957 *, uint8_t, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m1_9706_gshared)(__this, ___item, method)
// System.Collections.Generic.IEnumerator`1<TKey> System.Collections.Generic.Dictionary`2/KeyCollection<PunTeams/Team,System.Collections.Generic.List`1<PhotonPlayer>>::System.Collections.Generic.IEnumerable<TKey>.GetEnumerator()
#define KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m1_9876(__this, method) (( Object_t* (*) (KeyCollection_t1_957 *, const MethodInfo*))KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m1_9707_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<PunTeams/Team,System.Collections.Generic.List`1<PhotonPlayer>>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define KeyCollection_System_Collections_ICollection_CopyTo_m1_9877(__this, ___array, ___index, method) (( void (*) (KeyCollection_t1_957 *, Array_t *, int32_t, const MethodInfo*))KeyCollection_System_Collections_ICollection_CopyTo_m1_9708_gshared)(__this, ___array, ___index, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2/KeyCollection<PunTeams/Team,System.Collections.Generic.List`1<PhotonPlayer>>::System.Collections.IEnumerable.GetEnumerator()
#define KeyCollection_System_Collections_IEnumerable_GetEnumerator_m1_9878(__this, method) (( Object_t * (*) (KeyCollection_t1_957 *, const MethodInfo*))KeyCollection_System_Collections_IEnumerable_GetEnumerator_m1_9709_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<PunTeams/Team,System.Collections.Generic.List`1<PhotonPlayer>>::System.Collections.Generic.ICollection<TKey>.get_IsReadOnly()
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m1_9879(__this, method) (( bool (*) (KeyCollection_t1_957 *, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m1_9710_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/KeyCollection<PunTeams/Team,System.Collections.Generic.List`1<PhotonPlayer>>::System.Collections.ICollection.get_SyncRoot()
#define KeyCollection_System_Collections_ICollection_get_SyncRoot_m1_9880(__this, method) (( Object_t * (*) (KeyCollection_t1_957 *, const MethodInfo*))KeyCollection_System_Collections_ICollection_get_SyncRoot_m1_9711_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<PunTeams/Team,System.Collections.Generic.List`1<PhotonPlayer>>::CopyTo(TKey[],System.Int32)
#define KeyCollection_CopyTo_m1_9881(__this, ___array, ___index, method) (( void (*) (KeyCollection_t1_957 *, TeamU5BU5D_t8_184*, int32_t, const MethodInfo*))KeyCollection_CopyTo_m1_9712_gshared)(__this, ___array, ___index, method)
// System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2/KeyCollection<PunTeams/Team,System.Collections.Generic.List`1<PhotonPlayer>>::GetEnumerator()
#define KeyCollection_GetEnumerator_m1_5628(__this, method) (( Enumerator_t1_954  (*) (KeyCollection_t1_957 *, const MethodInfo*))KeyCollection_GetEnumerator_m1_9713_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2/KeyCollection<PunTeams/Team,System.Collections.Generic.List`1<PhotonPlayer>>::get_Count()
#define KeyCollection_get_Count_m1_9882(__this, method) (( int32_t (*) (KeyCollection_t1_957 *, const MethodInfo*))KeyCollection_get_Count_m1_9714_gshared)(__this, method)
