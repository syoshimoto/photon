﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PhotonTransformViewScaleControl
struct PhotonTransformViewScaleControl_t8_143;
// PhotonTransformViewScaleModel
struct PhotonTransformViewScaleModel_t8_140;
// PhotonStream
struct PhotonStream_t8_106;
// PhotonMessageInfo
struct PhotonMessageInfo_t8_104;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Vector3.h"

// System.Void PhotonTransformViewScaleControl::.ctor(PhotonTransformViewScaleModel)
extern "C" void PhotonTransformViewScaleControl__ctor_m8_916 (PhotonTransformViewScaleControl_t8_143 * __this, PhotonTransformViewScaleModel_t8_140 * ___model, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 PhotonTransformViewScaleControl::GetScale(UnityEngine.Vector3)
extern "C" Vector3_t6_49  PhotonTransformViewScaleControl_GetScale_m8_917 (PhotonTransformViewScaleControl_t8_143 * __this, Vector3_t6_49  ___currentScale, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PhotonTransformViewScaleControl::OnPhotonSerializeView(UnityEngine.Vector3,PhotonStream,PhotonMessageInfo)
extern "C" void PhotonTransformViewScaleControl_OnPhotonSerializeView_m8_918 (PhotonTransformViewScaleControl_t8_143 * __this, Vector3_t6_49  ___currentScale, PhotonStream_t8_106 * ___stream, PhotonMessageInfo_t8_104 * ___info, const MethodInfo* method) IL2CPP_METHOD_ATTR;
