﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.Color[]
struct ColorU5BU5D_t6_271;
// System.String[]
struct StringU5BU5D_t1_202;
// UnityEngine.Material[]
struct MaterialU5BU5D_t6_272;

#include "mscorlib_System_Object.h"

// PlayerVariables
struct  PlayerVariables_t8_60  : public Object_t
{
};
struct PlayerVariables_t8_60_StaticFields{
	// UnityEngine.Color[] PlayerVariables::playerColors
	ColorU5BU5D_t6_271* ___playerColors_0;
	// System.String[] PlayerVariables::playerColorNames
	StringU5BU5D_t1_202* ___playerColorNames_1;
	// UnityEngine.Material[] PlayerVariables::playerMaterials
	MaterialU5BU5D_t6_272* ___playerMaterials_2;
};
